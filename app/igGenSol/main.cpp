/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include <iostream>
#include <fstream>
#include <math.h>
#include <iomanip>
#include <sstream>
#include <algorithm>

#include <igFiles/igFileManager.h>
#include <igDistributed/igCommunicator.h>
#include <igCore/igMesh.h>

#include <igGenerator/igSolutionGenerator.h>
#include <igGenerator/igCaseGenerator.h>

using namespace std;

///////////////////////////////////////////////////////////////

char* getCmdOption(char ** begin, char ** end, const std::string & option)
{
    char ** itr = std::find(begin, end, option);
    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return 0;
}

bool cmdOptionExists(char** begin, char** end, const std::string& option)
{
    return std::find(begin, end, option) != end;
}


/////////////////////////////////////////////////////////

// main program
int main(int argc, char **argv)
{

    string case_name;
    string mesh_filename;
    string sol_filename;

    igFileManager *file_manager = new igFileManager;

    if (cmdOptionExists(argv, argv + argc, "-case"))
        case_name = getCmdOption(argv, argv + argc, "-case");
    else{
        cout << "case not provided !"<< endl;
        return 0;
    }

    if (cmdOptionExists(argv, argv + argc, "-mesh"))
        mesh_filename = getCmdOption(argv, argv + argc, "-mesh");
    else{
        cout << "mesh file not provided !"<< endl;
        return 0;
    }

    if (cmdOptionExists(argv, argv + argc, "-initial"))
        sol_filename = getCmdOption(argv, argv + argc, "-initial");
    else{
        cout << "initial solution file not provided !"<< endl;
        return 0;
    }

    bool perturbation = false;
    if (cmdOptionExists(argv, argv + argc, "-perturbation"))
        perturbation = true;

    //---------------- management of solution parameters ---------------------

    vector<double> *func_param;

    //  viscous burger case
    if(case_name == "viscous_burgers"){

        // default values
        func_param  = new vector<double>(3);
        (*func_param)[0] = 2;
        (*func_param)[1] = 0.1;
        (*func_param)[2] = 0;

    // default case : Euler / Navier-Stokes
    } else {

        func_param = new vector<double>(2);

        // Mach value for solution_euler_uniform function
        (*func_param)[0] = 0.5; // default Mach value

        if (cmdOptionExists(argv, argv + argc, "-mach"))
            (*func_param)[0]  = atof(getCmdOption(argv, argv + argc, "-mach"));


        // default incidence value
        (*func_param)[1] = 0.;

        if (cmdOptionExists(argv, argv + argc, "-incidence"))
            (*func_param)[1]  = atof(getCmdOption(argv, argv + argc, "-incidence"));

    }

    //------------- case parameters generation --------

    igCaseGenerator case_gen;

    case_gen.setCase(case_name);

    case_gen.generateCase();

    //--------------- mesh reading -----------------

    igCommunicator *communicator = new igCommunicator();
    communicator->initialize();

    vector<double> *coordinates = new vector<double>;
    vector<double> *velocities = new vector<double>;

    igMesh *mesh = new igMesh;
    mesh->setCommunicator(communicator);
    mesh->setCoordinates(coordinates);
    mesh->setVelocities(velocities);
    mesh->build(mesh_filename);


    //--------------- solution generation -----------------

    igSolutionGenerator *sol_gen = case_gen.solutionGenerator();

    sol_gen->setMesh(mesh);
    sol_gen->setSolutionFunction(case_gen.solutionFunction());
    sol_gen->setSolutionFunctionParameters(func_param->data());
    if(perturbation)
        sol_gen->enablePerturbation();

    for(int ivar=0; ivar<case_gen.variableNumber(); ivar++)
        sol_gen->generate(ivar);

    sol_gen->writeSolutionFile(sol_filename);

    delete mesh;
    delete velocities;
    delete coordinates;
    delete communicator;
    delete file_manager;

    delete func_param;

    return 0;
}
