/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include <algorithm>

#include <igFiles/igFileManager.h>
#include <igDistributed/igCommunicator.h>
#include <igCore/igMesh.h>
#include <igCore/igDistributor.h>
#include <igGenerator/igMeshExtruder.h>

using namespace std;

///////////////////////////////////////////////////////////////

char* getCmdOption(char ** begin, char ** end, const std::string & option)
{
    char ** itr = std::find(begin, end, option);
    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return 0;
}

bool cmdOptionExists(char** begin, char** end, const std::string& option)
{
    return std::find(begin, end, option) != end;
}


/////////////////////////////////////////////////////////

// main program
int main(int argc, char **argv)
{
    //--------------- arguments management -----------------

    string mesh_filename;
    if (cmdOptionExists(argv, argv + argc, "-mesh"))
        mesh_filename = getCmdOption(argv, argv + argc, "-mesh");
    else{
        cout << "mesh file not provided !"<< endl;
        return 0;
    }

    int layer_number = 1;
    if (cmdOptionExists(argv, argv + argc, "-layer")){
        layer_number = atoi(getCmdOption(argv, argv + argc, "-layer"));
    }

    double translation = 1;
    if (cmdOptionExists(argv, argv + argc, "-translate")){
        translation = atof(getCmdOption(argv, argv + argc, "-translate"));
    }

    double rotation = 0;
    if (cmdOptionExists(argv, argv + argc, "-rotate")){
        rotation = atof(getCmdOption(argv, argv + argc, "-rotate"));
    }

    double scale = 0;
    if (cmdOptionExists(argv, argv + argc, "-scale")){
        scale = atof(getCmdOption(argv, argv + argc, "-scale"));
    }

    bool periodic = false;
    if (cmdOptionExists(argv, argv + argc, "-periodic")){
        periodic = true;
    }

	string deformation_filename;
	bool deformation = false;
	if (cmdOptionExists(argv, argv + argc, "-deformation")){
		deformation_filename = getCmdOption(argv, argv + argc, "-deformation");
		deformation = true;
	}

	bool morphing_3D = false;
	double morphing_perturbation;
	if (cmdOptionExists(argv, argv + argc, "-morphing_3D")){
		morphing_perturbation = atof(getCmdOption(argv, argv + argc, "-morphing_3D"));
		morphing_3D = true;
	}


    //--------------- initialization -----------------

    igCommunicator *communicator = new igCommunicator();
    communicator->initialize();

    igFileManager file_manager;

    vector<double> *coordinates = new vector<double>;
    vector<double> *velocities = new vector<double>;

    igMesh *mesh = new igMesh;
    mesh->setCommunicator(communicator);
    mesh->setCoordinates(coordinates);
    mesh->setVelocities(velocities);
    mesh->build(mesh_filename);

	if(deformation)
		file_manager.readSolutionIgloo(deformation_filename,mesh->meshDimension(),mesh,velocities);


    //-------------- extrusion ----------------

    igMeshExtruder *extruder = new igMeshExtruder(mesh,coordinates,velocities);
    extruder->setLayerNumber(layer_number);
    extruder->setTranslation(translation);
    extruder->setRotation(rotation);
    extruder->setScale(scale);
    extruder->setCommunicator(communicator);
    if(periodic)
        extruder->enablePeriodicity();
    if(morphing_3D)
    	extruder->enableMorphingPerturbation(morphing_perturbation);
    extruder->extrude();

    igMesh *extruded_mesh = extruder->extrudedMesh();
    vector<double> *extruded_velocities = extruder->extrudedVelocities();

    //--------------- partitionning ----------------------------

    igDistributor *distributor = new igDistributor;
    distributor->setMesh(extruded_mesh);
    distributor->setPartitionNumber(1);
    distributor->run();

    //--------------- mesh and solution output -----------------

    cout << endl;
    cout << "mesh dimension: "<< extruded_mesh->meshDimension() << endl;
    cout << "element number: "<< extruded_mesh->elementNumber() << endl;
    cout << "face number  : "<< extruded_mesh->faceNumber() << endl;
    cout << "boundary number  : "<< extruded_mesh->boundaryFaceNumber() << endl;

    file_manager.writeMesh(extruded_mesh);
    file_manager.writeMeshIgloo("mesh_extruded.dat", extruded_mesh, distributor);

	if(deformation)
		file_manager.writeSolutionIgloo("deformation_extruded.dat",extruded_mesh->meshDimension(),extruded_mesh,extruded_velocities);

    //--------------------------------------

    delete distributor;
    delete extruder;
    delete mesh;
    delete coordinates;
    delete velocities;

    communicator->finalize();
    delete communicator;

    return 0;

}
