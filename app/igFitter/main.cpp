/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <stdlib.h>
#include <algorithm>

#include <igCore/igFittingCurvePoints.h>

using namespace std;

///////////////////////////////////////////////////////////////

char* getCmdOption(char ** begin, char ** end, const std::string & option)
{
    char ** itr = std::find(begin, end, option);
    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return 0;
}

bool cmdOptionExists(char** begin, char** end, const std::string& option)
{
    return std::find(begin, end, option) != end;
}

/////////////////////////////////////////////////////////

// main program
int main(int argc, char **argv)
{

	int N_el, degree, sample_number, multiplicity;
	string file_points;

	if (cmdOptionExists(argv, argv + argc, "-n"))
		N_el = atoi(getCmdOption(argv, argv + argc, "-n"));
    else{
        cout << "element number not provided !"<< endl;
        return 0;
    }

	if (cmdOptionExists(argv, argv + argc, "-degree"))
		degree = atoi(getCmdOption(argv, argv + argc, "-degree"));
	else{
		cout << "degree not provided !"<< endl;
		return 0;
	}

	if (cmdOptionExists(argv, argv + argc, "-multiplicity"))
		multiplicity = atoi(getCmdOption(argv, argv + argc, "-multiplicity"));
	else{
		multiplicity = 1;
	}

	if (cmdOptionExists(argv, argv + argc, "-geometry"))
		file_points = getCmdOption(argv, argv + argc, "-geometry");
	else{
		cout << "geometry not provided !"<< endl;
		return 0;
	}

	if (cmdOptionExists(argv, argv + argc, "-sample_number"))
		sample_number = atoi(getCmdOption(argv, argv + argc, "-sample_number"));
	else{
		cout << "sample number not provided !"<< endl;
		return 0;
	}

    bool force_vertical_tangent = false;
    if (cmdOptionExists(argv, argv + argc, "-vertical_tangent")){
		force_vertical_tangent = true;
	}

	bool flag_uniform = false;
	if (cmdOptionExists(argv, argv + argc, "-uniform"))
			flag_uniform = true;

	vector<double> x_pts(sample_number,0.), y_pts(sample_number,0.);

	// read points from input file
	ifstream data_file(file_points.c_str(), ios::in);
	if(data_file.is_open()){
		for(int i=0; i<sample_number; i++)
			data_file >> x_pts.at(i) >> y_pts.at(i);
	}
	data_file.close();

	igFittingCurvePoints *fitter = new igFittingCurvePoints;
	fitter->setDegree(degree);
	fitter->setSampleNumber(sample_number);
	fitter->setSpanNumber(N_el);
	fitter->setMultiplicity(multiplicity);
	fitter->setUniform(flag_uniform);
	fitter->setCoordinateBounds(x_pts.front(),x_pts.back(),y_pts.front(),y_pts.back());
	fitter->setXPoints(x_pts);
	fitter->setYPoints(y_pts);
	fitter->run();

	vector<vector<double>> x_sol = fitter->xSolution();
	vector<vector<double>> y_sol = fitter->ySolution();
	double weight = 1.;

    // force vertical tangent at leading edges
    if(force_vertical_tangent){
        x_sol.at(0).at(1) = 0;
    }

	ofstream cp_file("control_point.dat", ios::out);
	cp_file.precision(15);
	for(int i=0; i<N_el; i++){
		for(int j=0; j<(degree+1); j++){
			cp_file << scientific << x_sol.at(i).at(j) << "\t" << y_sol.at(i).at(j) << "\t" << weight << endl;
		}
		cp_file << endl;
	}
	cp_file.close();

	delete fitter;
	return 0;
}
