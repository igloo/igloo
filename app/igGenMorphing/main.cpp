/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <stdlib.h>
#include <cmath>
#include <algorithm>

#include <igCore/igElement.h>
#include <igCore/igMesh.h>
#include <igCore/igBasisBSpline.h>
#include <igDistributed/igCommunicator.h>
#include <igFiles/igFileManager.h>

using namespace std;

///////////////////////////////////////////////////////////////

char* getCmdOption(char ** begin, char ** end, const std::string & option)
{
    char ** itr = std::find(begin, end, option);
    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return 0;
}

bool cmdOptionExists(char** begin, char** end, const std::string& option)
{
    return std::find(begin, end, option) != end;
}

/////////////////////////////////////////////////////////

// main program
int main(int argc, char **argv)
{

	double length, amplitude, alpha;
	string spline_filename, mesh_filename;

	if (cmdOptionExists(argv, argv + argc, "-spline"))
		spline_filename = getCmdOption(argv, argv + argc, "-spline");
    else{
        cout << "Airfoil BSpline not provided !"<< endl;
        return 0;
    }

	if (cmdOptionExists(argv, argv + argc, "-mesh"))
		mesh_filename = getCmdOption(argv, argv + argc, "-mesh");
	else{
		cout << "mesh not provided !"<< endl;
		return 0;
	}

	length = 0.5;
	if (cmdOptionExists(argv, argv + argc, "-length"))
		length = atof(getCmdOption(argv, argv + argc, "-length"));

	amplitude = 0.05;
	if (cmdOptionExists(argv, argv + argc, "-amplitude"))
		amplitude = atof(getCmdOption(argv, argv + argc, "-amplitude"));

	alpha = 0.;
	if (cmdOptionExists(argv, argv + argc, "-alpha")){
		alpha = atof(getCmdOption(argv, argv + argc, "-alpha"));
		alpha = alpha*M_PI/180;
	}

	// Reading airfoil spline information
	int degree, knot_number;
	ifstream spline_file(spline_filename, ios::in);
	spline_file >> degree;
	spline_file >> knot_number;

	// Reading knot vector
	vector<double> knot_vector(knot_number);
	for(int i=0; i<knot_number; i++)
		spline_file >> knot_vector.at(i);

	// Initialize spline
    igBasisBSpline *basis = new igBasisBSpline(degree,&knot_vector);
    int function_number = basis->functionNumber();

    // Reading spline control points
    vector<double> cp_x(function_number,0.);
    vector<double> cp_y(function_number,0.);
    for(int i=0; i<function_number; i++)
    	spline_file >> cp_x.at(i) >> cp_y.at(i);
    spline_file.close();

    // Compute morphing of spline airfoil
    vector<double> cp_dx(function_number,0.);
	vector<double> cp_dy(function_number,0.);
	double cp_x0 = 1. - length;
	for(int i=0; i<function_number; i++){
		double cp_xi = cp_x.at(i);
		if(cp_xi > cp_x0){
			double eta = (cp_xi - cp_x0)/length;
			cp_dy.at(i) = amplitude * eta * eta;
		}
	}

	// Extract morphing of Bezier airfoil
	vector<vector<double>> dx_bezier, dy_bezier;
	basis->extractBezier(&cp_dx, &cp_dy, &dx_bezier, &dy_bezier);

	int airfoil_elements = dx_bezier.size();

	// Initialize communicator and mesh
    igCommunicator *communicator = new igCommunicator();
    communicator->initialize();

    vector<double> *coordinates = new vector<double>;
    vector<double> *velocities = new vector<double>;

    igMesh *mesh = new igMesh;
    mesh->setCommunicator(communicator);
    mesh->setCoordinates(coordinates);
    mesh->setVelocities(velocities);
    mesh->build(mesh_filename);

    // Initialize morphing state
    int internal_dof = mesh->controlPointNumber();
    int shared_dof = mesh->sharedControlPointNumber();
    int dimension = mesh->meshDimension();
    vector<double> *morphing_state = new vector<double>;
    morphing_state->resize((internal_dof+shared_dof)*dimension,0.);

    double dx_j, dy_j;
    int local_idx, global_idx;
    int control_point_number_by_direction = degree + 1;

    // Loop over airfoil elements
    for(int iel=0; iel<airfoil_elements; iel++){

    	igElement *elt = mesh->element(iel+2);
    	if(elt->subdomain()==1){

    		// Apply morphing weights to airfoil elements
    		// Loop over control points
    		for(int ipt=0; ipt<control_point_number_by_direction; ipt++){

    			double dy_0 = dy_bezier.at(iel).at(ipt);
    			for(int jpt=0; jpt<control_point_number_by_direction; jpt++){


    				dy_j = 0.;
        			if(jpt<2)
        				dy_j = dy_0;

        			// Apply rotation
        			dx_j = -dy_j*sin(alpha);
        			dy_j = dy_j*cos(alpha);

    				local_idx = ipt + jpt*control_point_number_by_direction;

    				global_idx = elt->globalId(local_idx,0);
    				morphing_state->at(global_idx) = dx_j;

    				global_idx = elt->globalId(local_idx,1);
    				morphing_state->at(global_idx) = dy_j;

    			}

    		}

    	}

    }

    // Treat wake elements
    igElement *elt = mesh->element(2+airfoil_elements);
    if(elt->subdomain()==1){

    	// Loop over control points
    	for(int ipt=0; ipt<control_point_number_by_direction; ipt++){

    		double factor_i = (double)ipt/(double)degree;
    		double dy_0 = 0.5*amplitude*(1. + cos(M_PI*factor_i));

    		for(int jpt=0; jpt<control_point_number_by_direction; jpt++){

    			dy_j = 0.;
    			if(jpt<2)
    				dy_j = dy_0;

    			// Apply rotation
    			dx_j = -dy_j*sin(alpha);
    			dy_j = dy_j*cos(alpha);

    			local_idx = ipt + jpt*control_point_number_by_direction;

				global_idx = elt->globalId(local_idx,0);
				morphing_state->at(global_idx) = dx_j;

    			global_idx = elt->globalId(local_idx,1);
    			morphing_state->at(global_idx) = dy_j;

    		}

    	}

    }
    elt = mesh->element(1);
    if(elt->subdomain()==1){

    	// Loop over control points
    	for(int ipt=0; ipt<control_point_number_by_direction; ipt++){

    		double factor_i = ((double)degree - (double)ipt)/(double)degree;
    		double dy_0 = 0.5*amplitude*(1. + cos(M_PI*factor_i));

    		for(int jpt=0; jpt<control_point_number_by_direction; jpt++){

    			dy_j = 0.;
    			if(jpt<2)
    				dy_j = dy_0;

    			// Apply rotation
    			dx_j = -dy_j*sin(alpha);
    			dy_j = dy_j*cos(alpha);

    			local_idx = ipt + jpt*control_point_number_by_direction;

				global_idx = elt->globalId(local_idx,0);
				morphing_state->at(global_idx) = dx_j;

    			global_idx = elt->globalId(local_idx,1);
    			morphing_state->at(global_idx) = dy_j;

    		}

    	}

    }

    // Write morphing field
    igFileManager file_manager;
    file_manager.writeSolutionIgloo("deformation.dat",dimension,mesh,morphing_state);

    delete morphing_state;
    delete mesh;
    delete coordinates;
    delete velocities;
    delete basis;

    if(communicator)
        communicator->finalize();
    delete communicator;

	return 0;
}
