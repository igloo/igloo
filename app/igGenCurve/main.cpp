/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include <iostream>
#include <fstream>
#include <math.h>
#include <iomanip>
#include <sstream>
#include <algorithm>

#include <igCore/igBasisBSpline.h>
#include <igCore/igInterpolatorCurve.h>

using namespace std;

///////////////////////////////////////////////////////////////

char* getCmdOption(char ** begin, char ** end, const std::string & option)
{
	char ** itr = std::find(begin, end, option);
	if (itr != end && ++itr != end)
	{
		return *itr;
	}
	return 0;
}

bool cmdOptionExists(char** begin, char** end, const std::string& option)
{
	return std::find(begin, end, option) != end;
}


/////////////////////////////////////////////////////////

// main program
int main(int argc, char **argv)
{
	string case_name;

	if (cmdOptionExists(argv, argv + argc, "-case"))
		case_name = getCmdOption(argv, argv + argc, "-case");
	else{
		cout << "case not provided !"<< endl;
		return 0;
	}

	vector<int> degree;
	vector<int> element_number;
	vector<int> pt_number;
	vector<int> knot_number;

	vector<vector<double>> pt_x;
	vector<vector<double>> pt_y;
	vector<vector<double>*> knot_vector;

	int curve_number;

    double coef_grid;
    double coef_membrane;
    double ratio;
    int clamp_number;

///////////////////////////////////////////////////////

	if(case_name == "airfoil_2"){

		curve_number = 2;

		pt_x.resize(curve_number);
		pt_y.resize(curve_number);
		knot_vector.resize(curve_number);

		degree.resize(curve_number);
		element_number.resize(curve_number);
		pt_number.resize(curve_number);
		knot_number.resize(curve_number);

		//--------- read user-defined parameter ------------
		double param_extra = 0;
		if (cmdOptionExists(argv, argv + argc, "-extra"))
			param_extra  = atof(getCmdOption(argv, argv + argc, "-extra"));
		else{
			cout << "parameter extrados not provided !"<< endl;
			return 0;
		}

		double param_intra = 0;
		if (cmdOptionExists(argv, argv + argc, "-intra"))
			param_intra  = atof(getCmdOption(argv, argv + argc, "-intra"));
		else{
			cout << "parameter intrados not provided !"<< endl;
			return 0;
		}

		//--------- curve 1 properties -----------------

		degree[0] = 2;
		element_number[0] = 4;
		pt_number[0] = 6;
		knot_number[0] = pt_number[0] + degree[0] + 1;

		// array allocation
		pt_x[0].resize(pt_number[0]);
		pt_y[0].resize(pt_number[0]);
		knot_vector[0] = new vector<double>;
		knot_vector[0]->resize(knot_number[0]);

		// curve definition for extrados
		pt_x[0][0] = 0.;
		pt_y[0][0] = 0.;

		pt_x[0][1] = 0.;
		pt_y[0][1] = 0.03 * param_extra;

		pt_x[0][2] = 0.06;
		pt_y[0][2] = 0.08 * param_extra;

		pt_x[0][3] = 0.3;
		pt_y[0][3] = 0.12 * param_extra;

		pt_x[0][4] = 0.7;
		pt_y[0][4] = 0.08 * param_extra;

		pt_x[0][5] = 1.;
		pt_y[0][5] = 0.;

		knot_vector[0]->at(0) = 0.;
		knot_vector[0]->at(1) = 0.;
		knot_vector[0]->at(2) = 0.;
		knot_vector[0]->at(3) = 0.1;
		knot_vector[0]->at(4) = 0.3;
		knot_vector[0]->at(5) = 0.7;
		knot_vector[0]->at(6) = 1.;
		knot_vector[0]->at(7) = 1.;
		knot_vector[0]->at(8) = 1.;

		//--------- curve 2 properties -----------------

		degree[1] = 2;
		element_number[1] = 4;
		pt_number[1] = 6;
		knot_number[1] = pt_number[1] + degree[1] + 1;

		// array allocation
		pt_x[1].resize(pt_number[1]);
		pt_y[1].resize(pt_number[1]);
		knot_vector[1] = new vector<double>;
		knot_vector[1]->resize(knot_number[1]);

		// curve definition for extrados
		pt_x[1][0] = 0.;
		pt_y[1][0] = 0.;

		pt_x[1][1] = 0.;
		pt_y[1][1] = -0.03 * param_intra;

		pt_x[1][2] = 0.06;
		pt_y[1][2] = -0.08 * param_intra;

		pt_x[1][3] = 0.3;
		pt_y[1][3] = -0.12 * param_intra;

		pt_x[1][4] = 0.7;
		pt_y[1][4] = -0.08 * param_intra;

		pt_x[1][5] = 1.;
		pt_y[1][5] = 0.;

		knot_vector[1]->at(0) = 0.;
		knot_vector[1]->at(1) = 0.;
		knot_vector[1]->at(2) = 0.;
		knot_vector[1]->at(3) = 0.1;
		knot_vector[1]->at(4) = 0.3;
		knot_vector[1]->at(5) = 0.7;
		knot_vector[1]->at(6) = 1.;
		knot_vector[1]->at(7) = 1.;
		knot_vector[1]->at(8) = 1.;

	}

	///////////////////////////////////////////////////////

	if(case_name == "airfoil_8"){

		curve_number = 2;

		pt_x.resize(curve_number);
		pt_y.resize(curve_number);
		knot_vector.resize(curve_number);

		degree.resize(curve_number);
		element_number.resize(curve_number);
		pt_number.resize(curve_number);
		knot_number.resize(curve_number);

		//--------- read user-defined parameter ------------

		double up1 = 0.015;
		if (cmdOptionExists(argv, argv + argc, "-up1")){
			up1  = atof(getCmdOption(argv, argv + argc, "-up1"));
		}

		double up2 = 0.05;
		if (cmdOptionExists(argv, argv + argc, "-up2")){
			up2  = atof(getCmdOption(argv, argv + argc, "-up2"));
		}

		double up3 = 0.07;
		if (cmdOptionExists(argv, argv + argc, "-up3")){
			up3  = atof(getCmdOption(argv, argv + argc, "-up3"));
		}

		double up4 = 0.05;
		if (cmdOptionExists(argv, argv + argc, "-up4")){
			up4  = atof(getCmdOption(argv, argv + argc, "-up4"));
		}

		double down1 = -0.015;
		if (cmdOptionExists(argv, argv + argc, "-down1")){
			down1  = atof(getCmdOption(argv, argv + argc, "-down1"));
		}

		double down2 = -0.05;
		if (cmdOptionExists(argv, argv + argc, "-down2")){
			down2  = atof(getCmdOption(argv, argv + argc, "-down2"));
		}

		double down3 = -0.07;
		if (cmdOptionExists(argv, argv + argc, "-down3")){
			down3  = atof(getCmdOption(argv, argv + argc, "-down3"));
		}

		double down4 = -0.05;
		if (cmdOptionExists(argv, argv + argc, "-down4")){
			down4  = atof(getCmdOption(argv, argv + argc, "-down4"));
		}


		//--------- curve 1 properties -----------------

		degree[0] = 2;
		element_number[0] = 4;
		pt_number[0] = 6;
		knot_number[0] = pt_number[0] + degree[0] + 1;

		// array allocation
		pt_x[0].resize(pt_number[0]);
		pt_y[0].resize(pt_number[0]);
		knot_vector[0] = new vector<double>;
		knot_vector[0]->resize(knot_number[0]);

		// curve definition for extrados
		pt_x[0][0] = 0.;
		pt_y[0][0] = 0.;

		pt_x[0][1] = 0.;
		pt_y[0][1] = up1;

		pt_x[0][2] = 0.06;
		pt_y[0][2] = up2;

		pt_x[0][3] = 0.3;
		pt_y[0][3] = up3;

		pt_x[0][4] = 0.7;
		pt_y[0][4] = up4;

		pt_x[0][5] = 1.;
		pt_y[0][5] = 0.;

		knot_vector[0]->at(0) = 0.;
		knot_vector[0]->at(1) = 0.;
		knot_vector[0]->at(2) = 0.;
		knot_vector[0]->at(3) = 0.1;
		knot_vector[0]->at(4) = 0.3;
		knot_vector[0]->at(5) = 0.7;
		knot_vector[0]->at(6) = 1.;
		knot_vector[0]->at(7) = 1.;
		knot_vector[0]->at(8) = 1.;

		//--------- curve 2 properties -----------------

		degree[1] = 2;
		element_number[1] = 4;
		pt_number[1] = 6;
		knot_number[1] = pt_number[1] + degree[1] + 1;

		// array allocation
		pt_x[1].resize(pt_number[1]);
		pt_y[1].resize(pt_number[1]);
		knot_vector[1] = new vector<double>;
		knot_vector[1]->resize(knot_number[1]);

		// curve definition for extrados
		pt_x[1][0] = 0.;
		pt_y[1][0] = 0.;

		pt_x[1][1] = 0.;
		pt_y[1][1] = down1;

		pt_x[1][2] = 0.06;
		pt_y[1][2] = down2;

		pt_x[1][3] = 0.3;
		pt_y[1][3] = down3;

		pt_x[1][4] = 0.7;
		pt_y[1][4] = down4;

		pt_x[1][5] = 1.;
		pt_y[1][5] = 0.;

		knot_vector[1]->at(0) = 0.;
		knot_vector[1]->at(1) = 0.;
		knot_vector[1]->at(2) = 0.;
		knot_vector[1]->at(3) = 0.1;
		knot_vector[1]->at(4) = 0.3;
		knot_vector[1]->at(5) = 0.7;
		knot_vector[1]->at(6) = 1.;
		knot_vector[1]->at(7) = 1.;
		knot_vector[1]->at(8) = 1.;

	}

    ///////////////////////////////////////////////////////

	if(case_name == "airfoil_16"){

		curve_number = 2;

		pt_x.resize(curve_number);
		pt_y.resize(curve_number);
		knot_vector.resize(curve_number);

		degree.resize(curve_number);
		element_number.resize(curve_number);
		pt_number.resize(curve_number);
		knot_number.resize(curve_number);

		//--------- read user-defined parameter ------------

		double up1 = 0.02;
		if (cmdOptionExists(argv, argv + argc, "-up1")){
			up1  = atof(getCmdOption(argv, argv + argc, "-up1"));
		}

		double up2 = 0.04;
		if (cmdOptionExists(argv, argv + argc, "-up2")){
			up2  = atof(getCmdOption(argv, argv + argc, "-up2"));
		}

		double up3 = 0.05;
		if (cmdOptionExists(argv, argv + argc, "-up3")){
			up3  = atof(getCmdOption(argv, argv + argc, "-up3"));
		}

		double up4 = 0.07;
		if (cmdOptionExists(argv, argv + argc, "-up4")){
			up4  = atof(getCmdOption(argv, argv + argc, "-up4"));
		}

        double up5 = 0.065;
		if (cmdOptionExists(argv, argv + argc, "-up5")){
			up5  = atof(getCmdOption(argv, argv + argc, "-up5"));
		}

		double up6 = 0.05;
		if (cmdOptionExists(argv, argv + argc, "-up6")){
			up6  = atof(getCmdOption(argv, argv + argc, "-up6"));
		}

		double up7 = 0.02;
		if (cmdOptionExists(argv, argv + argc, "-up7")){
			up7  = atof(getCmdOption(argv, argv + argc, "-up7"));
		}

		double up8 = 0.01;
		if (cmdOptionExists(argv, argv + argc, "-up8")){
			up8  = atof(getCmdOption(argv, argv + argc, "-up8"));
		}

        //---------

		double down1 = -0.015;
		if (cmdOptionExists(argv, argv + argc, "-down1")){
			down1  = atof(getCmdOption(argv, argv + argc, "-down1"));
		}

		double down2 = -0.045;
		if (cmdOptionExists(argv, argv + argc, "-down2")){
			down2  = atof(getCmdOption(argv, argv + argc, "-down2"));
		}

		double down3 = -0.06;
		if (cmdOptionExists(argv, argv + argc, "-down3")){
			down3  = atof(getCmdOption(argv, argv + argc, "-down3"));
		}

		double down4 = -0.08;
		if (cmdOptionExists(argv, argv + argc, "-down4")){
			down4  = atof(getCmdOption(argv, argv + argc, "-down4"));
		}

        double down5 = -0.07;
        if (cmdOptionExists(argv, argv + argc, "-down5")){
            down5  = atof(getCmdOption(argv, argv + argc, "-down5"));
        }

        double down6 = -0.03;
        if (cmdOptionExists(argv, argv + argc, "-down6")){
            down6  = atof(getCmdOption(argv, argv + argc, "-down6"));
        }

        double down7 = -0.005;
        if (cmdOptionExists(argv, argv + argc, "-down7")){
            down7  = atof(getCmdOption(argv, argv + argc, "-down7"));
        }

        double down8 = -0.00;
        if (cmdOptionExists(argv, argv + argc, "-down8")){
            down8  = atof(getCmdOption(argv, argv + argc, "-down8"));
        }


		//--------- curve 1 properties -----------------

		degree[0] = 3;
		element_number[0] = 7;
		pt_number[0] = 10;
		knot_number[0] = pt_number[0] + degree[0] + 1;

		// array allocation
		pt_x[0].resize(pt_number[0]);
		pt_y[0].resize(pt_number[0]);
		knot_vector[0] = new vector<double>;
		knot_vector[0]->resize(knot_number[0]);

		// curve definition for extrados
	    pt_y[0][0] = 0.;
		pt_y[0][1] = up1;
		pt_y[0][2] = up2;
		pt_y[0][3] = up3;
		pt_y[0][4] = up4;
		pt_y[0][5] = up5;
		pt_y[0][6] = up6;
		pt_y[0][7] = up7;
		pt_y[0][8] = up8;
		pt_y[0][9] = 0.;

        // uniform x points with vertical tangent at first extremity
        double current_x = 0;
        double delta_x = 1/double(pt_number[0]-2);
        pt_x[0].at(0) = current_x;
        for(int ipt=1; ipt<pt_number[0]; ipt++){
            pt_x[0].at(ipt) = current_x;
            current_x += delta_x;
        }

        // uniform knot vector
        double knot_span = 1/double(pt_number[0]-degree[0]);
        double current_knot = 0;
        for(int i=0; i<degree[0]+1; i++){
            knot_vector[0]->at(i) = current_knot;
        }
        for(int i=degree[0]+1; i<pt_number[0]; i++){
            current_knot += knot_span;
            knot_vector[0]->at(i) = current_knot;
        }
        current_knot = 1;
        for(int i=pt_number[0]; i<pt_number[0]+degree[0]+1; i++){
            knot_vector[0]->at(i) = current_knot;
        }

		//--------- curve 2 properties -----------------

        degree[1] = 3;
        element_number[1] = 7;
        pt_number[1] = 10;
        knot_number[1] = pt_number[1] + degree[1] + 1;

        // array allocation
        pt_x[1].resize(pt_number[1]);
        pt_y[1].resize(pt_number[1]);
        knot_vector[1] = new vector<double>;
        knot_vector[1]->resize(knot_number[1]);

        // curve definition for extrados
        pt_y[1][0] = 0.;
        pt_y[1][1] = down1;
        pt_y[1][2] = down2;
        pt_y[1][3] = down3;
        pt_y[1][4] = down4;
        pt_y[1][5] = down5;
        pt_y[1][6] = down6;
        pt_y[1][7] = down7;
        pt_y[1][8] = down8;
        pt_y[1][9] = 0.;

        // uniform x points with vertical tangent at first extremity
        current_x = 0;
        delta_x = 1/double(pt_number[1]-2);
        pt_x[1].at(0) = current_x;
        for(int ipt=1; ipt<pt_number[1]; ipt++){
            pt_x[1].at(ipt) = current_x;
            current_x += delta_x;
        }

        // uniform knot vector
        knot_span = 1/double(pt_number[1]-degree[1]);
        current_knot = 0;
        for(int i=0; i<degree[1]+1; i++){
            knot_vector[1]->at(i) = current_knot;
        }
        for(int i=degree[1]+1; i<pt_number[1]; i++){
            current_knot += knot_span;
            knot_vector[1]->at(i) = current_knot;
        }
        current_knot = 1;
        for(int i=pt_number[1]; i<pt_number[1]+degree[1]+1; i++){
            knot_vector[1]->at(i) = current_knot;
        }

	}

    ///////////////////////////////////////////////////////

	if(case_name == "airfoil_6"){

		curve_number = 2;

		pt_x.resize(curve_number);
		pt_y.resize(curve_number);
		knot_vector.resize(curve_number);

		degree.resize(curve_number);
		element_number.resize(curve_number);
		pt_number.resize(curve_number);
		knot_number.resize(curve_number);

		//--------- read user-defined parameter ------------

		double up1 = 0.02;
		if (cmdOptionExists(argv, argv + argc, "-up1")){
			up1  = atof(getCmdOption(argv, argv + argc, "-up1"));
		}

		double up2 = 0.07;
		if (cmdOptionExists(argv, argv + argc, "-up2")){
			up2  = atof(getCmdOption(argv, argv + argc, "-up2"));
		}

		double up3 = 0.05;
		if (cmdOptionExists(argv, argv + argc, "-up3")){
			up3  = atof(getCmdOption(argv, argv + argc, "-up3"));
		}

        //---------

		double down1 = -0.02;
		if (cmdOptionExists(argv, argv + argc, "-down1")){
			down1  = atof(getCmdOption(argv, argv + argc, "-down1"));
		}

		double down2 = -0.05;
		if (cmdOptionExists(argv, argv + argc, "-down2")){
			down2  = atof(getCmdOption(argv, argv + argc, "-down2"));
		}

		double down3 = -0.03;
		if (cmdOptionExists(argv, argv + argc, "-down3")){
			down3  = atof(getCmdOption(argv, argv + argc, "-down3"));
		}


		//--------- curve 1 properties -----------------

		degree[0] = 3;
		element_number[0] = 2;
		pt_number[0] = 5;
		knot_number[0] = pt_number[0] + degree[0] + 1;

		// array allocation
		pt_x[0].resize(pt_number[0]);
		pt_y[0].resize(pt_number[0]);
		knot_vector[0] = new vector<double>;
		knot_vector[0]->resize(knot_number[0]);

		// curve definition for extrados
	    pt_y[0][0] = 0.;
		pt_y[0][1] = up1;
		pt_y[0][2] = up2;
		pt_y[0][3] = up3;
		pt_y[0][4] = 0.;

        // uniform x points with vertical tangent at first extremity
        double current_x = 0;
        double delta_x = 1/double(pt_number[0]-2);
        pt_x[0].at(0) = current_x;
        for(int ipt=1; ipt<pt_number[0]; ipt++){
            pt_x[0].at(ipt) = current_x;
            current_x += delta_x;
        }

        // uniform knot vector
        double knot_span = 1/double(pt_number[0]-degree[0]);
        double current_knot = 0;
        for(int i=0; i<degree[0]+1; i++){
            knot_vector[0]->at(i) = current_knot;
        }
        for(int i=degree[0]+1; i<pt_number[0]; i++){
            current_knot += knot_span;
            knot_vector[0]->at(i) = current_knot;
        }
        current_knot = 1;
        for(int i=pt_number[0]; i<pt_number[0]+degree[0]+1; i++){
            knot_vector[0]->at(i) = current_knot;
        }

		//--------- curve 2 properties -----------------

        degree[1] = 3;
        element_number[1] = 2;
        pt_number[1] = 5;
        knot_number[1] = pt_number[1] + degree[1] + 1;

        // array allocation
        pt_x[1].resize(pt_number[1]);
        pt_y[1].resize(pt_number[1]);
        knot_vector[1] = new vector<double>;
        knot_vector[1]->resize(knot_number[1]);

        // curve definition for extrados
        pt_y[1][0] = 0.;
        pt_y[1][1] = down1;
        pt_y[1][2] = down2;
        pt_y[1][3] = down3;
        pt_y[1][9] = 0.;

        // uniform x points with vertical tangent at first extremity
        current_x = 0;
        delta_x = 1/double(pt_number[1]-2);
        pt_x[1].at(0) = current_x;
        for(int ipt=1; ipt<pt_number[1]; ipt++){
            pt_x[1].at(ipt) = current_x;
            current_x += delta_x;
        }

        // uniform knot vector
        knot_span = 1/double(pt_number[1]-degree[1]);
        current_knot = 0;
        for(int i=0; i<degree[1]+1; i++){
            knot_vector[1]->at(i) = current_knot;
        }
        for(int i=degree[1]+1; i<pt_number[1]; i++){
            current_knot += knot_span;
            knot_vector[1]->at(i) = current_knot;
        }
        current_knot = 1;
        for(int i=pt_number[1]; i<pt_number[1]+degree[1]+1; i++){
            knot_vector[1]->at(i) = current_knot;
        }

	}


    ///////////////////////////////////////////////////////

	if(case_name == "bezier_6"){

		curve_number = 2;

		pt_x.resize(curve_number);
		pt_y.resize(curve_number);
		knot_vector.resize(curve_number);

		degree.resize(curve_number);
		element_number.resize(curve_number);
		pt_number.resize(curve_number);
		knot_number.resize(curve_number);

		//--------- read user-defined parameter ------------

		double up1 = 0.1;
		if (cmdOptionExists(argv, argv + argc, "-up1")){
			up1  = atof(getCmdOption(argv, argv + argc, "-up1"));
		}

		double up2 = 0.2;
		if (cmdOptionExists(argv, argv + argc, "-up2")){
			up2  = atof(getCmdOption(argv, argv + argc, "-up2"));
		}

		double up3 = 0.1;
		if (cmdOptionExists(argv, argv + argc, "-up3")){
			up3  = atof(getCmdOption(argv, argv + argc, "-up3"));
		}

		double down1 = -0.1;
		if (cmdOptionExists(argv, argv + argc, "-down1")){
			down1  = atof(getCmdOption(argv, argv + argc, "-down1"));
		}

		double down2 = -0.2;
		if (cmdOptionExists(argv, argv + argc, "-down2")){
			down2  = atof(getCmdOption(argv, argv + argc, "-down2"));
		}

		double down3 = -0.1;
		if (cmdOptionExists(argv, argv + argc, "-down3")){
			down3  = atof(getCmdOption(argv, argv + argc, "-down3"));
		}


		//--------- curve 1 properties -----------------

		degree[0] = 4;
		element_number[0] = 1;
		pt_number[0] = 5;
		knot_number[0] = pt_number[0] + degree[0] + 1;

		// array allocation
		pt_x[0].resize(pt_number[0]);
		pt_y[0].resize(pt_number[0]);
		knot_vector[0] = new vector<double>;
		knot_vector[0]->resize(knot_number[0]);

		// curve definition for extrados
		pt_x[0][0] = 0.;
		pt_y[0][0] = 0.;

		pt_x[0][1] = 0.;
		pt_y[0][1] = up1;

		pt_x[0][2] = 0.3;
		pt_y[0][2] = up2;

		pt_x[0][3] = 0.6;
		pt_y[0][3] = up3;

		pt_x[0][4] = 1.;
		pt_y[0][4] = 0.;

		knot_vector[0]->at(0) = 0.;
		knot_vector[0]->at(1) = 0.;
		knot_vector[0]->at(2) = 0.;
		knot_vector[0]->at(3) = 0.;
		knot_vector[0]->at(4) = 0.;
		knot_vector[0]->at(5) = 1.;
		knot_vector[0]->at(6) = 1.;
		knot_vector[0]->at(7) = 1.;
		knot_vector[0]->at(8) = 1.;
		knot_vector[0]->at(9) = 1.;

		//--------- curve 2 properties -----------------

		degree[1] = 4;
		element_number[1] = 1;
		pt_number[1] = 5;
		knot_number[1] = pt_number[1] + degree[1] + 1;

		// array allocation
		pt_x[1].resize(pt_number[1]);
		pt_y[1].resize(pt_number[1]);
		knot_vector[1] = new vector<double>;
		knot_vector[1]->resize(knot_number[1]);

		// curve definition for extrados
		pt_x[1][0] = 0.;
		pt_y[1][0] = 0.;

		pt_x[1][1] = 0.;
		pt_y[1][1] = down1;

		pt_x[1][2] = 0.3;
		pt_y[1][2] = down2;

		pt_x[1][3] = 0.6;
		pt_y[1][3] = down3;

		pt_x[1][4] = 1.;
		pt_y[1][4] = 0.;

        knot_vector[1]->at(0) = 0.;
        knot_vector[1]->at(1) = 0.;
        knot_vector[1]->at(2) = 0.;
        knot_vector[1]->at(3) = 0.;
        knot_vector[1]->at(4) = 0.;
        knot_vector[1]->at(5) = 1.;
        knot_vector[1]->at(6) = 1.;
        knot_vector[1]->at(7) = 1.;
        knot_vector[1]->at(8) = 1.;
        knot_vector[1]->at(9) = 1.;

	}

    if(case_name == "membrane"){

		curve_number = 1;

		pt_x.resize(curve_number);
		pt_y.resize(curve_number);
		knot_vector.resize(curve_number);

		degree.resize(curve_number);
		element_number.resize(curve_number);
		pt_number.resize(curve_number);
		knot_number.resize(curve_number);

		//--------- read user-defined parameter ------------

        degree[0] = 2;
        if (cmdOptionExists(argv, argv + argc, "-degree")){
            degree[0] = atoi(getCmdOption(argv, argv + argc, "-degree"));
        }

        pt_number[0] = 5;
        if (cmdOptionExists(argv, argv + argc, "-pts")){
            pt_number[0] = atoi(getCmdOption(argv, argv + argc, "-pts"));
            if(pt_number[0] % 2 == 0)
                pt_number[0]++;
        }

        coef_membrane = 1.2;
        if (cmdOptionExists(argv, argv + argc, "-coef_membrane")){
            coef_membrane = atof(getCmdOption(argv, argv + argc, "-coef_membrane"));
        }

        coef_grid = 1.2;
        if (cmdOptionExists(argv, argv + argc, "-coef_grid")){
            coef_grid = atof(getCmdOption(argv, argv + argc, "-coef_grid"));
        }

        ratio = 0.5;
        if (cmdOptionExists(argv, argv + argc, "-ratio")){
            ratio = atof(getCmdOption(argv, argv + argc, "-ratio"));
        }

        clamp_number = 3;
        if (cmdOptionExists(argv, argv + argc, "-clamp")){
            clamp_number = atoi(getCmdOption(argv, argv + argc, "-clamp"));
        }

		//--------- curve properties -----------------

		element_number[0] =  pt_number[0] - degree[0];
		knot_number[0] = pt_number[0] + degree[0] + 1;

		// array allocation
		pt_x[0].resize(pt_number[0]);
		pt_y[0].resize(pt_number[0]);
		knot_vector[0] = new vector<double>;
		knot_vector[0]->resize(knot_number[0]);
        //
        // // distribution of control points for identity mapping
        // pt_x[0][0] = 0;
        // for(int i=1; i<degree[0]; i++){
        //     pt_x[0][i] = pt_x[0][i-1] + 1/double(pt_number[0]-degree[0])/degree[0]*i;
        // }
        // for(int i=degree[0]; i<pt_number[0]-degree[0]; i++){
        //     pt_x[0][i] = pt_x[0][i-1] + 1/double(pt_number[0]-degree[0]);
        // }
        // pt_x[0][pt_number[0]-1] = 1.;
        // for(int i=1; i<degree[0]; i++){
        //     pt_x[0][pt_number[0]-1-i] = pt_x[0][pt_number[0]-i] - 1/double(pt_number[0]-degree[0])/degree[0]*i;
        // }

        // uniform distribution of knots
        for(int i=0; i<degree[0]+1; i++){
            knot_vector[0]->at(i) = 0;
        }
        for(int i=degree[0]+1; i<knot_number[0]-degree[0]-1; i++){
            knot_vector[0]->at(i) = double(i-degree[0])/double(element_number[0]);
        }
        for(int i=knot_number[0]-degree[0]-1; i<knot_number[0]; i++){
            knot_vector[0]->at(i) = 1;
        }

        // interpolate geometric distribution for x-coordinates
        double step = 0.5*(1-coef_membrane)/(1-pow(coef_membrane,pt_number[0]/2));

        vector<double> target_param;
        target_param.resize(pt_number[0],0);
        for(int i=0; i<pt_number[0]; i++){
            target_param[i] = double(i)/double(pt_number[0]-1);
        }

        vector<double> target;
        target.resize(pt_number[0],0);
        target[0] = 0;
        for(int i=1; i<pt_number[0]/2+1; i++){
            target[i] = target[i-1] + step;
            step *= coef_membrane;
        }
        step /= coef_membrane;
        for(int i=pt_number[0]/2+1; i<pt_number[0]-1; i++){
            target[i] = target[i-1] + step;
            step /= coef_membrane;
        }
        target[pt_number[0]-1] = 1;

        igInterpolatorCurve *interpolator = new igInterpolatorCurve();
        interpolator->setDegree(degree[0]);
        interpolator->setKnotVector(knot_vector[0]);
        interpolator->setPoints(target_param, target);
        interpolator->run();
        for(int i=0; i<pt_number[0]; i++){
            pt_x[0][i] = interpolator->controlPoint(i);
        }

        // zero y-coordinates
        for(int i=0; i<pt_number[0]; i++){
            pt_y[0][i] = 0;
        }

        cout << "CFL length: " << (pt_x[0][1] - pt_x[0][0])*ratio << endl;


	}

///////////////// write nurbs file //////////////////////////////////////

	ofstream nurbs_file("nurbs.dat", ios::out);

    nurbs_file << degree[0] << endl;
    nurbs_file << pt_number[0] << endl;

    for(int i=0; i<knot_number[0]; i++){
        nurbs_file << knot_vector[0]->at(i) << endl;
    }

	for(int icurv=0; icurv<curve_number; icurv++){

        for(int i=0; i<pt_number[icurv]; i++){
            nurbs_file << pt_x[icurv][i] << " " << pt_y[icurv][i] << endl;
        }

	}
	nurbs_file.close();

///////////////// apply Bezier extraction and write file //////////////////////////////////////

	ofstream cp_file("control_point.dat", ios::out);

    // arrays for elements points
    vector<vector<double>> elt_x;
    vector<vector<double>> elt_y;

	for(int icurv=0; icurv<curve_number; icurv++){

		// B-Spline basis
		igBasisBSpline *basis = new igBasisBSpline(degree[icurv], knot_vector[icurv]);

		// extraction
		basis->extractBezier(&pt_x[icurv], &pt_y[icurv], &elt_x, &elt_y);

		for(int i=0; i<element_number[icurv]; i++){
			for(int j=0; j<(degree[icurv]+1); j++){
				cp_file << elt_x.at(i).at(j) << " " << elt_y.at(i).at(j) << " " << 1. << endl;
			}
			cp_file << endl;
		}

		delete basis;

	}
	cp_file.close();

///////////////// sample curves and write file //////////////////////////////////////

	ofstream curve_file("curve.dat", ios::out);

    int sample_number = 100;

	for(int icurv=0; icurv<curve_number; icurv++){

		// B-Spline basis
		igBasisBSpline *basis = new igBasisBSpline(degree[icurv], knot_vector[icurv]);

        // basis function values
        vector<double> values;
        values.resize(pt_number[icurv]);

        for(int i=0; i<sample_number; i++){

            double param = double(i)/double(sample_number-1);

            basis->evalFunction(param,values);

            double coord_x = 0.;
            double coord_y = 0.;
            for(int ipt=0; ipt<pt_number[icurv]; ipt++){
                coord_x += values[ipt]*pt_x[icurv][ipt];
                coord_y += values[ipt]*pt_y[icurv][ipt];
            }

            curve_file << coord_x << " " << coord_y << endl;
        }
        curve_file << endl;

		delete basis;

	}
	curve_file.close();

////////////////// write distribution file if necessary //////////////////////

    if(case_name == "membrane"){

        double x_max = 50;
        double y_max = 50;
        double x_clamp = 0.048;
        double x_uniform = 0.2;
        double y_uniform = 0.2;

        double step_clamp;
        if(clamp_number>0){
            step_clamp = x_clamp / clamp_number / degree[0];
        }

        //----------- geometric distribution for x upstream and downstream -------------

        vector<double> distrib;
        distrib.push_back(0);
        double step = (elt_x.at(0).at(degree[0])-elt_x.at(0).at(0))*(1-coef_grid)/(1-pow(coef_grid,degree[0]));
        int counter = 1;

        // membrane clamping
        for(int i=1; i<clamp_number*degree[0]+1; i++){
            distrib.push_back(step_clamp*i);
            counter++;
        }

        // upstream/downstream
        while(distrib.at(distrib.size()-1) < x_max || (distrib.size()-1)%degree[0] != 0){
            distrib.push_back(distrib.at(counter-1) + step);
            if(distrib.at(counter)>x_uniform){
                step *= coef_grid;
            }
            counter++;
        }

        vector<double> x_distrib;
        x_distrib.resize(element_number[0]*degree[0] + 2*distrib.size() -1,0);

        // upstream part
        int offset = distrib.size()-1;
        for(int i=0; i<distrib.size(); i++){
            x_distrib.at(offset-i) = -distrib[i];
        }

        // membrane part
        for(int i=0; i<element_number[0]; i++){
			for(int j=0; j<(degree[0]); j++){ // remove last point to avoid multiple entries
				x_distrib.at(offset+degree[0]*i+j) = elt_x.at(i).at(j);
			}
        }
        x_distrib.at(offset+element_number[0]*degree[0]) = 1;

        // downstream part
        offset = element_number[0]*degree[0] + distrib.size() -1;
        for(int i=0; i<distrib.size(); i++){
            x_distrib.at(offset+i) = 1 + distrib[i];
        }

        // write distribution for direction x
        ofstream distrib_file_x("distribution_x.dat", ios::out);
        distrib_file_x << x_distrib.size() <<endl;
        for(int i=0; i<x_distrib.size(); i++){
            distrib_file_x << x_distrib.at(i) << endl;
        }
        distrib_file_x.close();


        //---------------- geometric distribution for y below and above ---------------

        distrib.clear();
        distrib.push_back(0);
        step = (elt_x.at(0).at(degree[0])-elt_x.at(0).at(0))*(1-coef_grid)/(1-pow(coef_grid,degree[0]))*ratio;
        counter = 1;
        while(distrib.at(distrib.size()-1) < y_max || (distrib.size()-1)%degree[0] != 0){
            distrib.push_back(distrib.at(counter-1) + step);
            if(distrib.at(counter)>y_uniform){
                step *= coef_grid;
            }
            counter++;
        }

        vector<double> y_distrib;
        y_distrib.resize(2*distrib.size()-1,0);

        // below part
        offset = distrib.size()-1;
        for(int i=0; i<distrib.size(); i++){
            y_distrib.at(offset-i) = -distrib[i];
        }

        // above part
        offset = distrib.size()-1;
        for(int i=1; i<distrib.size(); i++){
            y_distrib.at(offset+i) = distrib[i];
        }

        // write distribution for direction y
        ofstream distrib_file_y("distribution_y.dat", ios::out);
        distrib_file_y << y_distrib.size() <<endl;
        for(int i=0; i<y_distrib.size(); i++){
            distrib_file_y << y_distrib.at(i) << endl;
        }
        distrib_file_y.close();



        // // geometric distribution along x
        // vector<double> bloc_distrib_x;
        // bloc_distrib_x.push_back(0);
        // double step = elt_x.at(0).at(1) - elt_x.at(0).at(0);
        // int counter = 1;
        // while(bloc_distrib_x.at(bloc_distrib_x.size()-1) < x_max || (bloc_distrib_x.size()-1)%degree[0] != 0){
        //     bloc_distrib_x.push_back(bloc_distrib_x.at(counter-1) + step);
        //     step *= coef;
        //     counter++;
        // }
        //
        // vector<double> x_distrib;
        // int size_x = element_number[0]*degree[0] + 2*bloc_distrib_x.size() -1;
        // x_distrib.resize(size_x,0);
        //
        // // main membrane part
        // int offset = bloc_distrib_x.size()-1;
        // for(int i=0; i<element_number[0]; i++){
		// 	for(int j=0; j<(degree[0]); j++){ // remove last point to avoid multiple entries
		// 		x_distrib.at(offset+degree[0]*i+j) = elt_x.at(i).at(j);
		// 	}
        // }
        // x_distrib.at(offset+element_number[0]*degree[0]) = 1;
        //
        // // upstream part
        // offset = bloc_distrib_x.size()-1;
        // for(int i=0; i<bloc_distrib_x.size(); i++){
        //     x_distrib.at(offset-i) = -bloc_distrib_x.at(i);
        // }
        //
        // // downstream part
        // offset = element_number[0]*degree[0] + bloc_distrib_x.size() -1;
        // for(int i=0; i<bloc_distrib_x.size(); i++){
        //     x_distrib.at(offset+i) = 1+bloc_distrib_x.at(i);
        // }
        //
        // ofstream distrib_file_x("distribution_x.dat", ios::out);
        // distrib_file_x << x_distrib.size() <<endl;
		// for(int i=0; i<x_distrib.size(); i++){
		// 	distrib_file_x << x_distrib.at(i) << endl;
		// }
    	// distrib_file_x.close();
        //
        //
        // // geometric distribution along y
        // vector<double> bloc_distrib_y;
        // bloc_distrib_y.push_back(0);
        // step = (elt_x.at(0).at(1) - elt_x.at(0).at(0))*ratio; //smaller step along y
        // counter = 1;
        // while(bloc_distrib_y.at(bloc_distrib_y.size()-1) < y_max || (bloc_distrib_y.size()-1)%degree[0] != 0){
        //     bloc_distrib_y.push_back(bloc_distrib_y.at(counter-1) + step);
        //     step *= coef;
        //     counter++;
        // }
        //
        // ofstream distrib_file_y("distribution_y.dat", ios::out);
        // distrib_file_y << 2*bloc_distrib_y.size()-1 <<endl;
        // for(int i=0; i<bloc_distrib_y.size(); i++){
		// 	distrib_file_y << -bloc_distrib_y.at(bloc_distrib_y.size()-i-1) << endl;
		// }
        // for(int i=1; i<bloc_distrib_y.size(); i++){
		// 	distrib_file_y << bloc_distrib_y.at(i) << endl;
		// }
    	// distrib_file_y.close();


    }

//////////////////////////////////////////////////////////////////////////

	return 0;
}
