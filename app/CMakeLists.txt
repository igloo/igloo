######################################################################

add_subdirectory(igloo)
add_subdirectory(igMembrane)
add_subdirectory(igExtruder)
add_subdirectory(igFitter)
add_subdirectory(igGenCurve)
add_subdirectory(igGenMesh)
add_subdirectory(igGenMorphing)
add_subdirectory(igGenSol)
add_subdirectory(igMeshDeform)
add_subdirectory(igMeshTester)
add_subdirectory(igPreElevator)
add_subdirectory(igPreRefiner)
add_subdirectory(igPartit)
add_subdirectory(igSmoother)
add_subdirectory(igVtkConverter)

######################################################################
### CMakeLists.txt ends here
