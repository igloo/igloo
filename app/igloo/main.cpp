/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include <iostream>
#include <fstream>
#include <math.h>
#include <iomanip>
#include <sstream>
#include <algorithm>
#include <limits>
#include <functional>

#include <igCore/igMesh.h>
#include <igCore/igMeshMover.h>
#include <igCore/igMeshMoverFSIPiston.h>
#include <igCore/igMeshMoverFSISpring.h>
#include <igCore/igMeshMoverFSIMembrane.h>
#include <igCore/igMeshMoverMorphing.h>
#include <igCore/igMeshMoverSliding.h>
#include <igSimulator/igSimulator.h>
#include <igSimulator/igSimulatorALE.h>
#include <igSimulator/igSimulatorRigidALE.h>
#include <igDistributed/igCommunicator.h>
#include <igFiles/igFileManager.h>

using namespace std;

///////////////////////////////////////////////////////////////

// starting screen
void startScreen(void)
{
    cout << endl;
    cout << "                 I G L O O                   " << endl;
    cout << "     NURBS-based Discontinuous Galerkin      " << endl;
    cout << "         R. Duvigneau - INRIA                " << endl;
    cout << endl;
}

///////////////////////////////////////////////////////////////

char* getCmdOption(char ** begin, char ** end, const std::string & option)
{
    char ** itr = std::find(begin, end, option);
    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return 0;
}

bool cmdOptionExists(char** begin, char** end, const std::string& option)
{
    return std::find(begin, end, option) != end;
}


///////////////////////////////////////////////////////////////

// main program
int main(int argc, char *argv[])
{

    //----------------------------- arguments ----------------------------------

    if(argc == 1){
        cout << "use: igloo -solver[acoustic/advection/burger/euler/navier-stokes/viscous_burger] -mesh [file1] -initial [file2] -time [final_time_value]" << endl;
        return 0;
    }

    string solver_name;
    string structure_name;
    string mesh_filename;
    string solution_filename;
    string integrator_name;
    string movement_type;
    string deformation_filename;
    double initial_time = 0.;
    double end_time;
    double period_save;
    bool restart;
    double cfl_coefficient;
    double refine_coef;
    double coarsen_coef;
    double adapt_period;
    double mach_ref;
    double reynolds_ref;
    double incidence;
    double shock_capturing_coef;
    int smoothing_step_number;
    int refine_max_level;
    bool compute_error;
    bool flag_ALE;
    bool check_positivity;

    vector<double> *amr_box;
    amr_box = new vector<double>(4);
    amr_box->at(0) = -numeric_limits<double>::max();
    amr_box->at(1) = numeric_limits<double>::max();
    amr_box->at(2) = -numeric_limits<double>::max();
    amr_box->at(3) = numeric_limits<double>::max();

    vector<double> *param_ALE = new vector<double>;

    if (cmdOptionExists(argv, argv + argc, "-solver"))
        solver_name = getCmdOption(argv, argv + argc, "-solver");
    else{
        cout << "solver type not provided !" << endl;
        return 0;
    }

    if (cmdOptionExists(argv, argv + argc, "-mesh"))
        mesh_filename = getCmdOption(argv, argv + argc, "-mesh");
    else{
        cout << "mesh file not provided !"<< endl;
        return 0;
    }

    if (cmdOptionExists(argv, argv + argc, "-initial"))
        solution_filename = getCmdOption(argv, argv + argc, "-initial");
    else{
        cout << "initial solution file not provided !"<< endl;
        return 0;
    }

    if (cmdOptionExists(argv, argv + argc, "-time"))
        end_time  = atof(getCmdOption(argv, argv + argc, "-time"));
    else{
        cout << "end time value not provided !"<< endl;
        return 0;
    }

    compute_error = false;
    if (cmdOptionExists(argv, argv + argc, "-error"))
        compute_error = true;

    if (cmdOptionExists(argv, argv + argc, "-save_period")){
        period_save  = atof(getCmdOption(argv, argv + argc, "-save_period"));
    } else {
        period_save = end_time;
    }

    if (cmdOptionExists(argv, argv + argc, "-integrator")){
        integrator_name  = getCmdOption(argv, argv + argc, "-integrator");
    } else {
        integrator_name = "ssp";
    }

    if (cmdOptionExists(argv, argv + argc, "-cfl")){
        cfl_coefficient  = atof(getCmdOption(argv, argv + argc, "-cfl"));
    } else {
        cfl_coefficient = 0.9;
    }

    if (cmdOptionExists(argv, argv + argc, "-refine_max")){
        refine_max_level  = atoi(getCmdOption(argv, argv + argc, "-refine_max"));
    } else {
        refine_max_level = 0;
    }

    if (cmdOptionExists(argv, argv + argc, "-refine_coef")){
        refine_coef  = atof(getCmdOption(argv, argv + argc, "-refine_coef"));
    } else {
        refine_coef = 2.;
    }

    if (cmdOptionExists(argv, argv + argc, "-coarsen_coef")){
        coarsen_coef  = atof(getCmdOption(argv, argv + argc, "-coarsen_coef"));
    } else {
        coarsen_coef = 0.5;
    }

    if (cmdOptionExists(argv, argv + argc, "-adapt_period")){
        adapt_period  = atof(getCmdOption(argv, argv + argc, "-adapt_period"));
    } else {
        adapt_period = 1.;
    }

    if (cmdOptionExists(argv, argv + argc, "-adapt_xmin")){
        amr_box->at(0)  = atof(getCmdOption(argv, argv + argc, "-adapt_xmin"));
    }

    if (cmdOptionExists(argv, argv + argc, "-adapt_xmax")){
        amr_box->at(1)  = atof(getCmdOption(argv, argv + argc, "-adapt_xmax"));
    }

    if (cmdOptionExists(argv, argv + argc, "-adapt_ymin")){
        amr_box->at(2)  = atof(getCmdOption(argv, argv + argc, "-adapt_ymin"));
    }

    if (cmdOptionExists(argv, argv + argc, "-adapt_ymax")){
        amr_box->at(3)  = atof(getCmdOption(argv, argv + argc, "-adapt_ymax"));
    }

    if (cmdOptionExists(argv, argv + argc, "-mach")){
        mach_ref  = atof(getCmdOption(argv, argv + argc, "-mach"));
    } else {
        mach_ref = 0.5;
    }

    if (cmdOptionExists(argv, argv + argc, "-reynolds")){
        reynolds_ref  = atof(getCmdOption(argv, argv + argc, "-reynolds"));
    } else {
        reynolds_ref = 70.;
    }

    if (cmdOptionExists(argv, argv + argc, "-incidence")){
        incidence  = atof(getCmdOption(argv, argv + argc, "-incidence"));
    } else {
        incidence = 0.;
    }

    if (cmdOptionExists(argv, argv + argc, "-shock")){
        shock_capturing_coef  = atof(getCmdOption(argv, argv + argc, "-shock"));
    } else {
        shock_capturing_coef = 0.;
    }

    if (cmdOptionExists(argv, argv + argc, "-viscosity_smoothing")){
        smoothing_step_number = atoi(getCmdOption(argv, argv + argc, "-viscosity_smoothing"));
    } else {
        smoothing_step_number = 0;
    }

    flag_ALE = false;
    if (cmdOptionExists(argv, argv + argc, "-ale")){
        flag_ALE  = true;
        movement_type = getCmdOption(argv, argv + argc, "-ale");
    }

    if (cmdOptionExists(argv, argv + argc, "-ale_param")){
    	param_ALE->push_back(atof(getCmdOption(argv, argv + argc, "-ale_param")));
    }

    if (cmdOptionExists(argv, argv + argc, "-structure")){
        structure_name = getCmdOption(argv, argv + argc, "-structure");
        flag_ALE  = true;
        movement_type = "fsi";
    } else {
        structure_name = "no_structure";
    }

    if (cmdOptionExists(argv, argv + argc, "-deformation"))
    	deformation_filename = getCmdOption(argv, argv + argc, "-deformation");


    bool flag_vorticity = false;
    if (cmdOptionExists(argv, argv + argc, "-vorticity"))
    {
        flag_vorticity  = true;
    }

    restart = false;
    if (cmdOptionExists(argv, argv + argc, "-restart"))
        restart = true;

    check_positivity = false;
    if (cmdOptionExists(argv, argv + argc, "-positivity"))
        check_positivity = true;


    //---------------- default solver dependent parameters -----------------

    vector<double> *solver_parameters = new vector<double>;
    if(solver_name == "viscous_burger"){
        solver_parameters->assign(3,0.);
        solver_parameters->at(0) = 2.;
        solver_parameters->at(1) = 0.1;
        solver_parameters->at(2) = 0.;
    }
    if((solver_name == "euler")||(solver_name == "navier-stokes")){
        solver_parameters->assign(3,0.);
        solver_parameters->at(0) = mach_ref;
        solver_parameters->at(1) = reynolds_ref;
        solver_parameters->at(2) = incidence;
    }

    //---------------------- mesh, solution & communicator ----------------------------

    igCommunicator *communicator = new igCommunicator();
    communicator->initialize();

    if(communicator->rank() == 0)
        startScreen();

    vector<double> *state = new vector<double>;
    vector<double> *coordinates = new vector<double>;
    vector<double> *velocities = new vector<double>;
    vector<double> *deformation = new vector<double>;

    igMesh *mesh = new igMesh;
    mesh->setCommunicator(communicator);
    mesh->setCoordinates(coordinates);
    mesh->setVelocities(velocities);
    mesh->build(mesh_filename);
    mesh->setTime(initial_time);

    igFileManager file_manager;
    file_manager.setPartitionNumber(communicator->size());
    file_manager.setMyPartition(communicator->rank());
    file_manager.readSolutionIgloo(solution_filename, mesh->variableNumber(), mesh, state);

    //------------------------------- mesh mover ---------------------------------------

    igMeshMover *mover;

    if(movement_type == "sliding" || movement_type == "sliding_pitch" || movement_type == "turbine"){
    	mover = new igMeshMoverSliding(mesh, coordinates, velocities);
    }
    else if(movement_type == "morphing"){
    	mover = new igMeshMoverMorphing(mesh, coordinates, velocities);
        file_manager.readSolutionIgloo(deformation_filename, mesh->meshDimension(), mesh, deformation);
    	mover->setDeformation(deformation);
    }
    else if(movement_type == "fsi"){
        if(structure_name == "piston"){
            mover = new igMeshMoverFSIPiston(mesh, coordinates, velocities);
        } else if(structure_name == "spring"){
            mover = new igMeshMoverFSISpring(mesh, coordinates, velocities);
        } else if(structure_name == "membrane"){
            mover = new igMeshMoverFSIMembrane(mesh, coordinates, velocities);
        } else {
            cout << "Unknown structural solver" << endl;
        }
    }
    else{
        mover = new igMeshMover(mesh, coordinates, velocities);
    }
    mover->setCommunicator(communicator);
    mover->initializeMovement(integrator_name, movement_type, param_ALE, refine_max_level);

    //---------------------------- Simulator -------------------------------------------

    igSimulator *simulator;

    if(flag_ALE){
    	if(mover->rigidMotion()){
    		simulator = new igSimulatorRigidALE;
    	}
    	else{
    		simulator = new igSimulatorALE;
    	}
    }
    else{
    	simulator = new igSimulator;
    }

    simulator->setCommunicator(communicator);
    simulator->setMesh(mesh);
    simulator->setMeshMover(mover);
    simulator->setInitialSolution(state);
    simulator->setCoordinates(coordinates);
    simulator->setVelocities(velocities);
    simulator->setInitialTime(initial_time);
    simulator->setSolver(solver_name);
    simulator->setStructure(structure_name);
    simulator->setIntegrator(integrator_name);
    simulator->setFinalTime(end_time);
    simulator->setSavePeriod(period_save);
    simulator->setCflCoefficient(cfl_coefficient);
    simulator->setSolverParameters(solver_parameters);
    simulator->setRefineCoefficient(refine_coef);
    simulator->setRefineMaxLevel(refine_max_level);
    simulator->setCoarsenCoefficient(coarsen_coef);
    simulator->setAdaptBox(amr_box);
    simulator->setAdaptPeriod(adapt_period);
    simulator->enableErrorComputation(compute_error);
    simulator->enableVorticity(flag_vorticity);
    simulator->setShockCapturing(shock_capturing_coef);
    simulator->setSmoothingViscosityNumber(smoothing_step_number);
    simulator->enableRestart(restart);
    simulator->enableCheckPositivity(check_positivity);

    simulator->initialize();

    simulator->run();

    simulator->finalize();

    //-----------------------------------------------------------------------------------

    delete param_ALE;
    delete amr_box;
    delete solver_parameters;
    delete deformation;
    delete coordinates;
    delete velocities;
    delete state;
    delete mesh;
    delete mover;

    communicator->finalize();
    delete communicator;



    return 0;

}

///////////////////////////////////////////////////////////////
