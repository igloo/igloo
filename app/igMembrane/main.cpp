/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include <iostream>
#include <fstream>
#include <math.h>
#include <iomanip>
#include <sstream>
#include <algorithm>

#include <igCore/igBasisBSpline.h>
#include <igCore/igMesh.h>
#include <igCore/igMeshMoverFSIMembrane.h>
#include <igDistributed/igCommunicator.h>
#include <igSolver/igStructuralSolver.h>
#include <igSolver/igStructuralSolverMembrane.h>
#include <igSolver/igCouplingInterfaceMembrane.h>
#include <igFiles/igFileManager.h>


using namespace std;

///////////////////////////////////////////////////////////////

char* getCmdOption(char ** begin, char ** end, const std::string & option)
{
	char ** itr = std::find(begin, end, option);
	if (itr != end && ++itr != end)
	{
		return *itr;
	}
	return 0;
}

bool cmdOptionExists(char** begin, char** end, const std::string& option)
{
	return std::find(begin, end, option) != end;
}

/////////////////////////////////////////////////////////

// main program
int main(int argc, char **argv)
{
    double time = 0;
    double time_step = 0.01;

////////////////// arguments ///////////////////////////////////////

    double time_max = time_step;
    if (cmdOptionExists(argv, argv + argc, "-time")){
        time_max = atof(getCmdOption(argv, argv + argc, "-time"));
    }

    string mesh_filename;
    if (cmdOptionExists(argv, argv + argc, "-mesh"))
        mesh_filename = getCmdOption(argv, argv + argc, "-mesh");
    else{
        cout << "mesh file not provided !"<< endl;
        return 0;
    }

/////////////// manage coupling interface //////////////////////


///////////////////// initialization ///////////////////////////////////////

    igFileManager file_manager;
    file_manager.setSavePeriod(time_max/10);

    igCommunicator *communicator = new igCommunicator();
    communicator->initialize();

    // mesh
    vector<double> *coordinates = new vector<double>;
    vector<double> *velocities = new vector<double>;
    igMesh *mesh = new igMesh;
    mesh->setCommunicator(communicator);
    mesh->setCoordinates(coordinates);
    mesh->setVelocities(velocities);
    mesh->build(mesh_filename);

    // interface
    igCouplingInterface *interface = new igCouplingInterfaceMembrane();
    file_manager.readNurbsCurve(interface->knotVector(), interface->controlPointsX(), interface->controlPointsY());
    interface->initializeStructureInterface();
    int fluid_displacement_size = (interface->interfaceDegree()+1)*interface->elementNumber()*2;
    int fluid_effort_size = interface->gaussPointNumber()*interface->elementNumber()*2;
    interface->initializeQuadrature(mesh);
    interface->initializeFluidInterface(fluid_effort_size,fluid_displacement_size,2);


    // mesh mover
    igMeshMover *mover = new igMeshMoverFSIMembrane(mesh, coordinates, velocities);
    mover->setCommunicator(communicator);
    string integrator_name = "none";
    string movement_type = "fsi";
    vector<double> *param_ALE = new vector<double>;
    int refine_max_level = 0;
    mover->initializeMovement(integrator_name, movement_type, param_ALE, refine_max_level);
    mover->initializeMapping(interface->knotVector(), interface->controlPointsX(), interface->controlPointsY());
    mover->setInterfaceFSI(interface->interfaceDisplacementsFluid(), interface->interfaceVelocitiesFluid());
    mover->initializeFractionalStep();
    mover->setFractionalStep(1);
    mover->initializeVelocity(time);

    // structural solver
    igStructuralSolver *structural_solver = new igStructuralSolverMembrane(interface->interfaceGaussPoints(), interface->interfaceGaussWeights(), interface->interfaceEffortsStructure(), interface->interfaceDisplacementsStructure(), interface->interfaceVelocitiesStructure());
    structural_solver->setCommunicator(communicator);
    structural_solver->initialize(interface->interfaceDegree(), interface->controlPointNumber(), interface->knotVector(), interface->controlPointsX(), interface->controlPointsY(), interface->gaussPointNumber());
    structural_solver->setTimeStep(time_step);

    // define external fluid efforts
    int size_eff = interface->interfaceEffortsFluid()->size();
    for (int ipt=0; ipt<size_eff; ipt++){
        interface->interfaceEffortsFluid()->at(ipt) = -0.001;
    }
    interface->convertFluidEffortsToStructure();


/////////////////// time loop /////////////////////////////

    int time_counter = 0;

    while(time < time_max){

        //------ compute structural model update

        structural_solver->compute();

        //------ split displacement: NURBS curve -> Bezier edges

		// extraction of displacement
        interface->convertStructureDisplacementsToFluid();

        // extraction of velocity
        interface->convertStructureVelocitiesToFluid();

        //------- mesh update
        mover->move(time);

        //------ plots
        file_manager.plotBSplineCurve(interface->interfaceDegree(), interface->controlPointNumber(), interface->knotVector(), interface->controlPointsX(), interface->interfaceDisplacementsStructure(), time_counter);
        file_manager.plotControlNet(interface->controlPointNumber(), interface->controlPointsX(), interface->interfaceDisplacementsStructure(), time_counter);
        file_manager.writeMesh(mesh, time);

        time += time_step;
        time_counter++;

    }

////////////////////////////////////////////////////////////

    cout << endl;
    cout << "deformation: " ;
    int size_displ = interface->interfaceDisplacementsStructure()->size();
    for(int i=0; i<size_displ; i++){
        cout << interface->interfaceDisplacementsStructure()->at(i) << " ";
    }
    cout << endl;


    communicator->finalize();

    delete structural_solver;

    delete coordinates;
    delete velocities;

    delete mesh;



	return 0;
}
