/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include <iostream>
#include <fstream>
#include <math.h>
#include <iomanip>
#include <sstream>
#include <algorithm>

#include <igFiles/igFileManager.h>
#include <igDistributed/igCommunicator.h>
#include <igCore/igMesh.h>
#include <igCore/igFace.h>
#include <igCore/igElement.h>
#include <igCore/igDistributor.h>

using namespace std;

///////////////////////////////////////////////////////////////

char* getCmdOption(char ** begin, char ** end, const std::string & option)
{
    char ** itr = std::find(begin, end, option);
    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return 0;
}

bool cmdOptionExists(char** begin, char** end, const std::string& option)
{
    return std::find(begin, end, option) != end;
}

/////////////////////////////////////////////////////////

double distance(vector<double> *coordinates, igElement *elt_start, int start, igElement *elt_end, int end)
{
    double x0 = (*coordinates)[elt_start->globalId(start,0)];
    double y0 = (*coordinates)[elt_start->globalId(start,1)];
    double x1 = (*coordinates)[elt_end->globalId(end,0)];
    double y1 = (*coordinates)[elt_end->globalId(end,1)];

    return sqrt( (x1-x0)*(x1-x0) + (y1-y0)*(y1-y0) );
}


/////////////////////////////////////////////////////////

// main program
int main(int argc, char **argv)
{
    //--------------- arguments management -----------------

    string mesh_filename;
    int step_number;

    if (cmdOptionExists(argv, argv + argc, "-mesh"))
        mesh_filename = getCmdOption(argv, argv + argc, "-mesh");
    else{
        cout << "mesh file not provided !"<< endl;
        return 0;
    }

    if (cmdOptionExists(argv, argv + argc, "-step"))
        step_number = atoi(getCmdOption(argv, argv + argc, "-step"));
    else{
        cout << "step not provided !"<< endl;
        return 0;
    }

    bool linearize = false;
    if (cmdOptionExists(argv, argv + argc, "-linearize")){
        linearize = true;
    }

    bool weighted = false;
    if (cmdOptionExists(argv, argv + argc, "-weighted")){
        weighted = true;
    }

    bool sliding = false;
    if (cmdOptionExists(argv, argv + argc, "-sliding")){
    	sliding = true;
    }

    bool subdomain_mode = false;
    int subdomain_id = 0;
	if (cmdOptionExists(argv, argv + argc, "-subdomain")){
		subdomain_id = atoi(getCmdOption(argv, argv + argc, "-subdomain"));
		subdomain_mode = true;
	}


    //--------------- initialization -----------------

    igCommunicator *communicator = new igCommunicator();
    communicator->initialize();

    igFileManager file_manager;

    vector<double> *coordinates = new vector<double>;
    vector<double> *velocities = new vector<double>;
    vector<double> *smoothed_coordinates = new vector<double>;

    igMesh *mesh = new igMesh;
    mesh->setCommunicator(communicator);
    mesh->setCoordinates(coordinates);
    mesh->setVelocities(velocities);
    mesh->build(mesh_filename);

    //-------------- init -------------------------

    double dumping = 0.5;
    double dist_eps = 1.e-13;

    smoothed_coordinates->resize(coordinates->size());

    for (int i=0; i<coordinates->size(); i++){
        (*smoothed_coordinates)[i] = (*coordinates)[i];
    }

    //------------- smoothing process -------------

    for(int istep=0; istep<step_number; istep++){

        cout << "step " << istep << endl;

        // loop over elements
        for (int iel=0; iel<mesh->elementNumber(); iel++){

            igElement *elt = mesh->element(iel);
            int degree = elt->cellDegree();

            if(!subdomain_mode || elt->subdomain() == subdomain_id){

                for(int icomp=0; icomp<mesh->meshDimension(); icomp++){

                    // treatment of internal DOFs
                    for(int i=1; i<degree; i++){
                        for(int j=1; j<degree; j++){

                            int index_C = elt->globalId(j*(degree+1)+i,icomp);
                            int index_S = elt->globalId((j-1)*(degree+1)+i,icomp);
                            int index_N = elt->globalId((j+1)*(degree+1)+i,icomp);
                            int index_W = elt->globalId(j*(degree+1)+i-1,icomp);
                            int index_E = elt->globalId(j*(degree+1)+i+1,icomp);

                            double coef_S = 1./distance(coordinates,elt,(j-1)*(degree+1)+i,elt,j*(degree+1)+i);
                            double coef_N = 1./distance(coordinates,elt,(j+1)*(degree+1)+i,elt,j*(degree+1)+i);
                            double coef_W = 1./distance(coordinates,elt,j*(degree+1)+i-1,elt,j*(degree+1)+i);
                            double coef_E = 1./distance(coordinates,elt,j*(degree+1)+i+1,elt,j*(degree+1)+i);
                            double coef_sum = coef_S + coef_N + coef_E + coef_W;

                            if(weighted){
                                (*smoothed_coordinates)[index_C] = (1.-dumping) * (*coordinates)[index_C]
                                    + dumping*( coef_S*(*coordinates)[index_S] + coef_N*(*coordinates)[index_N]
                                    +           coef_W*(*coordinates)[index_W] + coef_E*(*coordinates)[index_E] )/ (coef_sum);
                            } else {
                                (*smoothed_coordinates)[index_C] = (1.-dumping) * (*coordinates)[index_C]
                                    + dumping*( (*coordinates)[index_S] + (*coordinates)[index_N]
                                    +           (*coordinates)[index_W] + (*coordinates)[index_E] )/ (4.);
                            }

                        }
                    }


                    // treatment of the four faces
                    for(int ifac=0; ifac<4; ifac++){

                        if(elt->faceId(ifac) >= 0){
                            igFace *face = mesh->face(elt->faceId(ifac));

                            int index_L = face->leftElementIndex();
                            int index_R = face->rightElementIndex();
                            int index_elt_out;
                            bool elt_out_is_L;
                            if(index_L == iel){
                                index_elt_out = index_R;
                                elt_out_is_L = false;
                            } else {
                                index_elt_out = index_L;
                                elt_out_is_L = true;
                            }

                            igElement *elt_out = mesh->element(index_elt_out);

                            // check configuration of element out
                            int orientation;
                            int sense;
                            if(elt_out_is_L){
                                orientation = face->leftOrientation();
                                sense = face->leftSense();
                            } else {
                                orientation = face->rightOrientation();
                                sense = face->rightSense();
                            }

                            int index_dof_C;
                            int index_dof_in;
                            int index_dof_L;
                            int index_dof_R;
                            int index_dof_out;
                            int index_local_C;

                            double coef_in;
                            double coef_out;
                            double coef_L;
                            double coef_R;

                            // loop over face interior DOFs
                            for(int i=1; i<degree; i++){

                                if(ifac ==0){
                                    index_dof_C  = elt->globalId(0*(degree+1)+i,icomp);
                                    index_dof_in = elt->globalId(1*(degree+1)+i,icomp);
                                    index_dof_L  = elt->globalId(0*(degree+1)+i-1,icomp);
                                    index_dof_R  = elt->globalId(0*(degree+1)+i+1,icomp);
                                    index_local_C = 0*(degree+1)+i;
                                    coef_in = 1./distance(coordinates,elt,1*(degree+1)+i,elt,index_local_C);
                                    coef_L = 1./distance(coordinates,elt,0*(degree+1)+i-1,elt,index_local_C);
                                    coef_R = 1./distance(coordinates,elt,0*(degree+1)+i+1,elt,index_local_C);
                                } else if(ifac ==1){
                                    index_dof_C  = elt->globalId(i*(degree+1)+degree,icomp);
                                    index_dof_in = elt->globalId(i*(degree+1)+degree-1,icomp);
                                    index_dof_L  = elt->globalId((i-1)*(degree+1)+degree,icomp);
                                    index_dof_R  = elt->globalId((i+1)*(degree+1)+degree,icomp);
                                    index_local_C = i*(degree+1)+degree;
                                    coef_in = 1./distance(coordinates,elt,i*(degree+1)+degree-1,elt,index_local_C);
                                    coef_L = 1./distance(coordinates,elt,(i-1)*(degree+1)+degree,elt,index_local_C);
                                    coef_R = 1./distance(coordinates,elt,(i+1)*(degree+1)+degree,elt,index_local_C);
                                } else if(ifac == 2){
                                    index_dof_C  = elt->globalId(degree*(degree+1)+i,icomp);
                                    index_dof_in = elt->globalId((degree-1)*(degree+1)+i,icomp);
                                    index_dof_L  = elt->globalId(degree*(degree+1)+i-1,icomp);
                                    index_dof_R  = elt->globalId(degree*(degree+1)+i+1,icomp);
                                    index_local_C = degree*(degree+1)+i;
                                    coef_in = 1./distance(coordinates,elt,(degree-1)*(degree+1)+i,elt,index_local_C);
                                    coef_L = 1./distance(coordinates,elt,degree*(degree+1)+i-1,elt,index_local_C);
                                    coef_R = 1./distance(coordinates,elt,degree*(degree+1)+i+1,elt,index_local_C);
                                } else {
                                    index_dof_C  = elt->globalId(i*(degree+1),icomp);
                                    index_dof_in = elt->globalId(i*(degree+1)+1,icomp);
                                    index_dof_L  = elt->globalId((i-1)*(degree+1),icomp);
                                    index_dof_R  = elt->globalId((i+1)*(degree+1),icomp);
                                    index_local_C = i*(degree+1);
                                    coef_in = 1./distance(coordinates,elt,i*(degree+1)+1,elt,index_local_C);
                                    coef_L = 1./distance(coordinates,elt,(i-1)*(degree+1),elt,index_local_C);
                                    coef_R = 1./distance(coordinates,elt,(i+1)*(degree+1),elt,index_local_C);
                                }

                                if(orientation == 0){
                                    if(sense>0)
                                        index_dof_out = elt_out->globalId(1*(degree+1)+i,icomp);
                                    else
                                        index_dof_out = elt_out->globalId(2*(degree+1)-1-i,icomp);
                                    coef_out = 1./distance(coordinates,elt_out,1*(degree+1)+i,elt,index_local_C);
                                } else if(orientation == 1){
                                    if(sense>0)
                                        index_dof_out = elt_out->globalId(i*(degree+1)+degree-1,icomp);
                                    else
                                        index_dof_out = elt_out->globalId((degree-i)*(degree+1)+degree-1,icomp);
                                    coef_out = 1./distance(coordinates,elt_out,i*(degree+1)+degree-1,elt,index_local_C);
                                } else if(orientation == 2){
                                    if(sense>0)
                                        index_dof_out = elt_out->globalId((degree-1)*(degree+1)+i,icomp);
                                    else
                                        index_dof_out = elt_out->globalId(degree*(degree+1)-1-i,icomp);
                                    coef_out = 1./distance(coordinates,elt_out,(degree-1)*(degree+1)+i,elt,index_local_C);
                                } else {
                                    if(sense>0)
                                        index_dof_out = elt_out->globalId(i*(degree+1)+1,icomp);
                                    else
                                        index_dof_out = elt_out->globalId((degree-i)*(degree+1)+1,icomp);
                                    coef_out = 1./distance(coordinates,elt_out,i*(degree+1)+1,elt,index_local_C);
                                }

                                double coef_sum = coef_in + coef_out + coef_L + coef_R;

                                if(weighted){
                                    (*smoothed_coordinates)[index_dof_C] = (1.-dumping) * (*coordinates)[index_dof_C]
                                        + dumping*( coef_in*(*coordinates)[index_dof_in] + coef_out*(*coordinates)[index_dof_out]
                                                  + coef_L*(*coordinates)[index_dof_L] + coef_R*(*coordinates)[index_dof_R] )/(coef_sum);
                                } else {
                                    (*smoothed_coordinates)[index_dof_C] = (1.-dumping) * (*coordinates)[index_dof_C]
                                        + dumping*( (*coordinates)[index_dof_in] + (*coordinates)[index_dof_out]
                                                  + (*coordinates)[index_dof_L] + (*coordinates)[index_dof_R] )/(4.);
                                }

                            }

                        }


                    } // loop faces

                    // treatment of the four corners
                    for(int icor=0; icor<4; icor++){

                        int face_id1 = icor;
                        int face_id2 = icor+1;
                        if(face_id2 == 4) face_id2 = 0;

                        if( (elt->faceId(face_id1) >= 0) && (elt->faceId(face_id2) >= 0) ){

                            igFace *face1 = mesh->face(elt->faceId(face_id1));
                            igFace *face2 = mesh->face(elt->faceId(face_id2));

                            int index_L1 = face1->leftElementIndex();
                            int index_R1 = face1->rightElementIndex();
                            int index_elt_out1;
                            bool elt_out_is_L1;
                            if(index_L1 == iel){
                                index_elt_out1 = index_R1;
                                elt_out_is_L1 = false;
                            } else {
                                index_elt_out1 = index_L1;
                                elt_out_is_L1 = true;
                            }
                            int index_L2 = face2->leftElementIndex();
                            int index_R2 = face2->rightElementIndex();
                            int index_elt_out2;
                            bool elt_out_is_L2;
                            if(index_L2 == iel){
                                index_elt_out2 = index_R2;
                                elt_out_is_L2 = false;
                            } else {
                                index_elt_out2 = index_L2;
                                elt_out_is_L2 = true;
                            }

                            igElement *elt_out1 = mesh->element(index_elt_out1);
                            igElement *elt_out2 = mesh->element(index_elt_out2);

                            // check configuration of element out
                            int orientation1;
                            if(elt_out_is_L1)
                                orientation1 = face1->leftOrientation();
                            else
                                orientation1 = face1->rightOrientation();
                            int orientation2;
                            if(elt_out_is_L2)
                                orientation2 = face2->leftOrientation();
                            else
                                orientation2 = face2->rightOrientation();

                            int index_dof_C;
                            int index_dof_in1;
                            int index_dof_in2;
                            int index_dof_out1;
                            int index_dof_out2;

                            int index_local_C;

                            double coef_in1;
                            double coef_in2;
                            double coef_out1;
                            double coef_out2;

                            if(icor ==0){
                                index_dof_C   = elt->globalId(degree,icomp);
                                index_dof_in1 = elt->globalId(degree-1,icomp);
                                index_dof_in2 = elt->globalId((degree+1)+degree,icomp);
                                index_local_C = degree;
                                coef_in1 = 1./distance(coordinates,elt,degree-1,elt,index_local_C);
                                coef_in2 = 1./distance(coordinates,elt,(degree+1)+degree,elt,index_local_C);
                            } else if(icor ==1){
                                index_dof_C   = elt->globalId(degree*(degree+1)+degree,icomp);
                                index_dof_in1 = elt->globalId((degree-1)*(degree+1)+degree,icomp);
                                index_dof_in2 = elt->globalId(degree*(degree+1)+degree-1,icomp);
                                index_local_C = degree*(degree+1)+degree;
                                coef_in1 = 1./distance(coordinates,elt,(degree-1)*(degree+1)+degree,elt,index_local_C);
                                coef_in2 = 1./distance(coordinates,elt,degree*(degree+1)+degree-1,elt,index_local_C);
                            } else if(icor == 2){
                                index_dof_C   = elt->globalId(degree*(degree+1),icomp);
                                index_dof_in1 = elt->globalId(degree*(degree+1)+1,icomp);
                                index_dof_in2 = elt->globalId((degree-1)*(degree+1),icomp);
                                index_local_C = degree*(degree+1);
                                coef_in1 = 1./distance(coordinates,elt,degree*(degree+1)+1,elt,index_local_C);
                                coef_in2 = 1./distance(coordinates,elt,(degree-1)*(degree+1),elt,index_local_C);
                            } else {
                                index_dof_C   = elt->globalId(0,icomp);
                                index_dof_in1 = elt->globalId(degree+1,icomp);
                                index_dof_in2 = elt->globalId(1,icomp);
                                index_local_C = 0;
                                coef_in1 = 1./distance(coordinates,elt,degree+1,elt,index_local_C);
                                coef_in2 = 1./distance(coordinates,elt,1,elt,index_local_C);
                            }

                            if(orientation1 == 0){
                                index_dof_out1 = elt_out1->globalId((degree+1),icomp);
                                coef_out1 = 1./distance(coordinates,elt_out1,(degree+1),elt,index_local_C);
                            } else if(orientation1 == 1){
                                index_dof_out1 = elt_out1->globalId(degree-1,icomp);
                                coef_out1 = 1./distance(coordinates,elt_out1,degree-1,elt,index_local_C);
                            } else if(orientation1 == 2){
                                index_dof_out1 = elt_out1->globalId((degree-1)*(degree+1)+degree,icomp);
                                coef_out1 = 1./distance(coordinates,elt_out1,(degree-1)*(degree+1)+degree,elt,index_local_C);
                            } else {
                                index_dof_out1 = elt_out1->globalId(degree*(degree+1)+1,icomp);
                                coef_out1 = 1./distance(coordinates,elt_out1,degree*(degree+1)+1,elt,index_local_C);
                            }

                            if(orientation2 == 0){
                                index_dof_out2 = elt_out2->globalId((degree+1)+degree,icomp);
                                coef_out2 = 1./distance(coordinates,elt_out2,(degree+1)+degree,elt,index_local_C);
                            } else if(orientation2 == 1){
                                index_dof_out2 = elt_out2->globalId(degree*(degree+1)+degree-1,icomp);
                                coef_out2 = 1./distance(coordinates,elt_out2,degree*(degree+1)+degree-1,elt,index_local_C);
                            } else if(orientation2 == 2){
                                index_dof_out2 = elt_out2->globalId((degree-1)*(degree+1),icomp);
                                coef_out2 = 1./distance(coordinates,elt_out2,(degree-1)*(degree+1),elt,index_local_C);
                            } else {
                                index_dof_out2 = elt_out2->globalId(1,icomp);
                                coef_out2 = 1./distance(coordinates,elt_out2,1,elt,index_local_C);
                            }

                            double coef_sum = coef_in1 + coef_in2 + coef_out1 + coef_out2;

                            if(weighted){
                                (*smoothed_coordinates)[index_dof_C] = (1.-dumping) * (*coordinates)[index_dof_C]
                                    + dumping*( coef_in1*(*coordinates)[index_dof_in1] + coef_in2*(*coordinates)[index_dof_in2]
                                              + coef_out1*(*coordinates)[index_dof_out1] + coef_out2*(*coordinates)[index_dof_out2] )/(coef_sum);
                            } else {
                                (*smoothed_coordinates)[index_dof_C] = (1.-dumping) * (*coordinates)[index_dof_C]
                                    + dumping*( (*coordinates)[index_dof_in1] + (*coordinates)[index_dof_in2]
                                              + (*coordinates)[index_dof_out1] +(*coordinates)[index_dof_out2] )/(4.);
                            }

                        }


                    } // loop corners

                } // loop components

            } // test subdomain

        } // loop elements


        // enforce null change on boundaries
        for (int ifac=0; ifac<mesh->boundaryFaceNumber(); ifac++){

            igFace *face = mesh->boundaryFace(ifac);

            for(int icomp=0; icomp<mesh->meshDimension(); icomp++){

                for(int i=0; i<face->controlPointNumber(); i++){
                    int index = face->dofLeft(i,icomp);
                    (*smoothed_coordinates)[index] = (*coordinates)[index];
                }
            }

        }

        // enforce null change on sliding interface
        if(sliding){

        	for (int ifac=0; ifac<mesh->faceNumber(); ifac++){

        		igFace *face = mesh->face(ifac);

        		int left_subdomain = mesh->element(face->leftElementIndex())->subdomain();
        		int right_subdomain = mesh->element(face->rightElementIndex())->subdomain();

        		if(right_subdomain != left_subdomain){

        			for(int icomp=0; icomp<mesh->meshDimension(); icomp++){

        				for(int i=0; i<face->controlPointNumber(); i++){
        					int index = face->dofLeft(i,icomp);
        					(*smoothed_coordinates)[index] = (*coordinates)[index];
        				}
        			}

        		}

        	}

        }


        // enforce continuity
        bool continuous;
        do {
            continuous = true;
            for (int ifac=0; ifac<mesh->faceNumber(); ifac++){

                igFace *face = mesh->face(ifac);
                int degree = face->cellDegree();

                for(int icomp=0; icomp<mesh->meshDimension(); icomp++){

                    for(int i=0; i<face->controlPointNumber(); i++){

                        int index_L = face->dofLeft(i,icomp);
                        int index_R = face->dofRight(i,icomp);

                        // check continuity
                        if( fabs( (*smoothed_coordinates)[index_L] - (*smoothed_coordinates)[index_R] ) > dist_eps ){

                            double delta_L = fabs((*smoothed_coordinates)[index_L] - (*coordinates)[index_L]);
                            double delta_R = fabs((*smoothed_coordinates)[index_R] - (*coordinates)[index_R]);

                            if(delta_L < delta_R){
                                (*smoothed_coordinates)[index_R] = (*smoothed_coordinates)[index_L];
                            } else {
                                (*smoothed_coordinates)[index_L] = (*smoothed_coordinates)[index_R];
                            }

                            continuous = false;
                        }
                    }
                }

            } // loop faces
        } while (!continuous);


        // update coordinates
        for (int i=0; i<coordinates->size(); i++){
            (*coordinates)[i] = (*smoothed_coordinates)[i];
        }

        //Linearize elements
        if(linearize){
            for (int iel=0; iel<mesh->elementNumber(); iel++){

                igElement *elt = mesh->element(iel);

                // linearize faces except boundaries
                if(elt->faceId(0) > 0)
                    mesh->linearizeBoundary(elt,0,0);
                if(elt->faceId(2) > 0)
                    mesh->linearizeBoundary(elt,elt->controlPointNumber() - elt->controlPointNumberByDirection(),0);
                if(elt->faceId(3) > 0)
                    mesh->linearizeBoundary(elt,0,1);
                if(elt->faceId(1) > 0)
                    mesh->linearizeBoundary(elt,elt->controlPointNumberByDirection()-1,1);

                // linearization of interior points (Coons formula)
                mesh->coonsMethod(elt);

                elt->initializeGeometry();
            }
        }

    } // loop steps

    //--------------- GLVis output -----------------
    file_manager.writeMesh(mesh);


    //--------------- partitionning ----------------------------

    igDistributor *distributor = new igDistributor;
    distributor->setMesh(mesh);
    distributor->setPartitionNumber(1);
    distributor->run();

    //--------------- mesh and solution output -----------------

    file_manager.writeMeshIgloo("mesh_smoothed.dat", mesh, distributor);

    //--------------------------------------

    delete distributor;
    delete mesh;

    communicator->finalize();
    delete communicator;

    return 0;

}
