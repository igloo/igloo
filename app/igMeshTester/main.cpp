/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include <iostream>
#include <fstream>
#include <math.h>
#include <iomanip>
#include <sstream>
#include <algorithm>

#include <igCore/igMesh.h>
#include <igCore/igMeshMover.h>
#include <igCore/igMeshMoverMorphing.h>
#include <igCore/igMeshMoverSliding.h>
#include <igDistributed/igCommunicator.h>
#include <igFiles/igFileManager.h>

using namespace std;


char* getCmdOption(char ** begin, char ** end, const std::string & option)
{
    char ** itr = std::find(begin, end, option);
    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return 0;
}

bool cmdOptionExists(char** begin, char** end, const std::string& option)
{
    return std::find(begin, end, option) != end;
}


///////////////////////////////////////////////////////////////

// main program
int main(int argc, char *argv[])
{

    //----------------------------- arguments ----------------------------------

    string mesh_filename;
    string integrator_name;
    string movement_type;
    string deformation_filename;
    double initial_time = 0.;
    double end_time;
    double time_step;
    double period_save;
    vector<double> *param_ALE = new vector<double>;


    if (cmdOptionExists(argv, argv + argc, "-mesh"))
        mesh_filename = getCmdOption(argv, argv + argc, "-mesh");
    else{
        cout << "mesh file not provided !"<< endl;
        return 0;
    }

    if (cmdOptionExists(argv, argv + argc, "-time"))
        end_time  = atof(getCmdOption(argv, argv + argc, "-time"));
    else{
        cout << "end time value not provided !"<< endl;
        return 0;
    }

    if (cmdOptionExists(argv, argv + argc, "-save_period")){
        period_save  = atof(getCmdOption(argv, argv + argc, "-save_period"));
    } else {
        period_save = end_time;
    }

    if (cmdOptionExists(argv, argv + argc, "-integrator")){
        integrator_name  = getCmdOption(argv, argv + argc, "-integrator");
    } else {
        integrator_name = "ssp";
    }

    if (cmdOptionExists(argv, argv + argc, "-step")){
        time_step  = atof(getCmdOption(argv, argv + argc, "-step"));
    } else {
        time_step = 0.05;
    }

    if (cmdOptionExists(argv, argv + argc, "-ale"))
    {
        movement_type = getCmdOption(argv, argv + argc, "-ale");
    }

    if (cmdOptionExists(argv, argv + argc, "-ale_param")){
    	param_ALE->push_back(atof(getCmdOption(argv, argv + argc, "-ale_param")));
    }

    if (cmdOptionExists(argv, argv + argc, "-deformation"))
    	deformation_filename = getCmdOption(argv, argv + argc, "-deformation");

    //---------------------- mesh, solution & communicator ----------------------------

    igCommunicator *communicator = new igCommunicator();
    communicator->initialize();

    vector<double> *coordinates = new vector<double>;
    vector<double> *velocities = new vector<double>;
    vector<double> *deformation = new vector<double>;

    igMesh *mesh = new igMesh;
    mesh->setCommunicator(communicator);
    mesh->setCoordinates(coordinates);
    mesh->setVelocities(velocities);
    mesh->build(mesh_filename);
    mesh->setTime(initial_time);

    igFileManager file_manager;
    file_manager.setPartitionNumber(communicator->size());
    file_manager.setMyPartition(communicator->rank());
    file_manager.setSavePeriod(period_save);

    igMeshMover *mover;

    if(movement_type == "sliding" || movement_type == "sliding_pitch" || movement_type == "turbine"){
    	mover = new igMeshMoverSliding(mesh, coordinates, velocities);
    }
    else if(movement_type == "morphing"){
    	mover = new igMeshMoverMorphing(mesh, coordinates, velocities);
        file_manager.readSolutionIgloo(deformation_filename, mesh->meshDimension(), mesh, deformation);
    	mover->setDeformation(deformation);
    }
    else{
        mover = new igMeshMover(mesh, coordinates, velocities);
    }

    mover->setCommunicator(communicator);
    mover->initializeMovement(integrator_name,movement_type,param_ALE,0);
    mover->initializeVelocity(initial_time);

    //---------------------------- Mesh movement -------------------------------------------

    bool time_iteration = true;
    double time = initial_time;

    file_manager.writeMesh(mesh);

    while(time_iteration){

    	if(time + time_step > end_time)
    	{
    		time_step = end_time - time;
    		time_iteration = false;
    	}

    	time += time_step;

    	mover->move(time);
        // write mesh
        file_manager.writeMesh(mesh, time);

    }

    file_manager.writeMesh(mesh);

    delete mesh;
    delete mover;
    delete coordinates;
    delete velocities;
    delete deformation;

    if(communicator->rank() == 0){
        cout << ">>> end of program" << endl;
    }

    communicator->finalize();
    delete communicator;

    return 0;

}

///////////////////////////////////////////////////////////////
