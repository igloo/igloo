/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include <iostream>
#include <fstream>
#include <math.h>
#include <iomanip>
#include <sstream>
#include <algorithm>

#include <igGenerator/igMeshGenerator.h>
#include <igGenerator/igCaseGenerator.h>

#include <igDistributed/igCommunicator.h>


using namespace std;

///////////////////////////////////////////////////////////////

char* getCmdOption(char ** begin, char ** end, const std::string & option)
{
    char ** itr = std::find(begin, end, option);
    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return 0;
}

bool cmdOptionExists(char** begin, char** end, const std::string& option)
{
    return std::find(begin, end, option) != end;
}


/////////////////////////////////////////////////////////

// main program
int main(int argc, char **argv)
{

    string case_name;
    string mesh_filename;
    int element_number_1;
    int element_number_2;
    int element_number_3;
    int degree;
    int gauss_pt_number;
    double rotation_angle;
    bool flag_subdomains;

    int clamp_number;

    if (cmdOptionExists(argv, argv + argc, "-case"))
        case_name = getCmdOption(argv, argv + argc, "-case");
    else{
        cout << "case not provided !"<< endl;
        return 0;
    }

    if (cmdOptionExists(argv, argv + argc, "-mesh"))
        mesh_filename = getCmdOption(argv, argv + argc, "-mesh");
    else{
        cout << "mesh file not provided !"<< endl;
        return 0;
    }

    if (cmdOptionExists(argv, argv + argc, "-n1"))
        element_number_1  = atoi(getCmdOption(argv, argv + argc, "-n1"));
    else{
        cout << "element number not provided !"<< endl;
    }

    element_number_2 = 0;
    if (cmdOptionExists(argv, argv + argc, "-n2"))
        element_number_2  = atoi(getCmdOption(argv, argv + argc, "-n2"));

    element_number_3 = 0;
    if (cmdOptionExists(argv, argv + argc, "-n3"))
        element_number_3  = atoi(getCmdOption(argv, argv + argc, "-n3"));


    if (cmdOptionExists(argv, argv + argc, "-degree"))
        degree  = atoi(getCmdOption(argv, argv + argc, "-degree"));
    else{
        cout << "degree not provided !"<< endl;
        return 0;
    }

    if (cmdOptionExists(argv, argv + argc, "-gauss"))
        gauss_pt_number  = atoi(getCmdOption(argv, argv + argc, "-gauss"));
    else{
        cout << "Gauss points number not provided !" << endl;
        return 0;
    }

    rotation_angle = 0.;
    if (cmdOptionExists(argv, argv + argc, "-alpha"))
        rotation_angle  = - atof(getCmdOption(argv, argv + argc, "-alpha"))*M_PI/180;

    flag_subdomains = false;
    if (cmdOptionExists(argv, argv + argc, "-subdomains"))
    	flag_subdomains = true;

    clamp_number = 0;
    if (cmdOptionExists(argv, argv + argc, "-clamp"))
        clamp_number  = atoi(getCmdOption(argv, argv + argc, "-clamp"));



    //------------- case parameters generation --------

    igCaseGenerator case_gen;

    case_gen.setCase(case_name);
    case_gen.setElementNumber(element_number_1,element_number_2,element_number_3);

    case_gen.generateCase();

    //--------------- mesh generation -----------------

    igCommunicator *communicator = new igCommunicator();
    communicator->initialize();

    igMeshGenerator *mesh_gen = case_gen.meshGenerator();

    mesh_gen->setCommunicator(communicator);
    mesh_gen->setDegree(degree);
    mesh_gen->setElementNumber(case_gen.elementNumber1(),case_gen.elementNumber2(),case_gen.elementNumber3());
    mesh_gen->setVariableNumber(case_gen.variableNumber());
    mesh_gen->setDerivativeNumber(case_gen.derivativeNumber());
    mesh_gen->setGaussPointNumber(gauss_pt_number);
    mesh_gen->setDomainBounds(case_gen.xiMin(), case_gen.xiMax(), case_gen.etaMin(), case_gen.etaMax(), case_gen.zetaMin(), case_gen.zetaMax());
    mesh_gen->setRotation(rotation_angle);
    mesh_gen->setScale(case_gen.scaleFactor());
    mesh_gen->setPeriodicity(case_gen.periodicBoundary());
    mesh_gen->setSubdomains(flag_subdomains);
    mesh_gen->setXFunction(case_gen.xFunction());
    mesh_gen->setYFunction(case_gen.yFunction());
    mesh_gen->setZFunction(case_gen.zFunction());
    mesh_gen->setDitributionXiFunction(case_gen.distributionXiFunction());
    mesh_gen->setDitributionEtaFunction(case_gen.distributionEtaFunction());
    mesh_gen->setDitributionZetaFunction(case_gen.distributionZetaFunction());
    mesh_gen->setInitialPatch(case_gen.xCoordinateInput(),case_gen.yCoordinateInput(),case_gen.weightInput(),case_gen.sizeInput(0),case_gen.sizeInput(1));
    mesh_gen->setExactSolutionId(case_gen.exactSolutionId());
    mesh_gen->setBoundaryConditions(case_gen.typeSouthBoundary(),case_gen.typeEastBoundary(),
                                     case_gen.typeNorthBoundary(),case_gen.typeWestBoundary(),
                                     case_gen.typeTopBoundary(),case_gen.typeBottomBoundary());
    mesh_gen->setDistributionFile(case_gen.distributionFile());
    mesh_gen->setClamp(clamp_number);

    mesh_gen->generate();

    if(case_gen.enforceSymetry())
        mesh_gen->enforceSymmetry(case_gen.symetryCoordinate());
    if(case_gen.enforceMembrane())
        mesh_gen->enforceMembrane();

    if(!mesh_gen->generatedMesh())
        mesh_gen->buildGeneratedMesh();

    mesh_gen->writeMeshFile(mesh_filename);

    return 0;
}
