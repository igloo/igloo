/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include <iostream>
#include <fstream>
#include <math.h>
#include <iomanip>
#include <sstream>
#include <algorithm>

#include <igFiles/igFileManager.h>
#include <igDistributed/igCommunicator.h>
#include <igCore/igMesh.h>
#include <igCore/igDistributor.h>
#include <igCore/igMeshElevator.h>
#include <igCore/igElement.h>

using namespace std;

///////////////////////////////////////////////////////////////

char* getCmdOption(char ** begin, char ** end, const std::string & option)
{
    char ** itr = std::find(begin, end, option);
    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return 0;
}

bool cmdOptionExists(char** begin, char** end, const std::string& option)
{
    return std::find(begin, end, option) != end;
}


/////////////////////////////////////////////////////////

// main program
int main(int argc, char **argv)
{
    //--------------- arguments management -----------------

    string mesh_filename;
    int degree_final;
    int gauss_pt_number;

    if (cmdOptionExists(argv, argv + argc, "-mesh"))
        mesh_filename = getCmdOption(argv, argv + argc, "-mesh");
    else{
        cout << "mesh file not provided !"<< endl;
        return 0;
    }

    if (cmdOptionExists(argv, argv + argc, "-degree"))
        degree_final = atoi(getCmdOption(argv, argv + argc, "-degree"));
    else{
        cout << "degree not provided !"<< endl;
        return 0;
    }

    if (cmdOptionExists(argv, argv + argc, "-gauss"))
        gauss_pt_number = atoi(getCmdOption(argv, argv + argc, "-gauss"));
    else{
        cout << "gauss not provided !"<< endl;
        return 0;
    }


    //--------------- initialization -----------------

    igCommunicator *communicator = new igCommunicator();
    communicator->initialize();

    igFileManager file_manager;

    vector<double> *coordinates = new vector<double>;
    vector<double> *velocities = new vector<double>;

    igMesh *mesh = new igMesh;
    mesh->setCommunicator(communicator);
    mesh->setCoordinates(coordinates);
    mesh->setVelocities(velocities);
    mesh->build(mesh_filename);

    int internal_dof = mesh->controlPointNumber();
    int shared_dof = mesh->sharedControlPointNumber();
    int variable_number = mesh->variableNumber();

    int degree_init = mesh->element(0)->cellDegree();

    vector<double> *state = new vector<double>;
    state->resize((internal_dof+shared_dof)*variable_number,0.);


    //-------------- elevation loop ----------------

    igMeshElevator *elevator = new igMeshElevator;
    elevator->initialize(mesh,coordinates);
    elevator->setGaussPointNumber(gauss_pt_number);

    for(int ideg=degree_init; ideg<degree_final;ideg++){
        cout << "elevation process to degree " << ideg+1 <<endl;
        elevator->elevate();
    }
    //--------------- GLVis output -----------------
    file_manager.writeMesh(mesh);


    //--------------- partitionning ----------------------------

    igDistributor *distributor = new igDistributor;
    distributor->setMesh(mesh);
    distributor->setPartitionNumber(1);
    distributor->run();

    //--------------- mesh and solution output -----------------

    file_manager.writeMeshIgloo("mesh_elevated.dat", mesh, distributor);

    //--------------------------------------

    delete elevator;
    delete distributor;
    delete mesh;

    if(communicator)
        communicator->finalize();
    delete communicator;

    return 0;

}
