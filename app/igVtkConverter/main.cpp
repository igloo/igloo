/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <stdlib.h>
#include <algorithm>

#include <igFiles/igFileManager.h>
#include <igFiles/igVtkWriter.h>
#include <igDistributed/igCommunicator.h>
#include <igCore/igMesh.h>
#include <igCore/igBasisBezier.h>
#include <igCore/igDistributor.h>

using namespace std;

///////////////////////////////////////////////////////////////

char* getCmdOption(char ** begin, char ** end, const std::string & option)
{
    char ** itr = std::find(begin, end, option);
    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return 0;
}

bool cmdOptionExists(char** begin, char** end, const std::string& option)
{
    return std::find(begin, end, option) != end;
}


/////////////////////////////////////////////////////////

// main program
int main(int argc, char **argv)
{
    //--------------- arguments management -----------------

    int file_id = 0;
    if (cmdOptionExists(argv, argv + argc, "-file_id")){
        file_id = atoi(getCmdOption(argv, argv + argc, "-file_id"));
    }

    int mesh_id = 0;
    if (cmdOptionExists(argv, argv + argc, "-mesh_id")){
        mesh_id = atoi(getCmdOption(argv, argv + argc, "-mesh_id"));
    }

    bool density = false;
    if (cmdOptionExists(argv, argv + argc, "-density")){
        density = true;
    }

    bool energy = false;
    if (cmdOptionExists(argv, argv + argc, "-energy")){
        energy = true;
    }

    bool momentum = false;
    if (cmdOptionExists(argv, argv + argc, "-momentum")){
        momentum = true;
    }

    bool wall = false;
    string mesh_filename;
    if (cmdOptionExists(argv, argv + argc, "-mesh")){
		mesh_filename = getCmdOption(argv, argv + argc, "-mesh");
	    wall = true;
	}

    int proc_number = 1;
    if (cmdOptionExists(argv, argv + argc, "-proc")){
        proc_number = atoi(getCmdOption(argv, argv + argc, "-proc"));
    }

    int refine_number = 0;
    if (cmdOptionExists(argv, argv + argc, "-refine")){
        refine_number = atoi(getCmdOption(argv, argv + argc, "-refine"));
    }

    // call to igVtkWriter tools
    igVtkWriter writer;

    //---------------- convert GlVis files for solution --------------------

    if(density || momentum || energy){
        writer.convertGlVis(mesh_id, file_id, proc_number, refine_number, density, energy, momentum);
    }

    //------------------- extract walls from mesh file ----------------------

    if(wall){

        igCommunicator *communicator = new igCommunicator();
        communicator->initialize();

        vector<double> *coordinates = new vector<double>;
        vector<double> *velocities = new vector<double>;

        igMesh *mesh = new igMesh;
        mesh->setCommunicator(communicator);
        mesh->setCoordinates(coordinates);
        mesh->setVelocities(velocities);
        mesh->build(mesh_filename);

        writer.extractWall(mesh, coordinates, refine_number);

        delete mesh;
        delete velocities;
        delete coordinates;

        communicator->finalize();
        delete communicator;
    }


    //---------------------------------------------


    return 0;

}
