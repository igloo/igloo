/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
 ****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 ***************************************************************************/
#include <iostream>
#include <algorithm>

#include <igFiles/igFileManager.h>
#include <igDistributed/igCommunicator.h>
#include <igCore/igMesh.h>
#include <igCore/igMeshRefiner.h>
#include <igCore/igDistributor.h>
#include <igSolver/igErrorEstimatorBox.h>
#include <igSolver/igErrorEstimatorEllipse.h>
#include <igSolver/igErrorEstimatorGlobal.h>
#include <igSolver/igErrorEstimatorWall.h>

using namespace std;

///////////////////////////////////////////////////////////////

char* getCmdOption(char ** begin, char ** end, const std::string & option)
{
	char ** itr = std::find(begin, end, option);
	if (itr != end && ++itr != end)
	{
		return *itr;
	}
	return 0;
}

bool cmdOptionExists(char** begin, char** end, const std::string& option)
{
	return std::find(begin, end, option) != end;
}

/////////////////////////////////////////////////////////

// main program
int main(int argc, char **argv)
{
	//--------------- arguments management -----------------

	string mesh_filename;
	if (cmdOptionExists(argv, argv + argc, "-mesh"))
		mesh_filename = getCmdOption(argv, argv + argc, "-mesh");
	else{
		cout << "mesh file not provided !"<< endl;
		return 0;
	}

	int step_number;

	bool wall_mode = false;
	if (cmdOptionExists(argv, argv + argc, "-wall")){
		step_number = atoi(getCmdOption(argv, argv + argc, "-wall"));
		wall_mode = true;
	}

	bool global_mode = false;
	if (cmdOptionExists(argv, argv + argc, "-global")){
		step_number = atoi(getCmdOption(argv, argv + argc, "-global"));
		global_mode = true;
	}

	bool region_mode = false;
	int parameter_number = 0;
	string region_filename;

	bool box_mode = false;
	if (cmdOptionExists(argv, argv + argc, "-box")){
		region_filename = getCmdOption(argv, argv + argc, "-box");
		region_mode = true;
		box_mode = true;
		parameter_number = 4;
	}

	bool ellipse_mode = false;
	if (cmdOptionExists(argv, argv + argc, "-ellipse")){
		region_filename = getCmdOption(argv, argv + argc, "-ellipse");
		region_mode = true;
		ellipse_mode = true;
		parameter_number = 5;
	}

	bool sliding = false;
	if (cmdOptionExists(argv, argv + argc, "-sliding"))
		sliding = true;

	bool remove_hanging_node = false;
	if (cmdOptionExists(argv, argv + argc, "-no_hanging"))
		remove_hanging_node = true;

	string deformation_filename;
	bool deformation = false;
	if (cmdOptionExists(argv, argv + argc, "-deformation")){
		deformation_filename = getCmdOption(argv, argv + argc, "-deformation");
		deformation = true;
	}

    bool subdomain_mode = false;
    int subdomain_id = 0;
	if (cmdOptionExists(argv, argv + argc, "-subdomain")){
		subdomain_id = atoi(getCmdOption(argv, argv + argc, "-subdomain"));
		subdomain_mode = true;
	}


	//--------------- initialization -----------------

	igCommunicator *communicator = new igCommunicator();
	communicator->initialize();

	igFileManager file_manager;

	int direction = 0;

	vector<double> *coordinates = new vector<double>;
	vector<double> *velocities = new vector<double>;

	igMesh *mesh = new igMesh;
	mesh->setCommunicator(communicator);
	mesh->setCoordinates(coordinates);
	mesh->setVelocities(velocities);
	mesh->build(mesh_filename);

	int internal_dof = mesh->controlPointNumber();
	int shared_dof = mesh->sharedControlPointNumber();
	int variable_number = mesh->variableNumber();

	vector<double> *state = new vector<double>;
	state->resize((internal_dof+shared_dof)*variable_number,0.);

	vector<double> *error_xi = new vector<double>;
	vector<double> *error_eta = new vector<double>;

	igErrorEstimator *error_estimator;
	if(box_mode)
		error_estimator = new igErrorEstimatorBox;
	if(ellipse_mode)
		error_estimator = new igErrorEstimatorEllipse;
	else if(wall_mode)
		error_estimator = new igErrorEstimatorWall;
	else if(global_mode)
		error_estimator = new igErrorEstimatorGlobal;
	error_estimator->setMesh(mesh);
	error_estimator->setError(error_xi,error_eta);
    if(subdomain_mode){
        error_estimator->setSubdomain(subdomain_id);
    }


	igMeshRefiner *refiner = new igMeshRefiner;
	refiner->initialize(mesh, state, coordinates, velocities);
	refiner->setRefineCoefficient(0.);
	refiner->setError(error_xi,error_eta);

	if(deformation)
		file_manager.readSolutionIgloo(deformation_filename,mesh->meshDimension(),mesh,velocities);


	//-------------- refinement loop ----------------

	ifstream arg_file(region_filename.c_str(), ios::in);

	if(region_mode)
		arg_file >> step_number;

	for(int istep=0; istep<step_number; istep++){

		if(region_mode){

			vector<double> args(parameter_number,0.);

			for(int i=0; i<parameter_number; i++){
				arg_file >> args[i];
			}
			arg_file >> direction;

			// compute error estimate
			error_estimator->setRegion(args);
			error_estimator->setRefinementDirection(direction);

		}

		// flag cells
		error_estimator->computeEstimator();

		// remove hanging nodes
		if(remove_hanging_node)
			error_estimator->removeHangingNodes();

		// refine grid
		refiner->select();
		refiner->refine();

		mesh->updateMeshProperties();
		mesh->updateGlobalMeshProperties();

		direction = error_estimator->refinementDirection();

		// reparametrize
		if(direction == 0 && sliding)
			refiner->reparametrize();

		//--------------- GLVis output -----------------
		file_manager.writeMesh(mesh);

	}

	if(arg_file.is_open())
		arg_file.close();

	//-------------- cleaning --------------------------

	mesh->cleanInactiveCells(state);

	//--------------- partitionning ----------------------------

	igDistributor *distributor = new igDistributor;
	distributor->setMesh(mesh);
	distributor->setPartitionNumber(1);
	distributor->run();

	//--------------- mesh and solution output -----------------

	file_manager.writeMeshIgloo("mesh_refined.dat", mesh, distributor);

	if(deformation)
		file_manager.writeSolutionIgloo("deformation_refined.dat",mesh->meshDimension(),mesh,velocities);

	//--------------------------------------

	delete distributor;
	delete error_estimator;
	delete error_xi;
	delete error_eta;
	delete mesh;

	if(communicator)
		communicator->finalize();
	delete communicator;

	return 0;

}
