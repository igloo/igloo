/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include <iostream>
#include <fstream>
#include <math.h>
#include <iomanip>
#include <sstream>
#include <algorithm>

#include <igFiles/igFileManager.h>
#include <igDistributed/igCommunicator.h>
#include <igCore/igDistributor.h>
#include <igCore/igMesh.h>
#include <igCore/igMeshRefiner.h>
#include <igSolver/igErrorEstimatorBox.h>

using namespace std;

///////////////////////////////////////////////////////////////

char* getCmdOption(char ** begin, char ** end, const std::string & option)
{
    char ** itr = std::find(begin, end, option);
    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return 0;
}

bool cmdOptionExists(char** begin, char** end, const std::string& option)
{
    return std::find(begin, end, option) != end;
}

/////////////////////////////////////////////////////////

// main program
int main(int argc, char **argv)
{
    //--------------- arguments management -----------------

    string mesh_filename;
    int partition_number_1 = 1;
    int partition_number_2 = 1;
    int partition_number_3 = 1;
    int element_number_1 = 0;
    int element_number_2 = 0;
    int element_number_3 = 0;


    if (cmdOptionExists(argv, argv + argc, "-mesh"))
        mesh_filename = getCmdOption(argv, argv + argc, "-mesh");
    else{
        cout << "mesh file not provided !"<< endl;
        return 0;
    }

    if (cmdOptionExists(argv, argv + argc, "-npart1"))
        partition_number_1  = atoi(getCmdOption(argv, argv + argc, "-npart1"));

    if (cmdOptionExists(argv, argv + argc, "-npart2"))
        partition_number_2  = atoi(getCmdOption(argv, argv + argc, "-npart2"));

    if (cmdOptionExists(argv, argv + argc, "-npart3"))
        partition_number_3  = atoi(getCmdOption(argv, argv + argc, "-npart3"));


    if (cmdOptionExists(argv, argv + argc, "-n1"))
        element_number_1  = atoi(getCmdOption(argv, argv + argc, "-n1"));

    if (cmdOptionExists(argv, argv + argc, "-n2"))
        element_number_2  = atoi(getCmdOption(argv, argv + argc, "-n2"));

    if (cmdOptionExists(argv, argv + argc, "-n3"))
        element_number_3  = atoi(getCmdOption(argv, argv + argc, "-n3"));

   bool flag_sliding = false;
   if (cmdOptionExists(argv, argv + argc, "-sliding"))
	   flag_sliding = true;

    bool single_patch;
    int total_partition_number;

    if(element_number_1 == 0 && element_number_2 == 0 && element_number_3 == 0){
        single_patch = false;
        total_partition_number = partition_number_1;
    } else {
        single_patch = true;
        total_partition_number = partition_number_1*partition_number_2*partition_number_3;
    }


    //--------------- initialization -----------------

    igCommunicator *communicator = new igCommunicator();
    communicator->initialize();

    igFileManager file_manager;

    vector<double> *coordinates = new vector<double>;
    vector<double> *velocities = new vector<double>;

    igMesh *mesh = new igMesh;
    mesh->setCommunicator(communicator);
    mesh->setCoordinates(coordinates);
    mesh->setVelocities(velocities);
    mesh->build(mesh_filename);

    igDistributor *distributor = new igDistributor;
    distributor->setMesh(mesh);
    distributor->setSliding(flag_sliding);

    //-------------------------------------------------

    if(single_patch){
        distributor->setPartitionNumber(partition_number_1, partition_number_2, partition_number_3);
        distributor->setElementNumber(element_number_1, element_number_2, element_number_3);
    } else
        distributor->setPartitionNumber(total_partition_number);

    cout << endl;
    cout << "Partitions: " << total_partition_number << endl;
    cout << endl;

    //-------------------------------------------------

    distributor->run();

    //--------------- mesh and solution output -----------------

    file_manager.writeMeshIgloo("mesh_distributed.dat", mesh, distributor);

    for (int ipart=0; ipart<total_partition_number; ipart++)
        if(mesh->meshDimension() == 2)
            file_manager.writeMesh2DGlvis("igloo.mesh", mesh, ipart, true);
        else
            file_manager.writeMesh3DGlvis("igloo.mesh", mesh, ipart, true);

    //--------------------------------------

    delete distributor;
    delete mesh;

    if(communicator)
        communicator->finalize();
    delete communicator;

    return 0;

}
