/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include <algorithm>

#include <igFiles/igFileManager.h>
#include <igDistributed/igCommunicator.h>
#include <igCore/igMesh.h>
#include <igCore/igDistributor.h>
#include <igCore/igMeshDeformer.h>
#include <igCore/igDeformAnalytic.h>

using namespace std;

///////////////////////////////////////////////////////////////

char* getCmdOption(char ** begin, char ** end, const std::string & option)
{
    char ** itr = std::find(begin, end, option);
    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return 0;
}

bool cmdOptionExists(char** begin, char** end, const std::string& option)
{
    return std::find(begin, end, option) != end;
}


/////////////////////////////////////////////////////////

// main program
int main(int argc, char **argv)
{
    //--------------- arguments management -----------------

    string mesh_filename;
    if (cmdOptionExists(argv, argv + argc, "-mesh"))
        mesh_filename = getCmdOption(argv, argv + argc, "-mesh");
    else{
        cout << "mesh file not provided !"<< endl;
        return 0;
    }

    double ampl = 0.; // amplitude for curv, sinus & thickness

    bool curv = false;
    if (cmdOptionExists(argv, argv + argc, "-curv")) {
        curv = true;
        if (cmdOptionExists(argv, argv + argc, "-ampl"))
            ampl = atof(getCmdOption(argv, argv + argc, "-ampl"));
    }

    bool sinus = false;
    if (cmdOptionExists(argv, argv + argc, "-sinus")){
        sinus = true;
        if (cmdOptionExists(argv, argv + argc, "-ampl"))
            ampl = atof(getCmdOption(argv, argv + argc, "-ampl"));
    }

    bool thickness = false;
    if (cmdOptionExists(argv, argv + argc, "-thickness")){
        thickness = true;
        if (cmdOptionExists(argv, argv + argc, "-ampl"))
            ampl = atof(getCmdOption(argv, argv + argc, "-ampl"));
    }

    bool translation = false;
    double tx = 0;
    double ty = 0;
    if (cmdOptionExists(argv, argv + argc, "-translation")){
        translation = true;
        if (cmdOptionExists(argv, argv + argc, "-tx"))
            tx = atof(getCmdOption(argv, argv + argc, "-tx"));
        if (cmdOptionExists(argv, argv + argc, "-ty"))
            ty = atof(getCmdOption(argv, argv + argc, "-ty"));
    }

    bool rotation = false;
    double alpha = 0.; // angle of rigid rotation
    double xc = 0.; // x coord of rotation center
    double yc = 0.; // y coord of rotation center
    double r0 = 1.; // radius of rigid rotation
    double r1 = 2.; // radius of transition rotation
    if (cmdOptionExists(argv, argv + argc, "-rotation")){
        rotation = true;
        if (cmdOptionExists(argv, argv + argc, "-alpha"))
            alpha = atof(getCmdOption(argv, argv + argc, "-alpha"));
        if (cmdOptionExists(argv, argv + argc, "-xc"))
            xc = atof(getCmdOption(argv, argv + argc, "-xc"));
        if (cmdOptionExists(argv, argv + argc, "-yc"))
            yc = atof(getCmdOption(argv, argv + argc, "-yc"));
        if (cmdOptionExists(argv, argv + argc, "-r0"))
            r0 = atof(getCmdOption(argv, argv + argc, "-r0"));
        if (cmdOptionExists(argv, argv + argc, "-r1"))
            r1 = atof(getCmdOption(argv, argv + argc, "-r1"));
    }

    bool linear = false;
    if (cmdOptionExists(argv, argv + argc, "-linear"))
        linear = true;

    //--------------- initialization -----------------

    igCommunicator *communicator = new igCommunicator();
    communicator->initialize();

    igFileManager file_manager;

    vector<double> *coordinates = new vector<double>;
    vector<double> *velocities = new vector<double>;

    igMesh *mesh = new igMesh;
    mesh->setCommunicator(communicator);
    mesh->setCoordinates(coordinates);
    mesh->setVelocities(velocities);
    mesh->build(mesh_filename);

    igMeshDeformer *deformer = new igMeshDeformer;
    deformer->initialize(mesh, coordinates, velocities);
    deformer->setCommunicator(communicator);

    //-------------- deformations ----------------

    double *params;
    function<double(double x, double y, double *params)> def_fun_x;
    function<double(double x, double y, double *params)> def_fun_y;

    if(curv){
        params = new double[1];
        params[0] = ampl;

        def_fun_x = x_curv;
        def_fun_y = y_curv;
    }

    if(translation){
        params = new double[2];
        params[0] = tx;
        params[1] = ty;

        def_fun_x = x_translate;
        def_fun_y = y_translate;
    }

    if(rotation){
        params = new double[5];
        params[0] = alpha;
        params[1] = xc;
        params[2] = yc;
        params[3] = r0;
        params[4] = r1;

        def_fun_x = x_rotate;
        def_fun_y = y_rotate;
    }

    if(sinus){
        params = new double[1];
        params[0] = ampl;

        def_fun_x = x_sinus;
        def_fun_y = y_sinus;
    }

    if(thickness){
        params = new double[1];
        params[0] = ampl;

        def_fun_x = x_thickness;
        def_fun_y = y_thickness;
    }


    deformer->setDeformationFunctions(def_fun_x, def_fun_y);
    deformer->setDeformationFunctionParameters(params);
    deformer->deform();

    // mesh linearization (for comparison with curved mesh)
    if(linear){
        mesh->linearize();
    }

    //--------------- partitionning ----------------------------

    igDistributor *distributor = new igDistributor;
    distributor->setMesh(mesh);
    distributor->setPartitionNumber(1);
    distributor->run();

    //--------------- mesh and solution output -----------------

    file_manager.writeMesh(mesh);
    file_manager.writeMeshIgloo("mesh_deformed.dat", mesh, distributor);

    //--------------------------------------

    delete distributor;
    delete mesh;

    communicator->finalize();
    delete communicator;

    delete params;

    return 0;

}
