#!/bin/sh


echo '> case euler naca AMR ...'

########## cleaning ##############
rm -rf Test_euler_naca_AMR
mkdir Test_euler_naca_AMR

cd Test_euler_naca_AMR

########### run case ##########

../../build/bin/igGenMesh -case euler_naca -mesh mesh.dat -n1 4 -n2 8 -degree 3 -gauss 4 -alpha 2. > igGenMesh.log
../../build/bin/igGenSol -case euler_naca -mesh mesh.dat -initial initial.dat > igGenSol.log
../../build/bin/igloo -solver euler -mesh mesh.dat -initial initial.dat -time 0.5 -cfl 0.7 -refine_coef 2. -coarsen_coef 0.5 -refine_max 3 -error -adapt_xmax 0. -adapt_ymin 0. > igloo.log

########### end ##########
diff igloo.log ../Reference/test_euler_naca_AMR.log
cd ..

echo '> case euler naca AMR done'

exit 0
