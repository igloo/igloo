#!/bin/sh

echo '> case navier-stokes membrane AMR ...'

########## cleaning ##############
rm -rf Test_ns_membrane_AMR
mkdir Test_ns_membrane_AMR

cd Test_ns_membrane_AMR

#cp ../Reference/box_membrane.dat ./box.dat

########### case paraemters #########

NPT=10
DEGREE=3
COEF_M=1.1
COEF_G=1.2
RATIO=0.5
CLAMP=1

LEVEL=2
REF=1.5
COA=0.8

AOA=12
MACH=0.1
RE=2500

PROC=2
TIME=0.1;
CFL=0.5;
SAVE=0.1;

########### run case ###############

GAUSS=`echo print $DEGREE + 1 | perl`


../../build/bin/igGenCurve -case membrane -degree $DEGREE -pts $NPT -coef_membrane $COEF_M -coef_grid $COEF_G -ratio $RATIO -clamp $CLAMP > gen_curve.log
../../build/bin/igGenMesh -case ns_membrane -mesh mesh.dat  -degree $DEGREE -gauss $GAUSS -clamp $CLAMP> gen_mesh.log
../../build/bin/igGenSol -case ns_membrane -mesh mesh.dat -initial initial.dat -incidence $AOA -mach $MACH > gen_sol.log
../../build/bin/igPartit -mesh mesh.dat -npart1 $PROC > partit.log

mpirun -np $PROC ../../build/bin/igloo -solver navier-stokes -mesh mesh_distributed.dat -initial initial.dat -structure membrane -time $TIME -reynolds $RE -mach $MACH -incidence $AOA -cfl $CFL -save_period $SAVE -error -refine_coef $REF -coarsen_coef $COA -refine_max $LEVEL -adapt_xmax 5 -adapt_xmin -2 -adapt_ymax 2 -adapt_ymin -2 > igloo.log


########### end ##########

diff igloo.log ../Reference/test_ns_membrane_AMR.log


cd ..

echo '> case navier-stokes membrane AMR done'

exit 0


########### end ##########


exit 0
