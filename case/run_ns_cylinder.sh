#!/bin/sh

echo '> case navier-stokes cylinder ...'

########## cleaning ##############
rm -rf Test_ns_cylinder
mkdir Test_ns_cylinder

cd Test_ns_cylinder

########### run case ##########

../../build/bin/igGenMesh -case ns_cylinder -mesh mesh.dat -n1 16 -n2 8 -degree 3 -gauss 4 > igGenMesh.log
../../build/bin/igGenSol -case ns_cylinder -mesh mesh.dat -initial initial.dat -mach 0.1 > igGenSol.log
../../build/bin/igPartit -mesh mesh.dat -npart1 2 -n1 16 -n2 8 > igPartit.log
mpirun -n 2 ../../build/bin/igloo -solver navier-stokes -mesh mesh_distributed.dat -initial initial.dat -time 0.25 -mach 0.1 -error > igloo.log

########### end ##########
diff igloo.log ../Reference/test_ns_cylinder.log
cd ..

echo '> case navier-stokes cylinder done'

exit 0
