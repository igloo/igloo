
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation,FFMpegFileWriter
from pylab import genfromtxt;
import os
import fnmatch

nbframe = len(fnmatch.filter(os.listdir('.'), 'curve_*.dat'))

fig, ax = plt.subplots()
xdata_pt, ydata_pt = [], []
xdata_curve, ydata_curve = [], []
ln_pt, = plt.plot([], [], color="red", linestyle="none", marker="s", animated=True)
ln_curve, = plt.plot([], [], color="blue", linestyle="-", animated=True)
f = np.arange(0, nbframe-1, 1)

pt_0 = genfromtxt("pt_0.dat")
xdata_pt = pt_0[:,0]
ydata_pt = pt_0[:,1]
curve_0 = genfromtxt("curve_0.dat")
xdata_curve = curve_0[:,0]
ydata_curve = curve_0[:,1]

def init():
    ax.set_xlim(-0.1, 1.1)
    ax.set_ylim(-0.5, 0.05)
    ln_pt.set_data(xdata_pt,ydata_pt)
    ln_curve.set_data(xdata_curve,ydata_curve)
    return ln_pt, ln_curve

def update(frame):

    filename_pt = "pt_"+str(frame)+".dat"
    pt = genfromtxt(filename_pt)
    xdata_pt = pt[:,0]
    ydata_pt = pt[:,1]
    ln_pt.set_data(xdata_pt, ydata_pt)

    filename_curve = "curve_"+str(frame)+".dat"
    curve = genfromtxt(filename_curve)
    xdata_curve = curve[:,0]
    ydata_curve = curve[:,1]
    ln_curve.set_data(xdata_curve, ydata_curve)

    return ln_pt, ln_curve


ani = FuncAnimation(fig, update, frames=f,
                    init_func=init, blit=True, interval = 2.,repeat=True)
plt.show()

# mywriter = FFMpegFileWriter(fps=25,codec="libx264")
# ani.save("test.mp4", writer=mywriter)
