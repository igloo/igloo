#!/bin/sh

echo '> case navier-stokes membrane ...'

########## cleaning ##############
rm -rf Test_ns_membrane
mkdir Test_ns_membrane

cd Test_ns_membrane

#cp ../Reference/box_membrane.dat ./box.dat

########### case paraemters #########

NPT=17
DEGREE=3
RATIO=1
CLAMP=2

AOA=12
MACH=0.1
RE=2500

PROC=2
TIME=0.1;
CFL=0.5;
SAVE=0.1;

########### run case ###############

GAUSS=`echo print $DEGREE + 1 | perl`


../../build/bin/igGenCurve -case membrane -degree $DEGREE -pts $NPT -ratio $RATIO -clamp $CLAMP > gen_curve.log
../../build/bin/igGenMesh -case ns_membrane -mesh mesh.dat  -degree $DEGREE -gauss $GAUSS -clamp $CLAMP> gen_mesh.log
../../build/bin/igGenSol -case ns_membrane -mesh mesh.dat -initial initial.dat -incidence $AOA -mach $MACH > gen_sol.log
../../build/bin/igPartit -mesh mesh.dat -npart1 $PROC > partit.log

mpirun -np $PROC ../../build/bin/igloo -solver navier-stokes -mesh mesh_distributed.dat -initial initial.dat -structure membrane -time $TIME -reynolds $RE -mach $MACH -incidence $AOA -cfl $CFL -save_period $SAVE -error > igloo.log


########### end ##########

diff igloo.log ../Reference/test_ns_membrane.log


cd ..

echo '> case navier-stokes membrane done'

exit 0


########### end ##########


exit 0
