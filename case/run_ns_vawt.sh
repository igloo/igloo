#!/bin/sh


echo '> case navier-stokes vawt ...'

########## cleaning ##############
rm -rf Test_ns_vawt
mkdir Test_ns_vawt

cd Test_ns_vawt

cp ../Reference/baseline_vawt.dat ./baseline.dat
cp ../Reference/box_vawt.dat ./box.dat

########### run case ##########

../../build/bin/igGenMesh -case ns_multi_patch -mesh mesh.dat -n1 136 -degree 2 -gauss 4  -subdomains > igGenMesh.log
../../build/bin/igPreRefiner -mesh mesh.dat -box box.dat -sliding > igPreRefiner.log
../../build/bin/igSmoother -mesh mesh_refined.dat -step 50 -sliding > igSmoother.log
../../build/bin/igGenSol -case ns_multi_patch -mesh mesh_smoothed.dat -initial solution_refined.dat -mach 0.1 > igGenSol.log
../../build/bin/igPartit -mesh mesh_smoothed.dat -npart1 2 -sliding > igPartit.log
mpirun -np 2 ../../build/bin/igloo -solver navier-stokes -mesh mesh_distributed.dat -initial solution_refined.dat -time 0.01 -cfl 0.5 -mach 0.1 -reynolds 100. -ale turbine -error > igloo.log

########### end ##########
diff igGenMesh.log ../Reference/test_ns_vawt_gen.log
diff igPreRefiner.log ../Reference/test_ns_vawt_pre.log
diff igSmoother.log ../Reference/test_ns_vawt_smoother.log
diff igPartit.log ../Reference/test_ns_vawt_part.log
diff igloo.log ../Reference/test_ns_vawt_igloo.log
cd ..

echo '> case navier-stokes vawt done'

exit 0
