#!/bin/sh


echo '> case euler cylinder ...'

########## cleaning ##############
rm -rf Test_euler_cylinder
mkdir Test_euler_cylinder

cd Test_euler_cylinder

########### run case ##########

../../build/bin/igGenMesh -case euler_cylinder -mesh mesh.dat -n1 4 -n2 4 -degree 5 -gauss 7 > igGenMesh.log
../../build/bin/igGenSol -case euler_cylinder -mesh mesh.dat -initial initial.dat > igGenSol.log
../../build/bin/igPartit -mesh mesh.dat -npart1 2 -n1 4 -n2 4 > igPartit.log
mpirun -n 2 ../../build/bin/igloo -solver euler -mesh mesh_distributed.dat -initial initial.dat -time 2. -error -integrator rk4 -cfl 0.7 > igloo.log

########### end ##########
diff igloo.log ../Reference/test_euler_cylinder.log
cd ..

echo '> case euler cylinder done'

exit 0
