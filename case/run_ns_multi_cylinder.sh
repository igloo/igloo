#!/bin/sh


echo '> case navier-stokes multi-patch cylinder ...'

########## cleaning ##############
rm -rf Test_ns_multi_cylinder
mkdir Test_ns_multi_cylinder

cd Test_ns_multi_cylinder

cp ../Reference/baseline_multi_cylinder.dat ./baseline.dat
cp ../Reference/box_multi_cylinder.dat ./box.dat

########### run case ##########

../../build/bin/igGenMesh -case ns_multi_patch -mesh mesh.dat -n1 15 -degree 3 -gauss 4 -alpha 0.5 > igGenMesh.log
../../build/bin/igPreRefiner -mesh mesh.dat -box ./box.dat > igPreRefiner.log
../../build/bin/igGenSol -case ns_multi_patch -mesh mesh_refined.dat -initial solution_refined.dat -mach 0.1 > igGenSol.log
../../build/bin/igPartit -mesh mesh_refined.dat -npart1 2 > igPartit.log
mpirun -np 2 ../../build/bin/igloo -solver navier-stokes -mesh mesh_distributed.dat -initial solution_refined.dat -time 5. -cfl 0.5 -mach 0.1 -reynolds 100. -error > igloo.log

########### end ##########
diff igGenMesh.log ../Reference/test_ns_multi_cylinder_gen.log
diff igPreRefiner.log ../Reference/test_ns_multi_cylinder_pre.log
diff igPartit.log ../Reference/test_ns_multi_cylinder_part.log
diff igloo.log ../Reference/test_ns_multi_cylinder_igloo.log
cd ..

echo '> case navier-stokes multi-patch cylinder done'

exit 0
