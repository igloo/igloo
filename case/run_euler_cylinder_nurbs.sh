#!/bin/sh


echo '> case euler cylinder nurbs ...'

########## cleaning ##############
rm -rf Test_euler_cylinder_nurbs
mkdir Test_euler_cylinder_nurbs

cd Test_euler_cylinder_nurbs

########### run case ##########

../../build/bin/igGenMesh -case euler_cylinder_nurbs -mesh mesh.dat -n1 2 -n2 2 -degree 2 -gauss 7 > igGenMesh.log
../../build/bin/igGenSol -case euler_cylinder_nurbs -mesh mesh.dat -initial initial.dat > igGenSol.log
../../build/bin/igloo -solver euler -mesh mesh.dat -initial initial.dat -time 50. -integrator rk4 -error > igloo.log

########### end ##########
diff igloo.log ../Reference/test_euler_cylinder_nurbs.log
cd ..

echo '> case euler cylinder nurbs done'

exit 0
