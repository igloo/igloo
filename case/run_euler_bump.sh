#!/bin/sh


echo '> case euler bump ...'

########## cleaning ##############
rm -rf Test_euler_bump
mkdir Test_euler_bump

cd Test_euler_bump

########### run case ##########

../../build/bin/igGenMesh -case euler_bump_exp -mesh mesh.dat -n1 8 -n2 4 -degree 3 -gauss 5 > igGenMesh.log
../../build/bin/igGenSol -case euler_bump_exp -mesh mesh.dat -initial initial.dat > igGenSol.log
../../build/bin/igloo -solver euler -mesh mesh.dat -initial initial.dat -time 1. -error > igloo.log

########### end ##########
diff igloo.log ../Reference/test_euler_bump.log
cd ..

echo '> case euler bump done'

exit 0
