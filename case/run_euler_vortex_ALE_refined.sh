#!/bin/sh


echo '> case euler vortex ALE refined ...'

########## cleaning ##############
rm -rf Test_euler_vortex_ALE_refined
mkdir Test_euler_vortex_ALE_refined

cd Test_euler_vortex_ALE_refined

########### run case ##########

../../build/bin/igGenMesh -case euler_vortex -mesh mesh.dat -n1 16 -n2 16 -degree 3 -gauss 5 > igGenMesh.log
../../build/bin/igPreRefiner -mesh mesh.dat -box ../Reference/box_vortex.dat > igPreRefiner.log
../../build/bin/igGenSol -case euler_vortex -mesh mesh_refined.dat -initial solution_refined.dat > igGenSol.log
../../build/bin/igPartit -mesh mesh_refined.dat -npart1 2 > igPartit.log
mpirun -np 2 ../../build/bin/igloo -solver euler -mesh mesh_distributed.dat -initial solution_refined.dat -time 2. -integrator rk4 -error -ale sinusoidal > igloo.log

########### end ##########
diff igPreRefiner.log ../Reference/test_euler_vortex_ALE_refined_pre.log
diff igPartit.log ../Reference/test_euler_vortex_ALE_refined_part.log
diff igloo.log ../Reference/test_euler_vortex_ALE_refined.log
cd ..

echo '> case euler vortex ALE refined done'

exit 0
