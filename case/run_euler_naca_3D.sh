#!/bin/sh


echo '> case euler naca 3D ...'

########## cleaning ##############
rm -rf Test_euler_naca_3D
mkdir Test_euler_naca_3D

cd Test_euler_naca_3D

########### run case ##########

../../build/bin/igGenMesh -case euler_naca_3D -mesh mesh.dat -n1 6 -n2 16 -n3 4 -degree 2 -gauss 3 -alpha 2. > igGenMesh.log
../../build/bin/igGenSol -case euler_naca_3D -mesh mesh.dat -initial initial.dat > igGenSol.log
../../build/bin/igPartit -mesh mesh.dat -n1 6 -n2 16 -n3 4 -npart1 1 -npart2 1 -npart3 2 > igPart.log

mpirun -np 2 ../../build/bin/igloo -solver euler -mesh mesh_distributed.dat -initial initial.dat -time 0.5 -error > igloo.log

########### end ##########
diff igloo.log ../Reference/test_euler_naca_3D.log
cd ..

echo '> case euler naca 3D done'

exit 0
