#!/bin/sh

echo '> case navier-stokes plate ...'

########## cleaning ##############
rm -rf Test_ns_plate
mkdir Test_ns_plate

cd Test_ns_plate

########### run case ##########

../../build/bin/igGenMesh -case ns_plate -mesh mesh.dat -n1 12 -n2 8 -degree 3 -gauss 5  > igGenMesh.log
../../build/bin/igGenSol -case ns_plate -mesh mesh.dat -initial initial.dat  > igGenSol.log
../../build/bin/igPartit -mesh mesh.dat -npart1 2 -n1 12 -n2 8 > igPartit.log
mpirun -n 2 ../../build/bin/igloo -solver navier-stokes -mesh mesh_distributed.dat -initial initial.dat -time 1. -error > igloo.log

########### end ##########
diff igloo.log ../Reference/test_ns_plate.log
cd ..

echo '> case navier-stokes plate done'

exit 0
