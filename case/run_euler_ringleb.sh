#!/bin/sh


echo '> case euler ringleb ...'

########## cleaning ##############
rm -rf Test_euler_ringleb
mkdir Test_euler_ringleb

cd Test_euler_ringleb

########### run case ##########

../../build/bin/igGenMesh -case euler_ringleb -mesh mesh.dat -n1 8 -n2 8 -degree 3 -gauss 5 > igGenMesh.log
../../build/bin/igGenSol -case euler_ringleb -mesh mesh.dat -initial initial.dat > igGenSol.log
../../build/bin/igloo -solver euler -mesh mesh.dat -initial initial.dat -time 5. -error -integrator rk4  > igloo.log

########### end ##########
diff igloo.log ../Reference/test_euler_ringleb.log
cd ..

echo '> case euler ringleb done'

exit 0
