#!/bin/sh


echo '> case advection circular ALE deformation ...'

########## cleaning ##############
rm -rf Test_advection_circular_ALE_deformation
mkdir Test_advection_circular_ALE_deformation

cd Test_advection_circular_ALE_deformation

########### run case ##########

../../build/bin/igGenMesh -case advection_circular -mesh mesh.dat  -n1 8 -n2 8 -degree 3 -gauss 5 > igGenMesh.log
../../build/bin/igGenSol -case advection_circular -mesh mesh.dat -initial initial.dat  > igGenSol.log
../../build/bin/igloo -solver advection2D -mesh mesh.dat -initial initial.dat -time 1. -error -integrator rk4 -ale sinusoidal > igloo.log

########### end ##########
diff igloo.log ../Reference/test_advection_circular_ALE_deformation.log
cd ..



echo '> case advection circular ALE deformation done'

exit 0
