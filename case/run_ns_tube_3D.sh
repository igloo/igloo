#!/bin/sh

echo '> case navier-stokes tube 3D ...'

########## cleaning ##############
rm -rf Test_ns_tube_3D
mkdir Test_ns_tube_3D

cd Test_ns_tube_3D

########### run case ##########

../../build/bin/igGenMesh -case ns_tube_3D -mesh mesh.dat -n1 10 -n2 16 -n3 6 -degree 3 -gauss 4 > igGenMesh.log
../../build/bin/igGenSol -case ns_tube_3D -mesh mesh.dat -initial initial.dat -mach 0.3 > igGenSol.log
../../build/bin/igPartit -mesh mesh.dat -n1 10 -n2 16 -n3 6 -npart1 1 -npart2 1 -npart3 2 > igPartit.log

mpirun -np 2 ../../build/bin/igloo -solver navier-stokes -mesh mesh_distributed.dat -initial initial.dat -time 0.001  -cfl 0.5 -mach 0.3 -reynolds 500. -error > igloo.log

../../build/bin/igVtkConverter -proc 2 -mesh_id 0 -file_id 1 -refine 2 -density -momentum -energy > vtk.log
../../build/bin/igVtkConverter -mesh mesh.dat -refine 8 > vtk_surf.log

########### end ##########
diff igloo.log ../Reference/test_ns_tube_3D.log
diff vtk.log ../Reference/test_ns_tube_3D_vtk.log
diff vtk_surf.log ../Reference/test_ns_tube_3D_vtk_surf.log

cd ..

echo '> case navier-stokes tube 3D done'

exit 0
