#!/bin/sh


echo '> case euler vortex AMR ...'

########## cleaning ##############
rm -rf Test_euler_vortex_AMR
mkdir Test_euler_vortex_AMR

cd Test_euler_vortex_AMR

########### run case ##########

../../build/bin/igGenMesh -case euler_vortex -mesh mesh.dat -n1 6 -n2 6 -degree 4 -gauss 5 > gen_mesh.log
../../build/bin/igGenSol -case euler_vortex -mesh mesh.dat -initial initial.dat > gen_sol.log
../../build/bin/igPartit -mesh mesh.dat -npart1 2 > part.log

mpirun -np 2 ../../build/bin/igloo -solver euler -mesh mesh_distributed.dat -initial initial.dat -time 2. -refine_max 3 -refine_coef 2. -coarsen coef 0.5 -error > igloo.log

########### end ##########
diff igloo.log ../Reference/test_euler_vortex_AMR.log
cd ..

echo '> case euler vortex AMR done'

exit 0
