#!/bin/sh


echo '> case acoustic annular ...'

########## cleaning ##############
rm -rf Test_acoustic_annular
mkdir Test_acoustic_annular

cd Test_acoustic_annular

########### run case ##########

../../build/bin/igGenMesh -case acoustic_annular -mesh mesh.dat -n1 8 -n2 8 -degree 3 -gauss 4 > igGenMesh.log
../../build/bin/igGenSol -case acoustic_annular -mesh mesh.dat -initial initial.dat > igGenSol.log
../../build/bin/igloo -solver acoustic -mesh mesh.dat -initial initial.dat -error  -integrator rk4 -cfl 0.7  -time 5. > igloo.log

########### end ##########
diff igloo.log ../Reference/test_acoustic_annular.log
cd ..

echo '> case acoustic_annular done'

exit 0
