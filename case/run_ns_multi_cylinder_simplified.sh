#!/bin/sh


echo '> case navier-stokes multi-patch cylinder simplified ...'

########## cleaning ##############
rm -rf Test_ns_multi_cylinder_simplified
mkdir Test_ns_multi_cylinder_simplified

cd Test_ns_multi_cylinder_simplified

cp ../Reference/baseline_multi_cylinder_simplified.dat ./baseline.dat
cp ../Reference/box_multi_cylinder_simplified.dat ./box.dat

########### run case ##########

../../build/bin/igGenMesh -case ns_multi_patch -mesh mesh.dat -n1 6 -degree 3 -gauss 4 > igGenMesh.log
../../build/bin/igPreElevator -mesh mesh.dat -degree 6 -gauss 7 > igPreElevator.log
../../build/bin/igPreRefiner -mesh mesh_elevated.dat -box ./box.dat > igPreRefiner.log
../../build/bin/igGenSol -case ns_multi_patch -mesh mesh_refined.dat -initial solution_refined.dat -mach 0.1 > igGenSol.log
../../build/bin/igPartit -mesh mesh_refined.dat -npart1 2 > igPartit.log
mpirun -np 2 ../../build/bin/igloo -solver navier-stokes -mesh mesh_distributed.dat -initial solution_refined.dat -time 0.5 -cfl 0.5 -mach 0.1 -reynolds 100. -error > igloo.log

########### end ##########
diff igGenMesh.log ../Reference/test_ns_multi_cylinder_simplified_gen.log
diff igPreElevator.log ../Reference/test_ns_multi_cylinder_simplified_elev.log
diff igPreRefiner.log ../Reference/test_ns_multi_cylinder_simplified_pre.log
diff igPartit.log ../Reference/test_ns_multi_cylinder_simplified_part.log
diff igloo.log ../Reference/test_ns_multi_cylinder_simplified_igloo.log
cd ..

echo '> case navier-stokes multi-patch cylinder simplified done'

exit 0
