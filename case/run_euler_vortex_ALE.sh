#!/bin/sh


echo '> case euler vortex ALE ...'

########## cleaning ##############
rm -rf Test_euler_vortex_ALE
mkdir Test_euler_vortex_ALE

cd Test_euler_vortex_ALE

########### run case ##########

../../build/bin/igGenMesh -case euler_vortex -mesh mesh.dat -n1 16 -n2 16 -degree 3 -gauss 5 > igGenMesh.log
../../build/bin/igGenSol -case euler_vortex -mesh mesh.dat -initial initial.dat > igGenSol.log
../../build/bin/igloo -solver euler -mesh mesh.dat -initial initial.dat -time 2. -integrator rk4 -error -ale sinusoidal > igloo.log

########### end ##########
diff igloo.log ../Reference/test_euler_vortex_ALE.log
cd ..

echo '> case euler vortex ALE done'

exit 0
