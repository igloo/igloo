#!/bin/sh

echo '> case navier-stokes pipe ...'

########## cleaning ##############
rm -rf Test_ns_pipe
mkdir Test_ns_pipe

cd Test_ns_pipe

########### run case ##########

../../build/bin/igGenMesh -case ns_pipe -mesh mesh.dat -n1 16 -n2 8 -degree 3 -gauss 5 > igGenMesh.log
../../build/bin/igGenSol -case ns_pipe -mesh mesh.dat -initial initial.dat > igGenSol.log
../../build/bin/igPartit -mesh mesh.dat -npart2 2 -n1 16 -n2 8 > igPartit.log
mpirun -n 2 ../../build/bin/igloo -solver navier-stokes -mesh mesh_distributed.dat -initial initial.dat -time 0.5 -error > igloo.log

########### end ##########
diff igloo.log ../Reference/test_ns_pipe.log
cd ..

echo '> case navier-stokes pipe done'

exit 0
