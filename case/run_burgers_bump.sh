#!/bin/sh


echo '> case burgers bump ...'

########## cleaning ##############
rm -rf Test_burgers_bump
mkdir Test_burgers_bump

cd Test_burgers_bump

########### run case ##########

../../build/bin/igGenMesh -case burgers_bump -mesh mesh.dat -n1 32 -degree 3 -gauss 5 > igGenMesh.log
../../build/bin/igGenSol -case burgers_bump -mesh mesh.dat -initial initial.dat > igGenSol.log
../../build/bin/igloo -solver burger -mesh mesh.dat -initial initial.dat -time 0.5 -integrator rk4 -error -shock 1. > igloo.log

########### end ##########
diff igloo.log ../Reference/test_burgers_bump.log
cd ..

echo '> case burgers bump done'

exit 0
