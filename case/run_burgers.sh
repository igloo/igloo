#!/bin/sh

echo ' '
echo '---- Burgers cases ----'
echo ' '
./run_burgers_bump.sh
echo ' '
./run_viscous_burgers_shock.sh

exit 0
