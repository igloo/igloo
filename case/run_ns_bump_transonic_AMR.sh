#!/bin/sh


echo '> case navier-stokes bump transonic AMR ...'

########## cleaning ##############
rm -rf Test_ns_bump_transonic_AMR
mkdir Test_ns_bump_transonic_AMR

cd Test_ns_bump_transonic_AMR

########### run case ##########

../../build/bin/igGenMesh -case euler_bump_cos -mesh mesh.dat -n1 8 -n2 4 -degree 3 -gauss 5 > igGenMesh.log
../../build/bin/igGenSol -case euler_bump_cos -mesh mesh.dat -initial initial.dat -mach 0.65 > igGenSol.log
../../build/bin/igPartit -mesh mesh.dat -npart1 2 > part.log

mpirun -np 2 ../../build/bin/igloo -solver navier-stokes -mesh mesh_distributed.dat -initial initial.dat -time 0.5 -integrator ssp -mach 0.65 -reynolds 1.E15 -shock 0.5 -cfl 0.7 -refine_coef 2. -coarsen_coef 0.5 -refine_max 3 > igloo.log

########### end ##########
diff igloo.log ../Reference/test_ns_bump_transonic_AMR.log
cd ..

echo '> case navier-stokes bump transonic AMR done'

exit 0
