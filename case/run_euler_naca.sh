#!/bin/sh


echo '> case euler naca ...'

########## cleaning ##############
rm -rf Test_euler_naca
mkdir Test_euler_naca

cd Test_euler_naca

########### run case ##########

../../build/bin/igGenMesh -case euler_naca -mesh mesh.dat -n1 4 -n2 16 -degree 2 -gauss 4 -alpha 2. > igGenMesh.log
../../build/bin/igGenSol -case euler_naca -mesh mesh.dat -initial initial.dat > igGenSol.log
../../build/bin/igloo -solver euler -mesh mesh.dat -initial initial.dat -time 0.5 -error > igloo.log

########### end ##########
diff igloo.log ../Reference/test_euler_naca.log
cd ..

echo '> case euler naca done'

exit 0
