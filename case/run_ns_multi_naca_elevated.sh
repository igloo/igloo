#!/bin/sh


echo '> case navier-stokes multi-patch naca with elevation ...'

########## cleaning ##############
rm -rf Test_ns_multi_naca_elevated
mkdir Test_ns_multi_naca_elevated

cd Test_ns_multi_naca_elevated

cp ../Reference/baseline_multi_naca.dat baseline.dat
cp ../Reference/box_multi_naca_elevated.dat box.dat

########### run case ##########

../../build/bin/igGenMesh -case ns_multi_patch -mesh mesh.dat -n1 38 -degree 3 -gauss 4 > igGenMesh.log
../../build/bin/igPreElevator -mesh mesh.dat -degree 6 -gauss 7 > igPreElevator.log
../../build/bin/igPreRefiner -mesh mesh_elevated.dat -box box.dat > igPreRefiner.log
../../build/bin/igGenSol -case ns_multi_patch -mesh mesh_refined.dat -initial solution_refined.dat > igGenSol.log
../../build/bin/igPartit -mesh mesh_refined.dat -npart1 2 > igPartit.log
mpirun -np 2 ../../build/bin/igloo -solver navier-stokes -mesh mesh_distributed.dat -initial solution_refined.dat -time 0.01 -reynolds 1000. -cfl 0.5 -error > igloo.log

########### end ##########

diff igGenMesh.log ../Reference/test_ns_multi_naca_elevated_gen_mesh_h.log
diff igPreElevator.log ../Reference/test_ns_multi_naca_elevated_pre_p.log
diff igPreRefiner.log ../Reference/test_ns_multi_naca_elevated_pre_h.log
diff igGenSol.log ../Reference/test_ns_multi_naca_elevated_gen_sol.log
diff igPartit.log ../Reference/test_ns_multi_naca_elevated_part.log
diff igloo.log ../Reference/test_ns_multi_naca_elevated_igloo.log
cd ..

echo '> case navier-stokes multi-patch naca with elevation done'

exit 0
