#!/bin/sh


echo '> case navier-stokes shock-vortex interaction ...'

########## cleaning ##############
rm -rf Test_ns_shock_vortex
mkdir Test_ns_shock_vortex

cd Test_ns_shock_vortex

########### run case ##########

../../build/bin/igGenMesh -case euler_shock_vortex -mesh mesh.dat -n1 40 -n2 40 -degree 3 -gauss 5 > igGenMesh.log
../../build/bin/igGenSol -case euler_shock_vortex -mesh mesh.dat -initial initial.dat > igGenSol.log
../../build/bin/igPartit -mesh mesh.dat -npart1 2 -n1 40 -n2 40 > igPartit.log
mpirun -n 2 ../../build/bin/igloo -solver navier-stokes -mesh mesh_distributed.dat -initial initial.dat -error -time 0.05 -reynolds 1.E15 -shock 1. > igloo.log

########### end ##########
diff igloo.log ../Reference/test_ns_shock_vortex.log
cd ..

echo '> case navier-stokes shock-vortex interaction done'

exit 0
