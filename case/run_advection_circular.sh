#!/bin/sh


echo '> case advection circular ...'

########## cleaning ##############
rm -rf Test_advection_circular
mkdir Test_advection_circular

cd Test_advection_circular

########### run case ##########
../../build/bin/igGenMesh -case advection_circular -mesh mesh.dat -n1 8 -n2 8 -degree 5 -gauss 7 > igGenMesh.log
../../build/bin/igGenSol -case advection_circular -mesh mesh.dat -initial initial.dat > igGenSol.log
../../build/bin/igloo -solver advection2D -mesh mesh.dat -initial initial.dat -time 1. -error -integrator rk4 > igloo.log

########### end ##########
diff igloo.log ../Reference/test_advection_circular.log
cd ..



echo '> case advection circular done'

exit 0
