#!/bin/sh


echo ' '
echo '---- Fluid-structure cases ----'
echo ' '
./run_ns_spring_naca.sh
echo ' '
./run_membrane.sh
echo ' '
./run_ns_membrane.sh
echo ' '
./run_ns_membrane_AMR.sh


exit 0
