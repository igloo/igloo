#!/bin/sh


echo '> case navier-stokes multi-element airfoil ...'

########## cleaning ##############
rm -rf Test_ns_multi_airfoil
mkdir Test_ns_multi_airfoil

cd Test_ns_multi_airfoil

cp ../Reference/baseline_multi_airfoil.dat ./baseline.dat

########### run case ##########

../../build/bin/igGenMesh -case ns_multi_patch -mesh mesh.dat -n1 27 -degree 3 -gauss 4 > igGenMesh.log
../../build/bin/igPreElevator -mesh mesh.dat -degree 4 -gauss 5 > igPreElevator.log
../../build/bin/igMeshDeform  -mesh mesh_elevated.dat -rotation -alpha -3. -r0 10. -r1 20. > igMeshDeform.log
../../build/bin/igPreRefiner -mesh mesh_deformed.dat -global 5 > igPreRefiner.log
../../build/bin/igSmoother -mesh mesh_refined.dat -step 10 -weighted > igSmoother.log
../../build/bin/igGenSol -case ns_multi_patch -mesh mesh_smoothed.dat -initial solution.dat -mach 0.3 > igGenSol.log
../../build/bin/igPartit -mesh mesh_smoothed.dat -npart1 2 > igPartit.log

mpirun -np 2 ../../build/bin/igloo -solver navier-stokes -mesh mesh_distributed.dat -initial solution.dat -time 0.01 -mach 0.3 -cfl 0.5 -reynolds 3000 -error > igloo.log



########### end ##########
diff igPreRefiner.log ../Reference/test_ns_multi_airfoil_pre.log
diff igloo.log ../Reference/test_ns_multi_airfoil_igloo.log
cd ..

echo '> case navier-stokes multi-element airfoil done'

exit 0
