#!/bin/sh


echo '> case navier-stokes naca ALE gradual ...'

########## cleaning ##############
rm -rf Test_ns_naca_ALE_gradual
mkdir Test_ns_naca_ALE_gradual

cd Test_ns_naca_ALE_gradual

cp ../Reference/baseline_multi_naca.dat baseline.dat
cp ../Reference/box_multi_naca.dat box.dat

########### run case ##########

../../build/bin/igGenMesh -case ns_multi_patch -mesh mesh.dat -n1 38 -degree 3 -gauss 5 > igGenMesh.log
../../build/bin/igPreRefiner -mesh mesh.dat -initial solution.dat -box box.dat > igPreRefiner.log
../../build/bin/igGenSol -case ns_multi_patch -mesh mesh_refined.dat -initial solution_refined.dat > igGenSol.log
../../build/bin/igloo -solver navier-stokes -mesh mesh_refined.dat -initial solution_refined.dat -time 0.01 -reynolds 1000. -cfl 0.5 -error -ale gradual > igloo.log

########### end ##########
diff igPreRefiner.log ../Reference/test_ns_naca_gradual_pre.log
diff igloo.log ../Reference/test_ns_naca_gradual_igloo.log
cd ..

echo '> case navier-stokes naca ALE gradual done'

exit 0
