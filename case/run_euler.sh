#!/bin/sh


echo ' '
echo '---- Euler cases ----'
echo ' '
./run_euler_vortex.sh
echo ' '
./run_euler_vortex_ALE.sh
echo ' '
./run_euler_vortex_ALE_refined.sh
echo ' '
./run_euler_vortex_AMR.sh
echo ' '
./run_euler_vortex_sliding.sh
echo ' '
./run_euler_cylinder.sh
echo ' '
./run_euler_cylinder_nurbs.sh
echo ' '
./run_euler_ringleb.sh
echo ' '
./run_euler_bump.sh
echo ' '
./run_euler_bump_3D.sh
echo ' '
./run_euler_naca.sh
echo ' '
./run_euler_naca_3D.sh
echo ' '
./run_euler_naca_AMR.sh
echo ' '
./run_euler_naca_refined.sh

exit 0
