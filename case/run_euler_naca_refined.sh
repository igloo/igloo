#!/bin/sh


echo '> case euler naca refined ...'

########## cleaning ##############
rm -rf Test_euler_naca_refined
mkdir Test_euler_naca_refined

cd Test_euler_naca_refined

########### run case ##########

../../build/bin/igGenMesh -case euler_naca -mesh mesh_ini.dat -n1 6 -n2 8 -degree 3 -gauss 4 -alpha 2. > igGenMesh.log
../../build/bin/igPreRefiner -mesh mesh_ini.dat -box ../Reference/box_naca.dat > igPreRefiner.log
../../build/bin/igGenSol -case euler_naca -mesh mesh_refined.dat -initial solution_refined.dat > igGenSol.log
../../build/bin/igloo -solver euler -mesh mesh_refined.dat -initial solution_refined.dat -time 0.2 -cfl 0.8 -positivity -error > igloo.log

########### end ##########
diff igPreRefiner.log ../Reference/test_euler_naca_refined_pre.log
diff igloo.log ../Reference/test_euler_naca_refined_igloo.log
cd ..

echo '> case euler naca refined done'

exit 0
