#!/bin/sh


echo ' '
echo '---- Navier-Stokes cases ----'
echo ' '
./run_ns_plate.sh
echo ' '
./run_ns_cylinder.sh
echo ' '
./run_ns_pipe.sh
echo ' '
./run_ns_bump_transonic.sh
echo ' '
./run_ns_shock_vortex.sh
echo ' '
./run_ns_bump_transonic_AMR.sh
echo ' '
./run_ns_multi_airfoil.sh
echo ' '
./run_ns_naca_ALE_gradual.sh
echo ' '
./run_ns_multi_naca_elevated.sh
echo ' '
./run_ns_multi_cylinder.sh
echo ' '
./run_ns_multi_cylinder_simplified.sh
echo ' '
./run_ns_morphing.sh
echo ' '
./run_ns_tube_3D.sh
echo ' '
./run_ns_vawt.sh
echo ' '
./run_ns_extrude_naca_3D.sh

exit 0
