#!/bin/sh


echo '> case navier-stokes bump transonic ...'

########## cleaning ##############
rm -rf Test_ns_bump_transonic
mkdir Test_ns_bump_transonic

cd Test_ns_bump_transonic

########### run case ##########

../../build/bin/igGenMesh -case euler_bump_cos -mesh mesh.dat -n1 20 -n2 10 -degree 3 -gauss 4 > igGenMesh.log
../../build/bin/igGenSol -case euler_bump_cos -mesh mesh.dat -initial initial.dat -mach 0.65 > igGenSol.log
../../build/bin/igPartit -mesh mesh.dat -npart2 2 -n1 20 -n2 10 > igPartit.log
mpirun -np 2 ../../build/bin/igloo -solver navier-stokes -mesh mesh_distributed.dat -initial initial.dat -time 1. -error -mach 0.65 -reynolds 1.E15 -shock 1. -viscosity_smoothing 3 > igloo.log

########### end ##########
diff igloo.log ../Reference/test_ns_bump_transonic.log
cd ..

echo '> case navier-stokes bump transonic done'

exit 0
