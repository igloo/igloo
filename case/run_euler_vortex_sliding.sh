#!/bin/sh


echo '> case euler vortex sliding ...'

########## cleaning ##############
rm -rf Test_euler_vortex_sliding
mkdir Test_euler_vortex_sliding

cd Test_euler_vortex_sliding

cp ../Reference/baseline_multi_vortex.dat ./baseline.dat
cp ../Reference/box_multi_vortex.dat ./box.dat

########### run case ##########

../../build/bin/igGenMesh -case euler_vortex_sliding -mesh mesh.dat -n1 9 -degree 2 -gauss 4  -subdomains > igGenMesh.log
../../build/bin/igPreRefiner -mesh mesh.dat -box ./box.dat -sliding > igPreRefiner.log
../../build/bin/igGenSol -case euler_vortex_sliding -mesh mesh_refined.dat -initial initial_refined.dat > igGenSol.log
../../build/bin/igloo -solver euler -mesh mesh_refined.dat -initial initial_refined.dat -time 1.5 -integrator ssp -error -ale sliding > igloo.log

########### end ##########
diff igGenMesh.log ../Reference/test_euler_vortex_sliding_gen_mesh.log
diff igPreRefiner.log ../Reference/test_euler_vortex_sliding_pre.log
diff igGenSol.log ../Reference/test_euler_vortex_sliding_gen_sol.log
diff igloo.log ../Reference/test_euler_vortex_sliding_igloo.log
cd ..

echo '> case euler vortex sliding done'

exit 0
