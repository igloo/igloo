#!/bin/sh


echo '> case viscous burgers shock ...'

########## cleaning ##############
rm -rf Test_viscous_burgers_shock
mkdir Test_viscous_burgers_shock

cd Test_viscous_burgers_shock

########### run case ##########

../../build/bin/igGenMesh -case viscous_burgers -mesh mesh.dat -n1 16 -degree 3 -gauss 5  > igGenMesh.log
../../build/bin/igGenSol -case viscous_burgers -mesh mesh.dat -initial initial.dat  > igGenSol.log
../../build/bin/igloo -solver viscous_burger -mesh mesh.dat -initial initial.dat -time 0.5 -integrator rk4 -error -cfl 0.25 > igloo.log

########### end ##########
diff igloo.log ../Reference/test_viscous_burgers_shock.log
cd ..

echo '> case viscous burgers shock done'

exit 0
