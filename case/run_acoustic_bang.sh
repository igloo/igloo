#!/bin/sh


echo '> case acoustic bang ...'

########## cleaning ##############
rm -rf Test_acoustic_bang
mkdir Test_acoustic_bang

cd Test_acoustic_bang

########### run case ##########

../../build/bin/igGenMesh -case acoustic_bang -mesh mesh.dat -n1 16 -n2 32 -degree 3 -gauss 4 > igGenMesh.log
../../build/bin/igGenSol -case acoustic_bang -mesh mesh.dat -initial initial.dat > igGenSol.log
../../build/bin/igloo -solver acoustic -mesh mesh.dat -initial initial.dat -error  -integrator rk4 -cfl 0.5  -time 2. > igloo.log

########### end ##########
diff igloo.log ../Reference/test_acoustic_bang.log
cd ..

echo '> case acoustic_bang done'

exit 0
