#!/bin/sh


echo '> case membrane ...'

########## cleaning ##############
rm -rf Test_membrane
mkdir Test_membrane

cd Test_membrane

########### case paraemters #########

NPT=17
DEGREE=3
COEF=1.2
RATIO=1
CLAMP=2

########### run case ###############

GAUSS=`echo print $DEGREE + 1 | perl`


../../build/bin/igGenCurve -case membrane -degree $DEGREE -pts $NPT -coef $COEF -ratio $RATIO -clamp $CLAMP > gen_curve.log
../../build/bin/igGenMesh -case ns_membrane -mesh mesh.dat  -degree $DEGREE -gauss $GAUSS -clamp $CLAMP > gen_mesh.log
../../build/bin/igMembrane -time 3  -mesh mesh.dat > membrane.log

########### end ##########
diff membrane.log ../Reference/test_membrane.log

cd ..

echo '> case membrane done'

exit 0
