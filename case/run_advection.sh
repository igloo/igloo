#!/bin/sh


echo ' '
echo '---- Advection cases ----'
echo ' '
./run_advection_circular.sh
echo ' '
./run_advection_circular_AMR.sh
echo ' '
./run_advection_circular_ALE_rigid.sh
echo ' '
./run_advection_circular_ALE_deformation.sh
echo ' '
./run_advection_circular_AMR_ALE_deformation.sh

exit 0
