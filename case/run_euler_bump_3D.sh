#!/bin/sh


echo '> case euler bump 3D ...'

########## cleaning ##############
rm -rf Test_euler_bump_3D
mkdir Test_euler_bump_3D

cd Test_euler_bump_3D

########### run case ##########

../../build/bin/igGenMesh -case euler_bump_exp_3D -mesh mesh.dat -n1 12 -n2 6 -n3 6 -degree 3 -gauss 4 > igGenMesh.log
../../build/bin/igGenSol -case euler_bump_exp_3D -mesh mesh.dat -initial initial.dat -mach 0.3 > igGenSol.log
../../build/bin/igloo -solver euler -mesh mesh.dat -initial initial.dat -time 0.05 -cfl 0.5 -mach 0.3 -error > igloo.log

########### end ##########
diff igloo.log ../Reference/test_euler_bump_3D.log
cd ..

echo '> case euler 3D bump done'

exit 0
