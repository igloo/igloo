#!/bin/sh

echo '-> tests starting '
echo ' '
./run_burgers.sh
./run_advection.sh
./run_acoustic.sh
./run_euler.sh
./run_ns.sh
./run_fsi.sh
echo ' '
echo '-> tests ending '

exit 0
