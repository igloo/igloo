#!/bin/sh


echo '> case navier-stokes spring naca ...'

########## cleaning ##############
rm -rf Test_ns_spring_naca
mkdir Test_ns_spring_naca

cd Test_ns_spring_naca

cp ../Reference/baseline_multi_naca_spring.dat baseline.dat
cp ../Reference/box_multi_naca_spring.dat box.dat

########### run case ##########

../../build/bin/igGenMesh -case ns_multi_patch -mesh mesh.dat -n1 38 -degree 3 -gauss 4 > igGenMesh.log
../../build/bin/igPreRefiner -mesh mesh.dat -box box.dat > igPreRefiner.log
../../build/bin/igMeshDeform  -mesh mesh_refined.dat -rotation -alpha -5 -r0 2. -r1 7. > igMeshDeform1.log
../../build/bin/igMeshDeform  -mesh mesh_deformed.dat -translation -tx -0.5  > igMeshDeform2.log
../../build/bin/igGenSol -case ns_multi_patch -mesh mesh_deformed.dat -initial solution.dat > igGenSol.log
../../build/bin/igPartit -mesh mesh_deformed.dat -npart1 2 > igPartit.log

mpirun -np 2 ../../build/bin/igloo -solver navier-stokes -mesh mesh_distributed.dat -initial solution.dat -structure spring -time 0.01 -reynolds 1000. -cfl 0.5 -error -save_period 0.01 > igloo.log


########### end ##########
diff igloo.log ../Reference/test_ns_spring_naca.log

cd ..

echo '> case navier-stokes spring naca done'

exit 0
