#!/bin/sh


echo '> case navier-stokes morphing ...'

########## cleaning ##############
rm -rf Test_ns_morphing
mkdir Test_ns_morphing

cd Test_ns_morphing

cp ../Reference/baseline_morphing.dat ./baseline.dat
cp ../Reference/box_morphing.dat ./box.dat
cp ../Reference/spline_morphing.dat ./spline.dat

########### run case ##########

../../build/bin/igGenMesh -case ns_multi_patch -mesh mesh.dat -n1 65 -degree 3 -gauss 5  -subdomains > igGenMesh.log
../../build/bin/igGenMorphing -mesh mesh.dat -spline spline.dat -length 0.5 -amplitude 0.04 -alpha 0. > igGenMorphing.log
../../build/bin/igPreRefiner -mesh mesh.dat -box box.dat -deformation deformation.dat > igPreRefiner.log
../../build/bin/igGenSol -case ns_multi_patch -mesh mesh_refined.dat -initial solution_refined.dat -mach 0.2 > igGenSol.log
../../build/bin/igloo -solver navier-stokes -mesh mesh_refined.dat -initial solution_refined.dat -cfl 0.5 -time 0.002 -mach 0.2 -reynolds 100. -ale morphing -ale_param 0.2 -deformation deformation_refined.dat -error > igloo.log

########### end ##########
diff igGenMesh.log ../Reference/test_ns_morphing_gen.log
diff igGenMorphing.log ../Reference/test_ns_morphing_morph.log
diff igPreRefiner.log ../Reference/test_ns_morphing_pre.log
diff igloo.log ../Reference/test_ns_morphing_igloo.log
cd ..

echo '> case navier-stokes morphing done'

exit 0
