#!/bin/sh


echo '> case advection circular AMR ...'

########## cleaning ##############
rm -rf Test_advection_circular_AMR
mkdir Test_advection_circular_AMR

cd Test_advection_circular_AMR

########### run case ##########

../../build/bin/igGenMesh -case advection_circular -mesh mesh.dat -n1 8 -n2 8 -degree 2 -gauss 3 > igGenMesh.log
../../build/bin/igGenSol -case advection_circular -mesh mesh.dat -initial initial.dat > igGenSol.log
../../build/bin/igloo -solver advection2D -mesh mesh.dat -initial initial.dat -time 0.25 -error -integrator rk4 -refine_max 3 -refine_coef 2 -coarsen_coef 0.5 > igloo.log

########### end ##########
diff igloo.log ../Reference/test_advection_circular_AMR.log
cd ..



echo '> case advection circular AMR done'

exit 0
