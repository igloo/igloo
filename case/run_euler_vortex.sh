#!/bin/sh


echo '> case euler vortex ...'

########## cleaning ##############
rm -rf Test_euler_vortex
mkdir Test_euler_vortex

cd Test_euler_vortex

########### run case ##########

../../build/bin/igGenMesh -case euler_vortex -mesh mesh.dat -n1 16 -n2 16 -degree 3 -gauss 5 > igGenMesh.log
../../build/bin/igGenSol -case euler_vortex -mesh mesh.dat -initial initial.dat > igGenSol.log
../../build/bin/igloo -solver euler -mesh mesh.dat -initial initial.dat -time 2. -integrator rk4 -error > igloo.log

########### end ##########
diff igloo.log ../Reference/test_euler_vortex.log
cd ..

echo '> case euler vortex done'

exit 0
