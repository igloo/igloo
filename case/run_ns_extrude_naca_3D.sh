#!/bin/sh


echo '> case navier-stokes extrude naca 3D ...'

########## cleaning ##############
rm -rf Test_ns_extrude_naca3D
mkdir Test_ns_extrude_naca3D

cd Test_ns_extrude_naca3D

cp ../Reference/baseline_extrude_naca3D.dat ./baseline.dat
cp ../Reference/box_extrude_naca3D.dat ./box.dat

###################################

../../build/bin/igGenMesh -case ns_multi_patch -mesh mesh.dat -n1 60 -degree 3 -gauss 4 > igGenMesh.log
../../build/bin/igPreRefiner -mesh mesh.dat -initial solution.dat -box box.dat > igPreRefiner.log
../../build/bin/igMeshDeform  -mesh mesh_refined.dat -rotation -alpha -8. -r0 50. -r1 50. > igMeshDeform.log
../../build/bin/igExtruder -mesh mesh_deformed.dat -layer 6 -translate 2. -scale 0.1 -periodic > igExtruder.log
../../build/bin/igGenSol -case ns_extrude -mesh mesh_extruded.dat -initial solution.dat  > igSolGenerator.log
../../build/bin/igPartit -mesh mesh_extruded.dat -npart1 2 > igPartit.log

mpirun -np 2 ../../build/bin/igloo -solver navier-stokes -mesh mesh_distributed.dat -initial solution.dat -time 0.01 -reynolds 1000. -cfl 0.75 -error > igloo.log


########### end ##########

diff igGenMesh.log ../Reference/test_ns_extrude_naca3D_gen.log
diff igPreRefiner.log ../Reference/test_ns_extrude_naca3D_pre.log
diff igExtruder.log ../Reference/test_ns_extrude_naca3D_extr.log
diff igloo.log ../Reference/test_ns_extrude_naca3D_igloo.log

cd ..

echo '> case navier-stokes extrude naca 3D done'

exit 0
