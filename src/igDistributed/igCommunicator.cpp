/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igCommunicator.h"

#include <igCore/igAssert.h>

#include <fstream>
#include <iostream>

#include <mpi.h>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igCommunicator::igCommunicator(void)
{
    this->partition_map = nullptr;

    this->shared_dof_map = nullptr;
    this->send_dof_map = nullptr;
    this->receive_dof_map = nullptr;

    this->shared_elt_map = nullptr;
    this->send_elt_map = nullptr;
    this->receive_elt_map = nullptr;

    this->buffer_dof_send = nullptr;
    this->buffer_dof_recv = nullptr;
    this->buffer_elt_send = nullptr;
    this->buffer_elt_recv = nullptr;
}

igCommunicator::~igCommunicator(void)
{
    int neighbor_number = 0;

    if (partition_map)
        neighbor_number = partition_map->size();

    for (int ineigh = 0; ineigh < neighbor_number; ++ineigh) {
        delete buffer_dof_send[ineigh];
        delete buffer_dof_recv[ineigh];
        delete buffer_elt_send[ineigh];
        delete buffer_elt_recv[ineigh];
    }

    if(buffer_dof_send)
        delete[] buffer_dof_send;
    if(buffer_dof_recv)
        delete[] buffer_dof_recv;
    if(buffer_elt_send)
        delete[] buffer_elt_send;
    if(buffer_elt_recv)
        delete[] buffer_elt_recv;
}

////////////////////////////////////////////////////////////////////////

void igCommunicator::initialize(void)
{
    MPI_Init(NULL, NULL);

    // Get the number of processes
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // Get the rank of the process
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    if (world_rank == 0) {
        cout << "MPI initialized" << endl;
        cout << "Number of processes: " << world_size << endl;
    }
}

void igCommunicator::finalize(void)
{
    if (world_rank == 0) {
        cout << "MPI finalized" << endl;
    }
    MPI_Finalize();
}

//////////////////////////////////////////////////////////////////////////

int igCommunicator::rank(void)
{
    return this->world_rank;
}

int igCommunicator::size(void)
{
    return this->world_size;
}

////////////////////////////////////////////////////////////////////////

void igCommunicator::setPartitionMap(vector<int> *map)
{
    IG_ASSERT(map, "igCommunicator::setPartitionMap(): input map is null.");

    this->partition_map = map;
}

void igCommunicator::setSharedDofMap(vector<int> *map)
{
    IG_ASSERT(map, "igCommunicator::setSharedDofMap(): input map is null.");

    this->shared_dof_map = map;
}

void igCommunicator::setSendDofMap(vector<vector<int> *> *map)
{
    IG_ASSERT(map, "igCommunicator::setSendDofMap(): input map is null.");

    this->send_dof_map = map;
}

void igCommunicator::setReceiveDofMap(vector<vector<int> *> *map)
{
    IG_ASSERT(map, "igCommunicator::setReceiveDofMap(): input map is null.");

    this->receive_dof_map = map;
}

void igCommunicator::setSharedEltMap(vector<int> *map)
{
    IG_ASSERT(map, "igCommunicator::setSharedEltMap(): input map is null.");

    this->shared_elt_map = map;
}

void igCommunicator::setSendEltMap(vector<vector<int> *> *map)
{
    IG_ASSERT(map, "igCommunicator::setSendEltMap(): input map is null.");

    this->send_elt_map = map;
}

void igCommunicator::setReceiveEltMap(vector<vector<int> *> *map)
{
    IG_ASSERT(map, "igCommunicator::setReceiveEltMap(): input map is null.");

    this->receive_elt_map = map;
}

/////////////////////////////////////////////////////////////////////

void igCommunicator::initializeDofBuffers(int field_number)
{
    int neighbor_number = partition_map->size();

    buffer_dof_send = new double*[neighbor_number];
    buffer_dof_recv = new double*[neighbor_number];
    for (int ineigh = 0; ineigh < neighbor_number; ++ineigh) {
        buffer_dof_send[ineigh] = new double[shared_dof_map->at(ineigh)*field_number];
        buffer_dof_recv[ineigh] = new double[shared_dof_map->at(ineigh)*field_number];
    }
}

void igCommunicator::initializeEltBuffers(void)
{
    int neighbor_number = partition_map->size();

    buffer_elt_send = new int*[neighbor_number];
    buffer_elt_recv = new int*[neighbor_number];
    for (int ineigh = 0; ineigh < neighbor_number; ++ineigh) {
        buffer_elt_send[ineigh] = new int[shared_elt_map->at(ineigh)];
        buffer_elt_recv[ineigh] = new int[shared_elt_map->at(ineigh)];
    }
}

/////////////////////////////////////////////////////////////////////

void igCommunicator::resizeDofBuffers(int field_number)
{
    int neighbor_number = partition_map->size();

    for (int ineigh = 0; ineigh < neighbor_number; ++ineigh) {
        delete buffer_dof_send[ineigh];
        delete buffer_dof_recv[ineigh];
        buffer_dof_send[ineigh] = new double[shared_dof_map->at(ineigh)*field_number];
        buffer_dof_recv[ineigh] = new double[shared_dof_map->at(ineigh)*field_number];
    }
}

void igCommunicator::resizeEltBuffers(void)
{
    int neighbor_number = partition_map->size();

    for (int ineigh = 0; ineigh < neighbor_number; ++ineigh) {
        delete buffer_elt_send[ineigh];
        delete buffer_elt_recv[ineigh];
        buffer_elt_send[ineigh] = new int[shared_elt_map->at(ineigh)];
        buffer_elt_recv[ineigh] = new int[shared_elt_map->at(ineigh)];
    }
}


////////////////////////////////////////////////////////////////////////

double igCommunicator::distributeTimeStep(double local_time_step)
{
    double global_time_step;

    MPI_Allreduce( &local_time_step, &global_time_step, 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);

    return global_time_step;
}

double igCommunicator::distributeError(double local_error)
{
    double global_error;

    MPI_Allreduce( &local_error, &global_error, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

    return global_error;
}

double igCommunicator::distributeEffort(double local_effort)
{
    double global_effort;

    MPI_Allreduce( &local_effort, &global_effort, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

    return global_effort;
}

int igCommunicator::addInt(int number)
{
    int global_number;
    MPI_Allreduce( &number, &global_number, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
    return global_number;
}

int igCommunicator::distributeGlobalIdCounter(int my_counter, int my_rank)
{
    MPI_Bcast( &my_counter, 1, MPI_INT, my_rank, MPI_COMM_WORLD);
    return my_counter;
}

void igCommunicator::distributeInterfaceDouble(double *interface_local, double *interface_global, int total_length)
{
    MPI_Allreduce( interface_local, interface_global, total_length, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
}

void igCommunicator::distributeInterfaceInt(int *counter_local, int *counter_global, int total_length)
{
    MPI_Allreduce( counter_local, counter_global, total_length, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
}

//////////////////////////////////////////////////////////////////////

void igCommunicator::distributeField(vector<double> *state, int field_number)
{
    IG_ASSERT(state, "igCommunicator::distributeField(): input state is null.");

    int neighbor_number = 0;
    if(partition_map)
        neighbor_number = partition_map->size();

    MPI_Request request[neighbor_number];
    MPI_Status status[neighbor_number];

    //------------------- receive --------------------------

    for (int ineigh = 0; ineigh < neighbor_number; ++ineigh) {

        int neighbor_id = partition_map->at(ineigh);
        int buffer_size = shared_dof_map->at(ineigh)*field_number;

        MPI_Irecv(buffer_dof_recv[ineigh], buffer_size, MPI_DOUBLE, neighbor_id, 0, MPI_COMM_WORLD, &request[ineigh]);
    }

    //------------------- send --------------------------

    for (int ineigh = 0; ineigh < neighbor_number; ++ineigh) {

        int neighbor_id = partition_map->at(ineigh);
        int buffer_size = shared_dof_map->at(ineigh)*field_number;

        for (int i = 0; i < buffer_size; ++i) {
            int index = send_dof_map->at(ineigh)->at(i);
            buffer_dof_send[ineigh][i] = state->at(index);
        }

        MPI_Send(buffer_dof_send[ineigh], buffer_size, MPI_DOUBLE, neighbor_id, 0, MPI_COMM_WORLD);
    }

    //------------------- synchronize --------------------------

    for (int ineigh = 0; ineigh < neighbor_number; ++ineigh) {

        int neighbor_id = partition_map->at(ineigh);
        int buffer_size = shared_dof_map->at(ineigh)*field_number;

        int ierr = MPI_Wait(&request[ineigh],&status[ineigh]);

        for (int i = 0; i < buffer_size; ++i) {
            int index = receive_dof_map->at(ineigh)->at(i);
            state->at(index) = buffer_dof_recv[ineigh][i];
        }
    }
}

//////////////////////////////////////////////////////////////////////////////

void igCommunicator::distributeElementArrayInt(vector<int> *array)
{
    IG_ASSERT(array, "igCommunicator::distributeElementArrray(): input state is null.");

    int neighbor_number = 0;
    if(partition_map)
        neighbor_number = partition_map->size();

    MPI_Request request[neighbor_number];
    MPI_Status status[neighbor_number];

    //------------------- receive --------------------------

    for (int ineigh = 0; ineigh < neighbor_number; ++ineigh) {

        int neighbor_id = partition_map->at(ineigh);
        int buffer_size = shared_elt_map->at(ineigh);

        MPI_Irecv(buffer_elt_recv[ineigh], buffer_size, MPI_INT, neighbor_id, 0, MPI_COMM_WORLD, &request[ineigh]);
    }

    //------------------- send --------------------------

    for (int ineigh = 0; ineigh < neighbor_number; ++ineigh){

        int neighbor_id = partition_map->at(ineigh);
        int buffer_size = shared_elt_map->at(ineigh);

        for (int i = 0; i < buffer_size; ++i) {
            int index = send_elt_map->at(ineigh)->at(i);
            buffer_elt_send[ineigh][i] = array->at(index);
        }

        MPI_Send(buffer_elt_send[ineigh], buffer_size, MPI_INT, neighbor_id, 0, MPI_COMM_WORLD);
    }

    //------------------- synchronize --------------------------

    for (int ineigh = 0; ineigh < neighbor_number; ++ineigh) {

        int neighbor_id = partition_map->at(ineigh);
        int buffer_size = shared_elt_map->at(ineigh);

        int ierr = MPI_Wait(&request[ineigh],&status[ineigh]);

        for (int i = 0; i < buffer_size; ++i) {
            int index = receive_elt_map->at(ineigh)->at(i);
            array->at(index) = buffer_elt_recv[ineigh][i];
        }
    }
}


//////////////////////////////////////////////////////////////////////////////

void igCommunicator::addElementArrayInt(vector<int> *array)
{
    IG_ASSERT(array, "igCommunicator::addElementArrray(): input state is null.");

    int neighbor_number = 0;
    if(partition_map)
        neighbor_number = partition_map->size();

    MPI_Request request[neighbor_number];
    MPI_Status status[neighbor_number];

    //------------------- receive --------------------------

    for (int ineigh = 0; ineigh < neighbor_number; ++ineigh) {

        int neighbor_id = partition_map->at(ineigh);
        int buffer_size = shared_elt_map->at(ineigh);

        MPI_Irecv(buffer_elt_recv[ineigh], buffer_size, MPI_INT, neighbor_id, 0, MPI_COMM_WORLD, &request[ineigh]);
    }

    //------------------- send --------------------------

    for (int ineigh = 0; ineigh < neighbor_number; ++ineigh){

        int neighbor_id = partition_map->at(ineigh);
        int buffer_size = shared_elt_map->at(ineigh);

        for (int i = 0; i < buffer_size; ++i) {
            int index = send_elt_map->at(ineigh)->at(i);
            buffer_elt_send[ineigh][i] = array->at(index);
        }

        MPI_Send(buffer_elt_send[ineigh], buffer_size, MPI_INT, neighbor_id, 0, MPI_COMM_WORLD);
    }

    //------------------- synchronize --------------------------

    for (int ineigh = 0; ineigh < neighbor_number; ++ineigh) {

        int neighbor_id = partition_map->at(ineigh);
        int buffer_size = shared_elt_map->at(ineigh);

        int ierr = MPI_Wait(&request[ineigh],&status[ineigh]);

        for (int i = 0; i < buffer_size; ++i) {
            int index = receive_elt_map->at(ineigh)->at(i);
            array->at(index) += buffer_elt_recv[ineigh][i];
        }
    }
}
