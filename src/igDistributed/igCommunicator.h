/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igDistributedExport.h>

#include <vector>

using namespace std;

class IGDISTRIBUTED_EXPORT igCommunicator
{
public:
     igCommunicator(void);
    ~igCommunicator(void);

public:
    void initialize(void);
    void finalize(void);

    void setPartitionMap(vector<int> *map);
    void setSharedDofMap(vector<int> *map);
    void setSendDofMap(vector< vector<int>* > *map);
    void setReceiveDofMap(vector< vector<int>* > *map);
    void setSharedEltMap(vector<int> *map);
    void setSendEltMap(vector< vector<int>* > *map);
    void setReceiveEltMap(vector< vector<int>* > *map);

    void initializeDofBuffers(int field_number);
    void initializeEltBuffers(void);
    void resizeDofBuffers(int field_number);
    void resizeEltBuffers(void);

    double distributeTimeStep(double local_time_step);

    void distributeField(vector<double> *state, int field_number);
    void distributeElementArrayInt(vector<int> *array);
    void addElementArrayInt(vector<int> *array);

    double distributeError(double local_error);
    double distributeEffort(double local_effort);

    void distributeInterfaceDouble(double *interface_local, double *interface_global, int total_length);
    void distributeInterfaceInt(int *counter_local, int *counter_global, int total_length);

    int distributeGlobalIdCounter(int my_counter, int my_rank);

    int addInt(int number);

    int rank(void);
    int size(void);

private:
    int world_size;
    int world_rank;

    vector<int> *partition_map;
    vector<int> *shared_dof_map;
    vector< vector<int>* > *send_dof_map;
    vector< vector<int>* > *receive_dof_map;

    vector<int> *shared_elt_map;
    vector< vector<int>* > *send_elt_map;
    vector< vector<int>* > *receive_elt_map;

    double **buffer_dof_send;
    double **buffer_dof_recv;

    int **buffer_elt_send;
    int **buffer_elt_recv;

};

//
// igCommunicator.h ends here
