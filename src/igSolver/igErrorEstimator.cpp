/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igErrorEstimator.h"

#include "igSolver.h"

#include <iostream>
#include <math.h>

#include <igCore/igMesh.h>
#include <igCore/igCell.h>
#include <igCore/igElement.h>
#include <igCore/igFace.h>
#include <igCore/igAssert.h>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igErrorEstimator::igErrorEstimator(void)
{
    this->local_error = new vector<double>;

    this->subdomain_mode = false;
}

igErrorEstimator::~igErrorEstimator(void)
{

}

/////////////////////////////////////////////////////////////////////////////

void igErrorEstimator::setMesh(igMesh *mesh)
{
    IG_ASSERT(mesh, "no mesh");
    this->mesh = mesh;
}

void igErrorEstimator::setSolver(igSolver *solver)
{
    IG_ASSERT(solver, "no solver");
    this->solver = solver;
}

void igErrorEstimator::setState(vector<double> *state)
{
    IG_ASSERT(state, "no state");
    this->state = state;
}

void igErrorEstimator::setTime(double time)
{
    this->time = time;
}

void igErrorEstimator::setError(vector<double> *error_xi, vector<double> *error_eta)
{
    IG_ASSERT(error_xi, "no error_xi");
    IG_ASSERT(error_eta, "no error_eta");
    this->error_xi = error_xi;
    this->error_eta = error_eta;
}

void igErrorEstimator::setRefinementDirection(int direction)
{
    this->refine_direction = direction;

    cout << endl;
    cout << "Refinement direction: " << direction << endl;
    cout << endl;
}

void igErrorEstimator::setSubdomain(int subdomain_id)
{
    this->subdomain_to_refine = subdomain_id;
    this->subdomain_mode = true;
}

/////////////////////////////////////////////////////////////////////////////

int igErrorEstimator::refinementDirection(void)
{
	return refine_direction;
}

/////////////////////////////////////////////////////////////////////////////

void igErrorEstimator::removeHangingNodes(void)
{
    cout << "Removing haning nodes ..." << endl;

    bool check_hanging = true;
    double eps = 1.E-15;

    while (check_hanging) {

        check_hanging = false;

        for(int ifac = 0; ifac < mesh->faceNumber(); ifac++){

            igFace *face = mesh->face(ifac);

            int left_index = face->leftElementIndex();
            int right_index = face->rightElementIndex();

            // propagation left -> right

            if((*error_xi)[left_index] > 0){

                if(face->leftOrientation() == 0 || face->leftOrientation() == 2){

                    if(face->rightOrientation() == 0 || face->rightOrientation() == 2){
                        if((*error_xi)[right_index] < eps){
                            (*error_xi)[right_index] = 1;
                            check_hanging = true;
                        }
                    } else {
                        if((*error_eta)[right_index] < eps){
                            (*error_eta)[right_index] = 1;
                            check_hanging = true;
                        }
                    }

                }
            }

            if((*error_eta)[left_index] > 0){

                if(face->leftOrientation() == 1 || face->leftOrientation() == 3){

                    if(face->rightOrientation() == 0 || face->rightOrientation() == 2){
                        if((*error_xi)[right_index] < eps){
                            (*error_xi)[right_index] = 1;
                            check_hanging = true;
                        }
                    } else {
                        if((*error_eta)[right_index] < eps){
                            (*error_eta)[right_index] = 1;
                            check_hanging = true;
                        }
                    }

                }
            }

            // propagation right -> left

            if((*error_xi)[right_index] > 0){

                if(face->rightOrientation() == 0 || face->rightOrientation() == 2){

                    if(face->leftOrientation() == 0 || face->leftOrientation() == 2){
                        if((*error_xi)[left_index] < eps){
                            (*error_xi)[left_index] = 1;
                            check_hanging = true;
                        }
                    } else {
                        if((*error_eta)[left_index] < eps){
                            (*error_eta)[left_index] = 1;
                            check_hanging = true;
                        }
                    }

                }
            }

            if((*error_eta)[right_index] > 0){

                if(face->rightOrientation() == 1 || face->rightOrientation() == 3){

                    if(face->leftOrientation() == 0 || face->leftOrientation() == 2){
                        if((*error_xi)[left_index] < eps){
                            (*error_xi)[left_index] = 1;
                            check_hanging = true;
                        }
                    } else {
                        if((*error_eta)[left_index] < eps){
                            (*error_eta)[left_index] = 1;
                            check_hanging = true;
                        }
                    }


                }
            }

        }

    }

    cout << "... Done" << endl;
}
