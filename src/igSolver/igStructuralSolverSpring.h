/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igSolverExport.h>

#include "igStructuralSolver.h"

#include <fstream>
#include <vector>

using namespace std;

class IGSOLVER_EXPORT igStructuralSolverSpring : public igStructuralSolver
{
public:
    igStructuralSolverSpring(vector<double> *interface_gauss_pts, vector<double> *interface_gauss_wgt, vector<double> *interface_efforts, vector<double> *interface_displacements, vector<double> *interface_velocities);
    virtual ~igStructuralSolverSpring(void);

public:
    void compute(void) override;

private:
    double time;
    double time_start;

    int structure_dof;

    vector<double> stiffness;
    vector<double> mass;

    vector<double> displacement;
    vector<double> velocity;
    vector<double> acceleration;

    vector<double> fluid_effort;

    bool open_file;
    ofstream fsi_file;

};

//
// igStructuralSolverSpring.h ends here
