/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igCouplingInterfaceSpring.h"

#include <igDistributed/igCommunicator.h>


///////////////////////////////////////////////////////////////

igCouplingInterfaceSpring::igCouplingInterfaceSpring(void) : igCouplingInterface()
{

    // set main structural parameters
    this->ctrl_pt_number = 1;
    this->degree = 0;
    this->gauss_pt_number = 1;
    this->elt_number = 0;

    // interface arrays for the structure
    interface_efforts_s->resize(3, 0);
    interface_displacements_s->resize(3, 0);
    interface_velocities_s->resize(3, 0);

}

igCouplingInterfaceSpring::~igCouplingInterfaceSpring(void)
{

}


/////////////////////////////////////////////////////////////

void igCouplingInterfaceSpring::convertFluidEffortsToStructure(void)
{
    int gauss_pt_number = interface_efforts_f->size()/3;
    double fx = 0;
    double fy = 0;
    double mz = 0;

    // sum all local contributions
    int counter = 0;
    for(int ipt=0; ipt<gauss_pt_number; ipt++){
        fx += interface_efforts_f->at(counter);
        fy += interface_efforts_f->at(counter+1);
        mz += interface_efforts_f->at(counter+2);
        counter +=3;
    }

    // MPI communication
    double global_fx = communicator->distributeEffort(fx);
    double global_fy = communicator->distributeEffort(fy);
    double global_mz = communicator->distributeEffort(mz);

    interface_efforts_s->at(0) = global_fx;
    interface_efforts_s->at(1) = global_fy;
    interface_efforts_s->at(2) = global_mz;

}
