/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igFlux.h"

#include <fstream>
#include <iostream>
#include <math.h>
#include <igCore/igAssert.h>

///////////////////////////////////////////////////////////////

igFlux::igFlux(int variable_number, int dimension, bool flag_ALE)
{
    this->variable_number = variable_number;
    this->dimension = dimension;

    this->ALE_formulation = flag_ALE;

    this->equation_number = variable_number; // by default
}

igFlux::~igFlux(void)
{
}

///////////////////////////////////////////////////////////////

void igFlux::setTime(double current_time)
{
    this->time = current_time;
}

//////////////////////////////////////////////////////////////

void igFlux::setDiffusionCoefficient(double coefficient)
{
}

void igFlux::setArtificialViscosity(double)
{
}

void igFlux::setArtificialViscosity(double, double)
{
}

///////////////////////////////////////////////////////////////

int igFlux::equationNumber(void) const
{
    return this->equation_number;
}

//////////////////////////////////////////////////////////////

void igFlux::boundaryFlux(const vector<double> *state_left, const vector<double> *derivative_left, const vector<double> *state_right, const vector<double> *derivative_right, vector<double> *flux_normal_left, vector<double> *flux_normal_right, const vector<double> *mesh_vel, const double *normal, int face_type)
{
    IG_ASSERT(state_left, "no state_left");
    IG_ASSERT(state_right, "no state_right");
    IG_ASSERT(derivative_left, "no derivative_left");
    IG_ASSERT(derivative_right, "no derivative_right");
    IG_ASSERT(flux_normal_left, "no flux_normal_left");
    IG_ASSERT(flux_normal_right, "no flux_normal_right");
    IG_ASSERT(normal, "no normal");
    // by default : computation of the numerical flux at point k
    this->numerical(state_left, derivative_left, state_right, derivative_right,
                    flux_normal_left, flux_normal_right, mesh_vel, normal);
}

///////////////////////////////////////////////////////////////

double igFlux::laxFriedrichsFlux(double flux_left, double flux_right, double state_left, double state_right, double speed)
{
    return (flux_left+flux_right)/2. + speed*(state_left-state_right)/2.;
}

///////////////////////////////////////////////////////////////

double igFlux::hllFlux(double flux_left, double flux_right, double state_left, double state_right, double speed_left, double speed_right)
{
    double flux;

    if (speed_left > 0) {
        flux = flux_left;
    } else if(speed_right < 0) {
        flux = flux_right;
    } else {
        flux = (speed_right*flux_left - speed_left*flux_right + speed_left*speed_right*(state_right-state_left))/(speed_right-speed_left);
    }

    return flux;
}

///////////////////////////////////////////////////////////////

double igFlux::localDGFlux(double flux_left, double flux_right, double sign)
{
    if (sign > 0) {
        return flux_left;
    }

    return flux_right;
}

///////////////////////////////////////////////////////////////

double igFlux::centeredFlux(double flux_left, double flux_right)
{
    return (flux_left + flux_right)*0.5;
}

///////////////////////////////////////////////////////////////

double igFlux::penaltyFlux(double state_left, double state_right)
{
    return (state_left-state_right);
}
