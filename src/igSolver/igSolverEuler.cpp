/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igSolverEuler.h"

#include "igFluxEuler2D.h"
#include "igFluxEuler3D.h"
#include "igCouplingInterface.h"

#include <igCore/igMesh.h>
#include <igCore/igElement.h>
#include <igCore/igFace.h>
#include <igCore/igAssert.h>
#include <igCore/igBoundaryIds.h>

#include <igDistributed/igCommunicator.h>

#include <igGenerator/igSolutionAnalyticEuler.h>

#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <math.h>
#include <limits>


///////////////////////////////////////////////////////////////

igSolverEuler::igSolverEuler(int variable_number, int dimension, vector<double> *solver_parameters, bool flag_ALE) : igSolverCompressibleFlow(variable_number, dimension, solver_parameters, flag_ALE)
{
    // flux
    if(dimension == 2){
        flux = new igFluxEuler2D(variable_number, dimension, flag_ALE);
    } else {
        flux = new igFluxEuler3D(variable_number, dimension, flag_ALE);
    }

}

igSolverEuler::~igSolverEuler(void)
{

}

/////////////////////////////////////////////////////////////

void igSolverEuler::computeIncrement(vector<double> *increment)
{
    // reset residual list
    this->resetResidual();

    // update time for time-dependent cases
    flux->setTime(time);

    // communication of state
    communicator->distributeField(state_list, variable_number);

    // compute the different DG terms
    this->volumicIntegrator(flux,residual_list);
    this->surfacicIntegrator(flux,residual_list);
    this->boundaryIntegrator(flux,residual_list);

    this->updateSolution(increment,residual_list,variable_number);
}

void igSolverEuler::computeIncrementAle(vector<double> *increment)
{
    // reset residual list
    this->resetResidual();

    // update time for time-dependent cases
    flux->setTime(time);

    // communication of state
    communicator->distributeField(state_list, variable_number);

    // compute the different DG terms
    this->volumicIntegrator(flux,residual_list);
    this->surfacicIntegrator(flux,residual_list);
    this->boundaryIntegrator(flux,residual_list);

    for(int idof=0; idof<residual_list->size(); idof++)
    	increment->at(idof) = residual_list->at(idof);

}

//////////////////////////////////////////////////////////////

double igSolverEuler::timeStep(void)
{
    vector<double> *mesh_vel = new vector<double>(this->dimension,0.);
    double my_time_step = numeric_limits<double>::max();

    // for time-dependent cases:
    flux->setTime(time);

    vector<double> *local_state = new vector<double>;
    local_state->resize(variable_number);
    vector<double> *derivative = new vector<double>;
    derivative->resize(variable_number);

    for (int iel=0; iel<mesh->elementNumber(); iel++) {

        igElement *elt = mesh->element(iel);

        if (elt->isActive()) {

            // NB : this is an (over) estimate of wave speed (cf convex properties of NURBS)
            double max_wave_speed = 0;
            for (int idof=0; idof<elt->controlPointNumber(); idof++){
                if(ALE_formulation)
                    elt->controlPointVelocity(idof,mesh_vel);
                this->extractState(elt,idof,local_state,derivative);
                max_wave_speed = fmax(max_wave_speed,flux->waveSpeed(local_state,mesh_vel));
            }

            double diameter = 2*elt->radius() / (1. + 2*elt->cellDegree());

            double local_step = diameter / max_wave_speed;
            my_time_step = fmin(my_time_step,local_step);
        }
    }

    // communication of time step for distributed computations
    double time_step = communicator->distributeTimeStep(my_time_step);

    delete local_state;
    delete derivative;
    delete mesh_vel;

    return time_step * this->cfl_coefficient;
}

//////////////////////// Boundary solution /////////////////////////////////////////

void igSolverEuler::boundarySolution(igFace *face, int index, double time, vector<double> *state_interior, vector<double> *state, vector<double> *mesh_vel)
{
    IG_ASSERT(face, "no face");
    IG_ASSERT(state_interior, "no state_interior");
    IG_ASSERT(state, "no state");

    // retrieve interior and exterior variables
    this->retrieveInteriorExteriorQuantities(state_interior,state);

    // retrieve geometric data
    this->retrieveGeometricalQuantities(face,mesh_vel,index);

    //////////////////// generic cases ///////////////

    if(face->type() < id_max_std){

        //-------------- wall conditions -----------------------

        if(face->type() == id_slip_wall || face->type() == id_slip_wall_fsi){
            this->computeSlipWall();
        }

        //---------- inlet / outlet conditions using RI ----------

        if(face->type() == id_io_riemann ){
            this->computeIORiemann();
        }

        //---------- imposed density, velocity / pressure ----------

        if(face->type() == id_io_density_velocity_pressure ){
            this->computeIODensityVelocityPressure();
        }

        //---------- supersonic inlet / outlet conditions ----------

        if(face->type() == id_io_supersonic ){
            this->computeIOSupersonic();
        }

        //--------------------------------------------------------

        // recover conservative boundary state
        this->recoverBoundaryState(state);
    }

    //////////////////// analytical cases ///////////////
    else{

        //---------- Ringleb case ------------------------------

        if(face->type() >= id_ringleb){

            double x = face->gaussPointPhysicalCoordinate(index,0);
            double y = face->gaussPointPhysicalCoordinate(index,1);

            for(int ivar=0; ivar<variable_number; ivar++){
                (*state)[ivar] = solution_euler_ringleb_boundary(x,y,time,ivar,face->type());
            }
        }

        //----------- other analytical cases -----------------

        else {
            this->exactSolution(face->gaussPointPhysicalCoordinate(index),time,state,face->type(),0.,0.);
        }
    }

}

//////////////////////// computation of efforts /////////////////////////////////////////

void igSolverEuler::computeEfforts(void)
{
    if(open_effort_file && communicator->rank() == 0){
        effort_file.open("efforts.dat");
        effort_file.precision(15);
        open_effort_file = false;
    }

    vector<double> state(variable_number);
    vector<double> derivative(derivative_number);

    double Fx = 0.;
    double Fy = 0.;
    double Mz = 0.;

    int local_counter = 0;

    interface->resetLocalEffort();

    // loop over all boundary interfaces
    for (int ifac = 0; ifac < mesh->boundaryFaceNumber(); ++ifac) {

        igFace *face = mesh->boundaryFace(ifac);

        if (face->isActive()) {

            int face_type = face->type();

            // only wall faces
            if(face_type == id_slip_wall || face_type == id_slip_wall_fsi ){

                int eval_number = face->gaussPointNumber();

                // loop over quadrature points
                for (int k = 0; k < eval_number; ++k) {

                    double gauss_point_weight =  face->gaussPointWeight(k);
                    double gauss_point_jacobian = face->gaussPointJacobian(k);
                    double gauss_point_coord_x = face->gaussPointPhysicalCoordinate(k,0);
                    double gauss_point_coord_y = face->gaussPointPhysicalCoordinate(k,1);

                    // normal vector at point k
                    double *normal_vector = face->gaussPointNormalPtr(k);

                    // left (interior) solution at point k
                    this->solutionAtGaussPoint(face, k, &state, &derivative);

                    // retrieve pressure value
                    double rho = state[0];
                    double u   = state[1] / rho;
                    double v   = state[2] / rho;
                    double energy = state[3];
                    double p   = (gamma-1.) * (energy - 0.5 * rho * (u*u + v*v));

                    // computation of local effort and local torque (about the origin)
                    double dF_x = p * normal_vector[0];
                    double dF_y = p * normal_vector[1];
                    double dM_z = (gauss_point_coord_x * dF_y - gauss_point_coord_y * dF_x);

                    Fx += dF_x * gauss_point_jacobian * gauss_point_weight;
                    Fy += dF_y * gauss_point_jacobian * gauss_point_weight;
                    Mz += dM_z * gauss_point_jacobian * gauss_point_weight;

                    // storage for fsi
                    if(face_type == id_slip_wall_fsi){
                        interface->addLocalEffort(local_counter, dF_x, 0., dF_y, 0., dM_z, gauss_point_jacobian * gauss_point_weight,normal_vector[0],normal_vector[1]);
                        local_counter ++;
                    }

                }

            }
        }
    }

    // MPI communications
    double Global_Fx = communicator->distributeEffort(Fx);
    double Global_Fy = communicator->distributeEffort(Fy);
    double Global_Mz = communicator->distributeEffort(Mz);

        // write into file
    if(communicator->rank() == 0)
    	effort_file << scientific << this->time << " " << Global_Fx << " " << Global_Fy << " " << Global_Mz;

}


//////////////////////// exact solution /////////////////////////////////////////


void igSolverEuler::exactSolution(double *position, double time, vector<double> *solution, int solution_id, double x_elt, double y_elt)
{
    double *generic_param;

    // vortex case
    if(solution_id == id_vortex){
        for(int ivar=0; ivar<variable_number; ivar++){
            (*solution)[ivar] = solution_euler_vortex(position,time,ivar,generic_param,generic_param);
        }
    }

    // cylinder case
    else if(solution_id == id_cylinder){
        for(int ivar=0; ivar<variable_number; ivar++){
            (*solution)[ivar] = solution_euler_cylinder(position,time,ivar,generic_param,generic_param);
        }
    }

    // Ringleb case
    else if(solution_id >= id_ringleb){
        for(int ivar=0; ivar<variable_number; ivar++){
            (*solution)[ivar] = solution_euler_ringleb(position,time,ivar,generic_param,generic_param);
        }
    }
}
