/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igFluxAcoustic.h"

#include <fstream>
#include <iostream>
#include <math.h>
#include <igCore/igAssert.h>

///////////////////////////////////////////////////////////////

igFluxAcoustic::igFluxAcoustic(int variable_number, int dimension, bool flag_ALE) : igFlux(variable_number, dimension, flag_ALE)
{
    this->sound_speed = 1;

    flux_left.assign(variable_number*dimension, 0.);
    flux_right.assign(variable_number*dimension, 0.);
}

igFluxAcoustic::~igFluxAcoustic(void)
{
}

///////////////////////////////////////////////////////////////

void igFluxAcoustic::setTime(double current_time)
{
    this->time = current_time;
}

///////////////////////////////////////////////////////////////

void igFluxAcoustic::physical(const vector<double> *state, const vector<double> *derivative, const vector<double> *mesh_vel, vector<double> *flux)
{
    IG_ASSERT(state, "no state");
    IG_ASSERT(derivative, "no derivative");
    IG_ASSERT(flux, "no flux");

    double u = (*state)[0];
    double v   = (*state)[1];
    double p   = (*state)[2];

    // velocity-x flux
    (*flux)[0] = p - mesh_vel->at(0)*u;
    (*flux)[1] = - mesh_vel->at(1)*u;

    // velocity-y flux
    (*flux)[2] = - mesh_vel->at(0)*v;
    (*flux)[3] = p - mesh_vel->at(1)*v;

    // pressure flux
    (*flux)[4] = sound_speed*sound_speed*u - mesh_vel->at(0)*p;
    (*flux)[5] = sound_speed*sound_speed*v - mesh_vel->at(1)*p;

}

/////////////////////////////////////////////////////////////////

void igFluxAcoustic::numerical(const vector<double> *state_left, const vector<double> *derivative_left,
                            const vector<double> *state_right, const vector<double> *derivative_right,
                            vector<double> *flux_normal_left, vector<double> *flux_normal_right,
							const vector<double> *mesh_vel, const double *normal)
{
    IG_ASSERT(state_left, "no state_left");
    IG_ASSERT(state_right, "no state_right");
    IG_ASSERT(derivative_left, "no derivative_left");
    IG_ASSERT(derivative_right, "no derivative_right");
    IG_ASSERT(flux_normal_left, "no flux_normal_left");
    IG_ASSERT(flux_normal_right, "no flux_normal_right");
    IG_ASSERT(normal, "no normal");

    // wave speed at left and right
    double left_wave_speed = this->interfacialWaveSpeed(state_left, mesh_vel, normal);
    double right_wave_speed = this->interfacialWaveSpeed(state_right, mesh_vel, normal);

    double max_wave_speed = max(fabs(left_wave_speed), fabs(right_wave_speed));

    // flux vector at left and right
    this->physical(state_left, derivative_left, mesh_vel, &flux_left);
    this->physical(state_right, derivative_right, mesh_vel, &flux_right);

    for(int ivar=0; ivar<variable_number; ivar++){

        // normal flux at left and right
        (*flux_normal_left)[ivar] = 0.;
        (*flux_normal_right)[ivar] = 0.;
        for (int idim = 0; idim < dimension; ++idim) {
            (*flux_normal_left)[ivar] += flux_left[idim+ivar*dimension] * normal[idim];
            (*flux_normal_right)[ivar] += flux_right[idim+ivar*dimension] * normal[idim];
        }

        // computation of the numerical flux (Lax-Friedrichs)
        double flux = this->laxFriedrichsFlux((*flux_normal_left)[ivar], (*flux_normal_right)[ivar],
                                          (*state_left)[ivar], (*state_right)[ivar],
										  max_wave_speed);

        (*flux_normal_left)[ivar]  = flux;
        (*flux_normal_right)[ivar] = flux;

    }

}

/////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////

double igFluxAcoustic::waveSpeed(const vector<double> *state, const vector<double> *mesh_vel)
{
	double speed = 0.;
	for (int idim = 0; idim < dimension; ++idim) {
		speed += mesh_vel->at(idim) * mesh_vel->at(idim);
	}
	speed = fabs(sound_speed + sqrt(speed));
	return speed;
}

double igFluxAcoustic::interfacialWaveSpeed(const vector<double> *state, const vector<double> *mesh_vel, const double *normal)
{
	double normal_speed = 0.;
	for (int idim = 0; idim < dimension; ++idim) {
		normal_speed += mesh_vel->at(idim)*normal[idim];
	}
	normal_speed = sound_speed - normal_speed;
	return normal_speed;
}

