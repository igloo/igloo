/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igStructuralSolverPiston.h"

#include <igDistributed/igCommunicator.h>

#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <math.h>
#include <limits>


///////////////////////////////////////////////////////////////

igStructuralSolverPiston::igStructuralSolverPiston(vector<double> *interface_gauss_pts, vector<double> *interface_gauss_wgt, vector<double> *interface_efforts, vector<double> *interface_displacements, vector<double> *interface_velocities) : igStructuralSolver(interface_gauss_pts,interface_gauss_wgt,interface_efforts,interface_displacements,interface_velocities)
{
    // spring characteristics
    this->mass = 1.E3;
    this->stiffness = 1.E7;
    this->surface = 0.2;

    // initial conditions
    this->time = 0;
    this->displacement = 0.2;
    this->velocity = 0;

    this->rest_pressure = 1E5;
    double initial_pressure = 77472;
    this->acceleration = (-stiffness*displacement + surface*(initial_pressure-rest_pressure))/mass;

    this->open_file = true;
}

igStructuralSolverPiston::~igStructuralSolverPiston(void)
{

}

/////////////////////////////////////////////////////////////

void igStructuralSolverPiston::compute(void)
{

    //---------- retrieve fluid efforts on piston ----------

    double fluid_effort = interface_efforts->at(0);

    //------------- piston movement --------------------

    double acceleration_old = acceleration;

    double variation = (fluid_effort - surface*rest_pressure - stiffness*displacement + mass*(4*velocity/time_step+acceleration))
                    / (4*mass/(time_step*time_step) + stiffness);

    displacement += variation;

    acceleration = 4/(time_step*time_step)*variation - 4/time_step*velocity - acceleration;
    velocity += time_step/2 * (acceleration+acceleration_old);

    // fill arrays
    this->interface_displacements->at(0) = displacement;
    this->interface_displacements->at(1) = 0;
    this->interface_displacements->at(2) = 0;

    this->interface_velocities->at(0) = velocity;
    this->interface_velocities->at(1) = 0;
    this->interface_velocities->at(2) = 0;

    time += time_step;

    // write output file
    if(open_file && communicator->rank() == 0){
        fsi_file.open("fsi.dat");
        fsi_file.precision(12);
        open_file = false;
    }

    if(communicator->rank() == 0)
        fsi_file << scientific << time << " " << fluid_effort << " " << displacement << " " << velocity << endl;


    return;
}
