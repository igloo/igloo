/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igSolverViscousBurger.h"

#include "igFluxViscousBurger.h"
#include "igFluxViscousBurgerGradient.h"

#include <igCore/igMesh.h>
#include <igCore/igElement.h>
#include <igCore/igFace.h>
#include <igCore/igAssert.h>
#include <igGenerator/igSolutionAnalyticViscousBurgers.h>

#include <fstream>
#include <iostream>
#include <math.h>
#include <limits>

#define PI 3.14159265

///////////////////////////////////////////////////////////////

igSolverViscousBurger::igSolverViscousBurger(int variable_number, int dimension, vector<double> *solver_parameters, bool flag_ALE) : igSolver(variable_number, dimension, solver_parameters, flag_ALE)
{
    this->derivative_number = variable_number;

    diffusion_coefficient = solver_parameters->at(1);
    solution_inlet = solver_parameters->at(0);
    solution_outlet = solver_parameters->at(2);

    flux = new igFluxViscousBurger(variable_number, dimension, flag_ALE);
    grad_flux = new igFluxViscousBurgerGradient(variable_number, dimension, flag_ALE);

    flux->setDiffusionCoefficient(diffusion_coefficient);
    grad_flux->setDiffusionCoefficient(diffusion_coefficient);
}

igSolverViscousBurger::~igSolverViscousBurger(void)
{
    delete grad_residual_list;
    delete grad_state_list;

    delete flux;
    delete grad_flux;
}

//////////////////////////////////////////////////////////////

void igSolverViscousBurger::initializeDerivative(void)
{
    int size = residual_list->size();

    grad_residual_list = new vector<double>;
    grad_residual_list->assign(size,0.);

    grad_state_list = new vector<double>;
    grad_state_list->assign(size,0);
}

/////////////////////////////////////////////////////////////

void igSolverViscousBurger::resetDerivative(void)
{
    // reset residuals list for gradient
    this->resetGradData();

    // compute DG terms for gradient
    this->volumicIntegrator(grad_flux,grad_residual_list);
    this->surfacicIntegrator(grad_flux,grad_residual_list);
    this->boundaryIntegrator(grad_flux,grad_residual_list);

    this->updateSolution(grad_state_list,grad_residual_list,derivative_number);
}

//////////////////////////////////////////////////////////////

void igSolverViscousBurger::resetGradData(void)
{
    for (int i=0; i<grad_residual_list->size(); i++){
        grad_residual_list->at(i) = 0.;
    }
}

/////////////////////////////////////////////////////////////

void igSolverViscousBurger::computeIncrement(vector<double> *increment)
{
    IG_ASSERT(increment, "no increment");

    // reset residual list
    this->resetResidual();

    // compute the different DG terms
    this->volumicIntegrator(flux,residual_list);
    this->surfacicIntegrator(flux,residual_list);
    this->boundaryIntegrator(flux,residual_list);

    this->updateSolution(increment,residual_list,variable_number);
}

void igSolverViscousBurger::computeIncrementAle(vector<double> *increment)
{
	// reset residual list
	this->resetResidual();

	// compute the different DG terms
	this->volumicIntegrator(flux,residual_list);
	this->surfacicIntegrator(flux,residual_list);
	this->boundaryIntegrator(flux,residual_list);

    for(int idof=0; idof<residual_list->size(); idof++)
    	increment->at(idof) = residual_list->at(idof);

}

//////////////////////////////////////////////////////////////

double igSolverViscousBurger::timeStep(void)
{
	vector<double> *mesh_vel = new vector<double>(this->dimension,0.);
    double time_step = numeric_limits<double>::max();

    vector<double> *local_state = new vector<double>;
    local_state->resize(variable_number);
    vector<double> *derivative = new vector<double>;
    derivative->resize(derivative_number);

    for (int iel=0; iel<mesh->elementNumber(); iel++){

        igElement *elt = mesh->element(iel);

        double max_wave_speed = 0;
        for (int idof=0; idof<elt->controlPointNumber(); idof++){
            this->extractState(elt,idof,local_state,derivative);
            max_wave_speed = fmax(max_wave_speed,flux->waveSpeed(local_state,mesh_vel));
        }

        double diameter = 2*elt->radius() / (2.* elt->cellDegree()+1.);

        double local_step_convection = diameter / max_wave_speed;
        double local_step_diffusion = (diameter*diameter)/(2*diffusion_coefficient);

        time_step = fmin(time_step,local_step_convection);
        time_step = fmin(time_step,local_step_diffusion);
    }
    delete local_state;
    delete derivative;
    delete mesh_vel;

    return time_step * this->cfl_coefficient;
}

//////////////////////////////////////////////////////////////////

void igSolverViscousBurger::exactSolution(double *position, double time, vector<double> *solution, int solution_id, double x_elt, double y_elt)
{
    IG_ASSERT(position, "no position");
    IG_ASSERT(solution, "no solution");

    double *generic_param;

    for (int ivar=0; ivar < variable_number; ivar++){
      (*solution)[ivar] = solution_viscous_burgers(position,time,ivar,generic_param,this->solver_parameters->data());
    }

}

//////////////////////////////////////////////////////////////////

void igSolverViscousBurger::exactDerivative(double *position, double time, vector<double> *derivative, int solution_id, double x_elt, double y_elt)
{
    IG_ASSERT(position, "no position");
    IG_ASSERT(derivative, "no derivative");

    double *generic_param;

    for (int ivar=0; ivar < variable_number; ivar++){
      (*derivative)[ivar] = solution_viscous_burgers_gradient(position,time,ivar,generic_param,this->solver_parameters->data());
    }

}
