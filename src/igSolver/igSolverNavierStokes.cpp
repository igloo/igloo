/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igSolverNavierStokes.h"

#include "igFluxNavierStokes2D.h"
#include "igFluxNavierStokes3D.h"
#include "igFluxNavierStokesGradient2D.h"
#include "igFluxNavierStokesGradient3D.h"
#include "igCouplingInterface.h"

#include <igCore/igMesh.h>
#include <igCore/igElement.h>
#include <igCore/igFace.h>
#include <igCore/igAssert.h>
#include <igCore/igBoundaryIds.h>

#include <igDistributed/igCommunicator.h>

#include <igGenerator/igSolutionAnalyticNavierStokes.h>

#include <iostream>
#include <math.h>
#include <limits>

///////////////////////////////////////////////////////////////

igSolverNavierStokes::igSolverNavierStokes(int variable_number, int dimension, vector<double> *solver_parameters, bool flag_ALE) : igSolverCompressibleFlow(variable_number, dimension, solver_parameters, flag_ALE)
{
    // set gradient size
    derivative_number = variable_number*dimension;

    // solver parameters
    reynolds_ref = solver_parameters->at(1);
    diffusion_coefficient = gamma*mach_ref/reynolds_ref;

    // fluxes
    if(dimension == 2){
        flux = new igFluxNavierStokes2D(variable_number, dimension, flag_ALE);
        grad_flux = new igFluxNavierStokesGradient2D(variable_number, dimension, flag_ALE);
    } else {
        flux = new igFluxNavierStokes3D(variable_number, dimension, flag_ALE);
        grad_flux = new igFluxNavierStokesGradient3D(variable_number, dimension, flag_ALE);
    }

    flux->setDiffusionCoefficient(diffusion_coefficient);

    // arrays for grad residuals and variables
    grad_residual_list = new vector<double>;
    grad_state_list = new vector<double>;

    // arrays for artificial viscosity
    smooth_viscosity_list = new vector<double>;
    smooth_viscosity_contrib = new vector<int>;

}

igSolverNavierStokes::~igSolverNavierStokes(void)
{
    delete smooth_viscosity_list;
    delete smooth_viscosity_contrib;

    delete grad_residual_list;
    delete grad_state_list;

    delete flux;
    delete grad_flux;
}


//////////////////////////////////////////////////////////////

void igSolverNavierStokes::setSmoothingViscosityNumber(int number)
{
    smoothing_step_number = number;
}


//////////////////////////////////////////////////////////////

void igSolverNavierStokes::initializeDerivative(void)
{
    int residual_size = residual_list->size();
    grad_residual_list->resize(residual_size*dimension, 0.);

    int state_size = state_list->size();
    grad_state_list->resize(state_size*dimension);

    int local_viscosity_size = mesh->controlPointNumber()+mesh->sharedControlPointNumber();

    artificial_viscosity_list->assign(local_viscosity_size,0.);
    smooth_viscosity_list->resize(local_viscosity_size);
    smooth_viscosity_contrib->resize(local_viscosity_size);

    vorticity_list->resize(local_viscosity_size);

}

//////////////////////////////////////////////////////////////

void igSolverNavierStokes::resetDerivative(void)
{
    // reset residuals list for gradient
    this->resetGradData();

    // communication of state
    communicator->distributeField(state_list, variable_number);
    communicator->distributeField(artificial_viscosity_list, 1);

    // compute DG terms for gradient
    this->volumicIntegrator(grad_flux, grad_residual_list);
    this->surfacicIntegrator(grad_flux, grad_residual_list);
    this->boundaryIntegrator(grad_flux, grad_residual_list);

    this->updateSolution(grad_state_list, grad_residual_list, derivative_number);
}

//////////////////////////////////////////////////////////////

void igSolverNavierStokes::resetGradData(void)
{
    for (int i = 0; i < grad_residual_list->size(); ++i) {
        (*grad_residual_list)[i] = 0.;
    }
}

/////////////////////////////////////////////////////////////

void igSolverNavierStokes::computeIncrement(vector<double> *increment)
{

    // reset residual list
    this->resetResidual();

    // update time for time-dependent cases
    flux->setTime(time);

    // communication of state gradient
    communicator->distributeField(grad_state_list, derivative_number);

    // compute the different DG terms
    this->volumicIntegrator(flux,  residual_list);
    this->surfacicIntegrator(flux, residual_list);
    this->boundaryIntegrator(flux, residual_list);

    this->updateSolution(increment, residual_list, variable_number);
}

void igSolverNavierStokes::computeIncrementAle(vector<double> *increment)
{
    // reset residual list
    this->resetResidual();

    // update time for time-dependent cases
    flux->setTime(time);

    // communication of state gradient
    communicator->distributeField(grad_state_list, derivative_number);

    // compute the different DG terms
    this->volumicIntegrator(flux,  residual_list);
    this->surfacicIntegrator(flux, residual_list);
    this->boundaryIntegrator(flux, residual_list);

    for(int idof=0; idof<residual_list->size(); idof++)
    	increment->at(idof) = residual_list->at(idof);

}

//////////////////////////////////////////////////////////////

double igSolverNavierStokes::timeStep(void)
{
    double my_time_step = numeric_limits<double>::max();

    // for time-dependent cases:
    flux->setTime(time);

    vector<double> *local_state = new vector<double>(variable_number);
    vector<double> *derivative = new vector<double>(derivative_number);
    vector<double> *mesh_vel = new vector<double>(this->dimension,0.);

    double local_viscosity;

    for (int iel = 0; iel < mesh->elementNumber(); ++iel) {

        igElement *elt = mesh->element(iel);

        if (elt->isActive()) {

            // NB : this is an (over) estimate of wave speed (cf convex properties of NURBS)
            double max_wave_speed = 0;

            for (int idof = 0; idof < elt->controlPointNumber(); ++idof) {
                if(ALE_formulation)
                    elt->controlPointVelocity(idof,mesh_vel);
                this->extractState(elt, idof, local_state, derivative);
                max_wave_speed = fmax(max_wave_speed, flux->waveSpeed(local_state,mesh_vel));
            }
            this->extractArtificialViscosity(elt, 0, local_viscosity);

            double diameter = 2 * elt->radius() / (1. + 2 * elt->cellDegree());

            double local_step_advection = diameter / max_wave_speed;
            double local_step_diffusion = diameter*diameter / (2.*(diffusion_coefficient+local_viscosity));
            double local_step = 1./(1./local_step_advection + 1./local_step_diffusion);

            my_time_step = fmin(my_time_step, local_step);

        }
    }

    // communication of time step for distributed computations
    double time_step = communicator->distributeTimeStep(my_time_step);
    this->current_time_step = time_step * this->cfl_coefficient;

    delete local_state;
    delete derivative;
    delete mesh_vel;

    return this->current_time_step;
}


//////////////////////////////////// Boundary solution  ////////////////////////////////////////////////////////

void igSolverNavierStokes::boundarySolution(igFace *face, int index, double time, vector<double> *state_interior, vector<double> *state, vector<double> *mesh_vel)
{

    IG_ASSERT(face, "no face");
    IG_ASSERT(state_interior, "no state_interior");
    IG_ASSERT(state, "no state");

    // retrieve interior and exterior variables
    this->retrieveInteriorExteriorQuantities(state_interior,state);

    // retrieve geometric data
    this->retrieveGeometricalQuantities(face,mesh_vel,index);

    //////////////////// generic cases ///////////////

    if(face->type() < id_max_std){

        //---------- slip wall conditions ----------

        if(face->type() == id_slip_wall){
            this->computeSlipWall();
        }

        //---------- no slip wall conditions ----------

        if(face->type() == id_noslip_wall || face->type() == id_noslip_wall_fsi){
            this->computeNoSlipWall();
        }

        //---------- inlet / outlet Riemann Invariant conditions ----------

        if(face->type() == id_io_riemann ){
            this->computeIORiemann();
        }
        //---------- imposed density, velocity / pressure ----------

        if(face->type() == id_io_density_velocity_pressure ){
            this->computeIODensityVelocityPressure();
        }

        //---------- supersonic inlet / outlet conditions ----------

        if(face->type() == id_io_supersonic ){
            this->computeIOSupersonic();
        }

        //--------------------------------------------------------

        // recover conservative boundary state
        this->recoverBoundaryState(state);
    }

    //////////////////// analytical cases ///////////////
    else{

        //----------- shock-vortex interaction case -------------

        if(face->type() == id_shock_vortex_left || face->type() == id_shock_vortex_right){

            double *generic_param;
            double *coord = face->gaussPointPhysicalCoordinate(index);

            rho_ext = solution_euler_shock_vortex(coord,time,0,coord,generic_param);
            velocity_ext[0] = solution_euler_shock_vortex(coord,time,1,coord,generic_param)/rho_ext;
            velocity_ext[1] = solution_euler_shock_vortex(coord,time,2,coord,generic_param)/rho_ext;
            p_ext = (gamma-1)*(solution_euler_shock_vortex(coord,time,3,coord,generic_param) - 0.5*rho_ext*(velocity_ext[0]*velocity_ext[0]+velocity_ext[1]*velocity_ext[1]));

            if(face->type() == id_shock_vortex_left){
                this->computeIOSupersonic();
            }
            if(face->type() == id_shock_vortex_right){
                this->computeIORiemann();
            }

            this->recoverBoundaryState(state);

        }

    }

}

void igSolverNavierStokes::boundaryDerivative(igFace *face, int index, double time, vector<double> *derivative_interior, vector<double> *derivative)
{
    IG_ASSERT(face, "no face");
    IG_ASSERT(derivative_interior, "no derivative_interior");
    IG_ASSERT(derivative, "no derivative");

    for (int ivar=0; ivar<derivative_number; ivar++) {
        (*derivative)[ivar] = (*derivative_interior)[ivar];
    }

    return;
}

//////////////////////// computation of efforts /////////////////////////////////////////

void igSolverNavierStokes::computeEfforts(void)
{
    if(open_effort_file && communicator->rank() == 0){
        effort_file.open("efforts.dat");
        effort_file.precision(15);
        open_effort_file = false;
    }

    vector<double> state(variable_number);
    vector<double> derivative(derivative_number);
    vector<double> *mesh_vel = new vector<double>(this->dimension,0.);

    double Fx_inv = 0.;
    double Fy_inv = 0.;

    double Fx_vis = 0.;
    double Fy_vis = 0.;

    double Mz = 0.;
    double Pwr = 0.;

    int local_counter = 0;

    interface->resetLocalEffort();

    // loop over all boundary interfaces
    for (int ifac = 0; ifac < mesh->boundaryFaceNumber(); ++ifac) {

        igFace *face = mesh->boundaryFace(ifac);

        if (face->isActive()) {

            int face_type = face->type();

            // efforts computed for slip and no-slip wall faces
            if(face_type == id_slip_wall || face_type == id_noslip_wall || face_type == id_noslip_wall_fsi){

                int eval_number = face->gaussPointNumber();

                // loop over quadrature points
                for (int k = 0; k < eval_number; ++k) {

                    double gauss_point_weight =  face->gaussPointWeight(k);
                    double gauss_point_jacobian = face->gaussPointJacobian(k);
                    double gauss_point_coord_x = face->gaussPointPhysicalCoordinate(k,0);
                    double gauss_point_coord_y = face->gaussPointPhysicalCoordinate(k,1);

                    // normal vector at point k
                    double *normal_vector = face->gaussPointNormalPtr(k);

                    // left (interior) solution at point k
                    this->solutionAtGaussPoint(face, k, &state, &derivative);

                    // get mesh velocity at point k
                    if(ALE_formulation)
                    	face->gaussPointVelocity(k,mesh_vel);

                    // retrieve pressure value
                    double rho = state[0];
                    double u   = state[1] / rho;
                    double v   = state[2] / rho;
                    double energy = state[3];
                    double p   = (gamma-1.) * (energy - 0.5 * rho * (u*u + v*v));

                    // computation of local inviscid effort
                    double dF_x_inv = p * normal_vector[0];
                    double dF_y_inv = p * normal_vector[1];

                    // retrieve local shear stress
                    double DuDx = ( derivative[2] - derivative[0]*u )/rho;
                    double DuDy = ( derivative[3] - derivative[1]*u )/rho;

                    double DvDx = ( derivative[4] - derivative[0]*v )/rho;
                    double DvDy = ( derivative[5] - derivative[1]*v )/rho;

                    double Txx = 2./3. * diffusion_coefficient * ( 2*DuDx - DvDy);
                    double Txy =  diffusion_coefficient * ( DuDy + DvDx );
                    double Tyy = 2./3. * diffusion_coefficient * ( 2*DvDy - DuDx);

                    // computation of local viscous effort
                    double dF_x_vis = - Txx * normal_vector[0] - Txy *normal_vector[1];
                    double dF_y_vis = - Txy * normal_vector[0] - Tyy *normal_vector[1];

                    // computation of local efforts and torque (about origin)
                    double dF_x = dF_x_inv + dF_x_vis;
                    double dF_y = dF_y_inv + dF_y_vis;
                    double dM_z = (gauss_point_coord_x * dF_y - gauss_point_coord_y * dF_x);
                    double dPwr = (dF_x * mesh_vel->at(0) + dF_y * mesh_vel->at(1));

                    // sum contributions
                    Fx_inv += dF_x_inv * gauss_point_jacobian * gauss_point_weight;
                    Fy_inv += dF_y_inv * gauss_point_jacobian * gauss_point_weight;

                    Fx_vis += dF_x_vis * gauss_point_jacobian * gauss_point_weight;
                    Fy_vis += dF_y_vis * gauss_point_jacobian * gauss_point_weight;

                    Mz += dM_z * gauss_point_jacobian * gauss_point_weight;
                    Pwr += dPwr * gauss_point_jacobian * gauss_point_weight;

                    // storage for fsi
                    if(face_type == id_noslip_wall_fsi){
                        interface->addLocalEffort(local_counter, dF_x_inv, dF_x_vis, dF_y_inv, dF_y_vis, dM_z, gauss_point_jacobian * gauss_point_weight,normal_vector[0],normal_vector[1]);
                        local_counter ++;
                    }

                }

            }
        }
    }

    // MPI communications
    double global_Fx_inv = communicator->distributeEffort(Fx_inv);
    double global_Fy_inv = communicator->distributeEffort(Fy_inv);

    double global_Fx_vis = communicator->distributeEffort(Fx_vis);
    double global_Fy_vis = communicator->distributeEffort(Fy_vis);

    double global_Mz = communicator->distributeEffort(Mz);
    double global_Pwr = communicator->distributeEffort(Pwr);

    double Fx_total = global_Fx_inv + global_Fx_vis;
    double Fy_total = global_Fy_inv + global_Fy_vis;

    // write into file
	if(communicator->rank() == 0)
		effort_file << scientific << time << " "  << Fx_total << " " << Fy_total << " "  << global_Mz << " "  << global_Pwr << endl;

    delete mesh_vel;
}

//////////////////////// computation of vorticity /////////////////////////////////////////

void igSolverNavierStokes::computeVorticity(void)
{
    // Loop over elements
    for(int ielt = 0; ielt < mesh->elementNumber(); ielt++){

        igElement *elt = mesh->element(ielt);

        if(elt->isActive()){

            int *global_ids_var = elt->globalIdPtr(0);

            // loop over control points
            for (int idof = 0; idof < elt->controlPointNumber(); ++idof){

                vector<double> derivative(derivative_number);

                // loop over derivatives
                for (int ivar = 0; ivar < derivative_number; ++ivar){
                    int *global_ids = elt->globalIdPtr(ivar);
                    derivative[ivar] = (*grad_state_list)[global_ids[idof]];
                }

                // Curl of momentum vector (rho*u,rho*v) (NURBS field)
                (*vorticity_list)[global_ids_var[idof]] = derivative[4] - derivative[3];

            }

        }

    }
}

//////////////////////////////////////////////////////////////

double igSolverNavierStokes::artificialViscosity(int index)
{
    return (*artificial_viscosity_list)[index];
}

/////////////////////////////////////////////////////////////

void igSolverNavierStokes::applyLimiter(double shock_capturing_coef)
{
    // local viscosity computed element by element
    for (int iel = 0; iel < mesh->elementNumber(); ++iel) {

        igElement *elt = mesh->element(iel);

        if (elt->isActive()) {

            int degree = elt->cellDegree();
            int function_number = elt->controlPointNumber();
            int eval_number = elt->gaussPointNumber();
            int *global_ids_var = elt->globalIdPtr(0);

            // ad-hoc parameters
            double sensor_mean = shock_capturing_coef /((degree+1)*(degree+1));
            double sensor_delta = 0.5 * sensor_mean;

            // density variation
            double sensor = (this->computeVariationXi(elt,0) + this->computeVariationEta(elt,0))/2.;

            //-------------- local viscosity estimate ---------

            double local_viscosity;
            double viscosity_max = mach_ref * elt->radius() / (degree+1);

            if (sensor <= sensor_mean-sensor_delta) {
                local_viscosity = 0.;

            } else {
                if (sensor > sensor_mean+sensor_delta) {
                    local_viscosity = viscosity_max;

                } else {
                    local_viscosity = viscosity_max/2. * (1. + sin(0.5*M_PI*(sensor-sensor_mean)/sensor_delta));
                }
            }

            for (int idof = 0; idof < function_number; ++idof) {
                (*artificial_viscosity_list)[global_ids_var[idof]] = local_viscosity;
            }
        }
    }

    // smoothing of the artificial viscosity field
    this->viscositySmoothing();


}

///////////////////////////////////////////////////////////////

void igSolverNavierStokes::artificialViscosityAtGaussPoint(igFace *face, int index_pt, double &left_artificial_viscosity, double &right_artificial_viscosity)
{
    IG_ASSERT(face, "no face");

    int function_number = face->controlPointNumber();

    // loop over basis functions
    left_artificial_viscosity  = 0.;
    right_artificial_viscosity = 0.;
    for (int i = 0; i < function_number; ++i) {
        left_artificial_viscosity  += face->gaussPointFunctionValueLeft(index_pt,i)  * (*artificial_viscosity_list)[face->dofLeft(i,0)];
        right_artificial_viscosity += face->gaussPointFunctionValueRight(index_pt,i) * (*artificial_viscosity_list)[face->dofRight(i,0)];
    }
}

void igSolverNavierStokes::artificialViscosityAtGaussPoint(igFace *face, int index_pt, double &left_artificial_viscosity)
{
    IG_ASSERT(face, "no face");
    int function_number = face->controlPointNumber();

    // loop over basis functions
    left_artificial_viscosity = 0.;
    for (int i = 0; i < function_number; ++i) {
        left_artificial_viscosity += face->gaussPointFunctionValueLeft(index_pt,i) * (*artificial_viscosity_list)[face->dofLeft(i,0)];
    }
}

void igSolverNavierStokes::artificialViscosityAtGaussPoint(igElement *element, int index_pt, double &artificial_viscosity)
{
    IG_ASSERT(element, "no element");
    int function_number = element->controlPointNumber();
    double *gauss_point_values = element->gaussPointFunctionPtr(index_pt);
    int *global_ids = element->globalIdPtr(0);

    // loop over basis functions
    artificial_viscosity = 0.;
    for (int i = 0; i < function_number; ++i) {
        artificial_viscosity += gauss_point_values[i] * (*artificial_viscosity_list)[global_ids[i]];
    }
}

void igSolverNavierStokes::extractArtificialViscosity(igElement *element, int index_pt, double &artificial_viscosity)
{
    IG_ASSERT(element, "no element");
    artificial_viscosity = (*artificial_viscosity_list)[element->globalId(index_pt,0)];
}

////////////////////////////////////////////////////////


void igSolverNavierStokes::viscositySmoothing(void)
{
    // smoothing loop
    for (int istep=0; istep<smoothing_step_number; istep++){

        // MPI communication required for face-based smoothing
        communicator->distributeField(artificial_viscosity_list, 1);

        // copy element-wise viscosity
        for (int i=0; i<artificial_viscosity_list->size(); i++){
            (*smooth_viscosity_list)[i] = (*artificial_viscosity_list)[i];
            (*smooth_viscosity_contrib)[i] = 1;
        }

        // loop over faces to sum element contributions
        for (int ifac=0; ifac<mesh->faceNumber(); ifac++){

            igFace *face = mesh->face(ifac);

            if(face->isActive()){

                // loop over face DOFs for left and right contributions
                for (int idof=0; idof<face->controlPointNumber(); idof++){

                    int index_left = face->dofLeft(idof, 0);
                    int index_right = face->dofRight(idof, 0);

                    (*smooth_viscosity_list)[index_left] += (*artificial_viscosity_list)[index_right];
                    (*smooth_viscosity_contrib)[index_left] ++;

                    (*smooth_viscosity_list)[index_right] += (*artificial_viscosity_list)[index_left];
                    (*smooth_viscosity_contrib)[index_right] ++;

                }
            }
        }

        // normalize
        for (int i=0; i<artificial_viscosity_list->size(); i++){
            (*artificial_viscosity_list)[i] = (*smooth_viscosity_list)[i] / (*smooth_viscosity_contrib)[i];
        }
    }

}

////////////////////////////////////////////////////////////////
