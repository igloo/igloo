/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igFluxEuler3D.h"

#include <fstream>
#include <iostream>
#include <math.h>
#include <igCore/igAssert.h>
#include <igCore/igBoundaryIds.h>

///////////////////////////////////////////////////////////////

igFluxEuler3D::igFluxEuler3D(int variable_number, int dimension, bool flag_ALE) : igFluxCompressibleFlow3D(variable_number, dimension, flag_ALE)
{

}

igFluxEuler3D::~igFluxEuler3D(void)
{

}

///////////////////////////////////////////////////////////////

void igFluxEuler3D::physical(const vector<double> *state, const vector<double> *derivative, const vector<double> *mesh_vel, vector<double> *flux)
{
    IG_ASSERT(state, "no state");
    IG_ASSERT(derivative, "no derivative");
    IG_ASSERT(flux, "no flux");

    this->convective_physical_flux(state, mesh_vel, flux);

}

/////////////////////////////////////////////////////////////////

void igFluxEuler3D::numerical(const vector<double> *state_left, const vector<double> *derivative_left,
                            const vector<double> *state_right, const vector<double> *derivative_right,
                            vector<double> *flux_normal_left, vector<double> *flux_normal_right,
                            const vector<double> *mesh_vel, const double *normal)
{
    IG_ASSERT(state_left, "no state_left");
    IG_ASSERT(state_right, "no state_right");
    IG_ASSERT(derivative_left, "no derivative_left");
    IG_ASSERT(derivative_right, "no derivative_right");
    IG_ASSERT(flux_normal_left, "no flux_normal_left");
    IG_ASSERT(flux_normal_right, "no flux_normal_right");
    IG_ASSERT(normal, "no normal");


    // wave speed at left and right
    double max_wave_speed = max(this->maxInterfacialWaveSpeed(state_left,mesh_vel,normal), this->maxInterfacialWaveSpeed(state_right,mesh_vel,normal));
    double min_wave_speed = min(this->minInterfacialWaveSpeed(state_left,mesh_vel,normal), this->minInterfacialWaveSpeed(state_right,mesh_vel,normal));

    // flux left and right
    this->physical(state_left,derivative_left,mesh_vel,convective_flux_left);
    this->physical(state_right,derivative_right,mesh_vel,convective_flux_right);

    for(int ivar=0; ivar<variable_number; ivar++){

        double *conv_flux_var_left  = &(*convective_flux_left)[ivar*dimension];
        double *conv_flux_var_right = &(*convective_flux_right)[ivar*dimension];

        // normal flux at left and right
        double normal_conv_flux_left = 0.;
        double normal_conv_flux_right = 0.;
        for (int idim = 0; idim < dimension; ++idim) {
            normal_conv_flux_left  +=  conv_flux_var_left[idim] * normal[idim];
            normal_conv_flux_right += conv_flux_var_right[idim] * normal[idim];
        }

        // computation of the numerical flux (HLL)
        double flux = this->hllFlux(normal_conv_flux_left, normal_conv_flux_right,
                (*state_left)[ivar], (*state_right)[ivar], min_wave_speed, max_wave_speed);

        (*flux_normal_left)[ivar]  = flux;
        (*flux_normal_right)[ivar] = flux;

    }

}

/////////////////////////////////////////////////////////////////

void igFluxEuler3D::boundaryFlux(const vector<double> *state_left, const vector<double> *derivative_left, const vector<double> *state_right, const vector<double> *derivative_right, vector<double> *flux_normal_left, vector<double> *flux_normal_right, const vector<double> *mesh_vel, const double *normal, int face_type)
{

    if(face_type == id_slip_wall || face_type == id_slip_wall_fsi) {

        this->physical(state_right,derivative_right,mesh_vel,convective_flux_left);

        // computation of normal flux
        for (int ivar = 0; ivar < variable_number; ++ivar) {

            double *conv_flux_var_left = &(*convective_flux_left)[ivar*dimension];

            // normal flux at left
            double normal_conv_flux_left = 0.;
            for (int idim = 0; idim < dimension; ++idim) {
                normal_conv_flux_left += conv_flux_var_left[idim] * normal[idim];
            }
            (*flux_normal_left)[ivar] = normal_conv_flux_left;
        }

    } else {

        // numerical flux for all other cases

        this->numerical(state_left, derivative_left, state_right, derivative_right,
                        flux_normal_left, flux_normal_right, mesh_vel, normal);
    }
}

//////////////////////////////////////////////////////////////////
