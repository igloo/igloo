/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igFluxNavierStokesGradient2D.h"

#include <fstream>
#include <iostream>
#include <math.h>
#include <igCore/igAssert.h>
#include <igCore/igBoundaryIds.h>

///////////////////////////////////////////////////////////////

igFluxNavierStokesGradient2D::igFluxNavierStokesGradient2D(int variable_number, int dimension, bool flag_ALE) : igFlux(variable_number, dimension, flag_ALE)
{
    // specific size for gradient
    this->equation_number = variable_number*dimension;

    flux_left = new vector<double>(equation_number*dimension,0.);
    flux_right = new vector<double>(equation_number*dimension,0.);
}

igFluxNavierStokesGradient2D::~igFluxNavierStokesGradient2D(void)
{
    delete flux_left;
    delete flux_right;
}

///////////////////////////////////////////////////////////////

void igFluxNavierStokesGradient2D::physical(const vector<double> *state, const vector<double> *derivative, const vector<double> *mesh_vel, vector<double> *flux)
{
    IG_ASSERT(state, "no state");
    IG_ASSERT(derivative, "no derivative");
    IG_ASSERT(flux, "no flux");

    int index = 0.;

    for(int ivar=0; ivar<variable_number; ivar++){

        // variable dw0 / dx
        (*flux)[index+0] = -(*state)[ivar];
        (*flux)[index+1] = 0.;

        // variable dw0 / dy
        (*flux)[index+2] = 0.;
        (*flux)[index+3] = -(*state)[ivar];

        index += 4;
    }

}

///////////////////////////////////////////////////////////////

void igFluxNavierStokesGradient2D::numerical(const vector<double> *state_left, const vector<double> *derivative_left, const vector<double> *state_right, const vector<double> *derivative_right, vector<double> *flux_normal_left, vector<double> *flux_normal_right, const vector<double> *mesh_vel, const double *normal)
{
    IG_ASSERT(state_left, "no state_left");
    IG_ASSERT(state_right, "no state_right");
    IG_ASSERT(derivative_left, "no derivative_left");
    IG_ASSERT(derivative_right, "no derivative_right");
    IG_ASSERT(flux_normal_left, "no flux_normal_left");
    IG_ASSERT(flux_normal_right, "no flux_normal_right");
    IG_ASSERT(normal, "no normal");

    // computation of physical fluxes left and right
    this->physical(state_left, derivative_left, mesh_vel, flux_left);
    this->physical(state_right, derivative_right, mesh_vel, flux_right);

    // loop over gradient variables
    for (int ivar = 0; ivar < equation_number; ++ivar) {

        double *flux_var_left  = &(*flux_left)[ivar*dimension];
        double *flux_var_right = &(*flux_right)[ivar*dimension];

        double normal_flux_left = 0.;
        double normal_flux_right = 0.;
        for (int idim = 0; idim < dimension; ++idim) {
            normal_flux_left  += flux_var_left[idim]  * normal[idim];
            normal_flux_right += flux_var_right[idim] * normal[idim];
        }

        // computation of the numerical flux (centered)
        double flux = this->centeredFlux(normal_flux_left,normal_flux_right);

        (*flux_normal_left)[ivar] = flux;
        (*flux_normal_right)[ivar] = flux;
    }
}

////////////////////////////////////////////////////////

void igFluxNavierStokesGradient2D::boundaryFlux(const vector<double> *state_left, const vector<double> *derivative_left, const vector<double> *state_right, const vector<double> *derivative_right, vector<double> *flux_normal_left, vector<double> *flux_normal_right, const vector<double> *mesh_vel, const double *normal, int face_type)
{

    if(face_type == id_slip_wall || face_type == id_noslip_wall || face_type == id_slip_wall_fsi || face_type == id_noslip_wall_fsi){ // specific treatment for walls

        // computation of physical fluxes FOR BOUNDARY STATE
        this->physical(state_right, derivative_right, mesh_vel, flux_left);

        // computation of normal flux
        for (int ivar = 0; ivar < equation_number; ++ivar) {

            double *flux_var_left = &(*flux_left)[ivar*dimension];

            // normal flux at left
            double normal_flux_left = 0.;
            for (int idim = 0; idim < dimension; ++idim) {
                normal_flux_left += flux_var_left[idim] * normal[idim];
            }

            (*flux_normal_left)[ivar] = normal_flux_left ;
        }

    } else {
        this->numerical(state_left, derivative_left, state_right, derivative_right,
                        flux_normal_left, flux_normal_right, mesh_vel, normal);
    }
}

//////////////////////////////////////////////////////////////

double igFluxNavierStokesGradient2D::waveSpeed(const vector<double> *, const vector<double> *)
{
    return 0;
}

double igFluxNavierStokesGradient2D::interfacialWaveSpeed(const vector<double> *, const vector<double> *, const double *)
{
    return 0;
}
