/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igCouplingInterfaceMembrane.h"

#include "igBasisBSpline.h"

#include <igCore/igBasisBSpline.h>
#include <igCore/igQuadrature.h>
#include <igCore/igMesh.h>
#include <igCore/igFace.h>
#include <igCore/igBoundaryIds.h>
#include <igDistributed/igCommunicator.h>

#include <string>
#include <iostream>
#include <limits>
#include <vector>
#include <math.h>

///////////////////////////////////////////////////////////////

igCouplingInterfaceMembrane::igCouplingInterfaceMembrane(void) : igCouplingInterface()
{
    this->interface_gauss_pts_local = new vector<double>;
    this->interface_gauss_wgt_local = new vector<double>;
    this->interface_gauss_label_local = new vector<int>;
}

igCouplingInterfaceMembrane::~igCouplingInterfaceMembrane(void)
{
    if(this->basis){
        delete this->basis;
    }

    delete this->interface_gauss_pts_local ;
    delete this->interface_gauss_wgt_local ;
    delete this->interface_gauss_label_local ;
}

////////////////////////////////////////////////////////////

void igCouplingInterfaceMembrane::initializeStructureInterface(void)
{
    // set main structural parameters
    this->ctrl_pt_number = pt_x->size();
    this->degree = knot_vector->size() - ctrl_pt_number -1;
    this->gauss_pt_number = degree+1;
    this->elt_number = ctrl_pt_number - degree;

    // interface arrays for the structure
    interface_displacements_s->resize(ctrl_pt_number, 0);
    interface_velocities_s->resize(ctrl_pt_number, 0);

    // initialise basis functions
    this->basis = new igBasisBSpline(degree, knot_vector);
}


void igCouplingInterfaceMembrane::initializeFluidInterface(int quadrature_number, int point_number, int dimension)
{
    // storage of local efforts
    interface_efforts_f->resize(quadrature_number,0);
    interface_efforts_s->resize(quadrature_number,0);

    // storage of points displacement (correction because of upper/lower faces)
    interface_displacements_f->resize(point_number/2, 0);
    interface_velocities_f->resize(point_number/2, 0);
}

void igCouplingInterfaceMembrane::initializeQuadratureMapping(int quadrature_number)
{
    // reset arrays
    interface_quadrature_counter_f->clear();
    interface_quadrature_counter_f->resize(communicator->size(),0);
    interface_quadrature_counter_s->clear();
    interface_quadrature_counter_s->resize(communicator->size(),0);

    // fill local counter
    interface_quadrature_counter_f->at(communicator->rank()) = quadrature_number;

    // distribute to fill global counter array
    communicator->distributeInterfaceInt(interface_quadrature_counter_f->data(),interface_quadrature_counter_s->data(),interface_quadrature_counter_f->size());

    // index for storage of local data into global arrays
    index_storage = 0;
    for(int iproc=0; iproc<communicator->rank(); iproc++){
        index_storage += interface_quadrature_counter_s->at(iproc);
    }

}

void igCouplingInterfaceMembrane::initializeQuadrature(igMesh *mesh)
{
    //-------- first step : initialize membrane quadrature arrays ----------

    int  gauss_pt_number_f = mesh->face(0)->gaussPointNumber();

    igQuadrature quadrature;
    vector<double> gauss_point_coord_list;
    vector<double> gauss_point_weight_list;
    gauss_point_coord_list.assign(gauss_pt_number_f,0.);
    gauss_point_weight_list.assign(gauss_pt_number_f,0.);
    quadrature.initializeGaussPoints(1,gauss_pt_number_f,gauss_point_coord_list,gauss_point_weight_list);

    // interface array for quadrature points (parametric coord + weights) on both sides
    interface_gauss_pts->resize(gauss_pt_number_f*elt_number*2, 0);
    interface_gauss_wgt->resize(gauss_pt_number_f*elt_number*2, 0);

    // set labels to know orientation
    interface_gauss_label->resize(gauss_pt_number_f*elt_number*2);
    for(int i=0; i<interface_gauss_label->size()/2; i++){
        interface_gauss_label->at(i) = 1;
    }
    for(int i=interface_gauss_label->size()/2; i<interface_gauss_label->size(); i++){
        interface_gauss_label->at(i) = -1;
    }

    // interface array to store fluid efforts
    interface_efforts_s->resize(gauss_pt_number_f*elt_number*2, 0);

    // fill initial quadrature point parameters + weights (both sides)
    for (int iel=0; iel<elt_number; iel++){

        int span_index = iel+degree;
        double interval = knot_vector->at(span_index+1) - knot_vector->at(span_index);

        for (int k=0; k<gauss_pt_number_f; k++){
            interface_gauss_pts->at(iel*gauss_pt_number_f+k) = knot_vector->at(span_index) + gauss_point_coord_list[k] * interval;
            interface_gauss_wgt->at(iel*gauss_pt_number_f+k) = gauss_point_weight_list[k] * interval;
        }
    }
    for (int i=0; i<elt_number*gauss_pt_number_f; i++){
        interface_gauss_pts->at(elt_number*gauss_pt_number_f+i) = interface_gauss_pts->at(i);
        interface_gauss_wgt->at(elt_number*gauss_pt_number_f+i) = interface_gauss_wgt->at(i);
    }

    //------ second : set face data by coordinate matching --------------

    vector<double> values;
    values.resize(ctrl_pt_number);

    // loop over fsi faces (level 0)
    for(int ifac=0; ifac<mesh->boundaryFaceNumber(); ifac++){
        igFace *face = mesh->boundaryFace(ifac);

        if(face->type() == id_slip_wall_fsi || face->type() == id_noslip_wall_fsi){

            int  gauss_pt_number_f = face->gaussPointNumber();

            // loop over face Gauss points
            for(int k=0; k<gauss_pt_number_f; k++){

                double x_gauss_face = face->gaussPointPhysicalCoordinate(k,0);
                int index_gauss = -1;
                double dist_x_min = std::numeric_limits<double>::max();

                // loop over interface Gauss points
                for( int igauss=0; igauss<interface_gauss_pts->size(); igauss++){

                    double par = interface_gauss_pts->at(igauss);
                    basis->evalFunction(par,values);

                    double x_gauss = 0;
                    for(int i=0; i<ctrl_pt_number; i++){
                        x_gauss += values[i] * pt_x->at(i);
                    }
                    double dist_x = fabs(x_gauss - x_gauss_face);

                    double normal_y = face->gaussPointNormalPtr(k)[1];

                    if(dist_x < dist_x_min && normal_y*interface_gauss_label->at(igauss)>0 ){
                        dist_x_min = dist_x;
                        index_gauss = igauss;
                    }
                }

                // store data into face: quadrature coordinate + weight
                face->setInterfaceQuadratureCoordinate(k,interface_gauss_pts->at(index_gauss));
                face->setInterfaceQuadratureWeight(k,interface_gauss_wgt->at(index_gauss));
                face->setInterfaceQuadratureLabel(k,interface_gauss_label->at(index_gauss));

            }
        }

    }

}

void igCouplingInterfaceMembrane::initializeMapping(igMesh *mesh)
{
    vector<double> values;
    values.resize(ctrl_pt_number);

    // clear map for adaption
    interface_mapping->clear();

    // loop over fsi faces
    for(int ifac=0; ifac<mesh->boundaryFaceNumber(); ifac++){
        igFace *face = mesh->boundaryFace(ifac);
        if(face->isActive()){
            if(face->type() == id_slip_wall_fsi || face->type() == id_noslip_wall_fsi){

                int  gauss_pt_number_f = face->gaussPointNumber();

                // loop over face Gauss points
                for(int k=0; k<gauss_pt_number_f; k++){

                    double x_gauss_face = face->gaussPointPhysicalCoordinate(k,0);
                    int index_gauss = -1;
                    double dist_x_min = std::numeric_limits<double>::max();

                    // loop over interface Gauss points
                    for( int igauss=0; igauss<interface_gauss_pts->size(); igauss++){

                        double par = interface_gauss_pts->at(igauss);
                        basis->evalFunction(par,values);

                        double x_gauss = 0;
                        for(int i=0; i<ctrl_pt_number; i++){
                            x_gauss += values[i] * pt_x->at(i);
                        }
                        double dist_x = fabs(x_gauss - x_gauss_face);

                        double normal_y = face->gaussPointNormalPtr(k)[1];

                        if(dist_x < dist_x_min && normal_y*interface_gauss_label->at(igauss)>0 ){
                            dist_x_min = dist_x;
                            index_gauss = igauss;
                        }
                    }

                    // store mapping
                    interface_mapping->push_back(index_gauss);

                }
            }
        }
    }

}

void igCouplingInterfaceMembrane::fillQuadrature(igMesh *mesh, int global_quadrature_number)
{

    // clean global quadrature arrays
    interface_gauss_pts->clear();
    interface_gauss_pts->resize(global_quadrature_number);
    interface_gauss_wgt->clear();
    interface_gauss_wgt->resize(global_quadrature_number);
    interface_gauss_label->clear();
    interface_gauss_label->resize(global_quadrature_number);

    // clean local quadrature arrays
    interface_gauss_pts_local->clear();
    interface_gauss_pts_local->resize(global_quadrature_number);
    interface_gauss_wgt_local->clear();
    interface_gauss_wgt_local->resize(global_quadrature_number);
    interface_gauss_label_local->clear();
    interface_gauss_label_local->resize(global_quadrature_number);

    int local_counter = 0;

    // loop over fsi faces to fill local arrays
    for(int ifac=0; ifac<mesh->boundaryFaceNumber(); ifac++){
        igFace *face = mesh->boundaryFace(ifac);
        if(face->isActive()){
            if(face->type() == id_slip_wall_fsi || face->type() == id_noslip_wall_fsi){

                int  gauss_pt_number_f = face->gaussPointNumber();

                // loop over face Gauss points
                for(int k=0; k<gauss_pt_number_f; k++){

                    // retrieve interface data
                    double weight = face->interfaceQuadratureWeight(k);
                    double par = face->interfaceQuadratureCoordinate(k);
                    int label = face->interfaceQuadratureLabel(k);

                    // fill interface arrays
                    interface_gauss_pts_local->at(index_storage+local_counter) = par;
                    interface_gauss_wgt_local->at(index_storage+local_counter) = weight;
                    interface_gauss_label_local->at(index_storage+local_counter) = label;

                    local_counter++;
                }
            }
        }
    }

    // communicate quadrature arrays
    communicator->distributeInterfaceDouble(interface_gauss_pts_local->data(),interface_gauss_pts->data(),interface_gauss_pts->size());
    communicator->distributeInterfaceDouble(interface_gauss_wgt_local->data(),interface_gauss_wgt->data(),interface_gauss_wgt->size());
    communicator->distributeInterfaceInt(interface_gauss_label_local->data(),interface_gauss_label->data(),interface_gauss_label->size());


}

///////////////////////////////////////////////////////////

void igCouplingInterfaceMembrane::addLocalEffort(int counter, double Fx_inv, double Fx_vis, double Fy_inv, double Fy_vis, double Mz, double area, double nx, double ny)
{
    double sign = ny/fabs(ny);

    // store normal pressure contribution
    interface_efforts_f->at(interface_mapping->at(counter)) += (Fx_inv*nx + Fy_inv*ny) * sign;

}

/////////////////////////////////////////////////////////////

void igCouplingInterfaceMembrane::convertFluidEffortsToStructure(void)
{
    // distribute the effort distribution along interface
    communicator->distributeInterfaceDouble(interface_efforts_f->data(),interface_efforts_s->data(),interface_efforts_f->size());

}

void igCouplingInterfaceMembrane::convertStructureDisplacementsToFluid(void)
{
    vector<vector<double>> elt_x;
    vector<vector<double>> elt_y;

    // extraction of displacement
    basis->extractBezier(pt_x, interface_displacements_s, &elt_x, &elt_y);

    // build fluid interface array
    int counter = 0;

    for(int i=0; i<elt_number; i++){
        for(int j=0; j<degree+1; j++){
            interface_displacements_f->at(counter) = elt_y.at(i).at(j);
            counter++;
        }
    }
}

void igCouplingInterfaceMembrane::convertStructureVelocitiesToFluid(void)
{
    vector<vector<double>> elt_x;
    vector<vector<double>> elt_y;

    // extraction of velocity
    basis->extractBezier(pt_x, interface_velocities_s, &elt_x, &elt_y);

    // build fluid interface array
    int counter = 0;

    for(int i=0; i<elt_number; i++){
        for(int j=0; j<degree+1; j++){
            interface_velocities_f->at(counter) = elt_y.at(i).at(j);
            counter++;
        }
    }
}

/////////////////////////////////////////////////////////////

string igCouplingInterfaceMembrane::name(void)
{
    return "membrane";
}
