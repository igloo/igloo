/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igSolverAcoustic.h"

#include <igFluxAcoustic.h>

#include <igCore/igMesh.h>
#include <igCore/igElement.h>
#include <igCore/igFace.h>
#include <igCore/igBoundaryIds.h>

#include <igGenerator/igSolutionAnalyticAcoustic.h>

#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <math.h>
#include <limits>


///////////////////////////////////////////////////////////////

igSolverAcoustic::igSolverAcoustic(int variable_number, int dimension, vector<double> *solver_parameters, bool flag_ALE) : igSolver(variable_number, dimension, solver_parameters, flag_ALE)
{
    flux = new igFluxAcoustic(variable_number, dimension, flag_ALE);
}

igSolverAcoustic::~igSolverAcoustic(void)
{

}

/////////////////////////////////////////////////////////////

void igSolverAcoustic::computeIncrement(vector<double> *increment)
{
    // reset residual list
    this->resetResidual();

    // update time for time-dependent cases
    flux->setTime(time);

    // compute the different DG terms
    this->volumicIntegrator(flux,residual_list);
    this->surfacicIntegrator(flux,residual_list);
    this->boundaryIntegrator(flux,residual_list);

    this->updateSolution(increment,residual_list,variable_number);
}

void igSolverAcoustic::computeIncrementAle(vector<double> *increment)
{
    // reset residual list
    this->resetResidual();

    // update time for time-dependent cases
    flux->setTime(time);

    // compute the different DG terms
    this->volumicIntegrator(flux,residual_list);
    this->surfacicIntegrator(flux,residual_list);
    this->boundaryIntegrator(flux,residual_list);

    for(int idof=0; idof<residual_list->size(); idof++)
    	increment->at(idof) = residual_list->at(idof);

}


//////////////////////////////////////////////////////////////

double igSolverAcoustic::timeStep(void)
{
	vector<double> *mesh_vel = new vector<double>(this->dimension,0.);
    double time_step = numeric_limits<double>::max();

    // for time-dependent cases:
    flux->setTime(time);

    vector<double> *local_state = new vector<double>;
    local_state->resize(variable_number);
    vector<double> *derivative = new vector<double>;
    derivative->resize(variable_number);

    for (int iel=0; iel<mesh->elementNumber(); iel++) {

        igElement *elt = mesh->element(iel);

        if (elt->isActive()) {

        	// NB : this is an (over) estimate of wave speed (cf convex properties of NURBS)
        	double max_wave_speed = 0;
        	for (int idof=0; idof<elt->controlPointNumber(); idof++){
        		if(ALE_formulation)
        			elt->controlPointVelocity(idof,mesh_vel);
        		this->extractState(elt,idof,local_state,derivative);
        		max_wave_speed = fmax(max_wave_speed,flux->waveSpeed(local_state,mesh_vel));
        	}

            double diameter = 2*elt->radius() / (1. + 2*elt->cellDegree());
            double local_step = diameter / max_wave_speed;
            time_step = fmin(time_step,local_step);

        }
    }

    delete local_state;
    delete derivative;
    delete mesh_vel;

    return time_step * this->cfl_coefficient;
}

//////////////////////// Boundary solution /////////////////////////////////////////

void igSolverAcoustic::boundarySolution(igFace *face, int index, double time, vector<double> *state_interior, vector<double> *state, vector<double> *mesh_vel)
{

    double u_int = state_interior->at(0);
    double v_int = state_interior->at(1);
    double p_int = state_interior->at(2);

    double *normal = face->gaussPointNormalPtr(index);

    double u;
    double v;
    double p;

    //---------- wall conditions ----------

    if(face->type() == id_acoustic_wall ){

        double normal_velocity_int = u_int*normal[0] + v_int*normal[1];
        double normal_wall_velocity = mesh_vel->at(0)*normal[0] +  mesh_vel->at(1)*normal[1];

        u = u_int + 2*(normal_wall_velocity - normal_velocity_int)*normal[0];
        v = v_int + 2*(normal_wall_velocity - normal_velocity_int)*normal[1];
        p = p_int;


        state->at(0) = u;
        state->at(1) = v;
        state->at(2) = p;

    }

    //---------- symmetry conditions ----------

    else if(face->type() == id_acoustic_sym){

        double normal_velocity_int = u_int*normal[0] + v_int*normal[1];

        u = u_int - 2*normal_velocity_int*normal[0];
        v = v_int - 2*normal_velocity_int*normal[1];
        p = p_int;

        state->at(0) = u;
        state->at(1) = v;
        state->at(2) = p;
    }

    else { // other analytical cases

      this->exactSolution(face->gaussPointPhysicalCoordinate(index),time,state,face->type(),0.,0.);
    }

}

//////////////////////// exact solution /////////////////////////////////////////


void igSolverAcoustic::exactSolution(double *position, double time, vector<double> *solution, int solution_id, double x_elt, double y_elt)
{
    double *generic_param;

    // annular resonnator case
    for(int ivar=0; ivar<variable_number; ivar++)
        solution->at(ivar) = solution_acoustic_annular(position,  time, ivar, generic_param, generic_param);

}


////////////////////////////////////////////////////////////////
