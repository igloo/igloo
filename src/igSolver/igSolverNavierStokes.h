/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igSolverExport.h>

#include "igSolverCompressibleFlow.h"

#include <fstream>
#include <vector>

class igMesh;
class igElement;
class igFlux;

class IGSOLVER_EXPORT igSolverNavierStokes : public igSolverCompressibleFlow
{
public:
     igSolverNavierStokes(int variable_number, int dimension, vector<double> *solver_parameters, bool flag_ALE);
    ~igSolverNavierStokes(void);

public:
    void computeIncrement(vector<double> *increment) override;
    void computeIncrementAle(vector<double> *increment) override;
    double timeStep(void) override;

    void applyLimiter(double shock_capturing_coef) override;

    void setSmoothingViscosityNumber(int number) override;

    void computeEfforts(void) override;
    void computeVorticity(void) override;

private:
    void initializeDerivative(void) override;
    void resetDerivative(void) override;
    void resetGradData(void);
    double artificialViscosity(int index);

    void boundarySolution(igFace *face, int index, double time, vector<double> *state_interior, vector<double> *state, vector<double> *mesh_vel) override;
    void boundaryDerivative(igFace *face, int index, double time, vector<double> *derivative_interior, vector<double> *derivative) override;

    void artificialViscosityAtGaussPoint(igFace *face, int index_pt, double &left_artificial_viscosity, double &right_artificial_viscosity) override;
    void artificialViscosityAtGaussPoint(igFace *face, int index_pt, double &left_artificial_viscosity) override;
    void artificialViscosityAtGaussPoint(igElement *element, int index_pt, double &artificial_viscosity) override;
    void extractArtificialViscosity(igElement *element, int index_pt, double &artificial_viscosity) override;

    void viscositySmoothing(void);


private:
    igFlux *flux;
    igFlux *grad_flux;

    double diffusion_coefficient;
    double reynolds_ref;

    double current_time_step;

    vector<double> *smooth_viscosity_list;
    vector<int> *smooth_viscosity_contrib;

    int smoothing_step_number;

};


//
// igSolverNavierStokes.h ends here
