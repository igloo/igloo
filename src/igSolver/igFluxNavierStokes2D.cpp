/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igFluxNavierStokes2D.h"

#include <fstream>
#include <iostream>
#include <math.h>
#include <igCore/igAssert.h>
#include <igCore/igBoundaryIds.h>

///////////////////////////////////////////////////////////////

igFluxNavierStokes2D::igFluxNavierStokes2D(int variable_number, int dimension, bool flag_ALE) : igFluxCompressibleFlow2D(variable_number, dimension, flag_ALE)
{

    diffusive_flux_left  = new vector<double>(variable_number*dimension, 0.);
    diffusive_flux_right = new vector<double>(variable_number*dimension, 0.);

    prandtl = 0.72;

    local_viscosity = 0;
    local_viscosity_left = 0;
    local_viscosity_right = 0;

}

igFluxNavierStokes2D::~igFluxNavierStokes2D(void)
{
    delete diffusive_flux_left;
    delete diffusive_flux_right;
}

///////////////////////////////////////////////////////////////

void igFluxNavierStokes2D::physical(const vector<double> *state, const vector<double> *derivative, const vector<double> *mesh_vel, vector<double> *flux)
{
    IG_ASSERT(state, "no state");
    IG_ASSERT(derivative, "no derivative");
    IG_ASSERT(flux, "no flux");

    this->convective_physical_flux(state, mesh_vel, convective_flux_left);
    this->diffusive_physical_flux(state, derivative, local_viscosity, diffusive_flux_left);

    for (int ivar = 0; ivar < equation_number*dimension; ++ivar) {
        (*flux)[ivar] = (*convective_flux_left)[ivar] + (*diffusive_flux_left)[ivar];
    }
}


/////////////////////////////////////////////////////////////////

void igFluxNavierStokes2D::numerical(const vector<double> *state_left, const vector<double> *derivative_left,
                                   const vector<double> *state_right, const vector<double> *derivative_right,
                                   vector<double> *flux_normal_left, vector<double> *flux_normal_right,
                                   const vector<double> *mesh_vel, const double *normal)
{
    IG_ASSERT(state_left, "no state_left");
    IG_ASSERT(state_right, "no state_right");
    IG_ASSERT(derivative_left, "no derivative_left");
    IG_ASSERT(derivative_right, "no derivative_right");
    IG_ASSERT(flux_normal_left, "no flux_normal_left");
    IG_ASSERT(flux_normal_right, "no flux_normal_right");
    IG_ASSERT(normal, "no normal");

    // wave speed at left and right
    double max_wave_speed = max(this->maxInterfacialWaveSpeed(state_left,mesh_vel,normal), this->maxInterfacialWaveSpeed(state_right,mesh_vel,normal));
    double min_wave_speed = min(this->minInterfacialWaveSpeed(state_left,mesh_vel,normal), this->minInterfacialWaveSpeed(state_right,mesh_vel,normal));

    // computation of convective fluxes
    this->convective_physical_flux(state_left, mesh_vel, convective_flux_left);
    this->convective_physical_flux(state_right, mesh_vel, convective_flux_right);

    // computation of diffusive fluxes
    this->diffusive_physical_flux(state_left, derivative_left, local_viscosity_left, diffusive_flux_left);
    this->diffusive_physical_flux(state_right, derivative_right, local_viscosity_right, diffusive_flux_right);

    // loop over variables
    for (int ivar = 0; ivar < variable_number; ++ivar) {

        double *conv_flux_var_left  = &(*convective_flux_left)[ivar*dimension];
        double *conv_flux_var_right = &(*convective_flux_right)[ivar*dimension];
        double *diff_flux_var_left  = &(*diffusive_flux_left)[ivar*dimension];
        double *diff_flux_var_right = &(*diffusive_flux_right)[ivar*dimension];

        // normal flux at left and right
        double normal_conv_flux_left = 0.;
        double normal_conv_flux_right = 0.;
        double normal_diff_flux_left = 0.;
        double normal_diff_flux_right = 0.;
        for (int idim = 0; idim < dimension; ++idim) {
            normal_conv_flux_left  +=  conv_flux_var_left[idim] * normal[idim];
            normal_conv_flux_right += conv_flux_var_right[idim] * normal[idim];

            normal_diff_flux_left  +=  diff_flux_var_left[idim] * normal[idim];
            normal_diff_flux_right += diff_flux_var_right[idim] * normal[idim];
        }

        // convective numerical flux (HLL)
        double flux = this->hllFlux(normal_conv_flux_left, normal_conv_flux_right,
                                    (*state_left)[ivar], (*state_right)[ivar],
                                    min_wave_speed, max_wave_speed);

        // diffusive numerical flux (centered)
        flux += this->centeredFlux(normal_diff_flux_left, normal_diff_flux_right);

        (*flux_normal_left)[ivar]  = flux;
        (*flux_normal_right)[ivar] = flux;
    }
}

//////////////////////////////////////////////////////////////

void igFluxNavierStokes2D::boundaryFlux(const vector<double> *state_left, const vector<double> *derivative_left, const vector<double> *state_right, const vector<double> *derivative_right, vector<double> *flux_normal_left, vector<double> *flux_normal_right, const vector<double> *mesh_vel, const double *normal, int face_type)
{
    double nx = normal[0];
    double ny = normal[1];

    if(face_type == id_slip_wall || face_type == id_noslip_wall || face_type == id_slip_wall_fsi || face_type == id_noslip_wall_fsi) { // specific treatment for adiabatic walls

        // computation of physical fluxes FOR BOUNDARY STATES
        this->convective_physical_flux(state_right, mesh_vel, convective_flux_left);
        this->diffusive_physical_flux(state_right, derivative_right, local_viscosity_right, diffusive_flux_left);

        // computation of normal flux
        for (int ivar = 0; ivar < variable_number; ++ivar) {

            double *conv_flux_var_left = &(*convective_flux_left)[ivar*dimension];
            double *diff_flux_var_left = &(*diffusive_flux_left)[ivar*dimension];

            // normal flux at left
            double normal_conv_flux_left = 0.;
            double normal_diff_flux_left = 0.;
            for (int idim = 0; idim < dimension; ++idim) {
                normal_conv_flux_left += conv_flux_var_left[idim] * normal[idim];
                normal_diff_flux_left += diff_flux_var_left[idim] * normal[idim];
            }
            (*flux_normal_left)[ivar] = normal_conv_flux_left + normal_diff_flux_left;
        }

        // modification of flux for adiabaticity condition
        (*flux_normal_left)[1+dimension] -= qx*nx + qy*ny; // energy flux


    } else {
        this->numerical(state_left, derivative_left, state_right, derivative_right,
                        flux_normal_left, flux_normal_right, mesh_vel, normal);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////

void igFluxNavierStokes2D::diffusive_physical_flux(const vector<double> *state, const vector<double> *derivative, double local_viscosity, vector<double> *flux)
{

    double rho =  (*state)[0];
    double u = (*state)[1]/rho;
    double v = (*state)[2]/rho;
    double ei = (*state)[3]/rho -0.5*(u*u+v*v);

    double DrhoDx = (*derivative)[0];
    double DrhoDy = (*derivative)[1];

    double DuDx = ((*derivative)[2] - DrhoDx*u)/rho;
    double DuDy = ((*derivative)[3] - DrhoDy*u)/rho;

    double DvDx = ((*derivative)[4] - DrhoDx*v)/rho;
    double DvDy = ((*derivative)[5] - DrhoDy*v)/rho;

    double DeiDx = ((*derivative)[6] - 0.5*DrhoDx*(u*u+v*v) - rho*(u*DuDx+v*DvDx) - DrhoDx*ei )/rho;
    double DeiDy = ((*derivative)[7] - 0.5*DrhoDy*(u*u+v*v) - rho*(u*DuDy+v*DvDy) - DrhoDy*ei )/rho;

    double Txx = 2./3. * (diffusion_coefficient+local_viscosity) * ( 2*DuDx - DvDy);
    double Txy =  (diffusion_coefficient+local_viscosity) * ( DuDy + DvDx);
    double Tyy = 2./3. * (diffusion_coefficient+local_viscosity) * ( 2*DvDy - DuDx);

    qx = -gamma*(diffusion_coefficient+local_viscosity)/prandtl * DeiDx;
    qy = -gamma*(diffusion_coefficient+local_viscosity)/prandtl * DeiDy;


    // density flux
    (*flux)[0] = 0.;
    (*flux)[1] = 0.;

    // momentum-x flux
    (*flux)[2] = -Txx;
    (*flux)[3] = -Txy;

    // momentum-y flux
    (*flux)[4] = -Txy;
    (*flux)[5] = -Tyy;

    // energy flux
    (*flux)[6] = -u*Txx - v*Txy + qx;
    (*flux)[7] = -u*Txy - v*Tyy + qy;

}


//////////////////////////////////////////////////////////////

void igFluxNavierStokes2D::setDiffusionCoefficient(double coefficient)
{
    diffusion_coefficient = coefficient;
}

//////////////////////////////////////////////////////////////

void igFluxNavierStokes2D::setArtificialViscosity(double artificial_viscosity)
{
    local_viscosity = artificial_viscosity;
}

void igFluxNavierStokes2D::setArtificialViscosity(double artificial_viscosity_left, double artificial_viscosity_right)
{
    local_viscosity_left = artificial_viscosity_left;
    local_viscosity_right = artificial_viscosity_right;
}

//////////////////////////////////////////////////////////////////
