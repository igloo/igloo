/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igErrorEstimatorBox.h"

#include <iostream>
#include <math.h>

#include <igCore/igMesh.h>
#include <igCore/igCell.h>
#include <igCore/igElement.h>
#include <igCore/igFace.h>
#include <igCore/igAssert.h>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igErrorEstimatorBox::igErrorEstimatorBox(void) : igErrorEstimator()
{
    refine_counter = 0;
    refine_direction = 0;
}

igErrorEstimatorBox::~igErrorEstimatorBox(void)
{

}

/////////////////////////////////////////////////////////////////////////////

void igErrorEstimatorBox::setRegion(vector<double> &args)
{
    this->xbox_min = args[0];
    this->xbox_max = args[1];
    this->ybox_min = args[2];
    this->ybox_max = args[3];

    cout << endl;
    cout << "Box refinement:" << endl;
    cout << xbox_min << " " << xbox_max << endl;
    cout << ybox_min << " " << ybox_max << endl;
    cout << endl;

}

/////////////////////////////////////////////////////////////////////////////

void igErrorEstimatorBox::computeEstimator(void)
{
    error_xi->assign(mesh->elementNumber(),0.);
    error_eta->assign(mesh->elementNumber(),0.);

    double value_error_xi = 0;
    double value_error_eta = 0;

    refine_counter++;
    if(refine_direction == 0)
        value_error_xi = 1;
    else
        value_error_eta = 1;


    for(int iel=0; iel<mesh->elementNumber(); iel++){

    	igElement *elt = mesh->element(iel);
    	if(elt->isActive()){

            if(!subdomain_mode || elt->subdomain() == this->subdomain_to_refine){

        		if(elt->barycenter(0) < xbox_max && elt->barycenter(0) > xbox_min &&
        				elt->barycenter(1) < ybox_max && elt->barycenter(1) > ybox_min){

        			(*error_xi)[iel] = value_error_xi;
        			(*error_eta)[iel] = value_error_eta;

        		}
            }

    	}

    }

}
