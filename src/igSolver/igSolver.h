/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igSolverExport.h>

#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

class igMesh;
class igElement;
class igFace;
class igFlux;
class igSource;
class igCouplingInterface;
class igCommunicator;
class igLinAlg;

class IGSOLVER_EXPORT igSolver
{
public:
             igSolver(int variable_number, int dimension,vector<double> *solver_parameters, bool flag_ALE);
    virtual ~igSolver(void);

public:
    void setMesh(igMesh *input_mesh);
    void setState(vector<double> *state);
    void setTime(double current_time);
    void setCommunicator(igCommunicator *com);
    void setStabilityCoefficient(double coefficient);
    void setArtificialViscosity(vector<double> *artificial_viscosity);
    void setVorticity(vector<double> *vorticity);
    void setInterface(igCouplingInterface *interface);
    void enableShockCapturing(void);
    void enableCheckPositivity(void);

    void initialize(void);
    void resetResidual(void);

    void computeMassMatrix(void);
    void inverseMassMatrix(void);

    void computeJumpCriterion(vector<double> *jump_xi, vector<double> *jump_eta);
    void computeViscosityCriterion(vector<double> *err_xi, vector<double> *err_eta);

    void computeGlobalErrorNorm(void);
    void computeGlobalErrorNormWithFile(void);
    void computeLocalError(vector<double> *error, igElement *elt, double time);

    double computeVariationXi(igElement *elt, int ivar);
    double computeVariationEta(igElement *elt, int ivar);

    virtual void computeIncrement(vector<double> *increment) = 0;
    virtual void computeIncrementAle(vector<double> *increment) = 0;
    virtual double timeStep(void) = 0;

    virtual void applyLimiter(double shock_capturing_coef);

    virtual void setSmoothingViscosityNumber(int number);

    virtual void updateSolution(vector<double> *solution, vector<double> *residual, int equation_number);
    virtual void computeEfforts(void);
    virtual void computeVorticity(void);

protected:
    igLinAlg *lin_solver;
    igMesh *mesh;
    igCommunicator *communicator;

    vector<double> *solver_parameters;

    vector<double*> mass_matrix_list;
    vector<double> *state_list;
    vector<double> *grad_state_list;
    vector<double> *residual_list;
    vector<double> *grad_residual_list;

    vector<double> *artificial_viscosity_list;
    vector<double> *vorticity_list;

    igCouplingInterface *interface;

    bool shock_capturing;

    bool check_positivity;

    bool ALE_formulation;

    double cfl_coefficient;

    double time;

    int dimension;
    int variable_number;
    int derivative_number;

protected:
    ofstream effort_file;

protected:
    void volumicIntegrator(igFlux *flux_function, vector<double> *updated_residual);
    void surfacicIntegrator(igFlux *flux_function, vector<double> *updated_residual);
    void boundaryIntegrator(igFlux *flux_function, vector<double> *updated_residual);
    void sourceIntegrator(igSource *source_function, vector<double> *updated_residual);

protected:
    virtual void extractState(igElement *element, int index_pt, vector<double> *state, vector<double> *derivative);
    virtual void extractArtificialViscosity(igElement *element, int index_pt, double &artificial_viscosity);

    virtual void solutionAtGaussPoint(igElement *element, int index_pt, vector<double> *solution, vector<double> *derivative);
    virtual double solutionAtGaussPoint(igElement *element, int index_pt, int component);
    virtual double solutionLeftAtGaussPoint(igFace *face, int index_pt, int component);
    virtual double solutionRightAtGaussPoint(igFace *face, int index_pt, int component);
    virtual void solutionAtGaussPoint(igFace *face, int index_pt, vector<double> *solution_left, vector<double> *derivative_left, vector<double> *solution_right, vector<double> *derivative_right);
    virtual void solutionAtGaussPoint(igFace *face, int index_pt, vector<double> *solution_left, vector<double> *derivative_left);
    virtual void solutionJumpAtGaussPoint(igFace *face, int index_pt, vector<double> *solution_left, vector<double> *solution_right);
    virtual void solutionJumpAtGaussPoint(igFace *face, int index_pt, vector<double> *solution_left);
    virtual void solutionGradientAtGaussPoint(igElement *element, int index_pt, vector<double> *solution_gradient, vector<double> *derivative_gradient);
    virtual void artificialViscosityAtGaussPoint(igFace *face, int index_pt, double &left_artificial_viscosity, double &right_artificial_viscosity);
    virtual void artificialViscosityAtGaussPoint(igFace *face, int index_pt, double &left_artificial_viscosity);
    virtual void artificialViscosityAtGaussPoint(igElement *element, int index_pt, double &artificial_viscosity);

    double solutionAtArbitraryPoint(igElement *elt, double *coordinate, int component);
    double coordinateAtArbitraryPoint(igElement *elt, double *coordinate, int component);

    virtual void initializeDerivative(void);
    virtual void resetDerivative(void);

    virtual void boundarySolution(igFace *face, int index, double time, vector<double> *state_interior, vector<double> *state, vector<double> *mesh_vel);
    virtual void boundaryDerivative(igFace *face, int index, double time, vector<double> *derivative_interior, vector<double> *derivative);

    virtual void exactSolution(double *position, double time, vector<double> *solution, int solution_id, double x_elt, double y_elt);
    virtual void exactDerivative(double *position, double time, vector<double> *derivative, int solution_id, double x_elt, double y_elt);

    virtual void checkPositivity(void);

};

//
// igSolver.h ends here
