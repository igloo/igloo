/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igSolverExport.h>

#include "igStructuralSolver.h"

#include <fstream>
#include <vector>

using namespace std;

class igLinAlg;
class igBasisBSpline;

class IGSOLVER_EXPORT igStructuralSolverMembrane : public igStructuralSolver
{
public:
    igStructuralSolverMembrane(vector<double> *interface_gauss_pts, vector<double> *interface_gauss_wgt, vector<double> *interface_efforts, vector<double> *interface_displacements, vector<double> *interface_velocities);
    virtual ~igStructuralSolverMembrane(void);

public:
    void initialize(int degree, int ctrl_pt_number, vector<double> *knot_vector, vector<double> *pt_x, vector<double> *pt_y, int gauss_pt_number) override;
    void compute(void) override;

private:
    void computeMassMatrix(void);
    void computeStiffnessMatrix(void);
    void computeJacobian(void);
    void computeLength(void);
    void computeExternalForces(void);
    void assembleMatrix(void);
    void assembleRHS(void);
    void inverseMatrix(void);
    void applyBoundaryConditions(void);

private:
    double time;

    int ctrl_pt_number;
    int elt_number;
    int knot_number;
    int degree;
    int gauss_pt_number;

    vector<double> *knot_vector;
    vector<double> *control_point_list;

    vector<double> gauss_point_coord_list;
    vector<double> gauss_point_weight_list;
    vector<double> gauss_point_jacobian_list;

    vector<double> values;

    double membrane_thickness;
    double membrane_young;
    double membrane_density;
    double membrane_tension;
    double membrane_length;
    double membrane_pre_length;
    double membrane_damping;

    double coef_m;
    double coef_k;
    double coef_d;

    double *system_matrix;
    double *stiffness_matrix;
    double *mass_matrix;
    double *force_vector;
    double *rhs_vector;

    vector<double> displacement;
    vector<double> velocity;
    vector<double> acceleration;

    igBasisBSpline *basis;

    igLinAlg *lin_solver;

};

//
// igStructuralSolverMembrane.h ends here
