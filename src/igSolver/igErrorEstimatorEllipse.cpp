/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igErrorEstimatorEllipse.h"

#include <iostream>
#include <math.h>

#include <igCore/igMesh.h>
#include <igCore/igCell.h>
#include <igCore/igElement.h>
#include <igCore/igFace.h>
#include <igCore/igAssert.h>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igErrorEstimatorEllipse::igErrorEstimatorEllipse(void) : igErrorEstimator()
{
    refine_counter = 0;
    refine_direction = 0;
}

igErrorEstimatorEllipse::~igErrorEstimatorEllipse(void)
{

}

/////////////////////////////////////////////////////////////////////////////

void igErrorEstimatorEllipse::setRegion(vector<double> &args)
{
    this->center_x = args[0];
    this->center_y = args[1];
    this->semiaxis_1 = args[2];
    this->semiaxis_2 = args[3];
    this->rotation = args[4];

    cout << endl;
    cout << "Ellipse refinement:" << endl;
    cout << center_x << " " << center_y << endl;
    cout << semiaxis_1 << " " << semiaxis_2 << endl;
    cout << rotation << endl;
    cout << endl;
}

/////////////////////////////////////////////////////////////////////////////

void igErrorEstimatorEllipse::computeEstimator(void)
{
    error_xi->assign(mesh->elementNumber(),0.);
    error_eta->assign(mesh->elementNumber(),0.);

	double x_el, y_el, estimator;
    double value_error_xi = 0;
    double value_error_eta = 0;

    refine_counter++;
    if(refine_direction == 0)
        value_error_xi = 1;
    else
        value_error_eta = 1;

    rotation = rotation*M_PI/180;

    for(int iel=0; iel<mesh->elementNumber(); iel++){

    	igElement *elt = mesh->element(iel);
    	if(elt->isActive()){

    		x_el = elt->barycenter(0);
    		y_el = elt->barycenter(1);
    		estimator = pow( (x_el - center_x)*cos(rotation) + (y_el - center_y)*sin(rotation) ,2)/pow(semiaxis_1,2)
    				  + pow( (x_el - center_x)*sin(rotation) - (y_el - center_y)*cos(rotation) ,2)/pow(semiaxis_2,2);

    		if(estimator <= 1){

    			(*error_xi)[iel] = value_error_xi;
    			(*error_eta)[iel] = value_error_eta;

    		}

    	}

    }

}
