/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igStructuralSolverMembrane.h"

#include "igLinAlg.h"

#include <igCore/igBasisBSpline.h>
#include <igCore/igQuadrature.h>
#include <igDistributed/igCommunicator.h>

#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <math.h>
#include <limits>

///////////////////////////////////////////////////////////////

igStructuralSolverMembrane::igStructuralSolverMembrane(vector<double> *interface_gauss_pts, vector<double> *interface_gauss_wgt, vector<double> *interface_efforts, vector<double> *interface_displacements, vector<double> *interface_velocities) : igStructuralSolver(interface_gauss_pts,interface_gauss_wgt,interface_efforts,interface_displacements,interface_velocities)
{
    // Membrane characteristics
    membrane_thickness = 0.001;
    membrane_young = 700;
    membrane_pre_length = 0.;
    membrane_density = 825;
    membrane_damping = 0.;

    coef_m = this->membrane_density * this->membrane_thickness;
    coef_d = this->membrane_density * this->membrane_damping;

    this->lin_solver = new igLinAlg;
}

igStructuralSolverMembrane::~igStructuralSolverMembrane(void)
{
    delete this->basis;
    delete this->system_matrix;
    delete this->mass_matrix;
    delete this->stiffness_matrix;
    delete this->lin_solver;
}

////////////////////////////////////////////////////////////////

void igStructuralSolverMembrane::initialize(int degree, int ctrl_pt_number, vector<double> *knot_vector, vector<double> *pt_x, vector<double> *pt_y, int gauss_pt_number)
{
    // FE model
    this->degree = degree;
    this->ctrl_pt_number = ctrl_pt_number;
    this->gauss_pt_number = gauss_pt_number;

    this->knot_number =  this->ctrl_pt_number + this->degree + 1; // k=n+p+1
    this->elt_number = this->ctrl_pt_number - this->degree;// clamped

    this->knot_vector = knot_vector;
    this->control_point_list = pt_x;

    //BSpline basis
    this->basis = new igBasisBSpline(this->degree, this->knot_vector);

    // array of function values
    values.resize(ctrl_pt_number);

    // initial conditions
    this->time = 0;
    this->displacement.resize(ctrl_pt_number,0);
    this->velocity.resize(ctrl_pt_number,0);
    this->acceleration.resize(ctrl_pt_number,0);

    // compute the Gauss point properties
    igQuadrature quadrature;
    this->gauss_point_coord_list.assign(this->gauss_pt_number,0.);
    this->gauss_point_weight_list.assign(this->gauss_pt_number,0.);
    quadrature.initializeGaussPoints(1,this->gauss_pt_number, this->gauss_point_coord_list, this->gauss_point_weight_list);

    // compute Jacobian
    this->gauss_point_jacobian_list.assign(this->gauss_pt_number*this->elt_number,0.);
    this->computeJacobian();

    // create matrices
    this->system_matrix = new double[ctrl_pt_number*ctrl_pt_number];
    this->mass_matrix = new double[ctrl_pt_number*ctrl_pt_number];
    this->stiffness_matrix = new double[ctrl_pt_number*ctrl_pt_number];
    this->force_vector = new double[ctrl_pt_number];
    this->rhs_vector = new double[ctrl_pt_number];

    // compute fixed matrices
    this->computeMassMatrix();
}

////////////////////////////////////////////////////////////////

void igStructuralSolverMembrane::computeMassMatrix(void)
{
    // initialization
    for(int i=0; i<ctrl_pt_number*ctrl_pt_number; i++){
        this->mass_matrix[i] = 0;
    }

    // loop over elements = knot spans
    for(int iel=0; iel<this->elt_number; iel++){

        // loop over Gauss points
        for(int k=0; k<this->gauss_pt_number; k++){

            // parametric coordinate
            int span_index = iel+this->degree;
            double interval = this->knot_vector->at(span_index+1) - this->knot_vector->at(span_index);
            double par = this->knot_vector->at(span_index) + this->gauss_point_coord_list[k] * interval;

            // jacobian
            double jacobian = this->gauss_point_jacobian_list.at(iel*gauss_pt_number+k);

            // evaluate all functions at current Gauss point
            basis->evalFunction(par,values);

            // loop over functions
            for(int i=0; i<this->ctrl_pt_number; i++){
                for(int j=0; j<this->ctrl_pt_number; j++){

                    this->mass_matrix[i*ctrl_pt_number+j] += this->gauss_point_weight_list[k]*interval * jacobian * values[i] * values[j];

                }
            }

        }

    }

}

void igStructuralSolverMembrane::computeStiffnessMatrix(void)
{
    // initialization
    for(int i=0; i<ctrl_pt_number*ctrl_pt_number; i++){
        this->stiffness_matrix[i] = 0;
    }

    // loop over elements = knot spans
    for(int iel=0; iel<this->elt_number; iel++){

        // loop over Gauss points
        for(int k=0; k<this->gauss_pt_number; k++){

            // parametric coordinate
            int span_index = iel+this->degree;
            double interval = this->knot_vector->at(span_index+1) - this->knot_vector->at(span_index);
            double par = this->knot_vector->at(span_index) + this->gauss_point_coord_list[k] * interval;

            // jacobian
            double jacobian = this->gauss_point_jacobian_list.at(iel*gauss_pt_number+k);

            // evaluate all functions at current Gauss point
            basis->evalGradient(par,values);

            // evaluate non-linear terms
            double dxdxi = 0;
            double dydxi = 0;

            // loop over control points
            for(int i=0; i<this->ctrl_pt_number; i++){
                dxdxi += values[i] * this->control_point_list->at(i);
                dydxi += values[i] * this->displacement.at(i);
            }
            double non_linear_term = pow(1 + pow(dydxi/dxdxi,2),-1.5);

            // loop over functions
            for(int i=0; i<this->ctrl_pt_number; i++){
                for(int j=0; j<this->ctrl_pt_number; j++){

                    this->stiffness_matrix[i*ctrl_pt_number+j] += this->gauss_point_weight_list[k]*interval * values[i] * values[j] * non_linear_term / jacobian;

                }
            }

        }

    }

}

void igStructuralSolverMembrane::computeExternalForces(void)
{
    // initialization
    for(int i=0; i<ctrl_pt_number; i++){
        this->force_vector[i] = 0;
    }

    // loop over interface Gauss points
    for( int igauss=0; igauss<interface_gauss_pts->size(); igauss++){

        double par_gauss = interface_gauss_pts->at(igauss);

        basis->evalGradient(par_gauss,values);

        // compute Jacobian
        double jacobian = 0;
        for(int i=0; i<ctrl_pt_number; i++){
            jacobian +=  values[i] * control_point_list->at(i);
        }

        // evaluate non-linear terms
        double dxdxi = 0;
        double dydxi = 0;
        for(int i=0; i<this->ctrl_pt_number; i++){
            dxdxi += values[i] * this->control_point_list->at(i);
            dydxi += values[i] * this->displacement.at(i);
        }
        double non_linear_term = pow(1 + pow(dydxi/dxdxi,2),-0.5);

        // compute function values
        basis->evalFunction(par_gauss,values);

        // add contribution
        for(int i=0; i<ctrl_pt_number; i++){
            this->force_vector[i] += interface_gauss_wgt->at(igauss) * jacobian * non_linear_term * values[i] * interface_efforts->at(igauss);
        }
    }

}

void igStructuralSolverMembrane::computeJacobian(void)
{
    // initialization
    for(int i=0; i<gauss_point_jacobian_list.size(); i++){
        this->gauss_point_jacobian_list.at(i) = 0;
    }

    // loop over elements = knot spans
    for(int iel=0; iel<this->elt_number; iel++){

        // loop over Gauss points
        for(int k=0; k<this->gauss_pt_number; k++){

            // parametric coordinate
            int span_index = iel+this->degree;
            double interval = this->knot_vector->at(span_index+1) - this->knot_vector->at(span_index);
            double par = this->knot_vector->at(span_index) + this->gauss_point_coord_list[k] * interval;

            // evaluate all derivatives at current Gauss point
            basis->evalGradient(par,values);

            // loop over functions
            for(int i=0; i<this->ctrl_pt_number; i++){

                this->gauss_point_jacobian_list.at(iel*gauss_pt_number+k) +=  values[i] * this->control_point_list->at(i);

            }

        }

    }
}

void igStructuralSolverMembrane::computeLength(void)
{
    this->membrane_length = 0;

    // loop over elements = knot spans
    for(int iel=0; iel<this->elt_number; iel++){

        // loop over Gauss points
        for(int k=0; k<this->gauss_pt_number; k++){

            // parametric coordinate
            int span_index = iel+this->degree;
            double interval = this->knot_vector->at(span_index+1) - this->knot_vector->at(span_index);
            double par = this->knot_vector->at(span_index) + this->gauss_point_coord_list[k] * interval;

            // evaluate all derivatives at current Gauss point
            basis->evalGradient(par,values);

            // jacobian
            double jacobian = this->gauss_point_jacobian_list.at(iel*gauss_pt_number+k);

            double dxdxi = 0;
            double dydxi = 0;

            // loop over control points
            for(int i=0; i<this->ctrl_pt_number; i++){
                dxdxi += values[i] * this->control_point_list->at(i);
                dydxi += values[i] * this->displacement.at(i);
            }

            // add local contribution
            this->membrane_length +=  this->gauss_point_weight_list[k]*interval * sqrt(dxdxi*dxdxi + dydxi*dydxi);
        }
    }
}

/////////////////////////////////////////////////////////////

void igStructuralSolverMembrane::assembleMatrix(void)
{

    for(int i=0; i<this->ctrl_pt_number; i++){
        for(int j=0; j<this->ctrl_pt_number; j++){

            this->system_matrix[i*ctrl_pt_number+j] = coef_m * this->mass_matrix[i*ctrl_pt_number+j] * 4/(this->time_step*this->time_step)
                                                    + coef_k * this->stiffness_matrix[i*ctrl_pt_number+j]
                                                    + coef_d * this->mass_matrix[i*ctrl_pt_number+j] * 2/(this->time_step);

        }
    }

}

void igStructuralSolverMembrane::assembleRHS(void)
{

    for(int i=0; i<this->ctrl_pt_number; i++){

        // external force term
        this->rhs_vector[i] = this->force_vector[i];

        // stiffness term
        for(int j=0; j<this->ctrl_pt_number; j++){
            this->rhs_vector[i] -= coef_k * this->stiffness_matrix[i*ctrl_pt_number+j] * this->displacement[j];
        }

        // mass term
        for(int j=0; j<this->ctrl_pt_number; j++){
            this->rhs_vector[i] += coef_m * this->mass_matrix[i*ctrl_pt_number+j] * (this->velocity[j]*4/time_step + this->acceleration[j]);
        }

        // damping term
        for(int j=0; j<this->ctrl_pt_number; j++){
            this->rhs_vector[i] += coef_d * this->mass_matrix[i*ctrl_pt_number+j] * this->velocity[j];
        }

    }

}

void igStructuralSolverMembrane::inverseMatrix(void)
{
    // lapack library
    lin_solver->inverse(this->system_matrix, this->ctrl_pt_number);

}

void igStructuralSolverMembrane::applyBoundaryConditions(void)
{
    // modify first and last lines to respect BC

    this->rhs_vector[0] = 0;
    this->system_matrix[0] = 1;
    for(int j=1; j<this->ctrl_pt_number; j++){
        this->system_matrix[j] = 0;
    }

    this->rhs_vector[this->ctrl_pt_number-1] = 0;
    this->system_matrix[this->ctrl_pt_number*this->ctrl_pt_number-1] = 1;
    for(int j=0; j<this->ctrl_pt_number-1; j++){
        this->system_matrix[(ctrl_pt_number-1)*ctrl_pt_number+j] = 0;
    }

}

/////////////////////////////////////////////////////////////

void igStructuralSolverMembrane::compute(void)
{
    //-------- update non-linear model -------

    this->computeLength();
    this->coef_k = this->membrane_thickness * this->membrane_young
                 * (this->membrane_length - 1 + this->membrane_pre_length);
    this->computeStiffnessMatrix();

    //---------- compute rhs term -------------

    this->computeExternalForces();
    this->assembleRHS();

    //---------- compute and inverse matrix ---------------------

    this->assembleMatrix();
    this->applyBoundaryConditions();
    this->inverseMatrix();

    //-------------- update model -----------------

    for(int i=0; i<this->ctrl_pt_number; i++){

        double acceleration_old = acceleration[i];

        double variation = 0;
        for(int j=0; j<this->ctrl_pt_number; j++){
            variation += this->system_matrix[i*ctrl_pt_number+j] * this->rhs_vector[j];
        }

        displacement[i] += variation;
        acceleration[i] = 4/(time_step*time_step)*variation - 4/time_step*velocity[i] - acceleration[i];
        velocity[i] += time_step/2 * (acceleration[i]+acceleration_old);

    }
    this->computeLength();

    //-------------- fill interface arrays -------------------

    for(int i=0; i<this->ctrl_pt_number; i++){
        this->interface_displacements->at(i) = displacement[i];
        this->interface_velocities->at(i) = velocity[i];
    }

    time += time_step;

    return;
}
