/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igErrorEstimatorGlobal.h"

#include <iostream>
#include <math.h>

#include <igCore/igMesh.h>
#include <igCore/igCell.h>
#include <igCore/igElement.h>
#include <igCore/igFace.h>
#include <igCore/igAssert.h>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igErrorEstimatorGlobal::igErrorEstimatorGlobal(void) : igErrorEstimator()
{
    step_number = 0;
}

igErrorEstimatorGlobal::~igErrorEstimatorGlobal(void)
{

}

/////////////////////////////////////////////////////////////////////////////


void igErrorEstimatorGlobal::computeEstimator(void)
{
    error_xi->assign(mesh->elementNumber(),0.);
    error_eta->assign(mesh->elementNumber(),0.);

    double value_error_xi = 0;
    double value_error_eta = 0;

    if(step_number%2 == 0){
    	refine_direction = 0;
        value_error_xi = 1;
    }
    else{
    	refine_direction = 1;
        value_error_eta = 1;
    }
    for (int iel=0; iel<mesh->elementNumber(); iel++){

        (*error_xi)[iel] = value_error_xi;
        (*error_eta)[iel] = value_error_eta;

    }

    step_number++;
}
