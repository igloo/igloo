/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igStructuralSolverSpring.h"

#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <math.h>
#include <limits>

#include <igDistributed/igCommunicator.h>


///////////////////////////////////////////////////////////////

igStructuralSolverSpring::igStructuralSolverSpring(vector<double> *interface_gauss_pts, vector<double> *interface_gauss_wgt, vector<double> *interface_efforts, vector<double> *interface_displacements, vector<double> *interface_velocities) : igStructuralSolver(interface_gauss_pts,interface_gauss_wgt,interface_efforts,interface_displacements,interface_velocities)
{
    // spring characteristics
    this->structure_dof = 3;
    this->mass.resize(structure_dof);
    mass[0] = 1;
    mass[1] = 1;
    mass[2] = 1;
    this->stiffness.resize(structure_dof);
    stiffness[0] = 0.25;
    stiffness[1] = 0.25;
    stiffness[2] = 0.25;

    this->time_start = 0;

    // initial conditions
    this->time = 0;
    this->displacement.resize(structure_dof,0);
    this->velocity.resize(structure_dof,0);
    this->acceleration.resize(structure_dof,0);
    this->fluid_effort.resize(structure_dof,0);

    this->open_file = true;
}

igStructuralSolverSpring::~igStructuralSolverSpring(void)
{

}

/////////////////////////////////////////////////////////////

void igStructuralSolverSpring::compute(void)
{

    //---------- retrieve fluid efforts ----------

    fluid_effort[0] = interface_efforts->at(0);
    fluid_effort[1] = interface_efforts->at(1);
    fluid_effort[2] = interface_efforts->at(2);

    //------------- Spring movement --------------------

    for(int idof=0; idof<structure_dof; idof++){

        double acceleration_old = acceleration[idof];

        double variation = (fluid_effort[idof] - stiffness[idof]*displacement[idof] + mass[idof]*(4*velocity[idof]/time_step+acceleration[idof]))
                        / (4*mass[idof]/(time_step*time_step) + stiffness[idof]);

        if(time > time_start){
            displacement[idof] += variation;
            acceleration[idof] = 4/(time_step*time_step)*variation - 4/time_step*velocity[idof] - acceleration[idof];
            velocity[idof] += time_step/2 * (acceleration[idof]+acceleration_old);
        }

    }

    // fill interface arrays
    this->interface_displacements->at(0) = displacement[0];
    this->interface_displacements->at(1) = displacement[1];
    this->interface_displacements->at(2) = displacement[2];
    this->interface_velocities->at(0) = velocity[0];
    this->interface_velocities->at(1) = velocity[1];
    this->interface_velocities->at(2) = velocity[2];

    time += time_step;

    // write output file
    if(open_file && communicator->rank() == 0){
        fsi_file.open("fsi.dat");
        fsi_file.precision(12);
        open_file = false;
    }

    if(communicator->rank() == 0)
        fsi_file << scientific << time << " " << fluid_effort[0] << " " << fluid_effort[1] << " " << fluid_effort[2]  << " " << displacement[0] << " " << displacement[1] << " " << displacement[2] << endl;


    return;
}
