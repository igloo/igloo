/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igSolverExport.h>

#include <vector>
#include <string>

class igCommunicator;
class igMesh;

using namespace std;

class IGSOLVER_EXPORT igCouplingInterface
{
public:
             igCouplingInterface(void);
    virtual ~igCouplingInterface(void);

public:
    virtual void initializeFluidInterface(int quadrature_number, int point_number, int dimension);
    virtual void initializeStructureInterface(void);
    virtual void initializeQuadrature(igMesh *mesh);
    virtual void initializeMapping(igMesh *mesh);

    virtual void initializeQuadraturePointNumber(int fluid_gauss_pts_number);
    virtual void initializeQuadratureMapping(int quadrature_number);

    virtual void fillQuadrature(igMesh *mesh, int global_quadrature_number);

    void setCommunicator(igCommunicator *communicator);

    void resetLocalEffort(void);
    virtual void addLocalEffort(int counter, double Fx_inv, double Fx_vis, double Fy_inv, double Fy_vis, double Mz, double area, double nx, double ny);

    virtual void convertStructureDisplacementsToFluid(void);
    virtual void convertStructureVelocitiesToFluid(void);
    virtual void convertFluidEffortsToStructure(void);

public:
    vector<double>* interfaceEffortsStructure(void);
    vector<double>* interfaceDisplacementsStructure(void);
    vector<double>* interfaceVelocitiesStructure(void);

    vector<double>* interfaceEffortsFluid(void);
    vector<double>* interfaceDisplacementsFluid(void);
    vector<double>* interfaceVelocitiesFluid(void);

    vector<double>* interfaceGaussPoints(void);
    vector<double>* interfaceGaussWeights(void);

    vector<double>* knotVector(void);
    vector<double>* controlPointsX(void);
    vector<double>* controlPointsY(void);

    int interfaceDegree(void);
    int controlPointNumber(void);
    int gaussPointNumber(void);
    int elementNumber(void);

    int quadraturePointNumber(void);

    virtual string name(void);

protected:

    igCommunicator *communicator;

    vector<int> *interface_mapping;
    int quadrature_point_per_face;

    vector<double> *pt_x;
    vector<double> *pt_y;
    vector<double> *knot_vector;

    int ctrl_pt_number;
    int degree;
    int gauss_pt_number;
    int elt_number;

    vector<double> *interface_efforts_s;
    vector<double> *interface_displacements_s;
    vector<double> *interface_velocities_s;

    vector<double> *interface_efforts_f;
    vector<double> *interface_displacements_f;
    vector<double> *interface_velocities_f;

    vector<double> *interface_gauss_pts;
    vector<double> *interface_gauss_wgt;
    vector<int> *interface_gauss_label;

    vector<int> *interface_quadrature_counter_f;
    vector<int> *interface_quadrature_counter_s;

};

//
// igCouplingInterface.h ends here
