/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igSolverBurger.h"

#include "igFluxBurger.h"

#include <igCore/igMesh.h>
#include <igCore/igElement.h>
#include <igCore/igFace.h>
#include <igCore/igAssert.h>

#include <fstream>
#include <iostream>
#include <math.h>
#include <limits>

///////////////////////////////////////////////////////////////

double limiter(double a, double b, double c, double M)
{
    double value;

    if (fabs(a) < fabs(M)) {
        value = a;

    } else {
        double s = (copysign(1.,a)+copysign(1.,b)+copysign(1.,c))/3.;
        if(fabs(fabs(s)-1.) < 1.E-15){
            //cout << "limiter on case 1 " << a << " " << b << " " << c <<endl;
            double value_min = min(fabs(a),fabs(b));
            value_min = min(value_min,fabs(c));
            value = s*value_min;

        } else {
            //cout << "limiter on case 2 " << a << " " << b << " " << c << endl;
            value = 0.;
        }
    }

    return value;
}

///////////////////////////////////////////////////////////////

igSolverBurger::igSolverBurger(int variable_number, int dimension, vector<double> *solver_parameters, bool flag_ALE) : igSolver(variable_number, dimension, solver_parameters, flag_ALE)
{
    flux = new igFluxBurger(variable_number, dimension, flag_ALE);
}

igSolverBurger::~igSolverBurger(void)
{

}

///////////////////////////////////////////////////////////////

void igSolverBurger::applyLimiter(double shock_capturing_coef)
{
    // loop over all elements
    for (int iel=1; iel<mesh->elementNumber()-1; iel++){

        // current element
        igElement *elt = mesh->element(iel);
        int function_number = elt->controlPointNumber();

        double mean_solution = 0;
        for (int i=0; i<elt->controlPointNumber(); i++)
            mean_solution += state_list->at(elt->globalId(i,0));
        mean_solution /= elt->controlPointNumber();

        // left and right solutions
        double solution_left = state_list->at(elt->globalId(0,0));
        double solution_right = state_list->at(elt->globalId(function_number-1,0));

        // left element
        igElement *elt_left = mesh->element(iel-1);

        double mean_solution_left = 0;
        for (int i=0; i<elt_left->controlPointNumber(); i++)
            mean_solution_left += state_list->at(elt_left->globalId(i,0));
        mean_solution_left /= elt_left->controlPointNumber();

        // right element
        igElement *elt_right = mesh->element(iel+1);

        double mean_solution_right = 0;
        for (int i=0; i<elt_right->controlPointNumber(); i++)
            mean_solution_right += state_list->at(elt_right->globalId(i,0));
        mean_solution_right /= elt_right->controlPointNumber();

        // compute slopes
        double slope_mean_left = (mean_solution-mean_solution_left);
        double slope_mean_right = (mean_solution_right-mean_solution);
        double slope_left = (mean_solution-solution_left);
        double slope_right = (solution_right-mean_solution);

        // local curvature at extremum
        double curvature = 0.05;

        // apply limiter
        double limited_slope_left = limiter(slope_left,slope_mean_left,slope_mean_right,curvature);
        double limited_slope_right = limiter(slope_right,slope_mean_left,slope_mean_right,curvature);

        // apply limiter
        if(fabs(limited_slope_left-slope_left) > 1.E-15 || fabs(limited_slope_right-slope_right) > 1.E-15){

            double limited_slope = (limited_slope_right + limited_slope_left)*0.5;

            for (int i=0; i<elt->controlPointNumber(); i++)
                state_list->at(elt->globalId(i,0)) = mean_solution + limited_slope
                                                           *(2.*double(i)/double(function_number-1)-1.);
        }
    }
}

//////////////////////////////////////////////////////////////

void igSolverBurger::computeIncrement(vector<double> *increment)
{
    // reset residual list
    this->resetResidual();

    // compute the different DG terms
    this->volumicIntegrator(flux,residual_list);
    this->surfacicIntegrator(flux,residual_list);
    this->boundaryIntegrator(flux,residual_list);

    this->updateSolution(increment,residual_list,variable_number);
}

void igSolverBurger::computeIncrementAle(vector<double> *increment)
{
	// reset residual list
	this->resetResidual();

	// compute the different DG terms
	this->volumicIntegrator(flux,residual_list);
	this->surfacicIntegrator(flux,residual_list);
	this->boundaryIntegrator(flux,residual_list);

    for(int idof=0; idof<residual_list->size(); idof++)
    	increment->at(idof) = residual_list->at(idof);

}

//////////////////////////////////////////////////////////////

double igSolverBurger::timeStep(void)
{
	vector<double> *mesh_vel = new vector<double>(this->dimension,0.);
    double time_step = numeric_limits<double>::max();

    vector<double> *local_state = new vector<double>;
    local_state->resize(variable_number);
    vector<double> *derivative = new vector<double>;
    derivative->resize(derivative_number);

    for (int iel=0; iel<mesh->elementNumber(); iel++) {

        igElement *elt = mesh->element(iel);

        double max_wave_speed = 0;
        for (int idof=0; idof<elt->controlPointNumber(); idof++){
            this->extractState(elt,idof,local_state,derivative);
            max_wave_speed = fmax(max_wave_speed,flux->waveSpeed(local_state,mesh_vel));
        }

        double diameter = 2*elt->radius() / (2.* elt->cellDegree()+1.);

        double local_step = diameter / max_wave_speed;
        time_step = fmin(time_step,local_step);

        }
    delete local_state;
    delete derivative;
    delete mesh_vel;

    return time_step * this->cfl_coefficient;
}

/////////////////////////////////////////////////////////////////

void igSolverBurger::exactSolution(double *position, double time, vector<double> *solution, double x_elt, double y_elt)
{
    solution->at(0) = 0;
}

//////////////////////////////////////////////////////////////////
