/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include "igErrorEstimator.h"

#include <igSolverExport.h>
#include <vector>

using namespace std;


class IGSOLVER_EXPORT igErrorEstimatorEllipse : public igErrorEstimator
{
public:
    igErrorEstimatorEllipse(void);
    virtual ~igErrorEstimatorEllipse(void);

public:
    void setRegion(vector<double> &args);

public:
    void computeEstimator(void);

private:
    double center_x;
    double center_y;
    double semiaxis_1;
    double semiaxis_2;
    double rotation;

    int refine_counter;

};

//
// igErrorEstimatorEllipse.h ends here
