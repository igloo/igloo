/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igSolverExport.h>

#include "igSolver.h"

#include <fstream>
#include <vector>

class igMesh;
class igElement;

class IGSOLVER_EXPORT igSolverCompressibleFlow : public igSolver
{
public:
     igSolverCompressibleFlow(int variable_number, int dimension, vector<double> *solver_parameters, bool flag_ALE);
    ~igSolverCompressibleFlow(void);

protected:
    void retrieveInteriorExteriorQuantities(vector<double> *state_interior, vector<double> *state_exterior);
    void retrieveGeometricalQuantities(igFace *face, vector<double> *mesh_vel, int index);

    void computeSlipWall(void);
    void computeNoSlipWall(void);
    void computeIORiemann(void);
    void computeIODensityVelocityPressure(void);
    void computeIOSupersonic(void);

    void recoverBoundaryState(vector<double> *state);

    void checkPositivity(void) override;

protected:
    double gamma;
    double mach_ref;
    double incidence;

    double rho_int;
    vector<double> velocity_int;
    double p_int;
    double rho_ext;
    vector<double> velocity_ext;
    double p_ext;
    double rho_bnd;
    vector<double> velocity_bnd;
    double p_bnd;

    vector<double> velocity_mesh;

    vector<double> normal;
    vector<double> tangent1;
    vector<double> tangent2;

    vector<double> coord;

    bool open_effort_file;

};

//
// igSolverCompressibleFlow.h ends here
