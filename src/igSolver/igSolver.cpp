/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igSolver.h"

#include "igLinAlg.h"
#include "igFlux.h"
#include "igSource.h"

#include <igCore/igMesh.h>
#include <igCore/igElement.h>
#include <igCore/igFace.h>
#include <igCore/igLinAlg.h>
#include <igCore/igBoundaryIds.h>
#include <igCore/igBasisBezier.h>

#include <igDistributed/igCommunicator.h>

#include <algorithm>
#include <iostream>
#include <math.h>

///////////////////////////////////////////////////////////////

igSolver::igSolver(int variable_number, int dimension, vector<double> *solver_parameters, bool flag_ALE)
{
    this->variable_number = variable_number;
    this->dimension = dimension;
    this->ALE_formulation = flag_ALE;
    this->solver_parameters = solver_parameters;

    this->derivative_number = 0; // by default (if not viscous)

    residual_list = new vector<double>;

    this->mesh = nullptr;
    this->communicator = nullptr;
    this->grad_state_list = nullptr;
    this->grad_residual_list = nullptr;

    this->shock_capturing = false;

    this->check_positivity = false;

    this->lin_solver = new igLinAlg;
}

igSolver::~igSolver(void)
{
    if(mass_matrix_list.size()>0){
        for (int iel = 0; iel < mesh->elementNumber(); ++iel) {
            delete[] mass_matrix_list[iel];
        }
    }

    if(residual_list)
        delete residual_list;

    delete lin_solver;
}

///////////////////////////////////////////////////////////////

void igSolver::setMesh(igMesh *input_mesh)
{
    mesh = input_mesh;
}

//////////////////////////////////////////////////////////////

void igSolver::setState(vector<double> *state)
{
    state_list = state;

    if(this->check_positivity)
        checkPositivity();
}

void igSolver::setArtificialViscosity(vector<double> *artificial_viscosity)
{
    artificial_viscosity_list = artificial_viscosity;
}

void igSolver::setVorticity(vector<double> *vorticity)
{
    this->vorticity_list = vorticity;
}

/////////////////////////////////////////////////////////////

void igSolver::setCommunicator(igCommunicator *com)
{
    communicator = com;
}

//////////////////////////////////////////////////////////////

void igSolver::setTime(double current_time)
{
    time = current_time;
}

//////////////////////////////////////////////////////////////

void igSolver::setStabilityCoefficient(double coefficient)
{
    cfl_coefficient = coefficient;
}

//////////////////////////////////////////////////////////////

void igSolver::setSmoothingViscosityNumber(int number)
{

}

////////////////////////////////////////////////////////////////

void igSolver::setInterface(igCouplingInterface *interface)
{
    this->interface = interface;
}

////////////////////////////////////////////////////////////////

void igSolver::enableShockCapturing(void)
{
    this->shock_capturing = true;
}

///////////////////////////////////////////////////////////////

void igSolver::enableCheckPositivity(void)
{
    this->check_positivity = true;
}

//////////////////////////////////////////////////////////////

void igSolver::initialize(void)
{
    int size = (mesh->controlPointNumber() + mesh->sharedControlPointNumber()) * mesh->variableNumber();
    residual_list->resize(size, 0.);

    this->initializeDerivative();
}

//////////////////////////////////////////////////////////////

void igSolver::initializeDerivative(void)
{
}

//////////////////////////////////////////////////////////////

void igSolver::resetResidual(void)
{
    std::fill(residual_list->begin(), residual_list->end(), 0.);

    this->resetDerivative();
}

//////////////////////////////////////////////////////////////

void igSolver::resetDerivative(void)
{
}

///////////////////////////////////////////////////////////////

void igSolver::applyLimiter(double shock_capturing_coef)
{
}

///////////////////////////////////////////////////////////////

void igSolver::computeEfforts(void)
{
}

void igSolver::computeVorticity(void)
{
}

///////////////////////////////////////////////////////////////

void igSolver::computeMassMatrix(void)
{
    // delete existing matrix
    int previous_element_number = mass_matrix_list.size();
    for (int iel=0; iel<previous_element_number; iel++)
        delete[] mass_matrix_list[iel];
    mass_matrix_list.clear();

    // loop over all new elements
    for (int iel = 0; iel < mesh->elementNumber(); ++iel) {

        igElement *element = mesh->element(iel);
        int function_number = element->controlPointNumber();
        int eval_number = element->gaussPointNumber();

        // local matrix allocation
        double *matrix = new double[function_number*function_number];

        // computation of local matrix
        for (int i=0; i<function_number; i++){
            for (int j=0; j<function_number; j++){

                // only upper part
                if (j >= i) {
                    double coef = 0.;

                    // loop over quadrature points
                    for (int k = 0; k < eval_number; ++k) {

                        coef += element->gaussPointWeight(k) * element->gaussPointFunctionValue(k,i)
                                                             * element->gaussPointFunctionValue(k,j)
                                                             * element->gaussPointJacobian(k);
                    }
                    matrix[i*function_number+j] = coef;

                } else {
                    matrix[i*function_number+j] = matrix[j*function_number+i];
                }

            }
        }
        // storage into global list
        mass_matrix_list.push_back(matrix);
    }
}

//////////////////////////////////////////////////////////////

void igSolver::inverseMassMatrix(void)
{
    // loop over all elements
    for (int iel = 0; iel < mesh->elementNumber(); ++iel) {

        // local mass matrix
        double *matrix = mass_matrix_list[iel];

        // lapack library
        lin_solver->inverse(matrix, mesh->element(iel)->controlPointNumber());
    }
}

//////////////////////////////////////////////////////////////

void igSolver::updateSolution(vector<double> *solution_, vector<double> *residual_, int equation_number)
{
    auto &solution = *solution_;
    auto &residual = *residual_;

    // loop over all elements
    for (int iel = 0; iel < mesh->elementNumber(); ++iel) {

        igElement *element = mesh->element(iel);

        if (element->isActive()) {
            int function_number = element->controlPointNumber();
            double *mass_matrix = mass_matrix_list[iel];

            // loop over variables
            for (int ivar = 0; ivar < equation_number; ++ivar) {
                // computation of all dof
                int *global_ids = element->globalIdPtr(ivar);
                for (int i = 0; i < function_number; ++i) {
                    double value = 0.;

                    // multiply inverse of mass matrix with residual vector
                    for (int j = 0; j < function_number; ++j) {
                        value += mass_matrix[i*function_number+j] * residual[global_ids[j]];
                    }
                    solution[global_ids[i]] = value;
                }
            }
        }
    }
}

///////////////////////////////////////////////////////////////

void igSolver::volumicIntegrator(igFlux *flux_function, vector<double> *updated_res)
{
    int equation_number = flux_function->equationNumber();

    vector<double> state(variable_number);
    vector<double> derivative(derivative_number);
    vector<double> flux(equation_number*dimension);
    vector<double> *mesh_vel = new vector<double>(this->dimension,0.);
    double artificial_viscosity;

    auto &updated_residual = *updated_res;

    // loop over all elements
    for (int iel = 0; iel < mesh->elementNumber(); ++iel) {

        igElement *element = mesh->element(iel);

        if (element->isActive()) {

            int function_number = element->controlPointNumber();
            int eval_number = element->gaussPointNumber();

            // loop over quadrature points
            for (int k = 0; k < eval_number; ++k) {

                double gauss_point_weight = element->gaussPointWeight(k);
                double gauss_point_jacobian = element->gaussPointJacobian(k);
                double *gauss_point_gradient_values = element->gaussPointGradientPtr(k);

                // computation of the solution at point k
                this->solutionAtGaussPoint(element, k, &state, &derivative);

                // compute artificial viscosity at point k
                if(shock_capturing){
                    this->artificialViscosityAtGaussPoint(element, k, artificial_viscosity);
                    flux_function->setArtificialViscosity(artificial_viscosity);
                }

                // get mesh velocity at point k
                if(ALE_formulation)
                    element->gaussPointVelocity(k,mesh_vel);

                // computation of flux vector at point k
                flux_function->physical(&state, &derivative, mesh_vel, &flux);

                // loop over equations
                for (int ivar = 0; ivar < equation_number; ++ivar) {

                    int *global_ids_var = element->globalIdPtr(ivar);
                    double *flux_var = &flux[ivar*dimension];

                    // loop over degrees of freedom
                    for (int i = 0; i < function_number; ++i) {

                        // compute scalar product : flux * grad
                        double scalar = 0.;
                        for (int idim = 0; idim < dimension; ++idim) {
                            scalar += flux_var[idim] * gauss_point_gradient_values[i*dimension + idim];
                        }

                        // integration of the volumic residual
                        updated_residual[global_ids_var[i]] += gauss_point_weight * scalar * gauss_point_jacobian;
                    }
                }
            }
        }
    }

    delete mesh_vel;
}

///////////////////////////////////////////////////////////////////

void igSolver::surfacicIntegrator(igFlux *flux_function, vector<double> *updated_res)
{
    int equation_number = flux_function->equationNumber();

    vector<double> state_left(variable_number);
    vector<double> derivative_left(derivative_number);
    vector<double> flux_normal_left(equation_number);
    vector<double> state_right(variable_number);
    vector<double> derivative_right(derivative_number);
    vector<double> flux_normal_right(equation_number);
    vector<double> *mesh_vel = new vector<double>(this->dimension,0.);

    double left_artificial_viscosity;
    double right_artificial_viscosity;

    auto &updated_residual = *updated_res;

    // loop over all interfaces
    for (int ifac = 0; ifac < mesh->faceNumber(); ++ifac) {

        igFace *face = mesh->face(ifac);

        if (face->isActive()) {

            int function_number = face->controlPointNumber();
            int eval_number = face->gaussPointNumber();

            int left_factor  = face->leftInside()  ? 1 : 0;
            int right_factor = face->rightInside() ? 1 : 0;

            // loop over quadrature points
            for (int k = 0; k < eval_number; ++k) {

                double gauss_point_weight =  face->gaussPointWeight(k);
                double gauss_point_jacobian = face->gaussPointJacobian(k);
                double *gauss_point_function_values_left = face->gaussPointFunctionPtrLeft(k);
                double *gauss_point_function_values_right = face->gaussPointFunctionPtrRight(k);

                // normal vector at point k
                double *normal_vector = face->gaussPointNormalPtr(k);

                // compute solution at point k
                this->solutionAtGaussPoint(face, k, &state_left, &derivative_left, &state_right, &derivative_right);

                // compute artificial viscosity at point k
                if(shock_capturing){
                    this->artificialViscosityAtGaussPoint(face, k, left_artificial_viscosity, right_artificial_viscosity);
                    flux_function->setArtificialViscosity(left_artificial_viscosity, right_artificial_viscosity);
                }

                // get mesh velocity at point k
                if(ALE_formulation)
                    face->gaussPointVelocity(k,mesh_vel);

                // computation of the numerical flux at point k
                flux_function->numerical(&state_left, &derivative_left, &state_right, &derivative_right,
                        &flux_normal_left, &flux_normal_right, mesh_vel, normal_vector);

                // loop over equations
                for (int ivar = 0; ivar < equation_number; ++ivar) {
                    auto left_res  = left_factor  * gauss_point_weight * flux_normal_left[ivar]  * gauss_point_jacobian;
                    auto right_res = right_factor * gauss_point_weight * flux_normal_right[ivar] * gauss_point_jacobian;

                    // loop over degrees of freedom
                    for (int i = 0; i < function_number; ++i) {

                        // storage
                        updated_residual[face->dofLeft(i,ivar)]  -= left_res  * gauss_point_function_values_left[i];
                        updated_residual[face->dofRight(i,ivar)] += right_res * gauss_point_function_values_right[i];
                    }

                }

            }

        }
    }

    delete mesh_vel;
}

/////////////////////////////////////////////////////////

void igSolver::boundaryIntegrator(igFlux *flux_function, vector<double> *updated_res)
{
    int equation_number = flux_function->equationNumber();

    vector<double> state_left(variable_number);
    vector<double> derivative_left(derivative_number);
    vector<double> flux_normal_left(equation_number);
    vector<double> state_right(variable_number);
    vector<double> derivative_right(derivative_number);
    vector<double> flux_normal_right(equation_number);
    vector<double> *mesh_vel = new vector<double>(this->dimension,0.);
    double left_artificial_viscosity;

    auto &updated_residual = *updated_res;

    // loop over all boundary interfaces
    for (int ifac = 0; ifac < mesh->boundaryFaceNumber(); ++ifac) {

        igFace *face = mesh->boundaryFace(ifac);

        if (face->isActive()) {

            int function_number = face->controlPointNumber();
            int eval_number = face->gaussPointNumber();
            int face_type = face->type();

            // loop over quadrature points
            for (int k = 0; k < eval_number; ++k) {

                double gauss_point_weight =  face->gaussPointWeight(k);
                double gauss_point_jacobian = face->gaussPointJacobian(k);
                double *gauss_point_function_values_left = face->gaussPointFunctionPtrLeft(k);

                // normal vector at point k
                double *normal_vector = face->gaussPointNormalPtr(k);

                // compute left (interior) solution at point k
                this->solutionAtGaussPoint(face, k, &state_left, &derivative_left);

                // compute artificial viscosity
                if(shock_capturing){
                    this->artificialViscosityAtGaussPoint(face, k, left_artificial_viscosity);
                    double right_artificial_viscosity = left_artificial_viscosity;
                    flux_function->setArtificialViscosity(left_artificial_viscosity, right_artificial_viscosity);
                }

                // get mesh velocity at point k
                if(ALE_formulation)
                    face->gaussPointVelocity(k,mesh_vel);

                // right (outside) solution
                this->boundarySolution(face, k, time, &state_left, &state_right, mesh_vel);
                this->boundaryDerivative(face, k, time, &derivative_left, &derivative_right);

                // computation of boundary flux
                flux_function->boundaryFlux(&state_left, &derivative_left, &state_right, &derivative_right,
                                            &flux_normal_left, &flux_normal_right, mesh_vel, normal_vector, face_type);

                // loop over equations
                for (int ivar = 0; ivar < equation_number; ++ivar) {
                    auto res_contrib = gauss_point_weight * flux_normal_left[ivar] * gauss_point_jacobian;
                    // loop over degrees of freedom
                    for (int i = 0; i < function_number; ++i) {
                        // storage
                        updated_residual[face->dofLeft(i, ivar)] -= res_contrib * gauss_point_function_values_left[i];
                    }
                }

            }
        }
    }

    delete mesh_vel;
}

///////////////////////////////////////////////////////////////

void igSolver::sourceIntegrator(igSource *source_function, vector<double> *updated_res)
{
    vector<double> state(variable_number);
    vector<double> derivative(variable_number);
    vector<double> state_gradient(variable_number);
    vector<double> derivative_gradient(variable_number);
    vector<double> source(variable_number);

    auto &updated_residual = *updated_res;

    // loop over all elements
    for (int iel = 0; iel < mesh->elementNumber(); ++iel) {

        igElement *element = mesh->element(iel);

        if (element->isActive()) {

            int function_number = element->controlPointNumber();
            int eval_number = element->gaussPointNumber();

            // loop over quadrature points
            for (int k = 0; k < eval_number; ++k) {

                // computation of the solution at point k
                this->solutionAtGaussPoint(element, k, &state, &derivative);
                this->solutionGradientAtGaussPoint(element, k, &state_gradient, &derivative_gradient);

                // computation of source function at point k
                source_function->value(&state, &state_gradient, &derivative, &derivative_gradient, &source);

                // loop over degrees of freedom
                for (int i = 0; i < function_number; ++i) {

                    // loop over variables
                    for (int ivar = 0; ivar < variable_number; ++ivar) {
                        // integration
                        updated_residual[element->globalId(i,ivar)] += element->gaussPointWeight(k)
                            * source[ivar] * element->gaussPointFunctionValue(k,i)
                            * element->gaussPointJacobian(k);
                    }
                }
            }
        }
    }
}

///////////////////////////////////////////////////////////////////

void igSolver::computeJumpCriterion(vector<double> *jump_xi, vector<double> *jump_eta)
{

    vector<double> *state_left = new vector<double>;
    state_left->resize(variable_number);
    vector<double> *state_right = new vector<double>;
    state_right->resize(variable_number);

    // MPI communication to synchronize states
    communicator->distributeField(state_list, variable_number);

    // loop over all interior interfaces
    for (int ifac=0; ifac<mesh->faceNumber(); ifac++){

        igFace *face = mesh->face(ifac);
        if(face->isActive()){

            int function_number = face->controlPointNumber();
            int eval_number = face->gaussPointNumber();

            double face_jump = 0.;

            // loop over quadrature points
            for (int k=0; k<eval_number; k++){

                double gauss_point_weight =  face->gaussPointWeight(k);
                double gauss_point_jacobian = face->gaussPointJacobian(k);

                // left and right solutions at point k
                this->solutionJumpAtGaussPoint(face,k,state_left,state_right);

                // loop over equations
                for (int ivar=0; ivar<variable_number; ivar++){

                    // jump integration
                    face_jump += gauss_point_weight * fabs(state_left->at(ivar) - state_right->at(ivar)) * gauss_point_jacobian;
                }

            }

            if(face->leftInside()){
                if(abs(face->leftOrientation()) == 1 || abs(face->leftOrientation()) == 3)
                    jump_xi->at(face->leftElementIndex()) += face_jump/face->area();
                else
                    jump_eta->at(face->leftElementIndex()) += face_jump/face->area();
            }

            if(face->rightInside()){
                if(abs(face->rightOrientation()) == 1 || abs(face->rightOrientation()) == 3)
                    jump_xi->at(face->rightElementIndex()) += face_jump/face->area();
                else
                    jump_eta->at(face->rightElementIndex()) += face_jump/face->area();
            }
        }
    }

    delete state_left;
    delete state_right;

}

void igSolver::computeViscosityCriterion(vector<double> *err_xi, vector<double> *err_eta)
{

    for (int iel = 0; iel < mesh->elementNumber(); ++iel) {

        igElement *elt = mesh->element(iel);

        err_xi->at(iel) = this->computeVariationXi(elt,0);
        err_eta->at(iel) = this->computeVariationEta(elt,0);
    }
}

double igSolver::computeVariationXi(igElement *elt, int ivar)
{
    int degree = elt->cellDegree();
    int function_number = elt->controlPointNumber();
    int eval_number = elt->gaussPointNumber();

    //----------- shock detector ----------------

    double variation = 0.;
    double mean_value = 0.;
    int *global_ids_var = elt->globalIdPtr(ivar);

    for (int j = 0; j < degree+1; ++j) {
        for (int i = 0; i < degree+1; ++i) {
            mean_value += fabs((*state_list)[global_ids_var[i+(degree+1)*j]]);
        }
    }
    mean_value /= (degree+1)*(degree+1);

    for (int j = 0; j < degree+1; ++j) {
        for (int i = 0; i < degree; ++i) {
            variation += fabs((*state_list)[global_ids_var[i+1+(degree+1)*j]] - (*state_list)[global_ids_var[i+(degree+1)*j]]);
        }
    }
    variation /= (degree+1);

    double reference = 0.;
    for (int j = 0; j < degree+1; ++j) {
        reference += fabs((*state_list)[global_ids_var[degree+(degree+1)*j]] - (*state_list)[global_ids_var[(degree+1)*j]]);
    }
    reference /= (degree+1);

    return (variation - reference)/mean_value;

}

double igSolver::computeVariationEta(igElement *elt, int ivar)
{
    int degree = elt->cellDegree();
    int function_number = elt->controlPointNumber();
    int eval_number = elt->gaussPointNumber();

    //----------- shock detector ----------------

    double variation = 0.;
    double mean_value = 0.;
    int *global_ids_var = elt->globalIdPtr(ivar);

    for (int j = 0; j < degree+1; ++j) {
        for (int i = 0; i < degree+1; ++i) {
            mean_value += fabs((*state_list)[global_ids_var[i+(degree+1)*j]]);
        }
    }
    mean_value /= (degree+1)*(degree+1);

    for (int i = 0; i < degree+1; ++i) {
        for (int j = 0; j < degree; ++j) {
            variation += fabs((*state_list)[global_ids_var[i+(degree+1)*(j+1)]] - (*state_list)[global_ids_var[i+(degree+1)*j]]);
        }
    }
    variation /= (degree+1);

    double reference = 0.;
    for (int i = 0; i < degree+1; ++i) {
        reference += fabs((*state_list)[global_ids_var[i+(degree+1)*degree]] - (*state_list)[global_ids_var[i]]);
    }
    reference /= (degree+1);

    return (variation - reference)/mean_value;

}

//////////////////////////////////////////////////////////////

void igSolver::solutionAtGaussPoint(igElement *element, int index_pt, vector<double> *solution_, vector<double> *derivative_)
{
    int function_number = element->controlPointNumber();
    double *gauss_point_values = element->gaussPointFunctionPtr(index_pt);

    auto& states = *state_list;
    auto& solution = *solution_;

    // loop over variables
    for (int ivar = 0; ivar < variable_number; ++ivar) {

        int *global_ids = element->globalIdPtr(ivar);

        // loop over basis functions
        double value = 0.;
        for (int i = 0; i < function_number; ++i) {
            value += gauss_point_values[i] * states[global_ids[i]];
        }
        solution[ivar] = value;
    }

    auto& grad_states = *grad_state_list;
    auto& derivative = *derivative_;

    // loop over derivatives
    for (int ivar = 0; ivar < derivative_number; ++ivar){

        int *global_ids = element->globalIdPtr(ivar);

        // loop over basis functions
        double grad_value = 0.;
        for (int i = 0; i < function_number; ++i){
            grad_value += gauss_point_values[i] * grad_states[global_ids[i]];
        }
        derivative[ivar] = grad_value;
    }
}

double igSolver::solutionAtGaussPoint(igElement *element, int index_pt, int component){

    int function_number = element->controlPointNumber();
    double *gauss_point_values = element->gaussPointFunctionPtr(index_pt);

    int *global_ids = element->globalIdPtr(component);

    auto& states = *state_list;

    // loop over basis functions
    double value = 0.;
    for (int i = 0; i < function_number; ++i) {
        //value += gauss_point_values[i] * state_list->at(global_ids[i]);
        value += gauss_point_values[i] * states[global_ids[i]];
    }

    return value;
}

double igSolver::solutionLeftAtGaussPoint(igFace *face, int index_pt, int component)
{
    int function_number = face->controlPointNumber();

    // loop over basis functions
    double value_left = 0.;
    auto& states = *state_list;

    for (int i = 0; i < function_number; ++i) {
        value_left += face->gaussPointFunctionValueLeft(index_pt,i) * states[face->dofLeft(i,component)];
    }
    return value_left;
}

double igSolver::solutionRightAtGaussPoint(igFace *face, int index_pt, int component)
{
    int function_number = face->controlPointNumber();

    // loop over basis functions
    double value_right = 0.;
    auto& states = *state_list;

    for (int i = 0; i < function_number; ++i) {
        value_right += face->gaussPointFunctionValueRight(index_pt,i) * states[face->dofRight(i,component)];
    }
    return value_right;
}

void igSolver::solutionAtGaussPoint(igFace *face, int index_pt, vector<double> *solution_left, vector<double> *derivative_left, vector<double> *solution_right, vector<double> *derivative_right)
{
    int function_number = face->controlPointNumber();

    auto& states = *state_list;
    // loop over variables
    for (int ivar = 0; ivar < variable_number; ++ivar) {

        // loop over basis functions
        double value_left  = 0.;
        double value_right = 0.;
        for (int i=0; i<function_number; i++){
            value_left  += face->gaussPointFunctionValueLeft(index_pt,i)  * states[face->dofLeft(i,ivar)];
            value_right += face->gaussPointFunctionValueRight(index_pt,i) * states[face->dofRight(i,ivar)];
        }
        (*solution_left)[ivar]  = value_left;
        (*solution_right)[ivar] = value_right;
    }

    auto& grad_states = *grad_state_list;
    // loop over derivatives
    for (int ivar = 0; ivar < derivative_number; ++ivar) {

        // loop over basis functions
        double grad_value_left  = 0.;
        double grad_value_right = 0.;
        for (int i = 0; i < function_number; ++i) {
            grad_value_left  += face->gaussPointFunctionValueLeft(index_pt,i)  * grad_states[face->dofLeft(i,ivar)];
            grad_value_right += face->gaussPointFunctionValueRight(index_pt,i) * grad_states[face->dofRight(i,ivar)];
        }
        (*derivative_left)[ivar]  = grad_value_left;
        (*derivative_right)[ivar] = grad_value_right;
    }
}

void igSolver::solutionAtGaussPoint(igFace *face, int index_pt, vector<double> *solution_left_, vector<double> *derivative_left_)
{
    int function_number = face->controlPointNumber();

    auto &states = *state_list;
    auto &solution_left = *solution_left_;
    // loop over variables
    for (int ivar = 0; ivar < variable_number; ++ivar) {

        // loop over basis functions
        double value_left = 0.;
        for (int i = 0; i < function_number; ++i) {
            value_left += face->gaussPointFunctionValueLeft(index_pt,i) * states[face->dofLeft(i,ivar)];
        }
        solution_left[ivar] = value_left;
    }

    auto &grad_states = *grad_state_list;
    auto &derivative_left = *derivative_left_;

    // loop over derivatives
    for (int ivar = 0; ivar < derivative_number; ++ivar) {

        // loop over basis functions
        double grad_value_left = 0.;
        for (int i = 0; i < function_number; ++i) {
            grad_value_left += face->gaussPointFunctionValueLeft(index_pt,i) * grad_states[face->dofLeft(i,ivar)];
        }
        derivative_left[ivar] = grad_value_left;
    }
}

void igSolver::solutionJumpAtGaussPoint(igFace *face, int index_pt, vector<double> *solution_left, vector<double> *solution_right)
{
    int function_number = face->controlPointNumber();

    auto& states = *state_list;
    // loop over variables
    for (int ivar = 0; ivar < variable_number; ++ivar) {

        // loop over basis functions
        double value_left  = 0.;
        double value_right = 0.;
        for (int i=0; i<function_number; i++){
            value_left  += face->gaussPointFunctionValueLeft(index_pt,i)  * states[face->dofLeft(i,ivar)];
            value_right += face->gaussPointFunctionValueRight(index_pt,i) * states[face->dofRight(i,ivar)];
        }
        (*solution_left)[ivar]  = value_left;
        (*solution_right)[ivar] = value_right;
    }
}

void igSolver::solutionJumpAtGaussPoint(igFace *face, int index_pt, vector<double> *solution_left_)
{
    int function_number = face->controlPointNumber();

    auto &states = *state_list;
    auto &solution_left = *solution_left_;
    // loop over variables
    for (int ivar = 0; ivar < variable_number; ++ivar) {

        // loop over basis functions
        double value_left = 0.;
        for (int i = 0; i < function_number; ++i) {
            value_left += face->gaussPointFunctionValueLeft(index_pt,i) * states[face->dofLeft(i,ivar)];
        }
        solution_left[ivar] = value_left;
    }
}


double igSolver::solutionAtArbitraryPoint(igElement *elt, double *coord, int component){

    vector<double> values1(elt->controlPointNumberByDirection());
    vector<double> values2(elt->controlPointNumberByDirection());

    igBasisBezier basis1(elt->cellDegree());
    igBasisBezier basis2(elt->cellDegree());

    basis2.evalFunction(coord[1], values2);
    basis1.evalFunction(coord[0], values1);

    double weighted_sum = 0.;
    int index_sum = 0;
    for (int ictl2 = 0; ictl2 < elt->controlPointNumberByDirection(); ++ictl2) {
        for (int ictl1 = 0; ictl1 < elt->controlPointNumberByDirection(); ++ictl1) {
            weighted_sum += values1[ictl1]*values2[ictl2]*elt->controlPointWeight(index_sum);
            ++index_sum;
        }
    }

    index_sum = 0;
    double solution_value = 0.;
    for (int ictl2 = 0; ictl2 < elt->controlPointNumberByDirection(); ++ictl2) {
        for (int ictl1 = 0; ictl1 < elt->controlPointNumberByDirection(); ++ictl1) {

            solution_value += values1[ictl1]*values2[ictl2]*elt->controlPointWeight(index_sum)/weighted_sum * state_list->at(elt->globalId(index_sum, component));
            index_sum++;
        }
    }

    return solution_value;
}


double igSolver::coordinateAtArbitraryPoint(igElement *elt, double *coord, int component){

    vector<double> values1(elt->controlPointNumberByDirection());
    vector<double> values2(elt->controlPointNumberByDirection());

    igBasisBezier basis1(elt->cellDegree());
    igBasisBezier basis2(elt->cellDegree());

    basis2.evalFunction(coord[1], values2);
    basis1.evalFunction(coord[0], values1);

    double weighted_sum = 0.;
    int index_sum = 0;
    for (int ictl2 = 0; ictl2 < elt->controlPointNumberByDirection(); ++ictl2) {
        for (int ictl1 = 0; ictl1 < elt->controlPointNumberByDirection(); ++ictl1) {
            weighted_sum += values1[ictl1]*values2[ictl2]*elt->controlPointWeight(index_sum);
            ++index_sum;
        }
    }

    index_sum = 0;
    double solution_value = 0.;
    for (int ictl2 = 0; ictl2 < elt->controlPointNumberByDirection(); ++ictl2) {
        for (int ictl1 = 0; ictl1 < elt->controlPointNumberByDirection(); ++ictl1) {

            solution_value += values1[ictl1]*values2[ictl2]*elt->controlPointWeight(index_sum)/weighted_sum  * elt->controlPointCoordinate(index_sum, component);
            index_sum++;
        }
    }

    return solution_value;
}


//////////////////////////////////////////////////////////////

void igSolver::solutionGradientAtGaussPoint(igElement *element, int index_pt, vector<double> *solution_gradient_, vector<double> *derivative_gradient_)
{
    int function_number = element->controlPointNumber();
    auto &solution_gradient = *solution_gradient_;
    auto &derivative_gradient = *derivative_gradient_;

    auto &states = *state_list;
    // loop over variables
    for (int ivar = 0; ivar < variable_number; ++ivar) {

        int *global_ids = element->globalIdPtr(ivar);

        // loop over basis functions
        double value = 0.;
        for (int i = 0; i < function_number; ++i) {
            value += element->gaussPointGradientValue(index_pt,i,0) * states[global_ids[i]];
        }
        solution_gradient[ivar] = value;
    }

    auto &grad_states = *grad_state_list;

    // loop over derivatives
    for (int ivar = 0; ivar < derivative_number; ++ivar) {

        // loop over basis functions
        double grad_value = 0.;
        for (int i = 0; i < function_number; ++i) {
            grad_value += element->gaussPointGradientValue(index_pt,i,0) * grad_states[element->globalId(i,ivar)];
        }
        derivative_gradient[ivar] = grad_value;
    }
}

//////////////////////////////////////////////////////////////

void igSolver::extractState(igElement *element, int index_pt, vector<double> *state, vector<double> *derivative)
{
    // loop over variables
    for (int ivar = 0; ivar < variable_number; ++ivar) {
        (*state)[ivar] = (*state_list)[element->globalId(index_pt,ivar)];
    }
    // loop over derivatives
    for (int ivar = 0; ivar < derivative_number; ++ivar) {
        (*derivative)[ivar] = (*grad_state_list)[element->globalId(index_pt,ivar)];
    }
}

void igSolver::extractArtificialViscosity(igElement *element, int index_pt, double &artificial_viscosity)
{
    artificial_viscosity = 0.;
}

//////////////////////////////////////////////////////////////

void igSolver::computeGlobalErrorNorm(void)
{
    vector<double> error(variable_number, 0.);
    vector<double> local_error(variable_number, 0.);
    vector<double> global_error(variable_number, 0.);

    for (int iel = 0; iel < mesh->elementNumber(); ++iel) {

        igElement *elt = mesh->element(iel);

        if(elt->isActive()){

             this->computeLocalError(&local_error, elt, time);

             for (int ivar = 0; ivar < variable_number; ++ivar) {
                 error[ivar] += local_error[ivar];
             }

        }
    }

    // MPI communication
    for (int ivar = 0; ivar < variable_number; ++ivar) {
        double error_var = error[ivar];
        double global_error_var =  communicator->distributeError(error_var);
        global_error[ivar] = global_error_var;
    }

    if (communicator->rank() == 0) {
        for (int ivar = 0; ivar < mesh->variableNumber(); ++ivar) {
            cout << "-> error field "<< ivar << " : " << sqrt(global_error[ivar]) << endl;
        }
    }
}

void igSolver::computeGlobalErrorNormWithFile(void)
{
    vector<double> error(variable_number, 0.);
    vector<double> local_error(variable_number, 0.);

    double *coord =  new double[2];
    ifstream err_file("test_error.dat", ios::in);

    vector<double> refx;
    vector<double> refy;
    vector<double> refval;
    double value;

    for (int i=0; i<409600; i++){
        err_file >> value;
        refx.push_back(value);
        err_file >> value;
        refy.push_back(value);
        err_file >> value;
        refval.push_back(value);
    }

    for (int iel = 0; iel < mesh->elementNumber(); ++iel) {

        igElement *elt = mesh->element(iel);

        if(elt->isActive()){

            int npt = 10;
            for (int i2=0; i2<npt; i2++){
                for (int i1=0; i1<npt; i1++){

                    coord[0] = double(i1)/double(npt-1);
                    coord[1] = double(i2)/double(npt-1);

                    double coordx = this->coordinateAtArbitraryPoint(elt, coord, 0);
                    double coordy = this->coordinateAtArbitraryPoint(elt, coord, 1);
                    value  = this->solutionAtArbitraryPoint(elt, coord, 2);

                    double err_coord = 10;
                    int best_pt = 0;
                    for(int i=0; i<409600; i++){
                        double dist = sqrt((coordx - refx[i])*(coordx - refx[i]) + (coordy - refy[i])*(coordy - refy[i]));
                        if(dist < err_coord){
                            best_pt = i;
                            err_coord = dist;
                        }
                    }
                    error[2] += (value - refval[best_pt])*(value - refval[best_pt])/(mesh->elementNumber()*npt*npt);

//                    err_file << coordx << " " << coordy << " " << value << endl;
                }

            }


        }
    }

}


void igSolver::computeLocalError(vector<double> *error_, igElement *elt, double time)
{
    auto &error = *error_;
    error.assign(variable_number, 0.);

    vector<double> exact_values(variable_number, 0.);
    vector<double> state(variable_number);
    vector<double> derivative(derivative_number);

    // retrieve analytical solution Id
    int exact_solution_id = mesh->exactSolutionId();

    int sample_number = elt->gaussPointNumber();
    for (int k = 0; k < sample_number; ++k) {

        double *position = elt->gaussPointPhysicalCoordinate(k);
        double jacobian = elt->gaussPointJacobian(k);

        this->exactSolution(position, time, &exact_values, exact_solution_id, elt->barycenter(0), elt->barycenter(1));
        this->solutionAtGaussPoint(elt, k, &state, &derivative);

        for (int ivar = 0; ivar < variable_number; ++ivar) {
            error[ivar] += (state[ivar] - exact_values[ivar]) * (state[ivar] - exact_values[ivar]) * elt->gaussPointWeight(k) * jacobian;
        }

    }

}

//////////////////////////////////////////////////////////////

void igSolver::boundarySolution(igFace *face, int index, double time, vector<double> *state_interior, vector<double> *state, vector<double> *mesh_vel)
{
    int exact_solution_id = mesh->exactSolutionId();

    this->exactSolution(face->gaussPointPhysicalCoordinate(index),time,state,exact_solution_id,0.,0.);
}

void igSolver::boundaryDerivative(igFace *face, int index, double time, vector<double> *derivative_interior, vector<double> *derivative)
{
    int exact_solution_id = mesh->exactSolutionId();

    this->exactDerivative(face->gaussPointPhysicalCoordinate(index),time,derivative,exact_solution_id,0.,0.);
}

void igSolver::exactSolution(double *position, double time, vector<double> *solution, int solution_id, double x_elt, double y_elt)
{
    solution->assign(solution->size(),0.);
}

void igSolver::exactDerivative(double *position, double time, vector<double> *derivative, int solution_id, double x_elt, double y_elt)
{
    derivative->assign(derivative->size(),0.);
}

///////////////////////////////////////////////////////////////

void igSolver::artificialViscosityAtGaussPoint(igFace *face, int index_pt, double &left_artificial_viscosity, double &right_artificial_viscosity)
{
    left_artificial_viscosity = 0.;
    right_artificial_viscosity = 0.;
}

void igSolver::artificialViscosityAtGaussPoint(igFace *face, int index_pt, double &left_artificial_viscosity)
{
    left_artificial_viscosity = 0.;
}

void igSolver::artificialViscosityAtGaussPoint(igElement *element, int index_pt, double &artificial_viscosity)
{
    artificial_viscosity = 0.;
}

///////////////////////////////////////////////////////////////////

void igSolver::checkPositivity(void)
{
}
