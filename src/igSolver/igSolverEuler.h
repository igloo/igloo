/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igSolverExport.h>

#include "igSolverCompressibleFlow.h"

#include <fstream>
#include <vector>

class igMesh;
class igElement;
class igFlux;

class IGSOLVER_EXPORT igSolverEuler : public igSolverCompressibleFlow
{
public:
     igSolverEuler(int variable_number, int dimension, vector<double> *solver_parameters, bool flag_ALE);
    ~igSolverEuler(void);

public:
    void computeIncrement(vector<double> *increment) override;
    void computeIncrementAle(vector<double> *increment) override;
    double timeStep(void) override;

    void computeEfforts(void) override;

private:

    void boundarySolution(igFace *face, int index, double time, vector<double> *state_interior, vector<double> *state, vector<double> *mesh_vel) override;
    void exactSolution(double *position, double time, vector<double> *solution, int solution_id, double x_elt, double y_elt) override;

private:
    igFlux *flux;

};

//
// igSolverEuler.h ends here
