/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igSolverExport.h>

#include <vector>

using namespace std;

class IGSOLVER_EXPORT igFlux
{
public:
             igFlux(int variable_number, int dimension, bool flag_ALE);
    virtual ~igFlux(void);

public:
    int equationNumber(void) const;

public:
    virtual void setTime(double current_time);
    virtual void setDiffusionCoefficient(double coefficient);
    virtual void setArtificialViscosity(double artificial_viscosity);
    virtual void setArtificialViscosity(double artificial_viscosity_left, double artificial_viscosity_right);

public:
    virtual void physical(const vector<double> *state, const vector<double> *derivative, const vector<double> *mesh_vel, vector<double> *flux) = 0;

    virtual void numerical(const vector<double> *state_left, const vector<double> *derivative_left,
                           const vector<double> *state_right, const vector<double> *derivative_right,
                           vector<double> *flux_normal_left, vector<double> *flux_normal_right,
                           const vector<double> *mesh_vel, const double *normal) = 0;

    virtual double waveSpeed(const vector<double> *state, const vector<double> *mesh_vel) = 0;
    virtual double interfacialWaveSpeed(const vector<double> *state, const vector<double> *mesh_vel, const double *normal) = 0;

    virtual void boundaryFlux(const vector<double> *state_left, const vector<double> *derivative_left,
                              const vector<double> *state_right, const vector<double> *derivative_right,
                              vector<double> *flux_normal_left, vector<double> *flux_normal_right,
                              const vector<double> *mesh_vel, const double *normal, int face_type);

protected:
    double laxFriedrichsFlux(double flux_left, double flux_right, double state_left, double state_right, double speed);
    double hllFlux(double flux_left, double flux_right, double state_left, double state_right, double speed_left, double speed_right);

    double localDGFlux(double flux_left, double flux_right, double sign);
    double penaltyFlux(double state_left, double state_right);

    double centeredFlux(double flux_left, double flux_right);

protected:
    bool ALE_formulation;
    int variable_number;
    int dimension;

    int equation_number;

    double time;
};

//
// igFlux.h ends here
