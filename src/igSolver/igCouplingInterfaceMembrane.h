/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igSolverExport.h>

#include "igCouplingInterface.h"

using namespace std;

class igBasisBSpline;

class IGSOLVER_EXPORT igCouplingInterfaceMembrane : public igCouplingInterface
{
public:
             igCouplingInterfaceMembrane(void);
    virtual ~igCouplingInterfaceMembrane(void);

public:
    void initializeFluidInterface(int quadrature_number, int point_number, int dimension) override;
    void initializeStructureInterface(void) override;
    void initializeQuadrature(igMesh *mesh) override;
    void initializeMapping(igMesh *mesh) override;
    void initializeQuadratureMapping(int quadrature_number) override;

    void fillQuadrature(igMesh *mesh, int global_quadrature_number) override;

    void addLocalEffort(int counter, double Fx_inv, double Fx_vis, double Fy_inv, double Fy_vis, double Mz, double area, double nx, double ny) override;

    void convertStructureDisplacementsToFluid(void) override;
    void convertStructureVelocitiesToFluid(void) override;
    void convertFluidEffortsToStructure(void) override;


    string name(void) override;


private:
    igBasisBSpline *basis;

    int index_storage;

    vector<double> *interface_gauss_pts_local;
    vector<double> *interface_gauss_wgt_local;
    vector<int> *interface_gauss_label_local;


};

//
// igCouplingInterface.h ends here
