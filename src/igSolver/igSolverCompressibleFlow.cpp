/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igSolverCompressibleFlow.h"

#include <igCore/igMesh.h>
#include <igCore/igElement.h>
#include <igCore/igFace.h>
#include <igCore/igBoundaryIds.h>

#include <igDistributed/igCommunicator.h>

#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <math.h>
#include <limits>


///////////////////////////////////////////////////////////////

igSolverCompressibleFlow::igSolverCompressibleFlow(int variable_number, int dimension, vector<double> *solver_parameters, bool flag_ALE) : igSolver(variable_number, dimension, solver_parameters, flag_ALE)
{
    gamma = 1.4;
    mach_ref = solver_parameters->at(0);
    incidence = solver_parameters->at(2);

    open_effort_file = true;

    velocity_int.assign(3,0.);
    velocity_ext.assign(3,0.);
    velocity_mesh.assign(3,0.);
    velocity_bnd.assign(3,0.);
    normal.assign(3,0.);
    tangent1.assign(3,0.);
    tangent2.assign(3,0.);

    coord.assign(3,0.);
}

igSolverCompressibleFlow::~igSolverCompressibleFlow(void)
{
    effort_file.close();
}


//////////////////////////////////////////////////////////////////////////

void igSolverCompressibleFlow::computeSlipWall(void)
{
    // normal relative velocity
    double normal_velocity = 0;
    for(int idim=0; idim<dimension; idim++){
        normal_velocity += (velocity_int[idim] - velocity_mesh[idim])*normal[idim];
    }

    rho_bnd = rho_int;
    for(int idim=0; idim<dimension; idim++){
        velocity_bnd[idim] = velocity_int[idim] - normal_velocity*normal[idim];
    }
    p_bnd = p_int;
}

void igSolverCompressibleFlow::computeNoSlipWall(void)
{
    rho_bnd = rho_int;
    for(int idim=0; idim<dimension; idim++){
        velocity_bnd[idim] = velocity_mesh[idim];
    }
    p_bnd = p_int;
}

void igSolverCompressibleFlow::computeIODensityVelocityPressure(void)
{
    // normal relative velocity
    double normal_velocity = 0;
    for(int idim=0; idim<dimension; idim++){
        normal_velocity += (velocity_int[idim] - velocity_mesh[idim])*normal[idim];
    }

    bool outlet = true;
    if( normal_velocity < 0 ){
        outlet = false;
    }

    // selection inlet / outlet
    if(outlet){ // outlet case
        rho_bnd = rho_int;
        for(int idim=0; idim<dimension; idim++){
            velocity_bnd[idim] = velocity_int[idim];
        }
        p_bnd = p_ext;
    } else { // inlet case
        rho_bnd = rho_ext;
        for(int idim=0; idim<dimension; idim++){
            velocity_bnd[idim] = velocity_ext[idim];
        }
        p_bnd = p_int;
    }

}

void igSolverCompressibleFlow::computeIOSupersonic(void)
{
    // normal relative velocity
    double normal_velocity = 0;
    for(int idim=0; idim<dimension; idim++){
        normal_velocity += (velocity_int[idim] - velocity_mesh[idim])*normal[idim];
    }

    bool outlet = true;
    if( normal_velocity < 0 ){
        outlet = false;
    }

    // selection inlet / outlet
    if(outlet){ // outlet case
        rho_bnd = rho_int;
        for(int idim=0; idim<dimension; idim++){
            velocity_bnd[idim] = velocity_int[idim];
        }
        p_bnd = p_int;
    } else { // inlet case
        rho_bnd = rho_ext;
        for(int idim=0; idim<dimension; idim++){
            velocity_bnd[idim] = velocity_ext[idim];
        }
        p_bnd = p_ext;
    }
}

void igSolverCompressibleFlow::computeIORiemann(void)
{

    // normal relative velocity
    double normal_velocity = 0;
    for(int idim=0; idim<dimension; idim++){
        normal_velocity += (velocity_int[idim] - velocity_mesh[idim])*normal[idim];
    }

    bool outlet = true;
    if( normal_velocity < 0 ){
        outlet = false;
        for(int idim=0; idim<dimension; idim++){
            normal[idim] *= -1;
            tangent1[idim] *= -1;
            tangent2[idim] *= -1;
        }
    }

    // external state
    double sound_speed_ext = sqrt(gamma*p_ext/rho_ext);
    double normal_velocity_ext = velocity_ext[0]*normal[0] + velocity_ext[1]*normal[1] + velocity_ext[2]*normal[2];
    double tangent_velocity_ext1 = velocity_ext[0]*tangent1[0] + velocity_ext[1]*tangent1[1] + velocity_ext[2]*tangent1[2];
    double tangent_velocity_ext2 = velocity_ext[0]*tangent2[0] + velocity_ext[1]*tangent2[1] + velocity_ext[2]*tangent2[2];

    // internal state
    double sound_speed_int = sqrt(gamma*p_int/rho_int);
    double normal_velocity_int = velocity_int[0]*normal[0] + velocity_int[1]*normal[1] + velocity_int[2]*normal[2];
    double tangent_velocity_int1 = velocity_int[0]*tangent1[0] + velocity_int[1]*tangent1[1] + velocity_int[2]*tangent1[2];
    double tangent_velocity_int2 = velocity_int[0]*tangent2[0] + velocity_int[1]*tangent2[1] + velocity_int[2]*tangent2[2];

    // Riemann invariant
    double r1_int = normal_velocity_int - 2.*sound_speed_int/(gamma-1.);
    double r1_ext = normal_velocity_ext - 2.*sound_speed_ext/(gamma-1.);

    double r2_int = p_int/pow(rho_int,gamma);
    double r2_ext = p_ext/pow(rho_ext,gamma);

    double r3_int = tangent_velocity_int1;
    double r3_ext = tangent_velocity_ext1;
    double r3bis_int = tangent_velocity_int2;
    double r3bis_ext = tangent_velocity_ext2;

    double r4_int = normal_velocity_int + 2.*sound_speed_int/(gamma-1.);
    double r4_ext = normal_velocity_ext + 2.*sound_speed_ext/(gamma-1.);

    // selection inlet / outlet
    if(outlet){ // outlet case

        if(fabs(normal_velocity_int)/sound_speed_int<1.){ // subsonic case
            rho_bnd = pow( (r4_int-r1_ext)*(r4_int-r1_ext)*(gamma-1.)*(gamma-1.)/(16.*gamma*r2_int) , 1./(gamma-1.));
            velocity_bnd[0] = 0.5*(r1_ext+r4_int)*normal[0] + r3_int*tangent1[0] + r3bis_int*tangent2[0];
            velocity_bnd[1] = 0.5*(r1_ext+r4_int)*normal[1] + r3_int*tangent1[1] + r3bis_int*tangent2[1];
            velocity_bnd[2] = 0.5*(r1_ext+r4_int)*normal[2] + r3_int*tangent1[2] + r3bis_int*tangent2[2];
            p_bnd = r2_int*pow(rho_bnd,gamma);
        }
        else{ // supersonic case
            rho_bnd = rho_int;
            velocity_bnd[0] = velocity_int[0];
            velocity_bnd[1] = velocity_int[1];
            velocity_bnd[2] = velocity_int[2];
            p_bnd = p_int;
        }

    }
    else { // inlet case

        if(fabs(normal_velocity_ext)/sound_speed_ext<1.){ // subsonic case
            rho_bnd = pow( (r4_ext-r1_int)*(r4_ext-r1_int)*(gamma-1.)*(gamma-1.)/(16.*gamma*r2_ext) , 1./(gamma-1.));
            velocity_bnd[0] = 0.5*(r1_int+r4_ext)*normal[0] + r3_ext*tangent1[0] + r3bis_ext*tangent2[0];
            velocity_bnd[1] = 0.5*(r1_int+r4_ext)*normal[1] + r3_ext*tangent1[1] + r3bis_ext*tangent2[1];
            velocity_bnd[2] = 0.5*(r1_int+r4_ext)*normal[2] + r3_ext*tangent1[2] + r3bis_ext*tangent2[2];
            p_bnd = r2_ext*pow(rho_bnd,gamma);
        }
        else{ // supersonic case
            rho_bnd = rho_ext;
            velocity_bnd[0] = velocity_ext[0];
            velocity_bnd[1] = velocity_ext[1];
            velocity_bnd[2] = velocity_ext[2];
            p_bnd = p_ext;
        }

    }

}



//////////////////////////////////////////////////////////////////////////

void igSolverCompressibleFlow::retrieveInteriorExteriorQuantities(vector<double> *state_interior, vector<double> *state_exterior)
{
    // retrieve interior flow
    rho_int = (*state_interior)[0];
    double norm2 = 0;
    for(int idim=0; idim<dimension; idim++){
        velocity_int[idim] = (*state_interior)[idim+1]/rho_int;
        norm2 += velocity_int[idim]*velocity_int[idim];
    }
    p_int   = (gamma-1.)*((*state_interior)[dimension+1] - 0.5*rho_int*norm2);

    // retrieve exterior flow
    rho_ext = 1.4;
    velocity_ext[0] = mach_ref * cos(M_PI/180*incidence);
    velocity_ext[1] = mach_ref * sin(M_PI/180*incidence);
    velocity_ext[2] = 0.;
    p_ext = 1.;
}


void igSolverCompressibleFlow::retrieveGeometricalQuantities(igFace *face, vector<double> *mesh_vel, int index)
{
    double *normal_face = face->gaussPointNormalPtr(index);
    double *tangent1_face = face->gaussPointTangent1Ptr(index);

    for(int idim=0; idim<dimension; idim++){
        normal[idim] = normal_face[idim];
        tangent1[idim] = tangent1_face[idim];
        velocity_mesh[idim] = (*mesh_vel)[idim];
    }

    // 2nde tangent vector to be orthogonal
    tangent2[0] = normal[1]*tangent1[2] - normal[2]*tangent1[1];
    tangent2[1] = normal[2]*tangent1[0] - normal[0]*tangent1[2];
    tangent2[2] = normal[0]*tangent1[1] - normal[1]*tangent1[0];
}

void igSolverCompressibleFlow::recoverBoundaryState(vector<double> *state)
{
    // recover boundary conservative state quantities
    (*state)[0] = rho_bnd;
    double norm2 = 0;
    for(int idim=0; idim<dimension; idim++){
        (*state)[idim+1] = rho_bnd*velocity_bnd[idim];
        norm2 += velocity_bnd[idim]*velocity_bnd[idim];
    }
    (*state)[dimension+1] = p_bnd/(gamma-1.) + 0.5*rho_bnd*norm2;
}


////////////////////////////////////////////////////////////////

void igSolverCompressibleFlow::checkPositivity(void)
{
    vector<int> positivity;
    positivity.assign(mesh->elementNumber(), 0);

    int correction = 0;

    // NB: we use interior arrays as variables

    double eps = 1.E-12;
    double worst_pt = eps;
    double norm2;

    //------------ check quadrature points ------------
    for (int iel = 0; iel < mesh->elementNumber(); ++iel) {

        igElement *element = mesh->element(iel);

        if (element->isActive()) {

            int eval_number = element->gaussPointNumber();
            for (int k = 0; k < eval_number; ++k) {

                rho_int = this->solutionAtGaussPoint(element,k,0);
                norm2 = 0;
                for(int idim=0; idim<dimension; idim++){
                    velocity_int[idim] = this->solutionAtGaussPoint(element,k,idim+1)/rho_int;
                    norm2 += velocity_int[idim]*velocity_int[idim];
                }
                p_int   = (gamma-1.)*(this->solutionAtGaussPoint(element,k,dimension+1) - 0.5*rho_int*norm2);

                if(rho_int < eps)
                    positivity.at(iel) = 1;

                if(p_int < eps)
                    positivity.at(iel) = 2;

                if(rho_int < worst_pt){
                    for(int idim=0; idim<dimension; idim++){
                        coord[idim] = element->barycenter(idim);
                    }
                    worst_pt = rho_int;
                }

                if(p_int < worst_pt){
                    for(int idim=0; idim<dimension; idim++){
                        coord[idim] = element->barycenter(idim);
                    }
                    worst_pt = p_int;
                }

            }
        }
    }

    for (int ifac = 0; ifac < mesh->faceNumber(); ++ifac) {

        igFace *face = mesh->face(ifac);

        if (face->isActive()) {

            int eval_number = face->gaussPointNumber();
            for (int k = 0; k < eval_number; ++k) {

                // left
                if(face->leftInside()){

                    rho_int = this->solutionLeftAtGaussPoint(face,k,0);
                    norm2 = 0;
                    for(int idim=0; idim<dimension; idim++){
                        velocity_int[idim] = this->solutionLeftAtGaussPoint(face,k,idim+1)/rho_int;
                        norm2 += velocity_int[idim]*velocity_int[idim];
                    }
                    p_int   = (gamma-1.)*(this->solutionLeftAtGaussPoint(face,k,dimension+1) - 0.5*rho_int*norm2);

                    if(rho_int < eps)
                        positivity.at(face->leftElementIndex()) = 1;

                    if(p_int < eps)
                        positivity.at(face->leftElementIndex()) = 2;

                    if(rho_int < worst_pt){
                        for(int idim=0; idim<dimension; idim++){
                            coord[idim] = mesh->element(face->leftElementIndex())->barycenter(idim);
                        }
                        worst_pt = rho_int;
                    }

                    if(p_int < worst_pt){
                        for(int idim=0; idim<dimension; idim++){
                            coord[idim] = mesh->element(face->leftElementIndex())->barycenter(idim);
                        }
                        worst_pt = p_int;
                    }
                }

                // right
                if(face->rightInside()){
                    rho_int = this->solutionRightAtGaussPoint(face,k,0);
                    norm2 = 0;
                    for(int idim=0; idim<dimension; idim++){
                        velocity_int[idim] = this->solutionRightAtGaussPoint(face,k,idim+1)/rho_int;
                        norm2 += velocity_int[idim]*velocity_int[idim];
                    }
                    p_int   = (gamma-1.)*(this->solutionRightAtGaussPoint(face,k,dimension+1) - 0.5*rho_int*norm2);

                    if(rho_int < eps)
                        positivity.at(face->rightElementIndex()) = 1;

                    if(p_int < eps)
                        positivity.at(face->rightElementIndex()) = 2;

                    if(rho_int < worst_pt){
                        for(int idim=0; idim<dimension; idim++){
                            coord[idim] = mesh->element(face->rightElementIndex())->barycenter(idim);
                        }
                        worst_pt = rho_int;
                    }

                    if(p_int < worst_pt){
                        for(int idim=0; idim<dimension; idim++){
                            coord[idim] = mesh->element(face->rightElementIndex())->barycenter(idim);
                        }
                        worst_pt = p_int;
                    }
                }
            }

        }
    }

    for (int ifac = 0; ifac < mesh->boundaryFaceNumber(); ++ifac) {

        igFace *face = mesh->boundaryFace(ifac);

        if (face->isActive()) {

            int eval_number = face->gaussPointNumber();
            for (int k = 0; k < eval_number; ++k) {

                rho_int = this->solutionLeftAtGaussPoint(face,k,0);
                norm2 = 0;
                for(int idim=0; idim<dimension; idim++){
                    velocity_int[idim] = this->solutionLeftAtGaussPoint(face,k,idim+1)/rho_int;
                    norm2 += velocity_int[idim]*velocity_int[idim];
                }
                p_int   = (gamma-1.)*(this->solutionLeftAtGaussPoint(face,k,dimension+1) - 0.5*rho_int*norm2);

                if(rho_int < eps)
                    positivity.at(face->leftElementIndex()) = 1;

                if(p_int < eps)
                    positivity.at(face->leftElementIndex()) = 2;

                if(rho_int < worst_pt){
                    for(int idim=0; idim<dimension; idim++){
                        coord[idim] = mesh->element(face->leftElementIndex())->barycenter(idim);
                    }
                    worst_pt = rho_int;
                }

                if(p_int < worst_pt){
                    for(int idim=0; idim<dimension; idim++){
                        coord[idim] = mesh->element(face->leftElementIndex())->barycenter(idim);
                    }
                    worst_pt = p_int;
                }

            }

        }
    }

    //--------------------- retrieve positivity -----------------------

    for (int iel = 0; iel < mesh->elementNumber(); ++iel) {

        igElement *element = mesh->element(iel);

        if (element->isActive()) {

            if(positivity.at(iel) > 0){

                int function_number = element->controlPointNumber();
                int eval_number = element->gaussPointNumber();

                //--------- compute mean conservative values -------
                vector<double> mean;
                mean.assign(variable_number, 0.);

                for (int k = 0; k < eval_number; ++k) {

                    double gauss_point_weight = element->gaussPointWeight(k);
                    double gauss_point_jacobian = element->gaussPointJacobian(k);

                    for(int ivar=0; ivar<variable_number; ivar++)
                        mean.at(ivar) += this->solutionAtGaussPoint(element,k,ivar)*gauss_point_jacobian*gauss_point_weight;
                }
                for(int ivar=0; ivar<variable_number; ivar++)
                    mean.at(ivar) /= element->area();

                //----------- change dof according to mean values -----------
                for(int ivar=0; ivar<variable_number; ivar++){
                    for(int idof=0; idof<element->controlPointNumber(); idof++){

                        state_list->at(element->globalId(idof, ivar)) = mean.at(ivar);

                    }
                }
                correction++;

            }
        }
    }

    if(correction > 0)
        cout << "Warning: positivity corrections " << correction << " time " << time << " at " << coord[0] << " " << coord[1] << " " << coord[2] << endl;

}

////////////////////////////////////////////////////////////////
