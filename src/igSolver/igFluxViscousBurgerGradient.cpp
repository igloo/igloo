/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igFluxViscousBurgerGradient.h"

#include <fstream>
#include <iostream>
#include <math.h>
#include <igCore/igAssert.h>

///////////////////////////////////////////////////////////////

igFluxViscousBurgerGradient::igFluxViscousBurgerGradient(int variable_number, int dimension, bool flag_ALE) : igFlux(variable_number, dimension, flag_ALE)
{

}

igFluxViscousBurgerGradient::~igFluxViscousBurgerGradient(void)
{

}

///////////////////////////////////////////////////////////////

void igFluxViscousBurgerGradient::physical(const vector<double> *state, const vector<double> *derivative, const vector<double> *mesh_vel, vector<double> *flux)
{
    IG_ASSERT(state, "no state");
    IG_ASSERT(derivative, "no derivative");
    IG_ASSERT(flux, "no flux");

    (*flux)[0] = -sqrt(diffusion_coefficient) * (*state)[0];
}

///////////////////////////////////////////////////////////////

void igFluxViscousBurgerGradient::numerical(const vector<double> *state_left, const vector<double> *derivative_left, const vector<double> *state_right, const vector<double> *derivative_right, vector<double> *flux_normal_left, vector<double> *flux_normal_right, const vector<double> *mesh_vel, const double *normal)
{
    IG_ASSERT(state_left, "no state_left");
    IG_ASSERT(state_right, "no state_right");
    IG_ASSERT(derivative_left, "no derivative_left");
    IG_ASSERT(derivative_right, "no derivative_right");
    IG_ASSERT(flux_normal_left, "no flux_normal_left");
    IG_ASSERT(flux_normal_right, "no flux_normal_right");
    IG_ASSERT(normal, "no normal");

    // computation of fluxes
    this->physical(state_left, derivative_left, mesh_vel, flux_normal_left);
    this->physical(state_right, derivative_right, mesh_vel, flux_normal_right);

    // loop over variables
    for (int ivar = 0; ivar < variable_number; ++ivar) {

        double normal_flux_left  = (*flux_normal_left)[ivar]  * normal[0];
        double normal_flux_right = (*flux_normal_right)[ivar] * normal[0];

        // computation of the numerical flux (LDG)
        double flux = this->localDGFlux(normal_flux_left, normal_flux_right, -1.);

        (*flux_normal_left)[ivar]  = flux;
        (*flux_normal_right)[ivar] = flux;
    }
}

//////////////////////////////////////////////////////////////

double igFluxViscousBurgerGradient::waveSpeed(const vector<double> *, const vector<double> *)
{
    return 0;
}

double igFluxViscousBurgerGradient::interfacialWaveSpeed(const vector<double> *, const vector<double> *, const double *)
{
    return 0;
}

//////////////////////////////////////////////////////////////

void igFluxViscousBurgerGradient::setDiffusionCoefficient(double coefficient)
{
    diffusion_coefficient = coefficient;
}
