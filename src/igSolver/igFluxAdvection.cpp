/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igFluxAdvection.h"

#include <fstream>
#include <iostream>
#include <math.h>
#include <igCore/igAssert.h>

///////////////////////////////////////////////////////////////

igFluxAdvection::igFluxAdvection(int variable_number, int dimension, bool flag_ALE) : igFlux(variable_number, dimension, flag_ALE)
{
    advection_velocity.assign(dimension, 1.);
    //advection_velocity.at(0) = 1.;
    //advection_velocity.at(1) = 0.75;

    flux_left.assign(variable_number*dimension, 0.);
    flux_right.assign(variable_number*dimension, 0.);
}

igFluxAdvection::~igFluxAdvection(void)
{
}

///////////////////////////////////////////////////////////////

void igFluxAdvection::setTime(double current_time)
{
    this->time = current_time;

    if(dimension == 2){
        advection_velocity[0] = -4*M_PI*sin(2*M_PI*time);
        advection_velocity[1] =  4*M_PI*cos(2*M_PI*time);
    }
}

///////////////////////////////////////////////////////////////

void igFluxAdvection::physical(const vector<double> *state, const vector<double> *derivative, const vector<double> *mesh_vel, vector<double> *flux)
{
    IG_ASSERT(state, "no state");
    IG_ASSERT(derivative, "no derivative");
    IG_ASSERT(flux, "no flux");

    for (int idim = 0; idim < dimension; ++idim) {
        (*flux)[idim] = (*state)[0] * (advection_velocity[idim] - mesh_vel->at(idim));
    }
}

/////////////////////////////////////////////////////////////////

void igFluxAdvection::numerical(const vector<double> *state_left, const vector<double> *derivative_left,
                                const vector<double> *state_right, const vector<double> *derivative_right,
                                vector<double> *flux_normal_left, vector<double> *flux_normal_right,
								const vector<double> *mesh_vel, const double *normal)
{
    IG_ASSERT(state_left, "no state_left");
    IG_ASSERT(state_right, "no state_right");
    IG_ASSERT(derivative_left, "no derivative_left");
    IG_ASSERT(derivative_right, "no derivative_right");
    IG_ASSERT(flux_normal_left, "no flux_normal_left");
    IG_ASSERT(flux_normal_right, "no flux_normal_right");
    IG_ASSERT(normal, "no normal");

    // wave speed at left and right
    double left_wave_speed = this->interfacialWaveSpeed(state_left, mesh_vel, normal);
    double right_wave_speed = this->interfacialWaveSpeed(state_right, mesh_vel, normal);

    double max_wave_speed = max(fabs(left_wave_speed), fabs(right_wave_speed));

    // flux vector at left and right
    this->physical(state_left, derivative_left, mesh_vel, &flux_left);
    this->physical(state_right, derivative_right, mesh_vel, &flux_right);

    // normal flux at left and right
    (*flux_normal_left)[0] = 0.;
    (*flux_normal_right)[0] = 0.;
    for (int idim = 0; idim < dimension; ++idim) {
        (*flux_normal_left)[0] += flux_left[idim] * normal[idim];
        (*flux_normal_right)[0] += flux_right[idim] * normal[idim];
    }

    // computation of the numerical flux (Lax-Friedrichs)
    double flux = this->laxFriedrichsFlux((*flux_normal_left)[0], (*flux_normal_right)[0],
                                          (*state_left)[0], (*state_right)[0],
                                          max_wave_speed);

    (*flux_normal_left)[0]  = flux;
    (*flux_normal_right)[0] = flux;
}

//////////////////////////////////////////////////////////////////

double igFluxAdvection::waveSpeed(const vector<double> *state, const vector<double> *mesh_vel)
{
    double speed = 0.;
    for (int idim = 0; idim < dimension; ++idim) {
        speed += (advection_velocity[idim] - mesh_vel->at(idim)) * (advection_velocity[idim] - mesh_vel->at(idim));
    }
    return sqrt(speed);
}

double igFluxAdvection::interfacialWaveSpeed(const vector<double> *state, const vector<double> *mesh_vel, const double *normal)
{
    double normal_speed = 0.;
    for (int idim = 0; idim < dimension; ++idim) {
        normal_speed += (advection_velocity[idim] - mesh_vel->at(idim))* normal[idim];
    }
    return normal_speed;
}
