/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igErrorEstimatorViscosity.h"

#include "igSolver.h"

#include <iostream>
#include <math.h>

#include <igCore/igMesh.h>
#include <igCore/igCell.h>
#include <igCore/igElement.h>
#include <igCore/igFace.h>
#include <igCore/igAssert.h>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igErrorEstimatorViscosity::igErrorEstimatorViscosity(void) : igErrorEstimator()
{

}

igErrorEstimatorViscosity::~igErrorEstimatorViscosity(void)
{

}

/////////////////////////////////////////////////////////////////////////////

void igErrorEstimatorViscosity::computeEstimator(void)
{
    error_xi->assign(mesh->elementNumber(),0.);
    error_eta->assign(mesh->elementNumber(),0.);

    solver->computeViscosityCriterion(error_xi, error_eta);
}
