/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igSourceViscousBurger.h"

#include <fstream>
#include <iostream>
#include <math.h>
#include <igCore/igAssert.h>

///////////////////////////////////////////////////////////////

igSourceViscousBurger::igSourceViscousBurger(void) : igSource()
{

}

igSourceViscousBurger::~igSourceViscousBurger(void)
{

}

///////////////////////////////////////////////////////////////

void igSourceViscousBurger::value(vector<double> *state, vector<double> *state_gradient,
                                  vector<double> *derivative, vector<double> *derivative_gradient,
                                  vector<double> *source)
{
    IG_ASSERT(state, "no state");
    IG_ASSERT(derivative, "no derivative");
    IG_ASSERT(state_gradient, "no flux");
    IG_ASSERT(derivative_gradient, "no flux");
    IG_ASSERT(source, "no flux");

    source->at(0) = 0.;

    int variable_number = state->size();
    if (variable_number > 1) {
        source->at(1) = 0.;
    }
    if (variable_number > 2) {
        source->at(2) = 1./(2.*sqrt(diffusion_coefficient))*derivative_gradient->at(0);
    }
}

///////////////////////////////////////////////////////////////

void igSourceViscousBurger::setDiffusionCoefficient(double coefficient)
{
    diffusion_coefficient = coefficient;
}
