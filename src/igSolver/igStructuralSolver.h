/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igSolverExport.h>

#include <fstream>
#include <vector>

class igCommunicator;

using namespace std;

class IGSOLVER_EXPORT igStructuralSolver
{
public:
    igStructuralSolver(vector<double> *interface_gauss_pts, vector<double> *interface_gauss_wgt, vector<double> *interface_efforts, vector<double> *interface_displacements, vector<double> *interface_velocities);
    virtual ~igStructuralSolver(void);

public:
    void setTimeStep(double step);
    void setCommunicator(igCommunicator *com);

public:
    virtual void initialize(int degree, int ctrl_pt_number, vector<double> *knot_vector, vector<double> *pt_x, vector<double> *pt_y, int gauss_pt_number);
    virtual void compute(void);

protected:
    vector<double> *interface_efforts;
    vector<double> *interface_displacements;
    vector<double> *interface_velocities;
    vector<double> *interface_gauss_pts;
    vector<double> *interface_gauss_wgt;

    double time_step;

    igCommunicator *communicator;

};

//
// igStructuralSolver.h ends here
