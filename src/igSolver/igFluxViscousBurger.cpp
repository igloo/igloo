/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igFluxViscousBurger.h"

#include <fstream>
#include <iostream>
#include <math.h>
#include <igCore/igAssert.h>

///////////////////////////////////////////////////////////////

igFluxViscousBurger::igFluxViscousBurger(int variable_number, int dimension, bool flag_ALE) : igFlux(variable_number, dimension, flag_ALE)
{
    convective_flux_left = new vector<double>(variable_number, 0.);
    convective_flux_right = new vector<double>(variable_number, 0.);

    diffusive_flux_left = new vector<double>(variable_number, 0);
    diffusive_flux_right = new vector<double>(variable_number, 0);
}

igFluxViscousBurger::~igFluxViscousBurger(void)
{
    delete convective_flux_left;
    delete convective_flux_right;
    delete diffusive_flux_left;
    delete diffusive_flux_right;
}

///////////////////////////////////////////////////////////////

void igFluxViscousBurger::physical(const vector<double> *state, const vector<double> *derivative, const vector<double> *mesh_vel, vector<double> *flux)
{
    IG_ASSERT(state, "no state");
    IG_ASSERT(derivative, "no derivative");
    IG_ASSERT(flux, "no flux");

    this->convective_physical_flux(state, convective_flux_left);
    this->diffusive_physical_flux(derivative, diffusive_flux_left);

    for (int ivar = 0; ivar < variable_number; ++ivar) {
        (*flux)[ivar] = (*convective_flux_left)[ivar] + (*diffusive_flux_left)[ivar];
    }
}

///////////////////////////////////////////////////////////////

void igFluxViscousBurger::numerical(const vector<double> *state_left, const vector<double> *derivative_left,
                                    const vector<double> *state_right, const vector<double> *derivative_right,
                                    vector<double> *flux_normal_left, vector<double> *flux_normal_right,
									const vector<double> *mesh_vel, const double *normal)
{
    IG_ASSERT(state_left, "no state_left");
    IG_ASSERT(state_right, "no state_right");
    IG_ASSERT(derivative_left, "no derivative_left");
    IG_ASSERT(derivative_right, "no derivative_right");
    IG_ASSERT(flux_normal_left, "no flux_normal_left");
    IG_ASSERT(flux_normal_right, "no flux_normal_right");
    IG_ASSERT(normal, "no normal");

    // wave speed at left and right
    double left_wave_speed = this->waveSpeed(state_left, mesh_vel);
    double right_wave_speed = this->waveSpeed(state_right, mesh_vel);

    double max_wave_speed = max(fabs(left_wave_speed), fabs(right_wave_speed));

    // computation of convective fluxes
    this->convective_physical_flux(state_left,  convective_flux_left);
    this->convective_physical_flux(state_right, convective_flux_right);

    // computation of diffusive fluxes
    this->diffusive_physical_flux(derivative_left,  diffusive_flux_left);
    this->diffusive_physical_flux(derivative_right, diffusive_flux_right);

    // loop over variables
    for (int ivar = 0; ivar < variable_number; ++ivar) {

        double normal_conv_flux_left  = (*convective_flux_left)[ivar]*normal[0];
        double normal_conv_flux_right = (*convective_flux_right)[ivar]*normal[0];

        double normal_diff_flux_left  = (*diffusive_flux_left)[ivar] * normal[0];
        double normal_diff_flux_right = (*diffusive_flux_right)[ivar] * normal[0];

        // computation of the numerical flux (Lax-Friedrichs)
        double flux = this->laxFriedrichsFlux(normal_conv_flux_left, normal_conv_flux_right,
                                              (*state_left)[ivar], (*state_right)[ivar],
                                              max_wave_speed)
                    + this->localDGFlux(normal_diff_flux_left, normal_diff_flux_right, 1.);

        (*flux_normal_left)[ivar]  = flux;
        (*flux_normal_right)[ivar] = flux;
    }
}

//////////////////////////////////////////////////////////////

double igFluxViscousBurger::waveSpeed(const vector<double> *state, const vector<double> *mesh_vel)
{
    double speed = (*state)[0];

    return speed;
}

double igFluxViscousBurger::interfacialWaveSpeed(const vector<double> *state, const vector<double> *mesh_vel, const double *normal)
{
    double speed = (*state)[0] * normal[0];

    return speed;
}


//////////////////////////////////////////////////////////////

void igFluxViscousBurger::setDiffusionCoefficient(double coefficient)
{
    diffusion_coefficient = coefficient;
}

///////////////////////////////////////////////////////////////

void igFluxViscousBurger::convective_physical_flux(const vector<double> *state, vector<double> *flux)
{
    (*flux)[0] = 0.5 * (*state)[0]*(*state)[0];
}

///////////////////////////////////////////////////////////////

void igFluxViscousBurger::diffusive_physical_flux(const vector<double> *derivative, vector<double> *flux)
{
    double sqrt_diff_coeff = sqrt(diffusion_coefficient);
    double flux_0 = - sqrt_diff_coeff*(*derivative)[0];
    (*flux)[0] = flux_0;
}
