/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igErrorEstimatorWall.h"

#include <iostream>
#include <math.h>

#include <igCore/igMesh.h>
#include <igCore/igCell.h>
#include <igCore/igElement.h>
#include <igCore/igFace.h>
#include <igCore/igAssert.h>
#include <igCore/igBoundaryIds.h>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igErrorEstimatorWall::igErrorEstimatorWall(void) : igErrorEstimator()
{
    refine_counter = 0;
    refine_direction = 0;
}

igErrorEstimatorWall::~igErrorEstimatorWall(void)
{

}

/////////////////////////////////////////////////////////////////////////////


void igErrorEstimatorWall::computeEstimator(void)
{
    error_xi->assign(mesh->elementNumber(),0.);
    error_eta->assign(mesh->elementNumber(),0.);

    double value_error = 1;

    refine_counter++;
    int refined_elt_counter = 0;

    for (int ifac=0; ifac<mesh->boundaryFaceNumber(); ifac++){

        igFace *face = mesh->boundaryFace(ifac);
        if(face->isActive()){

            if(face->type() == id_noslip_wall || face->type() == id_slip_wall){ // no slip boundary condition

                int elt_id = face->leftElementIndex();
                int orientation = face->leftOrientation();

                if(orientation == 1 || orientation == 3){
                	refine_direction = 0;
                    (*error_xi)[elt_id] = value_error;
                }
                else{
                	refine_direction = 1;
                    (*error_eta)[elt_id] = value_error;
                }

                refined_elt_counter++;
            }

        }
    }

    cout << "wall refinement: " << refine_counter << " number of refined elements: " << refined_elt_counter << endl;
    cout << endl;


}
