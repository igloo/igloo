/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igSolverAdvection.h"

#include "igFluxAdvection.h"

#include <igCore/igMesh.h>
#include <igCore/igElement.h>
#include <igCore/igFace.h>
#include <igCore/igAssert.h>
#include <igGenerator/igSolutionAnalyticAdvection.h>

#include <fstream>
#include <iostream>
#include <math.h>
#include <limits>

///////////////////////////////////////////////////////////////

igSolverAdvection::igSolverAdvection(int variable_number, int dimension, vector<double> *solver_parameters, bool flag_ALE) : igSolver(variable_number, dimension, solver_parameters, flag_ALE)
{
    flux = new igFluxAdvection(variable_number, dimension, flag_ALE);
}

igSolverAdvection::~igSolverAdvection(void)
{

}

/////////////////////////////////////////////////////////////

void igSolverAdvection::computeIncrement(vector<double> *increment)
{
    // reset residual list
    this->resetResidual();

    // update time for time-dependent cases
    flux->setTime(time);

    // compute the different DG terms
    this->volumicIntegrator(flux,residual_list);
    this->surfacicIntegrator(flux,residual_list);
    this->boundaryIntegrator(flux,residual_list);

    this->updateSolution(increment,residual_list,variable_number);
}

void igSolverAdvection::computeIncrementAle(vector<double> *increment)
{
	// reset residual list
	this->resetResidual();

	// update time for time-dependent cases
	flux->setTime(time);

	// compute the different DG terms
	this->volumicIntegrator(flux,residual_list);
	this->surfacicIntegrator(flux,residual_list);
	this->boundaryIntegrator(flux,residual_list);

    for(int idof=0; idof<residual_list->size(); idof++)
    	increment->at(idof) = residual_list->at(idof);

}

//////////////////////////////////////////////////////////////

double igSolverAdvection::timeStep(void)
{
	vector<double> *mesh_vel = new vector<double>(this->dimension,0.);
    double time_step = numeric_limits<double>::max();

    // for time-dependent cases:
    flux->setTime(time);

    vector<double> *local_state = new vector<double>;
    local_state->resize(variable_number);
    vector<double> *derivative = new vector<double>;
    derivative->resize(variable_number);

    for (int iel=0; iel<mesh->elementNumber(); iel++){

        igElement *elt = mesh->element(iel);

        // NB : this is an (over) estimate of wave speed (cf convex properties of NURBS)
        double max_wave_speed = 0;
        for (int idof=0; idof<elt->controlPointNumber(); idof++){
        	if(ALE_formulation)
        		elt->controlPointVelocity(idof,mesh_vel);
            this->extractState(elt,idof,local_state,derivative);
            max_wave_speed = fmax(max_wave_speed,flux->waveSpeed(local_state,mesh_vel));
        }

        double diameter = 2*elt->radius() / (1. + 2*elt->cellDegree());

        //if(elt->cellDegree() == 0)
        //    diameter = (mesh->element(mesh->elementNumber()-1)->controlPointCoordinate(0,0)
        //                - mesh->element(0)->controlPointCoordinate(0,0) )/mesh->elementNumber();

        double local_step = diameter / max_wave_speed;
        time_step = fmin(time_step, local_step);
    }

    delete local_state;
    delete derivative;
    delete mesh_vel;

    return time_step * this->cfl_coefficient;
}

//////////////////////////////////////////////////////////////////

void igSolverAdvection::exactSolution(double *position, double time, vector<double> *solution, int solution_id, double x_elt, double y_elt)
{
    double *generic_param;
    
  solution->at(0) = solution_advection_circular(position, time, 0, generic_param, generic_param);
}

//////////////////////////////////////////////////////////////////
