/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igFluxNavierStokes3D.h"

#include <fstream>
#include <iostream>
#include <math.h>
#include <igCore/igAssert.h>
#include <igCore/igBoundaryIds.h>

///////////////////////////////////////////////////////////////

igFluxNavierStokes3D::igFluxNavierStokes3D(int variable_number, int dimension, bool flag_ALE) : igFluxCompressibleFlow3D(variable_number, dimension, flag_ALE)
{

    diffusive_flux_left  = new vector<double>(variable_number*dimension, 0.);
    diffusive_flux_right = new vector<double>(variable_number*dimension, 0.);

    prandtl = 0.72;

    local_viscosity = 0;
    local_viscosity_left = 0;
    local_viscosity_right = 0;
}

igFluxNavierStokes3D::~igFluxNavierStokes3D(void)
{
    delete diffusive_flux_left;
    delete diffusive_flux_right;
}

///////////////////////////////////////////////////////////////

void igFluxNavierStokes3D::physical(const vector<double> *state, const vector<double> *derivative, const vector<double> *mesh_vel, vector<double> *flux)
{
    IG_ASSERT(state, "no state");
    IG_ASSERT(derivative, "no derivative");
    IG_ASSERT(flux, "no flux");

    this->convective_physical_flux(state, mesh_vel, convective_flux_left);
    this->diffusive_physical_flux(state, derivative, local_viscosity, diffusive_flux_left);

    for (int ivar = 0; ivar < equation_number*dimension; ++ivar) {
        (*flux)[ivar] = (*convective_flux_left)[ivar] + (*diffusive_flux_left)[ivar];
    }
}


/////////////////////////////////////////////////////////////////

void igFluxNavierStokes3D::numerical(const vector<double> *state_left, const vector<double> *derivative_left,
                                   const vector<double> *state_right, const vector<double> *derivative_right,
                                   vector<double> *flux_normal_left, vector<double> *flux_normal_right,
                                   const vector<double> *mesh_vel, const double *normal)
{
    IG_ASSERT(state_left, "no state_left");
    IG_ASSERT(state_right, "no state_right");
    IG_ASSERT(derivative_left, "no derivative_left");
    IG_ASSERT(derivative_right, "no derivative_right");
    IG_ASSERT(flux_normal_left, "no flux_normal_left");
    IG_ASSERT(flux_normal_right, "no flux_normal_right");
    IG_ASSERT(normal, "no normal");

    // wave speed at left and right
    double max_wave_speed = max(this->maxInterfacialWaveSpeed(state_left,mesh_vel,normal), this->maxInterfacialWaveSpeed(state_right,mesh_vel,normal));
    double min_wave_speed = min(this->minInterfacialWaveSpeed(state_left,mesh_vel,normal), this->minInterfacialWaveSpeed(state_right,mesh_vel,normal));

    // computation of convective fluxes
    this->convective_physical_flux(state_left, mesh_vel, convective_flux_left);
    this->convective_physical_flux(state_right, mesh_vel, convective_flux_right);

    // computation of diffusive fluxes
    this->diffusive_physical_flux(state_left, derivative_left, local_viscosity_left, diffusive_flux_left);
    this->diffusive_physical_flux(state_right, derivative_right, local_viscosity_right, diffusive_flux_right);

    // loop over variables
    for (int ivar = 0; ivar < variable_number; ++ivar) {

        double *conv_flux_var_left  = &(*convective_flux_left)[ivar*dimension];
        double *conv_flux_var_right = &(*convective_flux_right)[ivar*dimension];
        double *diff_flux_var_left  = &(*diffusive_flux_left)[ivar*dimension];
        double *diff_flux_var_right = &(*diffusive_flux_right)[ivar*dimension];

        // normal flux at left and right
        double normal_conv_flux_left = 0.;
        double normal_conv_flux_right = 0.;
        double normal_diff_flux_left = 0.;
        double normal_diff_flux_right = 0.;
        for (int idim = 0; idim < dimension; ++idim) {
            normal_conv_flux_left  +=  conv_flux_var_left[idim] * normal[idim];
            normal_conv_flux_right += conv_flux_var_right[idim] * normal[idim];

            normal_diff_flux_left  +=  diff_flux_var_left[idim] * normal[idim];
            normal_diff_flux_right += diff_flux_var_right[idim] * normal[idim];
        }

        // convective numerical flux (HLL)
        double flux = this->hllFlux(normal_conv_flux_left, normal_conv_flux_right,
                                    (*state_left)[ivar], (*state_right)[ivar],
                                    min_wave_speed, max_wave_speed);

        // diffusive numerical flux (centered)
        flux += this->centeredFlux(normal_diff_flux_left, normal_diff_flux_right);

        (*flux_normal_left)[ivar]  = flux;
        (*flux_normal_right)[ivar] = flux;
    }
}

//////////////////////////////////////////////////////////////

void igFluxNavierStokes3D::boundaryFlux(const vector<double> *state_left, const vector<double> *derivative_left, const vector<double> *state_right, const vector<double> *derivative_right, vector<double> *flux_normal_left, vector<double> *flux_normal_right, const vector<double> *mesh_vel, const double *normal, int face_type)
{
    double nx = normal[0];
    double ny = normal[1];
    double nz = normal[2];

    if(face_type == id_slip_wall || face_type == id_noslip_wall || face_type == id_slip_wall_fsi || face_type == id_noslip_wall_fsi) { // specific treatment for adiabatic walls

        // computation of physical fluxes FOR BOUNDARY STATES
        this->convective_physical_flux(state_right, mesh_vel, convective_flux_left);
        this->diffusive_physical_flux(state_right, derivative_right, local_viscosity_right, diffusive_flux_left);

        // computation of normal flux
        for (int ivar = 0; ivar < variable_number; ++ivar) {

            double *conv_flux_var_left = &(*convective_flux_left)[ivar*dimension];
            double *diff_flux_var_left = &(*diffusive_flux_left)[ivar*dimension];

            // normal flux at left
            double normal_conv_flux_left = 0.;
            double normal_diff_flux_left = 0.;
            for (int idim = 0; idim < dimension; ++idim) {
                normal_conv_flux_left += conv_flux_var_left[idim] * normal[idim];
                normal_diff_flux_left += diff_flux_var_left[idim] * normal[idim];
            }
            (*flux_normal_left)[ivar] = normal_conv_flux_left + normal_diff_flux_left;
        }

        // modification of flux for adiabaticity condition
        (*flux_normal_left)[1+dimension] -= qx*nx + qy*ny + qz*nz; // energy flux


    } else {
        this->numerical(state_left, derivative_left, state_right, derivative_right,
                        flux_normal_left, flux_normal_right, mesh_vel, normal);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////

void igFluxNavierStokes3D::diffusive_physical_flux(const vector<double> *state, const vector<double> *derivative, double local_viscosity, vector<double> *flux)
{

    double rho =  (*state)[0];
    double u = (*state)[1]/rho;
    double v = (*state)[2]/rho;
    double w = (*state)[3]/rho;
    double ei = (*state)[4]/rho -0.5*(u*u+v*v+w*w);

    double DrhoDx = (*derivative)[0];
    double DrhoDy = (*derivative)[1];
    double DrhoDz = (*derivative)[2];

    double DuDx = ((*derivative)[3] - DrhoDx*u)/rho;
    double DuDy = ((*derivative)[4] - DrhoDy*u)/rho;
    double DuDz = ((*derivative)[5] - DrhoDz*u)/rho;

    double DvDx = ((*derivative)[6] - DrhoDx*v)/rho;
    double DvDy = ((*derivative)[7] - DrhoDy*v)/rho;
    double DvDz = ((*derivative)[8] - DrhoDz*v)/rho;

    double DwDx = ((*derivative)[9]  - DrhoDx*w)/rho;
    double DwDy = ((*derivative)[10] - DrhoDy*w)/rho;
    double DwDz = ((*derivative)[11] - DrhoDz*w)/rho;

    double DeiDx = ((*derivative)[12] - 0.5*DrhoDx*(u*u+v*v+w*w) - rho*(u*DuDx+v*DvDx+w*DwDx) - DrhoDx*ei )/rho;
    double DeiDy = ((*derivative)[13] - 0.5*DrhoDy*(u*u+v*v+w*w) - rho*(u*DuDy+v*DvDy+w*DwDy) - DrhoDy*ei )/rho;
    double DeiDz = ((*derivative)[14] - 0.5*DrhoDz*(u*u+v*v+w*w) - rho*(u*DuDz+v*DvDz+w*DwDz) - DrhoDz*ei )/rho;

    double Txx = 2./3. * (diffusion_coefficient+local_viscosity) * ( 2*DuDx - DvDy - DwDz);
    double Txy =  (diffusion_coefficient+local_viscosity) * ( DuDy + DvDx);
    double Txz =  (diffusion_coefficient+local_viscosity) * ( DuDz + DwDx);
    double Tyy = 2./3. * (diffusion_coefficient+local_viscosity) * ( 2*DvDy - DwDz - DuDx);
    double Tyz =  (diffusion_coefficient+local_viscosity) * ( DvDz + DwDy);
    double Tzz = 2./3. * (diffusion_coefficient+local_viscosity) * ( 2*DwDz - DuDx - DvDy);

    qx = -gamma*(diffusion_coefficient+local_viscosity)/prandtl * DeiDx;
    qy = -gamma*(diffusion_coefficient+local_viscosity)/prandtl * DeiDy;
    qz = -gamma*(diffusion_coefficient+local_viscosity)/prandtl * DeiDz;

    // density flux
    (*flux)[0] = 0.;
    (*flux)[1] = 0.;
    (*flux)[2] = 0.;

    // momentum-x flux
    (*flux)[3] = -Txx;
    (*flux)[4] = -Txy;
    (*flux)[5] = -Txz;

    // momentum-y flux
    (*flux)[6] = -Txy;
    (*flux)[7] = -Tyy;
    (*flux)[8] = -Tyz;

    // momentum-z flux
    (*flux)[9] =  -Txz;
    (*flux)[10] = -Tyz;
    (*flux)[11] = -Tzz;

    // energy flux
    (*flux)[12] = -u*Txx - v*Txy - w*Txz + qx;
    (*flux)[13] = -u*Txy - v*Tyy - w*Tyz + qy;
    (*flux)[14] = -u*Txz - v*Tyz - w*Tzz + qz;

}


//////////////////////////////////////////////////////////////

void igFluxNavierStokes3D::setDiffusionCoefficient(double coefficient)
{
    diffusion_coefficient = coefficient;
}

//////////////////////////////////////////////////////////////

void igFluxNavierStokes3D::setArtificialViscosity(double artificial_viscosity)
{
    local_viscosity = artificial_viscosity;
}

void igFluxNavierStokes3D::setArtificialViscosity(double artificial_viscosity_left, double artificial_viscosity_right)
{
    local_viscosity_left = artificial_viscosity_left;
    local_viscosity_right = artificial_viscosity_right;
}

//////////////////////////////////////////////////////////////////
