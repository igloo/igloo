/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igSolverExport.h>

#include "igFlux.h"

#include <vector>

using namespace std;

class IGSOLVER_EXPORT igFluxBurger : public igFlux
{
public:
     igFluxBurger(int variable_number, int dimension, bool flag_ALE);
    ~igFluxBurger(void);

public:
    void physical(const vector<double> *state, const vector<double> *derivative, const vector<double> *mesh_vel, vector<double> *flux) override;

    void numerical(const vector<double> *state_left, const vector<double> *derivative_left,
                   const vector<double> *state_right, const vector<double> *derivative_right,
                   vector<double> *flux_normal_left, vector<double> *flux_normal_right,
				   const vector<double> *mesh_vel, const double *normal) override;

    double waveSpeed(const vector<double> *state, const vector<double> *mesh_vel) override;
    double interfacialWaveSpeed(const vector<double> *state, const vector<double> *mesh_vel, const double *normal) override;
};

//
// igFluxBurger.h ends here
