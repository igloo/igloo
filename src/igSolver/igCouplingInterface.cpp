/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igCouplingInterface.h"

#include "igBasisBSpline.h"

#include <string>


///////////////////////////////////////////////////////////////

igCouplingInterface::igCouplingInterface(void)
{
    // allocate arrays
    this->pt_x = new vector<double>;
    this->pt_y = new vector<double>;
    this->knot_vector = new vector<double>;

    // interface arrays for the structure
    this->interface_efforts_s = new vector<double>;
    this->interface_displacements_s = new vector<double>;
    this->interface_velocities_s = new vector<double>;

    // interface arrays for the fluid
    this->interface_efforts_f = new vector<double>;
    this->interface_displacements_f = new vector<double>;
    this->interface_velocities_f = new vector<double>;

    // mapping face-element for communicator
    this->interface_mapping = new vector<int>;

    // counter of FSI quadrature for communicator
    this->interface_quadrature_counter_f = new vector<int>;
    this->interface_quadrature_counter_s = new vector<int>;

    // interface array for the Gauss points
    this->interface_gauss_pts = new vector<double>;
    this->interface_gauss_wgt = new vector<double>;
    this->interface_gauss_label = new vector<int>;
}

igCouplingInterface::~igCouplingInterface(void)
{
    delete this->interface_mapping;
    delete this->interface_quadrature_counter_f;
    delete this->interface_quadrature_counter_s;

    delete this->interface_efforts_s;
    delete this->interface_displacements_s;
    delete this->interface_velocities_s;

    delete this->interface_efforts_f;
    delete this->interface_displacements_f;
    delete this->interface_velocities_f;

    delete this->interface_gauss_pts;
    delete this->interface_gauss_wgt;
    delete this->interface_gauss_label;

    delete this->knot_vector;
    delete this->pt_x;
    delete this->pt_y;

}

/////////////////////////////////////////////////////////////

void igCouplingInterface::initializeStructureInterface(void)
{

}

void igCouplingInterface::initializeFluidInterface(int quadrature_number, int point_number, int dimension)
{
    // by default storage of local efforts+moment
    interface_efforts_f->resize(quadrature_number*(dimension+1),0);

    // by default storage of two translation+rotation
    interface_displacements_f->resize(dimension+1, 0);
    interface_velocities_f->resize(dimension+1, 0);
}

void igCouplingInterface::initializeQuadraturePointNumber(int fluid_gauss_pts_number)
{
    //  by default quadrature points = fluid gauss points
    quadrature_point_per_face = fluid_gauss_pts_number;
}

void igCouplingInterface::initializeMapping(igMesh *mesh)
{

}

void igCouplingInterface::initializeQuadrature(igMesh *mesh)
{

}

void igCouplingInterface::initializeQuadratureMapping(int quadrature_number)
{

}

void igCouplingInterface::fillQuadrature(igMesh *mesh, int global_quadrature_number)
{

}

void igCouplingInterface::setCommunicator(igCommunicator *communicator)
{
    this->communicator = communicator;
}

//////////////////////////////////////////////////////////////

void igCouplingInterface::resetLocalEffort(void)
{
    int size = interface_efforts_f->size();
    for(int i=0; i<size; i++){
        interface_efforts_f->at(i) = 0;
    }
}

void igCouplingInterface::addLocalEffort(int counter, double Fx_inv, double Fx_vis, double Fy_inv, double Fy_vis, double Mz, double area, double nx, double ny)
{
    // by default store local efforts contributions
    int index = counter*3;
    interface_efforts_f->at(index+0) = (Fx_inv+Fx_vis)*area;
    interface_efforts_f->at(index+1) = (Fy_inv+Fy_vis)*area;
    interface_efforts_f->at(index+2) = Mz*area;
}

///////////////////////////////////////////////////////////////

void igCouplingInterface::convertStructureDisplacementsToFluid(void)
{
    // by default simple copy
    for(int i=0; i<interface_displacements_s->size(); i++){
        interface_displacements_f->at(i) = interface_displacements_s->at(i);
    }
}

void igCouplingInterface::convertStructureVelocitiesToFluid(void)
{
    // by default simple copy
    for(int i=0; i<interface_velocities_s->size(); i++){
        interface_velocities_f->at(i) = interface_velocities_s->at(i);
    }
}

void igCouplingInterface::convertFluidEffortsToStructure(void)
{
    // by default simple copy
    for(int i=0; i<interface_efforts_f->size(); i++){
        interface_efforts_s->at(i) = interface_efforts_f->at(i);
    }
}

/////////////////////////////////////////////////////////////

vector<double>* igCouplingInterface::interfaceEffortsStructure(void)
{
    return this->interface_efforts_s;
}

vector<double>* igCouplingInterface::interfaceDisplacementsStructure(void)
{
    return this->interface_displacements_s;
}

vector<double>* igCouplingInterface::interfaceVelocitiesStructure(void)
{
    return this->interface_velocities_s;
}

vector<double>* igCouplingInterface::interfaceEffortsFluid(void)
{
    return this->interface_efforts_f;
}

vector<double>* igCouplingInterface::interfaceDisplacementsFluid(void)
{
    return this->interface_displacements_f;
}

vector<double>* igCouplingInterface::interfaceVelocitiesFluid(void)
{
    return this->interface_velocities_f;
}

vector<double>* igCouplingInterface::interfaceGaussPoints(void)
{
    return this->interface_gauss_pts;
}

vector<double>* igCouplingInterface::interfaceGaussWeights(void)
{
    return this->interface_gauss_wgt;
}

/////////////////////////////////////////////////////////////////

vector<double>* igCouplingInterface::knotVector(void)
{
    return this->knot_vector;
}

vector<double>* igCouplingInterface::controlPointsX(void)
{
    return this->pt_x;
}

vector<double>* igCouplingInterface::controlPointsY(void)
{
    return this->pt_y;
}

int igCouplingInterface::interfaceDegree(void)
{
    return this->degree;
}

int igCouplingInterface::controlPointNumber(void)
{
    return this->ctrl_pt_number;
}

int igCouplingInterface::gaussPointNumber(void)
{
    return this->gauss_pt_number;
}

int igCouplingInterface::elementNumber(void)
{
    return this->elt_number;
}

int igCouplingInterface::quadraturePointNumber(void)
{
    return this->quadrature_point_per_face;
}

string igCouplingInterface::name(void)
{
    return "interface";
}
