/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igFluxCompressibleFlow2D.h"

#include <fstream>
#include <iostream>
#include <math.h>
#include <igCore/igAssert.h>
#include <igCore/igBoundaryIds.h>

///////////////////////////////////////////////////////////////

igFluxCompressibleFlow2D::igFluxCompressibleFlow2D(int variable_number, int dimension, bool flag_ALE) : igFlux(variable_number, dimension, flag_ALE)
{
    convective_flux_left  = new vector<double>(variable_number*dimension, 0.);
    convective_flux_right = new vector<double>(variable_number*dimension, 0.);
}

igFluxCompressibleFlow2D::~igFluxCompressibleFlow2D(void)
{
    delete convective_flux_left;
    delete convective_flux_right;
}

///////////////////////////////////////////////////////////////

void igFluxCompressibleFlow2D::convective_physical_flux(const vector<double> *state, const vector<double> *mesh_vel, vector<double> *flux)
{

    double rho =  (*state)[0];
    double u = (*state)[1]/rho;
    double v = (*state)[2]/rho;
    double energy = (*state)[3];
    double p = (gamma-1.)*(energy - 0.5*rho*(u*u+v*v));

    double u_relative = u - mesh_vel->at(0);
    double v_relative = v - mesh_vel->at(1);

    // density flux
    (*flux)[0] = rho*u_relative;
    (*flux)[1] = rho*v_relative;

    // momentum-x flux
    (*flux)[2] = rho*u*u_relative + p;
    (*flux)[3] = rho*u*v_relative;

    // momentum-y flux
    (*flux)[4] = rho*v*u_relative;
    (*flux)[5] = rho*v*v_relative + p;

    // energy flux
    (*flux)[6] = energy*u_relative + p*u;
    (*flux)[7] = energy*v_relative + p*v;

}


////////////////////////////////////////////////////////////////

void igFluxCompressibleFlow2D::hllcFlux(const vector<double> *state_left, const vector<double> *state_right,
                           vector<double> *flux_normal_left, vector<double> *flux_normal_right, const double *normal)
{
    // geometric data
    double nx = normal[0];
    double ny = normal[1];

    // left state
    double rho_left = (*state_left)[0];
    double u_left   = (*state_left)[1]/rho_left;
    double v_left   = (*state_left)[2]/rho_left;
    double energy_left = (*state_left)[3];

    double specific_energy_left = energy_left/rho_left;
    double p_left = (gamma-1.)*(energy_left - 0.5*rho_left*(u_left*u_left+v_left*v_left));
    double sound_speed_left = sqrt(gamma*p_left/rho_left);
    double normal_velocity_left = u_left*nx + v_left*ny;
    double enthalpy_left = gamma/(gamma-1.)*p_left/rho_left + 0.5*(u_left*u_left+v_left*v_left);

    // right state
    double rho_right = (*state_right)[0];
    double u_right   = (*state_right)[1]/rho_right;
    double v_right   = (*state_right)[2]/rho_right;
    double energy_right = (*state_right)[3];

    double specific_energy_right = energy_right/rho_right;
    double p_right = (gamma-1.)*(energy_right - 0.5*rho_right*(u_right*u_right+v_right*v_right));
    double sound_speed_right = sqrt(gamma*p_right/rho_right);
    double normal_velocity_right = u_right*nx + v_right*ny;
    double enthalpy_right = gamma/(gamma-1.)*p_right/rho_right + 0.5*(u_right*u_right+v_right*v_right);

    // Roe average
    double left_sqrt_density  = sqrt(rho_left);
    double right_sqrt_density = sqrt(rho_right);
    double average_ratio = 1. / (left_sqrt_density + right_sqrt_density);

    double average_enthalpy = ( left_sqrt_density * enthalpy_left + right_sqrt_density * enthalpy_right ) * average_ratio;
    double average_normal_velocity = ( left_sqrt_density * normal_velocity_left + right_sqrt_density * normal_velocity_right ) * average_ratio;
    double average_u = ( left_sqrt_density * u_left + right_sqrt_density * u_right ) * average_ratio;
    double average_v = ( left_sqrt_density * v_left + right_sqrt_density * v_right ) * average_ratio;

    double average_sound_speed = sqrt( (gamma - 1.) * ( average_enthalpy - 0.5 * (average_u*average_u+average_v*average_v) ) );


    // computation of the fastest signal velocity
   double left_fastest_speed  = min( normal_velocity_left  - sound_speed_left , average_normal_velocity - average_sound_speed );
   double right_fastest_speed = max( normal_velocity_right + sound_speed_right, average_normal_velocity + average_sound_speed );

   // computation of the contact discontinuity velocity
   double contact_speed = ( p_right - p_left
                           + rho_left * normal_velocity_left * (left_fastest_speed - normal_velocity_left)
                           - rho_right * normal_velocity_right * (right_fastest_speed - normal_velocity_right) ) /
                         (  rho_left * (left_fastest_speed - normal_velocity_left)
                          - rho_right * (right_fastest_speed - normal_velocity_right) );

   // Computation of the star pressure using left or right state (both are equivalent)
   double star_pressure = p_left + rho_left * (left_fastest_speed - normal_velocity_left)
                                            * (contact_speed - normal_velocity_left);

   // Computation of the numerical flux according to the sign of velocities
   if (left_fastest_speed > 0.) {
       (*flux_normal_left)[0] = rho_left * normal_velocity_left;
       (*flux_normal_left)[1] = (*flux_normal_left)[0] * u_left + p_left * nx;
       (*flux_normal_left)[2] = (*flux_normal_left)[0] * v_left + p_left * ny;
       (*flux_normal_left)[3] = (*flux_normal_left)[0] * (specific_energy_left + p_left / rho_left);


   } else if (contact_speed > 0.) {
       double u_tangent = u_left - normal_velocity_left * nx ;
       double v_tangent = v_left - normal_velocity_left * ny ;

       (*flux_normal_left)[0] = rho_left * (left_fastest_speed - normal_velocity_left) / (left_fastest_speed - contact_speed) * contact_speed;
       (*flux_normal_left)[1] = (*flux_normal_left)[0] * (contact_speed * nx + u_tangent) + star_pressure * nx;
       (*flux_normal_left)[2] = (*flux_normal_left)[0] * (contact_speed * ny + v_tangent) + star_pressure * ny;
       (*flux_normal_left)[3] = (*flux_normal_left)[0] * (specific_energy_left + p_left / rho_left + left_fastest_speed * (contact_speed - normal_velocity_left));

   } else if (right_fastest_speed >= 0.) {
       double u_tangent = u_right - normal_velocity_right * nx ;
       double v_tangent = v_right - normal_velocity_right * ny ;

       (*flux_normal_left)[0] = rho_right * (right_fastest_speed - normal_velocity_right) / (right_fastest_speed - contact_speed) * contact_speed;
       (*flux_normal_left)[1] = (*flux_normal_left)[0] * (contact_speed * nx + u_tangent) + star_pressure * nx;
       (*flux_normal_left)[2] = (*flux_normal_left)[0] * (contact_speed * ny + v_tangent) + star_pressure * ny;
       (*flux_normal_left)[3] = (*flux_normal_left)[0] * (specific_energy_right + p_right / rho_right + right_fastest_speed * (contact_speed - normal_velocity_right));

   } else {
       (*flux_normal_left)[0] = rho_right * normal_velocity_right;
       (*flux_normal_left)[1] = (*flux_normal_left)[0] * u_right + p_right * nx;
       (*flux_normal_left)[2] = (*flux_normal_left)[0] * v_right + p_right * ny;
       (*flux_normal_left)[3] = (*flux_normal_left)[0] * (specific_energy_right + p_right / rho_right);

   }

   for (int ivar = 0; ivar < variable_number; ++ivar) {
       (*flux_normal_right)[ivar] = (*flux_normal_left)[ivar];
   }
}


//////////////////////////////////////////////////////////////////

double igFluxCompressibleFlow2D::waveSpeed(const vector<double> *state, const vector<double> *mesh_vel)
{
    double rho = (*state)[0];
    double u   = (*state)[1] / rho;
    double v   = (*state)[2] / rho;
    double u_mesh = mesh_vel->at(0);
    double v_mesh = mesh_vel->at(1);
    double p = (gamma-1.) * ((*state)[3] - 0.5 * rho * (u*u + v*v));

    double velocity = sqrt(pow(u - u_mesh,2) + pow(v - v_mesh,2));
    double sound_speed = sqrt(gamma*p/rho);

    return velocity + sound_speed;
}


double igFluxCompressibleFlow2D::interfacialWaveSpeed(const vector<double> *state, const vector<double> *mesh_vel, const double *normal)
{
    double nx = normal[0];
    double ny = normal[1];

    double rho = (*state)[0];
    double u   = (*state)[1] / rho;
    double v   = (*state)[2] / rho;
    double u_mesh = mesh_vel->at(0);
    double v_mesh = mesh_vel->at(1);
    double p = (gamma-1.) * ((*state)[3] - 0.5 * rho * (u*u + v*v));

    double normal_velocity = (u - u_mesh)*nx + (v - v_mesh)*ny;
    double sound_speed = sqrt(gamma*p/rho);

    return fabs(normal_velocity) + sound_speed;
}

double igFluxCompressibleFlow2D::maxInterfacialWaveSpeed(const vector<double> *state, const vector<double> *mesh_vel, const double *normal)
{
    double nx = normal[0];
    double ny = normal[1];

    double rho = (*state)[0];
    double u   = (*state)[1] / rho;
    double v   = (*state)[2] / rho;
    double u_mesh = mesh_vel->at(0);
    double v_mesh = mesh_vel->at(1);
    double p = (gamma-1.) * ((*state)[3] - 0.5 * rho * (u*u + v*v));

    double normal_velocity = (u - u_mesh)*nx + (v - v_mesh)*ny;
    double sound_speed = sqrt(gamma*p/rho);

    return max(normal_velocity + sound_speed, 0.);
}

double igFluxCompressibleFlow2D::minInterfacialWaveSpeed(const vector<double> *state, const vector<double> *mesh_vel, const double *normal)
{
    double nx = normal[0];
    double ny = normal[1];

    double rho = (*state)[0];
    double u   = (*state)[1] / rho;
    double v   = (*state)[2] / rho;
    double u_mesh = mesh_vel->at(0);
    double v_mesh = mesh_vel->at(1);
    double p = (gamma-1.) * ((*state)[3] - 0.5 * rho * (u*u + v*v));

    double normal_velocity = (u - u_mesh)*nx + (v - v_mesh)*ny;
    double sound_speed = sqrt(gamma*p/rho);

    return min(normal_velocity - sound_speed, 0.);
}

///////////////////////////////////////////////////////////////
