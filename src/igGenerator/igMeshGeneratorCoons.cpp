/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/

#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>

#include <igCore/igLinAlg.h>
#include <igFiles/igFileManager.h>

#include "igMeshGeneratorCoons.h"
#include "../igCore/igBasisBezier.h"

/////////////////////////////////////////////////////////////////////////

void find_coord(double x, double y, double &k,double &q);
void find_coord_boundary(double x, double y, double &k,double &q, int face_type);

double ringleb_rho(double k, double q);
double ringleb_p(double k, double q);
double ringleb_a(double k, double q);
double ringleb_j(double k, double q);
double ringleb_x(double k, double q);
double ringleb_y(double k, double q);

double k(double x1, double k_min, double k_max);
double q(double x2, double k);


/////////////////////////////////////////////////////////////////////////////

igMeshGeneratorCoons::igMeshGeneratorCoons() : igMeshGenerator()
{

}

igMeshGeneratorCoons::~igMeshGeneratorCoons(void)
{

}

/////////////////////////////////////////////////////////////////////////////

void igMeshGeneratorCoons::generate(void)
{
	igLinAlg lin_solver;

    int sample_number = 2*(degree+1);
    int size = degree-1;
    int pts_per_elt = (degree+1)*(degree+1);
    int discretization_steps = 100000;

    igBasis *basis = new igBasisBezier(degree);
    vector<double> values1;
    values1.assign(degree+1,0.);
    vector<double> values2;
    values2.assign(degree+1,0.);

    double *rhs_x = new double[size];
    double *rhs_y = new double[size];
    double *mat = new double[size*size];

    // problem parameters for ringleb problem
    double k_min = 0.7;
    double k_max = 1.5;
    double q_min = 0.4;

    //
    // structured grid generation and storage in a temporary arrays
    //

    // first cartesian grid
    vector<double> xcoord_tmp;
    xcoord_tmp.resize((element_number_1*degree+1)*(element_number_2*degree+1));
    vector<double> ycoord_tmp;
    ycoord_tmp.resize((element_number_1*degree+1)*(element_number_2*degree+1));
    vector<double> zcoord_tmp;
    zcoord_tmp.resize((element_number_1*degree+1)*(element_number_2*degree+1));
    vector<double> weight_tmp;
    weight_tmp.resize((element_number_1*degree+1)*(element_number_2*degree+1), 1.);


    //--------------- definition of outer wall boundary (kmin) -----------

    double k = k_min;

    double target_length = 0.;
    double q_previous = q_min;
    double q_current = q_min;
    for (int qit=0; qit<discretization_steps; qit++){
        q_current = min(k,q_min + (k-q_min)/(discretization_steps-1)*qit);
        target_length += sqrt( (ringleb_x(k,q_current)-ringleb_x(k,q_previous))*(ringleb_x(k,q_current)-ringleb_x(k,q_previous))
                           + (ringleb_y(k,q_current)-ringleb_y(k,q_previous))*(ringleb_y(k,q_current)-ringleb_y(k,q_previous)) );
        q_previous = q_current;
    }
    target_length /= float(element_number_2)/2.;

    double param_start = q_min;

    // loop over intervals
    for (int j=0; j<element_number_2; j++){

        // treatment of symmetry
        bool sym_top = true;
        int sign_incr = 1;
        if(2*j >= element_number_2){
            sym_top = false;
            sign_incr = -1;
        }

        //
        // extremities coordinates are known explicitly
        //

        // search q for uniform points distribution in (x,y)
        double q_first = param_start;
        double q_last = param_start;
        q_previous = param_start;
        double dist = 0.;
        do {
            q_last += sign_incr*(k-q_min)/element_number_2/discretization_steps;
            dist += sqrt((ringleb_x(k,q_previous)-ringleb_x(k,q_last))*(ringleb_x(k,q_previous)-ringleb_x(k,q_last))
                 + (ringleb_y(k,q_previous)-ringleb_y(k,q_last))*(ringleb_y(k,q_previous)-ringleb_y(k,q_last)));
            q_previous = q_last;
        } while (dist < target_length);

        // case of mid point
        if(2*j == element_number_2-2)
            q_last = k;
        // case of last point
        if(j == element_number_2-1)
            q_last = q_min;
        param_start = q_last;

        // points coordinates
        int index_1 = 0;
        int index_2_first = j*degree;
        int index_2_last = (j+1)*degree;

        int index_global_first = index_2_first *(element_number_1*degree+1) + index_1;
        int index_global_last  = index_2_last  *(element_number_1*degree+1) + index_1;

        xcoord_tmp.at(index_global_first) = ringleb_x(k,q_first);
        if(sym_top)
            ycoord_tmp.at(index_global_first) = ringleb_y(k,q_first);
        else
            ycoord_tmp.at(index_global_first) = - ringleb_y(k,q_first);

        xcoord_tmp.at(index_global_last) = ringleb_x(k,q_last);
        if(sym_top)
            ycoord_tmp.at(index_global_last) = ringleb_y(k,q_last);
        else
            ycoord_tmp.at(index_global_last) =  - ringleb_y(k,q_last);

        //
        // least-squares pb for internal control points
        //
        if(degree > 1){

            // initialize matrix and rhs, sol
            for (int ils=0; ils<size; ils++){
                rhs_x[ils] = 0.;
                rhs_y[ils] = 0.;
                for (int jls=0; jls<size; jls++)
                    mat[ils*size+jls] = 0.;
            }
            double target_length_sample = 0.;
            q_previous = q_first;
            q_current = q_first;
            for (int qit=0; qit<discretization_steps; qit++){
                q_current = min(k,q_first + (q_last-q_first)/(discretization_steps-1)*qit);
                target_length_sample += sqrt( (ringleb_x(k,q_current)-ringleb_x(k,q_previous))*(ringleb_x(k,q_current)-ringleb_x(k,q_previous))
                                   + (ringleb_y(k,q_current)-ringleb_y(k,q_previous))*(ringleb_y(k,q_current)-ringleb_y(k,q_previous)) );
                q_previous = q_current;
            }
            target_length_sample /= float(sample_number-1);
            double param_start_sample  = q_first;

            // compute matrix and rhs
            for (int kls=0; kls<sample_number; kls++){

                double coord1 = float(kls)/float(sample_number-1);
                basis->evalFunction(coord1,values1);

                // determine uniformy distributed in (x,y) target points
                double dist = 0.;
                double q = param_start_sample;
                q_previous = param_start_sample;
                do {
                    q += (q_last-q_first)/discretization_steps;
                    dist += sqrt((ringleb_x(k,q)-ringleb_x(k,q_previous))*(ringleb_x(k,q)-ringleb_x(k,q_previous))
                         + (ringleb_y(k,q)-ringleb_y(k,q_previous))*(ringleb_y(k,q)-ringleb_y(k,q_previous)));
                    q_previous = q;
                } while (dist < target_length_sample);

                // case of first point
                if(kls == 0)
                    q = q_first;
                // case of last point
                if(kls == sample_number-1)
                    q = q_last;
                param_start_sample = q;

                double target_x = ringleb_x(k,q);
                double target_y;
                if(sym_top)
                    target_y = ringleb_y(k,q);
                else
                    target_y = - ringleb_y(k,q);

                for (int ils=0; ils<size; ils++){

                    // rhs term
                    rhs_x[ils] += values1.at(ils+1) * target_x;
                    rhs_y[ils] += values1.at(ils+1) * target_y;

                    // matrix
                    for (int jls=0; jls<size; jls++)
                        mat[ils*size+jls] += values1.at(ils+1)*values1.at(jls+1);

                    // modification of rhs for bound constraints
                    rhs_x[ils] -= values1.at(ils+1)*values1.at(0)*xcoord_tmp.at(index_global_first)
                            +  values1.at(ils+1)*values1.at(degree)*xcoord_tmp.at(index_global_last);
                    rhs_y[ils] -= values1.at(ils+1)*values1.at(0)*ycoord_tmp.at(index_global_first)
                            +  values1.at(ils+1)*values1.at(degree)*ycoord_tmp.at(index_global_last);

                }

            }

            // solve system
            lin_solver.inverse(mat,size);

            // store solution
            for (int ils=0; ils<size; ils++){
                int index = index_global_first + (ils+1) *(element_number_1*degree+1);
                for (int jls=0; jls<size; jls++){
                    xcoord_tmp.at(index) += mat[ils*size+jls]*rhs_x[jls];
                    ycoord_tmp.at(index) += mat[ils*size+jls]*rhs_y[jls];
                }
            }

        }

    }

        //--------------- definition of inner wall boundary (kmax) -----------

    k = k_max;

    target_length = 0.;
    q_previous = q_min;
    q_current = q_min;
    for (int qit=0; qit<discretization_steps; qit++){
        q_current = min(k,q_min + (k-q_min)/(discretization_steps-1)*qit);
        target_length += sqrt( (ringleb_x(k,q_current)-ringleb_x(k,q_previous))*(ringleb_x(k,q_current)-ringleb_x(k,q_previous))
                           + (ringleb_y(k,q_current)-ringleb_y(k,q_previous))*(ringleb_y(k,q_current)-ringleb_y(k,q_previous)) );
        q_previous = q_current;
    }
    target_length /= float(element_number_2)/2.;

    param_start = q_min;

    // loop over intervals
    for (int j=0; j<element_number_2; j++){

        // treatment of symmetry
        bool sym_top = true;
        int sign_incr = 1;
        if(2*j >= element_number_2){
            sym_top = false;
            sign_incr = -1;
        }

        //
        // extremities coordinates are known explicitly
        //

        // search q for uniform points distribution in (x,y)
        double q_first = param_start;
        double q_last = param_start;
        q_previous = param_start;
        double dist = 0.;
        do {
            q_last += sign_incr*(k-q_min)/element_number_2/discretization_steps;
            dist += sqrt((ringleb_x(k,q_previous)-ringleb_x(k,q_last))*(ringleb_x(k,q_previous)-ringleb_x(k,q_last))
                 + (ringleb_y(k,q_previous)-ringleb_y(k,q_last))*(ringleb_y(k,q_previous)-ringleb_y(k,q_last)));
            q_previous = q_last;
        } while (dist < target_length);

        // case of mid point
        if(2*j == element_number_2-2)
            q_last = k;
        // case of last point
        if(j == element_number_2-1)
            q_last = q_min;
        param_start = q_last;

        int index_1 = element_number_1*degree;
        int index_2_first = j*degree;
        int index_2_last = (j+1)*degree;

        int index_global_first = index_2_first *(element_number_1*degree+1) + index_1;
        int index_global_last  = index_2_last  *(element_number_1*degree+1) + index_1;

        xcoord_tmp.at(index_global_first) = ringleb_x(k,q_first);
        if(sym_top)
            ycoord_tmp.at(index_global_first) = ringleb_y(k,q_first);
        else
            ycoord_tmp.at(index_global_first) = - ringleb_y(k,q_first);

        xcoord_tmp.at(index_global_last) = ringleb_x(k,q_last);
        if(sym_top)
            ycoord_tmp.at(index_global_last) = ringleb_y(k,q_last);
        else
            ycoord_tmp.at(index_global_last) =  - ringleb_y(k,q_last);

       //
        // least-squares pb for internal control points
        //

        if(degree > 1){

            // initialize matrix and rhs, sol
            for (int ils=0; ils<size; ils++){
                rhs_x[ils] = 0.;
                rhs_y[ils] = 0.;
                for (int jls=0; jls<size; jls++)
                    mat[ils*size+jls] = 0.;
            }
            double target_length_sample = 0.;
            q_previous = q_first;
            q_current = q_first;
            for (int qit=0; qit<discretization_steps; qit++){
                q_current = min(k,q_first + (q_last-q_first)/(discretization_steps-1)*qit);
                target_length_sample += sqrt( (ringleb_x(k,q_current)-ringleb_x(k,q_previous))*(ringleb_x(k,q_current)-ringleb_x(k,q_previous))
                                   + (ringleb_y(k,q_current)-ringleb_y(k,q_previous))*(ringleb_y(k,q_current)-ringleb_y(k,q_previous)) );
                q_previous = q_current;
            }
            target_length_sample /= float(sample_number-1);
            double param_start_sample  = q_first;

            // compute matrix and rhs
            for (int kls=0; kls<sample_number; kls++){

                double coord1 = float(kls)/float(sample_number-1);
                basis->evalFunction(coord1,values1);

                // determine uniformy distributed in (x,y) target points
                double dist = 0.;
                double q = param_start_sample;
                q_previous = param_start_sample;
                do {
                    q += (q_last-q_first)/discretization_steps;
                    dist += sqrt((ringleb_x(k,q)-ringleb_x(k,q_previous))*(ringleb_x(k,q)-ringleb_x(k,q_previous))
                         + (ringleb_y(k,q)-ringleb_y(k,q_previous))*(ringleb_y(k,q)-ringleb_y(k,q_previous)));
                    q_previous = q;
                } while (dist < target_length_sample);

                // case of first point
                if(kls == 0)
                    q = q_first;
                // case of last point
                if(kls == sample_number-1)
                    q = q_last;
                param_start_sample = q;

                double target_x = ringleb_x(k,q);
                double target_y;
                if(sym_top)
                    target_y = ringleb_y(k,q);
                else
                    target_y =  - ringleb_y(k,q);

                for (int ils=0; ils<size; ils++){

                    // rhs term
                    rhs_x[ils] += values1.at(ils+1) * target_x;
                    rhs_y[ils] += values1.at(ils+1) * target_y;

                    // matrix
                    for (int jls=0; jls<size; jls++)
                        mat[ils*size+jls] += values1.at(ils+1)*values1.at(jls+1);

                    // modification of rhs for bound constraints
                    rhs_x[ils] -= values1.at(ils+1)*values1.at(0)*xcoord_tmp.at(index_global_first)
                            +  values1.at(ils+1)*values1.at(degree)*xcoord_tmp.at(index_global_last);
                    rhs_y[ils] -= values1.at(ils+1)*values1.at(0)*ycoord_tmp.at(index_global_first)
                            +  values1.at(ils+1)*values1.at(degree)*ycoord_tmp.at(index_global_last);

                }

            }

            // solve system
            lin_solver.inverse(mat,size);

            // store solution
            for (int ils=0; ils<size; ils++){
                int index = index_global_first + (ils+1) *(element_number_1*degree+1);
                for (int jls=0; jls<size; jls++){
                    xcoord_tmp.at(index) += mat[ils*size+jls]*rhs_x[jls];
                    ycoord_tmp.at(index) += mat[ils*size+jls]*rhs_y[jls];
                }
            }

        }

    }

    //------------------ definition of inlet boundary (qmin) ------------

    double q = q_min;

    target_length = 0.;
    double k_previous = k_min;
    double k_current = k_min;
    for (int kit=0; kit<discretization_steps; kit++){
        k_current = k_min + (k_max-k_min)/(discretization_steps-1)*kit;
        target_length += sqrt( (ringleb_x(k_current,q)-ringleb_x(k_previous,q))*(ringleb_x(k_current,q)-ringleb_x(k_previous,q))
                           + (ringleb_y(k_current,q)-ringleb_y(k_previous,q))*(ringleb_y(k_current,q)-ringleb_y(k_previous,q)) );
        k_previous = k_current;
    }
    target_length /= float(element_number_1);

    param_start = k_min;

    // loop over intervals
    for (int i=0; i<element_number_1; i++){

        //
        // extremities coordinates are known explicitly
        //

        // search q for uniform points distribution in (x,y)
        double k_first = param_start;
        double k_last = param_start;
        k_previous = param_start;
        double dist = 0.;
        do {
            k_last += (k_max-k_min)/element_number_1/discretization_steps;
            dist += sqrt((ringleb_x(k_previous,q)-ringleb_x(k_last,q))*(ringleb_x(k_previous,q)-ringleb_x(k_last,q))
                 + (ringleb_y(k_previous,q)-ringleb_y(k_last,q))*(ringleb_y(k_previous,q)-ringleb_y(k_last,q)));
            k_previous = k_last;
        } while (dist < target_length);

        // case of last point
        if(i == element_number_1-1)
            k_last = k_max;
        param_start = k_last;

        int index_2 = 0;
        int index_1_first = i*degree;
        int index_1_last = (i+1)*degree;

        int index_global_first = index_2 *(element_number_1*degree+1) + index_1_first;
        int index_global_last  = index_2 *(element_number_1*degree+1) + index_1_last;

        xcoord_tmp.at(index_global_first) = ringleb_x(k_first,q);
        ycoord_tmp.at(index_global_first) = ringleb_y(k_first,q);

        xcoord_tmp.at(index_global_last) = ringleb_x(k_last,q);
        ycoord_tmp.at(index_global_last) = ringleb_y(k_last,q);


       //
        // least-squares pb for internal control points
        //

        if(degree > 1){

            // initialize matrix and rhs, sol
            for (int ils=0; ils<size; ils++){
                rhs_x[ils] = 0.;
                rhs_y[ils] = 0.;
                for (int jls=0; jls<size; jls++)
                    mat[ils*size+jls] = 0.;
            }
            double target_length_sample = 0.;
            k_previous = k_first;
            k_current = k_first;
            for (int kit=0; kit<discretization_steps; kit++){
                k_current = k_first + (k_last-k_first)/(discretization_steps-1)*kit;
                target_length_sample += sqrt( (ringleb_x(k_current,q)-ringleb_x(k_previous,q))*(ringleb_x(k_current,q)-ringleb_x(k_previous,q))
                                   + (ringleb_y(k_current,q)-ringleb_y(k_previous,q))*(ringleb_y(k_current,q)-ringleb_y(k_previous,q)) );
                k_previous = k_current;
            }
            target_length_sample /= float(sample_number-1);
            double param_start_sample  = k_first;

            // compute matrix and rhs
            for (int kls=0; kls<sample_number; kls++){

                double coord2 = float(kls)/float(sample_number-1);
                basis->evalFunction(coord2,values2);

                // determine uniformy distributed in (x,y) target points
                double dist = 0.;
                double k = param_start_sample;
                k_previous = param_start_sample;
                do {
                    k += (k_last-k_first)/discretization_steps;
                    dist += sqrt((ringleb_x(k,q)-ringleb_x(k_previous,q))*(ringleb_x(k,q)-ringleb_x(k_previous,q))
                         + (ringleb_y(k,q)-ringleb_y(k_previous,q))*(ringleb_y(k,q)-ringleb_y(k_previous,q)));
                    k_previous = k;
                } while (dist < target_length_sample);

                // case of first point
                if(kls == 0)
                    k = k_first;
                // case of last point
                if(kls == sample_number-1)
                    k = k_last;
                param_start_sample = k;

                double target_x = ringleb_x(k,q);
                double target_y = ringleb_y(k,q);

                for (int ils=0; ils<size; ils++){

                    // rhs term
                    rhs_x[ils] += values2.at(ils+1) * target_x;
                    rhs_y[ils] += values2.at(ils+1) * target_y;

                    // matrix
                    for (int jls=0; jls<size; jls++)
                        mat[ils*size+jls] += values2.at(ils+1)*values2.at(jls+1);

                    // modification of rhs for bound constraints
                    rhs_x[ils] -= values2.at(ils+1)*values2.at(0)*xcoord_tmp.at(index_global_first)
                            +  values2.at(ils+1)*values2.at(degree)*xcoord_tmp.at(index_global_last);
                    rhs_y[ils] -= values2.at(ils+1)*values2.at(0)*ycoord_tmp.at(index_global_first)
                            +  values2.at(ils+1)*values2.at(degree)*ycoord_tmp.at(index_global_last);

                }

            }

            // solve system
            lin_solver.inverse(mat,size);

            // store solution
            for (int ils=0; ils<size; ils++){
                int index = index_global_first + (ils+1);
                for (int jls=0; jls<size; jls++){
                    xcoord_tmp.at(index) += mat[ils*size+jls]*rhs_x[jls];
                    ycoord_tmp.at(index) += mat[ils*size+jls]*rhs_y[jls];
                }
            }

        }
    }

    //------------------ definition of outlet boundary (qmin y <0) ------------

    q = q_min;

    target_length = 0.;
    k_previous = k_min;
    k_current = k_min;
    for (int kit=0; kit<discretization_steps; kit++){
        k_current = k_min + (k_max-k_min)/(discretization_steps-1)*kit;
        target_length += sqrt( (ringleb_x(k_current,q)-ringleb_x(k_previous,q))*(ringleb_x(k_current,q)-ringleb_x(k_previous,q))
                           + (ringleb_y(k_current,q)-ringleb_y(k_previous,q))*(ringleb_y(k_current,q)-ringleb_y(k_previous,q)) );
        k_previous = k_current;
    }
    target_length /= float(element_number_1);

    param_start = k_min;

    // loop over intervals
    for (int i=0; i<element_number_1; i++){

        //
        // extremities coordinates are known explicitly
        //

        // search q for uniform points distribution in (x,y)
        double k_first = param_start;
        double k_last = param_start;
        k_previous = param_start;
        double dist = 0.;
        do {
            k_last += (k_max-k_min)/element_number_1/discretization_steps;
            dist += sqrt((ringleb_x(k_previous,q)-ringleb_x(k_last,q))*(ringleb_x(k_previous,q)-ringleb_x(k_last,q))
                 + (ringleb_y(k_previous,q)-ringleb_y(k_last,q))*(ringleb_y(k_previous,q)-ringleb_y(k_last,q)));
            k_previous = k_last;
        } while (dist < target_length);

        // case of last point
        if(i == element_number_1-1)
            k_last = k_max;
        param_start = k_last;

        int index_2 = element_number_2*degree;
        int index_1_first = i*degree;
        int index_1_last = (i+1)*degree;

        int index_global_first = index_2 *(element_number_1*degree+1) + index_1_first;
        int index_global_last  = index_2 *(element_number_1*degree+1) + index_1_last;

        xcoord_tmp.at(index_global_first) = ringleb_x(k_first,q);
        ycoord_tmp.at(index_global_first) = - ringleb_y(k_first,q);

        xcoord_tmp.at(index_global_last) = ringleb_x(k_last,q);
        ycoord_tmp.at(index_global_last) =  - ringleb_y(k_last,q);


       //
        // least-squares pb for internal control points
        //

        if(degree > 1){

            // initialize matrix and rhs, sol
            for (int ils=0; ils<size; ils++){
                rhs_x[ils] = 0.;
                rhs_y[ils] = 0.;
                for (int jls=0; jls<size; jls++)
                    mat[ils*size+jls] = 0.;
            }
            double target_length_sample = 0.;
            k_previous = k_first;
            k_current = k_first;
            for (int kit=0; kit<discretization_steps; kit++){
                k_current = k_first + (k_last-k_first)/(discretization_steps-1)*kit;
                target_length_sample += sqrt( (ringleb_x(k_current,q)-ringleb_x(k_previous,q))*(ringleb_x(k_current,q)-ringleb_x(k_previous,q))
                                   + (ringleb_y(k_current,q)-ringleb_y(k_previous,q))*(ringleb_y(k_current,q)-ringleb_y(k_previous,q)) );
                k_previous = k_current;
            }
            target_length_sample /= float(sample_number-1);
            double param_start_sample  = k_first;

            // compute matrix and rhs
            for (int kls=0; kls<sample_number; kls++){

                double coord2 = float(kls)/float(sample_number-1);
                basis->evalFunction(coord2,values2);

                // determine uniformy distributed in (x,y) target points
                double dist = 0.;
                double k = param_start_sample;
                k_previous = param_start_sample;
                do {
                    k += (k_last-k_first)/discretization_steps;
                    dist += sqrt((ringleb_x(k,q)-ringleb_x(k_previous,q))*(ringleb_x(k,q)-ringleb_x(k_previous,q))
                         + (ringleb_y(k,q)-ringleb_y(k_previous,q))*(ringleb_y(k,q)-ringleb_y(k_previous,q)));
                    k_previous = k;
                } while (dist < target_length_sample);

                // case of first point
                if(kls == 0)
                    k = k_first;
                // case of last point
                if(kls == sample_number-1)
                    k = k_last;
                param_start_sample = k;

                double target_x = ringleb_x(k,q);
                double target_y = - ringleb_y(k,q);

                for (int ils=0; ils<size; ils++){

                    // rhs term
                    rhs_x[ils] += values2.at(ils+1) * target_x;
                    rhs_y[ils] += values2.at(ils+1) * target_y;

                    // matrix
                    for (int jls=0; jls<size; jls++)
                        mat[ils*size+jls] += values2.at(ils+1)*values2.at(jls+1);

                    // modification of rhs for bound constraints
                    rhs_x[ils] -= values2.at(ils+1)*values2.at(0)*xcoord_tmp.at(index_global_first)
                            +  values2.at(ils+1)*values2.at(degree)*xcoord_tmp.at(index_global_last);
                    rhs_y[ils] -= values2.at(ils+1)*values2.at(0)*ycoord_tmp.at(index_global_first)
                            +  values2.at(ils+1)*values2.at(degree)*ycoord_tmp.at(index_global_last);

                }

            }

            // solve system
            lin_solver.inverse(mat,size);

            // store solution
            for (int ils=0; ils<size; ils++){
                int index = index_global_first + (ils+1);
                for (int jls=0; jls<size; jls++){
                    xcoord_tmp.at(index) += mat[ils*size+jls]*rhs_x[jls];
                    ycoord_tmp.at(index) += mat[ils*size+jls]*rhs_y[jls];
                }
            }

        }

    }

    //----------------- interior grid filling by discrete coons approach ---------------

    for (int j=1; j<element_number_2*degree; j++){
        for (int i=1; i<element_number_1*degree; i++){

            int index = (element_number_1*degree+1)*j + i;
            int index_00 = 0;
            int index_10 = element_number_1*degree;
            int index_11 =  (element_number_1*degree+1)*(element_number_2*degree) + element_number_1*degree;
            int index_01 = (element_number_1*degree+1)*element_number_2*degree;
            int index_i0 = i;
            int index_i1 = (element_number_1*degree+1)*element_number_2*degree + i;
            int index_0j = (element_number_1*degree+1)*j;
            int index_1j = (element_number_1*degree+1)*j + element_number_1*degree;

            double weight_i0 = 1.-double(j)/(element_number_2*degree);
            double weight_i1 = double(j)/(element_number_2*degree);
            double weight_0j = 1.-double(i)/(element_number_1*degree);
            double weight_1j = double(i)/(element_number_1*degree);

            xcoord_tmp.at(index) = weight_0j*xcoord_tmp.at(index_0j) + weight_1j*xcoord_tmp.at(index_1j)
                                 + weight_i0*xcoord_tmp.at(index_i0) + weight_i1*xcoord_tmp.at(index_i1)
                                 - weight_i0*weight_0j*xcoord_tmp.at(index_00)
                                 - weight_1j*weight_i1*xcoord_tmp.at(index_11)
                                 - weight_i0*weight_1j*xcoord_tmp.at(index_10)
                                 - weight_0j*weight_i1*xcoord_tmp.at(index_01);

            ycoord_tmp.at(index) = weight_0j*ycoord_tmp.at(index_0j) + weight_1j*ycoord_tmp.at(index_1j)
                                 + weight_i0*ycoord_tmp.at(index_i0) + weight_i1*ycoord_tmp.at(index_i1)
                                 - weight_i0*weight_0j*ycoord_tmp.at(index_00)
                                 - weight_1j*weight_i1*ycoord_tmp.at(index_11)
                                 - weight_i0*weight_1j*ycoord_tmp.at(index_10)
                                 - weight_0j*weight_i1*ycoord_tmp.at(index_01);

            zcoord_tmp.at(index) = 0.;
        }
    }


    //---------------- convert to DG structure --------------------

    xcoord.resize(element_number_1*element_number_2*(degree+1)*(degree+1));
    ycoord.resize(element_number_1*element_number_2*(degree+1)*(degree+1));
    zcoord.resize(element_number_1*element_number_2*(degree+1)*(degree+1));
    weight.resize(element_number_1*element_number_2*(degree+1)*(degree+1),1.);

    this->dgMeshConverter(xcoord_tmp, ycoord_tmp, zcoord_tmp, weight_tmp, xcoord, ycoord, zcoord, weight);


    //----------------- apply default face type ------------------

    face_type.resize(element_number_1*element_number_2*4,0);

    this->applyBoundaryCondition();
}
