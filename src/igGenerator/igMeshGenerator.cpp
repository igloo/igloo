/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include <iostream>
#include <fstream>
#include <math.h>

#include <igFiles/igFileManager.h>
#include <igCore/igMesh.h>
#include <igCore/igElement.h>
#include <igCore/igFace.h>
#include <igCore/igDistributor.h>

#include "igMeshGenerator.h"
#include "igBoundaryIds.h"

/////////////////////////////////////////////////////////////////////////////

igMeshGenerator::igMeshGenerator(void)
{
    this->mesh = nullptr;
    this->config_filename = "baseline.dat";
    this->periodic = false;
    this->enforce_membrane = false;
    this->distribution_file = false;

    this->communicator = nullptr;
}

/////////////////////////////////////////////////////////////////////////////

void igMeshGenerator::setCommunicator(igCommunicator *communicator)
{
    this->communicator = communicator;
}

void igMeshGenerator::setDegree(int degree)
{
    this->degree = degree;
}

void igMeshGenerator::setGaussPointNumber(int number)
{
    this->gauss_pt_number = number;
}

void igMeshGenerator::setVariableNumber(int number)
{
    this->variable_number = number;
}

void igMeshGenerator::setDerivativeNumber(int number)
{
    this->derivative_number = number;
}

void igMeshGenerator::setExactSolutionId(int id)
{
    this->exact_solution_id = id;
}

void igMeshGenerator::setElementNumber(int element_number_1, int element_number_2, int element_number_3)
{
    this->element_number_1 = element_number_1;
    this->element_number_2 = element_number_2;
    this->element_number_3 = element_number_3;
}

void igMeshGenerator::setDomainBounds(double xi_min, double xi_max, double eta_min, double eta_max, double zeta_min, double zeta_max)
{
    this->xi_min = xi_min;
    this->xi_max = xi_max;
    this->eta_min = eta_min;
    this->eta_max = eta_max;
    this->zeta_min = zeta_min;
    this->zeta_max = zeta_max;
}

void igMeshGenerator::writeMeshFile(const string& mesh_filename)
{
    igFileManager manager;

    mesh->updateMeshProperties();
    mesh->updateGlobalMeshProperties();

    if(this->mesh->meshDimension() == 2) { // 2D case

        cout << "write mesh: GLVis format" << endl;
        manager.writeMesh2DGlvis("igloo.mesh", this->mesh,0,false);

    } else if (this->mesh->meshDimension() == 3){ // 3D case

        cout << "write mesh: GLVis format" << endl;
        manager.writeMesh3DGlvis("igloo.mesh", this->mesh,0,false);
        //manager.writeStructuredMesh3DGlvis(element_number_1, element_number_2, element_number_3, degree, xcoord, ycoord, zcoord, weight, 1, 1, 1);

    }

    // igloo mesh writer
    igDistributor *distributor = new igDistributor;
    distributor->setMesh(mesh);
    distributor->setPartitionNumber(1);
    distributor->run();

    cout << "write mesh: Igloo format" << endl;
    manager.writeMeshIgloo(mesh_filename,this->mesh, distributor);

}

void igMeshGenerator::setXFunction(function<double (double, double, double)> x_fun)
{
    this->x_fun = x_fun;
}

void igMeshGenerator::setYFunction(function<double (double, double, double)> y_fun)
{
    this->y_fun = y_fun;
}

void igMeshGenerator::setZFunction(function<double (double, double, double)> z_fun)
{
    this->z_fun = z_fun;
}

void igMeshGenerator::setDitributionXiFunction(function<double (int, int, double, double)> distrib_xi_fun)
{
    this->distrib_xi_fun = distrib_xi_fun;
}

void igMeshGenerator::setDitributionEtaFunction(function<double (int, int, double, double)> distrib_eta_fun)
{
    this->distrib_eta_fun = distrib_eta_fun;
}

void igMeshGenerator::setDitributionZetaFunction(function<double (int, int, double, double)> distrib_zeta_fun)
{
    this->distrib_zeta_fun = distrib_zeta_fun;
}

void igMeshGenerator::setRotation(double angle)
{
    this->angle = angle;
}

void igMeshGenerator::setScale(double scale)
{
    this->scale = scale;
}

void igMeshGenerator::setPeriodicity(bool periodic)
{
    this->periodic = periodic;
}

void igMeshGenerator::setSubdomains(bool flag_subdomain)
{
	this->subdomains = flag_subdomain;
}

void igMeshGenerator::setInitialPatch(vector<double> &xcoord_in, vector<double> &ycoord_in, vector<double> &weight_in, int element_number_1_in, int element_number_2_in)
{
    this->xcoord_in = xcoord_in;
    this->ycoord_in = ycoord_in;
    this->weight_in = weight_in;

    this->element_number_1_in = element_number_1_in;
    this->element_number_2_in = element_number_2_in;
}

void igMeshGenerator::setConfigFileName(string name)
{
    this->config_filename = name;
}

void igMeshGenerator::setBoundaryConditions(int condition_id_south, int condition_id_east, int condition_id_north, int condition_id_west, int condition_id_top, int condition_id_bottom)
{
    this->condition_id_south = condition_id_south;
    this->condition_id_east = condition_id_east;
    this->condition_id_north = condition_id_north;
    this->condition_id_west = condition_id_west;
    this->condition_id_top = condition_id_top;
    this->condition_id_bottom = condition_id_bottom;
}

void igMeshGenerator::setDistributionFile(bool distribution_file)
{
    this->distribution_file = distribution_file;
}

void igMeshGenerator::setClamp(int number)
{
    this->clamp_number = number;
}

///////////////////////////////////////////////////////////

vector<double>& igMeshGenerator::xCoordinate()
{
    return xcoord;
}

vector<double>& igMeshGenerator::yCoordinate()
{
    return ycoord;
}

vector<double>& igMeshGenerator::zCoordinate()
{
    return zcoord;
}

vector<double>& igMeshGenerator::weightCoordinate()
{
    return weight;
}

///////////////////////////////////////////////////////////

void igMeshGenerator::dgMeshConverter(vector<double> &xcoord_tmp, vector<double> &ycoord_tmp, vector<double> &zcoord_tmp, vector<double> &weight_tmp, vector<double> &xcoord, vector<double> &ycoord, vector<double> &zcoord, vector<double> &weight)
{

    int pts_per_elt = (degree+1); // 1D case by default
    int jmax = 1;
    int kmax = 1;
    int ydofmax = 1;
    int zdofmax = 1;

    if(element_number_2 > 0){ // 2D case
        jmax = element_number_2;
        ydofmax = degree +1;
        pts_per_elt = (degree+1)*(degree+1);
    }
    if(element_number_3 > 0){ // 3D case
        kmax = element_number_3;
        zdofmax = degree +1;
        pts_per_elt = (degree+1)*(degree+1)*(degree+1);
    }

    // definition of vertices for each DG element
    for (int k=0; k<kmax; k++){
        for (int j=0; j<jmax; j++){
            for (int i=0; i<element_number_1; i++){

                // inside each element
                for (int in3=0; in3<zdofmax;in3++){
                    for (int in2=0; in2<ydofmax;in2++){
                        for (int in1=0; in1<degree+1;in1++){

                            int index_dg = k*element_number_1*element_number_2*pts_per_elt + (i+element_number_1*j)*pts_per_elt + (degree+1)*(degree+1)*in3 + in1+(degree+1)*in2;

                            int index_cartesian = (element_number_1*degree+1)*(element_number_2*degree+1)*(degree*k+in3) + (element_number_1*degree+1)*(degree*j+in2) + degree*i + in1;

                            xcoord[index_dg] = (xcoord_tmp[index_cartesian]*cos(angle) - ycoord_tmp[index_cartesian]*sin(angle)) * scale;
                            ycoord[index_dg] = (xcoord_tmp[index_cartesian]*sin(angle) + ycoord_tmp[index_cartesian]*cos(angle)) * scale;
                            zcoord[index_dg] = zcoord_tmp[index_cartesian];
                            weight[index_dg] = weight_tmp[index_cartesian];

                        }
                    }
                }
            }
        }
    }
}

//////////////////////////////////////////////////////////////

void igMeshGenerator::applyBoundaryCondition(void)
{
    if(!this->mesh){ // only single patch construction

        int kmax = 1;
        int jmax = 1;
        int imax = element_number_1;
        int face_number = 2;
        if(element_number_2 > 0){ // 2D case
            jmax = element_number_2;
            face_number = 4;
        }
        if(element_number_3 > 0){ // 3D case
            jmax = element_number_2;
            kmax = element_number_3;
            face_number = 6;
        }

        int current_elt_index = 0;
        for (int iel3=0; iel3<kmax; iel3++){
            for (int iel2=0; iel2<jmax; iel2++){
                for (int iel1=0; iel1<imax; iel1++){

                    if(iel1 == 0){ // west boundary
                        face_type.at(current_elt_index*face_number+0) = condition_id_west;
                    }

                    if(iel1 == element_number_1-1){ // east boundary
                        face_type.at(current_elt_index*face_number+1) = condition_id_east;
                    }

                    if(iel2 == 0 && element_number_2>0){ // south boundary
                        face_type.at(current_elt_index*face_number+2) = condition_id_south;
                    }

                    if(iel2 == element_number_2-1 && element_number_2>0){  // north boundary
                        face_type.at(current_elt_index*face_number+3) = condition_id_north;
                    }

                    if(iel3 == 0 && element_number_3>0){ // bottom boundary
                        face_type.at(current_elt_index*face_number+4) = condition_id_bottom;
                    }

                    if(iel3 == element_number_3-1 && element_number_3>0){ // top boundary
                        face_type.at(current_elt_index*face_number+5) = condition_id_top;
                    }

                    current_elt_index++;
                }
            }
        }
    }

}

void igMeshGenerator::enforceSymmetry(double coordinate)
{
    //south boundary
    for (int i=0; i<element_number_1; i++){

        int index = i*(degree+1)*(degree+1);
        double x = xcoord[index];

        if(x < coordinate)
            face_type.at(i*4 +2) = id_slip_wall;
    }

    // north boundary
    for (int i=0; i<element_number_1; i++){

        int index = (i+element_number_1*(element_number_2-1))*(degree+1)*(degree+1) ;
        double x = xcoord[index];

        if(x < coordinate)
            face_type.at((i+element_number_1*(element_number_2-1))*4 +3) = id_slip_wall;
    }

}

void igMeshGenerator::enforceMembrane(void)
{
    // flag to generate boundary faces in the mesh
    this->enforce_membrane = true;

    // read properties from membrane file
    ifstream nurbs_file("nurbs.dat", ios::in);
    int degree_membrane;
    nurbs_file >> degree_membrane;
    int pts_membrane;
    nurbs_file >> pts_membrane;
    nurbs_file.close();

    this->membrane_elt_number = pts_membrane - degree_membrane;
    this->upstream_membrane_elt_number = (element_number_1 - membrane_elt_number)/2;
    this->below_membrane_elt_number = element_number_2/2;

    int dof_per_elt = (degree_membrane+1)*(degree_membrane+1);

    // bottom layer (face north)
    int index_elt_start = element_number_1*(below_membrane_elt_number-1) + upstream_membrane_elt_number;

    for(int iel=0; iel<membrane_elt_number; iel++){
        int index = index_elt_start + iel;
        face_type.at(index*4 + 3) = id_noslip_wall_fsi;
    }

    // top layer (face south)
    index_elt_start = element_number_1*below_membrane_elt_number + upstream_membrane_elt_number;

    for(int iel=0; iel<membrane_elt_number; iel++){
        int index = index_elt_start + iel;
        face_type.at(index*4 + 2) = id_noslip_wall_fsi;
    }

    // bottom clamp layer (face north)
    index_elt_start = element_number_1*(below_membrane_elt_number-1) + upstream_membrane_elt_number - clamp_number;

    for(int iel=0; iel<clamp_number; iel++){
        int index = index_elt_start + iel;
        face_type.at(index*4 + 3) = id_noslip_wall;
    }

    // geometry modification
    double y_damp = 0.05;

    for(int iel=0; iel<clamp_number; iel++){
        for(int jel=0; jel<element_number_2/2; jel++){
            int index = index_elt_start + iel - element_number_1*jel;
            for(int idof=0; idof<dof_per_elt; idof++){
                double y_init = ycoord.at(index*dof_per_elt+idof);
                double x_init = xcoord.at(index*dof_per_elt+idof);
                double damping = 0;
                if(y_init>-y_damp && x_init<0 && x_init>-0.048){
                    damping = 1+y_init/y_damp;
                }
                double delta = -x_init*0.005/0.036;
                if(x_init>-0.036){
                    ycoord.at(index*dof_per_elt+idof) -= delta * damping;
                } else {
                    ycoord.at(index*dof_per_elt+idof) -= 0.005 * damping;
                }
            }
        }
    }

    index_elt_start = element_number_1*below_membrane_elt_number - upstream_membrane_elt_number;

    for(int iel=0; iel<clamp_number; iel++){
        int index = index_elt_start + iel;
        face_type.at(index*4 + 3) = id_noslip_wall;
    }

    for(int iel=0; iel<clamp_number; iel++){
        for(int jel=0; jel<element_number_2/2; jel++){
            int index = index_elt_start + iel - element_number_1*jel;
            for(int idof=0; idof<dof_per_elt; idof++){
                double y_init = ycoord.at(index*dof_per_elt+idof);
                double x_init = xcoord.at(index*dof_per_elt+idof);
                double damping = 0;
                if(y_init>-y_damp && x_init<1.048 && x_init>1.){
                    damping = 1+y_init/y_damp;
                }
                double delta = (x_init-1)*0.005/0.036;
                if(x_init<1.036){
                    ycoord.at(index*dof_per_elt+idof) -= delta * damping;
                } else {
                    ycoord.at(index*dof_per_elt+idof) -= 0.005 * damping;
                }
            }
        }
    }

    // top clamp layer (face south)
    index_elt_start = element_number_1*below_membrane_elt_number + upstream_membrane_elt_number - clamp_number;

    for(int iel=0; iel<clamp_number; iel++){
        int index = index_elt_start + iel;
        face_type.at(index*4 + 2) = id_noslip_wall;

    }

    // geometry modification

    for(int iel=0; iel<clamp_number; iel++){
        for(int jel=0; jel<element_number_2/2; jel++){
            int index = index_elt_start + iel + element_number_1*jel;
            for(int idof=0; idof<dof_per_elt; idof++){
                double y_init = ycoord.at(index*dof_per_elt+idof);
                double x_init = xcoord.at(index*dof_per_elt+idof);
                double damping = 0;
                if(y_init<y_damp && x_init<0 && x_init>-0.048){
                    damping = 1-y_init/y_damp;
                }
                double delta = -x_init*0.005/0.036;
                if(x_init>-0.036){
                    ycoord.at(index*dof_per_elt+idof) += delta * damping;
                } else {
                    ycoord.at(index*dof_per_elt+idof) += 0.005 * damping;
                }
            }
        }
    }

    index_elt_start = element_number_1*(below_membrane_elt_number+1) - upstream_membrane_elt_number;

    for(int iel=0; iel<clamp_number; iel++){
        int index = index_elt_start + iel;
        face_type.at(index*4 + 2) = id_noslip_wall;

    }

    for(int iel=0; iel<clamp_number; iel++){
        for(int jel=0; jel<element_number_2/2; jel++){
            int index = index_elt_start + iel + element_number_1*jel;
            for(int idof=0; idof<dof_per_elt; idof++){
                double y_init = ycoord.at(index*dof_per_elt+idof);
                double x_init = xcoord.at(index*dof_per_elt+idof);
                double damping = 0;
                if(y_init<y_damp && x_init<1.048 && x_init>1.){
                    damping = 1-y_init/y_damp;
                }
                double delta = (x_init-1)*0.005/0.036;
                if(x_init<1.036){
                    ycoord.at(index*dof_per_elt+idof) += delta * damping;
                } else {
                    ycoord.at(index*dof_per_elt+idof) += 0.005 * damping;
                }
            }
        }
    }

    // add membrane thickness (bottom side)
    double membrane_thickness = 0.001;

    index_elt_start = element_number_1*(below_membrane_elt_number-1) + upstream_membrane_elt_number -clamp_number;

    for(int iel=0; iel<membrane_elt_number+2*clamp_number; iel++){
        int index = index_elt_start + iel +1;
        for(int idof=0; idof<degree+1; idof++){
            double x_init = xcoord.at(index*dof_per_elt-1-idof);
            if(x_init > -0.048 && x_init < 1.048){
                ycoord.at(index*dof_per_elt-1-idof) -= membrane_thickness;
            }
        }
    }

}

double igMeshGenerator::xRotate(double x_tmp, double y_tmp)
{
    return (x_tmp*cos(angle) - y_tmp*sin(angle));
}


double igMeshGenerator::yRotate(double x_tmp, double y_tmp)
{
    return (x_tmp*sin(angle) + y_tmp*cos(angle));
}

void igMeshGenerator::buildGeneratedMesh(void)
{
    int dimension = 1;
    int parametric_dimension = 1;
    int pts_per_elt = (degree+1);
    int pts_per_dir1 = 1;
    int pts_per_dir2 = 1;
    int imax = element_number_1;
    int jmax = 1;
    int kmax = 1;
    int face_number = 2;
    int face_degree = 0;
    int gauss_pt_face = 1;
    if(element_number_2 > 0){ // 2D cases
        dimension = 2;
        parametric_dimension = 2;
        pts_per_elt = (degree+1)*(degree+1);
        pts_per_dir1 = degree+1;
        pts_per_dir2 = 1;
        jmax = element_number_2;
        face_number = 4;
        face_degree = degree;
        gauss_pt_face = gauss_pt_number;
    }
    if(element_number_3 > 0){ // 3D cases
        dimension = 3;
        parametric_dimension = 3;
        pts_per_elt = (degree+1)*(degree+1)*(degree+1);
        pts_per_dir1 = degree+1;
        pts_per_dir2 = degree+1;
        kmax = element_number_3;
        face_number = 6;
        face_degree = degree;
        gauss_pt_face = gauss_pt_number;
    }

    int max_field_number = max(variable_number,derivative_number);
    max_field_number = max(max_field_number,dimension+1);

    vector<double> *coord = new vector<double>;
    coord->resize(dimension);

    int left_elt_index;
    int right_elt_index;

    //-----------------------------------------------------------


    this->mesh = new igMesh;

    this->coordinates = new vector<double>;
    this->coordinates->resize(imax*jmax*kmax*pts_per_elt*(dimension+1), 0.);

    mesh->setCommunicator(this->communicator);
    mesh->setDimension(dimension);
    mesh->setVariableNumber(variable_number);
    mesh->setDerivativeNumber(derivative_number);
    mesh->setExactSolutionId(this->exact_solution_id);
    mesh->setCoordinates(this->coordinates);

    //----------------- elements list --------------------------

    for(int iel3=0; iel3<kmax; iel3++){
        for(int iel2=0; iel2<jmax; iel2++){
            for(int iel1=0; iel1<imax; iel1++){

                int partition_id = 0;
                int subdomain_id = 0;

                igElement *new_elt = new igElement;

                new_elt->setCellId(mesh->elementNumber());
                new_elt->setPartition(partition_id);
                new_elt->setSubdomain(subdomain_id);
                new_elt->setPhysicalDimension(dimension);
                new_elt->setParametricDimension(parametric_dimension);
                new_elt->setDegree(degree);
                new_elt->setGaussPointNumberByDirection(gauss_pt_number);
                new_elt->setVariableNumber(variable_number);
                new_elt->setDerivativeNumber(derivative_number);
                new_elt->setCoordinates(this->coordinates);

                new_elt->initializeDegreesOfFreedom();

                mesh->addElement(new_elt);
                mesh->updateMeshProperties();
            }
        }
    }

    // global id setting
    int global_id_counter = 0;
    for (int ivar=0; ivar<max_field_number; ivar++){
        for (int iel=0; iel < mesh->elementNumber(); iel++){
            igElement *elt = mesh->element(iel);
            for (int ipt=0; ipt<elt->controlPointNumber(); ipt++){
                elt->setGlobalId(ipt, ivar, global_id_counter);
                global_id_counter++;
            }
        }
    }

    for(int iel=0; iel<mesh->elementNumber(); iel++){

        igElement *new_elt = mesh->element(iel);

        int pt_counter = 0;
        for (int ipt=0; ipt<pts_per_elt; ipt++){

            int index = new_elt->globalId(ipt, 0);

            if(dimension == 1){
                this->coordinates->at(new_elt->globalId(ipt, 0)) = xcoord[index];
                this->coordinates->at(new_elt->globalId(ipt, 1)) = weight[index];
            } else if(dimension == 2){
                this->coordinates->at(new_elt->globalId(ipt, 0)) = xcoord[index];
                this->coordinates->at(new_elt->globalId(ipt, 1)) = ycoord[index];
                this->coordinates->at(new_elt->globalId(ipt, 2)) = weight[index];
            } else {
                this->coordinates->at(new_elt->globalId(ipt, 0)) = xcoord[index];
                this->coordinates->at(new_elt->globalId(ipt, 1)) = ycoord[index];
                this->coordinates->at(new_elt->globalId(ipt, 2)) = zcoord[index];
                this->coordinates->at(new_elt->globalId(ipt, 3)) = weight[index];
            }

            pt_counter++;
        }

        new_elt->initializeGeometry();
    }

    //--------------------- faces list --------------------------
    for (int iel3=0; iel3 < kmax ; iel3++){
        for (int iel2=0; iel2 < jmax ; iel2++){
            for (int iel1=0; iel1 < imax ; iel1++){

                if(periodic && iel2 == 0){ // periodicity faces in 2D = south-north faces only

                    int partition_id_left = 0;
                    int partition_id_right = 0;

                    igFace *new_face = new igFace;

                    new_face->setCellId(mesh->faceNumber());
                    new_face->setPhysicalDimension(dimension);
                    new_face->setParametricDimension(dimension-1);
                    new_face->setDegree(face_degree);
                    new_face->setGaussPointNumberByDirection(gauss_pt_face);
                    new_face->setVariableNumber(variable_number);
                    new_face->setDerivativeNumber(derivative_number);
                    new_face->setType(0);
                    new_face->setLeftPartitionId(partition_id_left);
                    new_face->setRightPartitionId(partition_id_right);
                    new_face->setLeftInside();
                    new_face->setRightInside();
                    new_face->setCoordinates(this->coordinates);

                    new_face->initializeDegreesOfFreedom();

                    left_elt_index = iel1+element_number_1*iel2+element_number_1*element_number_2*iel3;
                    right_elt_index = iel1+element_number_1*(element_number_2-1)+element_number_1*element_number_2*iel3;

                    igElement *elt_left = mesh->element(left_elt_index);
                    igElement *elt_right = mesh->element(right_elt_index);

                    new_face->setLeftElementIndex(left_elt_index);
                    new_face->setOrientationLeft(0);
                    new_face->setSenseLeft(1);
                    for (int ivar=0; ivar<max_field_number; ivar++){
                        for (int idof3=0; idof3<pts_per_dir2; idof3++){
                            for (int idof1=0; idof1<pts_per_dir1; idof1++){
                                int index = idof1+idof3*pts_per_dir1*pts_per_dir2;
                                new_face->setLeftDofMap(idof1+idof3*pts_per_dir1,ivar,elt_left->globalId(index,ivar));
                            }
                        }
                    }

                    new_face->setRightElementIndex(right_elt_index);
                    new_face->setOrientationRight(2);
                    new_face->setSenseRight(1);
                    for (int ivar=0; ivar<max_field_number; ivar++){
                        for (int idof3=0; idof3<pts_per_dir2; idof3++){
                            for (int idof1=0; idof1<pts_per_dir1; idof1++){
                                int index = idof1+pts_per_dir1*(pts_per_dir1-1)+idof3*pts_per_dir1*pts_per_dir2;
                                new_face->setRightDofMap(idof1+idof3*pts_per_dir1,ivar,elt_right->globalId(index,ivar));
                            }
                        }
                    }


                    for (int idim=0; idim<dimension; idim++)
                        coord->at(idim) = elt_left->barycenter(idim);

                    new_face->initializeGeometry();
                    new_face->initializeNormal(coord);

                    mesh->addFace(new_face);
                    mesh->updateMeshProperties();

                } else if(iel2>0){  // face South in 2D / 3D

                    int partition_id_left = 0;
                    int partition_id_right = 0;

                    igFace *new_face = new igFace;

                    new_face->setCellId(mesh->faceNumber());
                    new_face->setPhysicalDimension(dimension);
                    new_face->setParametricDimension(dimension-1);
                    new_face->setDegree(face_degree);
                    new_face->setGaussPointNumberByDirection(gauss_pt_face);
                    new_face->setVariableNumber(variable_number);
                    new_face->setDerivativeNumber(derivative_number);
                    new_face->setType(0);
                    new_face->setLeftPartitionId(partition_id_left);
                    new_face->setRightPartitionId(partition_id_right);
                    new_face->setLeftInside();
                    new_face->setRightInside();
                    new_face->setCoordinates(this->coordinates);

                    new_face->initializeDegreesOfFreedom();

                    left_elt_index = iel1+element_number_1*iel2+element_number_1*element_number_2*iel3;
                    right_elt_index = iel1+element_number_1*(iel2-1)+element_number_1*element_number_2*iel3;

                    igElement *elt_left = mesh->element(left_elt_index);
                    igElement *elt_right = mesh->element(right_elt_index);

                    new_face->setLeftElementIndex(left_elt_index);
                    new_face->setOrientationLeft(0);
                    new_face->setSenseLeft(1);
                    for (int ivar=0; ivar<max_field_number; ivar++){
                        for (int idof3=0; idof3<pts_per_dir2; idof3++){
                            for (int idof1=0; idof1<pts_per_dir1; idof1++){
                                int index = idof1+idof3*pts_per_dir1*pts_per_dir2;
                                new_face->setLeftDofMap(idof1+idof3*pts_per_dir1,ivar,elt_left->globalId(index,ivar));
                            }
                        }
                    }

                    new_face->setRightElementIndex(right_elt_index);
                    new_face->setOrientationRight(2);
                    new_face->setSenseRight(1);
                    for (int ivar=0; ivar<max_field_number; ivar++){
                        for (int idof3=0; idof3<pts_per_dir2; idof3++){
                            for (int idof1=0; idof1<pts_per_dir1; idof1++){
                                int index = pts_per_dir1*(pts_per_dir1-1) + idof1+idof3*pts_per_dir1*pts_per_dir2;
                                new_face->setRightDofMap(idof1+idof3*pts_per_dir1,ivar,elt_right->globalId(index,ivar));
                            }
                        }
                    }

                    for (int idim=0; idim<dimension; idim++)
                        coord->at(idim) = elt_left->barycenter(idim);

                    new_face->initializeGeometry();
                    new_face->initializeNormal(coord);

                    // not account for membrane face
                    if(face_type.at(left_elt_index*face_number+2) == id_interior){
                        mesh->addFace(new_face);
                        mesh->updateMeshProperties();
                    }

                }


                if(iel1 < element_number_1-1){ // face East in 1D, 2D, 3D

                    int partition_id_left = 0;
                    int partition_id_right = 0;

                    igFace *new_face = new igFace;

                    new_face->setCellId(mesh->faceNumber());
                    new_face->setPhysicalDimension(dimension);
                    new_face->setParametricDimension(dimension-1);
                    new_face->setDegree(face_degree);
                    new_face->setGaussPointNumberByDirection(gauss_pt_face);
                    new_face->setVariableNumber(variable_number);
                    new_face->setDerivativeNumber(derivative_number);
                    new_face->setType(0);
                    new_face->setLeftPartitionId(partition_id_left);
                    new_face->setRightPartitionId(partition_id_right);
                    new_face->setLeftInside();
                    new_face->setRightInside();
                    new_face->setCoordinates(this->coordinates);

                    new_face->initializeDegreesOfFreedom();

                    left_elt_index = iel1+element_number_1*iel2+element_number_1*element_number_2*iel3;
                    right_elt_index = iel1+1+element_number_1*iel2+element_number_1*element_number_2*iel3;

                    igElement *elt_left = mesh->element(left_elt_index);
                    igElement *elt_right = mesh->element(right_elt_index);

                    new_face->setLeftElementIndex(left_elt_index);
                    new_face->setOrientationLeft(1);
                    new_face->setSenseLeft(1);
                    for (int ivar=0; ivar<max_field_number; ivar++){
                        for (int idof3=0; idof3<pts_per_dir2; idof3++){
                            for (int idof2=0; idof2<pts_per_dir1; idof2++){
                                int index = degree+idof2*pts_per_dir1+idof3*pts_per_dir1*pts_per_dir2;
                                new_face->setLeftDofMap(idof2+idof3*pts_per_dir1,ivar,elt_left->globalId(index,ivar));
                            }
                        }
                    }

                    new_face->setRightElementIndex(right_elt_index);
                    new_face->setOrientationRight(3);
                    new_face->setSenseRight(1);
                    for (int ivar=0; ivar<max_field_number; ivar++){
                        for (int idof3=0; idof3<pts_per_dir2; idof3++){
                            for (int idof2=0; idof2<pts_per_dir1; idof2++){
                                int index = idof2*pts_per_dir1+idof3*pts_per_dir1*pts_per_dir2;
                                new_face->setRightDofMap(idof2+idof3*pts_per_dir1,ivar,elt_right->globalId(index,ivar));
                            }
                        }
                    }

                    for (int idim=0; idim<dimension; idim++)
                        coord->at(idim) = elt_left->barycenter(idim);

                    new_face->initializeGeometry();
                    new_face->initializeNormal(coord);

                    mesh->addFace(new_face);
                    mesh->updateMeshProperties();
                }

                if(iel3 < element_number_3-1){ // face top in 3D

                    int partition_id_left = 0;
                    int partition_id_right = 0;

                    igFace *new_face = new igFace;

                    new_face->setCellId(mesh->faceNumber());
                    new_face->setPhysicalDimension(dimension);
                    new_face->setParametricDimension(dimension-1);
                    new_face->setDegree(face_degree);
                    new_face->setGaussPointNumberByDirection(gauss_pt_face);
                    new_face->setVariableNumber(variable_number);
                    new_face->setDerivativeNumber(derivative_number);
                    new_face->setType(0);
                    new_face->setLeftPartitionId(partition_id_left);
                    new_face->setRightPartitionId(partition_id_right);
                    new_face->setLeftInside();
                    new_face->setRightInside();
                    new_face->setCoordinates(this->coordinates);

                    new_face->initializeDegreesOfFreedom();

                    left_elt_index = iel1+element_number_1*iel2+element_number_1*element_number_2*iel3;
                    right_elt_index = iel1+element_number_1*iel2+element_number_1*element_number_2*(iel3+1);

                    igElement *elt_left = mesh->element(left_elt_index);
                    igElement *elt_right = mesh->element(right_elt_index);

                    new_face->setLeftElementIndex(left_elt_index);
                    new_face->setOrientationLeft(1);
                    new_face->setSenseLeft(1);
                    for (int ivar=0; ivar<max_field_number; ivar++){
                        for (int idof2=0; idof2<pts_per_dir2; idof2++){
                            for (int idof1=0; idof1<pts_per_dir1; idof1++){
                                int index =  pts_per_elt-pts_per_dir1*pts_per_dir2 + idof1+idof2*pts_per_dir1;
                                new_face->setLeftDofMap(idof1+idof2*pts_per_dir1,ivar,elt_left->globalId(index,ivar));
                            }
                        }
                    }

                    new_face->setRightElementIndex(right_elt_index);
                    new_face->setOrientationRight(3);
                    new_face->setSenseRight(1);
                    for (int ivar=0; ivar<max_field_number; ivar++){
                        for (int idof2=0; idof2<pts_per_dir2; idof2++){
                            for (int idof1=0; idof1<pts_per_dir1; idof1++){
                                int index = idof1+idof2*pts_per_dir1;
                                new_face->setRightDofMap(idof1+idof2*pts_per_dir1,ivar,elt_right->globalId(index,ivar));
                            }
                        }
                    }

                    for (int idim=0; idim<dimension; idim++)
                        coord->at(idim) = elt_left->barycenter(idim);

                    new_face->initializeGeometry();
                    new_face->initializeNormal(coord);

                    mesh->addFace(new_face);
                    mesh->updateMeshProperties();
                }


            }
        }
    }


//------------------------- boundary list -----------------------------

    if(!periodic && dimension > 1){

        for (int iel3=0; iel3 < kmax ; iel3++){ //---------------- boundary South in 2D, 3D ----------------
            for (int iel1=0; iel1 < imax ; iel1++){

                int partition_id = 0;

                igFace *new_face =  new igFace;

                left_elt_index = iel1+element_number_1*element_number_2*iel3;

                new_face->setCellId(mesh->boundaryFaceNumber());
                new_face->setPhysicalDimension(dimension);
                new_face->setParametricDimension(dimension-1);
                new_face->setDegree(face_degree);
                new_face->setGaussPointNumberByDirection(gauss_pt_face);
                new_face->setVariableNumber(variable_number);
                new_face->setDerivativeNumber(derivative_number);
                new_face->setType(face_type.at(left_elt_index*face_number+2));
                new_face->setLeftPartitionId(partition_id);
                new_face->setLeftInside();
                new_face->setCoordinates(this->coordinates);

                new_face->initializeDegreesOfFreedom();

                igElement *elt_left = mesh->element(left_elt_index);

                new_face->setLeftElementIndex(left_elt_index);
                new_face->setOrientationLeft(0);
                new_face->setSenseLeft(1);
                for (int ivar=0; ivar<max_field_number; ivar++){
                    for (int idof3=0; idof3<pts_per_dir2; idof3++){
                        for (int idof1=0; idof1<pts_per_dir1; idof1++){
                            int index = idof1+idof3*pts_per_dir1*pts_per_dir2;
                            new_face->setLeftDofMap(idof1+idof3*pts_per_dir1,ivar,elt_left->globalId(index,ivar));
                        }
                    }
                }

                for (int idim=0; idim<dimension; idim++)
                    coord->at(idim) = elt_left->barycenter(idim);

                new_face->initializeGeometry();
                new_face->initializeNormal(coord);

                mesh->addBoundaryFace(new_face);
                mesh->updateMeshProperties();
            }
        }
    }

    for (int iel3=0; iel3 < kmax ; iel3++){ //-------------------- boundary East in 1D, 2D, 3D --------------------
        for (int iel2=0; iel2 < jmax ; iel2++){

            int partition_id = 0;

            igFace *new_face = new igFace;

            left_elt_index = element_number_1-1+element_number_1*iel2+element_number_1*element_number_2*iel3;

            new_face->setCellId(mesh->boundaryFaceNumber());
            new_face->setPhysicalDimension(dimension);
            new_face->setParametricDimension(dimension-1);
            new_face->setDegree(face_degree);
            new_face->setGaussPointNumberByDirection(gauss_pt_face);
            new_face->setVariableNumber(variable_number);
            new_face->setDerivativeNumber(derivative_number);
            new_face->setType(face_type[left_elt_index*face_number+1]);
            new_face->setLeftPartitionId(partition_id);
            new_face->setLeftInside();
            new_face->setCoordinates(this->coordinates);

            new_face->initializeDegreesOfFreedom();

            igElement *elt_left = mesh->element(left_elt_index);

            new_face->setLeftElementIndex(left_elt_index);
            new_face->setOrientationLeft(1);
            new_face->setSenseLeft(1);
            for (int ivar=0; ivar<max_field_number; ivar++){
                for (int idof3=0; idof3<pts_per_dir2; idof3++){
                    for (int idof2=0; idof2<pts_per_dir1; idof2++){
                        int index = degree+idof2*pts_per_dir1+idof3*pts_per_dir1*pts_per_dir2;
                        new_face->setLeftDofMap(idof2+idof3*pts_per_dir1,ivar,elt_left->globalId(index,ivar));
                    }
                }
            }

            for (int idim=0; idim<dimension; idim++)
                coord->at(idim) = elt_left->barycenter(idim);

            new_face->initializeGeometry();
            new_face->initializeNormal(coord);

            mesh->addBoundaryFace(new_face);
            mesh->updateMeshProperties();
        }
    }

    if(!periodic && dimension > 1){

        for (int iel3=0; iel3 < kmax ; iel3++){ //-------------------- boundary North in 2D, 3D --------------------
            for (int iel1=0; iel1 < imax ; iel1++){

                int partition_id = 0;

                igFace *new_face = new igFace;

                left_elt_index = iel1+element_number_1*(element_number_2-1)+element_number_1*element_number_2*iel3;

                new_face->setCellId(mesh->boundaryFaceNumber());
                new_face->setPhysicalDimension(dimension);
                new_face->setParametricDimension(dimension-1);
                new_face->setDegree(face_degree);
                new_face->setGaussPointNumberByDirection(gauss_pt_face);
                new_face->setVariableNumber(variable_number);
                new_face->setDerivativeNumber(derivative_number);
                new_face->setType(face_type[left_elt_index*face_number+3]);
                new_face->setLeftPartitionId(partition_id);
                new_face->setLeftInside();
                new_face->setCoordinates(this->coordinates);

                new_face->initializeDegreesOfFreedom();

                igElement *elt_left = mesh->element(left_elt_index);

                new_face->setLeftElementIndex(left_elt_index);
                new_face->setOrientationLeft(2);
                new_face->setSenseLeft(1);
                for (int ivar=0; ivar<max_field_number; ivar++){
                    for (int idof3=0; idof3<pts_per_dir2; idof3++){
                        for (int idof1=0; idof1<pts_per_dir1; idof1++){
                            int index = idof1+pts_per_dir1*(pts_per_dir1-1)+idof3*pts_per_dir1*pts_per_dir2;
                            new_face->setLeftDofMap(idof1+idof3*pts_per_dir1,ivar,elt_left->globalId(index,ivar));
                        }
                    }
                }

                for (int idim=0; idim<dimension; idim++)
                    coord->at(idim) = elt_left->barycenter(idim);

                new_face->initializeGeometry();
                new_face->initializeNormal(coord);

                mesh->addBoundaryFace(new_face);
                mesh->updateMeshProperties();
            }
        }
    }

    for (int iel3=0; iel3 < kmax ; iel3++){ //-------------------- boundary West in 1D, 2D, 3D --------------------
        for (int iel2=0; iel2 < jmax ; iel2++){

            int partition_id = 0;

            igFace *new_face = new igFace;

            left_elt_index = element_number_1*iel2+element_number_1*element_number_2*iel3;

            new_face->setCellId(mesh->boundaryFaceNumber());
            new_face->setPhysicalDimension(dimension);
            new_face->setParametricDimension(dimension-1);
            new_face->setDegree(face_degree);
            new_face->setGaussPointNumberByDirection(gauss_pt_face);
            new_face->setVariableNumber(variable_number);
            new_face->setDerivativeNumber(derivative_number);
            new_face->setType(face_type[left_elt_index*face_number+0]);
            new_face->setLeftPartitionId(partition_id);
            new_face->setLeftInside();
            new_face->setCoordinates(this->coordinates);

            new_face->initializeDegreesOfFreedom();

            igElement *elt_left = mesh->element(left_elt_index);

            new_face->setLeftElementIndex(left_elt_index);
            new_face->setOrientationLeft(3);
            new_face->setSenseLeft(1);
            for (int ivar=0; ivar<max_field_number; ivar++){
                for (int idof3=0; idof3<pts_per_dir2; idof3++){
                    for (int idof2=0; idof2<pts_per_dir1; idof2++){
                        int index = idof2*pts_per_dir1+idof3*pts_per_dir1*pts_per_dir2;
                        new_face->setLeftDofMap(idof2+idof3*pts_per_dir1,ivar,elt_left->globalId(index,ivar));
                    }
                }
            }

            for (int idim=0; idim<dimension; idim++)
                coord->at(idim) = elt_left->barycenter(idim);

            new_face->initializeGeometry();
            new_face->initializeNormal(coord);

            mesh->addBoundaryFace(new_face);
            mesh->updateMeshProperties();
        }
    }

    if(dimension > 2){

        for (int iel2=0; iel2 < jmax ; iel2++){ //---------------- boundary top 3D ----------------
            for (int iel1=0; iel1 < imax ; iel1++){

                int partition_id = 0;

                igFace *new_face =  new igFace;

                left_elt_index = element_number_1*element_number_2*(element_number_3-1)+iel1+element_number_1*iel2;

                new_face->setCellId(mesh->boundaryFaceNumber());
                new_face->setPhysicalDimension(dimension);
                new_face->setParametricDimension(dimension-1);
                new_face->setDegree(face_degree);
                new_face->setGaussPointNumberByDirection(gauss_pt_face);
                new_face->setVariableNumber(variable_number);
                new_face->setDerivativeNumber(derivative_number);
                new_face->setType(face_type.at(left_elt_index*face_number+5));
                new_face->setLeftPartitionId(partition_id);
                new_face->setLeftInside();
                new_face->setCoordinates(this->coordinates);

                new_face->initializeDegreesOfFreedom();

                igElement *elt_left = mesh->element(left_elt_index);

                new_face->setLeftElementIndex(left_elt_index);
                new_face->setOrientationLeft(0);
                new_face->setSenseLeft(1);
                for (int ivar=0; ivar<max_field_number; ivar++){
                    for (int idof2=0; idof2<pts_per_dir2; idof2++){
                        for (int idof1=0; idof1<pts_per_dir1; idof1++){
                            int index = pts_per_dir1*pts_per_dir1*(pts_per_dir2-1)+idof1+idof2*pts_per_dir1;
                            new_face->setLeftDofMap(idof1+idof2*pts_per_dir1,ivar,elt_left->globalId(index,ivar));
                        }
                    }
                }

                for (int idim=0; idim<dimension; idim++)
                    coord->at(idim) = elt_left->barycenter(idim);

                new_face->initializeGeometry();
                new_face->initializeNormal(coord);

                mesh->addBoundaryFace(new_face);
                mesh->updateMeshProperties();
            }
        }

        for (int iel2=0; iel2 < jmax ; iel2++){ //---------------- boundary bottom 3D ----------------
            for (int iel1=0; iel1 < imax ; iel1++){

                int partition_id = 0;

                igFace *new_face =  new igFace;

                left_elt_index = iel1+element_number_1*iel2;

                new_face->setCellId(mesh->boundaryFaceNumber());
                new_face->setPhysicalDimension(dimension);
                new_face->setParametricDimension(dimension-1);
                new_face->setDegree(face_degree);
                new_face->setGaussPointNumberByDirection(gauss_pt_face);
                new_face->setVariableNumber(variable_number);
                new_face->setDerivativeNumber(derivative_number);
                new_face->setType(face_type.at(left_elt_index*face_number+4));
                new_face->setLeftPartitionId(partition_id);
                new_face->setLeftInside();
                new_face->setCoordinates(this->coordinates);

                new_face->initializeDegreesOfFreedom();

                igElement *elt_left = mesh->element(left_elt_index);

                new_face->setLeftElementIndex(left_elt_index);
                new_face->setOrientationLeft(0);
                new_face->setSenseLeft(1);
                for (int ivar=0; ivar<max_field_number; ivar++){
                    for (int idof2=0; idof2<pts_per_dir2; idof2++){
                        for (int idof1=0; idof1<pts_per_dir1; idof1++){
                            int index = idof1+idof2*pts_per_dir1;
                            new_face->setLeftDofMap(idof1+idof2*pts_per_dir1,ivar,elt_left->globalId(index,ivar));
                        }
                    }
                }

                for (int idim=0; idim<dimension; idim++)
                    coord->at(idim) = elt_left->barycenter(idim);

                new_face->initializeGeometry();
                new_face->initializeNormal(coord);

                mesh->addBoundaryFace(new_face);
                mesh->updateMeshProperties();
            }
        }
    }

    //------------------ add interior membrane faces as boundaries ---------------

    if(enforce_membrane){

        // bottom layer (face north)
        int index_elt_start = element_number_1*(below_membrane_elt_number-1) + upstream_membrane_elt_number;

        for(int iel=0; iel<membrane_elt_number; iel++){

            int partition_id = 0;

            igFace *new_face = new igFace;

            int left_elt_index = index_elt_start + iel;

            new_face->setCellId(mesh->boundaryFaceNumber());
            new_face->setPhysicalDimension(dimension);
            new_face->setParametricDimension(dimension-1);
            new_face->setDegree(face_degree);
            new_face->setGaussPointNumberByDirection(gauss_pt_face);
            new_face->setVariableNumber(variable_number);
            new_face->setDerivativeNumber(derivative_number);
            new_face->setType(face_type[left_elt_index*face_number+3]);
            new_face->setLeftPartitionId(partition_id);
            new_face->setLeftInside();
            new_face->setCoordinates(this->coordinates);

            new_face->initializeDegreesOfFreedom();

            igElement *elt_left = mesh->element(left_elt_index);

            new_face->setLeftElementIndex(left_elt_index);
            new_face->setOrientationLeft(2);
            new_face->setSenseLeft(1);
            for (int ivar=0; ivar<max_field_number; ivar++){
                for (int idof3=0; idof3<pts_per_dir2; idof3++){
                    for (int idof1=0; idof1<pts_per_dir1; idof1++){
                        int index = idof1+pts_per_dir1*(pts_per_dir1-1)+idof3*pts_per_dir1*pts_per_dir2;
                        new_face->setLeftDofMap(idof1+idof3*pts_per_dir1,ivar,elt_left->globalId(index,ivar));
                    }
                }
            }

            for (int idim=0; idim<dimension; idim++)
                coord->at(idim) = elt_left->barycenter(idim);

            new_face->initializeGeometry();
            new_face->initializeNormal(coord);

            mesh->addBoundaryFace(new_face);
            mesh->updateMeshProperties();
        }

        // top layer (face south)
        index_elt_start = element_number_1*below_membrane_elt_number + upstream_membrane_elt_number;

        for(int iel=0; iel<membrane_elt_number; iel++){

            int partition_id = 0;

            igFace *new_face =  new igFace;

            left_elt_index = index_elt_start + iel;

            new_face->setCellId(mesh->boundaryFaceNumber());
            new_face->setPhysicalDimension(dimension);
            new_face->setParametricDimension(dimension-1);
            new_face->setDegree(face_degree);
            new_face->setGaussPointNumberByDirection(gauss_pt_face);
            new_face->setVariableNumber(variable_number);
            new_face->setDerivativeNumber(derivative_number);
            new_face->setType(face_type.at(left_elt_index*face_number+2));
            new_face->setLeftPartitionId(partition_id);
            new_face->setLeftInside();
            new_face->setCoordinates(this->coordinates);

            new_face->initializeDegreesOfFreedom();

            igElement *elt_left = mesh->element(left_elt_index);

            new_face->setLeftElementIndex(left_elt_index);
            new_face->setOrientationLeft(0);
            new_face->setSenseLeft(1);
            for (int ivar=0; ivar<max_field_number; ivar++){
                for (int idof3=0; idof3<pts_per_dir2; idof3++){
                    for (int idof1=0; idof1<pts_per_dir1; idof1++){
                        int index = idof1+idof3*pts_per_dir1*pts_per_dir2;
                        new_face->setLeftDofMap(idof1+idof3*pts_per_dir1,ivar,elt_left->globalId(index,ivar));
                    }
                }
            }

            for (int idim=0; idim<dimension; idim++)
                coord->at(idim) = elt_left->barycenter(idim);

            new_face->initializeGeometry();
            new_face->initializeNormal(coord);

            mesh->addBoundaryFace(new_face);
            mesh->updateMeshProperties();

        }

        // bottom clamp layer (face north)
        index_elt_start = element_number_1*(below_membrane_elt_number-1) + upstream_membrane_elt_number -clamp_number;

        for(int iel=0; iel<clamp_number; iel++){

            int partition_id = 0;

            igFace *new_face = new igFace;

            int left_elt_index = index_elt_start + iel;

            new_face->setCellId(mesh->boundaryFaceNumber());
            new_face->setPhysicalDimension(dimension);
            new_face->setParametricDimension(dimension-1);
            new_face->setDegree(face_degree);
            new_face->setGaussPointNumberByDirection(gauss_pt_face);
            new_face->setVariableNumber(variable_number);
            new_face->setDerivativeNumber(derivative_number);
            new_face->setType(face_type[left_elt_index*face_number+3]);
            new_face->setLeftPartitionId(partition_id);
            new_face->setLeftInside();
            new_face->setCoordinates(this->coordinates);

            new_face->initializeDegreesOfFreedom();

            igElement *elt_left = mesh->element(left_elt_index);

            new_face->setLeftElementIndex(left_elt_index);
            new_face->setOrientationLeft(2);
            new_face->setSenseLeft(1);
            for (int ivar=0; ivar<max_field_number; ivar++){
                for (int idof3=0; idof3<pts_per_dir2; idof3++){
                    for (int idof1=0; idof1<pts_per_dir1; idof1++){
                        int index = idof1+pts_per_dir1*(pts_per_dir1-1)+idof3*pts_per_dir1*pts_per_dir2;
                        new_face->setLeftDofMap(idof1+idof3*pts_per_dir1,ivar,elt_left->globalId(index,ivar));
                    }
                }
            }

            for (int idim=0; idim<dimension; idim++)
                coord->at(idim) = elt_left->barycenter(idim);

            new_face->initializeGeometry();
            new_face->initializeNormal(coord);

            mesh->addBoundaryFace(new_face);
            mesh->updateMeshProperties();
        }

        index_elt_start = element_number_1*below_membrane_elt_number - upstream_membrane_elt_number;

        for(int iel=0; iel<clamp_number; iel++){

            int partition_id = 0;

            igFace *new_face = new igFace;

            int left_elt_index = index_elt_start + iel;

            new_face->setCellId(mesh->boundaryFaceNumber());
            new_face->setPhysicalDimension(dimension);
            new_face->setParametricDimension(dimension-1);
            new_face->setDegree(face_degree);
            new_face->setGaussPointNumberByDirection(gauss_pt_face);
            new_face->setVariableNumber(variable_number);
            new_face->setDerivativeNumber(derivative_number);
            new_face->setType(face_type[left_elt_index*face_number+3]);
            new_face->setLeftPartitionId(partition_id);
            new_face->setLeftInside();
            new_face->setCoordinates(this->coordinates);

            new_face->initializeDegreesOfFreedom();

            igElement *elt_left = mesh->element(left_elt_index);

            new_face->setLeftElementIndex(left_elt_index);
            new_face->setOrientationLeft(2);
            new_face->setSenseLeft(1);
            for (int ivar=0; ivar<max_field_number; ivar++){
                for (int idof3=0; idof3<pts_per_dir2; idof3++){
                    for (int idof1=0; idof1<pts_per_dir1; idof1++){
                        int index = idof1+pts_per_dir1*(pts_per_dir1-1)+idof3*pts_per_dir1*pts_per_dir2;
                        new_face->setLeftDofMap(idof1+idof3*pts_per_dir1,ivar,elt_left->globalId(index,ivar));
                    }
                }
            }

            for (int idim=0; idim<dimension; idim++)
                coord->at(idim) = elt_left->barycenter(idim);

            new_face->initializeGeometry();
            new_face->initializeNormal(coord);

            mesh->addBoundaryFace(new_face);
            mesh->updateMeshProperties();
        }

        // top clamp layer (face south)
        index_elt_start = element_number_1*below_membrane_elt_number + upstream_membrane_elt_number -clamp_number;

        for(int iel=0; iel<clamp_number; iel++){

            int partition_id = 0;

            igFace *new_face =  new igFace;

            left_elt_index = index_elt_start + iel;

            new_face->setCellId(mesh->boundaryFaceNumber());
            new_face->setPhysicalDimension(dimension);
            new_face->setParametricDimension(dimension-1);
            new_face->setDegree(face_degree);
            new_face->setGaussPointNumberByDirection(gauss_pt_face);
            new_face->setVariableNumber(variable_number);
            new_face->setDerivativeNumber(derivative_number);
            new_face->setType(face_type.at(left_elt_index*face_number+2));
            new_face->setLeftPartitionId(partition_id);
            new_face->setLeftInside();
            new_face->setCoordinates(this->coordinates);

            new_face->initializeDegreesOfFreedom();

            igElement *elt_left = mesh->element(left_elt_index);

            new_face->setLeftElementIndex(left_elt_index);
            new_face->setOrientationLeft(0);
            new_face->setSenseLeft(1);
            for (int ivar=0; ivar<max_field_number; ivar++){
                for (int idof3=0; idof3<pts_per_dir2; idof3++){
                    for (int idof1=0; idof1<pts_per_dir1; idof1++){
                        int index = idof1+idof3*pts_per_dir1*pts_per_dir2;
                        new_face->setLeftDofMap(idof1+idof3*pts_per_dir1,ivar,elt_left->globalId(index,ivar));
                    }
                }
            }

            for (int idim=0; idim<dimension; idim++)
                coord->at(idim) = elt_left->barycenter(idim);

            new_face->initializeGeometry();
            new_face->initializeNormal(coord);

            mesh->addBoundaryFace(new_face);
            mesh->updateMeshProperties();

        }

        index_elt_start = element_number_1*(below_membrane_elt_number+1) - upstream_membrane_elt_number;

        for(int iel=0; iel<clamp_number; iel++){

            int partition_id = 0;

            igFace *new_face =  new igFace;

            left_elt_index = index_elt_start + iel;

            new_face->setCellId(mesh->boundaryFaceNumber());
            new_face->setPhysicalDimension(dimension);
            new_face->setParametricDimension(dimension-1);
            new_face->setDegree(face_degree);
            new_face->setGaussPointNumberByDirection(gauss_pt_face);
            new_face->setVariableNumber(variable_number);
            new_face->setDerivativeNumber(derivative_number);
            new_face->setType(face_type.at(left_elt_index*face_number+2));
            new_face->setLeftPartitionId(partition_id);
            new_face->setLeftInside();
            new_face->setCoordinates(this->coordinates);

            new_face->initializeDegreesOfFreedom();

            igElement *elt_left = mesh->element(left_elt_index);

            new_face->setLeftElementIndex(left_elt_index);
            new_face->setOrientationLeft(0);
            new_face->setSenseLeft(1);
            for (int ivar=0; ivar<max_field_number; ivar++){
                for (int idof3=0; idof3<pts_per_dir2; idof3++){
                    for (int idof1=0; idof1<pts_per_dir1; idof1++){
                        int index = idof1+idof3*pts_per_dir1*pts_per_dir2;
                        new_face->setLeftDofMap(idof1+idof3*pts_per_dir1,ivar,elt_left->globalId(index,ivar));
                    }
                }
            }

            for (int idim=0; idim<dimension; idim++)
                coord->at(idim) = elt_left->barycenter(idim);

            new_face->initializeGeometry();
            new_face->initializeNormal(coord);

            mesh->addBoundaryFace(new_face);
            mesh->updateMeshProperties();

        }


    }

    //----------------- finalize mesh ---------------------

    mesh->updateGlobalMeshProperties();
    mesh->initializeGlobalToLocalMap();

    delete coord;
}
