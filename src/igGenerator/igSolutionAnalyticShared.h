/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

////////////////////////////////////////////////////

inline double conservativeVariable(double rho, double u, double v, double p, int var_id)
{
    double value;

    switch(var_id){
    case 0:
        value = rho;
        break;
    case 1:
        value = rho*u;
        break;
    case 2:
        value = rho*v;
        break;
    case 3:
        value = p/(1.4-1.) + 0.5*rho*(u*u+v*v);
        break;
    default:
        value = 0.;
    }
    return value;
}

inline double conservativeVariable(double rho, double u, double v, double w, double p, int var_id)
{
    double value;

    switch(var_id){
    case 0:
        value = rho;
        break;
    case 1:
        value = rho*u;
        break;
    case 2:
        value = rho*v;
        break;
    case 3:
            value = rho*w;
            break;
    case 4:
        value = p/(1.4-1.) + 0.5*rho*(u*u+v*v);
        break;
    default:
        value = 0.;
    }
    return value;
}

////////////////////////////////////////////////////
