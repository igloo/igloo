/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include <iostream>

#include "igMeshGeneratorCartesian.h"
#include "igMeshGeneratorCompound.h"
#include "igMeshGeneratorCoons.h"
#include "igMeshGeneratorFitting.h"
#include "igMeshGeneratorSplitting.h"

#include "igSolutionGeneratorFittingCurve.h"
#include "igSolutionGeneratorConstant.h"
#include "igSolutionGeneratorFittingSurface.h"

#include "igDistributionAnalytic.h"
#include "igShapeAnalytic.h"
#include "igSolutionAnalyticAcoustic.h"
#include "igSolutionAnalyticAdvection.h"
#include "igSolutionAnalyticBurgers.h"
#include "igSolutionAnalyticEuler.h"
#include "igSolutionAnalyticNavierStokes.h"
#include "igSolutionAnalyticViscousBurgers.h"

#include <igCore/igBoundaryIds.h>

#include "igCaseGenerator.h"

/////////////////////////////////////////////////////////////////////////////

igCaseGenerator::igCaseGenerator(void)
{
    // parameters by default

    x_fun = x_id;
    y_fun = y_id;
    z_fun = z_id;
    sol_fun = solution_euler_uniform;
    distrib_xi_fun = distrib_xi_id_fun;
    distrib_eta_fun = distrib_eta_id_fun;
    distrib_zeta_fun = distrib_zeta_id_fun;

    variable_number = 4;
    derivative_number = 0;

    xi_min = 0.;
    xi_max = 1.;
    eta_min = 0.;
    eta_max = 1.;
    zeta_min = 0.;
    zeta_max = 1.;

    type_south = 0;
    type_east = 0;
    type_north = 0;
    type_west = 0;
    type_top = 0;
    type_bottom = 0;
    exact_solution_id = 0;

    enforce_membrane = false;
    distribution_file = false;
    enforce_sym = false;
    sym_coordinate = 0.;

    scale_factor = 1.;

    periodic = false;

    input_size.resize(2);
    input_size[0] = 1;
    input_size[1] = 1;
}

igCaseGenerator::~igCaseGenerator()
{
    delete mesh_gen;
    delete sol_gen;
}

/////////////////////////////////////////////////////////////////////////////


void igCaseGenerator::generateCase(void)
{

    //--------------- case selection ----------------

    if(case_name == "burgers_bump"){

        mesh_gen = new igMeshGeneratorCartesian;
        sol_gen = new igSolutionGeneratorFittingCurve;

        sol_fun = solution_burgers_bump;

        variable_number = 1;

        xi_min = -1.;
        xi_max = 1.;
        eta_min = -1.;
        eta_max = 1.;

    } else if(case_name == "viscous_burgers"){

        mesh_gen = new igMeshGeneratorCartesian;
        sol_gen = new igSolutionGeneratorFittingCurve;

        sol_fun = solution_viscous_burgers;

        variable_number = 1;
        derivative_number = 2*variable_number;

        xi_min = -1.;
        xi_max = 1.;
        eta_min = -1.;
        eta_max = 1.;

    } else if(case_name == "advection_circular"){

        mesh_gen = new igMeshGeneratorCartesian;
        sol_gen = new igSolutionGeneratorFittingSurface;

        sol_fun = solution_advection_circular;

        variable_number = 1;

        xi_min = -4.;
        xi_max = 4.;
        eta_min = -4.;
        eta_max = 4.;

        type_south = id_advection;
        type_east = id_advection;
        type_north = id_advection;
        type_west = id_advection;

    } else if(case_name == "acoustic_annular"){

        mesh_gen = new igMeshGeneratorFitting;
        sol_gen = new igSolutionGeneratorFittingSurface;

        x_fun = x_circle;
        y_fun = y_circle;
        sol_fun = solution_acoustic_annular;

        variable_number = 3;

        type_south = id_acoustic_sym;
        type_east = id_acoustic_wall;
        type_north = id_acoustic_sym;
        type_west = id_acoustic_wall;

    } else if(case_name == "acoustic_bang"){

        mesh_gen = new igMeshGeneratorFitting;
        sol_gen = new igSolutionGeneratorFittingSurface;

        x_fun = x_cylinder_bump;
        y_fun = y_cylinder_bump;
        sol_fun = solution_acoustic_bump;

        variable_number = 3;

        xi_min = 1.;
        xi_max = 4.;
        eta_min = 0.;
        eta_max = 1.;

        type_south = id_interior;
        type_east = id_acoustic_wall;
        type_north = id_periodic;
        type_west = id_acoustic_wall;

        periodic = true;

    } else if(case_name == "euler_bump_exp"){

        mesh_gen = new igMeshGeneratorFitting;
        sol_gen = new igSolutionGeneratorConstant;

        y_fun = y_bump_exp;

        xi_min = -1.5;
        xi_max = 1.5;
        eta_min = 0.;
        eta_max = 0.8;

        type_south = id_slip_wall;
        type_east = id_io_riemann;
        type_north = id_slip_wall;
        type_west = id_io_riemann;

    } else if(case_name == "euler_bump_exp_3D"){

        mesh_gen = new igMeshGeneratorFitting;
        sol_gen = new igSolutionGeneratorConstant;
        sol_fun = solution_euler_uniform_3D;

        y_fun = y_bump_exp_3D;
        z_fun = z_bump_exp_3D;

        variable_number = 5;

        xi_min = -1.;
        xi_max = 1.;
        eta_min = 0.;
        eta_max = 1.;
        zeta_min = 0.;
        zeta_max = 1.;

        type_south = id_slip_wall;
        type_east = id_io_riemann;
        type_north = id_slip_wall;
        type_west = id_io_riemann;
        type_top = id_slip_wall;
        type_bottom = id_slip_wall;

    } else if(case_name == "euler_bump_cos"){

        mesh_gen = new igMeshGeneratorFitting;
        sol_gen = new igSolutionGeneratorConstant;

        y_fun = y_bump_cos;

        derivative_number = 2*variable_number;

        xi_min = 0;
        xi_max = 4.;
        eta_min = 0.;
        eta_max = 1.;

        type_south = id_slip_wall;
        type_east = id_io_riemann;
        type_north = id_slip_wall;
        type_west = id_io_riemann;

    } else if(case_name == "euler_cylinder"){

        mesh_gen = new igMeshGeneratorFitting;
        sol_gen = new igSolutionGeneratorFittingSurface;

        x_fun = x_circle;
        y_fun = y_circle;
        sol_fun = solution_euler_cylinder;

        type_south = id_cylinder;
        type_east = id_slip_wall;
        type_north = id_cylinder;
        type_west = id_slip_wall;
        exact_solution_id = id_cylinder;

    } else if(case_name == "euler_cylinder_nurbs"){

        mesh_gen = new igMeshGeneratorSplitting;
        sol_gen = new igSolutionGeneratorFittingSurface;

        sol_fun = solution_euler_cylinder;

        type_south = id_cylinder;
        type_east = id_slip_wall;
        type_north = id_cylinder;
        type_west = id_slip_wall;
        exact_solution_id = id_cylinder;

        int degree = 2;

        xcoord_in.resize((degree+1)*(degree+1));
        ycoord_in.resize((degree+1)*(degree+1));
        weight_in.resize((degree+1)*(degree+1));

        xcoord_in.at(0) = 1.;
        ycoord_in.at(0) = 0.;
        weight_in.at(0) = 1.;

        xcoord_in.at(1) = 2.5;
        ycoord_in.at(1) = 0.;
        weight_in.at(1) = 1.;

        xcoord_in.at(2) = 4.;
        ycoord_in.at(2) = 0.;
        weight_in.at(2) = 1.;

        xcoord_in.at(3) = 1.;
        ycoord_in.at(3) = 1.;
        weight_in.at(3) = sqrt(2.)*0.5;

        xcoord_in.at(4) = 2.5;
        ycoord_in.at(4) = 2.5;
        weight_in.at(4) = sqrt(2.)*0.5;

        xcoord_in.at(5) = 4.;
        ycoord_in.at(5) = 4.;
        weight_in.at(5) = sqrt(2.)*0.5;

        xcoord_in.at(6) = 0.;
        ycoord_in.at(6) = 1.;
        weight_in.at(6) = 1.;

        xcoord_in.at(7) = 0.;
        ycoord_in.at(7) = 2.5;
        weight_in.at(7) = 1.;

        xcoord_in.at(8) = 0.;
        ycoord_in.at(8) = 4;
        weight_in.at(8) = 1.;

    } else if(case_name == "euler_vortex"){

        mesh_gen = new igMeshGeneratorCartesian;
        sol_gen = new igSolutionGeneratorFittingSurface;

        sol_fun = solution_euler_vortex;

        xi_min = 0.;
        xi_max = 10.;
        eta_min = -5.;
        eta_max = 5.;

        type_south = id_vortex;
        type_east = id_vortex;
        type_north = id_vortex;
        type_west = id_vortex;
        exact_solution_id = id_vortex;

    }  else if(case_name == "euler_vortex_sliding"){

    	mesh_gen = new igMeshGeneratorCompound;
        sol_gen = new igSolutionGeneratorFittingSurface;

        sol_fun = solution_euler_vortex;
        exact_solution_id = id_vortex;

    } else if(case_name == "euler_piston"){

        mesh_gen = new igMeshGeneratorCartesian;
        sol_gen = new igSolutionGeneratorConstant;

        sol_fun = solution_euler_rest;

        xi_min = 0.;
        xi_max = 1.2;
        eta_min = 0.;
        eta_max = 0.2;

        type_south = id_slip_wall;
        type_east = id_slip_wall_fsi;
        type_north = id_slip_wall;
        type_west = id_slip_wall;

    } else if(case_name == "euler_shock_vortex"){

        mesh_gen = new igMeshGeneratorCartesian;
        sol_gen = new igSolutionGeneratorFittingSurface;

        sol_fun = solution_euler_shock_vortex;

        derivative_number = 2*variable_number;

        xi_min = 0.;
        xi_max = 2.;
        eta_min = 0.;
        eta_max = 1.;

        type_south = id_slip_wall;
        type_east = id_shock_vortex_right;
        type_north = id_slip_wall;
        type_west = id_shock_vortex_left;

    } else if(case_name == "euler_ringleb"){

        mesh_gen = new igMeshGeneratorCoons;
        sol_gen = new igSolutionGeneratorFittingSurface;

        sol_fun = solution_euler_ringleb;

        xi_min = 0.7;
        xi_max = 1.5;
        eta_min = -0.4;
        eta_max = 0.4;

        type_south = id_ringleb+1;
        type_east = id_ringleb+2;
        type_north = id_ringleb+3;
        type_west = id_ringleb+4;
        exact_solution_id = id_ringleb;

    } else if(case_name == "euler_naca"){

        mesh_gen = new igMeshGeneratorFitting;
        sol_gen = new igSolutionGeneratorConstant;

        x_fun = x_naca;
        y_fun = y_naca;
        distrib_xi_fun = distrib_xi_naca_fun;
        distrib_eta_fun = distrib_eta_id_fun;

        xi_min = 0.;
        xi_max = 8.;
        eta_min = 0.;
        eta_max = 1.;

        type_south = id_interior;
        type_east = id_io_riemann;
        type_north = id_periodic;
        type_west = id_slip_wall;

        scale_factor = 1./(x_naca(1.,0.,0.) - x_naca(1.,0.5,0.));

        periodic = true;

    } else if(case_name == "euler_naca_3D"){

        mesh_gen = new igMeshGeneratorFitting;
        sol_gen = new igSolutionGeneratorConstant;

        x_fun = x_naca;
        y_fun = y_naca;
        distrib_xi_fun = distrib_xi_naca_fun;
        distrib_eta_fun = distrib_eta_id_fun;
        sol_fun = solution_euler_uniform_3D;

        variable_number = 5;

        xi_min = 0.;
        xi_max = 1.;
        eta_min = 0.;
        eta_max = 1.;
        zeta_min = 0.;
        zeta_max = 1.;

        type_south = id_interior;
        type_east = id_io_riemann;
        type_north = id_periodic;
        type_west = id_slip_wall;
        type_top = id_slip_wall;
        type_bottom = id_slip_wall;

        scale_factor = 1./(x_naca(1.,0.,0.) - x_naca(1.,0.5,0.));

        periodic = true;

    } else if(case_name == "euler_multi_patch"){

        mesh_gen = new igMeshGeneratorCompound;
        sol_gen = new igSolutionGeneratorConstant;

    } else if(case_name == "ns_multi_patch"){

        mesh_gen = new igMeshGeneratorCompound;
        sol_gen = new igSolutionGeneratorConstant;

        derivative_number = 2*variable_number;

    } else if(case_name == "ns_extrude"){

        mesh_gen = new igMeshGeneratorCompound;
        sol_gen = new igSolutionGeneratorConstant;

        sol_fun = solution_euler_uniform_3D;

        variable_number = 5;
        derivative_number = 3*variable_number;

    } else if(case_name == "ns_plate"){

        mesh_gen = new igMeshGeneratorFitting;
        sol_gen = new igSolutionGeneratorConstant;

        distrib_xi_fun = distrib_xi_plate_fun;
        distrib_eta_fun = distrib_eta_plate_fun;

        derivative_number = 2*variable_number;

        xi_min = -2.;
        xi_max = 8;
        eta_min = 0.;
        eta_max = 2.;

        type_south = id_noslip_wall;
        type_east = id_io_density_velocity_pressure;
        type_north = id_slip_wall;
        type_west = id_io_density_velocity_pressure;

        enforce_sym = true;
        sym_coordinate = 0.;

    } else if(case_name == "ns_cylinder"){

        mesh_gen = new igMeshGeneratorFitting;
        sol_gen = new igSolutionGeneratorConstant;

        x_fun = x_cylinder;
        y_fun = y_cylinder;
        distrib_xi_fun = distrib_xi_cylinder_fun;
        distrib_eta_fun = distrib_eta_cylinder_fun;

        derivative_number = 2*variable_number;

        xi_min = 0.;
        xi_max = 1.;
        eta_min = 0.;
        eta_max = 1.;

        type_south = id_interior;
        type_east = id_io_riemann;
        type_north = id_periodic;
        type_west = id_noslip_wall;

        periodic = true;

    } else if(case_name == "ns_pipe"){

        mesh_gen = new igMeshGeneratorFitting;
        sol_gen = new igSolutionGeneratorConstant;

        y_fun = y_pipe;

        derivative_number = 2*variable_number;

        xi_min = -5.;
        xi_max = 10;
        eta_min = 0.;
        eta_max = 2.;

        type_south = id_noslip_wall;
        type_east = id_io_riemann;
        type_north = id_noslip_wall;
        type_west = id_io_riemann;

        enforce_sym = true;
        sym_coordinate = -4.;

    } else if(case_name == "ns_bump_exp_3D"){

        mesh_gen = new igMeshGeneratorFitting;
        sol_gen = new igSolutionGeneratorConstant;
        sol_fun = solution_euler_uniform_3D;
        distrib_eta_fun = distrib_eta_bump_3D_fun;

        y_fun = y_bump_exp_3D;
        z_fun = z_bump_exp_3D;

        variable_number = 5;
        derivative_number = 3*variable_number;

        xi_min = -1.;
        xi_max = 1.;
        eta_min = 0.;
        eta_max = 1.;
        zeta_min = 0.;
        zeta_max = 1.;

        type_south = id_noslip_wall;
        type_east = id_io_riemann;
        type_north = id_noslip_wall;
        type_west = id_io_riemann;
        type_top = id_slip_wall;
        type_bottom = id_slip_wall;

    } else if(case_name == "ns_tube_3D"){

        mesh_gen = new igMeshGeneratorFitting;
        sol_gen = new igSolutionGeneratorConstant;

        x_fun = x_tube;
        y_fun = y_tube;
        z_fun = z_tube;

        sol_fun = solution_euler_uniform_3D;

        variable_number = 5;
        derivative_number = 3*variable_number;

        xi_min = 0.;
        xi_max = 6.;
        eta_min = 0.;
        eta_max = 1.;
        zeta_min = 1.;
        zeta_max = 2.;

        type_south = id_interior;
        type_east = id_io_density_velocity_pressure;
        type_north = id_periodic;
        type_west = id_io_density_velocity_pressure;
        type_top = id_noslip_wall;
        type_bottom = id_noslip_wall;

        periodic = true;

    } else if(case_name == "ns_membrane"){

        mesh_gen = new igMeshGeneratorCartesian;
        sol_gen = new igSolutionGeneratorConstant;

        derivative_number = 2*variable_number;

        xi_min = -50.;
        xi_max = 51;
        eta_min = -50.;
        eta_max = 50.;

        type_south = id_io_riemann;
        type_east = id_io_riemann;
        type_north = id_io_riemann;
        type_west = id_io_riemann;

        enforce_membrane = true;
        distribution_file = true;

    } else {
        cout << "unknown configuration" << endl;
    }

}

/////////////////////////////////////////////////////////////////

void igCaseGenerator::setCase(const string &case_name)
{
    this->case_name = case_name;
}

void igCaseGenerator::setElementNumber(int n1, int n2, int n3)
{
    this->element_number_1 = n1;
    this->element_number_2 = n2;
    this->element_number_3 = n3;
}

/////////////////////////////////////////////////////////////////

igMeshGenerator* igCaseGenerator::meshGenerator(void)
{
    return this->mesh_gen;
}

igSolutionGenerator* igCaseGenerator::solutionGenerator(void)
{
    return this->sol_gen;
}

function<double(double, double, double)> igCaseGenerator::xFunction(void)
{
    return this->x_fun;
}

function<double(double, double, double)> igCaseGenerator::yFunction(void)
{
    return this->y_fun;
}

function<double(double, double, double)> igCaseGenerator::zFunction(void)
{
    return this->z_fun;
}

function<double(int, int, double, double)> igCaseGenerator::distributionXiFunction(void)
{
    return this->distrib_xi_fun;
}

function<double(int, int, double, double)> igCaseGenerator::distributionEtaFunction(void)
{
    return this->distrib_eta_fun;
}

function<double(int, int, double, double)> igCaseGenerator::distributionZetaFunction(void)
{
    return this->distrib_zeta_fun;
}

function<double(double*, double, int, double*, double*)> igCaseGenerator::solutionFunction(void)
{
    return this->sol_fun;
}

double igCaseGenerator::xiMin(void)
{
    return this->xi_min;
}

double igCaseGenerator::xiMax(void)
{
    return this->xi_max;
}

double igCaseGenerator::etaMin(void)
{
    return this->eta_min;
}

double igCaseGenerator::etaMax(void)
{
    return this->eta_max;
}

double igCaseGenerator::zetaMin(void)
{
    return this->zeta_min;
}

double igCaseGenerator::zetaMax(void)
{
    return this->zeta_max;
}

int igCaseGenerator::variableNumber(void)
{
    return this->variable_number;
}

int igCaseGenerator::derivativeNumber(void)
{
    return this->derivative_number;
}

int igCaseGenerator::typeEastBoundary(void)
{
    return this->type_east;
}

int igCaseGenerator::typeWestBoundary(void)
{
    return this->type_west;
}

int igCaseGenerator::typeNorthBoundary(void)
{
    return this->type_north;
}

int igCaseGenerator::typeSouthBoundary(void)
{
    return this->type_south;
}

int igCaseGenerator::typeTopBoundary(void)
{
    return this->type_top;
}
int igCaseGenerator::typeBottomBoundary(void)
{
    return this->type_bottom;
}

int igCaseGenerator::exactSolutionId(void)
{
    return this->exact_solution_id;
}

int igCaseGenerator::elementNumber1(void)
{
    return this->element_number_1;
}

int igCaseGenerator::elementNumber2(void)
{
    return this->element_number_2;
}

int igCaseGenerator::elementNumber3(void)
{
    return this->element_number_3;
}

bool igCaseGenerator::enforceSymetry(void)
{
    return this->enforce_sym;
}

double igCaseGenerator::symetryCoordinate(void)
{
    return this->sym_coordinate;
}

bool igCaseGenerator::enforceMembrane(void)
{
    return this->enforce_membrane;
}

bool igCaseGenerator::distributionFile(void)
{
    return this->distribution_file;
}

double igCaseGenerator::scaleFactor(void)
{
    return this->scale_factor;
}

bool igCaseGenerator::periodicBoundary(void)
{
    return this->periodic;
}

vector<double>& igCaseGenerator::xCoordinateInput(void)
{
    return this->xcoord_in;
}

vector<double>& igCaseGenerator::yCoordinateInput(void)
{
    return this->ycoord_in;
}

vector<double>& igCaseGenerator::zCoordinateInput(void)
{
    return this->zcoord_in;
}

vector<double>& igCaseGenerator::weightInput(void)
{
    return this->weight_in;
}

int igCaseGenerator::sizeInput(int component)
{
    return input_size[component];
}
