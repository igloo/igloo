/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <vector>
#include <cmath>

using namespace std;

void pureSliding(double time, double time_step, vector<double> *coord, vector<double> *vel, vector<double> *param, const vector<double> *init, const int subdomain_number)
{
	double rps = 1;

	// param controls rotations per second
	if(param->size()>0)
		rps = param->at(0);

    // Exact mesh movement for sliding (rotation)
    if(subdomain_number == 0){
    	double omega = -rps*M_PI;
    	double x_c = 5.;
    	double y_c = 0.;
    	double delta_theta = omega*time_step;

    	double x = coord->at(0);
    	double y = coord->at(1);

    	coord->at(0) = x_c + cos(delta_theta)*(x - x_c) - sin(delta_theta)*(y - y_c);
    	coord->at(1) = y_c + sin(delta_theta)*(x - x_c) + cos(delta_theta)*(y - y_c);
    	vel->at(0) = -omega*(coord->at(1) - y_c);
    	vel->at(1) = omega*(coord->at(0) - x_c);
    } else if(subdomain_number == 1){
    	vel->at(0) = 0.;
    	vel->at(1) = 0.;
    }
}


void turbineSliding(double time, double time_step, vector<double> *coord, vector<double> *vel, vector<double> *param, const vector<double> *init, const int subdomain_number)
{
	double TSR = 1;

	// param controls tip speed ratio
	if(param->size()>0)
		TSR = param->at(0);

    // Exact mesh movement for sliding (rotation)
    if(subdomain_number == 0){
    	double omega = TSR*0.1/1.5;
    	double x_c = 0.;
    	double y_c = 0.;
    	double delta_theta = omega*time_step;

    	double x = coord->at(0);
    	double y = coord->at(1);

    	coord->at(0) = x_c + cos(delta_theta)*(x - x_c) - sin(delta_theta)*(y - y_c);
    	coord->at(1) = y_c + sin(delta_theta)*(x - x_c) + cos(delta_theta)*(y - y_c);
    	vel->at(0) = -omega*(coord->at(1) - y_c);
    	vel->at(1) = omega*(coord->at(0) - x_c);
    } else if(subdomain_number == 1){
    	vel->at(0) = 0.;
    	vel->at(1) = 0.;
    }
}


void slidingOscillation(double time, double time_step, vector<double> *coord, vector<double> *vel, vector<double> *param, const vector<double> *init, const int subdomain_number)
{
	double Delta_theta = 30;

	// param controls amplitude in degrees
	if(param->size()>0)
		Delta_theta = param->at(0);

    // Exact mesh movement for sliding (oscillation)
    if(subdomain_number == 0){
    	// Angle amplitude and frequency
    	double A = Delta_theta*M_PI/180.;
    	double freq = 0.05;

    	double x_c = 0.;
    	double y_c = 0.;
    	double x = coord->at(0);
    	double y = coord->at(1);

    	double time_end = time + time_step;
    	double delta_theta = A*(sin(2*M_PI*freq*time_end) - sin(2*M_PI*freq*time));
    	double omega = A*2*M_PI*freq*cos(2*M_PI*freq*time_end);

    	coord->at(0) = x_c + cos(delta_theta)*(x - x_c) - sin(delta_theta)*(y - y_c);
    	coord->at(1) = y_c + sin(delta_theta)*(x - x_c) + cos(delta_theta)*(y - y_c);
    	vel->at(0) = -omega*(coord->at(1) - y_c);
    	vel->at(1) = omega*(coord->at(0) - x_c);
    } else if(subdomain_number == 1){
    	vel->at(0) = 0.;
    	vel->at(1) = 0.;
    }
}
