/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <math.h>


double solution_viscous_burgers(double *coord, double time, int var_id, double *coord_elt, double *func_param)
{
    double x = coord[0];

    double value;

    double a = func_param[0];
    double eps = func_param[1];
    double b = func_param[2];

    double myarg = (x+0.5 - (a+b)/2*time)/(4.*eps) *(a-b);
    double mytanh = tanh(myarg);
    double mytime = (x+0.5 -a*time)/(4*eps);

    if(var_id == 0)
        value = (a+b)/2. - (a-b)/2. * mytanh;
    else if(var_id == 1)
        value = 1./2. - 1./2.*mytanh  - (a-b)/2.*(1. - mytanh*mytanh)*mytime;
    else if(var_id == 2)
        value = (a-b)/2. *(1. - mytanh*mytanh)*(x+0.5 -(a+b)/2*time)/(4*eps*eps)*(a-b);
    else if(var_id == 3)
        value = (1. - mytanh*mytanh)*( -mytime + (a-b)/2.* time/(4.*eps)
                                                + (a-b) * mytanh * mytime*mytime);
    else if(var_id == 4)
        value = (1. - mytanh*mytanh)*(x+0.5 - (a+b)/2*time)/(4.*eps*eps)*(a-b)
                *( 1./2. - (a-b)*mytanh*mytime )
                + (a-b)/2. * (1.-mytanh*mytanh)*mytime/eps;
    else if(var_id == 5)
        value = (a-b)*mytanh*(1.-mytanh*mytanh)*(x+0.5 -(a+b)/2*time)/(4*eps*eps)*(a-b)*(x+0.5 -(a+b)/2*time)/(4*eps*eps)*(a-b)
                -(a-b)*(1.-mytanh*mytanh)*(x+0.5 -(a+b)/2*time)/(4*eps*eps*eps)*(a-b);
    else if(var_id == 6)
        value = (1. - mytanh*mytanh)*(-2.*mytanh*mytime*( -mytime + (a-b)/2.* time/(4.*eps) + (a-b) * mytanh * mytime*mytime)
                                      + 3.*time/(8.*eps) + mytanh*mytime*mytime
                                     + (a-b)*(1-mytanh*mytanh)*mytime*mytime*mytime - (a-b)*mytanh*mytime*time/(2.*eps)  );
    else if(var_id == 7)
        value = (1. - mytanh*mytanh) * (  2*mytanh*(x+0.5 - (a+b)/2*time)/(4.*eps*eps)*(a-b)
                                                   * ( -mytime + (a-b)/2.* time/(4.*eps)  + (a-b) * mytanh * mytime*mytime)
                                                   +  mytime/eps - (a-b)/2*time/(4*eps*eps) - (a-b)*(1-mytanh*mytanh)*(x+0.5 - (a+b)/2*time)/(4.*eps*eps)*(a-b)*mytime*mytime
                                                   - (a-b)*mytanh*mytime*mytime *2./eps );

    else if(var_id == 8)
        value = 2*mytanh*(1.-mytanh*mytanh)*myarg/eps*myarg/eps*( 1./2. - (a-b)*mytanh*mytime )
                -2*(1.-mytanh*mytanh)*myarg/eps/eps*( 1./2. - (a-b)*mytanh*mytime )
                +(1.-mytanh*mytanh)*myarg/eps*( (a-b)*(1.-mytanh*mytanh)*myarg/eps*mytime + (a-b)*mytanh*mytime/eps )
                +(a-b)*mytanh*(1.-mytanh*mytanh)*myarg/eps*mytime/eps
                -(a-b)*(1.-mytanh*mytanh)*mytime/eps/eps;

    else if(var_id == 9)
        value = (a-b)*(1.-mytanh*mytanh)*myarg/eps*myarg/eps*myarg/eps * (-1+3*mytanh*mytanh)
                -6*(a-b)*mytanh*(1.-mytanh*mytanh)*myarg/eps*myarg/eps/eps
                +3*(a-b)*(1.-mytanh*mytanh)*myarg/(eps*eps*eps);

    return value;
}

double solution_viscous_burgers_gradient(double *coord, double time, int var_id, double *coord_elt, double *func_param)
{
    double x = coord[0];

    double value;

    double a = func_param[0];
    double eps = func_param[1];
    double b = func_param[2];

    double myarg = (x+0.5 - (a+b)/2*time)/(4.*eps) *(a-b);
    double mytanh = tanh(myarg);
    double mytime = (x+0.5 -a*time)/(4*eps);

    if(var_id == 0)
        value = sqrt(eps)*( -(a-b)/2. * (1. - mytanh*mytanh)*(a-b)/(4.*eps) );

    else if(var_id == 1)
        value = sqrt(eps)*(-1./2. *(1.-mytanh*mytanh)*(a-b)/(4.*eps)
                + (a-b)/2. * (x+0.5 -a*time)/(4.*eps) * 2*mytanh * (1. - mytanh*mytanh)*(a-b)/(4.*eps)
                - (a-b)/2. *(1. - mytanh*mytanh)/(4*eps) );

     else if(var_id == 2)
        value = sqrt(eps)*(-(a-b)/2 * 2*mytanh * (1. - mytanh*mytanh)*(a-b)/(4*eps)
                * (x+0.5 -(a+b)/2*time)/(4*eps*eps)*(a-b)
                + (a-b)/2. *(1. - mytanh*mytanh)*(a-b)/(4*eps*eps) );

     else if(var_id == 3)
        value = sqrt(eps)*( (-2.*mytanh*(1.-mytanh*mytanh)*(a-b)/(4.*eps))*( -mytime + (a-b)/2.* time/(4.*eps) + (a-b) * mytanh * mytime*mytime)
                + (1.-mytanh*mytanh)*(-1./(4.*eps) + (a-b)*(1.-mytanh*mytanh)*(a-b)/(4.*eps)*mytime*mytime)
                                     + (a-b)*mytanh*mytime/(2.*eps));
     else if(var_id == 4)
        value =sqrt(eps)*( (-2.*mytanh*(1.-mytanh*mytanh)*(a-b)/(4*eps) * (x+0.5 -(a+b)/2*time)/(4*eps*eps)*(a-b) + (1.-mytanh*mytanh)*(a-b)/(4*eps*eps) )
                                          * ( 1./2. - (a-b)*mytanh*mytime )
                                          + (1. - mytanh*mytanh)*(x+0.5 - (a+b)/2*time)/(4.*eps*eps)*(a-b)
                                          * ( -(a-b)*(1.-mytanh*mytanh)*(a-b)/(4.*eps)*mytime - (a-b)*mytanh/(4.*eps) )
                                          + (a-b)/2*(-2.*mytanh*(1.-mytanh*mytanh)*(a-b)/(4.*eps))*mytime/eps
                                          + (a-b)/2*(1.-mytanh*mytanh)/(4.*eps*eps)    );

     else if(var_id == 5)
        value =sqrt(eps)*( (a-b)*( (1.-mytanh*mytanh)*(1.-mytanh*mytanh) -2.*mytanh*mytanh*(1.-mytanh*mytanh) )*(a-b)/(4.*eps) * (x+0.5 -(a+b)/2*time)/(4*eps*eps)*(a-b)*(x+0.5 -(a+b)/2*time)/(4*eps*eps)*(a-b)
                                        + (a-b)*( mytanh*(1.-mytanh*mytanh) *2.*(x+0.5 -(a+b)/2*time)/(4*eps*eps)*(a-b) *(a-b)/(4.*eps*eps) )
                                        - (a-b)*( -2.*mytanh*(1.-mytanh*mytanh)*(a-b)/(4.*eps) ) * (x+0.5 -(a+b)/2*time)/(4*eps*eps*eps)*(a-b)
                                        - (a-b)*(1.-mytanh*mytanh)*(a-b)/(4.*eps*eps*eps)    );
     else if(var_id == 6)
        value = sqrt(eps)*( - 2*mytanh*(1. - mytanh*mytanh)*(a-b)/(4*eps)*(-2.*mytanh*mytime*( -mytime + (a-b)/2.* time/(4.*eps) + (a-b) * mytanh * mytime*mytime)
                                           + 3.*time/(8.*eps) + mytanh*mytime*mytime
                                           + (a-b)*(1-mytanh*mytanh)*mytime*mytime*mytime - (a-b)*mytanh*mytime*time/(2.*eps)  )
                                           +(1. - mytanh*mytanh)*(-2*(1. - mytanh*mytanh)*(a-b)/(4*eps)*( -mytime + (a-b)/2.* time/(4.*eps) + (a-b) * mytanh * mytime*mytime)
                                           -2.*mytanh/(4*eps)*( -mytime + (a-b)/2.* time/(4.*eps) + (a-b) * mytanh * mytime*mytime)
                                           -2.*mytanh*mytime*( -1./(4*eps) + (a-b) * (1. - mytanh*mytanh)*(a-b)/(4*eps) * mytime*mytime + (a-b) * mytanh * 2*mytime/(4*eps))
                                            + (1.-mytanh*mytanh)*(a-b)/(4*eps)*mytime*mytime +  mytanh*2*mytime/(4*eps)
                                            -2*(a-b)*mytanh*(1.-mytanh*mytanh)*(a-b)/(4*eps)*mytime*mytime*mytime + (a-b)*(1-mytanh*mytanh)*3*mytime*mytime/(4*eps)
                                            - (a-b)*(1.-mytanh*mytanh)*(a-b)/(4*eps)*mytime*time/(2.*eps) - (a-b)*mytanh*mytime*time/(8.*eps*eps)    )
                                           );

    else if(var_id == 7)
        value = sqrt(eps)*( - 2*mytanh*(1.-mytanh*mytanh)*(a-b)/(4*eps) * (  2*mytanh*(x+0.5 - (a+b)/2*time)/(4.*eps*eps)*(a-b)
                                * ( -mytime + (a-b)/2.* time/(4.*eps)  + (a-b) * mytanh * mytime*mytime)
                                 +  mytime/eps - (a-b)/2*time/(4*eps*eps) - (a-b)*(1-mytanh*mytanh)*(x+0.5 - (a+b)/2*time)/(4.*eps*eps)*(a-b)*mytime*mytime
                                 - (a-b)*mytanh*mytime*mytime *2./eps )
             + (1. - mytanh*mytanh) * (  2*(1-mytanh*mytanh)*(a-b)/(4*eps)*(x+0.5 - (a+b)/2*time)/(4.*eps*eps)*(a-b) * ( -mytime + (a-b)/2.* time/(4.*eps)  + (a-b) * mytanh * mytime*mytime)
                                      +  2*mytanh*(a-b)/(4.*eps*eps) * ( -mytime + (a-b)/2.* time/(4.*eps)  + (a-b) * mytanh * mytime*mytime)
                                      +  2*mytanh*(x+0.5 - (a+b)/2*time)/(4.*eps*eps)*(a-b) * ( -1/(4*eps) + (a-b)*(1-mytanh*mytanh)*(a-b)/(4*eps) * mytime*mytime + (a-b) * mytanh * 2*mytime/(4*eps))
                                         + 1/(4*eps*eps)
                                         - (a-b)*(-2*mytanh)*(1-mytanh*mytanh)*(a-b)/(4*eps)*(x+0.5 - (a+b)/2*time)/(4.*eps*eps)*(a-b)*mytime*mytime
                                         - (a-b)*(1-mytanh*mytanh)*(a-b)/(4.*eps*eps)*mytime*mytime
                                         - (a-b)*(1-mytanh*mytanh)*(x+0.5 - (a+b)/2*time)/(4.*eps*eps)*(a-b)*2*mytime/(4*eps)
                                       - (a-b)*(1-mytanh*mytanh)*(a-b)/(4*eps)*mytime*mytime *2./eps
                                       - (a-b)*mytanh*2*mytime/(4*eps) *2./eps
                                         ) );

    else if(var_id == 8)
        value = sqrt(eps)* ( 2*(1-mytanh*mytanh)*(a-b)/(4*eps)*(1.-mytanh*mytanh)*myarg/eps*myarg/eps*( 1./2. - (a-b)*mytanh*mytime )
                                            -4*mytanh*mytanh*(1-mytanh*mytanh)*(a-b)/(4*eps)*myarg/eps*myarg/eps*( 1./2. - (a-b)*mytanh*mytime )
                                            +4*mytanh*(1.-mytanh*mytanh)*(a-b)/(4*eps)/eps*myarg/eps*( 1./2. - (a-b)*mytanh*mytime )
                                            -2*mytanh*(1.-mytanh*mytanh)*myarg/eps*myarg/eps*( (a-b)*(1-mytanh*mytanh)*(a-b)/(4*eps)*mytime + (a-b)*mytanh/(4*eps) )
                                            +4*mytanh*(1-mytanh*mytanh)*(a-b)/(4*eps)*myarg/eps/eps*( 1./2. - (a-b)*mytanh*mytime )
                                            -2*(1.-mytanh*mytanh)*(a-b)/(4*eps)/eps/eps*( 1./2. - (a-b)*mytanh*mytime )
                                            +2*(1.-mytanh*mytanh)*myarg/eps/eps*( (a-b)*(1-mytanh*mytanh)*(a-b)/(4*eps)*mytime + (a-b)*mytanh/(4*eps) )
                                            -2*mytanh*(1-mytanh*mytanh)*(a-b)/(4*eps)*myarg/eps*( (a-b)*(1.-mytanh*mytanh)*myarg/eps*mytime + (a-b)*mytanh*mytime/eps )
                                            +(1.-mytanh*mytanh)*(a-b)/(4*eps)/eps*( (a-b)*(1.-mytanh*mytanh)*myarg/eps*mytime + (a-b)*mytanh*mytime/eps )
                                            +(1.-mytanh*mytanh)*myarg/eps*( -(a-b)*2*mytanh*(1.-mytanh*mytanh)*(a-b)/(4*eps)*myarg/eps*mytime +
                                                    (a-b)*(1.-mytanh*mytanh)*(a-b)/(4*eps)/eps*mytime + (a-b)*(1.-mytanh*mytanh)*myarg/eps/(4*eps) + (a-b)*(1-mytanh*mytanh)*(a-b)/4*eps*mytime/eps + (a-b)*mytanh/(4*eps)/eps )
                                            +(a-b)*(1-mytanh*mytanh)*(a-b)/(4*eps)*(1.-mytanh*mytanh)*myarg/eps*mytime/eps
                                            -(a-b)*mytanh*2*mytanh*(1-mytanh*mytanh)*(a-b)/(4*eps)*myarg/eps*mytime/eps
                                            +(a-b)*mytanh*(1.-mytanh*mytanh)*(a-b)/(4*eps)/eps*mytime/eps
                                            +(a-b)*mytanh*(1.-mytanh*mytanh)*myarg/eps/(4*eps)/eps
                                            +(a-b)*2*mytanh*(1-mytanh*mytanh)*(a-b)/(4*eps)*mytime/eps/eps
                                            -(a-b)*(1.-mytanh*mytanh)/(4*eps)/eps/eps   );

    else if(var_id == 9)
        value = sqrt(eps)* (  -2*(a-b)*mytanh*(1.-mytanh*mytanh)*(a-b)/(4*eps)*myarg/eps*myarg/eps*myarg/eps * (-1+3*mytanh*mytanh)
                                             +3*(a-b)*(1.-mytanh*mytanh)*myarg/eps*myarg/eps*(a-b)/(4*eps*eps) * (-1+3*mytanh*mytanh)
                                             - (a-b)*(1.-mytanh*mytanh)*myarg/eps*myarg/eps*myarg/eps * 6*mytanh*(1.-mytanh*mytanh)*(a-b)/(4*eps)
                                              -6*(a-b)*(1-mytanh*mytanh)*(a-b)/(4*eps)*(1.-mytanh*mytanh)*myarg/eps*myarg/eps/eps
                                              +12*(a-b)*mytanh*mytanh*(1-mytanh*mytanh)*(a-b)/(4*eps)*myarg/eps*myarg/eps/eps
                                              -6*(a-b)*mytanh*(1.-mytanh*mytanh)*2*myarg/eps/eps*(a-b)/(4*eps*eps)
                                             -6*(a-b)*mytanh*(1.-mytanh*mytanh)*(a-b)/(4*eps)*myarg/(eps*eps*eps)
                                             +3*(a-b)*(1.-mytanh*mytanh)*(a-b)/(4*eps*eps*eps*eps)   );

    return value;
}

double target_solution_viscous_burgers(double position, double time)
{
    double a = 2;
    double b = 0;
    double eps = 0.1;

    double mytanh = tanh( (position+0.5 - (a+b)/2*time)/(4.*eps) *(a-b));

    return (a+b)/2. - (a-b)/2. * mytanh;
}
