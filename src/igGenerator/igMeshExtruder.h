/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/

#pragma once

#include <igGeneratorExport.h>

#include <vector>

class igMesh;
class igCommunicator;

class IGGENERATOR_EXPORT igMeshExtruder
{
public:
     igMeshExtruder(igMesh *mesh, vector<double> *coordinates, vector<double> *velocities);
    ~igMeshExtruder();

public:
    void setLayerNumber(int number);
    void setCommunicator(igCommunicator *communicator);
    void setTranslation(double delta_trans);
    void setRotation(double delta_rot);
    void setScale(double delta_scale);
    void enablePeriodicity(void);
    void enableMorphingPerturbation(double epsilon);

public:
    void extrude(void);

public:
    igMesh *extrudedMesh(void);
    vector<double> *extrudedVelocities(void);

private:

    igCommunicator *communicator;

    igMesh *mesh2D;
    vector<double> *coordinates2D;
    vector<double> *velocities2D;

    igMesh *mesh;
    vector<double> *coordinates;
    vector<double> *velocities;

    int layer_number;

    double delta_trans;
    double delta_rot;
    double delta_scale;

    double morphing_epsilon;

    bool periodic;
    bool morphing_3D;


};

//
// igMeshGeneratorCompound.h ends here
