/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <math.h>
#include "igSolutionAnalyticShared.h"
#include "igRingleb.h"

////////////////////////////////////////////////////

double solution_euler_uniform(double *coord, double time, int var_id, double *coord_elt, double *func_param)
{
    double mach = func_param[0];
    double incidence = func_param[1];

    double rho = 1.4;
    double u = mach * cos(M_PI/180*incidence);
    double v = mach * sin(M_PI/180*incidence);
    double p = 1;

    return conservativeVariable(rho, u, v, p, var_id);
}

double solution_euler_uniform_3D(double *coord, double time, int var_id, double *coord_elt, double *func_param)
{
    double mach = func_param[0];

    double rho = 1.4;
    double u = mach;
    double v = 0.;
    double w = 0.;
    double p = 1;

    return conservativeVariable(rho, u, v, w, p, var_id);
}

double solution_euler_vortex(double *coord, double time, int var_id, double *coord_elt, double *func_param)
{
    double x = coord[0];
    double y = coord[1];

    double r2 = (x-time-5.)*(x-time-5.) + y*y;

    double u = 1. - 5.*exp(1.-r2)*y/(2.*M_PI);
    double v = 5.*exp(1.-r2)*(x-time-5.)/(2.*M_PI);
    double rho = pow( 1.-0.4/(16.*1.4*M_PI*M_PI)*25.*exp(2.*(1.-r2)) ,1./0.4);
    double p = pow(rho,1.4);

    return conservativeVariable(rho, u, v, p, var_id);
}

double solution_euler_ringleb(double *coord, double time, int var_id, double *coord_elt, double *func_param)
{
    double x = coord[0];
    double y = coord[1];

    double k;
    double q;
    find_coord(x,y,k,q);

    double rho = ringleb_rho(k,q);
    double u = q*cos(asin(q/k));
    double v = - q*q/k;
    double p = ringleb_p(k,q);

    if(y<0)
        u = -u;

    return conservativeVariable(rho, u, v, p, var_id);
}

double solution_euler_ringleb_boundary(double x, double y, double time, int var_id, int face_type)
{
    double k;
    double q;
    find_coord_boundary(x,y,k,q,face_type);

    double rho = ringleb_rho(k,q);
    double u = q*cos(asin(q/k));
    double v = - q*q/k;
    double p = ringleb_p(k,q);

    if(y<0)
        u = -u;

    return conservativeVariable(rho, u, v, p, var_id);
}

double solution_euler_rest(double *coord, double time, int var_id, double *coord_elt, double *func_param)
{
    double rho = 1.3;
    double u = 0.;
    double v = 0.;
    double p = 77472;

    return conservativeVariable(rho, u, v, p, var_id);
}

double solution_euler_cylinder(double *coord, double time, int var_id, double *coord_elt, double *func_param)
{

    double r0 = 1.;
    double r1 = 4.;
    double div = 1. / (r1/r0-r0/r1);

    double x = coord[0];
    double y = coord[1];

    double r = sqrt(x*x+y*y);
    double theta = (x>1.E-15) ? atan(y/x) : M_PI*0.5;

    double rho = 1;
    double p = rho * (1. + 0.2*0.2*div*div*(r*r/(2.*r1*r1)-2.*log(r)-r1*r1/(2.*r*r)));
    double u_theta = 0.2*div*(r1/r-r/r1);
    double u = -sin(theta)*u_theta;
    double v = cos(theta)*u_theta;

    double value = conservativeVariable(rho, u, v, p, var_id);

    return value;
}
