/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/

#include <iostream>
#include <fstream>
#include <sstream>

#include <igFiles/igFileManager.h>
#include <igCore/igMesh.h>
#include <igCore/igElement.h>

#include "igSolutionGeneratorConstant.h"


/////////////////////////////////////////////////////////////////////////////

igSolutionGeneratorConstant::igSolutionGeneratorConstant() : igSolutionGenerator()
{

}

/////////////////////////////////////////////////////////////////////////////

void igSolutionGeneratorConstant::generate(int var_id)
{
    int state_size = this->mesh->controlPointNumber() * this->mesh->variableNumber();

    if(!this->state){
        this->state = new vector<double>;
        this->state->resize(state_size);
    }

    double *generic_param;;

    double value = sol_fun(generic_param, 0., var_id, generic_param, this->func_param);

    // loop over elements
    for (int iel=0; iel<this->mesh->elementNumber(); iel++){

        igElement *elt = this->mesh->element(iel);

        int dof_number = elt->controlPointNumber();

        for (int idof=0; idof<dof_number; idof++){
            this->state->at(elt->globalId(idof,var_id)) = value;

            if(this->perturbation && var_id == 3){
                double delta = -1+float(rand())/float(RAND_MAX)*2;
                this->state->at(elt->globalId(idof,var_id)) = delta*0.01;
            }
        }

    }

}
