/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include <igFiles/igFileManager.h>
#include <igCore/igMesh.h>

#include "igSolutionGenerator.h"

/////////////////////////////////////////////////////////////////////////////

igSolutionGenerator::igSolutionGenerator(void)
{
    this->state = nullptr;
    this->perturbation = false;
}

/////////////////////////////////////////////////////////////////////////////

void igSolutionGenerator::setMesh(igMesh *mesh)
{
    this->mesh = mesh;
}



void igSolutionGenerator::writeSolutionFile(const string &sol_filename)
{
    igFileManager file_manager;
    file_manager.writeSolutionIgloo(sol_filename, mesh->variableNumber(), this->mesh, this->state);
    
}

void igSolutionGenerator::setSolutionFunction(function<double (double *coord, double time, int var_id, double *coord_elt, double *func_param)> sol_fun)
{
    this->sol_fun = sol_fun;
}

void igSolutionGenerator::setSolutionFunctionParameters(double *func_param)
{
    this->func_param = func_param;
}

void igSolutionGenerator::enablePerturbation(void)
{
    this->perturbation = true;
}

////////////////////////////////////////////////////////////////////////////

void igSolutionGenerator::clearState(void){

    if(this->state)
        this->state->clear();
}



///////////////////////////////////////////////////////////


vector<double>* igSolutionGenerator::generatedState(void)
{
    return this->state;
}
