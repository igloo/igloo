/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igGeneratorExport.h>

#include <string>
#include <functional>
#include <vector>

class igMesh;

using namespace std;

class IGGENERATOR_EXPORT igSolutionGenerator
{
public:
             igSolutionGenerator(void);
    virtual ~igSolutionGenerator(void) = default;

public:
    void setMesh(igMesh *mesh);
    void setSolutionFunction(function<double(double *coord, double time, int var_id, double *coord_elt, double *func_param)> sol_fun);
    void setSolutionFunctionParameters(double *func_param);

    void enablePerturbation(void);

public:
    void writeSolutionFile(const string& sol_filename);

public:
    vector<double>* generatedState(void);

public:
    virtual void generate(int var_id) = 0;

public:
    void clearState(void);

protected:

    igMesh *mesh;

    vector<double> *state;

    function<double(double *coord, double time, int var_id, double *coord_elt, double *func_param)> sol_fun;
    double *func_param;

    bool perturbation;

};

//
// igSolutionGenerator.h ends here
