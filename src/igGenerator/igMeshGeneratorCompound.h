/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <igGeneratorExport.h>

#include "igMeshGenerator.h"

class IGGENERATOR_EXPORT igMeshGeneratorCompound : public igMeshGenerator
{
public:
     igMeshGeneratorCompound(void);
    ~igMeshGeneratorCompound(void) = default;

public:
    void generate(void) override;


private:
    void readBoundaryPatch(ifstream &base_file, int patch_degree, vector<double> &xcoord_tmp, vector<double> &ycoord_tmp, vector<double> &weight_tmp);
    void readCornerPatch(ifstream &base_file, ifstream &curve_file, int patch_degree, vector<double> &xcoord_tmp, vector<double> &ycoord_tmp, vector<double> &weight_tmp);


};

//
// igMeshGeneratorCompound.h ends here

