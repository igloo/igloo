/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/

#include <iostream>
#include <fstream>
#include <sstream>

#include <igCore/igFittingCurve1D.h>
#include <igCore/igMesh.h>
#include <igCore/igElement.h>
#include <igFiles/igFileManager.h>

#include "igSolutionGeneratorFittingCurve.h"


/////////////////////////////////////////////////////////////////////////////

igSolutionGeneratorFittingCurve::igSolutionGeneratorFittingCurve() : igSolutionGenerator()
{

}

/////////////////////////////////////////////////////////////////////////////

void igSolutionGeneratorFittingCurve::generate(int var_id)
{

    int state_size = this->mesh->controlPointNumber() * this->mesh->variableNumber();
    int degree = this->mesh->element(0)->cellDegree();
    
    if(!this->state){
        this->state = new vector<double>;
        this->state->resize(state_size);
    }

    int dof_number = (degree+1);
    int sample_number = 10*(degree+1);

    igFittingCurve1D *fitting = new igFittingCurve1D;
    fitting->setSampleNumber(sample_number);
    fitting->setFunction(sol_fun,var_id,this->func_param);

    double value;
    
    // loop over elements
    for (int iel=0; iel<this->mesh->elementNumber(); iel++){

        igElement *elt = this->mesh->element(iel);

        fitting->setElement(elt);
        fitting->run();

        for (int idof=0; idof<dof_number; idof++)
            this->state->at(elt->globalId(idof,var_id)) = fitting->zControlPointFit(idof);

    }

    delete fitting;

}
