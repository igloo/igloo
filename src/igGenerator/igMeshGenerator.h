/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igGeneratorExport.h>

#include <string>
#include <functional>
#include <vector>

class igMesh;
class igCommunicator;

using namespace std;

class IGGENERATOR_EXPORT igMeshGenerator
{
public:
             igMeshGenerator(void);
    virtual ~igMeshGenerator(void) = default;

public:
    void setDegree(int degree);
    void setGaussPointNumber(int number);
    void setVariableNumber(int number);
    void setDerivativeNumber(int number);
    void setExactSolutionId(int id);
    void setElementNumber(int element_number_1, int element_number_2, int element_number_3);
    void setDomainBounds(double xi_min, double xi_max, double eta_min, double eta_max, double zeta_min, double zeta_max);
    void setRotation(double angle);
    void setScale(double scale);
    void setInitialPatch(vector<double> &xcoord_in, vector<double> &ycoord_in, vector<double> &weight_in, int element_number_1_in, int element_number_2_in);

    void setXFunction(function<double(double, double, double)> x_fun);
    void setYFunction(function<double(double, double, double)> y_fun);
    void setZFunction(function<double(double, double, double)> z_fun);
    void setDitributionXiFunction(function<double(int, int, double, double)> distrib_xi_fun);
    void setDitributionEtaFunction(function<double(int, int, double, double)> distrib_eta_fun);
    void setDitributionZetaFunction(function<double(int, int, double, double)> distrib_zeta_fun);
    void setConfigFileName(string);
    void setDistributionFile(bool distribution_file);

    void setBoundaryConditions(int condition_id_south, int condition_id_east, int condition_id_north,
                               int condition_id_west, int condition_id_top, int condition_id_bottom);

    void setClamp(int number);

public:
    void setCommunicator(igCommunicator *comunicator);

public:
    void enforceSymmetry(double coordinate);
    void enforceMembrane(void);

    void setPeriodicity(bool periodic);
    void setSubdomains(bool flag_subdomain);

    int variableNumber(void) { return variable_number; }
    igMesh *generatedMesh(void) { return this->mesh; }

public:
    void buildGeneratedMesh(void);

public:
    void writeMeshFile(const string& mesh_filename);

public:
    virtual void generate(void) = 0;

public:
    vector<double>& xCoordinate();
    vector<double>& yCoordinate();
    vector<double>& zCoordinate();
    vector<double>& weightCoordinate();

protected:
    void dgMeshConverter(vector<double> &xcoord_tmp, vector<double> &ycoord_tmp, vector<double> &zcoord_tmp, vector<double> &weight_tmp, vector<double> &xcoord, vector<double> &ycoord, vector<double> &zcoord, vector<double> &weight);

    void applyBoundaryCondition(void);

protected:
    int degree;
    int gauss_pt_number;
    int element_number_1;
    int element_number_2;
    int element_number_3;
    int element_number_1_in;
    int element_number_2_in;
    int variable_number;
    int derivative_number;
    int exact_solution_id;

    string config_filename;

    double xi_min;
    double xi_max;
    double eta_min;
    double eta_max;
    double zeta_min;
    double zeta_max;

    double angle;
    double scale;

    bool subdomains;
    bool periodic;
    bool distribution_file;

    bool enforce_membrane;
    int clamp_number;
    int membrane_elt_number;
    int upstream_membrane_elt_number;
    int below_membrane_elt_number;

    igMesh *mesh = 0;

    int condition_id_south;
    int condition_id_east;
    int condition_id_north;
    int condition_id_west;
    int condition_id_top;
    int condition_id_bottom;

    vector<double> xcoord;
    vector<double> ycoord;
    vector<double> zcoord;
    vector<double> weight;
    vector<int> face_type;

    vector<double> xcoord_in;
    vector<double> ycoord_in;
    vector<double> zcoord_in;
    vector<double> weight_in;

    vector<double> *coordinates;

    igCommunicator *communicator;

    function<double(double xi, double eta, double zeta)> x_fun;
    function<double(double xi, double eta, double zeta)> y_fun;
    function<double(double xi, double eta, double zeta)> z_fun;

    function<double(int i, int imax, double xi_min, double xi_max)> distrib_xi_fun;
    function<double(int j, int jmax, double eta_min, double eta_max)> distrib_eta_fun;
    function<double(int k, int kmax, double zeta_min, double zeta_max)> distrib_zeta_fun;

    double xRotate(double x_tmp, double y_tmp);
    double yRotate(double x_tmp, double y_tmp);

};

//
// igMeshGenerator.h ends here
