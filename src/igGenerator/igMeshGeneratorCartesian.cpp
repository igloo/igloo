/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/

#include <iostream>
#include <fstream>
#include <sstream>

#include <igCore/igFittingCurve.h>
#include <igFiles/igFileManager.h>

#include "igMeshGeneratorCartesian.h"


/////////////////////////////////////////////////////////////////////////////

igMeshGeneratorCartesian::igMeshGeneratorCartesian() : igMeshGenerator()
{

}


/////////////////////////////////////////////////////////////////////////////

void igMeshGeneratorCartesian::generate(void)
{

    // read distribution if necessary
    vector<double> x_distrib;
    vector<double> y_distrib;

    if(this->distribution_file){

        ifstream distrib_file_x("distribution_x.dat", ios::in);

        double number_pts_x;
        distrib_file_x >> number_pts_x;
        x_distrib.resize(number_pts_x,0);

        for(int i=0; i<number_pts_x; i++){
            distrib_file_x >> x_distrib.at(i);
        }
        distrib_file_x.close();

        ifstream distrib_file_y("distribution_y.dat", ios::in);

        double number_pts_y;
        distrib_file_y >> number_pts_y;
        y_distrib.resize(number_pts_y,0);

        for(int i=0; i<number_pts_y; i++){
            distrib_file_y >> y_distrib.at(i);
        }
        distrib_file_y.close();

        element_number_1 = (number_pts_x-1)/degree;
        element_number_2 = (number_pts_y-1)/degree;
    }


    // temporary cartesian grid
    vector<double> xcoord_tmp;
    xcoord_tmp.resize((element_number_1*degree+1)*(element_number_2*degree+1)*(element_number_3*degree+1));
    vector<double> ycoord_tmp;
    ycoord_tmp.resize((element_number_1*degree+1)*(element_number_2*degree+1)*(element_number_3*degree+1));
    vector<double> zcoord_tmp;
    zcoord_tmp.resize((element_number_1*degree+1)*(element_number_2*degree+1)*(element_number_3*degree+1));
    vector<double> weight_tmp;
    weight_tmp.resize((element_number_1*degree+1)*(element_number_2*degree+1)*(element_number_3*degree+1), 1.);

    double dxi = (xi_max-xi_min)/(element_number_1*degree);
    double deta = 1.;
    if(element_number_2>0)
        deta = (eta_max-eta_min)/(element_number_2*degree);
    double dzeta = 1.;
    if(element_number_3>0)
        dzeta = (zeta_max-zeta_min)/(element_number_3*degree);

    //-------------- construct grid ---------------

    // case 1D, 2D or 3D ? => 1D by default
    int kmax = 1;
    int z_number = 1;
    int jmax = 1;
    int y_number = 1;

    int face_number = 2;
    int z_face_number = 1;
    int y_face_number = 1;

    if(element_number_2 > 0){ // 2D cases
        jmax = element_number_2*degree+1;
        y_number = element_number_2*(degree+1);
        y_face_number = element_number_2;
        face_number = 4;
    }
    if(element_number_3 > 0){ // 3D cases
        jmax = element_number_2*degree+1;
        y_number = element_number_2*(degree+1);
        y_face_number = element_number_2;
        kmax = element_number_3*degree+1;
        z_number = element_number_3*(degree+1);
        z_face_number = element_number_3;
        face_number = 6;
    }


    for (int k=0; k<kmax; k++){

        double zeta = zeta_min + double(k)*dzeta;

        for (int j=0; j<jmax; j++){

            double eta = eta_min + double(j)*deta;

            for (int i=0; i<element_number_1*degree+1; ++i){

                double xi = xi_min + double(i)*dxi;

                int index = i+ j*(element_number_1*degree+1) + k*(element_number_1*degree+1)*(element_number_2*degree+1);

                if(this->distribution_file){
                    xcoord_tmp.at(index) = x_distrib.at(i);
                    ycoord_tmp.at(index) = y_distrib.at(j);
                    zcoord_tmp.at(index) = zeta;
                } else {
                    xcoord_tmp.at(index) = xi;
                    ycoord_tmp.at(index) = eta;
                    zcoord_tmp.at(index) = zeta;
                }
            }
        }
    }

    //---------------- convert to DG structure --------------------

    xcoord.resize(element_number_1*(degree+1)*y_number*z_number);
    ycoord.resize(element_number_1*(degree+1)*y_number*z_number);
    zcoord.resize(element_number_1*(degree+1)*y_number*z_number);
    weight.resize(element_number_1*(degree+1)*y_number*z_number,1.);

    this->dgMeshConverter(xcoord_tmp, ycoord_tmp, zcoord_tmp, weight_tmp, xcoord, ycoord, zcoord, weight);

    //----------------- face type and BC ------------------

    face_type.resize(element_number_1*y_face_number*z_face_number*face_number,0);

    this->applyBoundaryCondition();

}
