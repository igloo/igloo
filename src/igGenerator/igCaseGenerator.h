/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igGeneratorExport.h>

#include <string>
#include <functional>
#include <vector>

using namespace std;

class igMeshGenerator;
class igSolutionGenerator;

class IGGENERATOR_EXPORT igCaseGenerator
{
public:
    igCaseGenerator(void);
    ~igCaseGenerator(void);

public:
    void setCase(const string& case_name);
    void setElementNumber(int n1, int n2, int n3);

public:
    void generateCase(void);

public:
    igMeshGenerator* meshGenerator(void);
    igSolutionGenerator *solutionGenerator(void);

    function<double(double, double, double)> xFunction(void);
    function<double(double, double, double)> yFunction(void);
    function<double(double, double, double)> zFunction(void);
    function<double(int, int, double, double)> distributionXiFunction(void);
    function<double(int, int, double, double)> distributionEtaFunction(void);
    function<double(int, int, double, double)> distributionZetaFunction(void);
    function<double(double*, double, int, double*, double*)> solutionFunction(void);

    double xiMin(void);
    double xiMax(void);
    double etaMin(void);
    double etaMax(void);
    double zetaMin(void);
    double zetaMax(void);

    int variableNumber(void);
    int derivativeNumber(void);

    int elementNumber1(void);
    int elementNumber2(void);
    int elementNumber3(void);

    int typeSouthBoundary(void);
    int typeEastBoundary(void);
    int typeNorthBoundary(void);
    int typeWestBoundary(void);
    int typeTopBoundary(void);
    int typeBottomBoundary(void);
    int exactSolutionId(void);

    bool enforceSymetry(void);
    double symetryCoordinate(void);

    bool enforceMembrane(void);
    bool distributionFile(void);

    double rotationAngle(void);
    double scaleFactor(void);

    bool periodicBoundary(void);

    vector<double>& xCoordinateInput(void);
    vector<double>& yCoordinateInput(void);
    vector<double>& zCoordinateInput(void);
    vector<double>& weightInput(void);
    int sizeInput(int component);

private:
    string case_name;

    igMeshGenerator *mesh_gen;
    igSolutionGenerator *sol_gen;

    function<double(double, double, double)> x_fun;
    function<double(double, double, double)> y_fun;
    function<double(double, double, double)> z_fun;
    function<double(double *coord, double time, int var_id, double *coord_elt, double *func_param)> sol_fun;
    function<double(int, int, double, double)> distrib_xi_fun;
    function<double(int, int, double, double)> distrib_eta_fun;
    function<double(int, int, double, double)> distrib_zeta_fun;

    double xi_min;
    double xi_max;
    double eta_min;
    double eta_max;
    double zeta_min;
    double zeta_max;

    int variable_number;
    int derivative_number;

    int element_number_1;
    int element_number_2;
    int element_number_3;

    int type_south;
    int type_east;
    int type_north;
    int type_west;
    int type_top;
    int type_bottom;

    int exact_solution_id;
    bool enforce_sym = false;
    bool enforce_membrane = false;
    bool distribution_file;
    double sym_coordinate;

    double scale_factor;

    bool periodic = false;

    vector<int> input_size;
    vector<double> xcoord_in;
    vector<double> ycoord_in;
    vector<double> zcoord_in;
    vector<double> weight_in;

};

//
// igCaseGenerator.h ends here
