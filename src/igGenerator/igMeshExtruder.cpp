/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/

#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>

#include <igCore/igMesh.h>
#include <igCore/igElement.h>
#include <igCore/igCell.h>
#include <igCore/igFace.h>
#include <igCore/igBoundaryIds.h>

#include <igFiles/igFileManager.h>

#include "igMeshExtruder.h"


/////////////////////////////////////////////////////////////////////////////

igMeshExtruder::igMeshExtruder(igMesh *mesh, vector<double> *coordinates, vector<double> *velocities)
{
    this->mesh2D = mesh;
    this->coordinates2D = coordinates;
    this->velocities2D = velocities;

    this->mesh = nullptr;
    this->coordinates = nullptr;
    this->velocities = nullptr;

    this->periodic = false;
    this->morphing_3D = false;

    this->layer_number = 1;
    this->delta_trans = 1;
    this->delta_rot = 0;
    this->delta_scale = 1;
    this->morphing_epsilon = 0;

    this->communicator = nullptr;
}

igMeshExtruder::~igMeshExtruder()
{
    if(this->coordinates)
        delete coordinates;
    if(this->velocities)
        delete velocities;
    if(this->mesh)
        delete mesh;

}

/////////////////////////////////////////////////////////////////////////////

void igMeshExtruder::setLayerNumber(int number)
{
    this->layer_number = number;
}

void igMeshExtruder::setCommunicator(igCommunicator *communicator)
{
    this->communicator = communicator;
}

void igMeshExtruder::setTranslation(double delta_trans)
{
    this->delta_trans = delta_trans;
}

void igMeshExtruder::setRotation(double delta_rot)
{
    this->delta_rot = delta_rot;
}

void igMeshExtruder::setScale(double delta_scale)
{
    this->delta_scale = delta_scale;
}

void igMeshExtruder::enablePeriodicity(void)
{
    this->periodic = true;
}

void igMeshExtruder::enableMorphingPerturbation(double epsilon)
{
	this->morphing_3D = true;
	this->morphing_epsilon = epsilon;
}

/////////////////////////////////////////////////////////////////////////////

igMesh *igMeshExtruder::extrudedMesh(void)
{
    return this->mesh;
}

vector<double> *igMeshExtruder::extrudedVelocities(void)
{
	return this->velocities;
}

/////////////////////////////////////////////////////////////////////////////

void igMeshExtruder::extrude(void)
{
    int degree = mesh2D->element(0)->cellDegree();
    int gauss_pts_direction = mesh2D->element(0)->gaussPointNumberByDirection();
    int variable_number = mesh2D->variableNumber()+1;
    int derivative_number = 0;
    if(mesh2D->derivativeNumber()>0){
        derivative_number = variable_number*3;
    }

    // initialize geometric fields
    int array_size = (degree+1)*(degree+1)*(degree+1)*mesh2D->elementNumber()*layer_number;
    coordinates = new vector<double>;
    coordinates->resize(array_size*4, 0.);
    velocities = new vector<double>;
    velocities->resize(array_size*3, 0.);

    // initialize mesh data
    mesh = new igMesh;
    mesh->setDimension(3);
    mesh->setVariableNumber(variable_number);
    mesh->setDerivativeNumber(derivative_number);
    mesh->setExactSolutionId(0);
    mesh->setCoordinates(this->coordinates);
    mesh->setVelocities(this->velocities);
    mesh->setCommunicator(this->communicator);

    int max_field_number = max(variable_number,derivative_number);
    max_field_number = max(max_field_number,4);


    //-------------------
    //  Elements
    //-------------------

    //---------------- generate 3D elements -----------------------

    for(int ilayer=0; ilayer<layer_number; ilayer++){
        cout << "Extrusion of elements " << ilayer << " in progress ..." << endl;

        int element_counter = 0;

        for(int iel=0; iel<mesh2D->elementNumber(); iel++){

            igElement *new_elt = new igElement;
            new_elt->setCellId(element_counter);
            new_elt->setCellGlobalId(element_counter);
            new_elt->setPartition(0);
            new_elt->setSubdomain(0);
            new_elt->setPhysicalDimension(3);
            new_elt->setParametricDimension(3);
            new_elt->setDegree(degree);
            new_elt->setGaussPointNumberByDirection(gauss_pts_direction);
            new_elt->setVariableNumber(variable_number);
            new_elt->setDerivativeNumber(derivative_number);
            new_elt->setCoordinates(this->coordinates);
            new_elt->setVelocities(this->velocities);

            new_elt->initializeDegreesOfFreedom();

            mesh->addElement(new_elt);

            element_counter++;
        }
    }
    mesh->updateMeshProperties();
    mesh->updateGlobalMeshProperties();

    //----------------- global id setting -------------------

    int global_id_counter = 0;
    for (int ivar=0; ivar<max_field_number; ivar++){
        for (int iel=0; iel < mesh->elementNumber(); iel++){
            igElement *elt = mesh->element(iel);
            for (int ipt=0; ipt<elt->controlPointNumber(); ipt++){
                elt->setGlobalId(ipt, ivar, global_id_counter);
                global_id_counter++;
            }
        }
    }

    //---------------- set element coordinates -------------

    for(int ilayer=0; ilayer<layer_number; ilayer++){
        for(int iel=0; iel<mesh2D->elementNumber(); iel++){

            igElement *elt2D = mesh2D->element(iel);

            int new_elt_index = iel + ilayer*mesh2D->elementNumber();
            igElement *new_elt = mesh->element(new_elt_index);

            for(int islice=0; islice<degree+1; islice++){
                for (int ipt=0; ipt<elt2D->controlPointNumber(); ipt++){

                    int new_ipt= ipt + islice*(degree+1)*(degree+1);

                    double z_value = this->delta_trans/this->layer_number/degree * (ilayer*degree + islice);
                    double angle = this->delta_rot/this->layer_number/degree * (ilayer*degree + islice) /360*M_PI;

                    double scale_coef = double(ilayer*degree + islice)/this->layer_number/degree;
                    if(this->periodic)
                        scale_coef = pow(sin(M_PI*(scale_coef)),2); // for 0 slope extremities
                    double scale = 1 + this->delta_scale*scale_coef;

                    double x_value = coordinates2D->at(elt2D->globalId(ipt, 0));
                    double y_value = coordinates2D->at(elt2D->globalId(ipt, 1));

                    double velocity_factor = 1.;
                    if(morphing_3D){

                    	if( !(islice==0 || islice==degree) ){
                    		velocity_factor += pow(-1,ilayer)*morphing_epsilon;
                    	}

                    }

                    coordinates->at(new_elt->globalId(new_ipt, 0)) = (x_value*cos(angle) - y_value*sin(angle))*scale;
                    coordinates->at(new_elt->globalId(new_ipt, 1)) = (x_value*sin(angle) + y_value*cos(angle))*scale;
                    coordinates->at(new_elt->globalId(new_ipt, 2)) = z_value;
                    coordinates->at(new_elt->globalId(new_ipt, 3)) = coordinates2D->at(elt2D->globalId(ipt, 2));

                    velocities->at(new_elt->globalId(new_ipt, 0)) = velocities2D->at(elt2D->globalId(ipt, 0)) * velocity_factor;
                    velocities->at(new_elt->globalId(new_ipt, 1)) = velocities2D->at(elt2D->globalId(ipt, 1)) * velocity_factor;
                    velocities->at(new_elt->globalId(new_ipt, 2)) = 0.;

                }
            }
            new_elt->initializeGeometry();
            new_elt->setSubdomain(elt2D->subdomain());

        }
    }

    //-------------------
    //  Interior faces
    //-------------------

    int face_counter = 0;
    vector<double> *coord = new vector<double>;
    coord->resize(3);
    cout << endl;

    for(int ilayer=0; ilayer<layer_number; ilayer++){

        cout << "Extrusion of faces " << ilayer << " in progress ..." << endl;

        //--------- generation of faces extruded along z ----------

        for(int ifac=0; ifac<mesh2D->faceNumber(); ifac++){

            igFace *face2D = mesh2D->face(ifac);
            int orientation_left = face2D->leftOrientation();
            int sense_left = face2D->leftSense();
            int orientation_right = face2D->rightOrientation();
            int sense_right = face2D->rightSense();

            igFace *new_face = new igFace;

            new_face->setCellId(face_counter);
            new_face->setCellGlobalId(face_counter);
            new_face->setPhysicalDimension(3);
            new_face->setParametricDimension(2);
            new_face->setDegree(degree);
            new_face->setGaussPointNumberByDirection(gauss_pts_direction);
            new_face->setVariableNumber(variable_number);
            new_face->setDerivativeNumber(derivative_number);
            new_face->setType(id_interior);
            new_face->setLeftPartitionId(0);
            new_face->setRightPartitionId(0);
            new_face->setParameterIntervalLeft(0,1,0,1);
            new_face->setParameterIntervalRight(0,1,0,1);

            new_face->setLeftInside();
            new_face->setRightInside();
            new_face->setCoordinates(this->coordinates);
            new_face->setVelocities(this->velocities);

            new_face->initializeDegreesOfFreedom();

            //------ connectivity -----------------

            int offset;
            int incr;
            bool sign = false;
            if(sense_left<0)
                sign = true;

            switch(orientation_left){
            case 0:
                offset = 0;
                incr = 1;
                if(sign){
                    offset = degree;
                    incr *= -1;
                }
                break;
            case 1:
                offset = degree;
                incr = degree+1;
                if(sign){
                    offset = (degree+1)*(degree+1)-1;
                    incr *= -1;
                }
                break;
            case 2:
                offset = (degree+1)*degree;
                incr = 1;
                if(sign){
                    offset = (degree+1)*(degree+1)-1;
                    incr *= -1;
                }
                break;
            case 3:
                offset = 0;
                incr = degree+1;
                if(sign){
                    offset = (degree+1)*degree;
                    incr *= -1;
                }
                break;
            }

            int elt_left_index = face2D->leftElementIndex() + ilayer*mesh2D->elementNumber();
            igElement *elt_left = mesh->element(elt_left_index);

            new_face->setLeftElementIndex(elt_left_index);
            new_face->setOrientationLeft(orientation_left);
            new_face->setSenseLeft(sense_left);
            for(int islice=0; islice<degree+1; islice++){
                for (int idof=0; idof<face2D->controlPointNumber(); idof++){
                    for (int ivar=0; ivar<max_field_number; ivar++){
                        new_face->setLeftDofMap(idof+islice*(degree+1),ivar,elt_left->globalId(offset+idof*incr+islice*(degree+1)*(degree+1),ivar));
                    }
                    new_face->setLeftPointMap(idof+islice*(degree+1),elt_left->globalId(offset+idof*incr+islice*(degree+1)*(degree+1),0));
                }
            }

            sign = false;
            if(sense_right<0)
                sign = true;

            switch(orientation_right){
            case 0:
                offset = 0;
                incr = 1;
                if(sign){
                    offset = degree;
                    incr *= -1;
                }
                break;
            case 1:
                offset = degree;
                incr = degree+1;
                if(sign){
                    offset = (degree+1)*(degree+1)-1;
                    incr *= -1;
                }
                break;
            case 2:
                offset = (degree+1)*degree;
                incr = 1;
                if(sign){
                    offset = (degree+1)*(degree+1)-1;
                    incr *= -1;
                }
                break;
            case 3:
                offset = 0;
                incr = degree+1;
                if(sign){
                    offset = (degree+1)*degree;
                    incr *= -1;
                }
                break;
            }

            int elt_right_index = face2D->rightElementIndex() + ilayer*mesh2D->elementNumber();
            igElement *elt_right = mesh->element(elt_right_index);

            new_face->setRightElementIndex(elt_right_index);
            new_face->setOrientationRight(orientation_right);
            new_face->setSenseRight(sense_right);
            for(int islice=0; islice<degree+1; islice++){
                for (int idof=0; idof<face2D->controlPointNumber(); idof++){
                    for (int ivar=0; ivar<max_field_number; ivar++){
                        new_face->setRightDofMap(idof+islice*(degree+1),ivar,elt_right->globalId(offset+idof*incr+islice*(degree+1)*(degree+1),ivar));
                    }
                    new_face->setRightPointMap(idof+islice*(degree+1),elt_right->globalId(offset+idof*incr+islice*(degree+1)*(degree+1),0));
                }
            }

            for (int idim=0; idim<3; idim++)
                coord->at(idim) = elt_left->barycenter(idim);

            new_face->initializeGeometry();
            new_face->initializeNormal(coord);

            // storage in the face list
            mesh->addFace(new_face);

            face_counter++;

        }


        //--------- generation of top faces ----------

        if(ilayer != layer_number-1){ // except for the last layer
            for(int iel2D=0; iel2D<mesh2D->elementNumber(); iel2D++){

                igElement *elt2D = mesh2D->element(iel2D);

                igFace *new_face = new igFace;

                new_face->setCellId(face_counter);
                new_face->setCellGlobalId(face_counter);
                new_face->setPhysicalDimension(3);
                new_face->setParametricDimension(2);
                new_face->setDegree(degree);
                new_face->setGaussPointNumberByDirection(gauss_pts_direction);
                new_face->setVariableNumber(variable_number);
                new_face->setDerivativeNumber(derivative_number);
                new_face->setType(id_interior);
                new_face->setLeftPartitionId(0);
                new_face->setRightPartitionId(0);
                new_face->setParameterIntervalLeft(0,1,0,1);
                new_face->setParameterIntervalRight(0,1,0,1);

                new_face->setLeftInside();
                new_face->setRightInside();
                new_face->setCoordinates(this->coordinates);
                new_face->setVelocities(this->velocities);

                new_face->initializeDegreesOfFreedom();

                //------ connectivity -----------------

                int elt_left_index = iel2D + ilayer*mesh2D->elementNumber();
                igElement *elt_left = mesh->element(elt_left_index);

                new_face->setLeftElementIndex(elt_left_index);
                new_face->setOrientationLeft(5);
                new_face->setSenseLeft(1);

                for (int idof=0; idof<elt2D->controlPointNumber(); idof++){
                    for (int ivar=0; ivar<max_field_number; ivar++){
                        new_face->setLeftDofMap(idof,ivar,elt_left->globalId(idof+(degree+1)*(degree+1)*degree,ivar));
                    }
                    new_face->setLeftPointMap(idof,elt_left->globalId(idof+(degree+1)*(degree+1)*degree,0));
                }

                int elt_right_index = iel2D + (ilayer+1)*mesh2D->elementNumber();
                igElement *elt_right = mesh->element(elt_right_index);

                new_face->setRightElementIndex(elt_right_index);
                new_face->setOrientationRight(4);
                new_face->setSenseRight(1);

                for (int idof=0; idof<elt2D->controlPointNumber(); idof++){
                    for (int ivar=0; ivar<max_field_number; ivar++){
                        new_face->setRightDofMap(idof,ivar,elt_right->globalId(idof,ivar));
                    }
                    new_face->setRightPointMap(idof,elt_right->globalId(idof,0));
                }

                for (int idim=0; idim<3; idim++)
                    coord->at(idim) = elt_left->barycenter(idim);

                new_face->initializeGeometry();
                new_face->initializeNormal(coord);

                // storage in the face list
                mesh->addFace(new_face);

                face_counter++;

            }
        }

    }

    //-------------------
    //  Boundary faces
    //-------------------

    int boundary_face_counter = 0;
    cout << endl;

    for(int ilayer=0; ilayer<layer_number; ilayer++){

        cout << "Extrusion of boundaries " << ilayer << " in progress ..." << endl;

        //--------- generation of faces extruded along z ----------

        for(int ifac=0; ifac<mesh2D->boundaryFaceNumber(); ifac++){

            igFace *face2D = mesh2D->boundaryFace(ifac);
            int orientation_left = face2D->leftOrientation();
            int face_type = face2D->type();

            igFace *new_face = new igFace;

            new_face->setCellId(boundary_face_counter);
            new_face->setCellGlobalId(boundary_face_counter);
            new_face->setPhysicalDimension(3);
            new_face->setParametricDimension(2);
            new_face->setDegree(degree);
            new_face->setGaussPointNumberByDirection(gauss_pts_direction);
            new_face->setVariableNumber(variable_number);
            new_face->setDerivativeNumber(derivative_number);
            new_face->setType(face_type);
            new_face->setLeftPartitionId(0);
            new_face->setParameterIntervalLeft(0,1,0,1);
            new_face->setCoordinates(this->coordinates);
            new_face->setVelocities(this->velocities);
            new_face->setLeftInside();

            new_face->initializeDegreesOfFreedom();

            //------ connectivity -----------------

            int offset;
            int incr;

            switch(orientation_left){
            case 0:
                offset = 0;
                incr = 1;
                break;
            case 1:
                offset = degree;
                incr = degree+1;
                break;
            case 2:
                offset = (degree+1)*degree;
                incr = 1;
                break;
            case 3:
                offset = 0;
                incr = degree+1;
                break;
            }

            int elt_left_index = face2D->leftElementIndex() + ilayer*mesh2D->elementNumber();
            igElement *elt_left = mesh->element(elt_left_index);

            new_face->setLeftElementIndex(elt_left_index);
            new_face->setOrientationLeft(orientation_left);
            new_face->setSenseLeft(1);

            for(int islice=0; islice<degree+1; islice++){
                for (int idof=0; idof<face2D->controlPointNumber(); idof++){
                    for (int ivar=0; ivar<max_field_number; ivar++){
                        new_face->setLeftDofMap(idof+islice*(degree+1),ivar,elt_left->globalId(offset+idof*incr+islice*(degree+1)*(degree+1),ivar));
                    }
                    new_face->setLeftPointMap(idof,elt_left->globalId(offset+idof*incr+islice*(degree+1)*(degree+1),0));
                }
            }

            for (int idim=0; idim<3; idim++)
                coord->at(idim) = elt_left->barycenter(idim);

            new_face->initializeGeometry();
            new_face->initializeNormal(coord);

            // storage in the face list
            mesh->addBoundaryFace(new_face);

            boundary_face_counter++;

        }
    }


    //--------- generation of bottom faces, except for periodicity ----------

    if(!this->periodic){

        for(int iel2D=0; iel2D<mesh2D->elementNumber(); iel2D++){

            igElement *elt2D = mesh2D->element(iel2D);

            igFace *new_face = new igFace;

            new_face->setCellId(boundary_face_counter);
            new_face->setCellGlobalId(boundary_face_counter);
            new_face->setPhysicalDimension(3);
            new_face->setParametricDimension(2);
            new_face->setDegree(degree);
            new_face->setGaussPointNumberByDirection(gauss_pts_direction);
            new_face->setVariableNumber(variable_number);
            new_face->setDerivativeNumber(derivative_number);
            new_face->setType(id_slip_wall); // by default slip wall condition
            new_face->setLeftPartitionId(0);
            new_face->setParameterIntervalLeft(0,1,0,1);
            new_face->setLeftInside();
            new_face->setCoordinates(this->coordinates);
            new_face->setVelocities(this->velocities);

            new_face->initializeDegreesOfFreedom();

            //------ connectivity -----------------

            int elt_left_index = iel2D;
            igElement *elt_left = mesh->element(elt_left_index);

            new_face->setLeftElementIndex(elt_left_index);
            new_face->setOrientationLeft(4);
            new_face->setSenseLeft(1);

            for (int idof=0; idof<elt2D->controlPointNumber(); idof++){
                for (int ivar=0; ivar<max_field_number; ivar++){
                    new_face->setLeftDofMap(idof,ivar,elt_left->globalId(idof,ivar));
                }
                new_face->setLeftPointMap(idof, elt_left->globalId(idof,0));
            }

            for (int idim=0; idim<3; idim++)
                coord->at(idim) = elt_left->barycenter(idim);

            new_face->initializeGeometry();
            new_face->initializeNormal(coord);

            // storage in the face list
            mesh->addBoundaryFace(new_face);

            boundary_face_counter++;

        }

    //--------- generation of top faces, except for periodicity ----------

        for(int iel2D=0; iel2D<mesh2D->elementNumber(); iel2D++){

            igElement *elt2D = mesh2D->element(iel2D);

            igFace *new_face = new igFace;

            new_face->setCellId(boundary_face_counter);
            new_face->setCellGlobalId(boundary_face_counter);
            new_face->setPhysicalDimension(3);
            new_face->setParametricDimension(2);
            new_face->setDegree(degree);
            new_face->setGaussPointNumberByDirection(gauss_pts_direction);
            new_face->setVariableNumber(variable_number);
            new_face->setDerivativeNumber(derivative_number);
            new_face->setType(id_slip_wall);  // by default slip wall condition
            new_face->setLeftPartitionId(0);
            new_face->setParameterIntervalLeft(0,1,0,1);
            new_face->setLeftInside();
            new_face->setCoordinates(this->coordinates);
            new_face->setVelocities(this->velocities);

            new_face->initializeDegreesOfFreedom();

            //------ connectivity -----------------

            int elt_left_index = iel2D + (layer_number-1)*mesh2D->elementNumber();
            igElement *elt_left = mesh->element(elt_left_index);

            new_face->setLeftElementIndex(elt_left_index);
            new_face->setOrientationLeft(5);
            new_face->setSenseLeft(1);

            for (int idof=0; idof<elt2D->controlPointNumber(); idof++){
                for (int ivar=0; ivar<max_field_number; ivar++){
                    new_face->setLeftDofMap(idof,ivar,elt_left->globalId(idof+(degree+1)*(degree+1)*degree,ivar));
                }
                new_face->setLeftPointMap(idof, elt_left->globalId(idof+(degree+1)*(degree+1)*degree,0));
            }

            for (int idim=0; idim<3; idim++)
                coord->at(idim) = elt_left->barycenter(idim);

            new_face->initializeGeometry();
            new_face->initializeNormal(coord);

            // storage in the face list
            mesh->addBoundaryFace(new_face);

            boundary_face_counter++;

        }

    // case of periodicity : add interior faces at top
    } else {

        for(int iel2D=0; iel2D<mesh2D->elementNumber(); iel2D++){

            igElement *elt2D = mesh2D->element(iel2D);

            igFace *new_face = new igFace;

            new_face->setCellId(face_counter);
            new_face->setCellGlobalId(face_counter);
            new_face->setPhysicalDimension(3);
            new_face->setParametricDimension(2);
            new_face->setDegree(degree);
            new_face->setGaussPointNumberByDirection(gauss_pts_direction);
            new_face->setVariableNumber(variable_number);
            new_face->setDerivativeNumber(derivative_number);
            new_face->setType(id_interior);
            new_face->setLeftPartitionId(0);
            new_face->setRightPartitionId(0);
            new_face->setParameterIntervalLeft(0,1,0,1);
            new_face->setParameterIntervalRight(0,1,0,1);

            new_face->setLeftInside();
            new_face->setRightInside();
            new_face->setCoordinates(this->coordinates);
            new_face->setVelocities(this->velocities);

            new_face->initializeDegreesOfFreedom();

            //------ connectivity -----------------

            int elt_left_index = iel2D + (layer_number-1)*mesh2D->elementNumber();
            igElement *elt_left = mesh->element(elt_left_index);

            new_face->setLeftElementIndex(elt_left_index);
            new_face->setOrientationLeft(5);
            new_face->setSenseLeft(1);

            for (int idof=0; idof<elt2D->controlPointNumber(); idof++){
                for (int ivar=0; ivar<max_field_number; ivar++){
                    new_face->setLeftDofMap(idof,ivar,elt_left->globalId(idof+(degree+1)*(degree+1)*degree,ivar));
                }
                new_face->setLeftPointMap(idof,elt_left->globalId(idof+(degree+1)*(degree+1)*degree,0));
            }

            int elt_right_index = iel2D;
            igElement *elt_right = mesh->element(elt_right_index);

            new_face->setRightElementIndex(elt_right_index);
            new_face->setOrientationRight(4);
            new_face->setSenseRight(1);

            for (int idof=0; idof<elt2D->controlPointNumber(); idof++){
                for (int ivar=0; ivar<max_field_number; ivar++){
                    new_face->setRightDofMap(idof,ivar,elt_right->globalId(idof,ivar));
                }
                new_face->setRightPointMap(idof,elt_right->globalId(idof,0));
            }

            for (int idim=0; idim<3; idim++)
                coord->at(idim) = elt_left->barycenter(idim);

            new_face->initializeGeometry();
            new_face->initializeNormal(coord);

            // storage in the face list
            mesh->addFace(new_face);

            face_counter++;

        }

    }

    mesh->updateMeshProperties();
}


/////////////////////////////////////////////////////////////////////////////////////
