/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <vector>
#include <math.h>

using namespace std;

// deformation according to piston move
void pistonDeformation(double time, double time_step, vector<double> *coord, vector<double> *vel, vector<double> *param, const vector<double> *init, const int subdomain_number)
{
    // get deformation parameters
    double damping = param->at(0); // damping coefficient
    double piston_velocity = param->at(1); // velocity along x
    double piston_displacement = param->at(2); // translation along x

    // deformation law
    coord->at(0) = init->at(0) + damping * piston_displacement;
    coord->at(1) = init->at(1);

    vel->at(0) = damping * piston_velocity;
    vel->at(1) = 0;

}
