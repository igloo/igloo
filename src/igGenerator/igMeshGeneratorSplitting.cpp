/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include <math.h>
#include <iostream>
#include <fstream>

#include "igMeshGeneratorSplitting.h"

/////////////////////////////////////////////////////////////////////////////

igMeshGeneratorSplitting::igMeshGeneratorSplitting() : igMeshGenerator()
{

}

igMeshGeneratorSplitting::~igMeshGeneratorSplitting(void)
{

}

/////////////////////////////////////////////////////////////////////////////

void igMeshGeneratorSplitting::generate(void)
{

    vector<double> xcoord_tmp2;
    vector<double> ycoord_tmp2;
    vector<double> weight_tmp2;

    // temporary data for splitting
    vector<double> *coord_tmp_x = new vector<double>;
    coord_tmp_x->resize(degree+1);
    vector<double> *coord_tmp_y = new vector<double>;
    coord_tmp_y->resize(degree+1);
    vector<double> *coord_tmp_w = new vector<double>;
    coord_tmp_w->resize(degree+1);

    vector<double> *coord_curve1_x = new vector<double>;
    coord_curve1_x->resize(degree+1);
    vector<double> *coord_curve1_y= new vector<double>;
    coord_curve1_y->resize(degree+1);
    vector<double> *coord_curve1_w = new vector<double>;
    coord_curve1_w->resize(degree+1);

    vector<double> *coord_curve2_x = new vector<double>;
    coord_curve2_x->resize(degree+1);
    vector<double> *coord_curve2_y = new vector<double>;
    coord_curve2_y->resize(degree+1);
    vector<double> *coord_curve2_w = new vector<double>;
    coord_curve2_w->resize(degree+1);

    int element_number_1_curr = this->element_number_1_in;
    int element_number_2_curr = this->element_number_2_in;


    // refinement steps for the first direction

    while(element_number_1_curr < element_number_1){

        xcoord_tmp2.resize((2*element_number_1_curr*degree+1)*(element_number_2_curr*degree+1));
        ycoord_tmp2.resize((2*element_number_1_curr*degree+1)*(element_number_2_curr*degree+1));
        weight_tmp2.resize((2*element_number_1_curr*degree+1)*(element_number_2_curr*degree+1));

        // loop over elements in second direction
        for (int iel2 = 0; iel2<element_number_2_curr; iel2++){

            // loop over elements in first direction
            for (int iel1 = 0; iel1<element_number_1_curr; iel1++){

                // loop over xi-lines
                for (int icurve=0; icurve<degree+1; icurve++){

                    //int offset = iel1*degree + (element_number_1_curr*degree+1)*icurve;

                    int offset = iel1*degree + (element_number_1_curr*degree+1)*degree*iel2
                            + (element_number_1_curr*degree+1)*icurve;

                    // retrieve curve control points + weights in 3D space !
                    for(int ipt=0; ipt<degree+1; ipt++){
                        coord_tmp_x->at(ipt) = xcoord_in.at(offset + ipt) * weight_in.at(offset + ipt);
                        coord_tmp_y->at(ipt) = ycoord_in.at(offset + ipt) * weight_in.at(offset + ipt);
                        coord_tmp_w->at(ipt) = weight_in.at(offset + ipt);
                    }

                    // first and last points
                    coord_curve1_x->at(0) = coord_tmp_x->at(0);
                    coord_curve1_y->at(0) = coord_tmp_y->at(0);
                    coord_curve1_w->at(0) = coord_tmp_w->at(0);
                    coord_curve2_x->at(degree) = coord_tmp_x->at(degree);
                    coord_curve2_y->at(degree) = coord_tmp_y->at(degree);
                    coord_curve2_w->at(degree) = coord_tmp_w->at(degree);

                    // subdivision procedure
                    for (int istep=0; istep<degree; istep++){

                        for (int ipt=0; ipt<degree-istep; ipt++){
                            coord_tmp_x->at(ipt) = 0.5*coord_tmp_x->at(ipt) + 0.5*coord_tmp_x->at(ipt+1);
                            coord_tmp_y->at(ipt) = 0.5*coord_tmp_y->at(ipt) + 0.5*coord_tmp_y->at(ipt+1);
                            coord_tmp_w->at(ipt) = 0.5*coord_tmp_w->at(ipt) + 0.5*coord_tmp_w->at(ipt+1);
                        }
                        coord_curve1_x->at(1+istep) = coord_tmp_x->at(0);
                        coord_curve1_y->at(1+istep) = coord_tmp_y->at(0);
                        coord_curve1_w->at(1+istep) = coord_tmp_w->at(0);
                        coord_curve2_x->at(degree-1-istep) = coord_tmp_x->at(degree-1-istep);
                        coord_curve2_y->at(degree-1-istep) = coord_tmp_y->at(degree-1-istep);
                        coord_curve2_w->at(degree-1-istep) = coord_tmp_w->at(degree-1-istep);

                    }

                    // store control points
                    //int offset_new = 2*iel1*degree + (2*element_number_1_curr*degree+1)*icurve;
                    int offset_new = 2*iel1*degree + (2*element_number_1_curr*degree+1)*degree*iel2 + (2*element_number_1_curr*degree+1)*icurve;

                    for (int ipt=0; ipt<degree+1; ipt++){
                        xcoord_tmp2.at(offset_new + ipt) = coord_curve1_x->at(ipt) / coord_curve1_w->at(ipt);
                        ycoord_tmp2.at(offset_new + ipt) = coord_curve1_y->at(ipt) / coord_curve1_w->at(ipt);
                        weight_tmp2.at(offset_new + ipt) = coord_curve1_w->at(ipt);
                        xcoord_tmp2.at(offset_new + ipt + degree) = coord_curve2_x->at(ipt) / coord_curve2_w->at(ipt);
                        ycoord_tmp2.at(offset_new + ipt + degree) = coord_curve2_y->at(ipt) / coord_curve2_w->at(ipt);
                        weight_tmp2.at(offset_new + ipt + degree) = coord_curve2_w->at(ipt);
                    }

                }

            }
        }
        // copy result : back to 2D space !
        int new_size = xcoord_tmp2.size();
        xcoord_in.resize(new_size);
        ycoord_in.resize(new_size);
        weight_in.resize(new_size);

        for (int idof=0; idof<new_size; idof++){
            xcoord_in.at(idof) = xcoord_tmp2.at(idof);
            ycoord_in.at(idof) = ycoord_tmp2.at(idof);
            weight_in.at(idof) = weight_tmp2.at(idof);
        }
        element_number_1_curr *=2;

//        cout << "refinement step direction 1" << endl;
//        cout << "element1 number: " << element_number_1_curr << endl;
//        cout << endl;

    }


    // refinement steps for the second direction

    while(element_number_2_curr < element_number_2){
        xcoord_tmp2.resize((element_number_1_curr*degree+1)*(2*element_number_2_curr*degree+1));
        ycoord_tmp2.resize((element_number_1_curr*degree+1)*(2*element_number_2_curr*degree+1));
        weight_tmp2.resize((element_number_1_curr*degree+1)*(2*element_number_2_curr*degree+1));

        // loop over elements in second direction
        for (int iel2 = 0; iel2<element_number_2_curr; iel2++){

            // loop over elements in first direction
            for (int iel1 = 0; iel1<element_number_1_curr; iel1++){

                // loop over eta-lines
                for (int icurve=0; icurve<degree+1; icurve++){

                    int offset = iel1*degree + (element_number_1_curr*degree+1)*degree*iel2 + icurve;
                    int delta = (element_number_1_curr*degree+1);

                    // retrieve curve control points + weights in 3D space !
                    for(int ipt=0; ipt<degree+1; ipt++){
                        coord_tmp_x->at(ipt) = xcoord_in.at(offset + ipt*delta) * weight_in.at(offset + ipt*delta);
                        coord_tmp_y->at(ipt) = ycoord_in.at(offset + ipt*delta) * weight_in.at(offset + ipt*delta);
                        coord_tmp_w->at(ipt) = weight_in.at(offset + ipt*delta);
                    }

                    // first and last points
                    coord_curve1_x->at(0) = coord_tmp_x->at(0);
                    coord_curve1_y->at(0) = coord_tmp_y->at(0);
                    coord_curve1_w->at(0) = coord_tmp_w->at(0);
                    coord_curve2_x->at(degree) = coord_tmp_x->at(degree);
                    coord_curve2_y->at(degree) = coord_tmp_y->at(degree);
                    coord_curve2_w->at(degree) = coord_tmp_w->at(degree);

                    // subdivision procedure
                    for (int istep=0; istep<degree; istep++){

                        for (int ipt=0; ipt<degree-istep; ipt++){
                            coord_tmp_x->at(ipt) = 0.5*coord_tmp_x->at(ipt) + 0.5*coord_tmp_x->at(ipt+1);
                            coord_tmp_y->at(ipt) = 0.5*coord_tmp_y->at(ipt) + 0.5*coord_tmp_y->at(ipt+1);
                            coord_tmp_w->at(ipt) = 0.5*coord_tmp_w->at(ipt) + 0.5*coord_tmp_w->at(ipt+1);
                        }
                        coord_curve1_x->at(1+istep) = coord_tmp_x->at(0);
                        coord_curve1_y->at(1+istep) = coord_tmp_y->at(0);
                        coord_curve1_w->at(1+istep) = coord_tmp_w->at(0);
                        coord_curve2_x->at(degree-1-istep) = coord_tmp_x->at(degree-1-istep);
                        coord_curve2_y->at(degree-1-istep) = coord_tmp_y->at(degree-1-istep);
                        coord_curve2_w->at(degree-1-istep) = coord_tmp_w->at(degree-1-istep);

                    }

                    // store control points
                    int offset_new = iel1*degree + (element_number_1_curr*degree+1)*degree*2*iel2 + icurve;

                    for (int ipt=0; ipt<degree+1; ipt++){
                        xcoord_tmp2.at(offset_new + ipt*delta) = coord_curve1_x->at(ipt) / coord_curve1_w->at(ipt);
                        ycoord_tmp2.at(offset_new + ipt*delta) = coord_curve1_y->at(ipt) / coord_curve1_w->at(ipt);
                        weight_tmp2.at(offset_new + ipt*delta) = coord_curve1_w->at(ipt);
                        xcoord_tmp2.at(offset_new + ipt*delta + (element_number_1_curr*degree+1)*degree) = coord_curve2_x->at(ipt) / coord_curve2_w->at(ipt);
                        ycoord_tmp2.at(offset_new + ipt*delta + (element_number_1_curr*degree+1)*degree) = coord_curve2_y->at(ipt) / coord_curve2_w->at(ipt);
                        weight_tmp2.at(offset_new + ipt*delta + (element_number_1_curr*degree+1)*degree) = coord_curve2_w->at(ipt);
                    }

                    // re-parameterization of weights by applying "conic shape factors" cf NURBS book
                    double new_weight = weight_tmp2.at(offset_new + 1*delta)/sqrt(weight_tmp2.at(offset_new + 2*delta));
                    weight_tmp2.at(offset_new + 1*delta) = new_weight;
                    weight_tmp2.at(offset_new + 2*delta) = 1.;
                    weight_tmp2.at(offset_new + 1*delta + (element_number_1_curr*degree+1)*degree) = new_weight;
                    weight_tmp2.at(offset_new + 2*delta + (element_number_1_curr*degree+1)*degree) = 1.;
                }

            }
        }

        // copy result : back to 2D space !
        int new_size = xcoord_tmp2.size();
        xcoord_in.resize(new_size);
        ycoord_in.resize(new_size);
        zcoord_in.resize(new_size);
        weight_in.resize(new_size);
        for (int idof=0; idof<new_size; idof++){
            xcoord_in.at(idof) = xcoord_tmp2.at(idof);
            ycoord_in.at(idof) = ycoord_tmp2.at(idof);
            zcoord_in.at(idof) = 0.;
            weight_in.at(idof) = weight_tmp2.at(idof);
        }
        element_number_2_curr *=2;

//        cout << "refinement step direction 2" << endl;
//        cout << "element2 number: " << element_number_2_curr << endl;
//        cout << endl;

    }

    //---------------- convert to DG structure --------------------

    xcoord.resize(element_number_1*element_number_2*(degree+1)*(degree+1));
    ycoord.resize(element_number_1*element_number_2*(degree+1)*(degree+1));
    zcoord.resize(element_number_1*element_number_2*(degree+1)*(degree+1));
    weight.resize(element_number_1*element_number_2*(degree+1)*(degree+1),1.);

    this->dgMeshConverter(xcoord_in, ycoord_in, zcoord_in, weight_in, xcoord, ycoord, zcoord, weight);


    //----------------- apply default face type ------------------

    face_type.resize(element_number_1*element_number_2*4,0);

    this->applyBoundaryCondition();

    delete coord_tmp_x;
    delete coord_tmp_y;
    delete coord_tmp_w;
    delete coord_curve1_x;
    delete coord_curve1_y;
    delete coord_curve1_w;
    delete coord_curve2_x;
    delete coord_curve2_y;
    delete coord_curve2_w;
}
