/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <math.h>

using namespace std;

/////////////////////////////////////////////////////////

double x_id(double xi, double eta, double zeta){
    return xi;
}

double y_id(double xi, double eta, double zeta){
    return eta;
}

double z_id(double xi, double eta, double zeta){
    return zeta;
}

/////////////////////////////////////////////////////////

double y_bump_exp(double xi, double eta, double zeta){
    return eta + 0.0625*exp(-25.*xi*xi) * (1.-eta/0.8);
}

double y_bump_exp_3D(double xi, double eta, double zeta){
    return (1. + eta + 0.0625*exp(-25.*xi*xi)*(1.-eta))*cos(M_PI/2.*zeta);
}

double z_bump_exp_3D(double xi, double eta, double zeta){
    return (1. + eta + 0.0625*exp(-25.*xi*xi)*(1.-eta))*sin(M_PI/2.*zeta);
}

/////////////////////////////////////////////////////////

double y_pipe(double xi, double eta, double zeta){
    return eta + 0.8*tanh(-xi) * (1. - eta/2.);
}

/////////////////////////////////////////////////////////

double y_bump_cos(double xi, double eta, double zeta){

    double y = eta;
    if(xi>1 && xi<3)
        y += 0.1*(1.-cos((xi-1.)*M_PI)) * (1. - eta);
    return y;
}

/////////////////////////////////////////////////////////

double y_bump_cubic(double xi, double eta, double zeta){

    double height = 4.;
    double h = 0.2;

    double y = -height;

    if (xi >= 0 && xi <= 1){
        y += h*xi*xi*xi + 3.*h*xi*xi*(1.-xi);
    } else if (xi > 1 && xi <= 2){
        xi = (2.-xi);
        y +=  h*xi*xi*xi + 3.*h*xi*xi*(1.-xi);
    }

    return y * (1. - eta);
}

/////////////////////////////////////////////////////////

double r0 = 1.;
double r1 = 4.;

double x_circle(double xi, double eta, double zeta){
    return (r0+(r1-r0)*xi)*cos(0.5*M_PI*eta);
}

double y_circle(double xi, double eta, double zeta){
    return (r0+(r1-r0)*xi)*sin(0.5*M_PI*eta);
}

/////////////////////////////////////////////////////////

double xi0 = -0.06473;
double c = 0.8978307;

double x_naca(double r, double theta, double zeta){

    double alpha = c*c/( (xi0+r*cos(2*M_PI*theta))*(xi0+r*cos(2*M_PI*theta)) +r*r*sin(2*M_PI*theta)*sin(2*M_PI*theta));
    double span_ratio = 1.-0.1*zeta;

    return (xi0 + r*cos(2*M_PI*theta))*(1+alpha) * span_ratio;
}

double y_naca(double r, double theta, double zeta){

    double alpha = c*c/( (xi0+r*cos(2*M_PI*theta))*(xi0+r*cos(2*M_PI*theta)) +r*r*sin(2*M_PI*theta)*sin(2*M_PI*theta));
    double span_ratio = 1.-0.1*zeta;

    return r*sin(2*M_PI*theta)*(1-alpha) * span_ratio;
}

/////////////////////////////////////////////////////////

double x_cylinder(double r, double theta, double zeta){
    return (1.+r)*cos(2*M_PI*theta);
}

double y_cylinder(double r, double theta, double zeta){
    return (1.+r)*sin(2*M_PI*theta);
}


double x_cylinder_bump(double r, double theta, double zeta){
  return r*(1.+0.1*cos(16*M_PI*theta)*(1-(r-1)/3))*cos(2*M_PI*theta);
}

double y_cylinder_bump(double r, double theta, double zeta){
  return r*(1.+0.1*cos(16*M_PI*theta)*(1-(r-1)/3))*sin(2*M_PI*theta);
}

///////////////////////////////////////////////////////////

double z_tube(double h, double theta, double r){
    double coef_h;
    if(h<1.)
        coef_h = 0.;
    else if(h<2.)
        coef_h = sin(0.5*M_PI*(h-1.));
    else if(h<4.)
        coef_h = 1.;
    else if(h<5.)
        coef_h = sin(0.5*M_PI*(h-3.));
    else
        coef_h = 0.;

    return (r+0.95*pow(cos(4*M_PI*theta),8)*(1-(r-1))*coef_h)*cos(2*M_PI*(theta-h/6));
}

double y_tube(double h, double theta, double r){
    double coef_h;
    if(h<1.)
        coef_h = 0.;
    else if(h<2.)
        coef_h = sin(0.5*M_PI*(h-1.));
    else if(h<4.)
        coef_h = 1.;
    else if(h<5.)
        coef_h = sin(0.5*M_PI*(h-3.));
    else
        coef_h = 0.;

    return (r+0.95*pow(cos(4*M_PI*theta),8)*(1-(r-1))*coef_h)*sin(2*M_PI*(theta-h/6));
}

double x_tube(double h, double theta, double r){
  return h;
}
