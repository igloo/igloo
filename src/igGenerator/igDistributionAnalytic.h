/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/

#include <math.h>

using namespace std;

double distrib_eta_id_fun(int j, int jmax, double eta_min, double eta_max)
{
    double deta = (eta_max-eta_min)/jmax;
    return eta_min + deta*j;
}

double distrib_xi_id_fun(int i, int imax, double xi_min, double xi_max)
{
    double dxi = (xi_max-xi_min)/imax;
    return xi_min + dxi*i;
}

double distrib_zeta_id_fun(int k, int kmax, double zeta_min, double zeta_max)
{
    double dzeta = (zeta_max-zeta_min)/kmax;
    return zeta_min + dzeta*k;
}

/////////////////////////////////////////////////////////////////

double distrib_eta_plate_fun(int j, int jmax, double eta_min, double eta_max)
{
    double wall_coeff = 1.07;
    double value = eta_max * (1.-pow(wall_coeff,j))/(1.-pow(wall_coeff,jmax-1));
    return value;
}

double distrib_xi_plate_fun(int i, int imax, double eta_max, double xi_max)
{
    double wall_coeff = 1.07;
    double value;
    int iref = imax/4;
    if(i<iref)
        value = -xi_max * (1.-pow(wall_coeff,iref-i))/(1.-pow(wall_coeff,imax-iref));
    else
        value = xi_max * (1.-pow(wall_coeff,i-iref))/(1.-pow(wall_coeff,imax-iref));
    return value;
}

/////////////////////////////////////////////////////////////////

double distrib_eta_membrane_fun(int j, int jmax, double eta_min, double eta_max)
{
    double wall_coeff = 1.3;
    double value;

    if(j == jmax/2){
        value = 0;
    }
    else if(j > jmax/2){
        value = (eta_max-eta_min)/2 * (1.-pow(wall_coeff,j-jmax/2))/(1.-pow(wall_coeff,jmax/2));
    } else {
        value = -(eta_max-eta_min)/2 * (1.-pow(wall_coeff,jmax/2-j))/(1.-pow(wall_coeff,jmax/2));
    }
    return value;
}

double distrib_xi_membrane_fun(int i, int imax, double xi_min, double xi_max)
{
    double wall_coeff = 1.2;
    double value;
    int iref_le = 3*imax/7;
    int iref_te = 4*imax/7;

    if (i<iref_le){
        value = -(xi_max-xi_min-1)/2 * (1.-pow(wall_coeff,iref_le-i))/(1.-pow(wall_coeff,iref_le));
    } else if (iref_le<=i && i<=iref_te){
        value = double(i-iref_le)/double(iref_te-iref_le);
    } else {
        value = 1 + (xi_max-xi_min-1)/2 * (1.-pow(wall_coeff,i-iref_te))/(1.-pow(wall_coeff,iref_le));
    }
    return value;
}

/////////////////////////////////////////////////////////////////

double distrib_xi_naca_fun(int i, int imax, double xi_min, double xi_max)
{
    double wall_coeff = 1.5;
    double value = 1. + 0.5 * (1.-pow(wall_coeff,i))/(1.-wall_coeff);
    return value;
}

////////////////////////////////////////////////////////////////////

double distrib_xi_cylinder_fun(int i, int imax, double xi_min, double xi_max)
{
    double wall_coeff = 1.15;
    double value = 0.05 * (1.-pow(wall_coeff,i))/(1.-wall_coeff);
    return value;
}

double distrib_eta_cylinder_fun(int j, int jmax, double eta_min, double eta_max)
{
    double eta = eta_min + (eta_max-eta_min)/jmax*j;

    double coeff1 = 0.1;
    double coeff2 = 0.9;
    double eta_new = coeff1*3.*eta*(1.-eta)*(1.-eta) + coeff2*3.*eta*eta*(1.-eta) + eta*eta*eta;

    return eta_new;
}

///////////////////////////////////////////////////////////////////

double distrib_eta_bump_fun(int j, int jmax, double eta_min, double eta_max)
{
    double eta = eta_min + (eta_max-eta_min)/jmax*j;

    double coeff1 = 0.1;
    double coeff2 = 0.2;
    double eta_new = coeff1*3.*eta*(1.-eta)*(1.-eta) + coeff2*3.*eta*eta*(1.-eta) + eta*eta*eta;

    return eta_new;
}


double distrib_xi_bump_fun(int i, int imax, double xi_min, double xi_max)
{
    double xi = double(i)/double(imax);

    double coeff1 = 0.5;
    double coeff2 = 0.5;
    double xi_new = xi_min + (xi_max-xi_min)*(coeff1*3.*xi*(1.-xi)*(1.-xi) + coeff2*3.*xi*xi*(1.-xi) + xi*xi*xi);

    return xi_new;
}

double distrib_eta_bump_3D_fun(int j, int jmax, double eta_min, double eta_max)
{
    double eta = eta_min + (eta_max-eta_min)/jmax*j;

    double coeff1 = 0.05;
    double coeff2 = 0.95;
    double eta_new = coeff1*3.*eta*(1.-eta)*(1.-eta) + coeff2*3.*eta*eta*(1.-eta) + eta*eta*eta;

    return eta_new;
}
