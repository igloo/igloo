/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <vector>
#include <math.h>

using namespace std;



// morphing using periodic deformation
void periodicDeformation(double time, double time_step, vector<double> *coord, vector<double> *vel, vector<double> *param, const vector<double> *init, const int subdomain_number)
{
    // get deformation parameters
    double frequency = param->at(0);
    double def_x = param->at(1);
    double def_y = param->at(2);

    double omega = 2*M_PI*frequency;

    // deformation law
    coord->at(0) = init->at(0) + def_x*sin(omega*time);
    coord->at(1) = init->at(1) + def_y*sin(omega*time);

    vel->at(0) = omega * def_x*cos(omega*time);
    vel->at(1) = omega * def_y*cos(omega*time);

}
