/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/

#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>

#include <igCore/igFittingCurve.h>
#include <igCore/igFittingSurface3D.h>
#include <igFiles/igFileManager.h>

#include "igMeshGeneratorFitting.h"


/////////////////////////////////////////////////////////////////////////////

igMeshGeneratorFitting::igMeshGeneratorFitting() : igMeshGenerator()
{

}

igMeshGeneratorFitting::~igMeshGeneratorFitting(void)
{

}

/////////////////////////////////////////////////////////////////////////////

void igMeshGeneratorFitting::generate(void)
{
    if(element_number_3 == 0)
        this->generate2D();
    else
        this->generate3D();

}

void igMeshGeneratorFitting::generate2D(void)
{

    // temporary cartesian grid
    vector<double> xcoord_tmp;
    xcoord_tmp.resize((element_number_1*degree+1)*(element_number_2*degree+1)*(element_number_3*degree+1),0.);
    vector<double> ycoord_tmp;
    ycoord_tmp.resize((element_number_1*degree+1)*(element_number_2*degree+1)*(element_number_3*degree+1),0.);
    vector<double> zcoord_tmp;
    zcoord_tmp.resize((element_number_1*degree+1)*(element_number_2*degree+1)*(element_number_3*degree+1),0.);
    vector<double> weight_tmp;
    weight_tmp.resize((element_number_1*degree+1)*(element_number_2*degree+1)*(element_number_3*degree+1), 1.);

    igFittingCurve *fitting = new igFittingCurve;
    fitting->setDegree(degree);
    fitting->setSampleNumber(2*(degree+1));
    fitting->setXFunction(x_fun);
    fitting->setYFunction(y_fun);
    fitting->setZFunction(z_fun);


    //-------------- construct iso-xi lines ---------------

    for (int i=0; i<element_number_1+1; ++i){

        double xi = distrib_xi_fun(i,element_number_1,xi_min,xi_max);
        double zeta = 0;

        fitting->selectParameterFixed(0,2);
        fitting->setParameterFixed(xi,zeta);

        // loop over eta intervals
        for (int j=0; j<element_number_2; j++){

            // extremities : coordinates known explicitly
            double eta_first = distrib_eta_fun(j,element_number_2,eta_min,eta_max);
            double eta_last = distrib_eta_fun(j+1,element_number_2,eta_min,eta_max);

            int index_1 = i*degree;
            int index_2_first = j*degree;
            int index_2_last = (j+1)*degree;

            int index_global_first = index_1 + index_2_first*(element_number_1*degree+1);
            int index_global_last  = index_1 + index_2_last *(element_number_1*degree+1);

            xcoord_tmp.at(index_global_first) = x_fun(xi,eta_first,zeta);
            ycoord_tmp.at(index_global_first) = y_fun(xi,eta_first,zeta);
            zcoord_tmp.at(index_global_first) = z_fun(xi,eta_first,zeta);
            xcoord_tmp.at(index_global_last) = x_fun(xi,eta_last,zeta);
            ycoord_tmp.at(index_global_last) = y_fun(xi,eta_last,zeta);
            zcoord_tmp.at(index_global_last) = z_fun(xi,eta_last,zeta);

            // fitting intermediate coordinates
            fitting->setParameterBounds(eta_first,eta_last);
            fitting->setCoordinateBounds(xcoord_tmp.at(index_global_first), xcoord_tmp.at(index_global_last),
                                         ycoord_tmp.at(index_global_first), ycoord_tmp.at(index_global_last),
                                         zcoord_tmp.at(index_global_first), zcoord_tmp.at(index_global_last) );
            fitting->run();

            for (int ils=0; ils<degree-1; ils++){
                int index = index_global_first + (ils+1) *(element_number_1*degree+1);

                xcoord_tmp.at(index) = fitting->xControlPointFit(ils);
                ycoord_tmp.at(index) = fitting->yControlPointFit(ils);
                zcoord_tmp.at(index) = fitting->zControlPointFit(ils);
            }

        }

    }

    //--------------- construct iso-eta lines --------------------

    for (int j=0; j<element_number_2+1; j++){

        double eta = distrib_eta_fun(j,element_number_2,eta_min,eta_max);
        double zeta = 0;

        fitting->selectParameterFixed(1,2);
        fitting->setParameterFixed(eta,zeta);

        // loop over xi intervals
        for (int i=0; i<element_number_1; i++){

            // extremities : coordinates already computed
            double xi_first = distrib_xi_fun(i,element_number_1,xi_min,xi_max);
            double xi_last = distrib_xi_fun(i+1,element_number_1,xi_min,xi_max);

            int index_2 = j*degree;
            int index_1_first = i*degree;
            int index_1_last = (i+1)*degree;

            int index_global_first = index_1_first + index_2 *(element_number_1*degree+1);
            int index_global_last  = index_1_last  + index_2 *(element_number_1*degree+1);

            // fitting intermediate coordinates
            fitting->setParameterBounds(xi_first,xi_last);
            fitting->setCoordinateBounds(xcoord_tmp.at(index_global_first), xcoord_tmp.at(index_global_last),
                                         ycoord_tmp.at(index_global_first), ycoord_tmp.at(index_global_last),
                                         zcoord_tmp.at(index_global_first), zcoord_tmp.at(index_global_last) );
            fitting->run();

            for (int ils=0; ils<degree-1; ils++){
                int index = index_global_first + (ils+1);

                xcoord_tmp.at(index) = fitting->xControlPointFit(ils);
                ycoord_tmp.at(index) = fitting->yControlPointFit(ils);
                zcoord_tmp.at(index) = fitting->zControlPointFit(ils);
            }

        }

    }

    //---------------- control points in elements --------------------

    coonsInterpolation(degree,element_number_1,element_number_2,element_number_3,xcoord_tmp);
    coonsInterpolation(degree,element_number_1,element_number_2,element_number_3,ycoord_tmp);

    //---------------- convert to DG structure --------------------

    xcoord.resize(element_number_1*element_number_2*(degree+1)*(degree+1));
    ycoord.resize(element_number_1*element_number_2*(degree+1)*(degree+1));
    zcoord.resize(element_number_1*element_number_2*(degree+1)*(degree+1));
    weight.resize(element_number_1*element_number_2*(degree+1)*(degree+1),1.);

    this->dgMeshConverter(xcoord_tmp, ycoord_tmp, zcoord_tmp, weight_tmp, xcoord, ycoord, zcoord, weight);

    //----------------- apply default face type ------------------

    face_type.resize(element_number_1*element_number_2*4,0);

    this->applyBoundaryCondition();

    delete fitting;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////


void igMeshGeneratorFitting::generate3D(void)
{

    // temporary cartesian grid
    vector<double> xcoord_tmp;
    xcoord_tmp.resize((element_number_1*degree+1)*(element_number_2*degree+1)*(element_number_3*degree+1),0.);
    vector<double> ycoord_tmp;
    ycoord_tmp.resize((element_number_1*degree+1)*(element_number_2*degree+1)*(element_number_3*degree+1),0.);
    vector<double> zcoord_tmp;
    zcoord_tmp.resize((element_number_1*degree+1)*(element_number_2*degree+1)*(element_number_3*degree+1),0.);
    vector<double> weight_tmp;
    weight_tmp.resize((element_number_1*degree+1)*(element_number_2*degree+1)*(element_number_3*degree+1), 1.);

    /////////////////////// First step : edge fitting //////////////////////

    igFittingCurve *fitting_curve = new igFittingCurve;
    fitting_curve->setDegree(degree);
    fitting_curve->setSampleNumber(2*(degree+1));
    fitting_curve->setXFunction(x_fun);
    fitting_curve->setYFunction(y_fun);
    fitting_curve->setZFunction(z_fun);

    //-------------- construct iso-xi iso-zeta lines ---------------

    for (int k=0; k<element_number_3+1; ++k){
        for (int i=0; i<element_number_1+1; ++i){

            double xi = distrib_xi_fun(i,element_number_1,xi_min,xi_max);
            double zeta = distrib_zeta_fun(k,element_number_3,zeta_min,zeta_max);

            fitting_curve->selectParameterFixed(0,2);
            fitting_curve->setParameterFixed(xi,zeta);

            // loop over eta intervals
            for (int j=0; j<element_number_2; j++){

                // extremities : coordinates known explicitly
                double eta_first = distrib_eta_fun(j,element_number_2,eta_min,eta_max);
                double eta_last = distrib_eta_fun(j+1,element_number_2,eta_min,eta_max);

                int index_1 = i*degree;
                int index_2_first = j*degree;
                int index_2_last = (j+1)*degree;
                int index_3 = k*degree;

                int index_global_first = index_1 + index_2_first*(element_number_1*degree+1) + index_3*(element_number_1*degree+1)*(element_number_2*degree+1);
                int index_global_last  = index_1 + index_2_last *(element_number_1*degree+1) + index_3*(element_number_1*degree+1)*(element_number_2*degree+1);

                xcoord_tmp.at(index_global_first) = x_fun(xi,eta_first,zeta);
                ycoord_tmp.at(index_global_first) = y_fun(xi,eta_first,zeta);
                zcoord_tmp.at(index_global_first) = z_fun(xi,eta_first,zeta);
                xcoord_tmp.at(index_global_last) = x_fun(xi,eta_last,zeta);
                ycoord_tmp.at(index_global_last) = y_fun(xi,eta_last,zeta);
                zcoord_tmp.at(index_global_last) = z_fun(xi,eta_last,zeta);

                // fitting intermediate coordinates
                fitting_curve->setParameterBounds(eta_first,eta_last);
                fitting_curve->setCoordinateBounds(xcoord_tmp.at(index_global_first), xcoord_tmp.at(index_global_last),
                                             ycoord_tmp.at(index_global_first), ycoord_tmp.at(index_global_last),
                                             zcoord_tmp.at(index_global_first), zcoord_tmp.at(index_global_last) );
                fitting_curve->run();

                for (int ils=0; ils<degree-1; ils++){
                    int index = index_global_first + (ils+1) *(element_number_1*degree+1);

                    xcoord_tmp.at(index) = fitting_curve->xControlPointFit(ils);
                    ycoord_tmp.at(index) = fitting_curve->yControlPointFit(ils);
                    zcoord_tmp.at(index) = fitting_curve->zControlPointFit(ils);
                }

            }

        }
    }

    //--------------- construct iso-eta iso-zeta lines --------------------

    for (int k=0; k<element_number_3+1; ++k){
        for (int j=0; j<element_number_2+1; j++){

            double eta = distrib_eta_fun(j,element_number_2,eta_min,eta_max);
            double zeta = distrib_zeta_fun(k,element_number_3,zeta_min,zeta_max);

            fitting_curve->selectParameterFixed(1,2);
            fitting_curve->setParameterFixed(eta,zeta);

            // loop over xi intervals
            for (int i=0; i<element_number_1; i++){

                // extremities : coordinates already computed
                double xi_first = distrib_xi_fun(i,element_number_1,xi_min,xi_max);
                double xi_last = distrib_xi_fun(i+1,element_number_1,xi_min,xi_max);

                int index_2 = j*degree;
                int index_1_first = i*degree;
                int index_1_last = (i+1)*degree;
                int index_3 = k*degree;

                int index_global_first = index_1_first + index_2 *(element_number_1*degree+1) + index_3*(element_number_1*degree+1)*(element_number_2*degree+1);
                int index_global_last  = index_1_last  + index_2 *(element_number_1*degree+1) + index_3*(element_number_1*degree+1)*(element_number_2*degree+1);

                // fitting_curve intermediate coordinates
                fitting_curve->setParameterBounds(xi_first,xi_last);
                fitting_curve->setCoordinateBounds(xcoord_tmp.at(index_global_first), xcoord_tmp.at(index_global_last),
                                             ycoord_tmp.at(index_global_first), ycoord_tmp.at(index_global_last),
                                             zcoord_tmp.at(index_global_first), zcoord_tmp.at(index_global_last) );
                fitting_curve->run();

                for (int ils=0; ils<degree-1; ils++){
                    int index = index_global_first + (ils+1);

                    xcoord_tmp.at(index) = fitting_curve->xControlPointFit(ils);
                    ycoord_tmp.at(index) = fitting_curve->yControlPointFit(ils);
                    zcoord_tmp.at(index) = fitting_curve->zControlPointFit(ils);
                }

            }

        }
    }

    //--------------- construct iso-xi iso-eta lines --------------------

    for (int j=0; j<element_number_2+1; ++j){
        for (int i=0; i<element_number_1+1; i++){

            double eta = distrib_eta_fun(j,element_number_2,eta_min,eta_max);
            double xi = distrib_xi_fun(i,element_number_1,xi_min,xi_max);

            fitting_curve->selectParameterFixed(0,1);
            fitting_curve->setParameterFixed(xi,eta);

            // loop over zeta intervals
            for (int k=0; k<element_number_3; k++){

                // extremities : coordinates already computed
                double zeta_first = distrib_zeta_fun(k,element_number_3,zeta_min,zeta_max);
                double zeta_last = distrib_zeta_fun(k+1,element_number_3,zeta_min,zeta_max);

                int index_1 = i*degree;
                int index_2 = j*degree;
                int index_3_first = k*degree;
                int index_3_last = (k+1)*degree;

                int index_global_first = index_1 + index_2 *(element_number_1*degree+1) + index_3_first*(element_number_1*degree+1)*(element_number_2*degree+1);
                int index_global_last  = index_1 + index_2 *(element_number_1*degree+1) + index_3_last*(element_number_1*degree+1)*(element_number_2*degree+1);

                // fitting_curve intermediate coordinates
                fitting_curve->setParameterBounds(zeta_first,zeta_last);
                fitting_curve->setCoordinateBounds(xcoord_tmp.at(index_global_first), xcoord_tmp.at(index_global_last),
                                             ycoord_tmp.at(index_global_first), ycoord_tmp.at(index_global_last),
                                             zcoord_tmp.at(index_global_first), zcoord_tmp.at(index_global_last) );
                fitting_curve->run();

                for (int ils=0; ils<degree-1; ils++){
                    int index = index_global_first + (ils+1) *(element_number_1*degree+1)*(element_number_2*degree+1);

                    xcoord_tmp.at(index) = fitting_curve->xControlPointFit(ils);
                    ycoord_tmp.at(index) = fitting_curve->yControlPointFit(ils);
                    zcoord_tmp.at(index) = fitting_curve->zControlPointFit(ils);
                }

            }

        }
    }


    /////////////////////// Second step : face fitting //////////////////////

    igFittingSurface3D *fitting = new igFittingSurface3D;
    fitting->setDegree(degree);
    fitting->setSampleNumber(2*(degree+1));
    fitting->setXFunction(x_fun);
    fitting->setYFunction(y_fun);
    fitting->setZFunction(z_fun);

    vector<double> *edge_x = new vector<double>(4*degree);
    vector<double> *edge_y = new vector<double>(4*degree);
    vector<double> *edge_z = new vector<double>(4*degree);

    //-------------- construct iso-xi surfaces ---------------

    for (int i=0; i<element_number_1+1; ++i){

        double xi = distrib_xi_fun(i,element_number_1,xi_min,xi_max);

        fitting->selectParameterFixed(0);
        fitting->setParameterFixed(xi);

        // loop over eta+zeta intervals
        for (int k=0; k<element_number_3; k++){
            for (int j=0; j<element_number_2; j++){

                // edges constraints
                int edge_counter = 0;
                for (int kls=0; kls<degree+1; kls++){
                    for (int jls=0; jls<degree+1; jls++){
                        if( kls==0 || kls==degree || jls==0 || jls==degree){
                            int index = i*degree + j*degree*(element_number_1*degree+1) + k*degree*(element_number_1*degree+1)*(element_number_2*degree+1)
                                        + jls*(element_number_1*degree+1) + kls*(element_number_1*degree+1)*(element_number_2*degree+1);

                            (*edge_x)[edge_counter] = xcoord_tmp.at(index);
                            (*edge_y)[edge_counter] = ycoord_tmp.at(index);
                            (*edge_z)[edge_counter] = zcoord_tmp.at(index);
                            edge_counter++;
                        }
                    }
                }
                fitting->setCoordinateBounds(edge_x, edge_y, edge_z);

                // fitting intermediate coordinates
                double eta_first = distrib_eta_fun(j,element_number_2,eta_min,eta_max);
                double eta_last = distrib_eta_fun(j+1,element_number_2,eta_min,eta_max);
                double zeta_first = distrib_zeta_fun(k,element_number_3,zeta_min,zeta_max);
                double zeta_last = distrib_zeta_fun(k+1,element_number_3,zeta_min,zeta_max);
                fitting->setParameterBounds(eta_first,eta_last,zeta_first,zeta_last);

                fitting->run();

                for (int kls=0; kls<degree+1; kls++){
                    for (int jls=0; jls<degree+1; jls++){

                        int index = i*degree + j*degree*(element_number_1*degree+1) + k*degree*(element_number_1*degree+1)*(element_number_2*degree+1)
                                    + jls*(element_number_1*degree+1) + kls*(element_number_1*degree+1)*(element_number_2*degree+1);

                        xcoord_tmp.at(index) = fitting->xControlPointFit(jls+kls*(degree+1));
                        ycoord_tmp.at(index) = fitting->yControlPointFit(jls+kls*(degree+1));
                        zcoord_tmp.at(index) = fitting->zControlPointFit(jls+kls*(degree+1));

                    }
                }

            }
        }

    }

    //-------------- construct iso-eta surfaces ---------------

    for (int j=0; j<element_number_2+1; ++j){

        double eta = distrib_eta_fun(j,element_number_2,eta_min,eta_max);

        fitting->selectParameterFixed(1);
        fitting->setParameterFixed(eta);

        // loop over xi+zeta intervals
        for (int k=0; k<element_number_3; k++){
            for (int i=0; i<element_number_1; i++){

                // edges constraints
                int edge_counter = 0;
                for (int kls=0; kls<degree+1; kls++){
                    for (int ils=0; ils<degree+1; ils++){
                        if( kls==0 || kls==degree || ils==0 || ils==degree){
                            int index = i*degree + j*degree*(element_number_1*degree+1) + k*degree*(element_number_1*degree+1)*(element_number_2*degree+1)
                                        + ils + kls*(element_number_1*degree+1)*(element_number_2*degree+1);

                            (*edge_x)[edge_counter] = xcoord_tmp.at(index);
                            (*edge_y)[edge_counter] = ycoord_tmp.at(index);
                            (*edge_z)[edge_counter] = zcoord_tmp.at(index);
                            edge_counter++;
                        }
                    }
                }
                fitting->setCoordinateBounds(edge_x, edge_y, edge_z);

                // fitting intermediate coordinates
                double xi_first = distrib_xi_fun(i,element_number_1,xi_min,xi_max);
                double xi_last = distrib_xi_fun(i+1,element_number_1,xi_min,xi_max);
                double zeta_first = distrib_zeta_fun(k,element_number_3,zeta_min,zeta_max);
                double zeta_last = distrib_zeta_fun(k+1,element_number_3,zeta_min,zeta_max);
                fitting->setParameterBounds(xi_first,xi_last,zeta_first,zeta_last);

                fitting->run();

                for (int kls=0; kls<degree+1; kls++){
                    for (int ils=0; ils<degree+1; ils++){

                        int index = i*degree + j*degree*(element_number_1*degree+1) + k*degree*(element_number_1*degree+1)*(element_number_2*degree+1)
                                    + ils + kls*(element_number_1*degree+1)*(element_number_2*degree+1);

                        xcoord_tmp.at(index) = fitting->xControlPointFit(ils+kls*(degree+1));
                        ycoord_tmp.at(index) = fitting->yControlPointFit(ils+kls*(degree+1));
                        zcoord_tmp.at(index) = fitting->zControlPointFit(ils+kls*(degree+1));
                    }
                }

            }
        }

    }

    //-------------- construct iso-zeta surfaces ---------------

    for (int k=0; k<element_number_3+1; ++k){

        double zeta = distrib_zeta_fun(k,element_number_3,zeta_min,zeta_max);

        fitting->selectParameterFixed(2);
        fitting->setParameterFixed(zeta);

        // loop over xi+eta intervals
        for (int j=0; j<element_number_2; j++){
            for (int i=0; i<element_number_1; i++){

                // edges constraints
                int edge_counter = 0;
                for (int jls=0; jls<degree+1; jls++){
                    for (int ils=0; ils<degree+1; ils++){
                        if( jls==0 || jls==degree || ils==0 || ils==degree){
                            int index = i*degree + j*degree*(element_number_1*degree+1) + k*degree*(element_number_1*degree+1)*(element_number_2*degree+1)
                                        + ils + jls*(element_number_1*degree+1);

                            (*edge_x)[edge_counter] = xcoord_tmp.at(index);
                            (*edge_y)[edge_counter] = ycoord_tmp.at(index);
                            (*edge_z)[edge_counter] = zcoord_tmp.at(index);
                            edge_counter++;
                        }
                    }
                }
                fitting->setCoordinateBounds(edge_x, edge_y, edge_z);

                // fitting intermediate coordinates
                double xi_first = distrib_xi_fun(i,element_number_1,xi_min,xi_max);
                double xi_last = distrib_xi_fun(i+1,element_number_1,xi_min,xi_max);
                double eta_first = distrib_eta_fun(j,element_number_2,eta_min,eta_max);
                double eta_last = distrib_eta_fun(j+1,element_number_2,eta_min,eta_max);
                fitting->setParameterBounds(xi_first,xi_last,eta_first,eta_last);

                fitting->run();

                for (int jls=0; jls<degree+1; jls++){
                    for (int ils=0; ils<degree+1; ils++){

                        int index = i*degree + j*degree*(element_number_1*degree+1) + k*degree*(element_number_1*degree+1)*(element_number_2*degree+1)
                                    + ils + jls*(element_number_1*degree+1);

                        xcoord_tmp.at(index) = fitting->xControlPointFit(ils+jls*(degree+1));
                        ycoord_tmp.at(index) = fitting->yControlPointFit(ils+jls*(degree+1));
                        zcoord_tmp.at(index) = fitting->zControlPointFit(ils+jls*(degree+1));
                    }
                }

            }
        }

    }

    //---------------- control points in elements --------------------

    coonsInterpolation(degree,element_number_1,element_number_2,element_number_3,xcoord_tmp);
    coonsInterpolation(degree,element_number_1,element_number_2,element_number_3,ycoord_tmp);
    coonsInterpolation(degree,element_number_1,element_number_2,element_number_3,zcoord_tmp);

    //---------------- convert to DG structure --------------------

    xcoord.resize(element_number_1*element_number_2*element_number_3*(degree+1)*(degree+1)*(degree+1),0.);
    ycoord.resize(element_number_1*element_number_2*element_number_3*(degree+1)*(degree+1)*(degree+1),0.);
    zcoord.resize(element_number_1*element_number_2*element_number_3*(degree+1)*(degree+1)*(degree+1),0.);
    weight.resize(element_number_1*element_number_2*element_number_3*(degree+1)*(degree+1)*(degree+1),1.);

    this->dgMeshConverter(xcoord_tmp, ycoord_tmp, zcoord_tmp, weight_tmp, xcoord, ycoord, zcoord, weight);

    //----------------- apply default face type ------------------

    face_type.resize(element_number_1*element_number_2*element_number_3*6,0);

    this->applyBoundaryCondition();

    delete edge_x;
    delete edge_y;
    delete edge_z;
    delete fitting;
    delete fitting_curve;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////


void igMeshGeneratorFitting::coonsInterpolation(int degree, int element_number_1, int element_number_2, int element_number_3, vector<double> &field)
{
    double F1, F2, F3;
    double F1F2, F2F3, F3F1;
    double F1F2F3;

    int dimension = 2;
    if(element_number_3 > 0)
        dimension = 3;

    int imax = element_number_1;
    int jmax = element_number_2;
    int kmax = max(1,element_number_3);

    int in1_max = degree;
    int in2_max = degree;
    int in3_max = max(1,degree);

    // loop over elements
    for (int k=0; k<kmax; k++){
        for (int j=0; j<jmax; j++){
            for (int i=0; i<imax; i++){

                int index_000 = (element_number_2*degree+1)*(element_number_1*degree+1)*degree*k + (element_number_1*degree+1)*degree*j + degree*i;
                int index_100 = (element_number_2*degree+1)*(element_number_1*degree+1)*degree*k + (element_number_1*degree+1)*degree*j + degree*(i+1);
                int index_010 = (element_number_2*degree+1)*(element_number_1*degree+1)*degree*k + (element_number_1*degree+1)*degree*(j+1) + degree*i;
                int index_110 = (element_number_2*degree+1)*(element_number_1*degree+1)*degree*k + (element_number_1*degree+1)*degree*(j+1) + degree*(i+1);
                int index_001 = (element_number_2*degree+1)*(element_number_1*degree+1)*degree*(k+1) + (element_number_1*degree+1)*degree*j + degree*i;
                int index_101 = (element_number_2*degree+1)*(element_number_1*degree+1)*degree*(k+1) + (element_number_1*degree+1)*degree*j + degree*(i+1);
                int index_011 = (element_number_2*degree+1)*(element_number_1*degree+1)*degree*(k+1) + (element_number_1*degree+1)*degree*(j+1) + degree*i;
                int index_111 = (element_number_2*degree+1)*(element_number_1*degree+1)*degree*(k+1) + (element_number_1*degree+1)*degree*(j+1) + degree*(i+1);

                // inside each element
                for (int in3=1; in3<degree;in3++){
                    for (int in2=1; in2<degree;in2++){
                        for (int in1=1; in1<degree;in1++){

                            int index_i00 = index_000 + in1;
                            int index_i10 = index_000 + in1 + (element_number_1*degree+1)*degree;
                            int index_i01 = index_000 + in1 + (element_number_1*degree+1)*(element_number_2*degree+1)*degree;
                            int index_i11 = index_000 + in1 + (element_number_1*degree+1)*degree + (element_number_1*degree+1)*(element_number_2*degree+1)*degree;

                            int index_0j0 = index_000 + (element_number_1*degree+1)*in2;
                            int index_1j0 = index_000 + degree + (element_number_1*degree+1)*in2;
                            int index_0j1 = index_000 + (element_number_1*degree+1)*in2 + (element_number_1*degree+1)*(element_number_2*degree+1)*degree;
                            int index_1j1 = index_000 + degree + (element_number_1*degree+1)*in2 + (element_number_1*degree+1)*(element_number_2*degree+1)*degree;

                            int index_00k = index_000 + (element_number_1*degree+1)*(element_number_2*degree+1)*in3;
                            int index_10k = index_000 + degree + (element_number_1*degree+1)*(element_number_2*degree+1)*in3;
                            int index_01k = index_000 + (element_number_1*degree+1)*degree + (element_number_1*degree+1)*(element_number_2*degree+1)*in3;
                            int index_11k = index_000 + degree + (element_number_1*degree+1)*degree + (element_number_1*degree+1)*(element_number_2*degree+1)*in3;

                            int index_ij0 = index_000 + in1 + (element_number_1*degree+1)*in2;
                            int index_ij1 = index_000 + in1 + (element_number_1*degree+1)*in2 + (element_number_1*degree+1)*(element_number_2*degree+1)*degree;

                            int index_i0k = index_000 + in1 + (element_number_1*degree+1)*(element_number_2*degree+1)*in3;
                            int index_i1k = index_000 + in1 + (element_number_1*degree+1)*degree + (element_number_1*degree+1)*(element_number_2*degree+1)*in3;

                            int index_0jk = index_000 + (element_number_1*degree+1)*in2 + (element_number_1*degree+1)*(element_number_2*degree+1)*in3;
                            int index_1jk = index_000 + degree + (element_number_1*degree+1)*in2 + (element_number_1*degree+1)*(element_number_2*degree+1)*in3;

                            double weight_1 = double(in1)/degree;
                            double weight_2 = double(in2)/degree;
                            double weight_3 = double(in3)/degree;

                            if(dimension == 2){

                                F1 = (1.-weight_1)*field.at(index_0j0) + weight_1*field.at(index_1j0);
                                F2 = (1.-weight_2)*field.at(index_i00) + weight_2*field.at(index_i10);

                                F1F2 = (1.-weight_1)*(1.-weight_2)*field.at(index_000)
                                                + weight_1*(1.-weight_2)*field.at(index_100)
                                                + (1.-weight_1)*weight_2*field.at(index_010)
                                                + weight_1*weight_2*field.at(index_110);

                                int index = index_000 + in1 + (element_number_1*degree+1)*in2;

                                field.at(index) = F1 + F2 - F1F2;
                            }

                            if(dimension == 3){

                                F1 = (1.-weight_1)*field.at(index_0jk) + weight_1*field.at(index_1jk);
                                F2 = (1.-weight_2)*field.at(index_i0k) + weight_2*field.at(index_i1k);
                                F3 = (1.-weight_3)*field.at(index_ij0) + weight_3*field.at(index_ij1);

                                F1F2 = (1.-weight_1)*(1.-weight_2)*field.at(index_00k)
                                                + weight_1*(1.-weight_2)*field.at(index_10k)
                                                + (1.-weight_1)*weight_2*field.at(index_01k)
                                                + weight_1*weight_2*field.at(index_11k);

                                F2F3 = (1.-weight_2)*(1.-weight_3)*field.at(index_i00)
                                                + weight_2*(1.-weight_3)*field.at(index_i10)
                                                + (1.-weight_2)*weight_3*field.at(index_i01)
                                                + weight_2*weight_3*field.at(index_i11);

                                F3F1 = (1.-weight_3)*(1.-weight_1)*field.at(index_0j0)
                                                + weight_3*(1.-weight_1)*field.at(index_0j1)
                                                + (1.-weight_3)*weight_1*field.at(index_1j0)
                                                + weight_3*weight_1*field.at(index_1j1);

                                F1F2F3 = (1.-weight_1)*(1.-weight_2)*(1.-weight_3)*field.at(index_000)
                                        + weight_1*(1.-weight_2)*(1.-weight_3)*field.at(index_100)
                                        + (1.-weight_1)*weight_2*(1.-weight_3)*field.at(index_010)
                                        + (1.-weight_1)*(1.-weight_2)*weight_3*field.at(index_001)
                                        + weight_1*weight_2*(1.-weight_3)*field.at(index_110)
                                        + weight_1*(1.-weight_2)*weight_3*field.at(index_101)
                                        + (1.-weight_1)*weight_2*weight_3*field.at(index_011)
                                        + weight_1*weight_2*weight_3*field.at(index_111);

                                int index = index_000 + in1 + (element_number_1*degree+1)*in2 + (element_number_1*degree+1)*(element_number_2*degree+1)*in3;

                                field.at(index) = F1 + F2 + F3 - F1F2 - F2F3 - F3F1 + F1F2F3;
                            }

                        }
                    }
                }

            }
        }
    }

}
