/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <math.h>

#include <igCore/igBoundaryIds.h>

using namespace std;

////////////////////////////////////////////////////

// parametric transformation
double k(double x1, double k_min, double k_max)
{
    return k_min + (k_max-k_min)*x1;
}

double q(double x2, double k)
{
    return 0.5 + (k - 0.5)*x2;
}

// Ringleb solution
double ringleb_a(double k, double q)
{
    double gamma = 1.4;
    double a = sqrt(1.-(gamma-1.)/2.*q*q);

    return a;
}

double ringleb_rho(double k, double q)
{
    double gamma = 1.4;
    double a = ringleb_a(k,q);
    double rho = pow(a,2./(gamma-1.));

    return rho;
}

double ringleb_p(double k, double q)
{
    double gamma = 1.4;
    double a = ringleb_a(k,q);
    double p = pow(a,2.*gamma/(gamma-1.))/gamma;

    return p;
}

double ringleb_j(double k, double q)
{
    double a = ringleb_a(k,q);
    double j = 1./a + 1./(3.*a*a*a) + 1./(5.*a*a*a*a*a) - 0.5*log((1.+a)/(1.-a));

    return j;
}

double ringleb_x(double k, double q)
{
    double j = ringleb_j(k,q);
    double rho = ringleb_rho(k,q);

    double x = 1./(2.*rho) * (2./(k*k) - 1./(q*q)) - j/2.;

    return x;
}

double ringleb_y(double k, double q)
{
    double rho = ringleb_rho(k,q);

    double y = 1./(k*rho*q) * sqrt(1. - q*q/(k*k));

    return y;
}

void find_coord(double x, double y, double &k,double &q)
{
    // determine (k,q) coordinates

    double x_max;
    double x_min;
    double y_max;
    double y_min;
    double q_mid;
    double k_mid;
    double k_min  = 0.6;
    double k_max = 1.6;
    double eps = 1.E-10;

    double step_k = k_max - k_min;

    while(step_k > eps){

        k_mid = (k_min+k_max)*0.5;

        double q_min = 0.3;
        double q_max = k_mid;

        double step_q = q_max - q_min;

        while(step_q > eps){

            q_mid = (q_min+q_max)*0.5;
            double y_mid = ringleb_y(k_mid,q_mid);

            if(fabs(y) < y_mid){
                q_min = q_mid;
                y_max = y_mid;
            } else {
                q_max = q_mid;
                y_min = y_mid;
            }
            step_q = q_max-q_min;
        }
        q = q_mid;

        double x_mid = ringleb_x(k_mid,q);

        if(x < x_mid){
            k_min = k_mid;
            x_max = x_mid;
        } else {
            k_max = k_mid;
            x_min = x_mid;
        }
        step_k = k_max-k_min;
    }
    k = k_mid;

}

void find_coord_boundary(double x, double y, double &k,double &q, int face_type)
{
    double eps = 1.E-10;
    double k_min;
    double k_max;
    double q_min;
    double q_max;
    double x_min;
    double x_max;
    double y_min;
    double y_max;
    double step;
    double k_mid;
    double q_mid;

    switch(face_type){

    case id_ringleb+1: // inlet
        q = 0.4;
        k_min  = 0.7;
        k_max = 1.5;
        x_min = ringleb_x(k_max,q);
        x_max = ringleb_x(k_min,q);
        step = k_max - k_min;

        while(step > eps){

            k_mid = (k_min+k_max)*0.5;
            double x_mid = ringleb_x(k_mid,q);

            if(x < x_mid){
                k_min = k_mid;
                x_max = x_mid;
            } else {
                k_max = k_mid;
                x_min = x_mid;
            }
            step = k_max-k_min;
        }
        k = k_mid;
        break;

    case id_ringleb+2: // inner wall
        k = 1.5;
        q_min  = 0.4;
        q_max = k;
        y_max = ringleb_y(k,q_min);
        y_min = ringleb_y(k,q_max);
        step = q_max - q_min;

        while(step > eps){

            q_mid = (q_min+q_max)*0.5;
            double y_mid = ringleb_y(k,q_mid);

            if(fabs(y) < y_mid){
                q_min = q_mid;
                y_max = y_mid;
            } else {
                q_max = q_mid;
                y_min = y_mid;
            }
            step = q_max-q_min;
        }
        q = q_mid;
        break;

    case id_ringleb+3: // outlet
        q = 0.4;
        k_min  = 0.7;
        k_max = 1.5;
        x_min = ringleb_x(k_max,q);
        x_max = ringleb_x(k_min,q);
        step = k_max-k_min;

        while(step > eps){

            k_mid = (k_min+k_max)*0.5;
            double x_mid = ringleb_x(k_mid,q);

            if(x < x_mid){
                k_min = k_mid;
                x_max = x_mid;
            } else {
                k_max = k_mid;
                x_min = x_mid;
            }
            step = k_max-k_min;

        }
        k = k_mid;
        break;

    case id_ringleb+4: // outer wall
        k = 0.7;
        q_min  = 0.4;
        q_max = k;
        y_max = ringleb_y(k,q_min);
        y_min = ringleb_y(k,q_max);
        step = q_max - q_min;

        while(step > eps){

            q_mid = (q_min+q_max)*0.5;
            double y_mid = ringleb_y(k,q_mid);

            if(fabs(y) < y_mid){
                q_min = q_mid;
                y_max = y_mid;
            } else {
                q_max = q_mid;
                y_min = y_mid;
            }
            step = q_max-q_min;
        }
        q = q_mid;
        break;

    }

}
