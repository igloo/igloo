/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/

#include <iostream>
#include <fstream>
#include <sstream>

#include <igCore/igMesh.h>
#include <igCore/igElement.h>
#include <igCore/igCell.h>
#include <igCore/igFace.h>

#include <igFiles/igFileManager.h>

#include "igMeshGeneratorCompound.h"


/////////////////////////////////////////////////////////////////////////////

igMeshGeneratorCompound::igMeshGeneratorCompound() : igMeshGenerator()
{

}


/////////////////////////////////////////////////////////////////////////////

void igMeshGeneratorCompound::generate(void)
{
    // file for multi-patch definition
    ifstream base_file(config_filename.c_str(), ios::in);

    // file for curve definition
    ifstream curve_file("control_point.dat", ios::in);

    if(base_file.is_open()){

        // read generic data
        int patch_dimension;
        base_file >> patch_dimension;
        int patch_number;
        base_file >> patch_number;
        int patch_degree;
        base_file >> patch_degree;
        int gauss_pt;
        base_file >> gauss_pt;
        int interface_number;
        base_file >> interface_number;
        int boundary_number;
        base_file >> boundary_number;
        int curved_boundary_number;
        base_file >> curved_boundary_number;

        int patch_size = (patch_degree+1)*(patch_degree+1);
        int patch_gauss_pt = gauss_pt;
        int interface_gauss_pt = gauss_pt;

        vector<double> xcoord_tmp;
        xcoord_tmp.resize(patch_size);
        vector<double> ycoord_tmp;
        ycoord_tmp.resize(patch_size);
        vector<double> weight_tmp;
        weight_tmp.resize(patch_size);
        vector<double> *coord = new vector<double>;
        coord->resize(patch_dimension);

        int max_field_number = max(variable_number,derivative_number);
        max_field_number = max(max_field_number,patch_dimension+1);

        this->coordinates = new vector<double>;
        this->coordinates->resize(patch_number*patch_size*(patch_dimension+1), 0.);

        this->mesh = new igMesh;
        mesh->setCommunicator(this->communicator);
        mesh->setDimension(patch_dimension);
        mesh->setVariableNumber(variable_number);
        mesh->setDerivativeNumber(derivative_number);
        mesh->setExactSolutionId(exact_solution_id);
        mesh->setCoordinates(this->coordinates);

        double x_tmp;
        double y_tmp;

        //-------------------
        //  Patches loop
        //-------------------

        for (int ipatch=0; ipatch<patch_number; ipatch++){

            //--------------------- construct element ------------------

            igElement *new_elt = new igElement;

            new_elt->setCellId(ipatch);
            new_elt->setPartition(0);
            new_elt->setPhysicalDimension(patch_dimension);
            new_elt->setParametricDimension(patch_dimension);
            new_elt->setDegree(patch_degree);
            new_elt->setGaussPointNumberByDirection(patch_gauss_pt);
            new_elt->setVariableNumber(variable_number);
            new_elt->setDerivativeNumber(derivative_number);
            new_elt->setCoordinates(this->coordinates);

            new_elt->initializeDegreesOfFreedom();

            mesh->addElement(new_elt);
            mesh->updateMeshProperties();
        }

        //---------------------- global id setting ----------------

        int global_id_counter = 0;
        for (int ivar=0; ivar<max_field_number; ivar++){
            for (int iel=0; iel < mesh->elementNumber(); iel++){
                igElement *elt = mesh->element(iel);
                for (int ipt=0; ipt<elt->controlPointNumber(); ipt++){
                    elt->setGlobalId(ipt, ivar, global_id_counter);
                    global_id_counter++;
                }
            }
        }

        //-------------- read patch coordinates ---------------

        for (int ipatch=0; ipatch<patch_number; ipatch++){

        	int subdomain_number = 0;
        	if(this->subdomains)
        		base_file >> subdomain_number;

            if(curved_boundary_number == 0)
                this->readBoundaryPatch(base_file, patch_degree, xcoord_tmp, ycoord_tmp, weight_tmp);
            else
                this->readCornerPatch(base_file, curve_file, patch_degree, xcoord_tmp, ycoord_tmp, weight_tmp);

            cout << "read element : " << ipatch << endl;


            //--------------- construct interior data by coons -------------

            for (int j=1; j<patch_degree; j++){
                for (int i=1; i<patch_degree; i++){

                    int index = (patch_degree+1)*j + i;
                    int index_00 = 0;
                    int index_10 = patch_degree;
                    int index_11 =  (patch_degree+1)*patch_degree + patch_degree;
                    int index_01 = (patch_degree+1)*patch_degree;
                    int index_i0 = i;
                    int index_i1 = (patch_degree+1)*patch_degree + i;
                    int index_0j = (patch_degree+1)*j;
                    int index_1j = (patch_degree+1)*j + patch_degree;

                    double weight_i0 = 1.-double(j)/(patch_degree);
                    double weight_i1 = double(j)/(patch_degree);
                    double weight_0j = 1.-double(i)/(patch_degree);
                    double weight_1j = double(i)/(patch_degree);

                    xcoord_tmp.at(index) = weight_0j*xcoord_tmp.at(index_0j) + weight_1j*xcoord_tmp.at(index_1j)
                                         + weight_i0*xcoord_tmp.at(index_i0) + weight_i1*xcoord_tmp.at(index_i1)
                                         - weight_i0*weight_0j*xcoord_tmp.at(index_00)
                                         - weight_1j*weight_i1*xcoord_tmp.at(index_11)
                                         - weight_i0*weight_1j*xcoord_tmp.at(index_10)
                                         - weight_0j*weight_i1*xcoord_tmp.at(index_01);

                    ycoord_tmp.at(index) = weight_0j*ycoord_tmp.at(index_0j) + weight_1j*ycoord_tmp.at(index_1j)
                                         + weight_i0*ycoord_tmp.at(index_i0) + weight_i1*ycoord_tmp.at(index_i1)
                                         - weight_i0*weight_0j*ycoord_tmp.at(index_00)
                                         - weight_1j*weight_i1*ycoord_tmp.at(index_11)
                                         - weight_i0*weight_1j*ycoord_tmp.at(index_10)
                                         - weight_0j*weight_i1*ycoord_tmp.at(index_01);

                    weight_tmp.at(index) = weight_0j*weight_tmp.at(index_0j) + weight_1j*weight_tmp.at(index_1j)
                                         + weight_i0*weight_tmp.at(index_i0) + weight_i1*weight_tmp.at(index_i1)
                                         - weight_i0*weight_0j*weight_tmp.at(index_00)
                                         - weight_1j*weight_i1*weight_tmp.at(index_11)
                                         - weight_i0*weight_1j*weight_tmp.at(index_10)
                                         - weight_0j*weight_i1*weight_tmp.at(index_01);

                }
            }
            cout << "fill element : " << ipatch << endl;

            igElement *new_elt = mesh->element(ipatch);

            for (int ipt=0; ipt<new_elt->controlPointNumber(); ipt++){

                if(patch_dimension == 1){
                    this->coordinates->at(new_elt->globalId(ipt, 0)) = xcoord_tmp[ipt];
                    this->coordinates->at(new_elt->globalId(ipt, 1)) = weight_tmp[ipt];
                } else if(patch_dimension == 2){
                    this->coordinates->at(new_elt->globalId(ipt, 0)) = xcoord_tmp[ipt];
                    this->coordinates->at(new_elt->globalId(ipt, 1)) = ycoord_tmp[ipt];
                    this->coordinates->at(new_elt->globalId(ipt, 2)) = weight_tmp[ipt];
                }

            }
            new_elt->initializeGeometry();
            new_elt->setSubdomain(subdomain_number);

            cout << "completed element : " << ipatch << endl;

        }



        //-------------------
        //  Interfaces loop
        //-------------------

        int elt_left_index;
        int elt_right_index;
        int orientation_left;
        int orientation_right;
        int sense_left;
        int sense_right;

        // NOTE = negative orientation is used

        for (int ifac=0; ifac<interface_number; ifac++){

            //-------------- read interface connectivity ---------------

            base_file >> elt_left_index;
            base_file >> orientation_left;
            base_file >> sense_left;
            base_file >> elt_right_index;
            base_file >> orientation_right;
            base_file >> sense_right;

            //---------------- create new face ------------------

            igFace *new_face = new igFace;

            new_face->setCellId(mesh->faceNumber());
            new_face->setPhysicalDimension(patch_dimension);
            new_face->setParametricDimension(patch_dimension-1);
            new_face->setDegree(patch_degree);
            new_face->setGaussPointNumberByDirection(interface_gauss_pt);
            new_face->setVariableNumber(variable_number);
            new_face->setDerivativeNumber(derivative_number);
            new_face->setType(0);
            new_face->setLeftPartitionId(0);
            new_face->setRightPartitionId(0);
            new_face->setLeftInside();
            new_face->setRightInside();
            new_face->setCoordinates(this->coordinates);

            new_face->initializeDegreesOfFreedom();


            igElement *elt_left = mesh->element(elt_left_index);
            igElement *elt_right = mesh->element(elt_right_index);

            int offset;
            int incr;
            bool sign = false;
            if(sense_left<0)
                sign = true;

            switch(orientation_left){
            case 0:
                offset = 0;
                incr = 1;
                if(sign){
                    offset = patch_degree;
                    incr *= -1;
                }
                break;
            case 1:
                offset = patch_degree;
                incr = patch_degree+1;
                if(sign){
                    offset = (patch_degree+1)*(patch_degree+1)-1;
                    incr *= -1;
                }
                break;
            case 2:
                offset = (patch_degree+1)*patch_degree;
                incr = 1;
                if(sign){
                    offset = (patch_degree+1)*(patch_degree+1)-1;
                    incr *= -1;
                }
                break;
            case 3:
                offset = 0;
                incr = patch_degree+1;
                if(sign){
                    offset = (patch_degree+1)*patch_degree;
                    incr *= -1;
                }
                break;
            }

            new_face->setLeftElementIndex(elt_left_index);
            new_face->setOrientationLeft(orientation_left);
            new_face->setSenseLeft(sense_left);
            for (int ivar=0; ivar<max_field_number; ivar++){
                for (int idof=0; idof<new_face->controlPointNumber(); idof++){
                    new_face->setLeftDofMap(idof,ivar,elt_left->globalId(offset+idof*incr,ivar));
                }
            }

            sign = false;
            if(sense_right<0)
                sign = true;

            switch(orientation_right){
            case 0:
                offset = 0;
                incr = 1;
                if(sign){
                    offset = patch_degree;
                    incr *= -1;
                }
                break;
            case 1:
                offset = patch_degree;
                incr = patch_degree+1;
                if(sign){
                    offset = (patch_degree+1)*(patch_degree+1)-1;
                    incr *= -1;
                }
                break;
            case 2:
                offset = (patch_degree+1)*patch_degree;
                incr = 1;
                if(sign){
                    offset = (patch_degree+1)*(patch_degree+1)-1;
                    incr *= -1;
                }
                break;
            case 3:
                offset = 0;
                incr = patch_degree+1;
                if(sign){
                    offset = (patch_degree+1)*patch_degree;
                    incr *= -1;
                }
                break;
            }


            new_face->setRightElementIndex(elt_right_index);
            new_face->setOrientationRight(orientation_right);
            new_face->setSenseRight(sense_right);
            for (int ivar=0; ivar<max_field_number; ivar++){
                for (int idof=0; idof<new_face->controlPointNumber(); idof++){
                    new_face->setRightDofMap(idof,ivar,elt_right->globalId(offset+idof*incr,ivar));
                }
            }

            for (int idim=0; idim<patch_dimension; idim++)
                coord->at(idim) = elt_left->barycenter(idim);

            new_face->initializeGeometry();
            new_face->initializeNormal(coord);

            // storage in the face list
            mesh->addFace(new_face);
            mesh->updateMeshProperties();

            cout << "completed interface : " << ifac << endl;

        }


        //-------------------
        //  Boundary Interfaces loop
        //-------------------

        int interface_type;

        for (int ifac=0; ifac<boundary_number; ifac++){

            //-------------- read interface connectivity ---------------

            base_file >> elt_left_index;
            base_file >> orientation_left;
            base_file >> interface_type;

            cout << "read boundary interface : " << ifac << endl;

            //---------------- create new face ------------------

            igFace *new_face = new igFace;

            new_face->setCellId(mesh->boundaryFaceNumber());
            new_face->setPhysicalDimension(patch_dimension);
            new_face->setParametricDimension(patch_dimension-1);
            new_face->setDegree(patch_degree);
            new_face->setGaussPointNumberByDirection(interface_gauss_pt);
            new_face->setVariableNumber(variable_number);
            new_face->setDerivativeNumber(derivative_number);
            new_face->setType(interface_type);
            new_face->setLeftPartitionId(0);
            new_face->setLeftInside();
            new_face->setCoordinates(this->coordinates);

            new_face->initializeDegreesOfFreedom();

            igElement *elt_left = mesh->element(elt_left_index);

            int offset;
            int incr;

            switch(orientation_left){
            case 0:
                offset = 0;
                incr = 1;
                break;
            case 1:
                offset = patch_degree;
                incr = patch_degree+1;
                break;
            case 2:
                offset = (patch_degree+1)*patch_degree;
                incr = 1;
                break;
            case 3:
                offset = 0;
                incr = patch_degree+1;
                break;
            }

            new_face->setLeftElementIndex(elt_left_index);
            new_face->setOrientationLeft(orientation_left);
            new_face->setSenseLeft(1);
            for (int ivar=0; ivar<max_field_number; ivar++){
                for (int idof=0; idof<new_face->controlPointNumber(); idof++){
                    new_face->setLeftDofMap(idof,ivar,elt_left->globalId(offset+idof*incr,ivar));
                }
            }

            for (int idim=0; idim<patch_dimension; idim++)
                coord->at(idim) = elt_left->barycenter(idim);

            new_face->initializeGeometry();
            new_face->initializeNormal(coord);

            // storage in the face list
            mesh->addBoundaryFace(new_face);
            mesh->updateMeshProperties();

            cout << "completed boundary interface : " << ifac << endl;

        }

        base_file.close();

        curve_file.close();

        delete coord;

    }



}


/////////////////////////////////////////////////////////////////////////////////////

void igMeshGeneratorCompound::readBoundaryPatch(ifstream &base_file, int patch_degree, vector<double> &xcoord_tmp, vector<double> &ycoord_tmp, vector<double> &weight_tmp)
{
    double x_tmp;
    double y_tmp;

    // read south data
    for (int ipt=0; ipt<patch_degree+1; ipt++){
        base_file >> x_tmp;
        base_file >> y_tmp;
        xcoord_tmp[ipt] = xRotate(x_tmp,y_tmp);
        ycoord_tmp[ipt] = yRotate(x_tmp,y_tmp);
        base_file >> weight_tmp[ipt];
    }

    // read west & east data
    for (int ipt=1; ipt<patch_degree; ipt++){
        base_file >> x_tmp;
        base_file >> y_tmp;
        xcoord_tmp[(patch_degree+1)*ipt] = xRotate(x_tmp,y_tmp);
        ycoord_tmp[(patch_degree+1)*ipt] = yRotate(x_tmp,y_tmp);
        base_file >> weight_tmp[(patch_degree+1)*ipt];
        base_file >> x_tmp;
        base_file >> y_tmp;
        xcoord_tmp[patch_degree+(patch_degree+1)*ipt] = xRotate(x_tmp,y_tmp);
        ycoord_tmp[patch_degree+(patch_degree+1)*ipt] = yRotate(x_tmp,y_tmp);
        base_file >> weight_tmp[patch_degree+(patch_degree+1)*ipt];
    }

    // read north data
    for (int ipt=0; ipt<patch_degree+1; ipt++){
        base_file >> x_tmp;
        base_file >> y_tmp;
        xcoord_tmp[(patch_degree+1)*patch_degree+ipt] = xRotate(x_tmp,y_tmp);
        ycoord_tmp[(patch_degree+1)*patch_degree+ipt] = yRotate(x_tmp,y_tmp);
        base_file >> weight_tmp[(patch_degree+1)*patch_degree+ipt];
    }


}

void igMeshGeneratorCompound::readCornerPatch(ifstream &base_file, ifstream &curve_file, int patch_degree, vector<double> &xcoord_tmp, vector<double> &ycoord_tmp, vector<double> &weight_tmp)
{
    double x_tmp;
    double y_tmp;

    //--------------- read corners of element ------------------

    // read SW corner
    base_file >> x_tmp;
    base_file >> y_tmp;
    xcoord_tmp[0] = xRotate(x_tmp,y_tmp);
    ycoord_tmp[0] = yRotate(x_tmp,y_tmp);
    base_file >> weight_tmp[0];

    // read SE corner
    base_file >> x_tmp;
    base_file >> y_tmp;
    xcoord_tmp[patch_degree] = xRotate(x_tmp,y_tmp);
    ycoord_tmp[patch_degree] = yRotate(x_tmp,y_tmp);
    base_file >> weight_tmp[patch_degree];

    // read NW corner
    base_file >> x_tmp;
    base_file >> y_tmp;
    xcoord_tmp[(patch_degree+1)*patch_degree] = xRotate(x_tmp,y_tmp);
    ycoord_tmp[(patch_degree+1)*patch_degree] = yRotate(x_tmp,y_tmp);
    base_file >> weight_tmp[(patch_degree+1)*patch_degree];

    // read NE corner
    base_file >> x_tmp;
    base_file >> y_tmp;
    xcoord_tmp[(patch_degree+1)*(patch_degree+1)-1] = xRotate(x_tmp,y_tmp);
    ycoord_tmp[(patch_degree+1)*(patch_degree+1)-1] = yRotate(x_tmp,y_tmp);
    base_file >> weight_tmp[(patch_degree+1)*(patch_degree+1)-1];


    //---------------- read edge flags  -------------------

    int south_flag;
    int east_flag;
    int north_flag;
    int west_flag;

    base_file >> south_flag;
    base_file >> east_flag;
    base_file >> north_flag;
    base_file >> west_flag;

    //---------------- compute edges  -------------------

    //---- fist pass to read external data first

    if(south_flag < 0){
        for (int ipt=0; ipt<patch_degree+1; ipt++){
            curve_file >> x_tmp;
            curve_file >> y_tmp;
            curve_file >> weight_tmp[ipt];
            xcoord_tmp[ipt] = xRotate(x_tmp,y_tmp);
            ycoord_tmp[ipt] = yRotate(x_tmp,y_tmp);
        }
    }

    if(east_flag < 0){
        for (int ipt=0; ipt<patch_degree+1; ipt++){
            curve_file >> x_tmp;
            curve_file >> y_tmp;
            curve_file >> weight_tmp[patch_degree + ipt*(patch_degree+1)];
            xcoord_tmp[patch_degree + ipt*(patch_degree+1)] = xRotate(x_tmp,y_tmp);
            ycoord_tmp[patch_degree + ipt*(patch_degree+1)] = yRotate(x_tmp,y_tmp);
        }
    }

    if(north_flag < 0){
        for (int ipt=0; ipt<patch_degree+1; ipt++){
            curve_file >> x_tmp;
            curve_file >> y_tmp;
            curve_file >> weight_tmp[(patch_degree+1)*patch_degree+ipt];
            xcoord_tmp[(patch_degree+1)*patch_degree+ipt] = xRotate(x_tmp,y_tmp);
            ycoord_tmp[(patch_degree+1)*patch_degree+ipt] = yRotate(x_tmp,y_tmp);
        }
    }

    if(west_flag < 0){
        for (int ipt=0; ipt<patch_degree+1; ipt++){
            curve_file >> x_tmp;
            curve_file >> y_tmp;
            curve_file >> weight_tmp[(patch_degree+1)*ipt];
            xcoord_tmp[(patch_degree+1)*ipt] = xRotate(x_tmp,y_tmp);
            ycoord_tmp[(patch_degree+1)*ipt] = yRotate(x_tmp,y_tmp);
        }
    }

    //--- second pass to read internal data

    if(south_flag > 0){
        for (int ipt=1; ipt<patch_degree; ipt++){
            base_file >> x_tmp;
            base_file >> y_tmp;
            base_file >> weight_tmp[ipt];
            xcoord_tmp[ipt] = xRotate(x_tmp,y_tmp);
            ycoord_tmp[ipt] = yRotate(x_tmp,y_tmp);
        }
    }

    if(east_flag > 0){
        for (int ipt=1; ipt<patch_degree; ipt++){
            base_file >> x_tmp;
            base_file >> y_tmp;
            base_file >> weight_tmp[patch_degree + ipt*(patch_degree+1)];
            xcoord_tmp[patch_degree + ipt*(patch_degree+1)] = xRotate(x_tmp,y_tmp);
            ycoord_tmp[patch_degree + ipt*(patch_degree+1)] = yRotate(x_tmp,y_tmp);
        }
    }

    if(north_flag > 0){
        for (int ipt=1; ipt<patch_degree; ipt++){
            base_file >> x_tmp;
            base_file >> y_tmp;
            base_file >> weight_tmp[(patch_degree+1)*patch_degree+ipt];
            xcoord_tmp[(patch_degree+1)*patch_degree+ipt] = xRotate(x_tmp,y_tmp);
            ycoord_tmp[(patch_degree+1)*patch_degree+ipt] = yRotate(x_tmp,y_tmp);
        }
    }

    if(west_flag > 0){
        for (int ipt=1; ipt<patch_degree; ipt++){
            base_file >> x_tmp;
            base_file >> y_tmp;
            base_file >> weight_tmp[(patch_degree+1)*ipt];
            xcoord_tmp[(patch_degree+1)*ipt] = xRotate(x_tmp,y_tmp);
            ycoord_tmp[(patch_degree+1)*ipt] = yRotate(x_tmp,y_tmp);
        }
    }

    //--- last pass for linear interpolation
    //
    //

    if(south_flag == 0){
        for (int ipt=1; ipt<patch_degree; ipt++){
            double w = 1. - double(ipt)/double(patch_degree);
            xcoord_tmp[ipt] = xcoord_tmp[0]*w + xcoord_tmp[patch_degree]*(1.-w);
            ycoord_tmp[ipt] = ycoord_tmp[0]*w + ycoord_tmp[patch_degree]*(1.-w);
            weight_tmp[ipt] = weight_tmp[0]*w + weight_tmp[patch_degree]*(1.-w);
        }
    }

    if(east_flag == 0){
        for (int ipt=1; ipt<patch_degree; ipt++){
            double w = 1. - double(ipt)/double(patch_degree);
            xcoord_tmp[patch_degree + ipt*(patch_degree+1)] = xcoord_tmp[patch_degree]*w + xcoord_tmp[(patch_degree+1)*(patch_degree+1)-1]*(1.-w);
            ycoord_tmp[patch_degree + ipt*(patch_degree+1)] = ycoord_tmp[patch_degree]*w + ycoord_tmp[(patch_degree+1)*(patch_degree+1)-1]*(1.-w);
            weight_tmp[patch_degree + ipt*(patch_degree+1)] = weight_tmp[patch_degree]*w + weight_tmp[(patch_degree+1)*(patch_degree+1)-1]*(1.-w);
        }
    }

    if(north_flag == 0){
        for (int ipt=1; ipt<patch_degree; ipt++){
            double w = 1. - double(ipt)/double(patch_degree);
            xcoord_tmp[(patch_degree+1)*patch_degree+ipt] = xcoord_tmp[(patch_degree+1)*patch_degree]*w + xcoord_tmp[(patch_degree+1)*(patch_degree+1)-1]*(1.-w);
            ycoord_tmp[(patch_degree+1)*patch_degree+ipt] = ycoord_tmp[(patch_degree+1)*patch_degree]*w + ycoord_tmp[(patch_degree+1)*(patch_degree+1)-1]*(1.-w);
            weight_tmp[(patch_degree+1)*patch_degree+ipt] = weight_tmp[(patch_degree+1)*patch_degree]*w + weight_tmp[(patch_degree+1)*(patch_degree+1)-1]*(1.-w);
        }
    }

    if(west_flag == 0){
        for (int ipt=1; ipt<patch_degree; ipt++){
            double w = 1. - double(ipt)/double(patch_degree);
            xcoord_tmp[(patch_degree+1)*ipt] = xcoord_tmp[0]*w + xcoord_tmp[(patch_degree+1)*patch_degree]*(1.-w);
            ycoord_tmp[(patch_degree+1)*ipt] = ycoord_tmp[0]*w + ycoord_tmp[(patch_degree+1)*patch_degree]*(1.-w);
            weight_tmp[(patch_degree+1)*ipt] = weight_tmp[0]*w + weight_tmp[(patch_degree+1)*patch_degree]*(1.-w);
        }
    }


}
