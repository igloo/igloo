/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <math.h>
#include <iostream>

double Sign(double X, double Y) {
  if (Y<0.0) return (-fabs(X));
  else return (fabs(X));
}

double bessj0 (double X) {

const double
      P1=1.0, P2=-0.1098628627E-2, P3=0.2734510407E-4,
      P4=-0.2073370639E-5, P5= 0.2093887211E-6,
      Q1=-0.1562499995E-1, Q2= 0.1430488765E-3, Q3=-0.6911147651E-5,
      Q4= 0.7621095161E-6, Q5=-0.9349451520E-7,
      R1= 57568490574.0, R2=-13362590354.0, R3=651619640.7,
      R4=-11214424.18, R5= 77392.33017, R6=-184.9052456,
      S1= 57568490411.0, S2=1029532985.0, S3=9494680.718,
      S4= 59272.64853, S5=267.8532712, S6=1.0;
double
      AX,FR,FS,Z,FP,FQ,XX,Y, TMP;

  if (X==0.0) return 1.0;
  AX = fabs(X);
  if (AX < 8.0) {
    Y = X*X;
    FR = R1+Y*(R2+Y*(R3+Y*(R4+Y*(R5+Y*R6))));
    FS = S1+Y*(S2+Y*(S3+Y*(S4+Y*(S5+Y*S6))));
    TMP = FR/FS;
  }
  else {
    Z = 8./AX;
    Y = Z*Z;
    XX = AX-0.785398164;
    FP = P1+Y*(P2+Y*(P3+Y*(P4+Y*P5)));
    FQ = Q1+Y*(Q2+Y*(Q3+Y*(Q4+Y*Q5)));
    TMP = sqrt(0.636619772/AX)*(FP*cos(XX)-Z*FQ*sin(XX));
  }
  return TMP;
}

double bessj1 (double X) {

const double
  P1=1.0, P2=0.183105E-2, P3=-0.3516396496E-4, P4=0.2457520174E-5,
  P5=-0.240337019E-6,  P6=0.636619772,
  Q1= 0.04687499995, Q2=-0.2002690873E-3, Q3=0.8449199096E-5,
  Q4=-0.88228987E-6, Q5= 0.105787412E-6,
  R1= 72362614232.0, R2=-7895059235.0, R3=242396853.1,
  R4=-2972611.439,   R5=15704.48260,  R6=-30.16036606,
  S1=144725228442.0, S2=2300535178.0, S3=18583304.74,
  S4=99447.43394,    S5=376.9991397,  S6=1.0;

  double AX,FR,FS,Y,Z,FP,FQ,XX, TMP;

  AX = fabs(X);
  if (AX < 8.0) {
    Y = X*X;
    FR = R1+Y*(R2+Y*(R3+Y*(R4+Y*(R5+Y*R6))));
    FS = S1+Y*(S2+Y*(S3+Y*(S4+Y*(S5+Y*S6))));
    TMP = X*(FR/FS);
  }
  else {
    Z = 8.0/AX;
    Y = Z*Z;
    XX = AX-2.35619491;
    FP = P1+Y*(P2+Y*(P3+Y*(P4+Y*P5)));
    FQ = Q1+Y*(Q2+Y*(Q3+Y*(Q4+Y*Q5)));
    TMP = sqrt(P6/AX)*(cos(XX)*FP-Z*sin(XX)*FQ)*Sign(S6,X);
  }
  return TMP;
}

double solution_acoustic_annular(double *coord, double time, int var_id, double *coord_elt, double *func_param)
{
    double x = coord[0];
    double y = coord[1];
  
    double r1 = 1;
    double r2 = 4;

    double root1 = 3.83170597020751;
    double root2 = 16.4706300508776;

    double r = sqrt(x*x+y*y);
    double angle = atan2(y,x);

    double value;

    switch(var_id){

    case 0:
        value = bessj1(root1 + (r-r1)/(r2-r1)*(root2-root1)) * sin((root2-root1)/(r2-r1)*time)*cos(angle);
        break;
    case 1:
        value = bessj1(root1 + (r-r1)/(r2-r1)*(root2-root1)) * sin((root2-root1)/(r2-r1)*time)*sin(angle);
        break;
    case 2:
        value = bessj0(root1 + (r-r1)/(r2-r1)*(root2-root1)) * cos((root2-root1)/(r2-r1)*time);
        break;
    }

    return value;
}


double solution_acoustic_bump(double *coord, double time, int var_id, double *coord_elt, double *func_param)
{
  double x = coord[0];
  double y = coord[1];
  
  double value = 0;

  if(var_id == 2){

    double dist0 = sqrt( (x-2)*(x-2) + (y-1)*(y-1));
    double dist1 = sqrt( (x+2)*(x+2) + (y-2.5)*(y-2.5));

    double dist = fmin(dist0,dist1);
    
    if(dist < 0.25)
      value = 1.-sin(2*M_PI*dist)*sin(2*M_PI*dist);
  }
  return value;
}

