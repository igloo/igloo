/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <vector>
#include <math.h>

using namespace std;

// Constant uniform velocity
void uniformLaw(double time, double time_step, vector<double> *coord, vector<double> *vel, vector<double> *param, const vector<double> *init, const int subdomain_number)
{
    double Vx = 1;

	// param controls mesh velocity in x-direction
	if(param->size()>0)
		Vx = param->at(0);

	vel->at(0) = Vx;
	vel->at(1) = 0;

    coord->at(0) = init->at(0) + time*Vx;
    coord->at(1) = init->at(1);
}

// Constant rotation velocity
void rotationLaw(double time, double time_step, vector<double> *coord, vector<double> *vel, vector<double> *param, const vector<double> *init, const int subdomain_number)
{
    double rps = 1;

    // param controls rotations per second
    if(param->size()>0)
        rps = param->at(0);

    double omega = -rps*M_PI;
    double delta_theta = omega*time_step;

    double x = coord->at(0);
    double y = coord->at(1);

    coord->at(0) = cos(delta_theta)*x - sin(delta_theta)*y;
    coord->at(1) = sin(delta_theta)*x + cos(delta_theta)*y;
    vel->at(0) = -omega*coord->at(1);
    vel->at(1) = omega*coord->at(0);
}

// periodic deformation
void sinusoidalLaw(double time, double time_step, vector<double> *coord, vector<double> *vel, vector<double> *param, const vector<double> *init, const int subdomain_number)
{
	double Dx = 1.;
	double Dy = 1.;
	double Nx = 2.;
	double Ny = 2.;
	double coeff = sin(Nx*0.25*M_PI*init->at(0)) * sin(Ny*0.25*M_PI*init->at(1));

	vel->at(0) = Dx*coeff*sin(2*M_PI*time);
	vel->at(1) = Dy*coeff*sin(2*M_PI*time);

    coord->at(0) = init->at(0) - Dx*coeff*(cos(2*M_PI*time)-1.)/(2*M_PI);
    coord->at(1) = init->at(1) - Dy*coeff*(cos(2*M_PI*time)-1.)/(2*M_PI);
}


// accelerated move until prescribed velocity
void gradualStart(double time, double time_step, vector<double> *coord, vector<double> *vel, vector<double> *param, const vector<double> *init, const int subdomain_number)
{
	double U_ref = 0.5;
    double tau = 10.;

	// param controls reference speed
	if(param->size()>0)
		U_ref = param->at(0);

	if(time < tau){
		vel->at(0) = 0.5*U_ref*(1. + cos(M_PI*time/tau));
        coord->at(0) = init->at(0) + 0.5*U_ref*(time + tau/M_PI*sin(M_PI*time/tau));
    } else {
		vel->at(0) = 0.;
        coord->at(0) = init->at(0) + 0.5*U_ref*tau;
    }

	vel->at(1) = 0.;
    coord->at(1) = init->at(1);
}

// vertically moving cylinder
void oscillatingCylinder(double time, double time_step, vector<double> *coord, vector<double> *vel, vector<double> *param, const vector<double> *init, const int subdomain_number)
{
    // param controls rigid / deformable option
    bool rigid = true;
    if(param->size()>0)
        rigid = false;

    // Oscillation amplitude and frequency
	double A = 0.5;
	double Strouhal = 0.2250;
	double tau = 10.;
	double F = 0.875;
	double freq = F*Strouhal/tau;

	// Smoothing parameter for mesh deformation
	double d = 6.;
	int n = 2;

	double v_0 = A*2*M_PI*freq*cos(2*M_PI*freq*time);
    double x_0 = A*sin(2*M_PI*freq*time);
	double ls = 0.;
	if(!rigid){
		double y_0 = A*sin(2*M_PI*freq*time);
		ls = sqrt(pow(init->at(0),2)+pow(init->at(1)-y_0,2)) - 1.;
	}

	vel->at(0) = 0.;
	vel->at(1) = v_0*exp(-pow((ls/d),n));

    coord->at(0) = init->at(0);
    coord->at(1) = init->at(1) + x_0*exp(-pow((ls/d),n));
}

// channel with oscillating bodies
void oscillatingBodyChannel(double time, double time_step, vector<double> *coord, vector<double> *vel, vector<double> *param, const vector<double> *init, const int subdomain_number)
{
	// Oscillation amplitude and frequency
	double A = 0.04;
	double freq = 0.1;

    // body velocities
    double v_1 = A*2*M_PI*freq*cos(2*M_PI*freq*time);
    double v_2 = -A*2*M_PI*freq*cos(2*M_PI*freq*time);
    double v_3 = A*2*M_PI*freq*cos(2*M_PI*freq*time);
    double v_4 = -A*2*M_PI*freq*cos(2*M_PI*freq*time);

    // dependency regions
    double x0 = 0.3;
    double x01 = 1.3;
    double x1 = 2.3;

    double y01 = -0.25;
    double y1 = 0.;
    double y12 = 0.25;
    double y2 = 0.5;
    double y23 = 0.75;
    double y3 = 1.;
    double y34 = 1.25;
    double y4 = 1.5;
    double y45 = 1.75;


    double dx_int = 0.3;
    double dx_ext = 0.6;
    double dy_int = 0.2;
    double dy_ext = 0.25;

    double x = init->at(0);
    double y = init->at(1);

    double dx0 = fabs(x - x0);
    double dx1 = fabs(x - x1);

    double yc_1;
    double yc_2;
    double yc_3;
    double yc_4;

    // coordinates of body centers
    if(x<x01){
        yc_1 = y1 + A*sin(2*M_PI*freq*time);
        yc_2 = y2 - A*sin(2*M_PI*freq*time);
        yc_3 = y3 + A*sin(2*M_PI*freq*time);
        yc_4 = y4 - A*sin(2*M_PI*freq*time);
    } else {
        yc_1 = y1 - A*sin(2*M_PI*freq*time);
        yc_2 = y2 + A*sin(2*M_PI*freq*time);
        yc_3 = y3 - A*sin(2*M_PI*freq*time);
        yc_4 = y4 + A*sin(2*M_PI*freq*time);
    }

    double dy1 = fabs(y - yc_1);
    double dy2 = fabs(y - yc_2);
    double dy3 = fabs(y - yc_3);
    double dy4 = fabs(y - yc_4);

	double phi_x = 0.;
    double phi_y = 0.;

    // x-dumping
	if(dx0<=dx_int)
		phi_x = 1.;	// internal region (rigidly moving)
    else if(dx1<=dx_int)
    	phi_x = -1.;	// internal region (rigidly moving)
	else if (dx0<dx_ext)
		phi_x = 0.5*(1. + cos(M_PI*(dx0 - dx_int)/(dx_ext - dx_int)));	// transition region
    else if (dx1<dx_ext)
    	phi_x = -0.5*(1. + cos(M_PI*(dx1 - dx_int)/(dx_ext - dx_int)));	// transition region

    // by default : no move
	vel->at(0) = 0.;
    vel->at(1) = 0.;
    coord->at(0) = init->at(0);
    coord->at(1) = init->at(1);

    // y-dumping
    if( y>y01 && y<y12 ){
        if(dy1 < dy_int){
            phi_y = 1.;	// internal region (rigidly moving)
        } else if(y<yc_1){
            phi_y = (y - y01)/(yc_1-dy_int - y01);	// transition region down
        } else {
            phi_y = (y12 - y)/(y12 - yc_1-dy_int);		// transition region up
        }
        vel->at(1) = v_1*phi_x*phi_y;
        coord->at(1) += (yc_1-y1)*phi_x*phi_y;
    }

    if( y>y12 && y<y23 ){
        if(dy2 < dy_int){
            phi_y = 1.;	// internal region (rigidly moving)
        } else if(y<yc_2){
            phi_y = (y - y12)/(yc_2-dy_int - y12);	// transition region down
        } else {
            phi_y = (y23 - y)/(y23 - yc_2-dy_int);		// transition region up
        }
        vel->at(1) = v_2*phi_x*phi_y;
        coord->at(1) += (yc_2-y2)*phi_x*phi_y;
    }

    if( y>y23 && y<y34 ){
        if(dy3 < dy_int){
            phi_y = 1.;	// internal region (rigidly moving)
        } else if(y<yc_3){
            phi_y = (y - y23)/(yc_3-dy_int - y23);	// transition region down
        } else {
            phi_y = (y34 - y)/(y34 - yc_3-dy_int);		// transition region up
        }
        vel->at(1) = v_1*phi_x*phi_y;
        coord->at(1) += (yc_1-y1)*phi_x*phi_y;
    }

    if( y>y34 && y<y45 ){
        if(dy4 < dy_int){
            phi_y = 1.;	// internal region (rigidly moving)
        } else if(y<yc_4){
            phi_y = (y - y34)/(yc_4-dy_int - y34);	// transition region down
        } else {
            phi_y = (y45 - y)/(y45 - yc_4-dy_int);		// transition region up
        }
        vel->at(1) = v_2*phi_x*phi_y;
        coord->at(1) += (yc_2-y2)*phi_x*phi_y;
    }

}

// pitching for subsonic airfoil
void pitchingAirfoil(double time, double time_step, vector<double> *coord, vector<double> *vel, vector<double> *param, const vector<double> *init, const int subdomain_number)
{
	double Delta_theta = 20.;

	// param controls amplitude in degrees
	if(param->size()>0)
		Delta_theta = param->at(0);

	// Angle amplitude and frequency
	double A = Delta_theta*M_PI/180.;
	double freq = 0.1;

	// Centre of rotation for the mesh
	double x_c = 0.5;
	double y_c = 0.;

	// Define mesh movement regions
	double R_int = 1.;
	double R_ext = 4.;

	double r = sqrt(pow((coord->at(0) - x_c),2) + pow((coord->at(1) - y_c),2));
	double phi;

	if(r>=R_ext)
		phi = 0.;	// external region (fixed)
	else if(r<=R_int)
		phi = 1.;	// internal region (rigidly moving)
	else
		phi = 0.5*(1. + cos(M_PI*(r - R_int)/(R_ext - R_int)));	// transition region

    // movement law
	double omega = A*(2*M_PI*freq)*cos(2*M_PI*freq*time)*phi;

    double delta_theta = omega*time_step;
    double x = coord->at(0);
    double y = coord->at(1);

    coord->at(0) = x_c + cos(delta_theta)*(x - x_c) - sin(delta_theta)*(y - y_c);
    coord->at(1) = y_c + sin(delta_theta)*(x - x_c) + cos(delta_theta)*(y - y_c);

    vel->at(0) = -omega*(coord->at(1) - y_c);
    vel->at(1) = omega*(coord->at(0) - x_c);
}


// pitching for transonic airfoil
void pitchingTransonic(double time, double time_step, vector<double> *coord, vector<double> *vel, vector<double> *param, const vector<double> *init, const int subdomain_number)
{
	// Angle amplitude
	double Delta_theta = -2.51*M_PI/180.;

	// Frequency
	double k = 0.0814;
	double U_inf = 0.755;
	double freq = k*U_inf/M_PI;

	// Centre of rotation
	double x_c = 0.25;
	double y_c = 0.;

	// Define mesh movement regions
	double R_int = 1.;
	double R_ext = 2.;

	double r = sqrt(pow((coord->at(0) - x_c),2) + pow((coord->at(1) - y_c),2));
	double phi;

	if(r>=R_ext)
		phi = 0.;	// external region (fixed)
	else if(r<=R_int)
		phi = 1.;	// internal region (rigidly moving)
	else
		phi = 0.5*(1. + cos(M_PI*(r - R_int)/(R_ext - R_int)));	// transition region

    // movement law
	double omega = Delta_theta*(2*M_PI*freq)*cos(2*M_PI*freq*time)*phi;

    double delta_theta = omega*time_step;
    double x = coord->at(0);
    double y = coord->at(1);

    coord->at(0) = x_c + cos(delta_theta)*(x - x_c) - sin(delta_theta)*(y - y_c);
    coord->at(1) = y_c + sin(delta_theta)*(x - x_c) + cos(delta_theta)*(y - y_c);

    vel->at(0) = -omega*(coord->at(1) - y_c);
    vel->at(1) = omega*(coord->at(0) - x_c);
}

// pitching ellipse
void pitchingEllipse(double time, double time_step, vector<double> *coord, vector<double> *vel, vector<double> *param, const vector<double> *init, const int subdomain_number)
{
    double Delta_theta = 30.;

	// param controls amplitude in degrees
	if(param->size()>0)
		Delta_theta = param->at(0);

	// Angle amplitude and frequency
	double A = Delta_theta*M_PI/180.;
	double freq = 0.05;

	// Centre of rotation for the mesh
	double x_c = 0.;
	double y_c = 0.;

	// Define mesh movement regions
	double R_int = 1.5;
	double R_ext = 8.;

	double r = sqrt(pow((coord->at(0) - x_c),2) + pow((coord->at(1) - y_c),2));
	double phi;

	if(r>=R_ext)
		phi = 0.;	// external region (fixed)
	else if(r<=R_int)
		phi = 1.;	// internal region (rigidly moving)
	else
		phi = 0.5*(1. + cos(M_PI*(r - R_int)/(R_ext - R_int)));	// transition region

    // movement law
	double omega = A*(2*M_PI*freq)*cos(2*M_PI*freq*time)*phi;

    double delta_theta = omega*time_step;
    double x = coord->at(0);
    double y = coord->at(1);

    coord->at(0) = x_c + cos(delta_theta)*(x - x_c) - sin(delta_theta)*(y - y_c);
    coord->at(1) = y_c + sin(delta_theta)*(x - x_c) + cos(delta_theta)*(y - y_c);

    vel->at(0) = -omega*(coord->at(1) - y_c);
    vel->at(1) = omega*(coord->at(0) - x_c);
}
