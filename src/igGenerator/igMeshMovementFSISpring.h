/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <vector>
#include <math.h>

using namespace std;

// deformation according to 2D spring move
void springDeformation(double time, double time_step, vector<double> *coord, vector<double> *vel, vector<double> *param, const vector<double> *init, const int subdomain_number)
{
    // get deformation parameters
    double damping = param->at(0); // damping coefficient
    double v_x = param->at(1); // velocity along x
    double v_y = param->at(2); // velocity along y
    double om_z = param->at(3); // rotation velocity
    double t_x = param->at(4); // translation along x
    double t_y = param->at(5); // translation along y
    double r_z = param->at(6); // rotation angle

    // deformation law
    coord->at(0) = init->at(0) + damping * ( t_x + (cos(r_z)-1)*init->at(0) -sin(r_z)*init->at(1) );
    coord->at(1) = init->at(1) + damping * ( t_y + sin(r_z)*init->at(0) +(cos(r_z)-1)*init->at(1) );

    vel->at(0) = damping * ( v_x -sin(r_z)*om_z*init->at(0) -cos(r_z)*om_z*init->at(1) );
    vel->at(1) = damping * ( v_y +cos(r_z)*om_z*init->at(0) -sin(r_z)*om_z*init->at(1) );

}
