/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <math.h>
#include "igSolutionAnalyticShared.h"

////////////////////////////////////////////////////

double solution_euler_shock_vortex(double *coord, double time, int var_id, double *coord_elt, double *func_param)
{
  double x = coord[0];
  double y = coord[1];
  double x_elt = coord_elt[0];


  double u,v,rho,p;
  double r = sqrt((x-0.25)*(x-0.25) + (y-0.5)*(y-0.5));
  double a = 0.075;
  double b = 0.175;
  double M_s = 1.5;
  double M_v = 0.9;
  double g = 1.4;

  if(r>b)
  {

    if(x_elt<0.5)
    {
      // upstream condition
      u = M_s*sqrt(g);
      v = 0.;
      rho = 1.;
      p = 1.;
    }
    else
    {
      // downstream condition (determined from R-H relations)
      double ratio = (2.+(g-1.)*M_s*M_s)/((1.+g)*M_s*M_s);
      u = ratio*M_s*sqrt(g);
      v = 0.;
      rho = 1./ratio;
      p = 1. + 2.*g*(M_s*M_s - 1.)/(1.+g);
    }

  }
  else
  {
    // vortex condition
    double v_theta;
    double T_vor;
    if(r<a)
    {
      v_theta = M_v*sqrt(g)*r/a;
      T_vor = 1 + 0.5*((g-1.)/g)*M_v*M_v*g*( pow(a/(a*a-b*b),2.)*(a*a - 4.*b*b*log(a/b) - pow(b,4.)/(a*a)) - (1. - pow(r/a,2.)) );
    }
    else
    {
      v_theta = M_v*sqrt(g)*a*(r-b*b/r)/(a*a-b*b);
      T_vor = 1 + 0.5*((g-1.)/g)*M_v*M_v*g*pow(a/(a*a-b*b),2.)*(r*r - 4.*b*b*log(r/b) - pow(b,4.)/(r*r));
    }
    u = M_s*sqrt(g) - v_theta*(y-0.5)/(r+1e-8);
    v = v_theta*(x-0.25)/(r+1e-8);
    rho = pow(T_vor,1./(g-1.));
    p = pow(T_vor,g/(g-1.));
  }

  return conservativeVariable(rho, u, v, p, var_id);
}
