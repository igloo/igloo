/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <math.h>



double solution_advection_circular(double *coord, double time, int var_id, double *coord_elt, double *func_param)
{
    double x = coord[0];
    double y = coord[1];
    
    double value = exp(-(x-2.*cos(2*M_PI*time))*(x-2.*cos(2*M_PI*time))-(y-2.*sin(2*M_PI*time))*(y-2.*sin(2*M_PI*time)) );
    return value;
}
