/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igCoreExport.h>

#include <fstream>
#include <vector>
#include <functional>

class igCommunicator;
class igCell;
class igElement;
class igFace;
class igInterface;

using namespace std;

class IGCORE_EXPORT igMesh
{
public:
             igMesh(void);
    virtual ~igMesh(void);

public:
    void setCommunicator(igCommunicator *com);
    void setVariableNumber(int number);
    void setDerivativeNumber(int number);
    void setDimension(int dimension);
    void setExactSolutionId(int id);
    void setPartitionNumber(int number);
    void setCoordinates(vector<double> *coord);
    void setVelocities(vector<double> *vel);
    void setTime(double time);

public:
    void build(const string& filename);

public:
    void cleanInactiveCells(vector<double> *solution);

public:
    void refreshCommunicatorMaps(void);
    void refreshLevelArray(int new_elt_number, int new_shared_elt_number);
    void distributeLevelArray(void);

public:
    int elementNumber(void);
    int totalElementNumber(void);
    int activeElementNumber(void);
    int totalActiveElementNumber(void);
    int faceNumber(void);
    int totalFaceNumber(void);
    int activeFaceNumber(void);
    int totalActiveFaceNumber(void);
    int boundaryFaceNumber(void);
    int totalBoundaryFaceNumber(void);
    int activeBoundaryFaceNumber(void);
    int totalActiveBoundaryFaceNumber(void);
    int interfaceNumber(void);
    int sharedControlPointNumber(void);
    int sharedFaceNumber(void);
    int sharedElementNumber(void);
    int controlPointNumber(void);
    int variableNumber(void);
    int derivativeNumber(void);
    int meshDimension(void);
    int exactSolutionId(void);
    int partitionNumber(void);
    double meshTime(void);

    igElement* element(int index);
    igFace* face(int index);
    igFace* boundaryFace(int index);
    igInterface* interface(int index);
    double minElementSize(void);
    double minCharacteristicSize(void);
    int localElementId(int global_index);
    double minCoordinateX(void);
    double maxCoordinateX(void);
    double minCoordinateY(void);
    double maxCoordinateY(void);
    int elementLevel(int index, int direction);

public:
    void addElement(igElement *element);
    void addFace(igFace *face);
    void addBoundaryFace(igFace *face);
    void addInterface(igInterface *interface);
    void initializeGlobalToLocalMap(void);
    void setElementLevel(int index, int direction, int level);

    void updateMeshProperties(void);
    void updateGlobalMeshProperties(void);

    void computeElementNumber(void);
    void computeFaceNumber(void);
    void computeBoundaryFaceNumber(void);

private:
    igCommunicator *communicator;
    vector<int> *distributed_global_to_local;

    vector<int> *partition_map;
    vector<int> *shared_elt_map;
    vector<int> *shared_dof_map;
    vector< vector<int>* > *send_dof_map;
    vector< vector<int>* > *receive_dof_map;
    vector< vector<int>* > *send_elt_map;
    vector< vector<int>* > *receive_elt_map;

    int element_number;
    int total_element_number;
    int active_element_number;
    int total_active_element_number;
    vector<igElement*> element_list;
    vector<int> *element_level_list_0;
    vector<int> *element_level_list_1;

    int face_number;
    int total_face_number;
    int active_face_number;
    int total_active_face_number;
    vector<igFace*> face_list;

    int boundary_face_number;
    int total_boundary_face_number;
    int active_boundary_face_number;
    int total_active_boundary_face_number;
    vector<igFace*> boundary_list;

    int interface_number;
    vector<igInterface*> interface_list;

    vector<double> *coordinates;
    vector<double> *velocities;

    int control_point_number;

    int shared_face_number;
    int shared_dof_number;
    int shared_elt_number;

    int max_field_number;

    int variable_number;
    int dimension;
    int derivative_number;
    int exact_solution_id;
    int partition_number;

    double time;

private:
    int maxDegree(void);
    int minDegree(void);
    int maxControlPointNumber(void);
    int minControlPointNumber(void);
    int maxGaussPointNumber(void);
    int minGaussPointNumber(void);

    void computeControlPointNumber(void);
    void computeTotalElementNumber(void);
    void computeActiveElementNumber(void);
    void computeTotalActiveElementNumber(void);
    void computeTotalFaceNumber(void);
    void computeActiveFaceNumber(void);
    void computeTotalActiveFaceNumber(void);
    void computeTotalBoundaryFaceNumber(void);
    void computeActiveBoundaryFaceNumber(void);
    void computeTotalActiveBoundaryFaceNumber(void);
    void computeInterfaceNumber(void);

public:
    void linearize(void);
    void linearizeBoundary(igElement *elt, int start, int direction);
    void coonsMethod(igElement *elt);


};

//
// igMesh.h ends here
