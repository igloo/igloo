/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include "igFitting.h"

#include <igCoreExport>

#include <functional>
#include <vector>

using namespace std;

class IGCORE_EXPORT igFittingSurface : public igFitting
{
public:
     igFittingSurface(void);
    ~igFittingSurface(void) = default;

public:
    void setFunction(function<double(double *coord, double time, int var_id, double *coord_elt, double *func_param)> fun, int var_id, double *func_param);

public:
    void run(void);

public:
    double zControlPointFit(int index);

private:

    int var_id;

    vector<double> sol_z;

    function<double(double *coord, double time, int var_id, double *coord_elt, double *func_param)> fun;
    double *func_param;

};

//
// igFittingSurface.h ends here
