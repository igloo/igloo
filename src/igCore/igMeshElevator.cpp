/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igMeshElevator.h"

#include "igMesh.h"
#include "igCell.h"
#include "igElement.h"
#include "igFace.h"

#include <iostream>
#include <stdlib.h>
#include <iomanip>
#include <igAssert.h>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igMeshElevator::igMeshElevator(void)
{
    this->mesh = NULL;
    this->coordinates = NULL;
}

igMeshElevator::~igMeshElevator(void)
{

}


/////////////////////////////////////////////////////////////////////////////

void igMeshElevator::initialize(igMesh *mesh, vector<double> *coordinates)
{
    this->mesh = mesh;
    this->coordinates = coordinates;
}

void igMeshElevator::setGaussPointNumber(int number)
{
    this->gauss_pt_number = number;
}

/////////////////////////////////////////////////////////////////////////////

void igMeshElevator::elevate(void)
{
    int variable_number = mesh->variableNumber();
    int max_field_number = max(variable_number,mesh->derivativeNumber());
    max_field_number = max(max_field_number,mesh->meshDimension()+1);
    int dimension = mesh->meshDimension();

    int degree = mesh->element(0)->cellDegree();
    int degree_new = degree + 1;
    int internal_dof_number = mesh->elementNumber() * (degree_new+1)*(degree_new+1);

    int field_to_elevate = dimension + 1;


    int index;

    int ctrl_pts_per_elt_new = (degree_new+1)*(degree_new+1);

    vector<double> coord_tmp;
    coord_tmp.resize(degree+1);
    vector<double> coord_curve;
    coord_curve.resize(degree_new+1);
    vector<double> tmp_elevated;
    tmp_elevated.resize(ctrl_pts_per_elt_new*(dimension+1));

    // temporary coordinate array
    vector<double> *coordinates_tmp = new vector<double>;
    coordinates_tmp->resize(coordinates->size(),0.);

    for(int i=0; i<coordinates->size(); i++){
        coordinates_tmp->at(i) = coordinates->at(i);
    }

    // resize element array
    coordinates->resize(mesh->elementNumber()*ctrl_pts_per_elt_new*(dimension+1),0.);


    //------------- elements -------------------

    for (int iel=0; iel<mesh->elementNumber(); iel++){

        igElement *elt = mesh->element(iel);

        //--- first step : compute new coordinates and weights

        // first loop over lines of control net => treatment of direction Xi
        for (int icurve=0; icurve<degree+1; icurve++){

            // treatment of components
            for(int icomp=0; icomp<field_to_elevate; icomp++){

                // retrieve curve control points
                for(int ipt=0; ipt<degree+1; ipt++){

                    index = (degree+1)*icurve + ipt;

                    if(icomp < dimension) // geometry : coordinates * weights
                        coord_tmp.at(ipt) = coordinates_tmp->at(elt->globalId(index, icomp)) *
                            coordinates_tmp->at(elt->globalId(index, dimension));
                    else // geometry : weights
                        coord_tmp.at(ipt) = coordinates_tmp->at(elt->globalId(index, dimension));
                }

                // keep extremities
                coord_curve.at(0) = coord_tmp.at(0);
                coord_curve.at(degree_new) = coord_tmp.at(degree);

                // degree elevation procedure
                for (int ipt=1; ipt<degree_new; ipt++)
                    coord_curve.at(ipt) = double(ipt)/double(degree_new)*coord_tmp.at(ipt-1)
                        + (1.-double(ipt)/double(degree_new))*coord_tmp.at(ipt);

                // store control points
                for (int ipt=0; ipt<degree_new+1; ipt++){

                    index = (degree_new+1)*icurve + ipt;
                    tmp_elevated.at(ctrl_pts_per_elt_new*icomp + index) = coord_curve.at(ipt);
                }
            }
        }

        // second loop over lines of control net => treatment of direction Eta
        for (int icurve=0; icurve<degree_new+1; icurve++){

            // treatment of components
            for(int icomp=0; icomp<field_to_elevate; icomp++){

                // retrieve curve control points
                for(int ipt=0; ipt<degree+1; ipt++){

                    index = (degree_new+1)*ipt + icurve;

                    coord_tmp.at(ipt) = tmp_elevated.at(ctrl_pts_per_elt_new*icomp + index);
                }

                // keep extremities
                coord_curve.at(0) = coord_tmp.at(0);
                coord_curve.at(degree_new) = coord_tmp.at(degree);

                // degree elevation procedure
                for (int ipt=1; ipt<degree_new; ipt++)
                    coord_curve.at(ipt) = double(ipt)/double(degree_new)*coord_tmp.at(ipt-1)
                        + (1.-double(ipt)/double(degree_new))*coord_tmp.at(ipt);

                // store control points
                for (int ipt=0; ipt<degree_new+1; ipt++){

                    index = (degree_new+1)*ipt + icurve;
                    tmp_elevated.at(ctrl_pts_per_elt_new*icomp + index) = coord_curve.at(ipt);
                }
            }

            // recover NURBS formulation
            for(int icomp=0; icomp<dimension; icomp++){
                for (int ipt=0; ipt<degree_new+1; ipt++){
                    index = (degree_new+1)*ipt + icurve;
                    tmp_elevated.at(ctrl_pts_per_elt_new*icomp + index) /= tmp_elevated.at(ctrl_pts_per_elt_new*dimension + index);
                }
            }
        }

        //--- Second step : update element attributes

        // update degree and number of quadrature points
        elt->setDegree(degree_new);
        elt->setGaussPointNumberByDirection(this->gauss_pt_number);
        elt->initializeDegreesOfFreedom();

        // set dof map
        for (int ipt=0; ipt<elt->controlPointNumber(); ipt++){
            for (int ivar=0; ivar<max_field_number; ivar++){
                int index = internal_dof_number*ivar + ctrl_pts_per_elt_new*iel + ipt;
                elt->setGlobalId(ipt, ivar, index);
            }
        }

        // update control points and weights
        for (int icurve=0; icurve<degree_new+1; icurve++){
            for(int icomp=0; icomp<dimension+1; icomp++){
                for (int ipt=0; ipt<degree_new+1; ipt++){

                    index = (degree_new+1)*ipt + icurve;
                    coordinates->at(elt->globalId(index,icomp)) = tmp_elevated.at(ctrl_pts_per_elt_new*icomp + index);
                }
            }
        }
        elt->initializeGeometry();

    }
    delete coordinates_tmp;

    //----------------- faces --------------------

    int index_beg;
    int index_incr;
    int dof_index;

    vector<double> *coord = new vector<double>(dimension);

    for (int ifac=0; ifac<mesh->faceNumber(); ifac++){

        igFace *face = mesh->face(ifac);
        igElement *elt_left = mesh->element(face->leftElementIndex());
        igElement *elt_right = mesh->element(face->rightElementIndex());

        bool sign_left = false;
        if(face->leftSense()<0)
            sign_left = true;
        bool sign_right = false;
        if(face->rightSense()<0)
            sign_right = true;

        // update face attributes
        face->setDegree(degree_new);
        face->setGaussPointNumberByDirection(this->gauss_pt_number);
        face->initializeDegreesOfFreedom();

        // update left dof map
        switch(face->leftOrientation()){
        case 0:
            index_beg = 0;
            index_incr = 1;
            if(sign_left){
                index_beg = degree_new;
                index_incr *= -1;
            }
            break;
        case 1:
            index_beg = degree_new;
            index_incr = degree_new+1;
            if(sign_left){
                index_beg = (degree_new+1)*(degree_new+1)-1;
                index_incr *= -1;
            }
            break;
        case 2:
            index_beg = degree_new*(degree_new+1);
            index_incr = 1;
            if(sign_left){
                index_beg = (degree_new+1)*(degree_new+1)-1;
                index_incr *= -1;
            }
            break;
        case 3:
            index_beg = 0;
            index_incr = degree_new+1;
            if(sign_left){
                index_beg = (degree_new+1)*degree_new;
                index_incr *= -1;
            }
            break;
        }

        for (int idof=0; idof<face->controlPointNumber(); idof++){
            for (int ivar=0; ivar<max_field_number; ivar++)
                face->setLeftDofMap(idof,ivar,elt_left->globalId(index_beg+index_incr*idof,ivar));
            face->setLeftPointMap(idof, index_beg+index_incr*idof);
        }

        // update right dof map
        switch(face->rightOrientation()){
        case 0:
            index_beg = 0;
            index_incr = 1;
            if(sign_right){
                index_beg = degree_new;
                index_incr *= -1;
            }
            break;
        case 1:
            index_beg = degree_new;
            index_incr = degree_new+1;
            if(sign_right){
                index_beg = (degree_new+1)*(degree_new+1)-1;
                index_incr *= -1;
            }
            break;
        case 2:
            index_beg = degree_new*(degree_new+1);
            index_incr = 1;
            if(sign_right){
                index_beg = (degree_new+1)*(degree_new+1)-1;
                index_incr *= -1;
            }
            break;
        case 3:
            index_beg = 0;
            index_incr = degree_new+1;
            if(sign_right){
                index_beg = (degree_new+1)*degree_new;
                index_incr *= -1;
            }
            break;
        }

        for (int idof=0; idof<face->controlPointNumber(); idof++){
            for (int ivar=0; ivar<max_field_number; ivar++)
                face->setRightDofMap(idof,ivar,elt_right->globalId(index_beg+index_incr*idof,ivar));
            face->setRightPointMap(idof, index_beg+index_incr*idof);
        }

        // update geometry & normal
        face->initializeGeometry();

        for(int idim=0; idim<dimension; idim++)
            coord->at(idim) = elt_left->barycenter(idim);
        face->initializeNormal(coord);

    }

    //----------------- boundary faces --------------------

    for (int ifac=0; ifac<mesh->boundaryFaceNumber(); ifac++){

        igFace *face = mesh->boundaryFace(ifac);
        igElement *elt_left = mesh->element(face->leftElementIndex());

        // update face attributes
        face->setDegree(degree_new);
        face->setGaussPointNumberByDirection(this->gauss_pt_number);
        face->initializeDegreesOfFreedom();

        // update left dof map
        switch(face->leftOrientation()){
        case 0:
            index_beg = 0;
            index_incr = 1;
            break;
        case 1:
            index_beg = degree_new;
            index_incr = degree_new+1;
            break;
        case 2:
            index_beg = degree_new*(degree_new+1);
            index_incr = 1;
            break;
        case 3:
            index_beg = 0;
            index_incr = degree_new+1;
            break;
        }

        for (int idof=0; idof<face->controlPointNumber(); idof++){
            for (int ivar=0; ivar<max_field_number; ivar++)
                face->setLeftDofMap(idof,ivar,elt_left->globalId(index_beg+index_incr*idof,ivar));
            face->setLeftPointMap(idof, index_beg+index_incr*idof);
        }

        // update geometry & normal
        face->initializeGeometry();

        for(int idim=0; idim<dimension; idim++)
            coord->at(idim) = elt_left->barycenter(idim);
        face->initializeNormal(coord);

    }

    delete coord;



}
