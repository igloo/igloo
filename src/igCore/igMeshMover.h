/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igCoreExport.h>

#include <vector>
#include <functional>
#include <string>

using namespace std;

class igMesh;
class igElement;
class igFace;
class igInterface;
class igCommunicator;

class IGCORE_EXPORT igMeshMover
{
public:
	igMeshMover(igMesh *mesh, vector<double> *coordinates, vector<double> *velocities);
	virtual ~igMeshMover(void);

public:
    void setCommunicator(igCommunicator *communicator);

public:
	bool rigidMotion(void);

	virtual void setDeformation(vector<double> *deformation);
    virtual void setInterfaceFSI(vector<double> *displacements, vector<double> *velocities);
    virtual void setFractionalStep(double step);

    virtual void initializeFractionalStep(void);
	virtual void initializeVelocity(double initial_time);
    virtual void initializeMapping(vector<double> *knot_vector, vector<double> *pt_x, vector<double> *pt_y);
	virtual void initializeMovement(const string& integrator, const string& movement_type, vector<double> *param_ALE, int max_refine_level);
    virtual bool move(double solver_time);

    void evalCoordinateAndVelocity(double solver_time);
    void halfFaceCorrection(void);

protected:
    void splitFace(vector<double> *x_field, vector<double> *x_split_field,vector<double> *y_field, vector<double> *y_split_field, vector<double> *w_field, vector<double> *w_split_field,bool first_child);
    void refineCoordinateAndVelocity(void);

protected:
    function<void(double, double, vector<double>*, vector<double>*,  vector<double>*, const vector<double>*,  const int)> mesh_movement_law;

    virtual void retrieveParameters(int iel,int icp);

protected:
    igMesh *mesh;
    igCommunicator *communicator;

    vector<double> *point_coord;
	vector<double> *point_vel;
	vector<double> *initial_coord;

    vector<double> *initial_coord_level0;

    vector<double> *coordinates;
	vector<double> *velocities;

    string movement_law;
    vector<double> *movement_param;
    bool rigid;

    bool adaptive_mesh;
    int refine_max_level;

};

//
// igMeshMover.h ends here
