/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igCoreExport.h>

#include <vector>

class igFace;

using namespace std;

class IGCORE_EXPORT igInterface
{

public:
	igInterface(vector<double> *coord, vector<double> *vel);
	~igInterface(void) = default;

	void setId(int id);
	void setDimension(int dimension);
	void setMidParam(double param);
	void setSplitParam(double param);
	void setParentFace(igFace *face);
	void setChildFace(igFace *face, bool left);
	void setCenter(double x_c, double y_c);

	void setParentActive(void);
	void setChildActive(void);

	int interfaceId(void);
	double midParam(void);
	double splitParam(void);

	igFace* parentFace(void);
	igFace* childFace(bool left);

	double computeMidParam(void);
	double computeSplitParam(void);

	void split(void);
	void updateGeometry(void);

private:
	double computeDeltaTheta(const vector<double> &coord1, const vector<double> &coord2);

private:
	igFace *parent_face;
	igFace *left_child_face;
	igFace *right_child_face;

	bool split_active;
	int id;

	int degree;
	int control_point_number_by_direction;
	int dimension;

	double mid_param;
	double split_param;

	double x_center, y_center;

    vector<double> *coordinates;
    vector<double> *velocities;

};

//
// igInterface.h ends here
