/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igElement.h"

#include "igQuadrature.h"

#include <iostream>
#include <math.h>
#include "igBasisBezier.h"

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igElement::igElement() : igCell()
{

}

/////////////////////////////////////////////////////////////////////////////

void igElement::setGlobalId(int local_id, int var_id, int global_id)
{
    global_id_list[var_id*control_point_number+local_id] = global_id;
    return;
}

void igElement::setPartition(int partition)
{
    element_partition = partition;
    return;
}

void igElement::setSubdomain(int subdomain)
{
	this->element_subdomain = subdomain;
}

void igElement::setControlPointCoordinate(int index, int component, double coord)
{
    this->coordinates->at(globalId(index, component)) = coord;
}

void igElement::setControlPointCoordinates(int index, vector<double> *coord)
{
    for(int idim=0; idim<physical_dimension; idim++)
        this->coordinates->at(globalId(index, idim)) = coord->at(idim);
}

void igElement::setControlPointWeight(int index, double weight)
{
    this->coordinates->at(globalId(index, physical_dimension)) = weight;
}

void igElement::setControlPointVelocity(int index, int component, double vel)
{
    this->velocities->at(globalId(index, component)) = vel;
}

void igElement::setControlPointVelocity(int index, vector<double> *vel)
{
    for(int idim=0; idim<physical_dimension; idim++)
        this->velocities->at(globalId(index, idim)) = vel->at(idim);
}

void igElement::setFaceId(int orientation, int id)
{
    this->face_list.at(orientation) = id;
}

/////////////////////////////////////////////////////////////////////////////

double igElement::controlPointCoordinate(int index, int component) const
{
    return this->coordinates->at(this->globalId(index, component));
}

void igElement::controlPointCoordinates(int index, vector<double> *coord)
{
    for(int idim=0; idim<physical_dimension; idim++)
        coord->at(idim) = this->coordinates->at(globalId(index, idim));
}

double igElement::controlPointWeight(int index) const
{
    return this->coordinates->at(this->globalId(index,this->physical_dimension));
}

double igElement::controlPointVelocityComponent(int index, int component) const
{
    return this->velocities->at(this->globalId(index, component));
}

void igElement::controlPointVelocity(int index, vector<double> *vel)
{
    for(int idim=0; idim<physical_dimension; idim++)
        vel->at(idim) = this->velocities->at(globalId(index, idim));
}

double igElement::radius(void) const
{
    return element_radius;
}

int igElement::partition(void)
{
    return element_partition;
}

int igElement::subdomain(void)
{
	return element_subdomain;
}

double igElement::barycenter(int component) const
{
    double coord = 0.;
    for (int i = 0; i < control_point_number; ++i) {
        coord += controlPointCoordinate(i, component);
    }
    return coord / control_point_number;
}

int igElement::globalId(int local_id, int var_id) const
{
    return global_id_list[var_id*control_point_number+local_id];
}

int *igElement::globalIdPtr(int var_id)
{
    return &global_id_list[var_id*control_point_number];
}

int igElement::faceId(int orientation)
{
    return this->face_list.at(orientation);
}

/////////////////////////////////////////////////////////////////////////////

void igElement::initializeDegreesOfFreedom(void)
{
    // allocate control point list
    control_point_number_by_direction = degree+1;
    control_point_number = 1;
    for (int idim=0; idim<physical_dimension; idim++)
        control_point_number *= control_point_number_by_direction;

    // allocate gauss point list
    gauss_point_number = 1;
    for (int idim=0; idim<physical_dimension; idim++)
        gauss_point_number *= gauss_point_number_by_direction;
    gauss_point_coord_list.assign(gauss_point_number*physical_dimension,0.);
    gauss_point_physical_coord_list.assign(gauss_point_number*physical_dimension,0.);
    gauss_point_weight_list.assign(gauss_point_number,0.);

    // allocate gauss point attribute lists
    gauss_point_value_list.assign(gauss_point_number*control_point_number,0.);
    gauss_point_velocity_list.assign(gauss_point_number*physical_dimension,0.);
    gauss_point_gradient_list.assign(gauss_point_number*control_point_number*physical_dimension,0.);
    gauss_point_jacobian_list.assign(gauss_point_number,0.);
    gauss_point_jacobian_matrix_list.assign(gauss_point_number*physical_dimension*parametric_dimension,0.);

    // allocate global id list
    int max_field_number = max(variable_number,derivative_number);
    max_field_number = max(max_field_number,physical_dimension+1);
    global_id_list.assign(control_point_number*max_field_number,0);

    // compute the Gauss point properties
    igQuadrature quadrature;
    quadrature.initializeGaussPoints(physical_dimension,gauss_point_number_by_direction, gauss_point_coord_list, gauss_point_weight_list);

    // element-face map (3D setting by default)
    face_list.resize(6,-1);
}

void igElement::initializeGeometry(void)
{
    double grad0, grad1, grad2;
    double mat_00, mat_01, mat_02, mat_10, mat_11, mat_12, mat_20, mat_21, mat_22;
    double x_0, y_0, z_0, x_1, y_1, z_1, x_2, y_2, z_2, x_3, y_3, z_3;
    double dist2min;

    // evaluate basis functions at Gauss points
    this->evaluateGaussPoints();

    // evaluate Jacobian data
    int index = 0;
    for (int ipt = 0; ipt < gauss_point_number; ++ipt) {

        // components of the Jacobian matrix
        for (int i=0; i<physical_dimension; i++){
            for (int j = 0; j < parametric_dimension; ++j) {

                double jacobian = 0;

                for (int ifun = 0; ifun < control_point_number; ++ifun) {
                    jacobian += gaussPointGradientValue(ipt, ifun, j) * this->controlPointCoordinate(ifun, i);
                }
                gauss_point_jacobian_matrix_list[index] = jacobian;
                ++index;
            }
        }

        // determinant of the matrix
        switch(physical_dimension){

        case 1:
            gauss_point_jacobian_list[ipt] = this->gaussPointJacobianMatrix(ipt, 0, 0);
            break;
        case 2:
            gauss_point_jacobian_list[ipt] = this->gaussPointJacobianMatrix(ipt, 0, 0) * this->gaussPointJacobianMatrix(ipt, 1, 1)
                                           - this->gaussPointJacobianMatrix(ipt, 1, 0) * this->gaussPointJacobianMatrix(ipt, 0, 1);
            break;
        case 3:
            gauss_point_jacobian_list[ipt] = this->gaussPointJacobianMatrix(ipt, 0, 0) * ( this->gaussPointJacobianMatrix(ipt, 1, 1)*this->gaussPointJacobianMatrix(ipt, 2, 2) - this->gaussPointJacobianMatrix(ipt, 2, 1)*this->gaussPointJacobianMatrix(ipt, 1, 2))
                                           - this->gaussPointJacobianMatrix(ipt, 0, 1) * ( this->gaussPointJacobianMatrix(ipt, 1, 0)*this->gaussPointJacobianMatrix(ipt, 2, 2) - this->gaussPointJacobianMatrix(ipt, 2, 0)*this->gaussPointJacobianMatrix(ipt, 1, 2))
                                           + this->gaussPointJacobianMatrix(ipt, 0, 2) * ( this->gaussPointJacobianMatrix(ipt, 1, 0)*this->gaussPointJacobianMatrix(ipt, 2, 1) - this->gaussPointJacobianMatrix(ipt, 2, 0)*this->gaussPointJacobianMatrix(ipt, 1, 1));
            break;
        }

        // basis function gradients with respect to cartesian coordinates
        // multiplication by the inverse transposed Jacobian matrix (comatrix method)
        switch(physical_dimension){

        case 1:
            for (int ifun = 0; ifun < control_point_number; ++ifun) {
                gauss_point_gradient_list[(ipt*control_point_number+ifun)*physical_dimension] = this->gaussPointGradientValue(ipt, ifun, 0) / this->gaussPointJacobianMatrix(ipt, 0, 0);
            }
            break;

        case 2:

            mat_00 =  this->gaussPointJacobianMatrix(ipt, 1, 1) / this->gaussPointJacobian(ipt);
            mat_01 = -this->gaussPointJacobianMatrix(ipt, 1, 0) / this->gaussPointJacobian(ipt);
            mat_10 = -this->gaussPointJacobianMatrix(ipt, 0, 1) / this->gaussPointJacobian(ipt);
            mat_11 =  this->gaussPointJacobianMatrix(ipt, 0, 0) / this->gaussPointJacobian(ipt);

            for (int ifun = 0; ifun < control_point_number; ++ifun) {

                grad0 = this->gaussPointGradientValue(ipt, ifun, 0)*mat_00 + this->gaussPointGradientValue(ipt, ifun, 1)*mat_01;
                grad1 = this->gaussPointGradientValue(ipt, ifun, 0)*mat_10 + this->gaussPointGradientValue(ipt, ifun, 1)*mat_11;

                gauss_point_gradient_list[(ipt*control_point_number+ifun)*physical_dimension] = grad0;
                gauss_point_gradient_list[(ipt*control_point_number+ifun)*physical_dimension+1] = grad1;

            }
            break;

        case 3:

            mat_00 =  (this->gaussPointJacobianMatrix(ipt, 1, 1)*this->gaussPointJacobianMatrix(ipt, 2, 2) - this->gaussPointJacobianMatrix(ipt, 2, 1)*this->gaussPointJacobianMatrix(ipt, 1, 2))/this->gaussPointJacobian(ipt);
            mat_01 = -(this->gaussPointJacobianMatrix(ipt, 1, 0)*this->gaussPointJacobianMatrix(ipt, 2, 2) - this->gaussPointJacobianMatrix(ipt, 2, 0)*this->gaussPointJacobianMatrix(ipt, 1, 2))/this->gaussPointJacobian(ipt);
            mat_02 =  (this->gaussPointJacobianMatrix(ipt, 1, 0)*this->gaussPointJacobianMatrix(ipt, 2, 1) - this->gaussPointJacobianMatrix(ipt, 2, 0)*this->gaussPointJacobianMatrix(ipt, 1, 1))/this->gaussPointJacobian(ipt);
            mat_10 = -(this->gaussPointJacobianMatrix(ipt, 0, 1)*this->gaussPointJacobianMatrix(ipt, 2, 2) - this->gaussPointJacobianMatrix(ipt, 2, 1)*this->gaussPointJacobianMatrix(ipt, 0, 2))/this->gaussPointJacobian(ipt);
            mat_11 =  (this->gaussPointJacobianMatrix(ipt, 0, 0)*this->gaussPointJacobianMatrix(ipt, 2, 2) - this->gaussPointJacobianMatrix(ipt, 2, 0)*this->gaussPointJacobianMatrix(ipt, 0, 2))/this->gaussPointJacobian(ipt);
            mat_12 = -(this->gaussPointJacobianMatrix(ipt, 0, 0)*this->gaussPointJacobianMatrix(ipt, 2, 1) - this->gaussPointJacobianMatrix(ipt, 2, 0)*this->gaussPointJacobianMatrix(ipt, 0, 1))/this->gaussPointJacobian(ipt);
            mat_20 =  (this->gaussPointJacobianMatrix(ipt, 0, 1)*this->gaussPointJacobianMatrix(ipt, 1, 2) - this->gaussPointJacobianMatrix(ipt, 1, 1)*this->gaussPointJacobianMatrix(ipt, 0, 2))/this->gaussPointJacobian(ipt);
            mat_21 = -(this->gaussPointJacobianMatrix(ipt, 0, 0)*this->gaussPointJacobianMatrix(ipt, 1, 2) - this->gaussPointJacobianMatrix(ipt, 1, 0)*this->gaussPointJacobianMatrix(ipt, 0, 2))/this->gaussPointJacobian(ipt);
            mat_22 =  (this->gaussPointJacobianMatrix(ipt, 0, 0)*this->gaussPointJacobianMatrix(ipt, 1, 1) - this->gaussPointJacobianMatrix(ipt, 1, 0)*this->gaussPointJacobianMatrix(ipt, 0, 1))/this->gaussPointJacobian(ipt);

            for (int ifun = 0; ifun < control_point_number; ++ifun) {

                grad0 = this->gaussPointGradientValue(ipt, ifun, 0)*mat_00 + this->gaussPointGradientValue(ipt, ifun, 1)*mat_01 + this->gaussPointGradientValue(ipt, ifun, 2)*mat_02;
                grad1 = this->gaussPointGradientValue(ipt, ifun, 0)*mat_10 + this->gaussPointGradientValue(ipt, ifun, 1)*mat_11 + this->gaussPointGradientValue(ipt, ifun, 2)*mat_12;
                grad2 = this->gaussPointGradientValue(ipt, ifun, 0)*mat_20 + this->gaussPointGradientValue(ipt, ifun, 1)*mat_21 + this->gaussPointGradientValue(ipt, ifun, 2)*mat_22;

                gauss_point_gradient_list[(ipt*control_point_number+ifun)*physical_dimension] = grad0;
                gauss_point_gradient_list[(ipt*control_point_number+ifun)*physical_dimension+1] = grad1;
                gauss_point_gradient_list[(ipt*control_point_number+ifun)*physical_dimension+2] = grad2;

            }

            break;
        }

        for (int idim=0; idim<physical_dimension; idim++)
            gauss_point_physical_coord_list[ipt*physical_dimension+idim] = 0.;

        // gauss point physical coordinates
        for (int ifun = 0; ifun < control_point_number; ++ifun) {
            for (int idim = 0; idim < physical_dimension; ++idim) {
                gauss_point_physical_coord_list[ipt*physical_dimension+idim] += this->gaussPointFunctionValue(ipt, ifun) * this->controlPointCoordinate(ifun, idim);
            }
        }

    }

    // computation of the element radius and area

    switch (physical_dimension) {

    case 1:

        element_radius = (controlPointCoordinate(degree, 0) - controlPointCoordinate(0, 0))/2.;
        cell_area = 2. * element_radius;
        break;

    case 2:

        x_0 = this->controlPointCoordinate(0, 0);
        y_0 = this->controlPointCoordinate(0, 1);
        x_1 = this->controlPointCoordinate(degree, 0);
        y_1 = this->controlPointCoordinate(degree, 1);
        x_2 = this->controlPointCoordinate(control_point_number-degree-1, 0);
        y_2 = this->controlPointCoordinate(control_point_number-degree-1, 1);

        dist2min = min( (x_1-x_0)*(x_1-x_0)+(y_1-y_0)*(y_1-y_0) , (x_2-x_0)*(x_2-x_0)+(y_2-y_0)*(y_2-y_0) );

        element_radius = sqrt(dist2min)/2.;

        cell_area = 0.;
        for (int k = 0; k < gauss_point_number; ++k) {
            double gauss_point_weight =  this->gaussPointWeight(k);
            double gauss_point_jacobian = this->gaussPointJacobian(k);
            cell_area += gauss_point_weight * gauss_point_jacobian;
        }
        break;

    case 3:

        x_0 = this->controlPointCoordinate(0, 0);
        y_0 = this->controlPointCoordinate(0, 1);
        z_0 = this->controlPointCoordinate(0, 2);
        x_1 = this->controlPointCoordinate(degree, 0);
        y_1 = this->controlPointCoordinate(degree, 1);
        z_1 = this->controlPointCoordinate(degree, 2);
        x_2 = this->controlPointCoordinate(degree*(degree+1), 0);
        y_2 = this->controlPointCoordinate(degree*(degree+1), 1);
        z_2 = this->controlPointCoordinate(degree*(degree+1), 2);
        x_3 = this->controlPointCoordinate(degree*(degree+1)*(degree+1), 0);
        y_3 = this->controlPointCoordinate(degree*(degree+1)*(degree+1), 1);
        z_3 = this->controlPointCoordinate(degree*(degree+1)*(degree+1), 2);

        double d01 = (x_1-x_0)*(x_1-x_0)+(y_1-y_0)*(y_1-y_0)+(z_1-z_0)*(z_1-z_0);
        double d02 = (x_2-x_0)*(x_2-x_0)+(y_2-y_0)*(y_2-y_0)+(z_2-z_0)*(z_2-z_0);
        double d03 = (x_3-x_0)*(x_3-x_0)+(y_3-y_0)*(y_3-y_0)+(z_3-z_0)*(z_3-z_0);
        dist2min = min(  d01 , d02 );
        dist2min = min( dist2min , d03 );

        element_radius = sqrt(dist2min)/2.;

        cell_area = 0.;
        for (int k = 0; k < gauss_point_number; ++k) {
            double gauss_point_weight =  this->gaussPointWeight(k);
            double gauss_point_jacobian = this->gaussPointJacobian(k);
            cell_area += gauss_point_weight * gauss_point_jacobian;
        }
        break;
    }
}

/////////////////////////////////////////////////////////////////////////////

void igElement::evaluateGaussPoints(void)
{
    switch (physical_dimension) {

    case 1: {

        vector<double> values(control_point_number_by_direction);
        igBasisBezier basis(degree);

        for (int ipt = 0; ipt < gauss_point_number; ++ipt) {

            double var = gaussPointParametricCoordinate(ipt, 0);

            // function values
            basis.evalFunction(var, values);
            for (int ictl = 0; ictl < control_point_number; ++ictl) {
                gauss_point_value_list[ipt*control_point_number+ictl] = values[ictl];
            }

            // gradient values
            basis.evalGradient(var,values);
            for (int ictl = 0; ictl < control_point_number; ++ictl) {
                gauss_point_gradient_list[ipt*control_point_number+ictl] = values[ictl];
            }
        }
        break;}

    case 2: {

        vector<double> values1(control_point_number_by_direction);
        vector<double> values2(control_point_number_by_direction);

        vector<double> grad_values1(control_point_number_by_direction);
        vector<double> grad_values2(control_point_number_by_direction);

        igBasisBezier basis1(degree);
        igBasisBezier basis2(degree);

        int index_fun = 0;
        int index_grad = 0;

        for (int ipt = 0; ipt < gauss_point_number; ++ipt) {

            double var2 = gaussPointParametricCoordinate(ipt, 1);
            basis2.evalFunction(var2, values2);
            basis2.evalGradient(var2, grad_values2);

            double var1 = gaussPointParametricCoordinate(ipt, 0);
            basis1.evalFunction(var1, values1);
            basis1.evalGradient(var1, grad_values1);

            double weighted_sum = 0.;
            double weighted_sum_grad1 = 0.;
            double weighted_sum_grad2 = 0.;
            int index_sum = 0;
            for (int ictl2 = 0; ictl2 < control_point_number_by_direction; ++ictl2) {
                for (int ictl1 = 0; ictl1 < control_point_number_by_direction; ++ictl1) {
                    weighted_sum += values1[ictl1]*values2[ictl2]*controlPointWeight(index_sum);
                    weighted_sum_grad1 += grad_values1[ictl1]*values2[ictl2]*controlPointWeight(index_sum);
                    weighted_sum_grad2 += values1[ictl1]*grad_values2[ictl2]*controlPointWeight(index_sum);
                    ++index_sum;
                }
            }

            for (int ictl2 = 0; ictl2 < control_point_number_by_direction; ++ictl2) {
                for (int ictl1 = 0; ictl1 < control_point_number_by_direction; ++ictl1) {

                    int index_w = ictl2*control_point_number_by_direction+ictl1;

                    gauss_point_value_list[index_fun] = values1[ictl1]*values2[ictl2]*controlPointWeight(index_w)/weighted_sum;

                    gauss_point_gradient_list[index_grad] = ( grad_values1[ictl1]*values2[ictl2]*weighted_sum -  values1[ictl1]*values2[ictl2]*weighted_sum_grad1 )
                        / (weighted_sum*weighted_sum) * controlPointWeight(index_w);

                    gauss_point_gradient_list[index_grad+1] = ( values1[ictl1]*grad_values2[ictl2]*weighted_sum - values1[ictl1]*values2[ictl2]*weighted_sum_grad2 )
                        / (weighted_sum*weighted_sum) * controlPointWeight(index_w);

                    ++index_fun;
                    index_grad +=2;
                }
            }

        }
        break;}

    case 3: {

            vector<double> values1(control_point_number_by_direction);
            vector<double> values2(control_point_number_by_direction);
            vector<double> values3(control_point_number_by_direction);

            vector<double> grad_values1(control_point_number_by_direction);
            vector<double> grad_values2(control_point_number_by_direction);
            vector<double> grad_values3(control_point_number_by_direction);

            igBasisBezier basis1(degree);
            igBasisBezier basis2(degree);
            igBasisBezier basis3(degree);

            int index_fun = 0;
            int index_grad = 0;

            for (int ipt = 0; ipt < gauss_point_number; ++ipt) {

                double var3 = gaussPointParametricCoordinate(ipt, 2);
                basis3.evalFunction(var3, values3);
                basis3.evalGradient(var3, grad_values3);

                double var2 = gaussPointParametricCoordinate(ipt, 1);
                basis2.evalFunction(var2, values2);
                basis2.evalGradient(var2, grad_values2);

                double var1 = gaussPointParametricCoordinate(ipt, 0);
                basis1.evalFunction(var1, values1);
                basis1.evalGradient(var1, grad_values1);

                double weighted_sum = 0.;
                double weighted_sum_grad1 = 0.;
                double weighted_sum_grad2 = 0.;
                double weighted_sum_grad3 = 0.;
                int index_w = 0;
                for (int ictl3 = 0; ictl3 < control_point_number_by_direction; ++ictl3) {
                    for (int ictl2 = 0; ictl2 < control_point_number_by_direction; ++ictl2) {
                        for (int ictl1 = 0; ictl1 < control_point_number_by_direction; ++ictl1) {
                            weighted_sum += values1[ictl1]*values2[ictl2]*values3[ictl3]*controlPointWeight(index_w);
                            weighted_sum_grad1 += grad_values1[ictl1]*values2[ictl2]*values3[ictl3]*controlPointWeight(index_w);
                            weighted_sum_grad2 += values1[ictl1]*grad_values2[ictl2]*values3[ictl3]*controlPointWeight(index_w);
                            weighted_sum_grad3 += values1[ictl1]*values2[ictl2]*grad_values3[ictl3]*controlPointWeight(index_w);
                            ++index_w;
                        }
                    }
                }

                index_w = 0;
                for (int ictl3 = 0; ictl3 < control_point_number_by_direction; ++ictl3) {
                    for (int ictl2 = 0; ictl2 < control_point_number_by_direction; ++ictl2) {
                        for (int ictl1 = 0; ictl1 < control_point_number_by_direction; ++ictl1) {

                            gauss_point_value_list[index_fun] = values1[ictl1]*values2[ictl2]*values3[ictl3]*controlPointWeight(index_w)/weighted_sum;

                            gauss_point_gradient_list[index_grad] = ( grad_values1[ictl1]*values2[ictl2]*values3[ictl3]*weighted_sum -  values1[ictl1]*values2[ictl2]*values3[ictl3]*weighted_sum_grad1 )
                                / (weighted_sum*weighted_sum) * controlPointWeight(index_w);

                            gauss_point_gradient_list[index_grad+1] = ( values1[ictl1]*grad_values2[ictl2]*values3[ictl3]*weighted_sum - values1[ictl1]*values2[ictl2]*values3[ictl3]*weighted_sum_grad2 )
                                / (weighted_sum*weighted_sum) * controlPointWeight(index_w);

                            gauss_point_gradient_list[index_grad+2] = ( values1[ictl1]*values2[ictl2]*grad_values3[ictl3]*weighted_sum - values1[ictl1]*values2[ictl2]*values3[ictl3]*weighted_sum_grad3 )
                                / (weighted_sum*weighted_sum) * controlPointWeight(index_w);

                            ++index_w;
                            ++index_fun;
                            index_grad +=3;
                        }
                    }
                }

            }
            break;}


    }

    return;
}

/////////////////////////////////////////////////////////////////////////////
