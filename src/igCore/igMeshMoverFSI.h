/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igCoreExport.h>

#include "igMeshMover.h"

#include <vector>
#include <functional>
#include <string>

using namespace std;

class igMesh;
class igElement;
class igFace;
class igInterface;
class igCommunicator;

class IGCORE_EXPORT igMeshMoverFSI : public igMeshMover
{
public:
	igMeshMoverFSI(igMesh *mesh, vector<double> *coordinates, vector<double> *velocities);
	virtual ~igMeshMoverFSI(void);

public:
    void setInterfaceFSI(vector<double> *displacements, vector<double> *velocities) override;
    void setFractionalStep(double step) override;
    void initializeFractionalStep(void) override;

protected:
    int dof_number_per_elt;

    vector<double> *interface_displacements;
    vector<double> *interface_velocities;

    vector<double> *interface_displacements_ref;
    vector<double> *interface_velocities_ref;

    double fractional_step;

};

//
// igMeshMoverFSI.h ends here
