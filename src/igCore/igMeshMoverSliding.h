/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igCoreExport.h>

#include "igMeshMover.h"

#include <vector>
#include <functional>
#include <string>

using namespace std;

class igMesh;
class igElement;
class igFace;
class igInterface;
class igRungeKutta;
class igCommunicator;

class IGCORE_EXPORT igMeshMoverSliding : public igMeshMover
{
public:
	igMeshMoverSliding(igMesh *mesh, vector<double> *coordinates, vector<double> *velocities);
	virtual ~igMeshMoverSliding(void);

	void initializeMovement(const string& integrator, const string& movement_type, vector<double> *param_ALE, int max_refine_level) override;

    bool move(double solver_time) override;

private:
    void buildSlidingInterface(void);
    bool updateConnectivity(igInterface *interface);

private:
	double x_center, y_center;

};

//
// igMeshMoverSliding.h ends here
