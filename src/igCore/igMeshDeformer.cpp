/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igMeshDeformer.h"

#include "igMesh.h"
#include "igCell.h"
#include "igElement.h"
#include "igFace.h"
#include "igDeformAnalytic.h"
#include "igMeshMover.h"

#include <igDistributed/igCommunicator.h>

#include <iostream>
#include <stdlib.h>
#include <iomanip>
#include <igAssert.h>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igMeshDeformer::igMeshDeformer(void)
{
    this->mesh = NULL;
    this->coordinates = NULL;
}

igMeshDeformer::~igMeshDeformer(void)
{

}


/////////////////////////////////////////////////////////////////////////////

void igMeshDeformer::initialize(igMesh *mesh, vector<double> *coordinates, vector<double> *velocities)
{
    this->mesh = mesh;
    this->coordinates = coordinates;
    this->velocities = velocities;
}

void igMeshDeformer::setCommunicator(igCommunicator *communicator)
{
    this->communicator = communicator;
}

void igMeshDeformer::setDeformationFunctions(function<double(double x, double y, double *params)> def_fun_x, function<double(double x, double y, double *params)> def_fun_y)
{
    this->def_fun_x = def_fun_x;
    this->def_fun_y = def_fun_y;
}

void igMeshDeformer::setDeformationFunctionParameters(double *params)
{
    this->params = params;
}


/////////////////////////////////////////////////////////////////////////////


void igMeshDeformer::deform(void)
{
    // deform elements
    for (int iel = 0; iel < this->mesh->elementNumber(); ++iel) {

        igElement *elt = this->mesh->element(iel);

        for(int i = 0; i < elt->controlPointNumber(); ++i){
            double x = elt->controlPointCoordinate(i, 0);
            double y = elt->controlPointCoordinate(i, 1);

            this->coordinates->at(elt->globalId(i, 0)) = def_fun_x(x, y, params);
            this->coordinates->at(elt->globalId(i, 1)) = def_fun_y(x, y, params);
        }

        elt->initializeGeometry();
    }

    //compute velocity for inner faces
    for(int ifac=0; ifac<this->mesh->faceNumber(); ifac++){
        igFace *face = this->mesh->face(ifac);
        face->initializeGeometry();
        face->updateNormal();
    }

     //compute velocity for boundary faces
    for(int ifac=0; ifac<this->mesh->boundaryFaceNumber(); ifac++){
        igFace *face = this->mesh->boundaryFace(ifac);
        face->initializeGeometry();
        face->updateNormal();
     }

     igMeshMover mover(this->mesh,this->coordinates,this->velocities);
     mover.setCommunicator(this->communicator);
     mover.halfFaceCorrection();

}
