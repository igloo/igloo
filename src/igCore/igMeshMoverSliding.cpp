/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igMeshMoverSliding.h"

#include "igMesh.h"
#include "igCell.h"
#include "igElement.h"
#include "igFace.h"
#include "igInterface.h"
#include "igRungeKuttaSSP.h"
#include "igRungeKutta4.h"
#include "igRungeKutta2.h"

#include <igAssert.h>

#include <igGenerator/igMeshMovementSliding.h>
#include <igDistributed/igCommunicator.h>

#include <iostream>
#include <stdlib.h>
#include <cmath>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igMeshMoverSliding::igMeshMoverSliding(igMesh *mesh, vector<double> *coordinates, vector<double> *velocities) : igMeshMover(mesh,coordinates,velocities)
{
	IG_ASSERT(mesh, "no mesh");

	this->rigid = true;
	this->adaptive_mesh = false;
}

igMeshMoverSliding::~igMeshMoverSliding(void)
{

}


/////////////////////////////////////////////////////////////////////////////


void igMeshMoverSliding::initializeMovement(const string& integrator, const string& movement_type, vector<double> *param_ALE, int refine_max_level)
{
    this->movement_law = movement_type;
    this->movement_param = param_ALE;

    if(movement_type == "sliding"){
		mesh_movement_law = pureSliding;
		this->x_center = 5.;
		this->y_center = 0.;
	}
    if(movement_type == "turbine"){
		mesh_movement_law = turbineSliding;
		this->x_center = 0.;
		this->y_center = 0.;
	}
    else if(movement_type == "sliding_pitch"){
    	mesh_movement_law = slidingOscillation;
    	this->x_center = 0.;
    	this->y_center = 0.;
    }

    this->buildSlidingInterface();

    return;
}

/////////////////////////////////////////////////////////////////////////////


bool igMeshMoverSliding::move(double solver_time)
{
    if(solver_time != mesh->meshTime())
    {
        // // Move mesh
        this->evalCoordinateAndVelocity(solver_time);

        // Update interface
        igInterface *ifc;

        if(mesh->interfaceNumber()!=0){

        	// compute mid parameter for first interface
        	ifc = mesh->interface(0);
        	double mid_param = ifc->computeMidParam();

        	// update connectivity
        	bool to_split;
        	for(int iifc=0; iifc<mesh->interfaceNumber(); iifc++){
        		ifc = mesh->interface(iifc);
        		ifc->setMidParam(mid_param);
        		to_split = this->updateConnectivity(ifc);
        	}

        	// compute split parameter for first interface
        	ifc = mesh->interface(0);
        	double split_param = ifc->computeSplitParam();

        	// split and update sliding faces
        	if(to_split){
        		for(int iifc=0; iifc<mesh->interfaceNumber(); iifc++){
        			ifc = mesh->interface(iifc);
        			ifc->setSplitParam(split_param);
        			ifc->split();
        			ifc->updateGeometry();
        		}
        	}

        }

        mesh->setTime(solver_time);
        return true;
    }
    else
        return false;
}

/////////////////////////////////////////////////////////////////////////////

void igMeshMoverSliding::buildSlidingInterface(void)
{
	int interface_number = 0;
	int face_number = mesh->faceNumber();
	// Loop over faces
	for(int iface=0; iface<mesh->faceNumber(); iface++){

		igFace *face = mesh->face(iface);

		// Check if left and right elements are in different subdomains
		int left_subdomain = 0;
		int right_subdomain = 0;

		if(!face->shared()){
			left_subdomain = mesh->element(face->leftElementIndex())->subdomain();
			right_subdomain = mesh->element(face->rightElementIndex())->subdomain();
		}

		if(left_subdomain!=right_subdomain){

			// Create interface data structure
			igInterface *interface = new igInterface(this->coordinates, this->velocities);
			interface->setId(interface_number);
			interface->setDimension(mesh->meshDimension());
			interface->setCenter(this->x_center,this->y_center);
			interface->setParentFace(face);

            face->setParentSliding();

			igFace *left_child_face = new igFace(*face);
			left_child_face->setCellId(face_number);
			left_child_face->setInactive();
			left_child_face->setSliding();
			interface->setChildFace(left_child_face,true);

			face_number++;

			igFace *right_child_face = new igFace(*face);
			right_child_face->setCellId(face_number);
			right_child_face->setInactive();
			right_child_face->setSliding();
			interface->setChildFace(right_child_face,false);

			face_number++;

			// Add new faces and interface to mesh
			mesh->addFace(left_child_face);
			mesh->addFace(right_child_face);
			mesh->addInterface(interface);

			interface_number++;
		}

	}

	mesh->updateMeshProperties();

	// Loop over interfaces
	for(int iifc=0; iifc<mesh->interfaceNumber(); iifc++){

		igInterface *ifc = mesh->interface(iifc);
		igInterface *ifc_left;

		if(iifc!=0)
			ifc_left = mesh->interface(iifc-1);
		else
			ifc_left = mesh->interface(mesh->interfaceNumber()-1);

		// Initialize left child face connectivity
		igFace *parent_left = ifc_left->parentFace();
		igFace *left_child = ifc->childFace(true);
		left_child->setRightDofMap(parent_left->dofRight());
		left_child->setRightElementIndex(parent_left->rightElementIndex());

		ifc->computeMidParam();

	}

}


bool igMeshMoverSliding::updateConnectivity(igInterface *interface)
{
	double tol = 1e-4;
	double mid_param = interface->midParam();
	int id = interface->interfaceId();
	bool to_split = false;

	igFace *parent = interface->parentFace();
	igFace *left_child = interface->childFace(true);
	igFace *right_child = interface->childFace(false);

	if(fabs(mid_param)<tol){

		// there are faces that match exactly (right sense)
		parent->setRightElementIndex(right_child->rightElementIndex());
		parent->setRightDofMap(right_child->dofRight());
		interface->setParentActive();

	} else if(fabs(mid_param-1.)<tol){

		// there are faces that match exactly (left sense)
		parent->setRightElementIndex(left_child->rightElementIndex());
		parent->setRightDofMap(left_child->dofRight());
		interface->setParentActive();

	} else {

		to_split = true;

		igInterface *neigh_interface;
		igFace *neigh_left_child;
		igFace *neigh_right_child;

		if(mid_param>1.){

			// update connectivity (left sense)
			if(id!=0){

				neigh_interface = mesh->interface(id-1);
				neigh_right_child = neigh_interface->childFace(false);

				// update element connectivity
				right_child->setRightElementIndex(left_child->rightElementIndex());
				left_child->setRightElementIndex(neigh_right_child->rightElementIndex());

				// update DOF connectivity
				right_child->setRightDofMap(left_child->dofRight());
				left_child->setRightDofMap(neigh_right_child->dofRight());

			} else {

				neigh_interface = mesh->interface(mesh->interfaceNumber()-1);
				neigh_left_child = neigh_interface->childFace(true);
				neigh_right_child = neigh_interface->childFace(false);

				// update element connectivity
				left_child->setRightElementIndex(neigh_left_child->rightElementIndex());
				right_child->setRightElementIndex(neigh_right_child->rightElementIndex());

				// update DOF connectivity
				left_child->setRightDofMap(neigh_left_child->dofRight());
				right_child->setRightDofMap(neigh_right_child->dofRight());

			}

			interface->setChildActive();
			interface->computeMidParam();

		} else if(mid_param<0.){

			// update connectivity (right sense)
			if(id!=(mesh->interfaceNumber()-1)){

				neigh_interface = mesh->interface(id+1);
				neigh_left_child = neigh_interface->childFace(true);
				neigh_right_child = neigh_interface->childFace(false);

				// update element connectivity
				left_child->setRightElementIndex(neigh_left_child->rightElementIndex());
				right_child->setRightElementIndex(neigh_right_child->rightElementIndex());

				// update DOF connectivity
				left_child->setRightDofMap(neigh_left_child->dofRight());
				right_child->setRightDofMap(neigh_right_child->dofRight());

			} else {

				neigh_interface = mesh->interface(0);
				neigh_left_child = neigh_interface->childFace(true);

				// update element connectivity
				left_child->setRightElementIndex(right_child->rightElementIndex());
				right_child->setRightElementIndex(neigh_left_child->rightElementIndex());

				// update DOF connectivity
				left_child->setRightDofMap(right_child->dofRight());
				right_child->setRightDofMap(neigh_left_child->dofRight());

			}

			interface->setChildActive();
			interface->computeMidParam();

		}
	}

	mesh->updateMeshProperties();

	return to_split;
}
