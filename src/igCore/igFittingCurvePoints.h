/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include "igFitting.h"

#include <igCoreExport>

#include <functional>
#include <vector>

using namespace std;

class IGCORE_EXPORT igFittingCurvePoints : public igFitting
{
public:
     igFittingCurvePoints(void);
    ~igFittingCurvePoints(void);

public:
    void setUniform(bool flag_uniform);
    void setCoordinateBounds(double x_min, double x_max, double y_min, double y_max);
    void setSpanNumber(int n_spans);
    void setMultiplicity(int s);
    void setXPoints(vector<double> x_pts);
    void setYPoints(vector<double> y_pts);
    vector<vector<double>> xSolution(void);
    vector<vector<double>> ySolution(void);

public:
    void run(void);

private:

    bool uniform;

    int span_number;
    int multiplicity;

    double x_min;
    double x_max;
    double y_min;
    double y_max;

    vector<vector<double>> sol_x;
    vector<vector<double>> sol_y;

    vector<double> x_pts;
    vector<double> y_pts;
};

//
// igFittingCurvePoints.h ends here
