/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igMeshRefiner.h"

#include "igMesh.h"
#include "igCell.h"
#include "igElement.h"
#include "igFace.h"
#include "igBoundaryIds.h"

#include <cmath>
#include <iostream>
#include <stdlib.h>
#include <igAssert.h>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igMeshRefiner::igMeshRefiner(void)
{

}

igMeshRefiner::~igMeshRefiner(void)
{

}


/////////////////////////////////////////////////////////////////////////////

void igMeshRefiner::initialize(igMesh *mesh, vector<double> *state, vector<double> *coordinates, vector<double> *velocities)
{
    IG_ASSERT(mesh, "no mesh");
    IG_ASSERT(state, "no state");

    this->mesh = mesh;
    this->state = state;
    this->coordinates = coordinates;
    this->velocities = velocities;

    refine_flag.resize(mesh->elementNumber());

    refinement_step = 0;

    srand(10);

    return;
}

void igMeshRefiner::setError(vector<double> *error_xi, vector<double> *error_eta)
{
    IG_ASSERT(error_xi, "no error_xi");
    IG_ASSERT(error_eta, "no error_eta");

    this->error_xi = error_xi;
    this->error_eta = error_eta;
}

void igMeshRefiner::setRefineCoefficient(double refine_coef)
{
    this->refine_coef = refine_coef;
    return;
}

/////////////////////////////////////////////////////////////////////////////


void igMeshRefiner::select(void)
{
    cout << ">>> mesh refinement process: selection" << endl;

    blocking_flag.resize(mesh->elementNumber(),0);
    this->added_elt_number= 0;

    // first check if elements can be refined
    this->checkRefinement();

    // first loop over elements to compute global error
    double mean_error = 0.;
    for (int iel=0; iel<mesh->elementNumber(); iel++){
        igElement *elt = mesh->element(iel);
        if(elt->isActive())
            mean_error += error_xi->at(iel) + error_eta->at(iel);
    }
    mean_error /= mesh->activeElementNumber();

    // second loop over elements to select
    int blocking_config = 0;
    for (int iel=0; iel<mesh->elementNumber(); iel++){
        igElement *elt = mesh->element(iel);
        refine_flag.at(iel) = 0;

        if(elt->isActive()){
            double refine_ratio = (error_xi->at(iel)+error_eta->at(iel))/mean_error;

            if(refine_ratio > refine_coef){

                if( error_xi->at(iel) > error_eta->at(iel) ){
                    if ( elt_refinable_xi.at(iel) ){
                        refine_flag.at(iel) = 1;
                        this->added_elt_number += 2;
                    } else {
                        blocking_flag.at(iel) = 1;
                        blocking_config++;
                    }
                }
                else {
                    if( elt_refinable_eta.at(iel) ){
                        refine_flag.at(iel) = 2;
                        this->added_elt_number += 2;
                    } else {
                        blocking_flag.at(iel) = 2;
                        blocking_config++;
                    }
                }

            }

        }
    }

    // propagate refinement to avoid blocking configurations
    this->propagateRefinement();

    cout << "Mean error: " << mean_error << endl;
    cout << "blocking configurations: " << blocking_config << endl;
    cout << ">>> selection done" << endl;
    cout << endl;

    return;
}

///////////////////////////////////////////////////////////////////////

void igMeshRefiner::refine(void)
{
    cout << ">>> mesh refinement process: refinement" << endl;

    int previous_element_number = mesh->elementNumber();
    int previous_face_number = mesh->faceNumber();
    int previous_bnd_number = mesh->boundaryFaceNumber();
    int ctrl_pts_per_elt = mesh->element(0)->controlPointNumber();
    int variable_number = mesh->variableNumber();
    int old_state_size = state->size();
    int new_state_size = (previous_element_number+added_elt_number)*ctrl_pts_per_elt*variable_number;
    int new_coord_size = (previous_element_number+added_elt_number)*ctrl_pts_per_elt*(mesh->meshDimension()+1);
    int new_vel_size = (previous_element_number+added_elt_number)*ctrl_pts_per_elt*mesh->meshDimension();
    int max_field_number = max(variable_number,mesh->derivativeNumber());
    max_field_number = max(max_field_number,mesh->meshDimension()+1);

    //------------- fill new state vector and update global Id map ---------------

    vector<double> *state_tmp = new vector<double>;
    state_tmp->resize(state->size(),0.);

    vector<double> *coordinates_tmp = new vector<double>;
    coordinates_tmp->resize(coordinates->size(),0.);

    vector<double> *velocities_tmp = new vector<double>;
    velocities_tmp->resize(velocities->size(),0.);

    // fill temporary arrays
    for(int i=0; i<state->size(); i++){
        state_tmp->at(i) = state->at(i);
    }

    for(int i=0; i<coordinates->size(); i++){
        coordinates_tmp->at(i) = coordinates->at(i);
    }

    for(int i=0; i<velocities->size(); i++){
        velocities_tmp->at(i) = velocities->at(i);
    }

    // fill new state, coordinates and velocities
    state->resize(new_state_size);
    coordinates->resize(new_coord_size);
    velocities->resize(new_vel_size);

    int current_index = 0;
    int current_index_new = 0;
    for(int ivar=0; ivar<max_field_number; ivar++){
        for(int iel=0; iel<previous_element_number; iel++){
            for(int ipt=0; ipt<ctrl_pts_per_elt; ipt++){
                if(ivar<variable_number)
                    state->at(current_index_new) = state_tmp->at(current_index);
                if(ivar<mesh->meshDimension()+1)
                    coordinates->at(current_index_new) = coordinates_tmp->at(current_index);
                if(ivar<mesh->meshDimension())
                    velocities->at(current_index_new) = velocities_tmp->at(current_index);
                mesh->element(iel)->setGlobalId(ipt, ivar, current_index_new);
                current_index++;
                current_index_new++;
            }
        }
        current_index_new += added_elt_number*ctrl_pts_per_elt;
    }

    // copy level arrays
    mesh->refreshLevelArray(previous_element_number+added_elt_number,0);

    //------------ refinement of elements and faces ----------------------

    //-------- loop over all existing elements
    for (int iel=0; iel<previous_element_number; iel++){

        igElement *elt = mesh->element(iel);

        // refinement along Xi
        if( refine_flag.at(iel) == 1 ){

            this->refineElement(elt,0);

            // add flags for new elements
            refine_flag.push_back(0);
            refine_flag.push_back(0);

        }

        // refinement along Eta
        if( refine_flag.at(iel) == 2 ){

            this->refineElement(elt,1);

            refine_flag.push_back(0);
            refine_flag.push_back(0);

        }
    }

    //-------- update face maps for all existing faces
    for (int ifac=0; ifac<previous_face_number; ifac++){

        igFace *face = mesh->face(ifac);
            this->updateMap(face, true);
            this->updateMap(face, false);
    }
    for (int ifac=0; ifac<previous_bnd_number; ifac++){

        igFace *face = mesh->boundaryFace(ifac);
            this->updateMap(face, true);
    }


    //--------- loop over faces to update and refine faces
    for (int ifac=0; ifac<previous_face_number; ifac++){

        igFace *face = mesh->face(ifac);

        if(face->isActive()){

            int elt_left_index = face->leftElementIndex();
            int elt_right_index = face->rightElementIndex();

            // check refinement type for left element

            igElement *elt = mesh->element(elt_left_index);
            int orientation  = face->leftOrientation();
            bool left = true;
            bool face_to_refine = false;

            if( refine_flag.at(elt_left_index) == 1 ){ // refined along Xi
                if( orientation ==  1 || orientation ==  3){
                    this->updateFace(face,left);
                } else {
                    if(face->childRank() == 0) // additional test to avoid double refinement
                        face_to_refine = true;
                    else
                        this->updateHalfFace(face,left);
                }
            }
            else if( refine_flag.at(elt_left_index) == 2 ){ // refined along Eta
                if( orientation ==  0 || orientation ==  2){
                    this->updateFace(face,left);
                } else {
                    if(face->childRank() == 0) // additional test to avoid double refinement
                        face_to_refine = true;
                    else
                        this->updateHalfFace(face,left);
                }
            }

            // check refinement type for right element

            elt = mesh->element(elt_right_index);
            orientation  = face->rightOrientation();
            left = false;

            if( refine_flag.at(elt_right_index) == 1 ){ // refined along Xi
                if( orientation ==  1 || orientation ==  3){
                    this->updateFace(face,left);
                } else {
                    if(face->childRank() == 0) // additional test to avoid double refinement
                        face_to_refine = true;
                    else
                        this->updateHalfFace(face,left);
                }
            }
            else if( refine_flag.at(elt_right_index) == 2 ){ // refined along Eta
                if( orientation ==  0 || orientation ==  2){
                    this->updateFace(face,left);
                } else {
                    if(face->childRank() == 0) // additional test to avoid double refinement
                        face_to_refine = true;
                    else
                        this->updateHalfFace(face,left);
                }
            }

            if(face_to_refine)
                this->refineFace(face);
        }
    }

    //--------- loop over elements to add new mid-faces
    for (int iel=0; iel<previous_element_number; iel++){

        if( refine_flag.at(iel) != 0 )
            this->addFace(mesh->element(iel),refine_flag.at(iel));
    }

    //-------- loop over boundary faces
    for (int ibnd=0; ibnd < previous_bnd_number; ibnd++){

        igFace *face = mesh->boundaryFace(ibnd);
        int elt_left_index = face->leftElementIndex();

        igElement *elt = mesh->element(elt_left_index);
        int orientation  = face->leftOrientation();
        bool left = true;

        if( refine_flag.at(elt_left_index) == 1 ){ // refined along Xi
            if( orientation ==  1 || orientation ==  3)
                this->updateFace(face,left);
            else
                this->refineFace(face);
        }
        else if( refine_flag.at(elt_left_index) == 2 ){ // refined along Eta
            if( orientation ==  0 || orientation ==  2)
                this->updateFace(face,left);
            else
                this->refineFace(face);
        }
    }

    mesh->updateMeshProperties();

    cout << "Number of elements - previous: " << previous_element_number << ", new: " << mesh->elementNumber() << ", active: " << mesh->activeElementNumber()  << endl;
    cout << "Number of faces - previous: " << previous_face_number << ", new: " << mesh->faceNumber() << ", active: " << mesh->activeFaceNumber() << endl;
    cout << "Number of boundary faces - previous: " << previous_bnd_number << ", new: " << mesh->boundaryFaceNumber() << ", active: " << mesh->activeBoundaryFaceNumber() << endl;
    cout << "State vector size  - previous: " << old_state_size << ", new: " << new_state_size << endl;
    cout << "Number of active DOFs: " << mesh->activeElementNumber()*ctrl_pts_per_elt << endl;
    cout << ">>> refinement done" << endl;
    cout << endl;

    refinement_step++;

    delete state_tmp;
    delete coordinates_tmp;
    delete velocities_tmp;

    return;
}

//////////////////////////////////////////////////////

void igMeshRefiner::reparametrize(void)
{
	int degree = mesh->element(0)->cellDegree();
	int control_point_number_by_direction = mesh->element(0)->controlPointNumberByDirection();

	vector<double> old_weight(control_point_number_by_direction,1.);
	vector<double> new_weight(control_point_number_by_direction,1.);

	// change curve parametrization using conic shape factor
	for(int iel=0; iel<mesh->elementNumber(); iel++){
		igElement *elt = mesh->element(iel);
		if(elt->isActive()){

			for(int icrv=0; icrv<control_point_number_by_direction; icrv++){
				// retrieve old weights
				int istart = icrv*control_point_number_by_direction;
				for(int ipt=0; ipt<control_point_number_by_direction; ipt++)
					old_weight.at(ipt) = elt->controlPointWeight(istart+ipt);

				// compute new weight via conic shape factor
				if(degree==2){
					double c = old_weight.at(0)*old_weight.at(2)/(old_weight.at(1)*old_weight.at(1));
					new_weight.at(1) = sqrt(1./c);
				}
				/*else if(degree==3){
					double c1 = old_weight.at(0)*old_weight.at(2)/(old_weight.at(1)*old_weight.at(1));
					double c2 = old_weight.at(1)*old_weight.at(3)/(old_weight.at(2)*old_weight.at(2));
					new_weight.at(1) = 1./c1;
					new_weight.at(2) = 1./c2;
				}*/

				// set new weights
				for(int ipt=0; ipt<control_point_number_by_direction; ipt++)
					elt->setControlPointWeight(istart+ipt,new_weight.at(ipt));

			}
			elt->initializeGeometry();

		}
	}

}

//////////////////////////////////////////////////////

void igMeshRefiner::refineElement(igElement *elt, int refine_direction)
{
    IG_ASSERT(elt, "no element");

    int dimension = mesh->meshDimension();
    int degree = mesh->element(0)->cellDegree();
    int ctrl_pts_per_elt = mesh->element(0)->controlPointNumber();
    int variable_number = mesh->variableNumber();
    int max_field_number = max(variable_number,mesh->derivativeNumber());
    max_field_number = max(max_field_number,mesh->meshDimension()+1);
    int added_dof_number = (state->size())/variable_number;

    vector<double> *coord_tmp = new vector<double>;
    coord_tmp->resize(degree+1);

    vector<double> *coord_curve1 = new vector<double>;
    coord_curve1->resize(degree+1);

    vector<double> *coord_curve2 = new vector<double>;
    coord_curve2->resize(degree+1);

    int index;

    igElement *new_elt1 = new igElement;
    igElement *new_elt2 = new igElement;

    // set element as inactive father
    elt->setChild(0,mesh->elementNumber());
    elt->setChild(1,mesh->elementNumber()+1);
    elt->setInactive();

    // generation of first new element 1

    new_elt1->setCellId(mesh->elementNumber());
    new_elt1->setPartition(elt->partition());
    new_elt1->setSubdomain(elt->subdomain());
    new_elt1->setPhysicalDimension(dimension);
    new_elt1->setParametricDimension(dimension);
    new_elt1->setDegree(elt->cellDegree());
    new_elt1->setGaussPointNumberByDirection(elt->gaussPointNumberByDirection());
    new_elt1->setVariableNumber(mesh->variableNumber());
    new_elt1->setDerivativeNumber(mesh->derivativeNumber());
    new_elt1->setCoordinates(this->coordinates);
    new_elt1->setVelocities(this->velocities);

    new_elt1->initializeDegreesOfFreedom();

    int global_id_counter = mesh->elementNumber()*ctrl_pts_per_elt;
    for (int ivar=0; ivar<max_field_number; ivar++){
        for (int ipt=0; ipt<ctrl_pts_per_elt; ipt++){
            new_elt1->setGlobalId(ipt, ivar, global_id_counter);
            global_id_counter++;
        }
        global_id_counter += added_dof_number-ctrl_pts_per_elt;
    }

    // generation of second new element 2

    new_elt2->setCellId(mesh->elementNumber()+1);
    new_elt2->setPartition(elt->partition());
    new_elt2->setSubdomain(elt->subdomain());
    new_elt2->setPhysicalDimension(dimension);
    new_elt2->setParametricDimension(dimension);
    new_elt2->setDegree(elt->cellDegree());
    new_elt2->setGaussPointNumberByDirection(elt->gaussPointNumberByDirection());
    new_elt2->setVariableNumber(mesh->variableNumber());
    new_elt2->setDerivativeNumber(mesh->derivativeNumber());
    new_elt2->setCoordinates(this->coordinates);
    new_elt2->setVelocities(this->velocities);

    new_elt2->initializeDegreesOfFreedom();

    global_id_counter = (mesh->elementNumber()+1)*ctrl_pts_per_elt;
    for (int ivar=0; ivar<max_field_number; ivar++){
        for (int ipt=0; ipt<ctrl_pts_per_elt; ipt++){
            new_elt2->setGlobalId(ipt, ivar, global_id_counter);
            global_id_counter++;
        }
        global_id_counter += added_dof_number-ctrl_pts_per_elt;
    }

    // loop over lines
    for (int icurve=0; icurve<degree+1; icurve++){

        // treatment of components (geometrical + solution)
        for(int icomp=0; icomp<2*dimension+variable_number+1; icomp++){

            // retrieve curve control points
            for(int ipt=0; ipt<degree+1; ipt++){

                if(refine_direction == 0)
                    index = (degree+1)*icurve + ipt;
                else if(refine_direction == 1)
                    index = (degree+1)*ipt + icurve;

                if(icomp < dimension) // geometry : coordinates * weights
                    coord_tmp->at(ipt) = coordinates->at(elt->globalId(index, icomp)) *
                        coordinates->at(elt->globalId(index, dimension));
                else if(icomp == dimension) // geometry : weights
                    coord_tmp->at(ipt) = coordinates->at(elt->globalId(index, dimension));
                else if(icomp > dimension && icomp < 2*dimension+1)	// velocities * weights
                	coord_tmp->at(ipt) = velocities->at(elt->globalId(index,icomp-dimension-1)) *
						coordinates->at(elt->globalId(index, dimension));
                else // solution * weights
                    coord_tmp->at(ipt) = state->at(elt->globalId(index,icomp-2*dimension-1)) *
                        coordinates->at(elt->globalId(index, dimension));
            }

            // keep extremities
            coord_curve1->at(0) = coord_tmp->at(0);
            coord_curve2->at(degree) = coord_tmp->at(degree);

            // subdivision procedure
            for (int istep=0; istep<degree; istep++){

                for (int ipt=0; ipt<degree-istep; ipt++)
                    coord_tmp->at(ipt) = 0.5*coord_tmp->at(ipt) + 0.5*coord_tmp->at(ipt+1);

                coord_curve1->at(1+istep) = coord_tmp->at(0);
                coord_curve2->at(degree-1-istep) = coord_tmp->at(degree-1-istep);
            }

            // store control points
            for (int ipt=0; ipt<degree+1; ipt++){

                if(refine_direction == 0)
                    index = (degree+1)*icurve + ipt;
                else if(refine_direction == 1)
                    index = (degree+1)*ipt + icurve;

                if(icomp < dimension){ // geometry : coordinates * weights
                    coordinates->at(new_elt1->globalId(index,icomp)) = coord_curve1->at(ipt);
                    coordinates->at(new_elt2->globalId(index,icomp)) = coord_curve2->at(ipt);
                } else if(icomp == dimension) {// geometry : weights
                    coordinates->at(new_elt1->globalId(index,dimension)) = coord_curve1->at(ipt);
                    coordinates->at(new_elt2->globalId(index,dimension)) = coord_curve2->at(ipt);
                } else if(icomp > dimension && icomp < 2*dimension+1) { // velocities
                	velocities->at(new_elt1->globalId(index,icomp-dimension-1)) = coord_curve1->at(ipt);
                	velocities->at(new_elt2->globalId(index,icomp-dimension-1)) = coord_curve2->at(ipt);
                } else { // solution
                    state->at(new_elt1->globalId(index,icomp-2*dimension-1)) = coord_curve1->at(ipt);
                    state->at(new_elt2->globalId(index,icomp-2*dimension-1)) = coord_curve2->at(ipt);
                }

            }
        }

        // recover NURBS formulation
        for(int icomp=0; icomp<dimension; icomp++){
            for (int ipt=0; ipt<degree+1; ipt++){

                if(refine_direction == 0)
                    index = (degree+1)*icurve + ipt;
                else if(refine_direction == 1)
                    index = (degree+1)*ipt + icurve;

                coordinates->at(new_elt1->globalId(index,icomp)) /= coordinates->at(new_elt1->globalId(index,dimension));
                coordinates->at(new_elt2->globalId(index,icomp)) /= coordinates->at(new_elt2->globalId(index,dimension));
            }
        }

        for(int icomp=0; icomp<dimension; icomp++){
            for (int ipt=0; ipt<degree+1; ipt++){

                if(refine_direction == 0)
                    index = (degree+1)*icurve + ipt;
                else if(refine_direction == 1)
                    index = (degree+1)*ipt + icurve;

                velocities->at(new_elt1->globalId(index,icomp)) /= coordinates->at(new_elt1->globalId(index,dimension));
                velocities->at(new_elt2->globalId(index,icomp)) /= coordinates->at(new_elt2->globalId(index,dimension));
            }
        }

        for(int icomp=0; icomp<variable_number; icomp++){
            for (int ipt=0; ipt<degree+1; ipt++){

                if(refine_direction == 0)
                    index = (degree+1)*icurve + ipt;
                else if(refine_direction == 1)
                    index = (degree+1)*ipt + icurve;

                state->at(new_elt1->globalId(index,icomp)) /= coordinates->at(new_elt1->globalId(index,dimension));
                state->at(new_elt2->globalId(index,icomp)) /= coordinates->at(new_elt2->globalId(index,dimension));
            }
        }


    }

    new_elt1->initializeGeometry();
    new_elt2->initializeGeometry();

    new_elt1->setParent(elt->cellId());
    new_elt2->setParent(elt->cellId());

    mesh->addElement(new_elt1);
    mesh->addElement(new_elt2);
    mesh->computeElementNumber();

    // update level list
    int father_index = elt->cellId();
    int son1_index = mesh->elementNumber()-2;
    int son2_index = mesh->elementNumber()-1;

    int father_level_0 = mesh->elementLevel(father_index,0);
    int father_level_1 = mesh->elementLevel(father_index,1);

    if(refine_direction == 0){
        mesh->setElementLevel(son1_index, 0, father_level_0+1);
        mesh->setElementLevel(son1_index, 1, father_level_1);
        mesh->setElementLevel(son2_index, 0, father_level_0+1);
        mesh->setElementLevel(son2_index, 1, father_level_1);
    } else {
        mesh->setElementLevel(son1_index, 0, father_level_0);
        mesh->setElementLevel(son1_index, 1, father_level_1+1);
        mesh->setElementLevel(son2_index, 0, father_level_0);
        mesh->setElementLevel(son2_index, 1, father_level_1+1);
    }


    delete coord_tmp;
    delete coord_curve1;
    delete coord_curve2;

    return;
}

///////////////////////////////////////////////////////////////

void igMeshRefiner::refineFace(igFace *face)
{
    IG_ASSERT(face, "no face");

    int dimension = face->physicalDimension();
    int max_field_number = max(mesh->variableNumber(),mesh->derivativeNumber());
    max_field_number = max(max_field_number,mesh->meshDimension()+1);
    int degree = face->cellDegree();
    bool interior = true;
    if(face->type() != id_interior)
        interior = false;

    int partition_id_left = face->leftPartitionId();
    int partition_id_right;
    int elt_index_left = face->leftElementIndex();
    int elt_index_right;
    int orientation_left = face->leftOrientation();
    int orientation_right;
    int sense_left = face->leftSense();
    int sense_right;
    igElement *elt_left = mesh->element(elt_index_left);
    igElement *elt_right;
    bool double_refinement = false;

    int new_id1;
    int new_id2;
    if(interior){
        new_id1 = mesh->faceNumber();
        new_id2 = mesh->faceNumber()+1;
    } else {
        new_id1 = mesh->boundaryFaceNumber();
        new_id2 = mesh->boundaryFaceNumber()+1;
    }

    if(interior){
        partition_id_right = face->rightPartitionId();
        elt_index_right = face->rightElementIndex();
        orientation_right = face->rightOrientation();
        sense_right = face->rightSense();
        elt_right = mesh->element(elt_index_right);

        if(elt_left->hasChild() && elt_right->hasChild())
            double_refinement = true;
    }

    vector<double> *coord = new vector<double>;
    coord->resize(dimension);

    int index_beg;
    int index_incr;


    // create new face 1
    igFace *new_face1 = new igFace;

    new_face1->setCellId(new_id1);
    new_face1->setPhysicalDimension(dimension);
    new_face1->setParametricDimension(dimension-1);
    new_face1->setDegree(degree);
    new_face1->setGaussPointNumberByDirection(face->gaussPointNumberByDirection());
    new_face1->setVariableNumber(mesh->variableNumber());
    new_face1->setDerivativeNumber(mesh->derivativeNumber());
    new_face1->setType(face->type());
    new_face1->setLeftPartitionId(partition_id_left);
    if(face->leftInside())
        new_face1->setLeftInside();

    if(interior){
        new_face1->setRightPartitionId(partition_id_right);
        if(face->rightInside())
            new_face1->setRightInside();
    }

    bool sign_left = false;
    if(sense_left<0)
        sign_left = true;
    bool sign_right = false;
    if(sense_right<0)
        sign_right = true;

    int new_elt1_left_index;
    if(elt_left->hasChild()){
        if(sign_left)
            new_elt1_left_index = elt_left->child(1);
        else
            new_elt1_left_index = elt_left->child(0);
        new_face1->setParameterIntervalLeft(0.,1.);
    } else {
        new_elt1_left_index = elt_index_left;
        new_face1->setParameterIntervalLeft(0.,0.5);
        new_face1->setHalfFaceLeft(true);
    }
    igElement *elt_left1 = mesh->element(new_elt1_left_index);


    int new_elt1_right_index;
    igElement *elt_right1;
    if(interior){
        if(elt_right->hasChild()){
            if(sign_right)
                new_elt1_right_index = elt_right->child(1);
            else
                new_elt1_right_index = elt_right->child(0);
            new_face1->setParameterIntervalRight(0.,1.);
        } else {
            new_elt1_right_index = elt_index_right;
            new_face1->setParameterIntervalRight(0.,0.5);
            new_face1->setHalfFaceRight(true);
        }
        elt_right1 = mesh->element(new_elt1_right_index);
    }
    new_face1->setCoordinates(this->coordinates);
    new_face1->setVelocities(this->velocities);

    new_face1->initializeDegreesOfFreedom();

    new_face1->setLeftElementIndex(new_elt1_left_index);
    new_face1->setOrientationLeft(orientation_left);
    new_face1->setSenseLeft(sense_left);


    switch(orientation_left){
    case 0:
        index_beg = 0;
        index_incr = 1;
        if(sign_left){
            index_beg = degree;
            index_incr *= -1;
        }
        break;
    case 1:
        index_beg = degree;
        index_incr = degree+1;
        if(sign_left){
            index_beg = (degree+1)*(degree+1)-1;
            index_incr *= -1;
        }
        break;
    case 2:
        index_beg = degree*(degree+1);
        index_incr = 1;
        if(sign_left){
            index_beg = (degree+1)*(degree+1)-1;
            index_incr *= -1;
        }
        break;
    case 3:
        index_beg = 0;
        index_incr = degree+1;
        if(sign_left){
            index_beg = (degree+1)*degree;
            index_incr *= -1;
        }
        break;
    }

    for (int idof=0; idof<face->controlPointNumber(); idof++){
    	for (int ivar=0; ivar<max_field_number; ivar++)
    		new_face1->setLeftDofMap(idof,ivar,elt_left1->globalId(index_beg+index_incr*idof,ivar));
    	new_face1->setLeftPointMap(idof, index_beg+index_incr*idof);
    }


    if(interior){

        new_face1->setRightElementIndex(new_elt1_right_index);
        new_face1->setOrientationRight(orientation_right);
        new_face1->setSenseRight(sense_right);

        switch(orientation_right){
        case 0:
            index_beg = 0;
            index_incr = 1;
            if(sign_right){
                index_beg = degree;
                index_incr *= -1;
            }
            break;
        case 1:
            index_beg = degree;
            index_incr = degree+1;
            if(sign_right){
                index_beg = (degree+1)*(degree+1)-1;
                index_incr *= -1;
            }
            break;
        case 2:
            index_beg = degree*(degree+1);
            index_incr = 1;
            if(sign_right){
                index_beg = (degree+1)*(degree+1)-1;
                index_incr *= -1;
            }
            break;
        case 3:
            index_beg = 0;
            index_incr = degree+1;
            if(sign_right){
                index_beg = (degree+1)*degree;
                index_incr *= -1;
            }
            break;
        }

        for (int idof=0; idof<face->controlPointNumber(); idof++){
        	for (int ivar=0; ivar<max_field_number; ivar++)
        		new_face1->setRightDofMap(idof,ivar,elt_right1->globalId(index_beg+index_incr*idof,ivar));
        	new_face1->setRightPointMap(idof, index_beg+index_incr*idof);
        }

    }

    for (int idim=0; idim<dimension; idim++)
        coord->at(idim) = elt_left1->barycenter(idim);

    new_face1->initializeGeometry();
    new_face1->initializeNormal(coord);


    // create new face 2
    igFace *new_face2 = new igFace;

    // generic data
    new_face2->setCellId(new_id2);
    new_face2->setPhysicalDimension(dimension);
    new_face2->setParametricDimension(dimension-1);
    new_face2->setDegree(degree);
    new_face2->setGaussPointNumberByDirection(face->gaussPointNumberByDirection());
    new_face2->setVariableNumber(mesh->variableNumber());
    new_face2->setDerivativeNumber(mesh->derivativeNumber());
    new_face2->setType(face->type());
    new_face2->setLeftPartitionId(partition_id_left);
    if(face->leftInside())
        new_face2->setLeftInside();

    if(interior){
        new_face2->setRightPartitionId(partition_id_right);
        if(face->rightInside())
            new_face2->setRightInside();
    }

    int new_elt2_left_index;
    if(elt_left->hasChild()){
        if(sign_left)
            new_elt2_left_index = elt_left->child(0);
        else
            new_elt2_left_index = elt_left->child(1);
        new_face2->setParameterIntervalLeft(0.,1.);
    } else {
        new_elt2_left_index = elt_index_left;
        new_face2->setParameterIntervalLeft(0.5,1.);
        new_face2->setHalfFaceLeft(true);
    }
    igElement *elt_left2 = mesh->element(new_elt2_left_index);

    int new_elt2_right_index;
    igElement *elt_right2;
    if(interior){
        if(elt_right->hasChild()){
            if(sign_right)
                new_elt2_right_index = elt_right->child(0);
            else
                new_elt2_right_index = elt_right->child(1);
            new_face2->setParameterIntervalRight(0.,1.);
        } else {
            new_elt2_right_index = elt_index_right;
            new_face2->setParameterIntervalRight(0.5,1.);
            new_face1->setHalfFaceRight(true);
        }
        elt_right2 = mesh->element(new_elt2_right_index);
    }
    new_face2->setCoordinates(this->coordinates);
    new_face2->setVelocities(this->velocities);

    new_face2->initializeDegreesOfFreedom();

    new_face2->setLeftElementIndex(new_elt2_left_index);
    new_face2->setOrientationLeft(orientation_left);
    new_face2->setSenseLeft(sense_left);

    switch(orientation_left){
    case 0:
        index_beg = 0;
        index_incr = 1;
        if(sign_left){
            index_beg = degree;
            index_incr *= -1;
        }
        break;
    case 1:
        index_beg = degree;
        index_incr = degree+1;
        if(sign_left){
            index_beg = (degree+1)*(degree+1)-1;
            index_incr *= -1;
        }
        break;
    case 2:
        index_beg = degree*(degree+1);
        index_incr = 1;
        if(sign_left){
            index_beg = (degree+1)*(degree+1)-1;
            index_incr *= -1;
        }
        break;
    case 3:
        index_beg = 0;
        index_incr = degree+1;
        if(sign_left){
            index_beg = (degree+1)*degree;
            index_incr *= -1;
        }
        break;
    }

    for (int idof=0; idof<face->controlPointNumber(); idof++){
    	for (int ivar=0; ivar<max_field_number; ivar++)
    		new_face2->setLeftDofMap(idof,ivar,elt_left2->globalId(index_beg+index_incr*idof,ivar));
    	new_face2->setLeftPointMap(idof, index_beg+index_incr*idof);
    }

    // right element and dofs
    if(interior){

        new_face2->setRightElementIndex(new_elt2_right_index);
        new_face2->setOrientationRight(orientation_right);
        new_face2->setSenseRight(sense_right);

        switch(orientation_right){
        case 0:
            index_beg = 0;
            index_incr = 1;
            if(sign_right){
                index_beg = degree;
                index_incr *= -1;
            }
            break;
        case 1:
            index_beg = degree;
            index_incr = degree+1;
            if(sign_right){
                index_beg = (degree+1)*(degree+1)-1;
                index_incr *= -1;
            }
            break;
        case 2:
            index_beg = degree*(degree+1);
            index_incr = 1;
            if(sign_right){
                index_beg = (degree+1)*(degree+1)-1;
                index_incr *= -1;
            }
            break;
        case 3:
            index_beg = 0;
            index_incr = degree+1;
            if(sign_right){
                index_beg = (degree+1)*degree;
                index_incr *= -1;
            }
            break;
        }

        for (int idof=0; idof<face->controlPointNumber(); idof++){
        	for (int ivar=0; ivar<max_field_number; ivar++)
        		new_face2->setRightDofMap(idof,ivar,elt_right2->globalId(index_beg+index_incr*idof,ivar));
        	new_face2->setRightPointMap(idof, index_beg+index_incr*idof);
        }
    }

    for (int idim=0; idim<dimension; idim++)
        coord->at(idim) = elt_left2->barycenter(idim);

    new_face2->initializeGeometry();
    new_face2->initializeNormal(coord);

    if(!double_refinement){
        new_face1->setChildRank(1);
        new_face2->setChildRank(2);
    }

    new_face1->setParent(face->cellId());
    new_face2->setParent(face->cellId());

    face->setChild(0, new_face1->cellId());
    face->setChild(1, new_face2->cellId());

    // storage in the face list
    if(interior){
        mesh->addFace(new_face1);
        mesh->addFace(new_face2);
        mesh->computeFaceNumber();
    } else {
        mesh->addBoundaryFace(new_face1);
        mesh->addBoundaryFace(new_face2);
        mesh->computeBoundaryFaceNumber();
    }
    face->setInactive();

    delete coord;
    return;

}

/////////////////////////////////////////////////////////////////////////////////////

void igMeshRefiner::updateFace(igFace *face, bool left)
{
    IG_ASSERT(face, "no face");

    igElement *elt;
    int orientation;
    int sense;
    if(left){
        elt = mesh->element(face->leftElementIndex());
        orientation = face->leftOrientation();
        sense = face->leftSense();
    }
    else{
        elt = mesh->element(face->rightElementIndex());
        orientation = face->rightOrientation();
        sense = face->rightSense();
    }

    int max_field_number = max(mesh->variableNumber(),mesh->derivativeNumber());
    max_field_number = max(max_field_number,mesh->meshDimension()+1);
    int new_elt_index;
    int degree = elt->cellDegree();
    igElement *new_elt;

    bool sign = false;
    if(sense<0)
        sign = true;

    int index_beg;
    int index_incr;

    if(orientation == 0){

        // update element index
        new_elt_index = elt->child(0);

        // update DOF map
        index_beg = 0;
        index_incr = 1;
        if(sign){
            index_beg = degree;
            index_incr *= -1;
        }
    }
    else if(orientation == 1){

        // update element index
        new_elt_index = elt->child(1);

        // update DOF map
        index_beg = degree;
        index_incr = degree+1;
        if(sign){
            index_beg = (degree+1)*(degree+1)-1;
            index_incr *= -1;
        }
    }
    else if(orientation == 2){

        // update element index
        new_elt_index = elt->child(1);

        // update DOF map
        index_beg = degree*(degree+1);
        index_incr = 1;
        if(sign){
            index_beg = (degree+1)*(degree+1)-1;
            index_incr *= -1;
        }
    }
    else {

        // update element index
        new_elt_index = elt->child(0);

        // update DOF map
        index_beg = 0;
        index_incr = degree+1;
        if(sign){
            index_beg = (degree+1)*degree;
            index_incr *= -1;
        }
    }

    if(left)
        face->setLeftElementIndex(new_elt_index);
    else
        face->setRightElementIndex(new_elt_index);
    new_elt = mesh->element(new_elt_index);

    for (int idof=0; idof<face->controlPointNumber(); idof++){
    	for (int ivar=0; ivar<max_field_number; ivar++){
    		if(left){
    			face->setLeftDofMap(idof,ivar,new_elt->globalId(index_beg+index_incr*idof,ivar));
    			face->setLeftPointMap(idof, index_beg+index_incr*idof);
    		}
    		else{
    			face->setRightDofMap(idof,ivar,new_elt->globalId(index_beg+index_incr*idof,ivar));
    			face->setRightPointMap(idof, index_beg+index_incr*idof);
    		}
    	}
    }

    return;
}


/////////////////////////////////////////////////////////////////

void igMeshRefiner::addFace(igElement *elt, int refine_type)
{
    IG_ASSERT(elt, "no element");

    int dimension = elt->physicalDimension();
    vector<double> *coord = new vector<double>;
    coord->resize(dimension);

    int max_field_number = max(mesh->variableNumber(),mesh->derivativeNumber());
    max_field_number = max(max_field_number,mesh->meshDimension()+1);
    int new_elt_index;
    int degree = elt->cellDegree();
    igElement *new_elt;
    int partition_id = elt->partition();

    // refinement along Xi
    if(refine_type == 1){

        // create new face at mid element
        igFace *new_face = new igFace;

        new_face->setCellId(mesh->faceNumber());
        new_face->setPhysicalDimension(dimension);
        new_face->setParametricDimension(dimension-1);
        new_face->setDegree(degree);
        new_face->setGaussPointNumberByDirection(elt->gaussPointNumberByDirection());
        new_face->setVariableNumber(mesh->variableNumber());
        new_face->setDerivativeNumber(mesh->derivativeNumber());
        new_face->setType(0);
        new_face->setLeftPartitionId(partition_id);
        new_face->setRightPartitionId(partition_id);
        new_face->setLeftInside();
        new_face->setRightInside();
        new_face->setCoordinates(this->coordinates);
        new_face->setVelocities(this->velocities);

        new_face->initializeDegreesOfFreedom();

        int elt_left_index = elt->child(0);
        igElement *elt_left = mesh->element(elt_left_index);
        int elt_right_index = elt->child(1);
        igElement *elt_right = mesh->element(elt_right_index);

        new_face->setLeftElementIndex(elt_left_index);
        new_face->setOrientationLeft(1);
        new_face->setSenseLeft(1);
        for (int idof=0; idof<new_face->controlPointNumber(); idof++){
        	for (int ivar=0; ivar<max_field_number; ivar++)
        		new_face->setLeftDofMap(idof,ivar,elt_left->globalId(degree+idof*(degree+1),ivar));
        	new_face->setLeftPointMap(idof, degree+idof*(degree+1));
        }
        new_face->setRightElementIndex(elt_right_index);
        new_face->setOrientationRight(3);
        new_face->setSenseRight(1);
        for (int idof=0; idof<new_face->controlPointNumber(); idof++){
        	for (int ivar=0; ivar<max_field_number; ivar++)
        		new_face->setRightDofMap(idof,ivar,elt_right->globalId(idof*(degree+1),ivar));
        	new_face->setRightPointMap(idof, idof*(degree+1));
        }

        for (int idim=0; idim<dimension; idim++)
            coord->at(idim) = elt_left->barycenter(idim);

        new_face->initializeGeometry();
        new_face->initializeNormal(coord);

        // storage in the face list
        mesh->addFace(new_face);
        mesh->computeFaceNumber();

    }
    else { // refinement along Eta

        // create new face at mid element
        igFace *new_face = new igFace;

        new_face->setCellId(mesh->faceNumber());
        new_face->setPhysicalDimension(dimension);
        new_face->setParametricDimension(dimension-1);
        new_face->setDegree(degree);
        new_face->setGaussPointNumberByDirection(elt->gaussPointNumberByDirection());
        new_face->setVariableNumber(mesh->variableNumber());
        new_face->setDerivativeNumber(mesh->derivativeNumber());
        new_face->setType(0);
        new_face->setLeftPartitionId(partition_id);
        new_face->setRightPartitionId(partition_id);
        new_face->setLeftInside();
        new_face->setRightInside();
        new_face->setCoordinates(this->coordinates);
        new_face->setVelocities(this->velocities);

        new_face->initializeDegreesOfFreedom();

        int elt_left_index = elt->child(0);
        igElement *elt_left = mesh->element(elt_left_index);
        int elt_right_index = elt->child(1);
        igElement *elt_right = mesh->element(elt_right_index);

        new_face->setLeftElementIndex(elt_left_index);
        new_face->setOrientationLeft(2);
        new_face->setSenseLeft(1);
        for (int idof=0; idof<new_face->controlPointNumber(); idof++){
        	for (int ivar=0; ivar<max_field_number; ivar++)
        		new_face->setLeftDofMap(idof,ivar,elt_left->globalId(degree*(degree+1)+idof,ivar));
        	new_face->setLeftPointMap(idof, degree*(degree+1)+idof);
        }
        new_face->setRightElementIndex(elt_right_index);
        new_face->setOrientationRight(0);
        new_face->setSenseRight(1);
        for (int idof=0; idof<new_face->controlPointNumber(); idof++){
        	for (int ivar=0; ivar<max_field_number; ivar++)
        		new_face->setRightDofMap(idof,ivar,elt_right->globalId(idof,ivar));
        	new_face->setRightPointMap(idof, idof);
        }

        for (int idim=0; idim<dimension; idim++)
            coord->at(idim) = elt_left->barycenter(idim);

        new_face->initializeGeometry();
        new_face->initializeNormal(coord);

        // storage in the face list
        mesh->addFace(new_face);
        mesh->computeFaceNumber();

    }

    delete coord;

    return;
}

/////////////////////////////////////////////////////////////////////////////

void igMeshRefiner::updateMap(igFace *face, bool left)
{
    IG_ASSERT(face, "no face");
    int max_field_number = max(mesh->variableNumber(),mesh->derivativeNumber());
    max_field_number = max(max_field_number,mesh->meshDimension()+1);
    igElement *elt;
    bool interior = true;
    int ctrl_pts_per_elt;

    if(face->type() != id_interior)
        interior = false;

    if(left){
        elt = mesh->element(face->leftElementIndex());
        ctrl_pts_per_elt = elt->controlPointNumber();
    }
    else if(interior){
        elt = mesh->element(face->rightElementIndex());
        ctrl_pts_per_elt = elt->controlPointNumber();
    }

    for (int ivar=0; ivar<max_field_number; ivar++){
        for (int idof=0; idof<face->controlPointNumber(); idof++){

            if(left){
                int previous_id = face->dofLeft(idof,ivar);
                int new_id = previous_id + added_elt_number*ctrl_pts_per_elt*ivar;
                face->setLeftDofMap(idof,ivar,new_id);
            } else if(interior){
                int previous_id = face->dofRight(idof,ivar);
                int new_id = previous_id + added_elt_number*ctrl_pts_per_elt*ivar;
                face->setRightDofMap(idof,ivar,new_id);
            }
        }
    }

    return;
}

/////////////////////////////////////////////////////////////////////////////////

void igMeshRefiner::updateHalfFace(igFace *face, bool left)
{
    IG_ASSERT(face, "no face");

    igElement *elt;
    int orientation;
    int sense;
    if(left){
        elt = mesh->element(face->leftElementIndex());
        orientation = face->leftOrientation();
        sense = face->leftSense();
    }
    else{
        elt = mesh->element(face->rightElementIndex());
        orientation = face->rightOrientation();
        sense = face->rightSense();
    }

    int dimension = elt->physicalDimension();

    int max_field_number = max(mesh->variableNumber(),mesh->derivativeNumber());
    max_field_number = max(max_field_number,mesh->meshDimension()+1);
    int new_elt_index;
    int degree = elt->cellDegree();
    igElement *new_elt;

    bool sign = false;
    if(sense<0)
        sign = true;

    // update element index according to child rank
    if(sign)
        new_elt_index = elt->child(2-face->childRank());
    else
        new_elt_index = elt->child(face->childRank()-1);

    if(left)
        face->setLeftElementIndex(new_elt_index);
    else
        face->setRightElementIndex(new_elt_index);
    new_elt = mesh->element(new_elt_index);

    int index_beg;
    int index_incr;

    if(orientation == 0){

        // update DOF map
        index_beg = 0;
        index_incr = 1;
        if(sign){
            index_beg = degree;
            index_incr *= -1;
        }
    }
    else if(orientation == 1){

        // update DOF map
        index_beg = degree;
        index_incr = degree+1;
        if(sign){
            index_beg = (degree+1)*(degree+1)-1;
            index_incr *= -1;
        }
    }
    else if(orientation == 2){

        // update DOF map
        index_beg = degree*(degree+1);
        index_incr = 1;
        if(sign){
            index_beg = (degree+1)*(degree+1)-1;
            index_incr *= -1;
        }
    }
    else {

        // update DOF map
        index_beg = 0;
        index_incr = degree+1;
        if(sign){
            index_beg = (degree+1)*degree;
            index_incr *= -1;
        }
    }

    for (int idof=0; idof<face->controlPointNumber(); idof++){
    	for (int ivar=0; ivar<max_field_number; ivar++){
    		if(left){
    			face->setLeftDofMap(idof,ivar,new_elt->globalId(index_beg+index_incr*idof,ivar));
    			face->setLeftPointMap(idof, index_beg+index_incr*idof);
    		}
    		else{
    			face->setRightDofMap(idof,ivar,new_elt->globalId(index_beg+index_incr*idof,ivar));
    			face->setRightPointMap(idof, index_beg+index_incr*idof);
    		}
    	}
    }

    // correct the parameter interval
    if(left)
        face->setParameterIntervalLeft(0.,1.);
    else
        face->setParameterIntervalRight(0.,1.);
    face->evaluateGaussPoints();

    // correct the rank
    face->setChildRank(0);
    face->setHalfFaceLeft(false);
    face->setHalfFaceRight(false);
    return;
}

/////////////////////////////////////////////////////////////////////////////

void igMeshRefiner::checkRefinement(void)
{
    // by default all elements can be refined
    this->elt_refinable_xi.assign(mesh->elementNumber(),true);
    this->elt_refinable_eta.assign(mesh->elementNumber(),true);

    for (int ifac=0; ifac<mesh->faceNumber(); ifac++){

        igFace *face = mesh->face(ifac);

        if(face->isActive()){

            int left_index = face->leftElementIndex();
            int right_index = face->rightElementIndex();
            igElement *left_elt = mesh->element(left_index);
            igElement *right_elt = mesh->element(right_index);

            int orientation_left = face->leftOrientation();
            int orientation_right = face->rightOrientation();

            int level_left;
            int level_right;

            if(orientation_left == 0 || orientation_left == 2){ // left south or north
                level_left = mesh->elementLevel(left_index, 0);

                if(orientation_right == 0 || orientation_right == 2){ // right south or north
                    level_right = mesh->elementLevel(right_index,0);
                    if(level_left > level_right)
                        elt_refinable_xi.at(left_index) = false;
                    if(level_left < level_right)
                        elt_refinable_xi.at(right_index) = false;
                } else { // right east or west
                    level_right = mesh->elementLevel(right_index,1);
                    if(level_left > level_right)
                        elt_refinable_xi.at(left_index) = false;
                    if(level_left < level_right)
                        elt_refinable_eta.at(right_index) = false;
                }

            } else {
                level_left = mesh->elementLevel(left_index, 1); // left east or west

                if(orientation_right == 0 || orientation_right == 2){ // right south or north
                    level_right = mesh->elementLevel(right_index,0);
                    if(level_left > level_right)
                        elt_refinable_eta.at(left_index) = false;
                    if(level_left < level_right)
                        elt_refinable_xi.at(right_index) = false;
                } else { // right east or west
                    level_right = mesh->elementLevel(right_index,1);
                    if(level_left > level_right)
                        elt_refinable_eta.at(left_index) = false;
                    if(level_left < level_right)
                        elt_refinable_eta.at(right_index) = false;
                }
            }
        }
    }

    return;
}

/////////////////////////////////////////////////////////////////////////////

void igMeshRefiner::propagateRefinement(void)
{

    for (int ifac=0; ifac<mesh->faceNumber(); ifac++){

        igFace *face = mesh->face(ifac);

        if(face->isActive()){

            int left_index = face->leftElementIndex();
            int right_index = face->rightElementIndex();
            igElement *left_elt = mesh->element(left_index);
            igElement *right_elt = mesh->element(right_index);

            int orientation_left = face->leftOrientation();
            int orientation_right = face->rightOrientation();

            int level_left;
            int level_right;

            if(orientation_left == 0 || orientation_left == 2){ // left south or north
                level_left = mesh->elementLevel(left_index,0);

                if(orientation_right == 0 || orientation_right == 2){ // right south or north
                    level_right = mesh->elementLevel(right_index,0);

                    if(level_left > level_right && blocking_flag.at(left_index) == 1){ // blocked for Xi left
                        if(elt_refinable_xi.at(right_index) && refine_flag.at(right_index) == 0){
                            refine_flag.at(right_index) = 1;
                            this->added_elt_number += 2;
                        }
                    }
                    if(level_left < level_right && blocking_flag.at(right_index) == 1){ // blocked for Xi right
                        if(elt_refinable_xi.at(left_index)  && refine_flag.at(left_index) == 0){
                            refine_flag.at(left_index) = 1;
                            this->added_elt_number += 2;
                        }
                    }

                } else { // right east or west
                    level_right = mesh->elementLevel(right_index,1);

                    if(level_left > level_right && blocking_flag.at(left_index) == 1){ // blocked for Xi left
                        if(elt_refinable_eta.at(right_index)  && refine_flag.at(right_index) == 0){
                            refine_flag.at(right_index) = 2;
                            this->added_elt_number += 2;
                        }
                    }
                    if(level_left < level_right && blocking_flag.at(right_index) == 2){ // blocked for Eta right
                        if(elt_refinable_xi.at(left_index)  && refine_flag.at(left_index) == 0){
                            refine_flag.at(left_index) = 1;
                            this->added_elt_number += 2;
                        }
                    }
                }

            } else {
                level_left = mesh->elementLevel(left_index,1); // left east or west

                if(orientation_right == 0 || orientation_right == 2){ // right south or north
                    level_right = mesh->elementLevel(right_index,0);

                    if(level_left > level_right && blocking_flag.at(left_index) == 2){ // blocked for Eta left
                        if(elt_refinable_xi.at(right_index)  && refine_flag.at(right_index) == 0){
                            refine_flag.at(right_index) = 1;
                            this->added_elt_number += 2;
                        }
                    }
                    if(level_left < level_right && blocking_flag.at(right_index) == 1){ // blocked for Xi right
                        if(elt_refinable_eta.at(left_index)  && refine_flag.at(left_index) == 0){
                            refine_flag.at(left_index) = 2;
                            this->added_elt_number += 2;
                        }
                    }
                } else { // right east or west
                    level_right = mesh->elementLevel(right_index,1);

                    if(level_left > level_right && blocking_flag.at(left_index) == 2){ // blocked for Eta left
                        if(elt_refinable_eta.at(right_index)  && refine_flag.at(right_index) == 0){
                            refine_flag.at(right_index) = 2;
                            this->added_elt_number += 2;
                        }
                        if(level_left < level_right && blocking_flag.at(right_index) == 2){ // blocked for Eta right
                            if(elt_refinable_eta.at(left_index)  && refine_flag.at(left_index) == 0){
                                refine_flag.at(left_index) = 2;
                                this->added_elt_number += 2;
                            }
                        }
                    }
                }
            }
        }
    }

    return;
}
