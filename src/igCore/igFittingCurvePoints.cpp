/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/

#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <cmath>

#include "igFittingCurvePoints.h"
#include "igBasisBSpline.h"


using namespace std;

/////////////////////////////////////////////////////////////////////////////

igFittingCurvePoints::igFittingCurvePoints()
{

}

igFittingCurvePoints::~igFittingCurvePoints(void)
{

}

/////////////////////////////////////////////////////////////////////////////

void igFittingCurvePoints::setUniform(bool flag_uniform)
{
	this->uniform = flag_uniform;
}

void igFittingCurvePoints::setSpanNumber(int n_spans)
{
	this->span_number = n_spans;
}

void igFittingCurvePoints::setMultiplicity(int s)
{
	this->multiplicity = s;
}

void igFittingCurvePoints::setCoordinateBounds(double x_min, double x_max, double y_min, double y_max)
{
    this->x_min = x_min;
    this->x_max = x_max;
    this->y_min = y_min;
    this->y_max = y_max;
}

void igFittingCurvePoints::setXPoints(vector<double> x_pts)
{
    this->x_pts = x_pts;
}

void igFittingCurvePoints::setYPoints(vector<double> y_pts)
{
    this->y_pts = y_pts;
}

vector<vector<double>> igFittingCurvePoints::xSolution(void)
{
	return sol_x;
}

vector<vector<double>> igFittingCurvePoints::ySolution(void)
{
	return sol_y;
}

///////////////////////////////////////////////////////////////

void igFittingCurvePoints::run(void)
{

	// initialize B-Spline basis
	int knot_number = 2*(degree + 1) + (span_number - 1)*multiplicity;
	vector<double> knot_vector(knot_number,0.);
    igBasisBSpline *basis = new igBasisBSpline(degree,&knot_vector);

    int function_number = basis->functionNumber();
    int size = function_number - 2;
    double *rhs_x = new double[size];
    double *rhs_y = new double[size];
    double *mat = new double[size*size];

    vector<double> values(function_number,0.);
    vector<double> cp_x(function_number,0.);
    vector<double> cp_y(function_number,0.);

    // initialize matrix and rhs, sol
    for (int ils=0; ils<size; ils++){
        rhs_x[ils] = 0.;
        rhs_y[ils] = 0.;
        for (int jls=0; jls<size; jls++)
            mat[ils*size+jls] = 0.;
    }

    // compute curvilinear coordinate
    vector<double> coord(sample_number,0.);
    for(int i=1; i<sample_number; i++){
    	double dl =  sqrt(pow(x_pts.at(i)-x_pts.at(i-1),2) + pow(y_pts.at(i)-y_pts.at(i-1),2));
    	coord.at(i) = coord.at(i-1) + dl;
    }
    for(int i=0; i<sample_number; i++)
    	coord[i] /= coord.back();

    // compute knot vector
    double spacing = ((double)sample_number - 1.)/((double)span_number);
    int k=0;
    for(int i=0; i<(span_number+1); i++){

    	int index = (int)floor(spacing*i);
    	double u;

    	if(uniform)
        	u = (double)i/(double)span_number;
    	else
    		u = coord.at(index);

    	int s = multiplicity;
    	if(i==0 || i==span_number)
    		s = degree + 1;

    	for(int j=0; j<s; j++){
    		knot_vector.at(k) = u;
    		k++;
    	}

    }

    // compute matrix and rhs
    for (int kls=0; kls<sample_number; kls++){

        double target_x = x_pts.at(kls);
        double target_y = y_pts.at(kls);

        basis->evalFunction(coord.at(kls),values);

        for (int ils=0; ils<size; ils++){

            // rhs term
            rhs_x[ils] += values.at(ils+1) * target_x;
            rhs_y[ils] += values.at(ils+1) * target_y;

            // matrix
            for (int jls=0; jls<size; jls++)
                mat[ils*size+jls] += values.at(ils+1)*values.at(jls+1);

            // modification of rhs for bound constraints
            rhs_x[ils] -= values.at(ils+1)*values.at(0)*x_min
                    +  values.at(ils+1)*values.at(function_number-1)*x_max;
            rhs_y[ils] -= values.at(ils+1)*values.at(0)*y_min
                    +  values.at(ils+1)*values.at(function_number-1)*y_max;

        }
    }

    // solve system
    inverse_system(mat,size);

    // control points of B-Spline curve
    cp_x.front() = x_min;
    cp_y.front() = y_min;
    for (int ils=0; ils<size; ils++){
        cp_x.at(ils+1) = 0.;
        cp_y.at(ils+1) = 0.;
        for (int jls=0; jls<size; jls++){
            cp_x.at(ils+1) += mat[ils*size+jls]*rhs_x[jls];
            cp_y.at(ils+1) += mat[ils*size+jls]*rhs_y[jls];
        }
    }
    cp_x.back() = x_max;
    cp_y.back() = y_max;

    // plotting of fit points
    ofstream fit_file("curve_fit.dat", ios::out);
    for (int kls=0; kls<sample_number; kls++){
        basis->evalFunction(coord.at(kls),values);
        double x_pt = 0;
        double y_pt = 0;
        for(int i=0; i<function_number; i++){
            x_pt += values[i] * cp_x[i];
            y_pt += values[i] * cp_y[i];
        }
        fit_file << x_pt << " " << y_pt << endl;
    }

    // plotting of control points
    ofstream fit_pts_file("curve_fit_pts.dat", ios::out);
    for(int i=0; i<function_number; i++){
        fit_pts_file <<  cp_x[i] << " " << cp_y[i] << endl;
    }


    // extract Bezier segments from B-Spline curve
    basis->extractBezier(&cp_x,&cp_y,&sol_x,&sol_y);

    delete basis;
}
