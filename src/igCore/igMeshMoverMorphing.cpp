/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igMeshMoverMorphing.h"

#include "igMesh.h"
#include "igCell.h"
#include "igElement.h"
#include "igFace.h"
#include "igInterface.h"
#include "igRungeKuttaSSP.h"
#include "igRungeKutta4.h"
#include "igRungeKutta2.h"

#include <igAssert.h>

#include <igGenerator/igMeshMovementMorphing.h>
#include <igDistributed/igCommunicator.h>

#include <iostream>
#include <stdlib.h>
#include <cmath>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igMeshMoverMorphing::igMeshMoverMorphing(igMesh *mesh, vector<double> *coordinates, vector<double> *velocities) : igMeshMover(mesh,coordinates,velocities)
{
	IG_ASSERT(mesh, "no mesh");

	this->adaptive_mesh = false;

	int internal_dof = mesh->controlPointNumber();
	int shared_dof = mesh->sharedControlPointNumber();
	int dimension = mesh->meshDimension();

    this->total_coordinate_number = internal_dof + shared_dof;
    int degree = mesh->element(0)->cellDegree();

    this->dof_number_per_elt = (degree+1)*(degree+1);
}

igMeshMoverMorphing::~igMeshMoverMorphing(void)
{

}


/////////////////////////////////////////////////////////////////////////////

void igMeshMoverMorphing::setDeformation(vector<double> *deformation)
{
	this->deformation = deformation;
}

/////////////////////////////////////////////////////////////////////////////


void igMeshMoverMorphing::initializeMovement(const string& integrator, const string& movement_type,  vector<double> *param_ALE, int refine_max_level)
{
    this->movement_law = movement_type;
    this->movement_param = param_ALE;

    this->mesh_movement_law = periodicDeformation;

    if(movement_param->size()>0){
        this->frequency = movement_param->at(0);
    } else {
        this->frequency = 0.5;
    }

    this->refine_max_level = refine_max_level;
    if(refine_max_level == 0)
        this->adaptive_mesh = false;
    else
        this->adaptive_mesh = true;

    movement_param->resize(3);

    return;
}

void igMeshMoverMorphing::retrieveParameters(int iel,int icp)
{
    movement_param->at(0) = frequency; // deformation frequency

    int index = iel * dof_number_per_elt + icp;
    movement_param->at(1) = deformation->at(index); // defomation along x

    index += total_coordinate_number;
    movement_param->at(2) = deformation->at(index); // defomation along y
}
