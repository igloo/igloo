/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igMeshMoverFSISpring.h"

#include "igMesh.h"
#include "igCell.h"
#include "igElement.h"
#include "igFace.h"
#include "igInterface.h"

#include <igAssert.h>

#include <igGenerator/igMeshMovementFSISpring.h>
#include <igDistributed/igCommunicator.h>

#include <iostream>
#include <stdlib.h>
#include <cmath>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igMeshMoverFSISpring::igMeshMoverFSISpring(igMesh *mesh, vector<double> *coordinates, vector<double> *velocities) : igMeshMoverFSI(mesh,coordinates,velocities)
{
	IG_ASSERT(mesh, "no mesh");

    // damping coefficients for level 0

    this->damping = new vector<double>;
    this->damping->resize(mesh->controlPointNumber(),0);

    double y_rigid = 0.25;
    double y_damp = 9;
    double x_rigid = 0.75;
    double x_damp = 9;

    int counter = 0;
    for (int iel = 0; iel < mesh->elementNumber(); ++iel){
        igElement *elt = mesh->element(iel);
        for(int idof=0; idof<elt->controlPointNumber(); idof++){

            double x_pt = this->coordinates->at(elt->globalId(idof,0));
            double y_pt = this->coordinates->at(elt->globalId(idof,1));

            double damping_function_y = 0;
            if(-y_rigid <= y_pt && y_pt <= y_rigid){ // rigid deformation
                damping_function_y = 1;
            } else if (-y_damp <= y_pt && y_pt < -y_rigid){
                damping_function_y = cos(M_PI/2*(-y_rigid - y_pt)/(y_damp - y_rigid)); // damping y<0
            } else if (y_rigid < y_pt && y_pt <= y_damp){
                damping_function_y = cos(M_PI/2*(y_pt - y_rigid)/(y_damp - y_rigid)); // damping y>0
            }

            double damping_function_x = 0;
            if(-x_rigid <= x_pt && x_pt <= x_rigid){ // rigid deformation
                damping_function_x = 1;
            } else if (-x_damp <= x_pt && x_pt < -x_rigid){
                damping_function_x = cos(M_PI/2*(-x_rigid - x_pt)/(x_damp - x_rigid)); // damping x<0
            } else if (x_rigid < x_pt && x_pt <= x_damp){
                damping_function_x = cos(M_PI/2*(x_pt - x_rigid)/(x_damp - x_rigid)); // damping x>0
            }

            this->damping->at(counter) = damping_function_x * damping_function_y;
            counter++;
        }
    }

}

igMeshMoverFSISpring::~igMeshMoverFSISpring(void)
{
    delete this->damping;
}

/////////////////////////////////////////////////////////////////////////////

void igMeshMoverFSISpring::initializeMovement(const string& integrator, const string& movement_type,  vector<double> *param_ALE, int refine_max_level)
{
    this->movement_law = movement_type;
    this->movement_param = param_ALE;

    this->mesh_movement_law = springDeformation;

    this->refine_max_level = refine_max_level;
    if(refine_max_level == 0)
        this->adaptive_mesh = false;
    else
        this->adaptive_mesh = true;

    movement_param->resize(7);

    return;
}

////////////////////////////////////////////////////////////////////////////////

void igMeshMoverFSISpring::retrieveParameters(int iel,int icp)
{
    int index = iel * dof_number_per_elt + icp;

    movement_param->at(0) = damping->at(index); // damping coefficient
    movement_param->at(1) = interface_velocities_ref->at(0) * (1-fractional_step) + interface_velocities->at(0) * fractional_step; // velocity along x
    movement_param->at(2) = interface_velocities_ref->at(1) * (1-fractional_step) + interface_velocities->at(1) * fractional_step; // velocity along y
    movement_param->at(3) = interface_velocities_ref->at(2) * (1-fractional_step) + interface_velocities->at(2) * fractional_step; // rotation velocity
    movement_param->at(4) = interface_displacements_ref->at(0) * (1-fractional_step) + interface_displacements->at(0) * fractional_step; // translation along x
    movement_param->at(5) = interface_displacements_ref->at(1) * (1-fractional_step) + interface_displacements->at(1) * fractional_step; // translation along y
    movement_param->at(6) = interface_displacements_ref->at(2) * (1-fractional_step) + interface_displacements->at(2) * fractional_step; // rotation angle

}
