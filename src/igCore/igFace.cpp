/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igFace.h"

#include "igQuadrature.h"
#include "igElement.h"
#include "igBasisBezier.h"
#include "igBoundaryIds.h"

#include <iostream>
#include <math.h>
#include <limits>
#include <igAssert.h>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igFace::igFace() : igCell()
{
    shared_face = false;
    half_face_left = false;
    half_face_right = false;
    left_partition_id = 0;
    right_partition_id = 0;
    left_inside = false;
    right_inside = false;

    param_min_left1 = 0.;
    param_max_left1 = 1.;
    param_min_right1 = 0.;
    param_max_right1 = 1.;

    param_min_left2 = 0.;
    param_max_left2 = 1.;
    param_min_right2 = 0.;
    param_max_right2 = 1.;

    sliding_face = false;
    parent_sliding_face = false;
}

/////////////////////////////////////////////////////////////////////////////

void igFace::setLeftDofMap(int local_id, int var_id, int global_id)
{
    dof_map_left[var_id*control_point_number+local_id] = global_id;
    return;
}

void igFace::setRightDofMap(int local_id, int var_id, int global_id)
{
    dof_map_right[var_id*control_point_number+local_id] = global_id;
    return;
}

void igFace::setLeftDofMap(vector<int> dof)
{
	this->dof_map_left = dof;
}

void igFace::setRightDofMap(vector<int> dof)
{
	this->dof_map_right = dof;
}

void igFace::setLeftPointMap(int pt_face_id, int pt_elt_id)
{
    pt_map_left[pt_face_id] = pt_elt_id;
}

void igFace::setRightPointMap(int pt_face_id, int pt_elt_id)
{
    pt_map_right[pt_face_id] = pt_elt_id;
}

void igFace::setLeftElementIndex(int index)
{
    left_element_index = index;
    return;
}

void igFace::setRightElementIndex(int index)
{
    right_element_index = index;
    return;
}

void igFace::setOrientationLeft(int orientation)
{
    left_orientation = orientation;
    return;
}

void igFace::setOrientationRight(int orientation)
{
    right_orientation = orientation;
    return;
}

void igFace::setSenseLeft(int sense)
{
    left_sense = sense;
    return;
}

void igFace::setSenseRight(int sense)
{
    right_sense = sense;
    return;
}

void igFace::setType(int type)
{
    face_type = type;
    return;
}

void igFace::setShared(void)
{
    shared_face = true;
    return;
}

void igFace::setLeftPartitionId(int neighbor_id)
{
    left_partition_id = neighbor_id;
    return;
}

void igFace::setRightPartitionId(int neighbor_id)
{
    right_partition_id = neighbor_id;
    return;
}

void igFace::setLeftInside(void)
{
    left_inside = true;
    return;
}

void igFace::setRightInside(void)
{
    right_inside = true;
    return;
}

void igFace::setParameterIntervalLeft(double p_min, double p_max)
{
    param_min_left1 = p_min;
    param_max_left1 = p_max;
    return;
}

void igFace::setParameterIntervalRight(double p_min, double p_max)
{
    param_min_right1 = p_min;
    param_max_right1 = p_max;
    return;
}

void igFace::setParameterIntervalLeft(double p_min1, double p_max1,double p_min2, double p_max2)
{
    param_min_left1 = p_min1;
    param_max_left1 = p_max1;
    param_min_left2 = p_min2;
    param_max_left2 = p_max2;
    return;
}

void igFace::setParameterIntervalRight(double p_min1, double p_max1,double p_min2, double p_max2)
{
    param_min_right1 = p_min1;
    param_max_right1 = p_max1;
    param_min_right2 = p_min2;
    param_max_right2 = p_max2;
    return;
}

void igFace::setHalfFaceLeft(bool half)
{
    this->half_face_left = half;
}

void igFace::setHalfFaceRight(bool half)
{
    this->half_face_right = half;
}

/////////////////////////////////////////////////////////////////////////////

void igFace::setSliding()
{
	this->sliding_face = true;

	this->local_coordinates.resize(control_point_number_by_direction*(physical_dimension+1),0.);
	this->local_velocities.resize(control_point_number_by_direction*physical_dimension,0.);

	// initialize local coordinates
	for(int ipt=0; ipt<control_point_number_by_direction; ipt++){
		for(int icomp=0; icomp<=physical_dimension; icomp++){
			local_coordinates.at(control_point_number_by_direction*icomp + ipt) = coordinates->at(this->dofLeft(ipt, icomp));
		}
	}
}

void igFace::setParentSliding()
{
	this->parent_sliding_face = true;
}

void igFace::setLocalCoordinate(int index, int component, double coord)
{
	local_coordinates.at(control_point_number_by_direction*component + index) = coord;
}

void igFace::setLocalWeight(int index, double weight)
{
	local_coordinates.at(control_point_number_by_direction*physical_dimension + index) = weight;
}

void igFace::setLocalVelocity(int index, int component, double vel)
{
	local_velocities.at(control_point_number_by_direction*component + index) = vel;
}

void igFace::setInterfaceQuadratureCoordinate(int index, double par)
{
    interface_quadrature_coordinate_list.at(index) = par;
}

void igFace::setInterfaceQuadratureWeight(int index, double weight)
{
    interface_quadrature_weight_list.at(index) = weight;
}

void igFace::setInterfaceQuadratureLabel(int index, int label)
{
    interface_quadrature_label_list.at(index) = label;
}

/////////////////////////////////////////////////////////////////////////////

int igFace::dofLeft(int local_id, int var_id) const
{
    return dof_map_left[var_id*control_point_number+local_id];
}

int igFace::dofRight(int local_id, int var_id) const
{
    return dof_map_right[var_id*control_point_number+local_id];
}

int igFace::pointLeft(int pt_face_id)
{
    return pt_map_left[pt_face_id];
}

int igFace::pointRight(int pt_face_id)
{
    return pt_map_right[pt_face_id];
}

vector<int> igFace::dofLeft() const
{
	return dof_map_left;
}

vector<int> igFace::dofRight() const
{
	return dof_map_right;
}

double igFace::controlPointCoordinate(int index, int component) const
{
	if(sliding_face)
		return this->local_coordinates.at(control_point_number_by_direction*component + index);
	else if(half_face_left)
        return this->coordinates->at(this->dofRight(index, component));
    else
        return this->coordinates->at(this->dofLeft(index, component));
}

void igFace::controlPointCoordinates(int index, vector<double> *coord)
{
	if(sliding_face){
		for(int idim=0; idim<physical_dimension; idim++)
			coord->at(idim) = this->local_coordinates.at(control_point_number_by_direction*idim + index);
	} else if(half_face_left){
        for(int idim=0; idim<physical_dimension; idim++)
            coord->at(idim) = this->coordinates->at(dofRight(index, idim));
    } else {
        for(int idim=0; idim<physical_dimension; idim++)
            coord->at(idim) = this->coordinates->at(dofLeft(index, idim));
    }
}

double igFace::controlPointWeight(int index) const
{
	if(sliding_face)
		return this->local_coordinates.at(control_point_number_by_direction*physical_dimension + index);
	else if(half_face_left)
        return this->coordinates->at(this->dofRight(index, this->physical_dimension));
    else
        return this->coordinates->at(this->dofLeft(index, this->physical_dimension));
}

double igFace::controlPointVelocityComponent(int index, int component) const
{
	if(sliding_face)
		return this->local_velocities.at(control_point_number_by_direction*component + index);
	else if(half_face_left)
		return this->velocities->at(this->dofRight(index, component));
	else
		return this->velocities->at(this->dofLeft(index, component));
}

void igFace::controlPointVelocity(int index, vector<double> *vel)
{
	if(sliding_face){
		for(int idim=0; idim<physical_dimension; idim++)
			vel->at(idim) = local_velocities.at(control_point_number_by_direction*idim + index);
	} else if(half_face_left){
        for(int idim=0; idim<physical_dimension; idim++)
            vel->at(idim) = this->velocities->at(dofRight(index, idim));
    } else {
        for(int idim=0; idim<physical_dimension; idim++)
            vel->at(idim) = this->velocities->at(dofLeft(index, idim));
    }
}


double* igFace::gaussPointNormalPtr(int pt_index)
{
    return &gauss_point_normal_list[pt_index*physical_dimension];
}

double* igFace::gaussPointTangent1Ptr(int pt_index)
{
    return &gauss_point_tangent1_list[pt_index*physical_dimension];
}

double* igFace::gaussPointTangent2Ptr(int pt_index)
{
    return &gauss_point_tangent2_list[pt_index*physical_dimension];
}

double igFace::gaussPointFunctionValueLeft(int pt_index, int fun_index)
{
    return gauss_point_value_left_list[pt_index*control_point_number+fun_index];
}

double *igFace::gaussPointFunctionPtrLeft(int pt_index)
{
    return &gauss_point_value_left_list[pt_index*control_point_number];
}

double igFace::gaussPointFunctionValueRight(int pt_index, int fun_index)
{
    return gauss_point_value_right_list[pt_index*control_point_number+fun_index];
}

double *igFace::gaussPointFunctionPtrRight(int pt_index)
{
    return &gauss_point_value_right_list[pt_index*control_point_number];
}

double igFace::interfaceQuadratureCoordinate(int index)
{
    return interface_quadrature_coordinate_list.at(index);
}

double igFace::interfaceQuadratureWeight(int index)
{
    return interface_quadrature_weight_list.at(index);
}

int igFace::interfaceQuadratureLabel(int index)
{
    return interface_quadrature_label_list.at(index);
}

int igFace::leftElementIndex(void)
{
    return left_element_index;
}

int igFace::rightElementIndex(void)
{
    return right_element_index;
}

int igFace::leftOrientation(void)
{
    return left_orientation;
}

int igFace::rightOrientation(void)
{
    return right_orientation;
}

int igFace::leftSense(void)
{
    return left_sense;
}

int igFace::rightSense(void)
{
    return right_sense;
}

int igFace::type(void)
{
    return face_type;
}

bool igFace::shared(void)
{
    return shared_face;
}

int igFace::leftPartitionId(void)
{
    return left_partition_id;
}

int igFace::rightPartitionId(void)
{
    return right_partition_id;
}

bool igFace::leftInside(void)
{
    return left_inside;
}

bool igFace::rightInside(void)
{
    return right_inside;
}

double igFace::leftParameterBegin(void)
{
    return param_min_left1;
}

double igFace::leftParameterEnd(void)
{
    return param_max_left1;
}

double igFace::rightParameterBegin(void)
{
    return param_min_right1;
}

double igFace::rightParameterEnd(void)
{
    return param_max_right1;
}

double igFace::parameterBegin(int direction, bool left)
{
    double param;
    if(left){
        switch(direction){
        case 0:
            param = this->param_min_left1;
            break;
        case 1:
            param = this->param_min_left2;
            break;
        }
    } else {
        switch(direction){
        case 0:
            param = this->param_min_right1;
            break;
        case 1:
            param = this->param_min_right2;
            break;
        }
    }
    return param;
}

double igFace::parameterEnd(int direction, bool left)
{
    double param;
    if(left){
        switch(direction){
        case 0:
            param = this->param_max_left1;
            break;
        case 1:
            param = this->param_max_left2;
            break;
        }
    } else {
        switch(direction){
        case 0:
            param = this->param_max_right1;
            break;
        case 1:
            param = this->param_max_right2;
            break;
        }
    }
    return param;
}

bool igFace::halfFaceLeft(void)
{
    return this->half_face_left;
}

bool igFace::halfFaceRight(void)
{
    return this->half_face_right;
}

bool igFace::isSliding(void)
{
	return this->sliding_face;
}

bool igFace::isParentSliding(void)
{
	return this->parent_sliding_face;
}


/////////////////////////////////////////////////////////////////////////

void igFace::initializeDegreesOfFreedom(void)
{
    int param_size = max(1,parametric_dimension);

    // allocate control point list
    control_point_number_by_direction = degree+1;
    control_point_number = 1;
    for (int idim=0; idim<parametric_dimension; idim++)
        control_point_number *= control_point_number_by_direction;

    // allocate gauss point lists
    gauss_point_number = 1;
    for (int idim=0; idim<parametric_dimension; idim++)
        gauss_point_number *= gauss_point_number_by_direction;
    gauss_point_weight_list.assign(gauss_point_number, 0.);
    gauss_point_coord_list.assign(gauss_point_number*param_size, 0.);
    gauss_point_physical_coord_list.assign(gauss_point_number*physical_dimension, 0.);

    // allocate gauss point attribute lists
    gauss_point_value_list.assign(gauss_point_number*control_point_number, 0.);
    gauss_point_velocity_list.assign(gauss_point_number*physical_dimension,0.);
    gauss_point_value_left_list.assign(gauss_point_number*control_point_number, 0.);
    gauss_point_value_right_list.assign(gauss_point_number*control_point_number, 0.);
    gauss_point_jacobian_list.assign(gauss_point_number, 0.);
    gauss_point_gradient_list.assign(gauss_point_number*control_point_number*param_size, 0.);
    gauss_point_jacobian_matrix_list.assign(gauss_point_number*physical_dimension*param_size, 0.);
    interface_quadrature_coordinate_list.assign(gauss_point_number, 0.);
    interface_quadrature_weight_list.assign(gauss_point_number, 0.);
    interface_quadrature_label_list.assign(gauss_point_number, 0.);

    // compute the Gauss point properties
    igQuadrature quadrature;
    quadrature.initializeGaussPoints(param_size,gauss_point_number_by_direction, gauss_point_coord_list, gauss_point_weight_list);

    // map to left and right dof
    int max_field_number = max(variable_number,derivative_number);
    max_field_number = max(max_field_number,physical_dimension+1);
    dof_map_left.resize(control_point_number*max_field_number);
    dof_map_right.resize(control_point_number*max_field_number);
    pt_map_left.resize(control_point_number);
    pt_map_right.resize(control_point_number);

    // allocate normal vector at gauss points
    gauss_point_normal_list.assign(physical_dimension*gauss_point_number, 0.);
    gauss_point_tangent1_list.assign(physical_dimension*gauss_point_number, 0.);
    gauss_point_tangent2_list.assign(physical_dimension*gauss_point_number, 0.);

    // detect half-face (2D only)
    double param_mid_left = 0.5*(param_min_left1+param_max_left1);
    double param_mid_right = 0.5*(param_min_right1+param_max_right1);

    if(param_mid_left < 0.4 || param_mid_left > 0.6)
        this->setHalfFaceLeft(true);
    if(param_mid_right < 0.4 || param_mid_right > 0.6)
        this->setHalfFaceRight(true);
}

void igFace::initializeGeometry(void)
{
    vector<double> t1;
    vector<double> t2;
    double nx, ny, nz;
    t1.assign(physical_dimension,0.);
    t2.assign(physical_dimension,0.);
    int index = 0 ;

    // evaluate basis functions at Gauss points
	if(sliding_face)
		this->evaluateSlidingGaussPoints();
	else
		this->evaluateGaussPoints();

    // evaluate Jacobian data at gauss points
    for (int ipt=0; ipt<gauss_point_number; ipt++){

        // components of the Jacobian matrix
        for (int i=0; i<physical_dimension; i++){
            for (int j = 0; j < parametric_dimension; ++j) {

                double jacobian = 0;

                for (int ifun = 0; ifun < control_point_number; ++ifun) {
                    jacobian += gaussPointGradientValue(ipt, ifun, j) * this->controlPointCoordinate(ifun, i);
                }
                gauss_point_jacobian_matrix_list[index] = jacobian;
                ++index;
            }
        }

        // computation of the Jacobian
        switch(physical_dimension){

        case 1:
            // no real Jacobian !
            gauss_point_jacobian_list[ipt] = 1.;

            break;

        case 2:
            // jacobian J = sqrt( dx/dxi^2 + dy/dxi^2 )
            gauss_point_jacobian_list[ipt] = sqrt( this->gaussPointJacobianMatrix(ipt, 0, 0)*this->gaussPointJacobianMatrix(ipt, 0, 0) +
                    this->gaussPointJacobianMatrix(ipt, 1, 0)*this->gaussPointJacobianMatrix(ipt, 1, 0) );

            break;

        case 3:
            // Jacobian J = || t1 * t2 ||
            for (int idim = 0; idim < physical_dimension; ++idim) {
                    t1[idim] = this->gaussPointJacobianMatrix(ipt, idim, 0);
                    t2[idim] = this->gaussPointJacobianMatrix(ipt, idim, 1);
            }

            // cross product
            nx = t1[1]*t2[2] - t1[2]*t2[1];
            ny = t1[2]*t2[0] - t1[0]*t2[2];
            nz = t1[0]*t2[1] - t1[1]*t2[0];

            // norm
            gauss_point_jacobian_list[ipt] = sqrt(nx*nx + ny*ny + nz*nz);

            break;
        }

        // Gauss points physical coordinates
        for (int idim=0; idim<physical_dimension; idim++)
            gauss_point_physical_coord_list[ipt*physical_dimension+idim] = 0.;

        for (int ifun = 0; ifun < control_point_number; ++ifun) {
            for (int idim = 0; idim < physical_dimension; ++idim) {
                gauss_point_physical_coord_list[ipt*physical_dimension+idim] += this->gaussPointFunctionValue(ipt, ifun) * this->controlPointCoordinate(ifun, idim);
            }
        }
    }

    // computation of cell area
    cell_area = 0.;
    for (int k=0; k<gauss_point_number; k++){
        double gauss_point_weight =  this->gaussPointWeight(k);
        double gauss_point_jacobian = this->gaussPointJacobian(k);
        cell_area += gauss_point_weight * gauss_point_jacobian;
    }

}

//////////////////////////////////////////////////////////////////////////////

void igFace::computeNormal(void)
{

    switch(physical_dimension){

    case 1:
        // only one gauss point and 1 component in 1D case
        gauss_point_normal_list[0] = 1.;
        break;

    case 2:
        // vector normal to tangent (dy/dxi ; -dx/dxi)
        for (int ipt=0; ipt<gauss_point_number; ipt++){

            double normal_x = 0.;
            double normal_y = 0.;
            for (int ictrl=0; ictrl<control_point_number; ictrl++){
                normal_x += gaussPointGradientValue(ipt, ictrl, 0) * controlPointCoordinate(ictrl, 1);
                normal_y -= gaussPointGradientValue(ipt, ictrl, 0) * controlPointCoordinate(ictrl, 0);
            }

            // scaling
            double norm = sqrt(normal_x*normal_x + normal_y*normal_y);
            gauss_point_normal_list[ipt*physical_dimension] = normal_x/norm;
            gauss_point_normal_list[ipt*physical_dimension+1] = normal_y/norm;

            // tangent vector
            gauss_point_tangent1_list[ipt*physical_dimension] = normal_y/norm;
            gauss_point_tangent1_list[ipt*physical_dimension+1] = -normal_x/norm;
        }
        break;

    case 3:
        // cross product of two tangent vectors
        vector<double> t1;
        vector<double> t2;
        double nx, ny, nz;
        t1.assign(physical_dimension,0.);
        t2.assign(physical_dimension,0.);

        for (int ipt=0; ipt<gauss_point_number; ipt++){

            for (int idim = 0; idim < physical_dimension; ++idim) {
                    t1[idim] = this->gaussPointJacobianMatrix(ipt, idim, 0);
                    t2[idim] = this->gaussPointJacobianMatrix(ipt, idim, 1);
            }

            // cross product
            nx = t1[1]*t2[2] - t1[2]*t2[1];
            ny = t1[2]*t2[0] - t1[0]*t2[2];
            nz = t1[0]*t2[1] - t1[1]*t2[0];

            // scaling
            double norm = sqrt(nx*nx + ny*ny + nz*nz);
            gauss_point_normal_list[ipt*physical_dimension]   = nx/norm;
            gauss_point_normal_list[ipt*physical_dimension+1] = ny/norm;
            gauss_point_normal_list[ipt*physical_dimension+2] = nz/norm;

            // tangent vector
            double norm1 = sqrt(t1[0]*t1[0] + t1[1]*t1[1] + t1[2]*t1[2]);
            double norm2 = sqrt(t2[0]*t2[0] + t2[1]*t2[1] + t2[2]*t2[2]);
            for (int idim = 0; idim < physical_dimension; ++idim) {
                gauss_point_tangent1_list[ipt*physical_dimension+idim] = t1[idim]/norm1;
                gauss_point_tangent2_list[ipt*physical_dimension+idim] = t2[idim]/norm2;
            }
        }
        break;

    }
}

void igFace::initializeNormal(vector<double> *left_center)
{
    IG_ASSERT(left_center, "no left_center");

    this->computeNormal();

    // check direction L -> R
    vector<double> face_center(physical_dimension, 0.);
    vector<double> face_normal(physical_dimension, 0.);

    // compute vector element_left -> face
    for (int idim = 0; idim < physical_dimension; ++idim) {
        for (int idof=0; idof<this->controlPointNumber(); idof++) {
            face_center[idim] += controlPointCoordinate(idof, idim);
        }
        face_center[idim] /= this->controlPointNumber();
    }

    // compute mean normal vector
    for (int idim = 0; idim < physical_dimension; ++idim){
        for (int ipt = 0; ipt < gauss_point_number; ++ipt)
            face_normal[idim] += gauss_point_normal_list[ipt*physical_dimension+idim];
        face_normal[idim] /= gauss_point_number;
    }

    // check scalar product
    double product = 0;
    for (int idim = 0; idim < physical_dimension; ++idim) {
        product += face_normal[idim] * (face_center[idim] - left_center->at(idim));
    }
    if (product < 0.) {
        for (int ipt = 0; ipt < gauss_point_number; ++ipt) {
            for (int idim = 0; idim < physical_dimension; ++idim) {
                gauss_point_normal_list[ipt*physical_dimension+idim] *= -1.;
                gauss_point_tangent1_list[ipt*physical_dimension+idim] *= -1.;
                gauss_point_tangent2_list[ipt*physical_dimension+idim] *= -1.;
            }
        }
    }
}

void igFace::initializeNormalFromRight(vector<double> *right_center)
{
    IG_ASSERT(right_center, "no left_center");

    this->computeNormal();

    // check direction L -> R
    vector<double> face_center(physical_dimension, 0.);
    vector<double> face_normal(physical_dimension, 0.);

    // compute vector face -> element right
    for (int idim = 0; idim < physical_dimension; ++idim) {
        for (int idof=0; idof<this->controlPointNumber(); idof++) {
            face_center[idim] += controlPointCoordinate(idof, idim);
        }
        face_center[idim] /= this->controlPointNumber();
    }

    // compute mean normal vector
    for (int idim = 0; idim < physical_dimension; ++idim){
        for (int ipt = 0; ipt < gauss_point_number; ++ipt)
            face_normal[idim] += gauss_point_normal_list[ipt*physical_dimension+idim];
        face_normal[idim] /= gauss_point_number;
    }

    // check scalar product
    double product = 0;
    for (int idim = 0; idim < physical_dimension; ++idim) {
        product += face_normal[idim] * (right_center->at(idim) - face_center[idim]);
    }
    if (product < 0.) {
        for (int ipt = 0; ipt < gauss_point_number; ++ipt) {
            for (int idim = 0; idim < physical_dimension; ++idim) {
                gauss_point_normal_list[ipt*physical_dimension+idim] *= -1.;
                gauss_point_tangent1_list[ipt*physical_dimension+idim] *= -1.;
                gauss_point_tangent2_list[ipt*physical_dimension+idim] *= -1.;
            }
        }
    }
}


void igFace::updateNormal(void)
{
    // check direction L -> R
    vector<double> mean_normal_old(physical_dimension, 0.);
    vector<double> mean_normal_new(physical_dimension, 0.);

    // compute old mean normal vector
    for (int idim = 0; idim < physical_dimension; ++idim){
        for (int ipt = 0; ipt < gauss_point_number; ++ipt)
            mean_normal_old[idim] += gauss_point_normal_list[ipt*physical_dimension+idim];
        mean_normal_old[idim] /= gauss_point_number;
    }

    this->computeNormal();

    // compute new mean normal vector
    for (int idim = 0; idim < physical_dimension; ++idim){
        for (int ipt = 0; ipt < gauss_point_number; ++ipt)
            mean_normal_new[idim] += gauss_point_normal_list[ipt*physical_dimension+idim];
        mean_normal_new[idim] /= gauss_point_number;
    }

    // check scalar product between old and new mean normal
    double product = 0;
    for (int idim = 0; idim < physical_dimension; ++idim) {
        product += mean_normal_old[idim] * mean_normal_new[idim];
    }
    if (product < 0.) {
        for (int ipt = 0; ipt < gauss_point_number; ++ipt) {
            for (int idim = 0; idim < physical_dimension; ++idim) {
                gauss_point_normal_list[ipt*physical_dimension+idim] *= -1.;
                gauss_point_tangent1_list[ipt*physical_dimension+idim] *= -1.;
                gauss_point_tangent2_list[ipt*physical_dimension+idim] *= -1.;
            }
        }
    }

}

//////////////////////////////////////////////////////////////////////////////


void igFace::evaluateGaussPoints(void)
{
    switch(physical_dimension){

    case 1: {

        gauss_point_value_list[0] = 1.;
        gauss_point_gradient_list[0] = 0.;
        gauss_point_value_left_list[0] = 1.;
        gauss_point_value_right_list[0] = 1.;

        break;}

    case 2: {

        igBasisBezier basis(degree);

        vector<double> values(control_point_number_by_direction);
        vector<double> grad_values(control_point_number_by_direction);

        for (int ipt = 0; ipt < gauss_point_number; ++ipt) {

            // first pass: function and grad for the face
            double var = gauss_point_coord_list[ipt];
            basis.evalFunction(var,values);
            basis.evalGradient(var,grad_values);

            double weighted_sum = 0.;
            double weighted_sum_grad = 0.;
            for (int ictl=0; ictl<control_point_number_by_direction; ictl++){
                weighted_sum += values[ictl] * controlPointWeight(ictl);
                weighted_sum_grad += grad_values[ictl] * controlPointWeight(ictl);
            }

            for (int ictl = 0; ictl < control_point_number; ++ictl) {
                gauss_point_value_list[ipt*control_point_number+ictl] = values[ictl]*controlPointWeight(ictl)/weighted_sum;
            }
            for (int ictl = 0; ictl < control_point_number; ++ictl) {
                gauss_point_gradient_list[ipt*control_point_number+ictl] = ( grad_values[ictl] * weighted_sum - values[ictl] * weighted_sum_grad )
                    / (weighted_sum*weighted_sum) * controlPointWeight(ictl);
            }


            // second pass: function and grad for the left element
            var = param_min_left1 + gauss_point_coord_list[ipt]*(param_max_left1-param_min_left1);

            basis.evalFunction(var,values);

            weighted_sum = 0.;
            for (int ictl = 0; ictl < control_point_number_by_direction; ++ictl) {
                weighted_sum += values[ictl] * coordinates->at(dofLeft(ictl,physical_dimension));
            }

            for (int ictl = 0; ictl < control_point_number; ++ictl) {
                gauss_point_value_left_list[ipt*control_point_number+ictl] = values[ictl] * coordinates->at(dofLeft(ictl,physical_dimension)) / weighted_sum;
            }


            // third pass: function and grad for the right element, interior case
            if(this->face_type == id_interior){
                var = param_min_right1 + gauss_point_coord_list[ipt] * (param_max_right1-param_min_right1);

                basis.evalFunction(var, values);

                weighted_sum = 0.;
                // weighted_sum_grad = 0.;
                for (int ictl = 0; ictl < control_point_number_by_direction; ++ictl) {
                    weighted_sum += values[ictl] * coordinates->at(dofRight(ictl,physical_dimension));
                }

                for (int ictl = 0; ictl < control_point_number; ++ictl) {
                    gauss_point_value_right_list[ipt*control_point_number+ictl] = values[ictl] * coordinates->at(dofRight(ictl,physical_dimension)) / weighted_sum;
                }
            } else { // boundary case
                for (int ictl = 0; ictl < control_point_number; ++ictl) {
                    gauss_point_value_right_list[ipt*control_point_number+ictl] = gauss_point_value_left_list[ipt*control_point_number+ictl];
                }
            }
        }
        break;}

    case 3: {

        igBasisBezier basis1(degree);
        igBasisBezier basis2(degree);

        vector<double> values1(control_point_number_by_direction);
        vector<double> values2(control_point_number_by_direction);
        vector<double> grad_values1(control_point_number_by_direction);
        vector<double> grad_values2(control_point_number_by_direction);

        int index_fun = 0;
        int index_grad = 0;
        int index_fun_left = 0;
        int index_fun_right = 0;

        for (int ipt = 0; ipt < gauss_point_number; ++ipt) {

            // first pass: function and grad for the face
            double var1 = gaussPointParametricCoordinate(ipt, 0);
            basis1.evalFunction(var1,values1);
            basis1.evalGradient(var1,grad_values1);
            double var2 = gaussPointParametricCoordinate(ipt, 1);
            basis2.evalFunction(var2,values2);
            basis2.evalGradient(var2,grad_values2);

            double weighted_sum = 0.;
            double weighted_sum_grad1 = 0.;
            double weighted_sum_grad2 = 0.;
            int index_w = 0;
            for (int ictl2=0; ictl2<control_point_number_by_direction; ictl2++){
                for (int ictl1=0; ictl1<control_point_number_by_direction; ictl1++){
                    weighted_sum += values1[ictl1] * values2[ictl2] * controlPointWeight(index_w);
                    weighted_sum_grad1 += grad_values1[ictl1] * values2[ictl2] * controlPointWeight(index_w);
                    weighted_sum_grad2 += values1[ictl1] * grad_values2[ictl2] * controlPointWeight(index_w);
                    index_w++;
                }
            }

            index_w = 0;
            for (int ictl2=0; ictl2<control_point_number_by_direction; ictl2++){
                for (int ictl1=0; ictl1<control_point_number_by_direction; ictl1++){

                    gauss_point_value_list[index_fun] = values1[ictl1]*values2[ictl2]*controlPointWeight(index_w)/weighted_sum;

                    gauss_point_gradient_list[index_grad] = ( grad_values1[ictl1]*values2[ictl2]*weighted_sum -  values1[ictl1]*values2[ictl2]*weighted_sum_grad1 )
                        / (weighted_sum*weighted_sum) * controlPointWeight(index_w);

                    gauss_point_gradient_list[index_grad+1] = ( values1[ictl1]*grad_values2[ictl2]*weighted_sum - values1[ictl1]*values2[ictl2]*weighted_sum_grad2 )
                        / (weighted_sum*weighted_sum) * controlPointWeight(index_w);

                    ++index_w;
                    ++index_fun;
                    index_grad +=2;
                }
            }


            // second pass: function and grad for the left element
            var1 = param_min_left1 + gaussPointParametricCoordinate(ipt, 0)*(param_max_left1-param_min_left1);
            basis1.evalFunction(var1,values1);
            var2 = param_min_left2 + gaussPointParametricCoordinate(ipt, 1)*(param_max_left2-param_min_left2);
            basis2.evalFunction(var2,values2);

            weighted_sum = 0.;
            index_w = 0;
            for (int ictl2=0; ictl2<control_point_number_by_direction; ictl2++){
                for (int ictl1=0; ictl1<control_point_number_by_direction; ictl1++){
                    weighted_sum += values1[ictl1] * values2[ictl2] * coordinates->at(dofLeft(index_w,physical_dimension));
                    index_w++;
                }
            }

            index_w = 0;
            for (int ictl2=0; ictl2<control_point_number_by_direction; ictl2++){
                for (int ictl1=0; ictl1<control_point_number_by_direction; ictl1++){

                    gauss_point_value_left_list[index_fun_left] = values1[ictl1]*values2[ictl2]*coordinates->at(dofLeft(index_w,physical_dimension))/weighted_sum;

                    ++index_w;
                    ++index_fun_left;
                }
            }

            // third pass: function and grad for the right element, interior case
            if(this->face_type == id_interior){
                var1 = param_min_right1 + gaussPointParametricCoordinate(ipt, 0)*(param_max_right1-param_min_right1);
                basis1.evalFunction(var1,values1);
                var2 = param_min_right2 + gaussPointParametricCoordinate(ipt, 1)*(param_max_right2-param_min_right2);
                basis2.evalFunction(var2,values2);

                weighted_sum = 0.;
                index_w = 0;
                for (int ictl2=0; ictl2<control_point_number_by_direction; ictl2++){
                    for (int ictl1=0; ictl1<control_point_number_by_direction; ictl1++){
                        weighted_sum += values1[ictl1] * values2[ictl2] * coordinates->at(dofRight(index_w,physical_dimension));
                        index_w++;
                    }
                }

                index_w = 0;
                for (int ictl2=0; ictl2<control_point_number_by_direction; ictl2++){
                    for (int ictl1=0; ictl1<control_point_number_by_direction; ictl1++){

                        gauss_point_value_right_list[index_fun_right] = values1[ictl1]*values2[ictl2]*coordinates->at(dofRight(index_w,physical_dimension))/weighted_sum;

                        ++index_w;
                        ++index_fun_right;
                    }
                }
            }
        }
        break;}

    }

    return;
}


void igFace::evaluateSlidingGaussPoints(void)
{

	igBasisBezier basis(degree);

	double curve_x[2];
	double curve_y[2];

	vector<double> values(control_point_number_by_direction);
	vector<double> grad_values(control_point_number_by_direction);

	vector<double> x_left(control_point_number_by_direction);
	vector<double> y_left(control_point_number_by_direction);
	vector<double> w_left(control_point_number_by_direction);

	vector<double> x_right(control_point_number_by_direction);
	vector<double> y_right(control_point_number_by_direction);
	vector<double> w_right(control_point_number_by_direction);

	// retrieve coordinates from left and right elements
	for(int ictl = 0; ictl<control_point_number; ictl++){
		x_left.at(ictl) = coordinates->at(dofLeft(ictl,0));
		y_left.at(ictl) = coordinates->at(dofLeft(ictl,1));
		w_left.at(ictl) = coordinates->at(dofLeft(ictl,physical_dimension));

		x_right.at(ictl) = coordinates->at(dofRight(ictl,0));
		y_right.at(ictl) = coordinates->at(dofRight(ictl,1));
		w_right.at(ictl) = coordinates->at(dofRight(ictl,physical_dimension));
	}

	for (int ipt = 0; ipt < gauss_point_number; ++ipt) {

		// first pass: function and grad for the face
		double var = gauss_point_coord_list[ipt];
		basis.evalFunction(var,values);
		basis.evalGradient(var,grad_values);

		double weighted_sum = 0.;
		double weighted_sum_grad = 0.;
		for (int ictl=0; ictl<control_point_number_by_direction; ictl++){
			weighted_sum += values[ictl] * controlPointWeight(ictl);
			weighted_sum_grad += grad_values[ictl] * controlPointWeight(ictl);
		}

		for (int ictl = 0; ictl < control_point_number; ++ictl) {
			gauss_point_value_list[ipt*control_point_number+ictl] = values[ictl]*controlPointWeight(ictl)/weighted_sum;
		}
		for (int ictl = 0; ictl < control_point_number; ++ictl) {
			gauss_point_gradient_list[ipt*control_point_number+ictl] = ( grad_values[ictl] * weighted_sum - values[ictl] * weighted_sum_grad )
    	                    		/ (weighted_sum*weighted_sum) * controlPointWeight(ictl);
		}

		// second pass: function and grad for the left element
		double var_left = param_min_left1 + gauss_point_coord_list[ipt]*(param_max_left1-param_min_left1);

		basis.evalFunction(var_left,values);
		// basis.evalGradient(var_left,grad_values);

		weighted_sum = 0.;
		// weighted_sum_grad = 0.;
		for (int ictl = 0; ictl < control_point_number_by_direction; ++ictl) {
			weighted_sum += values[ictl] * coordinates->at(dofLeft(ictl,physical_dimension));
			// weighted_sum_grad += grad_values[ictl] * coordinates->at(dofLeft(ictl,physical_dimension));
		}

		for (int ictl = 0; ictl < control_point_number; ++ictl) {
			gauss_point_value_left_list[ipt*control_point_number+ictl] = values[ictl] * coordinates->at(dofLeft(ictl,physical_dimension)) / weighted_sum;
		}

		// for (int ictl = 0; ictl < control_point_number; ++ictl) {
		// 	gauss_point_gradient_left_list[ipt*control_point_number+ictl] = ( grad_values[ictl] * weighted_sum - values[ictl] * weighted_sum_grad )
    	//                                 		/ (weighted_sum*weighted_sum) * coordinates->at(dofLeft(ictl,physical_dimension));
		// }

		// third pass: function and grad for the right element
		double var_right = param_min_right1 + gauss_point_coord_list[ipt] * (param_max_right1-param_min_right1);

		// use left coordinates to find target point
		basis.evalCurve(var_left,x_left,w_left,curve_x);
		basis.evalCurve(var_left,y_left,w_left,curve_y);
		double x_target = curve_x[0];
		double y_target = curve_y[0];

		// point inversion algorithm
		var_right = basis.pointInversion(x_right,y_right,w_right,x_target,y_target,var_left,var_right);

		// compute basis functions and gradients
		basis.evalFunction(var_right, values);
		// basis.evalGradient(var_right, grad_values);

		weighted_sum = 0.;
		// weighted_sum_grad = 0.;
		for (int ictl = 0; ictl < control_point_number_by_direction; ++ictl) {
			weighted_sum += values[ictl] * coordinates->at(dofRight(ictl,physical_dimension));
			// weighted_sum_grad += grad_values[ictl] * coordinates->at(dofRight(ictl,physical_dimension));
		}

		for (int ictl = 0; ictl < control_point_number; ++ictl) {
			gauss_point_value_right_list[ipt*control_point_number+ictl] = values[ictl] * coordinates->at(dofRight(ictl,physical_dimension)) / weighted_sum;
		}
		// for (int ictl = 0; ictl < control_point_number; ++ictl) {
		// 	gauss_point_gradient_right_list[ipt*control_point_number+ictl] = ( grad_values[ictl]*weighted_sum - values[ictl]*weighted_sum_grad )
		//                         		/ (weighted_sum*weighted_sum) * coordinates->at(dofRight(ictl,physical_dimension));
		// }

	}

}

////////////////////////////////////////////////////////////////////////////////////

void igFace::initializeInterfaceProperties(igFace *face_parent)
{

    for (int k=0; k<gauss_point_number; k++){

        // retrieve interface data from parent face
        double weight_parent = face_parent->interfaceQuadratureWeight(k);
        double par_parent = face_parent->interfaceQuadratureCoordinate(k);
        int label_parent = face_parent->interfaceQuadratureLabel(k);

        // compute new interface coordinates and weights
        double weight_new = 0.5 * weight_parent;

        double interval_parent =  weight_parent / gauss_point_weight_list[k];
        double origin = par_parent - gauss_point_coord_list[k] * interval_parent;
        double par_new = origin + gauss_point_coord_list[k] * 0.5*interval_parent;
        if(this->childRank() == 2){
            par_new += 0.5*interval_parent;
        }

        // set data into new faces
        this->setInterfaceQuadratureCoordinate(k,par_new);
        this->setInterfaceQuadratureWeight(k,weight_new);
        this->setInterfaceQuadratureLabel(k,label_parent);

    }

}
