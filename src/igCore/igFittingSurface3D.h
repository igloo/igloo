/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include "igFitting.h"

#include <igCoreExport>

#include <functional>
#include <vector>

using namespace std;

class IGCORE_EXPORT igFittingSurface3D : public igFitting
{
public:
     igFittingSurface3D(void);
    ~igFittingSurface3D(void);

public:
    void setParameterBounds(double p_min1, double p_max1,double p_min2, double p_max2);
    void setCoordinateBounds(vector<double> *edge_x, vector<double> *edge_y, vector<double> *edge_z);
    void setParameterFixed(double p_fixed);
    void selectParameterFixed(int index);
    void setXFunction(function<double(double, double, double)> x_fun);
    void setYFunction(function<double(double, double, double)> y_fun);
    void setZFunction(function<double(double, double, double)> z_fun);

public:
    void run(void);

public:
    double xControlPointFit(int index);
    double yControlPointFit(int index);
    double zControlPointFit(int index);

private:
    int fixed_parameter;

    double p_min1;
    double p_max1;
    double p_min2;
    double p_max2;
    double p_fixed;

    vector<double> sol_x;
    vector<double> sol_y;
    vector<double> sol_z;

    vector<double> *edge_x;
    vector<double> *edge_y;
    vector<double> *edge_z;

    function<double(double, double, double)> x_fun;
    function<double(double, double, double)> y_fun;
    function<double(double, double, double)> z_fun;
};

//
// igFittingSurface3D.h ends here
