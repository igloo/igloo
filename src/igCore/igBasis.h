/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igCoreExport.h>

#include <vector>

using namespace std;

class IGCORE_EXPORT igBasis
{
public:
             igBasis(int degree_value);
    virtual ~igBasis(void) = default;

public:
    virtual int functionNumber() = 0;
    virtual void evalFunction(double var, vector<double> &values) = 0;
    virtual void evalGradient(double var, vector<double> &values) = 0;

protected:
    int function_number;
    int degree;
};

//
// igBasis.h ends here
