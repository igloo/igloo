/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igCell.h"

#include "igAssert.h"

#include <iostream>
#include <math.h>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igCell::igCell(void)
{
    this->active = true;
    this->has_child = false;
    this->child_rank = 0;
    this->cell_parent = -1;

    this->coordinates = nullptr;
    this->velocities = nullptr;
}

/////////////////////////////////////////////////////////////////////////////


void igCell::setCellId(int id_value)
{
    id = id_value;
}

void igCell::setCellGlobalId(int id_value)
{
    global_id = id_value;
}

void igCell::setDegree(int degree_value)
{
    degree = degree_value;
}

void igCell::setPhysicalDimension(int dimension_value)
{
    physical_dimension = dimension_value;
}

void igCell::setParametricDimension(int dimension_value)
{
    parametric_dimension = dimension_value;
}

void igCell::setVariableNumber(int number)
{
    variable_number = number;
}

void igCell::setDerivativeNumber(int number)
{
    derivative_number = number;
}

void igCell::setGaussPointNumberByDirection(int number)
{
    gauss_point_number_by_direction = number;
}

void igCell::setCoordinates(vector<double> *coord)
{
    this->coordinates = coord;
}

void igCell::setVelocities(vector<double> *vel)
{
    this->velocities = vel;
}

void igCell::setActive(void)
{
    this->active = true;
}

void igCell::setInactive(void)
{
    this->active = false;
}

void igCell::setChild(int id, int children_index)
{
    if(!this->has_child){
        has_child = true;
        child_list.resize(4);
    }
    child_list.at(id) = children_index;
}

void igCell::setChildRank(int rank)
{
    child_rank = rank;
}

void igCell::setParent(int id)
{
    this->cell_parent = id;
}

/////////////////////////////////////////////////////////////////////////////

int igCell::cellId(void) const
{
    return id;
}

int igCell::cellGlobalId(void) const
{
    return global_id;
}

int igCell::physicalDimension(void) const
{
    return physical_dimension;
}

int igCell::parametricDimension(void) const
{
    return physical_dimension;
}

int igCell::cellDegree(void) const
{
    return degree;
}

double igCell::area(void) const
{
    return cell_area;
}

int igCell::controlPointNumber(void) const
{
    return control_point_number;
}

int igCell::controlPointNumberByDirection(void) const
{
    return control_point_number_by_direction;
}

int igCell::gaussPointNumber(void) const
{
    return gauss_point_number;
}

int igCell::gaussPointNumberByDirection(void) const
{
    return gauss_point_number_by_direction;
}

double *igCell::gaussPointPhysicalCoordinate(int index)
{
    return &gauss_point_physical_coord_list[index * physical_dimension];
}

double *igCell::gaussPointParametricCoordinate(int index)
{
    return &gauss_point_coord_list[index*parametric_dimension];
}

double igCell::gaussPointPhysicalCoordinate(int index,int component) const
{
    return gauss_point_physical_coord_list[index*physical_dimension+component];
}

double igCell::gaussPointParametricCoordinate(int pt_index,int component) const
{
    return gauss_point_coord_list[pt_index*parametric_dimension+component];
}

double igCell::gaussPointWeight(int pt_index) const
{
    return gauss_point_weight_list[pt_index];
}

double igCell::gaussPointJacobian(int pt_index) const
{
    return gauss_point_jacobian_list[pt_index];
}

double igCell::gaussPointJacobianMatrix(int pt_index,int icomponent,int jcomponent) const
{
    return gauss_point_jacobian_matrix_list[pt_index*physical_dimension*parametric_dimension+icomponent*parametric_dimension+jcomponent];
}

double igCell::gaussPointFunctionValue(int pt_index,int fun_index) const
{
    return gauss_point_value_list[pt_index*control_point_number+fun_index];
}

double *igCell::gaussPointFunctionPtr(int pt_index)
{
    return &gauss_point_value_list[pt_index*control_point_number];
}

double igCell::gaussPointGradientValue(int pt_index,int fun_index,int component) const
{
    return gauss_point_gradient_list[(pt_index*control_point_number+fun_index)*parametric_dimension+component];
}

double *igCell::gaussPointGradientPtr(int pt_index,int fun_index)
{
    return &gauss_point_gradient_list[(pt_index*control_point_number+fun_index)*parametric_dimension];
}

double *igCell::gaussPointGradientPtr(int pt_index)
{
    return &gauss_point_gradient_list[pt_index*control_point_number*parametric_dimension];
}

void igCell::gaussPointVelocity(int pt_index, vector<double> *mesh_vel)
{
       for(int idim=0; idim<physical_dimension; idim++)
               mesh_vel->at(idim) = gauss_point_velocity_list[pt_index*physical_dimension+idim];
}


bool igCell::isActive(void)
{
    return this->active;
}

bool igCell::hasChild(void)
{
    return has_child;
}

int igCell::child(int id)
{
    return child_list[id];
}

int igCell::childRank(void)
{
    return child_rank;
}

int igCell::parent(void)
{
    return this->cell_parent;
}

////////////////////////////////////////////////////////

void igCell::computeGaussPointVelocity(void)
{
	//Loop over gauss points
	for(int k=0; k<gauss_point_number; k++){

		vector<double> mesh_vel(physical_dimension,0.);

		//Loop over basis functions
		for(int ifct=0; ifct<control_point_number; ifct++){

			for(int idim=0; idim<physical_dimension; idim++)
				mesh_vel.at(idim) += controlPointVelocityComponent(ifct,idim)*gaussPointFunctionValue(k,ifct);

		}

		for(int idim=0; idim<physical_dimension; idim++)
			gauss_point_velocity_list[k*physical_dimension+idim] = mesh_vel.at(idim);

	}
}
