/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igRungeKuttaSSP.h"

#include <iostream>
#include <igAssert.h>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igRungeKuttaSSP::igRungeKuttaSSP() : igRungeKutta()
{
	current_solution = new vector<double>;
}

igRungeKuttaSSP::~igRungeKuttaSSP(void)
{
	delete current_solution;

}

/////////////////////////////////////////////////////////////////////////////

void igRungeKuttaSSP::initialize(vector<double> *initial_solution, double initial_time)
{
	IG_ASSERT(initial_solution, "no initial_solution");
	solution = initial_solution;

	step_number = 3;
	current_step = 1;

	current_time = initial_time;

	int size = solution->size();

	increment1.resize(size);
	increment2.resize(size);
	increment3.resize(size);

	current_solution->resize(size);

	return;
}

void igRungeKuttaSSP::setIncrement(vector<double> *increment)
{
	IG_ASSERT(increment, "no increment");
	int size = increment->size();

	switch (current_step) {

	case 1:
		for (int i=0; i<size; i++){
			increment1[i] = increment->at(i);
		}
		break;

	case 2:
		for (int i=0; i<size; i++){
			increment2[i] = increment->at(i);
		}
		break;

	case 3:
		for (int i=0; i<size; i++){
			increment3[i] = increment->at(i);
		}
		break;

	}

	current_step++;

	return;
}


vector<double>* igRungeKuttaSSP::nextEvaluation(void)
{
	switch (current_step) {

	case 1:
		return solution;
		break;

	case 2:
		for (int i=0; i<current_solution->size(); i++){
			current_solution->at(i) = solution->at(i) + current_time_step * increment1.at(i);
		}
		return current_solution;
		break;

	case 3:
		for (int i=0; i<current_solution->size(); i++){
			current_solution->at(i) = solution->at(i) + 0.25 * current_time_step * (increment1.at(i)+increment2.at(i));
		}
		return current_solution;
		break;

	}

	return solution;
}

double igRungeKuttaSSP::nextTime(void)
{
	switch (current_step) {

	case 1:
		return current_time;
		break;

	case 2:
		return current_time + current_time_step;
		break;

	case 3:
		return current_time + 0.5* current_time_step;
		break;

	}
	return current_time;
}

void igRungeKuttaSSP::updateSolution(void)
{

	for (int i=0; i<solution->size(); i++){

		solution->at(i) += current_time_step/3. * ( 0.5*increment1.at(i) + 0.5*increment2.at(i)
				+ 2.*increment3.at(i) );
	}

	current_time += current_time_step;
	current_step = 1;

	return;
}
