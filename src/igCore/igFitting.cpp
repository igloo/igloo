/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/

#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>

#include "igFitting.h"


using namespace std;

/////////////////////////////////////////////////////////////////////////////

igFitting::igFitting()
{

}

/////////////////////////////////////////////////////////////////////////////

void igFitting::setDegree(int degree)
{
    this->degree = degree;
}

void igFitting::setSampleNumber(int number)
{
    this->sample_number = number;
}

void igFitting::setElement(igElement *elt)
{
    this->element = elt;
}

///////////////////////////////////////////////////////////////

void igFitting::inverse_system(double* A, int N)
{
    lin_solver.inverse(A,N);
}
