/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igCoreExport.h>

#include <vector>

using namespace std;

class IGCORE_EXPORT igRungeKutta
{
public:
             igRungeKutta(void);
    virtual ~igRungeKutta(void);

public:
    void setTimeStep(double time_step);
    int stepNumber(void);

    virtual void initialize(vector<double> *initial_solution, double initial_time) = 0;
    virtual vector<double>* nextEvaluation(void) = 0;
    virtual double nextTime(void) = 0;
    virtual void setIncrement(vector<double> *increment) = 0;
    virtual void updateSolution(void) = 0;

protected:
    int step_number;
    int current_step;

    double current_time_step;
    double current_time;

    vector<double> *solution;
    vector<double> *current_solution;
};

//
// igRungeKutta.h ends here
