/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/

#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>

#include "igFittingRefinedSurface.h"

#include "igBasisBezier.h"
#include "igElement.h"

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igFittingRefinedSurface::igFittingRefinedSurface()
{

}

/////////////////////////////////////////////////////////////////////////////

void igFittingRefinedSurface::setElements(igElement *elt_parent, igElement *elt0, igElement *elt1, igElement *elt2, igElement *elt3)
{
    this->elt_parent = elt_parent;
    this->elt0 = elt0;
    this->elt1 = elt1;
    this->elt2 = elt2;
    this->elt3 = elt3;
}

void igFittingRefinedSurface::setSolution(vector<double> *state, int variable_number)
{
    this->state = state;
    this->variable_number = variable_number;
}

/////////////////////////////////////////////////////////////////////////////

void igFittingRefinedSurface::run(void)
{
    
    int degree = elt_parent->cellDegree();
    igBasis *basis = new igBasisBezier(degree);

    vector<double> values0_elt0;
    values0_elt0.assign(degree+1,0.);
    vector<double> values1_elt0;
    values1_elt0.assign(degree+1,0.);
    vector<double> values0_elt1;
    values0_elt1.assign(degree+1,0.);
    vector<double> values1_elt1;
    values1_elt1.assign(degree+1,0.);
    vector<double> values0_elt2;
    values0_elt2.assign(degree+1,0.);
    vector<double> values1_elt2;
    values1_elt2.assign(degree+1,0.);
    vector<double> values0_elt3;
    values0_elt3.assign(degree+1,0.);
    vector<double> values1_elt3;
    values1_elt3.assign(degree+1,0.);

    int dof_number = (degree+1)*(degree+1);
    int size = (degree+1)*(degree+1);
    int gauss_pt_number = elt_parent->gaussPointNumber();
        
    double *rhs = new double[size];
    double *mat = new double[size*size];

    for(int ivar=0; ivar< variable_number; ivar++){

        // compute mean element value
        double var_int0 = this->computeIntegralElementState(elt0, ivar);
        double var_int1 = this->computeIntegralElementState(elt1, ivar);
        double var_int2 = this->computeIntegralElementState(elt2, ivar);
        double var_int3 = this->computeIntegralElementState(elt3, ivar);

        double var_mean  = (var_int0 + var_int1 + var_int2 + var_int3)/elt_parent->area();

        // least-squares approximation preserving the mean


        // initialize matrix and rhs, sol
        for (int i=0; i<size; i++){
            rhs[i] = 0.;
            for (int j=0; j<size; j++)
                mat[i*size+j] = 0.;
        }

        // compute matrix and rhs for least-squares fitting

        // loop for the two children
        for (int k=0; k<gauss_pt_number; k++){

            double coord0_ref = elt_parent->gaussPointParametricCoordinate(k,0);
            double coord1_ref = elt_parent->gaussPointParametricCoordinate(k,1);
            
            double coord0_elt0 = coord0_ref*0.5;
            double coord0_elt1 = 0.5 + coord0_ref*0.5;
            double coord0_elt2 = coord0_elt0;
            double coord0_elt3 = coord0_elt1;

            double coord1_elt0 = coord1_ref*0.5;
            double coord1_elt1 = coord1_elt0;
            double coord1_elt2 = 0.5 + coord1_ref*0.5;
            double coord1_elt3 = coord1_elt2;

            basis->evalFunction(coord0_elt0,values0_elt0);
            basis->evalFunction(coord1_elt0,values1_elt0);
            basis->evalFunction(coord0_elt1,values0_elt1);
            basis->evalFunction(coord1_elt1,values1_elt1);
            basis->evalFunction(coord0_elt2,values0_elt2);
            basis->evalFunction(coord1_elt2,values1_elt2);
            basis->evalFunction(coord0_elt3,values0_elt3);
            basis->evalFunction(coord1_elt3,values1_elt3);

            double weighted_sum_elt0 = 0.;
            double weighted_sum_elt1 = 0.;
            double weighted_sum_elt2 = 0.;
            double weighted_sum_elt3 = 0.;
            
            for (int ifun1=0; ifun1<degree+1; ifun1++){
                for (int ifun0=0; ifun0<degree+1; ifun0++){
                    int index = ifun0+(degree+1)*ifun1;
                    weighted_sum_elt0 += values0_elt0.at(ifun0)*values1_elt0.at(ifun1)*elt_parent->controlPointWeight(index);
                    weighted_sum_elt1 += values0_elt1.at(ifun0)*values1_elt1.at(ifun1)*elt_parent->controlPointWeight(index);
                    weighted_sum_elt2 += values0_elt2.at(ifun0)*values1_elt2.at(ifun1)*elt_parent->controlPointWeight(index);
                    weighted_sum_elt3 += values0_elt3.at(ifun0)*values1_elt3.at(ifun1)*elt_parent->controlPointWeight(index);
                }
            }

            double function_value_elt0 = this->solutionAtGaussPoint(elt0, k, ivar);
            double function_value_elt1 = this->solutionAtGaussPoint(elt1, k, ivar);
            double function_value_elt2 = this->solutionAtGaussPoint(elt2, k, ivar);
            double function_value_elt3 = this->solutionAtGaussPoint(elt3, k, ivar);

            for (int i1=0; i1<degree+1; i1++){
                for (int i0=0; i0<degree+1; i0++){

                    int index1 =  i0+(degree+1)*i1;
                    rhs[i0+(degree+1)*i1] += values0_elt0.at(i0)*values1_elt0.at(i1)*elt_parent->controlPointWeight(index1)/weighted_sum_elt0 * function_value_elt0;
                    rhs[i0+(degree+1)*i1] += values0_elt1.at(i0)*values1_elt1.at(i1)*elt_parent->controlPointWeight(index1)/weighted_sum_elt1 * function_value_elt1;
                    rhs[i0+(degree+1)*i1] += values0_elt2.at(i0)*values1_elt2.at(i1)*elt_parent->controlPointWeight(index1)/weighted_sum_elt2 * function_value_elt2;
                    rhs[i0+(degree+1)*i1] += values0_elt3.at(i0)*values1_elt3.at(i1)*elt_parent->controlPointWeight(index1)/weighted_sum_elt3 * function_value_elt3;
                    
                    for (int j1=0; j1<degree+1; j1++){
                        for (int j0=0; j0<degree+1; j0++){
                            int index2 = j0+(degree+1)*j1;
                            mat[(i0+(degree+1)*i1)*size+(j0+(degree+1)*j1)] +=
                                values0_elt0.at(i0)*values1_elt0.at(i1)*elt_parent->controlPointWeight(index1)/weighted_sum_elt0 *
                                values0_elt0.at(j0)*values1_elt0.at(j1)*elt_parent->controlPointWeight(index2)/weighted_sum_elt0;
                            mat[(i0+(degree+1)*i1)*size+(j0+(degree+1)*j1)] +=
                                values0_elt1.at(i0)*values1_elt1.at(i1)*elt_parent->controlPointWeight(index1)/weighted_sum_elt1 *
                                values0_elt1.at(j0)*values1_elt1.at(j1)*elt_parent->controlPointWeight(index2)/weighted_sum_elt1;
                            mat[(i0+(degree+1)*i1)*size+(j0+(degree+1)*j1)] +=
                                values0_elt2.at(i0)*values1_elt2.at(i1)*elt_parent->controlPointWeight(index1)/weighted_sum_elt2 *
                                values0_elt2.at(j0)*values1_elt2.at(j1)*elt_parent->controlPointWeight(index2)/weighted_sum_elt2;
                            mat[(i0+(degree+1)*i1)*size+(j0+(degree+1)*j1)] +=
                                values0_elt3.at(i0)*values1_elt3.at(i1)*elt_parent->controlPointWeight(index1)/weighted_sum_elt3 *
                                values0_elt3.at(j0)*values1_elt3.at(j1)*elt_parent->controlPointWeight(index2)/weighted_sum_elt3;
                                                                          
                        }
                    }
                }
            }
        }
        // solve system
        inverse_system(mat,size);

        int *index_ptr = elt_parent->globalIdPtr(ivar);
        for (int ils=0; ils<size; ils++){
            (*state)[index_ptr[ils]] = 0;
            for (int jls=0; jls<size; jls++){
                (*state)[index_ptr[ils]] += mat[ils*size+jls]*rhs[jls];
            }
        }

        // correction for variable conservation
        double var_mean_new = this->computeIntegralElementState(elt_parent, ivar)/elt_parent->area();

        for (int ils=0; ils<size; ils++){
            (*state)[index_ptr[ils]] += var_mean - var_mean_new;
        }
        
        
    }
    delete basis;
    delete[] mat;
    delete[] rhs;
    
}

/////////////////////////////////////////////////////

double igFittingRefinedSurface::computeIntegralElementState(igElement *element, int ivar)
{
    int eval_number = element->gaussPointNumber();
    int function_number = element->controlPointNumber();

    double mean_value = 0;
    
    // loop over quadrature points
    for (int k = 0; k < eval_number; ++k) {

        double gauss_point_weight = element->gaussPointWeight(k);
        double gauss_point_jacobian = element->gaussPointJacobian(k);
        double *gauss_point_function_values = element->gaussPointFunctionPtr(k);

        int *global_ids_var = element->globalIdPtr(ivar);

        // loop over degrees of freedom
        for (int i = 0; i < function_number; ++i) {

            mean_value += gauss_point_weight * gauss_point_jacobian * gauss_point_function_values[i] * (*state)[global_ids_var[i]];
        }
    }

    return mean_value;

}


/////////////////////////////////////////////////////////////////////////////

double igFittingRefinedSurface::solutionAtGaussPoint(igElement *element, int index_pt, int component){

    int function_number = element->controlPointNumber();
    double *gauss_point_values = element->gaussPointFunctionPtr(index_pt);

    int *global_ids = element->globalIdPtr(component);

    auto& states = *state;

    // loop over basis functions
    double value = 0.;
    for (int i = 0; i < function_number; ++i) {
        value += gauss_point_values[i] * states[global_ids[i]];
    }

    return value;
}


