/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igCoreExport.h>

#include <vector>

using namespace std;

class igMesh;
class igElement;
class igFace;

class IGCORE_EXPORT igMeshRefiner
{
public:
             igMeshRefiner(void);
    virtual ~igMeshRefiner(void);

public:
    void initialize(igMesh *mesh, vector<double> *state, vector<double> *coordinates, vector<double> *velocities);

    void setError(vector<double> *error_xi, vector<double> *error_eta);
    void setRefineCoefficient(double refine_coef);

    void select(void);
    void refine(void);

    void reparametrize(void);

private:
    void checkRefinement(void);
    void propagateRefinement(void);

    void refineElement(igElement *elt, int refine_direction);
    void refineFace(igFace *face);

    void updateFace(igFace *face, bool left);
    void updateMap(igFace *face, bool left);
    void updateHalfFace(igFace *face, bool left);

    void addFace(igElement *elt, int refine_type);

private:
    igMesh *mesh;
    vector<double> *state;
    vector<double> *coordinates;
    vector<double> *velocities;

    vector<double> *error_xi;
    vector<double> *error_eta;
    int added_elt_number;

    int refinement_step;
    double refine_coef;

    vector<int> refine_flag;
    vector<int> blocking_flag;

    vector<bool> elt_refinable_xi;
    vector<bool> elt_refinable_eta;
};

//
// igMeshRefiner.h ends here
