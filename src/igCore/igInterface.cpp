/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igInterface.h"

#include "igFace.h"
#include "igBasisBezier.h"

#include <cmath>
#include <iostream>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igInterface::igInterface(vector<double> *coord, vector<double> *vel)
{
	this->split_active = false;
	this->mid_param = 0.;
	this->split_param = 0.;
	this->coordinates = coord;
	this->velocities = vel;
}

/////////////////////////////////////////////////////////////////////////////

void igInterface::setId(int id)
{
	this->id = id;
}

void igInterface::setDimension(int dimension)
{
	this->dimension = dimension;
}

void igInterface::setMidParam(double param)
{
	this->mid_param = param;
}

void igInterface::setSplitParam(double param)
{
	this->split_param = param;
}

void igInterface::setParentFace(igFace *face)
{
	this->parent_face = face;
	this->degree = face->cellDegree();
	this->control_point_number_by_direction = face->controlPointNumberByDirection();
}

void igInterface::setChildFace(igFace *face, bool left)
{
	if(left)
		this->left_child_face = face;
	else
		this->right_child_face = face;
}

void igInterface::setCenter(double x_c, double y_c)
{
	this->x_center = x_c;
	this->y_center = y_c;
}

/////////////////////////////////////////////////////////////////////////////

void igInterface::setParentActive(void)
{
	parent_face->setActive();
	left_child_face->setInactive();
	right_child_face->setInactive();

	split_active = false;
}

void igInterface::setChildActive(void)
{
	parent_face->setInactive();
	left_child_face->setActive();
	right_child_face->setActive();

	split_active = true;
}

/////////////////////////////////////////////////////////////////////////////

int igInterface::interfaceId(void)
{
	return id;
}

double igInterface::midParam(void)
{
	return mid_param;
}

double igInterface::splitParam(void)
{
	return split_param;
}

igFace* igInterface::parentFace(void)
{
	return parent_face;
}

igFace* igInterface::childFace(bool left)
{
	if(left)
		return left_child_face;
	else
		return right_child_face;
}

/////////////////////////////////////////////////////////////////////////////

double igInterface::computeMidParam()
{
	vector<double> coord_min(this->dimension,0.), coord_max(this->dimension,0.), coord_mid(this->dimension,0.);

	for(int idim=0; idim<this->dimension; idim++){
		coord_min[idim] = coordinates->at(parent_face->dofLeft(0, idim));
		coord_max[idim] = coordinates->at(parent_face->dofLeft(parent_face->controlPointNumber()-1, idim));
		coord_mid[idim] = coordinates->at(right_child_face->dofRight(0, idim));
	}

	double theta_tot = computeDeltaTheta(coord_min,coord_max);
	double theta_mid = computeDeltaTheta(coord_min,coord_mid);

	double scalar_check = (coord_max[0] - coord_min[0])*(coord_mid[0] - coord_min[0]) +
			(coord_max[1] - coord_min[1])*(coord_mid[1] - coord_min[1]);

	if(scalar_check < 0.)
		theta_mid = -theta_mid;

	mid_param = theta_mid/theta_tot;

	return mid_param;
}


double igInterface::computeDeltaTheta(const vector<double> &coord1, const vector<double> &coord2)
{

	double scalar = (coord1[0] - x_center)*(coord2[0] - x_center) + (coord1[1] - y_center)*(coord2[1] - y_center);
	double long1 = sqrt((coord1[0] - x_center)*(coord1[0] - x_center) + (coord1[1] - y_center)*(coord1[1] - y_center));
	double long2 = sqrt((coord2[0] - x_center)*(coord2[0] - x_center) + (coord2[1] - y_center)*(coord2[1] - y_center));

	double cos = scalar/(long1*long2);

	double delta_theta;
	double tol = 1.e-10;

	if(abs(cos - 1.)<tol)
		delta_theta = 0.;
	else
		delta_theta = acos(cos);

	return delta_theta;
}


double igInterface::computeSplitParam(void)
{

	vector<double> x_parent(control_point_number_by_direction);
	vector<double> y_parent(control_point_number_by_direction);
	vector<double> w_parent(control_point_number_by_direction);

	double x_split = coordinates->at(right_child_face->dofRight(0, 0));
	double y_split = coordinates->at(right_child_face->dofRight(0, 1));

	// retrieve parent face geometry
	for(int ipt=0; ipt<control_point_number_by_direction; ipt++){
		x_parent.at(ipt) = parent_face->controlPointCoordinate(ipt,0);
		y_parent.at(ipt) = parent_face->controlPointCoordinate(ipt,1);
		w_parent.at(ipt) = parent_face->controlPointWeight(ipt);
	}

	igBasisBezier basis(degree);
	split_param = basis.pointInversion(x_parent,y_parent,w_parent,x_split,y_split,split_param, mid_param);

	return split_param;
}


/////////////////////////////////////////////////////////////////////////////

void igInterface::split(void)
{
	if(!split_active)
		setChildActive();

	vector<double> x_tmp(control_point_number_by_direction);
	vector<double> y_tmp(control_point_number_by_direction);
	vector<double> w_tmp(control_point_number_by_direction);
	vector<double> u_tmp(control_point_number_by_direction);
	vector<double> v_tmp(control_point_number_by_direction);

	vector<double> x_left(control_point_number_by_direction);
	vector<double> y_left(control_point_number_by_direction);
	vector<double> w_left(control_point_number_by_direction);
	vector<double> u_left(control_point_number_by_direction);
	vector<double> v_left(control_point_number_by_direction);

	vector<double> x_right(control_point_number_by_direction);
	vector<double> y_right(control_point_number_by_direction);
	vector<double> w_right(control_point_number_by_direction);
	vector<double> u_right(control_point_number_by_direction);
	vector<double> v_right(control_point_number_by_direction);

	// Retrieve parent face geometry
	for(int ipt=0; ipt<control_point_number_by_direction; ipt++){
		w_tmp.at(ipt) = parent_face->controlPointWeight(ipt);
		x_tmp.at(ipt) = parent_face->controlPointCoordinate(ipt,0) * w_tmp.at(ipt);
		y_tmp.at(ipt) = parent_face->controlPointCoordinate(ipt,1) * w_tmp.at(ipt);
		u_tmp.at(ipt) = parent_face->controlPointVelocityComponent(ipt,0) * w_tmp.at(ipt);
		v_tmp.at(ipt) = parent_face->controlPointVelocityComponent(ipt,1) * w_tmp.at(ipt);
	}

    // Keep extremities
    x_left.front() = x_tmp.front();
    x_right.back() = x_tmp.back();
    y_left.front() = y_tmp.front();
    y_right.back() = y_tmp.back();
    w_left.front() = w_tmp.front();
    w_right.back() = w_tmp.back();
    u_left.front() = u_tmp.front();
    u_right.back() = u_tmp.back();
    v_left.front() = v_tmp.front();
    v_right.back() = v_tmp.back();

    // Split parent face into child faces
    for(int istep=0; istep<degree; istep++){

        for (int ipt=0; ipt<degree-istep; ipt++){
            x_tmp.at(ipt) = (1 - split_param)*x_tmp.at(ipt) + split_param*x_tmp.at(ipt+1);
            y_tmp.at(ipt) = (1 - split_param)*y_tmp.at(ipt) + split_param*y_tmp.at(ipt+1);
            w_tmp.at(ipt) = (1 - split_param)*w_tmp.at(ipt) + split_param*w_tmp.at(ipt+1);
            u_tmp.at(ipt) = (1 - split_param)*u_tmp.at(ipt) + split_param*u_tmp.at(ipt+1);
            v_tmp.at(ipt) = (1 - split_param)*v_tmp.at(ipt) + split_param*v_tmp.at(ipt+1);
        }

        x_left.at(1+istep) = x_tmp.at(0);
        x_right.at(degree-1-istep) = x_tmp.at(degree-1-istep);
        y_left.at(1+istep) = y_tmp.at(0);
        y_right.at(degree-1-istep) = y_tmp.at(degree-1-istep);
        w_left.at(1+istep) = w_tmp.at(0);
        w_right.at(degree-1-istep) = w_tmp.at(degree-1-istep);
        u_left.at(1+istep) = u_tmp.at(0);
        u_right.at(degree-1-istep) = u_tmp.at(degree-1-istep);
        v_left.at(1+istep) = v_tmp.at(0);
        v_right.at(degree-1-istep) = v_tmp.at(degree-1-istep);
    }

    // Recover control points in 2D space
    for(int ipt=0; ipt<control_point_number_by_direction; ipt++){
    	x_left.at(ipt) /= w_left.at(ipt);
    	x_right.at(ipt) /= w_right.at(ipt);
    	y_left.at(ipt) /= w_left.at(ipt);
    	y_right.at(ipt) /= w_right.at(ipt);
    	u_left.at(ipt) /= w_left.at(ipt);
    	u_right.at(ipt) /= w_right.at(ipt);
    	v_left.at(ipt) /= w_left.at(ipt);
    	v_right.at(ipt) /= w_right.at(ipt);
    }

    // distribute split field to faces
    for(int ipt=0; ipt<control_point_number_by_direction; ipt++){

    	left_child_face->setLocalCoordinate(ipt,0,x_left.at(ipt));
    	left_child_face->setLocalCoordinate(ipt,1,y_left.at(ipt));
    	left_child_face->setLocalWeight(ipt,w_left.at(ipt));
    	left_child_face->setLocalVelocity(ipt,0,u_left.at(ipt));
    	left_child_face->setLocalVelocity(ipt,1,v_left.at(ipt));

    	right_child_face->setLocalCoordinate(ipt,0,x_right.at(ipt));
    	right_child_face->setLocalCoordinate(ipt,1,y_right.at(ipt));
    	right_child_face->setLocalWeight(ipt,w_right.at(ipt));
    	right_child_face->setLocalVelocity(ipt,0,u_right.at(ipt));
    	right_child_face->setLocalVelocity(ipt,1,v_right.at(ipt));

    }

}


void igInterface::updateGeometry(void)
{

	// change parametric interval of child faces
	left_child_face->setParameterIntervalLeft(0.,split_param);
	left_child_face->setParameterIntervalRight(1.-split_param,1.);

	right_child_face->setParameterIntervalLeft(split_param,1.);
	right_child_face->setParameterIntervalRight(0.,1.-split_param);

	// recompute gauss-points, normals and velocities
	left_child_face->initializeGeometry();
	left_child_face->updateNormal();
	left_child_face->computeGaussPointVelocity();

	right_child_face->initializeGeometry();
	right_child_face->updateNormal();
	right_child_face->computeGaussPointVelocity();

}

