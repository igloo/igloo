/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igCoreExport.h>

#include <vector>

using namespace std;

class igMesh;
class igElement;
class igFace;
class igCommunicator;

class IGCORE_EXPORT igMeshCoarsenerIsotropic
{
public:
             igMeshCoarsenerIsotropic(void);
    virtual ~igMeshCoarsenerIsotropic(void);

public:
    void initialize(igMesh *mesh, vector<double> *state, vector<double> *coordinates, vector<double> *velocities);

    void setCommunicator(igCommunicator *communicator);

    void setError(vector<double> *error_xi, vector<double> *error_eta);
    void setCoarseningCoefficient(double coef);

    void select(void);
    void coarsen(void);

private:
    void coarsenElement(igElement *elt, vector<double> *state);
    void checkCoarsening(void);
    void computeSharedDataNumber(void);

    bool checkBrotherhood(igElement *left_elt, igElement *right_elt);
    void coarsenMidFace(igFace *face);
    void coarsenFace(igFace *face);

    void updateFace(igFace *face, int elt_active_id, bool left);
    void updateHalfFace(igFace *face, bool left);
    void updateFaceMap(igFace *face, bool left);

private:
    igMesh *mesh;
    vector<double> *state;
    vector<double> *coordinates;
    vector<double> *velocities;

    vector<double> *error_xi;
    vector<double> *error_eta;

    int coarsening_step;
    double coarsening_coef;

    vector<int> *coarsening_flag;

    int removed_elt_number;
    int added_shared_dof_number;
    int new_shared_elt_number;
    int new_shared_dof_number;

    vector<int> *elt_coarsenable;

    igCommunicator *communicator;

};

//
// igMeshRefinerIsotropic.h ends here
