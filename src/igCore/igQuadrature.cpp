/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igQuadrature.h"

#include <iostream>
#include <math.h>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

void igQuadrature::initializeGaussPoints(int dimension, int gauss_point_number_by_direction,
                                         vector<double> &gauss_point_coord_list,
                                         vector<double> &gauss_point_weight_list)
{
    // firstly compute 1D Gauss points

    vector<double> coord_list(gauss_point_number_by_direction, 0.);
    vector<double> weight_list(gauss_point_number_by_direction, 0.);

    if (gauss_point_number_by_direction == 1) {
        weight_list[0] = 2.;
        coord_list[0] = 0.;

    } else if (gauss_point_number_by_direction == 2) {
        weight_list[0] = 1.;
        coord_list[0] = -0.577350269189625;
        weight_list[1] = 1.;
        coord_list[1] = 0.577350269189625;

    } else if (gauss_point_number_by_direction == 3) {
        weight_list[0] = 0.555555555555556;
        coord_list[0] = -0.774596669241483;
        weight_list[1] = 0.888888888888889;
        coord_list[1] = 0.;
        weight_list[2] = 0.555555555555556;
        coord_list[2] = 0.774596669241483;

    } else if (gauss_point_number_by_direction == 4) {
        weight_list[0] = 0.347854845137454;
        coord_list[0] = -0.861136311594052;
        weight_list[1] = 0.652145154862545;
        coord_list[1] = -0.339981043584856;
        weight_list[2] = 0.652145154862545;
        coord_list[2] = 0.339981043584856;
        weight_list[3] = 0.347854845137454;
        coord_list[3] = 0.861136311594052;

    } else if (gauss_point_number_by_direction == 5) {
        weight_list[0] = 0.236926885056189;
        coord_list[0] = -0.906179845938664;
        weight_list[1] = 0.478628670499365;
        coord_list[1] = -0.538469310105683;
        weight_list[2] = 0.568888889888889;
        coord_list[2] = 0.;
        weight_list[3] = 0.478628670499365;
        coord_list[3] = 0.538469310105683;
        weight_list[4] = 0.236926885056189;
        coord_list[4] = 0.906179845938664;

    } else if (gauss_point_number_by_direction == 6) {
        weight_list[0] = 0.1713244923791704;
        coord_list[0] = -0.9324695142031521;
        weight_list[1] = 0.3607615730481386;
        coord_list[1] = -0.6612093864662645;
        weight_list[2] = 0.4679139345726910;
        coord_list[2] = -0.2386191860831969;
        weight_list[3] = 0.4679139345726910;
        coord_list[3] = 0.2386191860831969;
        weight_list[4] = 0.3607615730481386;
        coord_list[4] = 0.6612093864662645;
        weight_list[5] = 0.1713244923791704;
        coord_list[5] = 0.9324695142031521;

    } else if (gauss_point_number_by_direction == 7) {
        weight_list[0] = 0.1294849661688697;
        coord_list[0] = -0.9491079123427585;
        weight_list[1] = 0.2797053914892766;
        coord_list[1] = -0.7415311855993945;
        weight_list[2] = 0.3818300505051189;
        coord_list[2] = -0.4058451513773972;
        weight_list[3] = 0.4179591836734694;
        coord_list[3] = 0.;
        weight_list[4] = 0.3818300505051189;
        coord_list[4] = 0.4058451513773972;
        weight_list[5] = 0.2797053914892766;
        coord_list[5] = 0.7415311855993945;
        weight_list[6] = 0.1294849661688697;
        coord_list[6] = 0.9491079123427585;

    } else if (gauss_point_number_by_direction == 8) {
        weight_list[0] = 0.101228536290376259;
        coord_list[0] = -0.960289856497536232;
        weight_list[1] = 0.22238103445337447;
        coord_list[1] = -0.79666647741362674;
        weight_list[2] = 0.313706645877887287;
        coord_list[2] = -0.525532409916328986;
        weight_list[3] = 0.362683783378361983;
        coord_list[3] = -0.183434642495649805;
        weight_list[4] = 0.36268378337836198;
        coord_list[4] = 0.183434642495649805;
        weight_list[5] = 0.313706645877887287;
        coord_list[5] = 0.525532409916328986;
        weight_list[6] = 0.222381034453374471;
        coord_list[6] = 0.79666647741362674;
        weight_list[7] = 0.10122853629037626;
        coord_list[7] = 0.960289856497536232;

    } else if (gauss_point_number_by_direction == 9) {
    	weight_list[0] = 0.081274388361574412;
        coord_list[0] = -0.96816023950762609;
        weight_list[1] = 0.1806481606948574;
        coord_list[1] = -0.836031107326635794;
        weight_list[2] = 0.260610696402935462;
        coord_list[2] = -0.613371432700590397;
        weight_list[3] = 0.31234707704000284;
        coord_list[3] = -0.32425342340380893;
        weight_list[4] = 0.33023935500125976;
        coord_list[4] = 0.;
        weight_list[5] = 0.31234707704000284;
        coord_list[5] = 0.32425342340380893;
        weight_list[6] = 0.26061069640293546;
        coord_list[6] = 0.613371432700590397;
        weight_list[7] = 0.180648160694857404;
        coord_list[7] = 0.836031107326635794;
        weight_list[8] = 0.081274388361574412;
        coord_list[8] = 0.96816023950762609;

    } else {
        cout << "Too many Gauss points." << endl;
    }

    // rescaling for [0,1] interval
    for (int i = 0; i < gauss_point_number_by_direction; ++i){
        coord_list[i] = (coord_list[i] + 1)/2.;
        weight_list[i] /= 2.;
    }


    // Then compute tensor product for multi-D
    if (dimension == 1) {
        for (int i = 0; i < gauss_point_number_by_direction; ++i) {
            gauss_point_coord_list[i] = coord_list[i];
            gauss_point_weight_list[i] = weight_list[i];
        }
    }

    if (dimension == 2) {
        for (int j = 0; j < gauss_point_number_by_direction; ++j) {
            for (int i = 0; i < gauss_point_number_by_direction; ++i) {
                int index = j*gauss_point_number_by_direction + i;
                gauss_point_coord_list[index*dimension] = coord_list[i];
                gauss_point_coord_list[index*dimension+1] = coord_list[j];
                gauss_point_weight_list[index] = weight_list[i]*weight_list[j];
            }
        }
    }

    if (dimension == 3) {
        for (int k = 0; k < gauss_point_number_by_direction; ++k) {
            for (int j = 0; j < gauss_point_number_by_direction; ++j) {
                for (int i = 0; i < gauss_point_number_by_direction; ++i) {
                    int index = k*gauss_point_number_by_direction*gauss_point_number_by_direction + j*gauss_point_number_by_direction + i;
                    gauss_point_coord_list[index*dimension] = coord_list[i];
                    gauss_point_coord_list[index*dimension+1] = coord_list[j];
                    gauss_point_coord_list[index*dimension+2] = coord_list[k];
                    gauss_point_weight_list[index] = weight_list[i]*weight_list[j]*weight_list[k];
                }
            }
        }
    }

    return;
}

/////////////////////////////////////////////////////////////////////////////
