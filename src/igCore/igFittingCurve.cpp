/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/

#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>

#include "igFittingCurve.h"
#include "igBasisBezier.h"


using namespace std;

/////////////////////////////////////////////////////////////////////////////

igFittingCurve::igFittingCurve()
{

}

igFittingCurve::~igFittingCurve(void)
{

}

/////////////////////////////////////////////////////////////////////////////


void igFittingCurve::setParameterBounds(double p_min, double p_max)
{
    this->p_min = p_min;
    this->p_max = p_max;
}

void igFittingCurve::setCoordinateBounds(double x_min, double x_max, double y_min, double y_max, double z_min, double z_max)
{
    this->x_min = x_min;
    this->x_max = x_max;
    this->y_min = y_min;
    this->y_max = y_max;
    this->z_min = z_min;
    this->z_max = z_max;
}

void igFittingCurve::setParameterFixed(double p_fixed1, double p_fixed2)
{
    this->p_fixed1 = p_fixed1;
    this->p_fixed2 = p_fixed2;
}

void igFittingCurve::selectParameterFixed(int index1, int index2)
{
    this->fixed_parameter1 = index1;
    this->fixed_parameter2 = index2;
}

void igFittingCurve::setXFunction(function<double (double, double, double)> x_fun)
{
    this->x_fun = x_fun;
}

void igFittingCurve::setYFunction(function<double (double, double, double)> y_fun)
{
    this->y_fun = y_fun;
}

void igFittingCurve::setZFunction(function<double (double, double, double)> z_fun)
{
    this->z_fun = z_fun;
}

///////////////////////////////////////////////////////////////

double igFittingCurve::xControlPointFit(int index)
{
    return sol_x.at(index);
}

double igFittingCurve::yControlPointFit(int index)
{
    return sol_y.at(index);
}

double igFittingCurve::zControlPointFit(int index)
{
    return sol_z.at(index);
}

///////////////////////////////////////////////////////////////

void igFittingCurve::run(void)
{

    igBasis *basis = new igBasisBezier(degree);

    int size = degree-1;
    double *rhs_x = new double[size];
    double *rhs_y = new double[size];
    double *rhs_z = new double[size];
    double *mat = new double[size*size];

    vector<double> values;
    values.assign(degree+1,0.);

    // initialize matrix and rhs, sol
    for (int ils=0; ils<size; ils++){
        rhs_x[ils] = 0.;
        rhs_y[ils] = 0.;
        rhs_z[ils] = 0.;
        for (int jls=0; jls<size; jls++)
            mat[ils*size+jls] = 0.;
    }

    // compute matrix and rhs
    for (int kls=0; kls<sample_number; kls++){

        double coord = float(kls)/float(sample_number-1);
        basis->evalFunction(coord,values);

        double param = p_min + (p_max-p_min)*coord;

        double target_x;
        double target_y;
        double target_z;
        if(fixed_parameter1 == 0 && fixed_parameter2 == 2){
            target_x = x_fun(p_fixed1,param,p_fixed2);
            target_y = y_fun(p_fixed1,param,p_fixed2);
            target_z = z_fun(p_fixed1,param,p_fixed2);
        } else if (fixed_parameter1 == 1 && fixed_parameter2 == 2){
            target_x = x_fun(param,p_fixed1,p_fixed2);
            target_y = y_fun(param,p_fixed1,p_fixed2);
            target_z = z_fun(param,p_fixed1,p_fixed2);
        } else {
            target_x = x_fun(p_fixed1,p_fixed2,param);
            target_y = y_fun(p_fixed1,p_fixed2,param);
            target_z = z_fun(p_fixed1,p_fixed2,param);
        }

        for (int ils=0; ils<size; ils++){

            // rhs term
            rhs_x[ils] += values.at(ils+1) * target_x;
            rhs_y[ils] += values.at(ils+1) * target_y;
            rhs_z[ils] += values.at(ils+1) * target_z;

            // matrix
            for (int jls=0; jls<size; jls++)
                mat[ils*size+jls] += values.at(ils+1)*values.at(jls+1);

            // modification of rhs for bound constraints
            rhs_x[ils] -= values.at(ils+1)*values.at(0)*x_min
                    +  values.at(ils+1)*values.at(degree)*x_max;
            rhs_y[ils] -= values.at(ils+1)*values.at(0)*y_min
                    +  values.at(ils+1)*values.at(degree)*y_max;
            rhs_z[ils] -= values.at(ils+1)*values.at(0)*z_min
                    +  values.at(ils+1)*values.at(degree)*z_max;

        }
    }

    // solve system
    if (size > 0) {
        inverse_system(mat,size);
    }

    // store solution
    sol_x.resize(size);
    sol_y.resize(size);
    sol_z.resize(size);

    for (int ils=0; ils<size; ils++){
        sol_x[ils] = 0.;
        sol_y[ils] = 0.;
        sol_z[ils] = 0.;
        for (int jls=0; jls<size; jls++){
            sol_x[ils] += mat[ils*size+jls]*rhs_x[jls];
            sol_y[ils] += mat[ils*size+jls]*rhs_y[jls];
            sol_z[ils] += mat[ils*size+jls]*rhs_z[jls];
        }
    }
    delete basis;

}
