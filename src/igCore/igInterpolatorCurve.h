/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once


#include <igCoreExport>

#include <vector>

#include "igLinAlg.h"

using namespace std;

class IGCORE_EXPORT igInterpolatorCurve
{
public:
     igInterpolatorCurve(void);
    ~igInterpolatorCurve(void);

public:
    void setKnotVector(vector<double> *knot_vector);
    void setPoints(vector<double> &parameters, vector<double> &coordinates);
    void setDegree(int degree);

public:
    void run(void);

public:
    double controlPoint(int index);

protected:
    int degree;
    vector<double> *knot_vector;
    vector<double> parameters;
    vector<double> coordinates;
    vector<double> control_points;

    igLinAlg lin_solver;

protected:
    void inverse_system(double* A, int N);

};

//
// igInterpolatorCurve.h ends here
