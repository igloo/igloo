/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igCoreExport.h>

#include <vector>

using namespace std;

class IGCORE_EXPORT igCell
{
public:
             igCell(void);
    virtual ~igCell(void) = default;

public:
    void setCellId(int id_value);
    void setCellGlobalId(int id_value);
    void setPhysicalDimension(int dimension_value);
    void setParametricDimension(int dimension_value);
    void setDegree(int degree_value);
    void setVariableNumber(int number);
    void setDerivativeNumber(int number);
    void setGaussPointNumberByDirection(int number);

    void setCoordinates(vector<double> *coord);
    void setVelocities(vector<double> *vel);

    void setChild(int id, int child_index);
    void setChildRank(int rank);
    void setParent(int id);

public:
    virtual void initializeDegreesOfFreedom(void) = 0;
    virtual void initializeGeometry(void) = 0;

    void computeGaussPointVelocity(void);

public:
    int cellId(void) const;
    int cellGlobalId(void) const;
    int physicalDimension(void) const;
    int parametricDimension(void) const;
    int cellDegree(void) const;

    double area(void) const;

    int controlPointNumber(void) const;
    int controlPointNumberByDirection(void) const;

    virtual double controlPointVelocityComponent(int index, int component) const = 0;
    virtual void controlPointVelocity(int index, vector<double> *vel) = 0;

    int gaussPointNumber(void) const;
    int gaussPointNumberByDirection(void) const;
    double *gaussPointPhysicalCoordinate(int index);
    double *gaussPointParametricCoordinate(int index);
    double gaussPointPhysicalCoordinate(int index, int component) const;
    double gaussPointParametricCoordinate(int index, int component) const;
    double gaussPointWeight(int index) const;
    double gaussPointJacobian(int index) const;
    double gaussPointJacobianMatrix(int index, int icomponent, int jcomponent) const;
    double gaussPointFunctionValue(int pt_index, int fun_index) const;
    double *gaussPointFunctionPtr(int pt_index);
    double gaussPointGradientValue(int pt_index, int fun_index, int component) const;
    double *gaussPointGradientPtr(int pt_index, int fun_index);
    double *gaussPointGradientPtr(int pt_index);
    void gaussPointVelocity(int pt_index, vector<double> *mesh_vel);

    void setActive(void);
    void setInactive(void);
    bool isActive(void);
    bool hasChild(void);
    int child(int id);
    int childRank(void);
    int parent(void);

protected:
    int id;
    int global_id;
    int child_rank;
    bool active;
    bool has_child;
    vector<int> child_list;
    int cell_parent;

    int degree;
    int physical_dimension;
    int parametric_dimension;
    int variable_number;
    int derivative_number;

    double cell_area;

    int control_point_number;
    int control_point_number_by_direction;

    vector<double> *coordinates;
    vector<double> *velocities;

    int gauss_point_number;
    int gauss_point_number_by_direction;
    vector<double> gauss_point_coord_list;
    vector<double> gauss_point_physical_coord_list;
    vector<double> gauss_point_weight_list;
    vector<double> gauss_point_jacobian_list;
    vector<double> gauss_point_jacobian_matrix_list;

    vector<double> gauss_point_value_list;
    vector<double> gauss_point_gradient_list;
    vector<double> gauss_point_velocity_list;
};

// ///////////////////////////////////////////////////////////////////


//
// igCell.h ends here
