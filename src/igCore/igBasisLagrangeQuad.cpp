/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igBasisLagrangeQuad.h"

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igBasisLagrangeQuad::igBasisLagrangeQuad(int degree_value) : igBasis(degree_value)
{
	this->function_number = this->degree + 1;
}

/////////////////////////////////////////////////////////////////////////////

int igBasisLagrangeQuad::functionNumber()
{
	return function_number;
}

void igBasisLagrangeQuad::evalFunction(double var, vector<double> &values)
{
    double x = -1. + 2.*var;

    values[0] = x*(x+1.)/2.;
    values[1] = 1.-x*x;
    values[2] = x*(x-1.)/2.;

}

void igBasisLagrangeQuad::evalGradient(double var, vector<double> &values)
{
    double x = -1. + 2.*var;

    values[0] = (2.*x+1.);
    values[1] = -4.*x;
    values[2] = (2.*x-1.);
}
