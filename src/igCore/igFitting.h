/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igCoreExport>

#include "igLinAlg.h"

using namespace std;

class igElement;

class IGCORE_EXPORT igFitting
{
public:
     igFitting(void);
    ~igFitting(void) = default;

public:
    void setSampleNumber(int number);
    void setElement(igElement *elt);
    void setDegree(int degree);

public:
    virtual void run(void) = 0;


protected:
    int sample_number;

    int degree;

    igElement *element;

    igLinAlg lin_solver;

protected:
    void inverse_system(double* A, int N);

};

//
// igFitting.h ends here
