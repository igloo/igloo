/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igCoreExport.h>

#include <vector>

using namespace std;

class igMesh;
class igElement;
class igFace;
class igCommunicator;

class IGCORE_EXPORT igMeshRefinerIsotropic
{
public:
             igMeshRefinerIsotropic(void);
    virtual ~igMeshRefinerIsotropic(void);

public:
    void initialize(igMesh *mesh, vector<double> *state, vector<double> *coordinates, vector<double> *velocities);

    void setError(vector<double> *error_xi, vector<double> *error_eta);
    void setRefineCoefficient(double refine_coef);
    void setRefineMaxLevel(int level);
    void setAdaptBox(vector<double> *amr_box);

    void setCommunicator(igCommunicator *communicator);

    void select(void);
    void refine(void);

private:
    void checkRefinement(void);
    void propagateRefinement(void);
    void computeSharedDataNumber(void);

    void refineElement(igElement *elt);
    void refineFace(igFace *face);

    void updateFace(igFace *face, bool left);
    void updateMap(igFace *face, bool left);
    void updateHalfFace(igFace *face, bool left);

    void addMidFaces(igElement *elt);
    void buildMidFace(igElement *elt, int face_index);

private:
    igMesh *mesh;
    vector<double> *state;
    vector<double> *coordinates;
    vector<double> *velocities;

    igCommunicator *communicator;

    vector<double> *error_xi;
    vector<double> *error_eta;
    int added_elt_number;
    int new_shared_dof_number;
    int added_shared_dof_number;
    int new_shared_elt_number;
    int added_shared_elt_number;

    int refinement_step;
    double refine_coef;
    int refine_max_level;
    vector<double> *amr_box;

    vector<int> *refine_flag;
    vector<int> *blocking_flag;

    vector<int> *elt_refinable;

    vector<int> *face_to_treat;

    int previous_element_number;
    int previous_face_number;
    int previous_bnd_number;

    int global_elt_id_counter;

};

//
// igMeshRefinerIsotropic.h ends here
