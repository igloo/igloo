/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igMeshRefinerIsotropic.h"

#include <igDistributed/igCommunicator.h>

#include "igMesh.h"
#include "igCell.h"
#include "igElement.h"
#include "igFace.h"
#include "igBoundaryIds.h"

#include <iostream>
#include <stdlib.h>
#include <iomanip>
#include <igAssert.h>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igMeshRefinerIsotropic::igMeshRefinerIsotropic(void)
{
    this->state = nullptr;
    this->mesh = nullptr;
    this->error_xi = nullptr;
    this->error_eta = nullptr;
    this->refine_flag = nullptr;
    this->blocking_flag = nullptr;
    this->elt_refinable = nullptr;
    this->face_to_treat = nullptr;
    this->coordinates = nullptr;
    this->velocities = nullptr;
    this->communicator = nullptr;
}

igMeshRefinerIsotropic::~igMeshRefinerIsotropic(void)
{
    if(this->refine_flag)
        delete refine_flag;
    if(this->blocking_flag)
        delete blocking_flag;
    if(this->elt_refinable)
        delete elt_refinable;
    if(this->face_to_treat)
        delete face_to_treat;
}


/////////////////////////////////////////////////////////////////////////////

void igMeshRefinerIsotropic::initialize(igMesh *mesh, vector<double> *state, vector<double> *coordinates, vector<double> *velocities)
{
    IG_ASSERT(mesh, "no mesh");
    IG_ASSERT(state, "no state");

    this->mesh = mesh;
    this->state = state;
    this->coordinates = coordinates;
    this->velocities = velocities;

    refinement_step = 0;
    refine_max_level = 0;

    this->refine_flag = new vector<int>();
    this->blocking_flag = new vector<int>();
    this->elt_refinable = new vector<int>();
    this->face_to_treat = new vector<int>();

    return;
}

void igMeshRefinerIsotropic::setError(vector<double> *error_xi, vector<double> *error_eta)
{
    IG_ASSERT(error_xi, "no error_xi");
    IG_ASSERT(error_eta, "no error_eta");

    this->error_xi = error_xi;
    this->error_eta = error_eta;
}

void igMeshRefinerIsotropic::setRefineCoefficient(double refine_coef)
{
    this->refine_coef = refine_coef;
    return;
}

void igMeshRefinerIsotropic::setRefineMaxLevel(int level)
{
    this->refine_max_level = level;
    return;
}

void igMeshRefinerIsotropic::setAdaptBox(vector<double> *amr_box)
{
    IG_ASSERT(amr_box, "no amr_box");

    this->amr_box = amr_box;
}

void igMeshRefinerIsotropic::setCommunicator(igCommunicator *communicator)
{
    this->communicator = communicator;
}


/////////////////////////////////////////////////////////////////////////////


void igMeshRefinerIsotropic::select(void)
{
    if(communicator->rank() == 0){
        cout << ">>> mesh refinement process: selection" << endl;
    }
    refine_flag->assign(mesh->elementNumber()+mesh->sharedElementNumber(),0);
    blocking_flag->assign(mesh->elementNumber()+mesh->sharedElementNumber(),0);

    this->added_elt_number= 0;

    // first check if elements can be refined
    this->checkRefinement();

    // MPI communication
    communicator->distributeElementArrayInt(elt_refinable);

    // first loop over elements to compute global error
    double local_error = 0.;
    for (int iel=0; iel<mesh->elementNumber(); iel++){
        igElement *elt = mesh->element(iel);
        if(elt->isActive())
            local_error += error_xi->at(iel) + error_eta->at(iel);
    }
    // MPI communication
    double mean_error = communicator->distributeError(local_error);

    mean_error /= mesh->totalActiveElementNumber();

    // second loop over elements to select
    int blocking_config = 0;
    int number_elt_to_refine = 0;
    for (int iel=0; iel<mesh->elementNumber(); iel++){
        igElement *elt = mesh->element(iel);
        refine_flag->at(iel) = 0;

        if(elt->isActive()){
            double refine_ratio = (error_xi->at(iel)+error_eta->at(iel))/mean_error;

            if(refine_ratio > refine_coef){

                if ( elt_refinable->at(iel) == 0 ){

                    if(mesh->elementLevel(iel,0) < refine_max_level){
                        refine_flag->at(iel) = 1;
                        number_elt_to_refine++;
                        if(!elt->hasChild())
                            this->added_elt_number += 4;
                    }
                } else {
                    blocking_flag->at(iel) = 1;
                    blocking_config++;
                }
            }

        }
    }

    // MPI communication
    this->communicator->distributeElementArrayInt(refine_flag);
    this->communicator->distributeElementArrayInt(blocking_flag);

    // propagate refinement to avoid blocking configurations
    this->propagateRefinement();

    // MPI communication
    this->communicator->distributeElementArrayInt(refine_flag);

    // compute new shared face and dof number
    this->computeSharedDataNumber();

    int total_number_elt_to_refine = this->communicator->addInt(number_elt_to_refine);

    if(communicator->rank() == 0){
        cout << "Mean error: " << mean_error << endl;
        cout << "Number of elements to refine: " << total_number_elt_to_refine << endl;
        cout << ">>> selection done" << endl;
        cout << endl;
    }

    return;
}

///////////////////////////////////////////////////////////////////////

void igMeshRefinerIsotropic::refine(void)
{
    if(communicator->rank() == 0){
        cout << ">>> mesh refinement process: refinement" << endl;
    }

    previous_element_number = mesh->elementNumber();
    previous_face_number = mesh->faceNumber();
    previous_bnd_number = mesh->boundaryFaceNumber();

    int ctrl_pts_per_elt = mesh->element(0)->controlPointNumber();
    int variable_number = mesh->variableNumber();
    int old_state_size = state->size();
    int new_state_size = ( (previous_element_number+added_elt_number)*ctrl_pts_per_elt + new_shared_dof_number ) *variable_number;
    int new_coord_size = ( (previous_element_number+added_elt_number)*ctrl_pts_per_elt + new_shared_dof_number ) *(mesh->meshDimension()+1);
    int new_vel_size   = ( (previous_element_number+added_elt_number)*ctrl_pts_per_elt + new_shared_dof_number ) *mesh->meshDimension();
    int max_field_number = max(variable_number,mesh->derivativeNumber());
    max_field_number = max(max_field_number,mesh->meshDimension()+1);

    // temporary array to manage face update

    this->face_to_treat->assign(previous_face_number,0);
    for (int ifac=0; ifac<previous_face_number; ifac++){
        igFace *face = mesh->face(ifac);
        if(face->isActive())
            this->face_to_treat->at(ifac) = 1;
    }

    //------------- fill temporary new state vector and update global Id map ---------------

    vector<double> *state_tmp = new vector<double>;
    state_tmp->resize(state->size(),0.);

    vector<double> *coordinates_tmp = new vector<double>;
    coordinates_tmp->resize(coordinates->size(),0.);

    vector<double> *velocities_tmp = new vector<double>;
    velocities_tmp->resize(velocities->size(),0.);

    // fill temporary arrays
    for(int i=0; i<state->size(); i++){
        state_tmp->at(i) = state->at(i);
    }

    for(int i=0; i<coordinates->size(); i++){
        coordinates_tmp->at(i) = coordinates->at(i);
    }

    for(int i=0; i<velocities->size(); i++){
        velocities_tmp->at(i) = velocities->at(i);
    }

    // fill new state, coordinates and velocities
    state->resize(new_state_size);
    coordinates->resize(new_coord_size);
    velocities->resize(new_vel_size);

    int current_index = 0;
    int current_index_new = 0;
    for(int ivar=0; ivar<max_field_number; ivar++){
        for(int iel=0; iel<previous_element_number; iel++){
            for(int ipt=0; ipt<ctrl_pts_per_elt; ipt++){
                if(ivar<variable_number)
                    state->at(current_index_new) = state_tmp->at(current_index);
                if(ivar<mesh->meshDimension()+1)
                    coordinates->at(current_index_new) = coordinates_tmp->at(current_index);
                if(ivar<mesh->meshDimension())
                    velocities->at(current_index_new) = velocities_tmp->at(current_index);
                mesh->element(iel)->setGlobalId(ipt, ivar, current_index_new);
                current_index++;
                current_index_new++;
            }
        }
        current_index += mesh->sharedControlPointNumber();
        current_index_new += added_elt_number*ctrl_pts_per_elt + new_shared_dof_number;
    }

    // copy level arrays
    mesh->refreshLevelArray(previous_element_number+added_elt_number,new_shared_elt_number);

    //------------ refinement of elements and faces ----------------------

    //-------- loop over all existing elements
    for (int iel=0; iel<previous_element_number; iel++){

        igElement *elt = mesh->element(iel);

        if( refine_flag->at(iel) == 1 )
            this->refineElement(elt);
    }
    mesh->updateMeshProperties();
    mesh->updateGlobalMeshProperties();

    // update face maps for all existing faces
    for (int ifac=0; ifac<previous_face_number; ifac++){
        igFace *face = mesh->face(ifac);
        this->updateMap(face, true);
        this->updateMap(face, false);
    }
    for (int ifac=0; ifac<previous_bnd_number; ifac++){
        igFace *face = mesh->boundaryFace(ifac);
        this->updateMap(face, true);
    }

    //--------- loop over faces to update and refine faces
    for (int ifac=0; ifac<previous_face_number; ifac++){

        igFace *face = mesh->face(ifac);
        if(face->isActive()){

            int elt_left_index = face->leftElementIndex();
            int elt_right_index = face->rightElementIndex();

            bool face_to_refine = false;

            if(this->face_to_treat->at(ifac) == 1){

                // check refinement type for left element
                if( refine_flag->at(elt_left_index) == 1){

                    if(face->childRank() == 0) // additional test to avoid double refinement
                        face_to_refine = true;
                    else
                        this->updateHalfFace(face,true);

                }

                // check refinement type for right element
                if( refine_flag->at(elt_right_index) == 1){

                    if(face->childRank() == 0) // additional test to avoid double refinement
                        face_to_refine = true;
                    else
                        this->updateHalfFace(face,false);

                }
            }

            if(face_to_refine)
                this->refineFace(face);
        }
    }

    // refresh communication maps and shared dof maps
    mesh->refreshCommunicatorMaps();

    // MPI communication for level array
    mesh->distributeLevelArray();

    // MPI communication for coordinates and velocities and update of shared faces
    communicator->distributeField(coordinates, mesh->meshDimension()+1);
    communicator->distributeField(velocities, mesh->meshDimension());

    vector<double> *coord = new vector<double>;
    coord->resize(mesh->meshDimension());
    for(int ifac=0; ifac<mesh->faceNumber(); ifac++){
        igFace *face = mesh->face(ifac);
        if(face->shared() && face->isActive()){
            face->initializeGeometry();
            igElement *elt;
            if(face->leftInside()){
                igElement *elt = mesh->element(face->leftElementIndex());
                for (int idim=0; idim<mesh->meshDimension(); idim++)
                    coord->at(idim) = elt->barycenter(idim);
                face->initializeNormal(coord);
            } else {
                igElement *elt = mesh->element(face->rightElementIndex());
                for (int idim=0; idim<mesh->meshDimension(); idim++)
                    coord->at(idim) = elt->barycenter(idim);
                face->initializeNormalFromRight(coord);
            }
        }
    }
    delete coord;


    //--------- loop over elements to add new mid-faces
    for (int iel=0; iel<previous_element_number; iel++){

        if( refine_flag->at(iel) == 1 )
            this->addMidFaces(mesh->element(iel));
    }

    //-------- loop over boundary faces
    for (int ibnd=0; ibnd < previous_bnd_number; ibnd++){

        igFace *face = mesh->boundaryFace(ibnd);
        int elt_left_index = face->leftElementIndex();

        igElement *elt = mesh->element(elt_left_index);

        if( refine_flag->at(elt_left_index) == 1 )
            this->refineFace(face);

    }

    mesh->updateMeshProperties();
    mesh->updateGlobalMeshProperties();

    //------------ update of global Ids for active elements ----------

    global_elt_id_counter = 0;
    int next_counter = global_elt_id_counter + mesh->activeElementNumber();

    for(int ipart=0; ipart<communicator->size(); ipart++){

        int new_counter = communicator->distributeGlobalIdCounter(next_counter,ipart);

        if(communicator->rank() == ipart+1){
            global_elt_id_counter = new_counter;
            next_counter = global_elt_id_counter + mesh->activeElementNumber();
        }
    }

    for(int iel=0; iel<mesh->elementNumber(); iel++){
        igElement *elt = mesh->element(iel);
        if(elt->isActive()){
            elt->setCellGlobalId(global_elt_id_counter);
            global_elt_id_counter++;
        }
    }

    //update for faces (necessary for ALE)
    for(int ifac = 0; ifac < mesh->faceNumber(); ifac++){
        igFace *face = mesh->face(ifac);
        if(face->isActive()){
            face->initializeGeometry();
            face->updateNormal();
        }
    }
    for (int ifac = 0; ifac < mesh->boundaryFaceNumber(); ++ifac) {
        igFace *face = mesh->boundaryFace(ifac);
        if(face->isActive()){
            face->initializeGeometry();
            face->updateNormal();
        }
    }

    //-------------------------------------------------------------------

    int total_previous_element_number = this->communicator->addInt(previous_element_number);
    int total_old_state_size = this->communicator->addInt(old_state_size);
    int total_new_state_size = this->communicator->addInt(new_state_size);
    int total_previous_face_number = this->communicator->addInt(previous_face_number);
    int total_previous_bnd_number = this->communicator->addInt(previous_bnd_number);

    if(communicator->rank() == 0){
        cout << "Number of elements - previous: " << total_previous_element_number << ", new: " << mesh->totalElementNumber() << ", active: " << mesh->totalActiveElementNumber()  << endl;
        cout << "Number of faces - previous: " << total_previous_face_number << ", new: " << mesh->totalFaceNumber() << ", active: " << mesh->totalActiveFaceNumber() << endl;
        cout << "Number of boundary faces - previous: " << total_previous_bnd_number << ", new: " << mesh->totalBoundaryFaceNumber() << ", active: " << mesh->totalActiveBoundaryFaceNumber() << endl;
        cout << "State vector size  - previous: " << total_old_state_size << ", new: " << total_new_state_size << endl;
        cout << "Number of active DOFs: " << mesh->totalActiveElementNumber()*ctrl_pts_per_elt << endl;
        cout << ">>> refinement done" << endl;
        cout << endl;
    }

    refinement_step++;

    delete state_tmp;
    delete coordinates_tmp;
    delete velocities_tmp;

    return;
}

//////////////////////////////////////////////////////

void igMeshRefinerIsotropic::refineElement(igElement *elt)
{
    IG_ASSERT(elt, "no element");

    int dimension = mesh->meshDimension();
    int degree = mesh->element(0)->cellDegree();
    int ctrl_pts_per_elt = mesh->element(0)->controlPointNumber();
    int variable_number = mesh->variableNumber();
    int max_field_number = max(variable_number,mesh->derivativeNumber());
    max_field_number = max(max_field_number,mesh->meshDimension()+1);
    int new_dof_number = (previous_element_number+added_elt_number)*ctrl_pts_per_elt + new_shared_dof_number;

    igElement *new_elt1;
    igElement *new_elt2;
    igElement *new_elt3;
    igElement *new_elt4;

    if(!elt->hasChild()){ // construction of new elements if not existing yet

        new_elt1 = new igElement;
        new_elt2 = new igElement;
        new_elt3 = new igElement;
        new_elt4 = new igElement;

        //------------ construction of new elements -----------

        // generation of new element 1
        new_elt1->setCellId(mesh->elementNumber());
        new_elt1->setPartition(elt->partition());
        new_elt1->setPhysicalDimension(dimension);
        new_elt1->setParametricDimension(dimension);
        new_elt1->setDegree(elt->cellDegree());
        new_elt1->setGaussPointNumberByDirection(elt->gaussPointNumberByDirection());
        new_elt1->setVariableNumber(mesh->variableNumber());
        new_elt1->setDerivativeNumber(mesh->derivativeNumber());
        new_elt1->setCoordinates(this->coordinates);
        new_elt1->setVelocities(this->velocities);

        new_elt1->initializeDegreesOfFreedom();

        int global_id_counter = mesh->elementNumber()*ctrl_pts_per_elt;
        for (int ivar=0; ivar<max_field_number; ivar++){
            for (int ipt=0; ipt<ctrl_pts_per_elt; ipt++){
                new_elt1->setGlobalId(ipt, ivar, global_id_counter);
                global_id_counter++;
            }
            global_id_counter += new_dof_number-ctrl_pts_per_elt;
        }

        // generation of second new element 2
        new_elt2->setCellId(mesh->elementNumber()+1);
        new_elt2->setPartition(elt->partition());
        new_elt2->setPhysicalDimension(dimension);
        new_elt2->setParametricDimension(dimension);
        new_elt2->setDegree(elt->cellDegree());
        new_elt2->setGaussPointNumberByDirection(elt->gaussPointNumberByDirection());
        new_elt2->setVariableNumber(mesh->variableNumber());
        new_elt2->setDerivativeNumber(mesh->derivativeNumber());
        new_elt2->setCoordinates(this->coordinates);
        new_elt2->setVelocities(this->velocities);

        new_elt2->initializeDegreesOfFreedom();

        global_id_counter = (mesh->elementNumber()+1)*ctrl_pts_per_elt;
        for (int ivar=0; ivar<max_field_number; ivar++){
            for (int ipt=0; ipt<ctrl_pts_per_elt; ipt++){
                new_elt2->setGlobalId(ipt, ivar, global_id_counter);
                global_id_counter++;
            }
            global_id_counter += new_dof_number-ctrl_pts_per_elt;
        }

        // generation of second new element 3
        new_elt3->setCellId(mesh->elementNumber()+2);
        new_elt3->setPartition(elt->partition());
        new_elt3->setPhysicalDimension(dimension);
        new_elt3->setParametricDimension(dimension);
        new_elt3->setDegree(elt->cellDegree());
        new_elt3->setGaussPointNumberByDirection(elt->gaussPointNumberByDirection());
        new_elt3->setVariableNumber(mesh->variableNumber());
        new_elt3->setDerivativeNumber(mesh->derivativeNumber());
        new_elt3->setCoordinates(this->coordinates);
        new_elt3->setVelocities(this->velocities);

        new_elt3->initializeDegreesOfFreedom();

        global_id_counter = (mesh->elementNumber()+2)*ctrl_pts_per_elt;
        for (int ivar=0; ivar<max_field_number; ivar++){
            for (int ipt=0; ipt<ctrl_pts_per_elt; ipt++){
                new_elt3->setGlobalId(ipt, ivar, global_id_counter);
                global_id_counter++;
            }
            global_id_counter += new_dof_number-ctrl_pts_per_elt;
        }

        // generation of second new element 4
        new_elt4->setCellId(mesh->elementNumber()+3);
        new_elt4->setPartition(elt->partition());
        new_elt4->setPhysicalDimension(dimension);
        new_elt4->setParametricDimension(dimension);
        new_elt4->setDegree(elt->cellDegree());
        new_elt4->setGaussPointNumberByDirection(elt->gaussPointNumberByDirection());
        new_elt4->setVariableNumber(mesh->variableNumber());
        new_elt4->setDerivativeNumber(mesh->derivativeNumber());
        new_elt4->setCoordinates(this->coordinates);
        new_elt4->setVelocities(this->velocities);

        new_elt4->initializeDegreesOfFreedom();

        global_id_counter = (mesh->elementNumber()+3)*ctrl_pts_per_elt;
        for (int ivar=0; ivar<max_field_number; ivar++){
            for (int ipt=0; ipt<ctrl_pts_per_elt; ipt++){
                new_elt4->setGlobalId(ipt, ivar, global_id_counter);
                global_id_counter++;
            }
            global_id_counter += new_dof_number-ctrl_pts_per_elt;
        }

    } else { // case of already existing childs

        new_elt1 = mesh->element(elt->child(0));
        new_elt1->setActive();
        new_elt2 = mesh->element(elt->child(1));
        new_elt2->setActive();
        new_elt3 = mesh->element(elt->child(2));
        new_elt3->setActive();
        new_elt4 = mesh->element(elt->child(3));
        new_elt4->setActive();

    }
    //---------- computation of control points and solution -------------

    vector<double> coord_tmp;
    coord_tmp.resize(degree+1);
    vector<double> coord_curve1;
    coord_curve1.resize(degree+1);
    vector<double> coord_curve2;
    coord_curve2.resize(degree+1);
    vector<double> tmp_split1;
    tmp_split1.resize(ctrl_pts_per_elt*(dimension+1+variable_number));
    vector<double> tmp_split2;
    tmp_split2.resize(ctrl_pts_per_elt*(dimension+1+variable_number));

    int index;

    // state variables + coordinates + weight
    int field_to_split = dimension + variable_number + 1;

    // first loop over lines of control net => treatment of direction Xi
    for (int icurve=0; icurve<degree+1; icurve++){

        // treatment of components (geometrical + solution)
        for(int icomp=0; icomp<field_to_split; icomp++){

            // retrieve curve control points
            for(int ipt=0; ipt<degree+1; ipt++){

                index = (degree+1)*icurve + ipt;

                if(icomp < dimension) // geometry : coordinates * weights
                    coord_tmp.at(ipt) = coordinates->at(elt->globalId(index, icomp)) *
                        coordinates->at(elt->globalId(index, dimension));
                else if(icomp == dimension) // geometry : weights
                    coord_tmp.at(ipt) = coordinates->at(elt->globalId(index, dimension));
                else //solution * weights
                    coord_tmp.at(ipt) = state->at(elt->globalId(index,icomp-dimension-1)) *
                        coordinates->at(elt->globalId(index, dimension));
            }

            // keep extremities
            coord_curve1.at(0) = coord_tmp.at(0);
            coord_curve2.at(degree) = coord_tmp.at(degree);

            // subdivision procedure
            for (int istep=0; istep<degree; istep++){

                for (int ipt=0; ipt<degree-istep; ipt++)
                    coord_tmp.at(ipt) = 0.5*coord_tmp.at(ipt) + 0.5*coord_tmp.at(ipt+1);

                coord_curve1.at(1+istep) = coord_tmp.at(0);
                coord_curve2.at(degree-1-istep) = coord_tmp.at(degree-1-istep);
            }

            // store control points
            for (int ipt=0; ipt<degree+1; ipt++){

                index = (degree+1)*icurve + ipt;

                tmp_split1.at(ctrl_pts_per_elt*icomp + index) = coord_curve1.at(ipt);
                tmp_split2.at(ctrl_pts_per_elt*icomp + index) = coord_curve2.at(ipt);
            }
        }
    }

    // second loop over lines of control net => treatment of direction Eta (first pair of elements)
    for (int icurve=0; icurve<degree+1; icurve++){

        // treatment of components (geometrical + solution)
        for(int icomp=0; icomp<field_to_split; icomp++){

            // retrieve curve control points
            for(int ipt=0; ipt<degree+1; ipt++){

                index = (degree+1)*ipt + icurve;

                coord_tmp.at(ipt) = tmp_split1.at(ctrl_pts_per_elt*icomp + index);
            }

            // keep extremities
            coord_curve1.at(0) = coord_tmp.at(0);
            coord_curve2.at(degree) = coord_tmp.at(degree);

            // subdivision procedure
            for (int istep=0; istep<degree; istep++){

                for (int ipt=0; ipt<degree-istep; ipt++)
                    coord_tmp.at(ipt) = 0.5*coord_tmp.at(ipt) + 0.5*coord_tmp.at(ipt+1);

                coord_curve1.at(1+istep) = coord_tmp.at(0);
                coord_curve2.at(degree-1-istep) = coord_tmp.at(degree-1-istep);
            }

            // store control points
            for (int ipt=0; ipt<degree+1; ipt++){

                index = (degree+1)*ipt + icurve;

                if(icomp < dimension){ // geometry : coordinates *weights
                    coordinates->at(new_elt1->globalId(index,icomp)) = coord_curve1.at(ipt);
                    coordinates->at(new_elt3->globalId(index,icomp)) = coord_curve2.at(ipt);
                } else if (icomp == dimension){
                    coordinates->at(new_elt1->globalId(index,dimension)) = coord_curve1.at(ipt);
                    coordinates->at(new_elt3->globalId(index,dimension)) = coord_curve2.at(ipt);
                }else{
                    state->at(new_elt1->globalId(index,icomp-dimension-1)) = coord_curve1.at(ipt);
                    state->at(new_elt3->globalId(index,icomp-dimension-1)) = coord_curve2.at(ipt);
                }

            }
        }

        // recover NURBS formulation
        for(int icomp=0; icomp<dimension; icomp++){
            for (int ipt=0; ipt<degree+1; ipt++){

                index = (degree+1)*ipt + icurve;
                coordinates->at(new_elt1->globalId(index,icomp)) /= coordinates->at(new_elt1->globalId(index,dimension));
                coordinates->at(new_elt3->globalId(index,icomp)) /= coordinates->at(new_elt3->globalId(index,dimension));
            }
        }

        for(int icomp=0; icomp<variable_number; icomp++){
            for (int ipt=0; ipt<degree+1; ipt++){

                index = (degree+1)*ipt + icurve;
                state->at(new_elt1->globalId(index,icomp)) /= coordinates->at(new_elt1->globalId(index,dimension));
                state->at(new_elt3->globalId(index,icomp)) /= coordinates->at(new_elt3->globalId(index,dimension));
            }
        }

    }

    // last loop over lines of control net => treatment of direction Eta (second pair of elements)
    for (int icurve=0; icurve<degree+1; icurve++){

        // treatment of components (geometrical + solution)
        for(int icomp=0; icomp<field_to_split; icomp++){

            // retrieve curve control points
            for(int ipt=0; ipt<degree+1; ipt++){

                index = (degree+1)*ipt + icurve;

                coord_tmp.at(ipt) = tmp_split2.at(ctrl_pts_per_elt*icomp + index);
            }

            // keep extremities
            coord_curve1.at(0) = coord_tmp.at(0);
            coord_curve2.at(degree) = coord_tmp.at(degree);

            // subdivision procedure
            for (int istep=0; istep<degree; istep++){

                for (int ipt=0; ipt<degree-istep; ipt++)
                    coord_tmp.at(ipt) = 0.5*coord_tmp.at(ipt) + 0.5*coord_tmp.at(ipt+1);

                coord_curve1.at(1+istep) = coord_tmp.at(0);
                coord_curve2.at(degree-1-istep) = coord_tmp.at(degree-1-istep);
            }

            // store control points
            for (int ipt=0; ipt<degree+1; ipt++){

                index = (degree+1)*ipt + icurve;

                if(icomp < dimension){ // geometry
                    coordinates->at(new_elt2->globalId(index,icomp)) = coord_curve1.at(ipt);
                    coordinates->at(new_elt4->globalId(index,icomp)) = coord_curve2.at(ipt);
                } else if (icomp == dimension){
                    coordinates->at(new_elt2->globalId(index,dimension)) = coord_curve1.at(ipt);
                    coordinates->at(new_elt4->globalId(index,dimension)) = coord_curve2.at(ipt);
                } else {
                    state->at(new_elt2->globalId(index,icomp-dimension-1)) = coord_curve1.at(ipt);
                    state->at(new_elt4->globalId(index,icomp-dimension-1)) = coord_curve2.at(ipt);
                }
            }
        }

                // recover coordinates and weights
        for(int icomp=0; icomp<dimension; icomp++){
            for (int ipt=0; ipt<degree+1; ipt++){

                index = (degree+1)*ipt + icurve;
                coordinates->at(new_elt2->globalId(index,icomp)) /= coordinates->at(new_elt2->globalId(index,dimension));
                coordinates->at(new_elt4->globalId(index,icomp)) /= coordinates->at(new_elt4->globalId(index,dimension));
            }
        }

        for(int icomp=0; icomp<variable_number; icomp++){
            for (int ipt=0; ipt<degree+1; ipt++){

                index = (degree+1)*ipt + icurve;
                state->at(new_elt2->globalId(index,icomp)) /= coordinates->at(new_elt2->globalId(index,dimension));
                state->at(new_elt4->globalId(index,icomp)) /= coordinates->at(new_elt4->globalId(index,dimension));
            }
        }


    }

    //----------- finalize element construction --------------

    if(!elt->hasChild()){

        // geometry
        new_elt1->initializeGeometry();
        new_elt2->initializeGeometry();
        new_elt3->initializeGeometry();
        new_elt4->initializeGeometry();

        // parent / child
        elt->setChild(0,mesh->elementNumber());
        elt->setChild(1,mesh->elementNumber()+1);
        elt->setChild(2,mesh->elementNumber()+2);
        elt->setChild(3,mesh->elementNumber()+3);

        new_elt1->setParent(elt->cellId());
        new_elt2->setParent(elt->cellId());
        new_elt3->setParent(elt->cellId());
        new_elt4->setParent(elt->cellId());

        // add into list
        mesh->addElement(new_elt1);
        mesh->addElement(new_elt2);
        mesh->addElement(new_elt3);
        mesh->addElement(new_elt4);
        mesh->computeElementNumber();

        // element level
        int father_index = elt->cellId();
        int father_level_0 = mesh->elementLevel(father_index,0);

        int son1_index = new_elt1->cellId();
        int son2_index = new_elt2->cellId();
        int son3_index = new_elt3->cellId();
        int son4_index = new_elt4->cellId();

        mesh->setElementLevel(son1_index, 0, father_level_0+1);
        mesh->setElementLevel(son2_index, 0, father_level_0+1);
        mesh->setElementLevel(son3_index, 0, father_level_0+1);
        mesh->setElementLevel(son4_index, 0, father_level_0+1);

    }

    elt->setInactive();

}

///////////////////////////////////////////////////////////////

void igMeshRefinerIsotropic::refineFace(igFace *face)
{
    IG_ASSERT(face, "no face");

    int dimension = face->physicalDimension();
    int max_field_number = max(mesh->variableNumber(),mesh->derivativeNumber());
    max_field_number = max(max_field_number,mesh->meshDimension()+1);
    int degree = face->cellDegree();
    bool interior = true;
    if(face->type() != id_interior)
        interior = false;

    int partition_id_left = face->leftPartitionId();
    int partition_id_right;
    int elt_index_left = face->leftElementIndex();
    int elt_index_right;
    int orientation_left = face->leftOrientation();
    int orientation_right;
    int sense_left = face->leftSense();
    int sense_right;
    bool double_refinement = false;

    bool left_active = true;
    bool right_active = true;
    if(refine_flag->at(elt_index_left) == 1)
        left_active = false;

    igElement *elt_left;
    igElement *elt_right;
    if(face->leftInside())
        elt_left = mesh->element(elt_index_left);

    if(interior){
        partition_id_right = face->rightPartitionId();
        elt_index_right = face->rightElementIndex();
        orientation_right = face->rightOrientation();
        sense_right = face->rightSense();

        if(face->rightInside())
            elt_right = mesh->element(elt_index_right);

        if(refine_flag->at(elt_index_right) == 1)
            right_active = false;

        if(!left_active && !right_active)
            double_refinement = true;
    }

    vector<double> *coord = new vector<double>;
    coord->resize(dimension);

    int index_beg;
    int index_incr;

    int new_id1;
    int new_id2;
    if(interior){
        new_id1 = mesh->faceNumber();
        new_id2 = mesh->faceNumber()+1;
    } else {
        new_id1 = mesh->boundaryFaceNumber();
        new_id2 = mesh->boundaryFaceNumber()+1;
    }


    igFace *new_face1;
    igFace *new_face2;

    //------------------------- first face --------------------

    if(!face->hasChild()){ // case of construction of a new face

        // create new face 1
        new_face1 = new igFace;

        new_face1->setCellId(new_id1);
        new_face1->setPhysicalDimension(dimension);
        new_face1->setParametricDimension(dimension-1);
        new_face1->setDegree(degree);
        new_face1->setGaussPointNumberByDirection(face->gaussPointNumberByDirection());
        new_face1->setVariableNumber(mesh->variableNumber());
        new_face1->setDerivativeNumber(mesh->derivativeNumber());
        new_face1->setType(face->type());
        new_face1->setLeftPartitionId(partition_id_left);
        if(face->leftInside())
            new_face1->setLeftInside();
        if(face->shared())
            new_face1->setShared();

        if(interior){
            new_face1->setRightPartitionId(partition_id_right);
            if(face->rightInside())
                new_face1->setRightInside();
        }
        new_face1->setCoordinates(this->coordinates);
        new_face1->setVelocities(this->velocities);

        new_face1->initializeDegreesOfFreedom();

    } else {

        if(interior){
            new_face1 = mesh->face(face->child(0));
        } else {
            new_face1 = mesh->boundaryFace(face->child(0));
        }
        new_face1->setActive();
    }

    bool sign_left = false;
    if(sense_left<0)
        sign_left = true;
    bool sign_right = false;
    if(sense_right<0)
        sign_right = true;


    // determine left element data
    igElement *elt_left1;
    int new_elt1_left_index = 0;
    if(face->leftInside()){ // if in the current partition
        if(!left_active){
            switch(orientation_left){
            case 0:
                if(sign_left)
                    new_elt1_left_index = elt_left->child(1);
                else
                    new_elt1_left_index = elt_left->child(0);
                break;
            case 1:
                if(sign_left)
                    new_elt1_left_index = elt_left->child(3);
                else
                    new_elt1_left_index = elt_left->child(1);
                break;
            case 2:
                if(sign_left)
                    new_elt1_left_index = elt_left->child(3);
                else
                    new_elt1_left_index = elt_left->child(2);
                break;
            case 3:
                if(sign_left)
                    new_elt1_left_index = elt_left->child(2);
                else
                    new_elt1_left_index = elt_left->child(0);
                break;
            }
            new_face1->setParameterIntervalLeft(0.,1.);
            new_face1->setHalfFaceLeft(false);

        } else {
            new_elt1_left_index = elt_index_left;
            new_face1->setParameterIntervalLeft(0.,0.5);
            new_face1->setHalfFaceLeft(true);
        }
        elt_left1 = mesh->element(new_elt1_left_index);
    } else { // in other partition
        if(!left_active){
            new_face1->setParameterIntervalLeft(0.,1.);
            new_face1->setHalfFaceLeft(false);
        } else {
            new_face1->setParameterIntervalLeft(0.,0.5);
            new_face1->setHalfFaceLeft(true);
        }
        new_elt1_left_index = -1;
    }

    // determine right element data
    igElement *elt_right1;
    int new_elt1_right_index = 0;
    if(face->rightInside()){ // if in the current partition
        if(interior){
            if(!right_active){
                switch(orientation_right){
                case 0:
                    if(sign_right)
                        new_elt1_right_index = elt_right->child(1);
                    else
                        new_elt1_right_index = elt_right->child(0);
                    break;
                case 1:
                    if(sign_right)
                        new_elt1_right_index = elt_right->child(3);
                    else
                        new_elt1_right_index = elt_right->child(1);
                    break;
                case 2:
                    if(sign_right)
                        new_elt1_right_index = elt_right->child(3);
                    else{
                        new_elt1_right_index = elt_right->child(2);
                    }
                    break;
                case 3:
                    if(sign_right)
                        new_elt1_right_index = elt_right->child(2);
                    else
                        new_elt1_right_index = elt_right->child(0);
                    break;
                }
                new_face1->setParameterIntervalRight(0.,1.);
                new_face1->setHalfFaceRight(false);

            } else {
                new_elt1_right_index = elt_index_right;
                new_face1->setParameterIntervalRight(0.,0.5);
                new_face1->setHalfFaceRight(true);
            }
            elt_right1 = mesh->element(new_elt1_right_index);
        }
    } else { // in other partition
        if(interior){
            if(!right_active){
                new_face1->setParameterIntervalRight(0.,1.);
                new_face1->setHalfFaceRight(false);
            } else {
                new_face1->setParameterIntervalRight(0.,0.5);
                new_face1->setHalfFaceRight(true);
            }
            new_elt1_right_index = -1;
        }
    }

    // set left map
    new_face1->setLeftElementIndex(new_elt1_left_index);
    new_face1->setOrientationLeft(orientation_left);
    new_face1->setSenseLeft(sense_left);

    if(face->leftInside()){
        switch(orientation_left){
        case 0:
            index_beg = 0;
            index_incr = 1;
            if(sign_left){
                index_beg = degree;
                index_incr *= -1;
            }
            break;
        case 1:
            index_beg = degree;
            index_incr = degree+1;
            if(sign_left){
                index_beg = (degree+1)*(degree+1)-1;
                index_incr *= -1;
            }
            break;
        case 2:
            index_beg = degree*(degree+1);
            index_incr = 1;
            if(sign_left){
                index_beg = (degree+1)*(degree+1)-1;
                index_incr *= -1;
            }
            break;
        case 3:
            index_beg = 0;
            index_incr = degree+1;
            if(sign_left){
                index_beg = (degree+1)*degree;
                index_incr *= -1;
            }
            break;
        }

        for (int idof=0; idof<face->controlPointNumber(); idof++){
        	for (int ivar=0; ivar<max_field_number; ivar++)
        		new_face1->setLeftDofMap(idof,ivar,elt_left1->globalId(index_beg+index_incr*idof,ivar));
        	new_face1->setLeftPointMap(idof, index_beg+index_incr*idof);
        }
    }


    if(interior){

        // set left map
        new_face1->setRightElementIndex(new_elt1_right_index);
        new_face1->setOrientationRight(orientation_right);
        new_face1->setSenseRight(sense_right);

        if(face->rightInside()){
            switch(orientation_right){
            case 0:
                index_beg = 0;
                index_incr = 1;
                if(sign_right){
                    index_beg = degree;
                    index_incr *= -1;
                }
                break;
            case 1:
                index_beg = degree;
                index_incr = degree+1;
                if(sign_right){
                    index_beg = (degree+1)*(degree+1)-1;
                    index_incr *= -1;
                }
                break;
            case 2:
                index_beg = degree*(degree+1);
                index_incr = 1;
                if(sign_right){
                    index_beg = (degree+1)*(degree+1)-1;
                    index_incr *= -1;
                }
                break;
            case 3:
                index_beg = 0;
                index_incr = degree+1;
                if(sign_right){
                    index_beg = (degree+1)*degree;
                    index_incr *= -1;
                }
                break;
            }

            for (int idof=0; idof<face->controlPointNumber(); idof++){
            	for (int ivar=0; ivar<max_field_number; ivar++)
            		new_face1->setRightDofMap(idof,ivar,elt_right1->globalId(index_beg+index_incr*idof,ivar));
            	new_face1->setRightPointMap(idof, index_beg+index_incr*idof);
            }
        }

    }

    if(!face->shared()){
        new_face1->initializeGeometry();
        for (int idim=0; idim<dimension; idim++)
            coord->at(idim) = elt_left1->barycenter(idim);
        new_face1->initializeNormal(coord);
    }


    //----------------- second face -------------------

    if(!face->hasChild()){ //case of construction of new face

        // create new face 2
        new_face2 = new igFace;

        // generic data
        new_face2->setCellId(new_id2);
        new_face2->setPhysicalDimension(dimension);
        new_face2->setParametricDimension(dimension-1);
        new_face2->setDegree(degree);
        new_face2->setGaussPointNumberByDirection(face->gaussPointNumberByDirection());
        new_face2->setVariableNumber(mesh->variableNumber());
        new_face2->setDerivativeNumber(mesh->derivativeNumber());
        new_face2->setType(face->type());
        new_face2->setLeftPartitionId(partition_id_left);
        if(face->leftInside())
            new_face2->setLeftInside();
        if(face->shared())
            new_face2->setShared();

        if(interior){
            new_face2->setRightPartitionId(partition_id_right);
            if(face->rightInside())
                new_face2->setRightInside();
        }
        new_face2->setCoordinates(this->coordinates);
        new_face2->setVelocities(this->velocities);

        new_face2->initializeDegreesOfFreedom();

    } else {

        if(interior){
            new_face2 = mesh->face(face->child(1));
        } else {
            new_face2 = mesh->boundaryFace(face->child(1));
        }
        new_face2->setActive();
    }

    // determine left element
    igElement *elt_left2;
    int new_elt2_left_index = 0;;
    if(face->leftInside()){ // if in the current partition
        if(!left_active){
            switch(orientation_left){
            case 0:
                if(sign_left)
                    new_elt2_left_index = elt_left->child(0);
                else
                    new_elt2_left_index = elt_left->child(1);
                break;
            case 1:
                if(sign_left)
                    new_elt2_left_index = elt_left->child(1);
                else
                    new_elt2_left_index = elt_left->child(3);
                break;
            case 2:
                if(sign_left)
                    new_elt2_left_index = elt_left->child(2);
                else
                    new_elt2_left_index = elt_left->child(3);
                break;
            case 3:
                if(sign_left)
                    new_elt2_left_index = elt_left->child(0);
                else
                    new_elt2_left_index = elt_left->child(2);
                break;
            }
            new_face2->setParameterIntervalLeft(0.,1.);
            new_face2->setHalfFaceLeft(false);

        } else {
            new_elt2_left_index = elt_index_left;
            new_face2->setParameterIntervalLeft(0.5,1.);
            new_face2->setHalfFaceLeft(true);
        }
        elt_left2 = mesh->element(new_elt2_left_index);
    } else { // in other partition
        if(!left_active){
            new_face2->setParameterIntervalLeft(0.,1.);
            new_face2->setHalfFaceLeft(false);
        } else {
            new_face2->setParameterIntervalLeft(0.5,1.);
            new_face2->setHalfFaceLeft(true);
        }
        new_elt2_left_index = -1;
    }

    // determine right element
    igElement *elt_right2;
    int new_elt2_right_index = 0;
    if(face->rightInside()){ // if in the current partition
        if(interior){
            if(!right_active){
                switch(orientation_right){
                case 0:
                    if(sign_right)
                        new_elt2_right_index = elt_right->child(0);
                    else
                        new_elt2_right_index = elt_right->child(1);
                    break;
                case 1:
                    if(sign_right)
                        new_elt2_right_index = elt_right->child(1);
                    else
                        new_elt2_right_index = elt_right->child(3);
                    break;
                case 2:
                    if(sign_right)
                        new_elt2_right_index = elt_right->child(2);
                    else
                        new_elt2_right_index = elt_right->child(3);
                    break;
                case 3:
                    if(sign_right)
                        new_elt2_right_index = elt_right->child(0);
                    else
                        new_elt2_right_index = elt_right->child(2);
                    break;
                }
                new_face2->setParameterIntervalRight(0.,1.);
                new_face2->setHalfFaceRight(false);

            } else {
                new_elt2_right_index = elt_index_right;
                new_face2->setParameterIntervalRight(0.5,1.);
                new_face2->setHalfFaceRight(true);
            }
            elt_right2 = mesh->element(new_elt2_right_index);
        }
    } else { // in other partition
        if(interior){
            if(!right_active){
                new_face2->setParameterIntervalRight(0.,1.);
                new_face2->setHalfFaceRight(false);
            } else {
                new_face2->setParameterIntervalRight(0.5,1.);
                new_face2->setHalfFaceRight(true);
            }
            new_elt2_right_index = -1;
        }
    }

    // set left map
    new_face2->setLeftElementIndex(new_elt2_left_index);
    new_face2->setOrientationLeft(orientation_left);
    new_face2->setSenseLeft(sense_left);

    if(face->leftInside()){
        switch(orientation_left){
        case 0:
            index_beg = 0;
            index_incr = 1;
            if(sign_left){
                index_beg = degree;
                index_incr *= -1;
            }
            break;
        case 1:
            index_beg = degree;
            index_incr = degree+1;
            if(sign_left){
                index_beg = (degree+1)*(degree+1)-1;
                index_incr *= -1;
            }
            break;
        case 2:
            index_beg = degree*(degree+1);
            index_incr = 1;
            if(sign_left){
                index_beg = (degree+1)*(degree+1)-1;
                index_incr *= -1;
            }
            break;
        case 3:
            index_beg = 0;
            index_incr = degree+1;
            if(sign_left){
                index_beg = (degree+1)*degree;
                index_incr *= -1;
            }
            break;
        }

        for (int idof=0; idof<face->controlPointNumber(); idof++){
        	for (int ivar=0; ivar<max_field_number; ivar++)
        		new_face2->setLeftDofMap(idof,ivar,elt_left2->globalId(index_beg+index_incr*idof,ivar));
        	new_face2->setLeftPointMap(idof, index_beg+index_incr*idof);
        }
    }

    // set right map
    if(interior){

        new_face2->setRightElementIndex(new_elt2_right_index);
        new_face2->setOrientationRight(orientation_right);
        new_face2->setSenseRight(sense_right);

        if(face->rightInside()){
            switch(orientation_right){
            case 0:
                index_beg = 0;
                index_incr = 1;
                if(sign_right){
                    index_beg = degree;
                    index_incr *= -1;
                }
                break;
            case 1:
                index_beg = degree;
                index_incr = degree+1;
                if(sign_right){
                    index_beg = (degree+1)*(degree+1)-1;
                    index_incr *= -1;
                }
                break;
            case 2:
                index_beg = degree*(degree+1);
                index_incr = 1;
                if(sign_right){
                    index_beg = (degree+1)*(degree+1)-1;
                    index_incr *= -1;
                }
                break;
            case 3:
                index_beg = 0;
                index_incr = degree+1;
                if(sign_right){
                    index_beg = (degree+1)*degree;
                    index_incr *= -1;
                }
                break;
            }

            for (int idof=0; idof<face->controlPointNumber(); idof++){
            	for (int ivar=0; ivar<max_field_number; ivar++)
            		new_face2->setRightDofMap(idof,ivar,elt_right2->globalId(index_beg+index_incr*idof,ivar));
            	new_face2->setRightPointMap(idof, index_beg+index_incr*idof);
            }
        }

    }

    if(!face->shared()){
        new_face2->initializeGeometry();
        for (int idim=0; idim<dimension; idim++)
            coord->at(idim) = elt_left2->barycenter(idim);
        new_face2->initializeNormal(coord);
    }

    // finalize new faces
    if(!double_refinement){
        new_face1->setChildRank(1);
        new_face2->setChildRank(2);
    } else {
        new_face1->setChildRank(0);
        new_face2->setChildRank(0);
    }

    if(!face->hasChild()){

        new_face1->setParent(face->cellId());
        new_face2->setParent(face->cellId());

        face->setChild(0, new_face1->cellId());
        face->setChild(1, new_face2->cellId());

        // storage in the face list
        if(interior){
            mesh->addFace(new_face1);
            mesh->addFace(new_face2);
            mesh->computeFaceNumber();
        } else {
            mesh->addBoundaryFace(new_face1);
            mesh->addBoundaryFace(new_face2);
            mesh->computeBoundaryFaceNumber();
        }

    }

    // set interface properties to new faces
    if(face->type() == id_slip_wall_fsi || face->type() == id_noslip_wall_fsi){
        new_face1->initializeInterfaceProperties(face);
        new_face2->initializeInterfaceProperties(face);
    }

    face->setInactive();

    delete coord;
    return;

}

/////////////////////////////////////////////////////////////////////////////////////


void igMeshRefinerIsotropic::addMidFaces(igElement *elt)
{
    IG_ASSERT(elt, "no element");

    for(int ifac=0; ifac<4; ifac++){

        this->buildMidFace(elt, ifac);
    }
}

void igMeshRefinerIsotropic::buildMidFace(igElement *elt, int face_index)
{

    IG_ASSERT(elt, "no element");

    int dimension = elt->physicalDimension();
    vector<double> *coord = new vector<double>;
    coord->resize(dimension);

    int max_field_number = max(mesh->variableNumber(),mesh->derivativeNumber());
    max_field_number = max(max_field_number,mesh->meshDimension()+1);
    int new_elt_index;
    int degree = elt->cellDegree();
    int partition_id = elt->partition();

    igFace *new_face;

    bool construct_face = true;
    if(mesh->element(elt->child(0))->cellId() < this->previous_element_number)
        construct_face = false;

    if(construct_face){ // case of construction of a new face

        // create new face at mid element
        new_face = new igFace;

        new_face->setCellId(mesh->faceNumber());
        new_face->setPhysicalDimension(dimension);
        new_face->setParametricDimension(dimension-1);
        new_face->setDegree(degree);
        new_face->setGaussPointNumberByDirection(elt->gaussPointNumberByDirection());
        new_face->setVariableNumber(mesh->variableNumber());
        new_face->setDerivativeNumber(mesh->derivativeNumber());
        new_face->setType(0);
        new_face->setLeftPartitionId(partition_id);
        new_face->setRightPartitionId(partition_id);
        new_face->setLeftInside();
        new_face->setRightInside();
        new_face->setCoordinates(this->coordinates);
        new_face->setVelocities(this->velocities);

        new_face->initializeDegreesOfFreedom();

    } else {

        int face_id;
        int elt_left_id;
        int elt_right_id;

        switch(face_index){
        case 0:
            elt_left_id=elt->child(0);
            elt_right_id=elt->child(1);
            break;
        case 1:
            elt_left_id=elt->child(0);
            elt_right_id=elt->child(2);
            break;
        case 2:
            elt_left_id=elt->child(1);
            elt_right_id=elt->child(3);
            break;
        case 3:
            elt_left_id=elt->child(2);
            elt_right_id=elt->child(3);
            break;
        }

        for (int ifac=0; ifac<mesh->faceNumber(); ifac++){
            igFace *face_current = mesh->face(ifac);
            if(face_current->leftElementIndex() == elt_left_id && face_current->rightElementIndex() == elt_right_id)
                face_id = face_current->cellId();
        }

        new_face = mesh->face(face_id);
        new_face->setActive();
    }

    int elt_left_index;
    int elt_right_index;
    int orientation_left;
    int orientation_right;
    int index_beg_left;
    int index_incr_left;
    int index_beg_right;
    int index_incr_right;

    switch(face_index){
    case 0:
        elt_left_index=elt->child(0);
        elt_right_index = elt->child(1);
        orientation_left = 1;
        orientation_right = 3;
        index_beg_left = degree;
        index_incr_left = degree+1;
        index_beg_right = 0;
        index_incr_right = degree+1;
        break;
    case 1:
        elt_left_index=elt->child(0);
        elt_right_index = elt->child(2);
        orientation_left = 2;
        orientation_right = 0;
        index_beg_left = degree*(degree+1);
        index_incr_left = 1;
        index_beg_right = 0;
        index_incr_right = 1;
        break;
    case 2:
        elt_left_index=elt->child(1);
        elt_right_index = elt->child(3);
        orientation_left = 2;
        orientation_right = 0;
        index_beg_left = degree*(degree+1);
        index_incr_left = 1;
        index_beg_right = 0;
        index_incr_right = 1;
        break;
    case 3:
        elt_left_index=elt->child(2);
        elt_right_index = elt->child(3);
        orientation_left = 1;
        orientation_right = 3;
        index_beg_left = degree;
        index_incr_left = degree+1;
        index_beg_right = 0;
        index_incr_right = degree+1;
        break;
    }

    igElement *elt_left = mesh->element(elt_left_index);
    igElement *elt_right = mesh->element(elt_right_index);

    new_face->setLeftElementIndex(elt_left_index);
    new_face->setOrientationLeft(orientation_left);
    new_face->setSenseLeft(1);
    for (int idof=0; idof<new_face->controlPointNumber(); idof++){
    	for (int ivar=0; ivar<max_field_number; ivar++)
    		new_face->setLeftDofMap(idof,ivar,elt_left->globalId(index_beg_left+idof*index_incr_left,ivar));
    	new_face->setLeftPointMap(idof, index_beg_left+idof*index_incr_left);
    }
    new_face->setRightElementIndex(elt_right_index);
    new_face->setOrientationRight(orientation_right);
    new_face->setSenseRight(1);
    for (int idof=0; idof<new_face->controlPointNumber(); idof++){
    	for (int ivar=0; ivar<max_field_number; ivar++)
    		new_face->setRightDofMap(idof,ivar,elt_right->globalId(index_beg_right+idof*index_incr_right,ivar));
    	new_face->setRightPointMap(idof, index_beg_right+idof*index_incr_right);
    }

    for (int idim=0; idim<dimension; idim++)
        coord->at(idim) = elt_left->barycenter(idim);

    new_face->initializeGeometry();
    new_face->initializeNormal(coord);

    // storage in the face list
    if(construct_face){
       mesh->addFace(new_face);
       mesh->computeFaceNumber();
   }

}


/////////////////////////////////////////////////////////////////////////////

void igMeshRefinerIsotropic::updateMap(igFace *face, bool left)
{
    IG_ASSERT(face, "no face");
    int max_field_number = max(mesh->variableNumber(),mesh->derivativeNumber());
    max_field_number = max(max_field_number,mesh->meshDimension()+1);
    int ctrl_pts_per_elt = mesh->element(0)->controlPointNumber();

    bool interior = true;
    if(face->type() != id_interior)
        interior = false;

    for (int ivar=0; ivar<max_field_number; ivar++){
        for (int idof=0; idof<face->controlPointNumber(); idof++){

            if(left){
                if(face->leftInside()){
                    int previous_id = face->dofLeft(idof,ivar);
                    int new_id = previous_id + (added_elt_number*ctrl_pts_per_elt + added_shared_dof_number)*ivar;
                    face->setLeftDofMap(idof,ivar,new_id);
                }
            } else if(interior){
                if(face->rightInside()){
                    int previous_id = face->dofRight(idof,ivar);
                    int new_id = previous_id + (added_elt_number*ctrl_pts_per_elt + added_shared_dof_number)*ivar;
                    face->setRightDofMap(idof,ivar,new_id);
                }
            }
        }
    }

}

/////////////////////////////////////////////////////////////////////////////////

void igMeshRefinerIsotropic::updateHalfFace(igFace *face, bool left)
{
    IG_ASSERT(face, "no face");
    igElement *elt;
    int orientation;
    int sense;

    // check if element is in my partition
    bool elt_inside = false;
    if(left && face->leftInside())
        elt_inside = true;
    if(!left && face->rightInside())
        elt_inside = true;

    if(elt_inside){

        if(left){
            elt = mesh->element(face->leftElementIndex());
            orientation = face->leftOrientation();
            sense = face->leftSense();
        }
        else{
            elt = mesh->element(face->rightElementIndex());
            orientation = face->rightOrientation();
            sense = face->rightSense();
        }

        int dimension = elt->physicalDimension();

        int max_field_number = max(mesh->variableNumber(),mesh->derivativeNumber());
        max_field_number = max(max_field_number,mesh->meshDimension()+1);
        int new_elt_index;
        int degree = elt->cellDegree();
        igElement *new_elt;

        bool sign = false;
        if(sense<0)
            sign = true;

        int facerank = face->childRank();

        // update element index according to child rank
        switch(orientation){

        case 0:
            if(sign){
                if(facerank == 1)
                    new_elt_index = elt->child(1);
                else
                    new_elt_index = elt->child(0);
            } else {
                if(facerank == 1)
                    new_elt_index = elt->child(0);
                else
                    new_elt_index = elt->child(1);
            }
        break;
        case 1:
            if(sign){
                if(facerank == 1)
                    new_elt_index = elt->child(3);
                else
                    new_elt_index = elt->child(1);
            } else {
                if(facerank == 1)
                    new_elt_index = elt->child(1);
                else
                    new_elt_index = elt->child(3);
            }
        break;
        case 2:
            if(sign){
                if(facerank == 1)
                    new_elt_index = elt->child(3);
                else
                    new_elt_index = elt->child(2);
            } else {
                if(facerank == 1)
                    new_elt_index = elt->child(2);
                else
                    new_elt_index = elt->child(3);
            }
        break;
        case 3:
            if(sign){
                if(facerank == 1)
                    new_elt_index = elt->child(2);
                else
                    new_elt_index = elt->child(0);
            } else {
                if(facerank == 1)
                    new_elt_index = elt->child(0);
                else
                    new_elt_index = elt->child(2);
            }
        break;
        }

        if(left)
            face->setLeftElementIndex(new_elt_index);
        else
            face->setRightElementIndex(new_elt_index);
        new_elt = mesh->element(new_elt_index);

        int index_beg;
        int index_incr;

        if(orientation == 0){
            index_beg = 0;
            index_incr = 1;
            if(sign){
                index_beg = degree;
                index_incr *= -1;
            }
        } else if(orientation == 1){
            index_beg = degree;
            index_incr = degree+1;
            if(sign){
                index_beg = (degree+1)*(degree+1)-1;
                index_incr *= -1;
            }
        } else if(orientation == 2){
            index_beg = degree*(degree+1);
            index_incr = 1;
            if(sign){
                index_beg = (degree+1)*(degree+1)-1;
                index_incr *= -1;
            }
        } else {
            index_beg = 0;
            index_incr = degree+1;
            if(sign){
                index_beg = (degree+1)*degree;
                index_incr *= -1;
            }
        }

        for (int idof=0; idof<face->controlPointNumber(); idof++){
    	    for (int ivar=0; ivar<max_field_number; ivar++){
    		    if(left){
    		        face->setLeftDofMap(idof,ivar,new_elt->globalId(index_beg+index_incr*idof,ivar));
    			    face->setLeftPointMap(idof, index_beg+index_incr*idof);
    		    }else{
    			    face->setRightDofMap(idof,ivar,new_elt->globalId(index_beg+index_incr*idof,ivar));
    			    face->setRightPointMap(idof, index_beg+index_incr*idof);
    		    }
    	    }
        }
    }

    // correct the rank
    face->setChildRank(0);
    face->setHalfFaceLeft(false);
    face->setHalfFaceRight(false);

    // correct the parameter interval
    if(left)
        face->setParameterIntervalLeft(0.,1.);
    else
        face->setParameterIntervalRight(0.,1.);

    if(!face->shared())
        face->evaluateGaussPoints();

    return;
}

/////////////////////////////////////////////////////////////////////////////

void igMeshRefinerIsotropic::checkRefinement(void)
{
    // by default all elements can be refined
    this->elt_refinable->assign(mesh->elementNumber()+mesh->sharedElementNumber(),0);

    for (int ifac=0; ifac<mesh->faceNumber(); ifac++){

        igFace *face = mesh->face(ifac);

        if(face->isActive()){

            int left_index = face->leftElementIndex();
            int right_index = face->rightElementIndex();

            int level_left = mesh->elementLevel(left_index,0);
            int level_right = mesh->elementLevel(right_index,0);

            if(level_left > level_right){
                if(face->leftInside())
                    elt_refinable->at(left_index) = 1;
                }
            if(level_left < level_right){
                if(face->rightInside())
                    elt_refinable->at(right_index) = 1;
                }
        }
    }

    //remove error outside the adaption box
    for(int iel=0; iel<mesh->elementNumber(); iel++){

        igElement *elt = mesh->element(iel);

        if(elt->barycenter(0) < amr_box->at(0) ||
           elt->barycenter(0) > amr_box->at(1) ||
           elt->barycenter(1) < amr_box->at(2) ||
           elt->barycenter(1) > amr_box->at(3) ){

            elt_refinable->at(iel) = 1;
        }
    }

    return;
}

/////////////////////////////////////////////////////////////////////////////

void igMeshRefinerIsotropic::propagateRefinement(void)
{

    for (int ifac=0; ifac<mesh->faceNumber(); ifac++){

        igFace *face = mesh->face(ifac);

        if(face->isActive()){

            int left_index = face->leftElementIndex();
            int right_index = face->rightElementIndex();

            int level_left = mesh->elementLevel(left_index,0);
            int level_right = mesh->elementLevel(right_index,0);

            if(level_left > level_right && blocking_flag->at(left_index) == 1){ // blocked for left
                if(elt_refinable->at(right_index) == 0 && refine_flag->at(right_index) == 0){
                    if(face->rightInside()){
                        refine_flag->at(right_index) = 1;
                        if(!(mesh->element(right_index)->hasChild()))
                            this->added_elt_number += 4;
                    }
                }
            }

            if(level_right > level_left && blocking_flag->at(right_index) == 1){ // blocked for right
                if(elt_refinable->at(left_index) == 0 && refine_flag->at(left_index) == 0){
                    if(face->leftInside()){
                        refine_flag->at(left_index) = 1;
                        if(!(mesh->element(left_index)->hasChild()))
                            this->added_elt_number += 4;
                    }
                }
            }

        }
    }

    return;
}

////////////////////////////////////////////////////////////////////////////////////////////////

void igMeshRefinerIsotropic::computeSharedDataNumber(void)
{
    this->new_shared_elt_number = 0;
    this->new_shared_dof_number = 0;

    int face_dof = mesh->face(0)->controlPointNumber();

    for (int ifac=0; ifac<mesh->faceNumber(); ifac++){

        igFace *face = mesh->face(ifac);

        if( face->shared() && face->isActive() ){

            int left_elt_index = face->leftElementIndex();
            int right_elt_index = face->rightElementIndex();

            if(face->leftInside()){

                if(face->halfFaceRight()){
                    if(refine_flag->at(right_elt_index) == 1)
                        this->new_shared_elt_number += 1;
                    else if(face->childRank() == 1)
                        this->new_shared_elt_number += 1;
                } else {
                    if(refine_flag->at(right_elt_index) == 1)
                        this->new_shared_elt_number += 2;
                    else
                        this->new_shared_elt_number += 1;
                }

            } else {

                if(face->halfFaceLeft()){
                    if(refine_flag->at(left_elt_index) == 1)
                        this->new_shared_elt_number += 1;
                    else if(face->childRank() == 1)
                        this->new_shared_elt_number += 1;
                } else {
                    if(refine_flag->at(left_elt_index) == 1)
                        this->new_shared_elt_number += 2;
                    else
                        this->new_shared_elt_number += 1;
                }

            }

        }
    }

    this->new_shared_dof_number = this->new_shared_elt_number * face_dof;

    this->added_shared_dof_number = this->new_shared_dof_number - mesh->sharedControlPointNumber();
    this->added_shared_elt_number = this->new_shared_elt_number - mesh->elementNumber();

}
