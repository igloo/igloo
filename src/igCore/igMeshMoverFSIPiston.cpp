/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igMeshMoverFSIPiston.h"

#include "igMesh.h"
#include "igCell.h"
#include "igElement.h"
#include "igFace.h"
#include "igInterface.h"

#include <igAssert.h>

#include <igGenerator/igMeshMovementFSIPiston.h>
#include <igDistributed/igCommunicator.h>

#include <iostream>
#include <stdlib.h>
#include <cmath>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igMeshMoverFSIPiston::igMeshMoverFSIPiston(igMesh *mesh, vector<double> *coordinates, vector<double> *velocities) : igMeshMoverFSI(mesh,coordinates,velocities)
{
	IG_ASSERT(mesh, "no mesh");

    // damping coefficients for level 0

    this->damping = new vector<double>;
    this->damping->resize(mesh->controlPointNumber(),0);

    for (int iel = 0; iel < mesh->elementNumber(); ++iel){
        igElement *elt = mesh->element(iel);
        for(int idof=0; idof<elt->controlPointNumber(); idof++){
            this->damping->at(elt->globalId(idof,0)) = this->coordinates->at(elt->globalId(idof,0));
        }
    }

}

igMeshMoverFSIPiston::~igMeshMoverFSIPiston(void)
{
    delete this->damping;
}

/////////////////////////////////////////////////////////////////////////////

void igMeshMoverFSIPiston::initializeMovement(const string& integrator, const string& movement_type,  vector<double> *param_ALE, int refine_max_level)
{
    this->movement_law = movement_type;
    this->movement_param = param_ALE;

    this->mesh_movement_law = pistonDeformation;

    this->refine_max_level = refine_max_level;
    if(refine_max_level == 0)
        this->adaptive_mesh = false;
    else
        this->adaptive_mesh = true;

    movement_param->resize(3);

    return;
}

/////////////////////////////////////////////////////////////////////////////


void igMeshMoverFSIPiston::retrieveParameters(int iel,int icp)
{
    int index = iel * dof_number_per_elt + icp;

    movement_param->at(0) = damping->at(index); // damping coefficient
    movement_param->at(1) = interface_velocities_ref->at(0) * (1-fractional_step) + interface_velocities->at(0) * fractional_step; // velocity along x
    movement_param->at(2) = interface_displacements_ref->at(0) * (1-fractional_step) + interface_displacements->at(0) * fractional_step; // translation along x

}
