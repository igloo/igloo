/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include <iostream>
#include <stdlib.h>
#include <igAssert.h>


#include "igMeshCoarsenerIsotropic.h"

#include "igMesh.h"
#include "igCell.h"
#include "igElement.h"
#include "igFace.h"
#include "igFittingRefinedSurface.h"
#include "igBasisBezier.h"

#include <igDistributed/igCommunicator.h>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igMeshCoarsenerIsotropic::igMeshCoarsenerIsotropic(void)
{
    this->state = nullptr;
    this->mesh = nullptr;
    this->error_xi = nullptr;
    this->error_eta = nullptr;
    this->elt_coarsenable = nullptr;
    this->coarsening_flag = nullptr;
    this->communicator = nullptr;
}

igMeshCoarsenerIsotropic::~igMeshCoarsenerIsotropic(void)
{
    if(this->elt_coarsenable)
        delete this->elt_coarsenable;
    if(this->coarsening_flag)
        delete this->coarsening_flag;

}


/////////////////////////////////////////////////////////////////////////////

void igMeshCoarsenerIsotropic::initialize(igMesh *mesh, vector<double> *state, vector<double> *coordinates, vector<double> *velocities)
{
    IG_ASSERT(mesh, "no mesh");
    IG_ASSERT(state, "no state");

    this->mesh = mesh;
    this->state = state;
    this->coordinates = coordinates;
    this->velocities = velocities;

    coarsening_step = 0;

    this->elt_coarsenable = new vector<int>();
    this->coarsening_flag = new vector<int>();

    return;
}

void igMeshCoarsenerIsotropic::setCommunicator(igCommunicator *communicator)
{
    IG_ASSERT(communicator, "no communicator");

    this->communicator = communicator;
}

void igMeshCoarsenerIsotropic::setError(vector<double> *error_xi, vector<double> *error_eta)
{
    IG_ASSERT(error_xi, "no error_xi");
    IG_ASSERT(error_eta, "no error_eta");

    this->error_xi = error_xi;
    this->error_eta = error_eta;
}

void igMeshCoarsenerIsotropic::setCoarseningCoefficient(double coef)
{
    this->coarsening_coef = coef;
    return;
}

/////////////////////////////////////////////////////////////////////////////


void igMeshCoarsenerIsotropic::select(void)
{
    if(communicator->rank() == 0){
        cout << endl;
        cout << ">>> mesh coarsening process: selection" << endl;
    }

    coarsening_flag->assign(mesh->elementNumber()+mesh->sharedElementNumber(),0);
    this->removed_elt_number= 0;

    // check if the element can be coarsen according to the neighbors
    this->checkCoarsening();

    // first loop over elements to compute global error
    double local_error = 0.;
    for (int iel=0; iel<mesh->elementNumber(); iel++){
        igElement *elt = mesh->element(iel);
        coarsening_flag->at(iel) = 0;
        if(elt->isActive())
            local_error += error_xi->at(iel) + error_eta->at(iel);
    }
    // MPI communication
    double mean_error = communicator->distributeError(local_error);

    mean_error /= mesh->totalActiveElementNumber();

    // second loop over elements to select
    for (int iel=0; iel<mesh->elementNumber(); iel++){

        igElement *elt = mesh->element(iel);

        if(elt->isActive()){

            // check if element has been refined previously
            if(mesh->elementLevel(iel,0) > 0){

                int iel_parent = elt->parent();
                igElement *elt_parent = mesh->element(iel_parent);

                bool all_brothers_ok = true;
                for(int ibrother=0; ibrother<4; ibrother++){

                    if(!mesh->element(elt_parent->child(ibrother))->isActive())
                        all_brothers_ok = false;
                    if(elt_coarsenable->at(elt_parent->child(ibrother)) != 0)
                        all_brothers_ok = false;
                }

                if(all_brothers_ok){

                    double coarsening_ratio = 0;
                    for(int ibrother=0; ibrother<4; ibrother++){

                        coarsening_ratio = max( coarsening_ratio,
                                (error_xi->at(elt_parent->child(ibrother))+error_eta->at(elt_parent->child(ibrother)))/mean_error);
                    }
                    if( coarsening_ratio < coarsening_coef && coarsening_flag->at(iel) == 0) {
                        for(int ibrother=0; ibrother<4; ibrother++){
                            coarsening_flag->at(elt_parent->child(ibrother)) = 1;
                        }
                        this->removed_elt_number += 4;
                    }

                } // check brothers
            } // check level
        }// check active
    }  // loop

    // MPI communication of coarsening flag array
    communicator->distributeElementArrayInt(coarsening_flag);

    int total_removed_elt_number = communicator->addInt(removed_elt_number);

    if(communicator->rank() == 0){
        cout << "Mean error: " << mean_error << endl;
        cout << "Number of elements to coarsen: " << total_removed_elt_number<< endl;
        cout << ">>> selection done" << endl;
        cout << endl;
    }

    return;
}

///////////////////////////////////////////////////////////////////////

void igMeshCoarsenerIsotropic::coarsen(void)
{
    if(communicator->rank() == 0){
        cout << ">>> mesh coarsening process: coarsening" << endl;
    }

    int previous_element_number = mesh->elementNumber();
    int previous_face_number = mesh->faceNumber();
    int previous_bnd_number = mesh->boundaryFaceNumber();
    int ctrl_pts_per_elt = mesh->element(0)->controlPointNumber();

    int variable_number = mesh->variableNumber();
    int old_state_size = state->size();
    int max_field_number = max(variable_number,mesh->derivativeNumber());
    max_field_number = max(max_field_number,mesh->meshDimension()+1);


    //--------------- coarsening of elements ---------------

    //-------- loop over all existing elements
    for (int iel=0; iel<mesh->elementNumber(); iel++){

        igElement *elt = mesh->element(iel);

        if(elt->isActive()){
            // check for coarseing flag
            if(coarsening_flag->at(iel) == 1){

                this->coarsenElement(elt, state);

            }
        }
    }

    //------------- coarsening of faces --------------------------

    // first : make mid-faces inactive
    for (int ifac=0; ifac<mesh->faceNumber(); ifac++){

        igFace *face = mesh->face(ifac);
        if(face->isActive()){

            int left_elt_id = face->leftElementIndex();
            int right_elt_id = face->rightElementIndex();

            if(face->leftInside() && face->rightInside()){

                igElement *left_elt = mesh->element(left_elt_id);
                igElement *right_elt = mesh->element(right_elt_id);

                int left_flag = coarsening_flag->at(left_elt_id);
                int right_flag = coarsening_flag->at(right_elt_id);

                if(left_flag == 1 && right_flag == 1){

                    bool brother = this->checkBrotherhood(left_elt, right_elt);

                    if(brother)
                        this->coarsenMidFace(face);
                }
            }

        }
    }

    // second : check for face coarsening and update of internal dof
    for (int ifac=0; ifac<mesh->faceNumber(); ifac++){

        igFace *face = mesh->face(ifac);

        if(face->isActive()){

            int left_elt_id = face->leftElementIndex();
            int right_elt_id = face->rightElementIndex();

            int left_flag = coarsening_flag->at(left_elt_id);
            int right_flag = coarsening_flag->at(right_elt_id);

            bool coarsen_left = false;
            bool coarsen_right = false;

            int left_active_id;
            int right_active_id;


            // check coarsening / update configurations for left elt
            if(left_flag == 1){
                coarsen_left = true;
                if(face->leftInside())
                    left_active_id = mesh->element(left_elt_id)->parent();
            } else {
                left_active_id = left_elt_id;
            }

            // check coarsening configurations for right elt
            if(right_flag == 1){
                coarsen_right = true;
                if(face->rightInside())
                    right_active_id = mesh->element(right_elt_id)->parent();
            } else {
                right_active_id = right_elt_id;
            }

            // coarsening of the face
            if( (face->halfFaceLeft() && coarsen_right) ||
                (face->halfFaceRight() && coarsen_left) ||
                (coarsen_left && coarsen_right) ){

                this->coarsenFace(face);

            }

            // update of face maps (when the face is not coarsen)
            if(face->isActive()){
                if(coarsen_left && !coarsen_right){
                    if(face->leftInside())
                        this->updateFace(face, left_active_id, true);
                    this->updateHalfFace(face,true);
                }
                if(coarsen_right && !coarsen_left){
                    if(face->rightInside())
                        this->updateFace(face, right_active_id, false);
                    this->updateHalfFace(face,false);
                }
            }

        }
    }

    //------------- coarsening of boundary faces --------------------------

    for (int ifac=0; ifac<mesh->boundaryFaceNumber(); ifac++){

        igFace *face = mesh->boundaryFace(ifac);
        if(face->isActive()){

            int left_elt_id = face->leftElementIndex();
            igElement *left_elt = mesh->element(left_elt_id);

            int left_flag = coarsening_flag->at(left_elt_id);

            bool coarsen_left = false;
            int left_active_id;

            // check coarsening / update configurations for left elt
            if(left_flag == 1){
                coarsen_left = true;
                left_active_id = left_elt->parent();
            }

            // face coarsening
            if(coarsen_left){

                this->coarsenFace(face);

                igFace *face_parent = mesh->boundaryFace(face->parent());
                this->updateFace(face_parent, left_active_id, true);
            }

        }
    }

    mesh->updateMeshProperties();
    mesh->updateGlobalMeshProperties();

    //------------------ update states and coordinates arrays ----------------------------

    int old_shared_dof_number = mesh->sharedControlPointNumber();

    this->computeSharedDataNumber();

    int new_state_size = ( previous_element_number*ctrl_pts_per_elt + new_shared_dof_number ) *variable_number;
    int new_coord_size = ( previous_element_number*ctrl_pts_per_elt + new_shared_dof_number ) *(mesh->meshDimension()+1);
    int new_vel_size   = ( previous_element_number*ctrl_pts_per_elt + new_shared_dof_number ) *mesh->meshDimension();

    this->added_shared_dof_number = new_shared_dof_number - old_shared_dof_number;

    //------------- fill temporary new state vector and update global Id map ---------------

    vector<double> *state_tmp = new vector<double>;
    state_tmp->resize(state->size(),0.);

    vector<double> *coordinates_tmp = new vector<double>;
    coordinates_tmp->resize(coordinates->size(),0.);

    vector<double> *velocities_tmp = new vector<double>;
    velocities_tmp->resize(velocities->size(),0.);

    // fill temporary arrays
    for(int i=0; i<state->size(); i++){
        state_tmp->at(i) = state->at(i);
    }

    for(int i=0; i<coordinates->size(); i++){
        coordinates_tmp->at(i) = coordinates->at(i);
    }

    for(int i=0; i<velocities->size(); i++){
        velocities_tmp->at(i) = velocities->at(i);
    }

    // fill new state, coordinates and velocities
    state->resize(new_state_size);
    coordinates->resize(new_coord_size);
    velocities->resize(new_vel_size);

    int current_index = 0;
    int current_index_new = 0;
    for(int ivar=0; ivar<max_field_number; ivar++){
        for(int iel=0; iel<previous_element_number; iel++){
            for(int ipt=0; ipt<ctrl_pts_per_elt; ipt++){
                if(ivar<variable_number)
                    state->at(current_index_new) = state_tmp->at(current_index);
                if(ivar<mesh->meshDimension()+1)
                    coordinates->at(current_index_new) = coordinates_tmp->at(current_index);
                if(ivar<mesh->meshDimension())
                    velocities->at(current_index_new) = velocities_tmp->at(current_index);
                mesh->element(iel)->setGlobalId(ipt, ivar, current_index_new);
                current_index++;
                current_index_new++;
            }
        }
        current_index += old_shared_dof_number;
        current_index_new += new_shared_dof_number;
    }

    // update maps
    for (int ifac=0; ifac<mesh->faceNumber(); ifac++){
        igFace *face = mesh->face(ifac);
        this->updateFaceMap(face, true);
        this->updateFaceMap(face, false);
    }
    for (int ifac=0; ifac<mesh->boundaryFaceNumber(); ifac++){
        igFace *face = mesh->boundaryFace(ifac);
        this->updateFaceMap(face, true);
    }

    // refresh communication maps and shared dof maps
    mesh->refreshCommunicatorMaps();

    // copy level arrays
    mesh->refreshLevelArray(previous_element_number,new_shared_elt_number);

    // MPI communication for level array
    mesh->distributeLevelArray();

    // MPI communication for coordinates and velocities and update of shared faces
    communicator->distributeField(coordinates, mesh->meshDimension()+1);
    communicator->distributeField(velocities, mesh->meshDimension());


    //update for faces (necessary for ALE)
    for(int ifac = 0; ifac < mesh->faceNumber(); ifac++){
        igFace *face = mesh->face(ifac);
        if(face->isActive()){
            face->initializeGeometry();
            face->updateNormal();
        }
    }
    for (int ifac = 0; ifac < mesh->boundaryFaceNumber(); ++ifac) {
        igFace *face = mesh->boundaryFace(ifac);
        if(face->isActive()){
            face->initializeGeometry();
            face->updateNormal();
        }
    }

    //--------------------------------------------------------

    int total_previous_element_number = this->communicator->addInt(previous_element_number);
    int total_previous_face_number = this->communicator->addInt(previous_face_number);
    int total_previous_bnd_number = this->communicator->addInt(previous_bnd_number);

    if(communicator->rank() == 0){
        cout << "Number of elements - previous: " << total_previous_element_number << ", new: " << mesh->totalElementNumber() << ", active: " << mesh->totalActiveElementNumber()  << endl;
        cout << "Number of faces - previous: " << total_previous_face_number << ", new: " << mesh->totalFaceNumber() << ", active: " << mesh->totalActiveFaceNumber() << endl;
        cout << "Number of boundary faces - previous: " << total_previous_bnd_number << ", new: " << mesh->totalBoundaryFaceNumber() << ", active: " << mesh->totalActiveBoundaryFaceNumber() << endl;
        cout << "Number of active DOFs: " << mesh->totalActiveElementNumber()*ctrl_pts_per_elt << endl;
        cout << ">>> coarsening done" << endl;
        cout << endl;
    }

    delete state_tmp;
    delete coordinates_tmp;
    delete velocities_tmp;

    return;
}

/////////////////////////////////////////////////////////////////////////////

void igMeshCoarsenerIsotropic::updateFaceMap(igFace *face, bool left)
{
    IG_ASSERT(face, "no face");
    int max_field_number = max(mesh->variableNumber(),mesh->derivativeNumber());
    max_field_number = max(max_field_number,mesh->meshDimension()+1);
    int ctrl_pts_per_elt = mesh->element(0)->controlPointNumber();

    bool interior = true;
    if(face->type() != 0)
        interior = false;

    for (int ivar=0; ivar<max_field_number; ivar++){
        for (int idof=0; idof<face->controlPointNumber(); idof++){

            if(left){
                if(face->leftInside()){
                    int previous_id = face->dofLeft(idof,ivar);
                    int new_id = previous_id + added_shared_dof_number*ivar;
                    face->setLeftDofMap(idof,ivar,new_id);
                }
            } else if(interior){
                if(face->rightInside()){
                    int previous_id = face->dofRight(idof,ivar);
                    int new_id = previous_id + added_shared_dof_number*ivar;
                    face->setRightDofMap(idof,ivar,new_id);
                }
            }
        }
    }

}

//////////////////////////////////////////////////////


void igMeshCoarsenerIsotropic::coarsenElement(igElement *elt, vector<double> *state)
{
    IG_ASSERT(elt, "no element 0");
    IG_ASSERT(state, "no state");

    //------------- retrieve brother elements --------------------

    int parent_id = elt->parent();
    igElement *elt_parent = mesh->element(parent_id);

    igElement *elt0 = mesh->element(elt_parent->child(0));
    igElement *elt1 = mesh->element(elt_parent->child(1));
    igElement *elt2 = mesh->element(elt_parent->child(2));
    igElement *elt3 = mesh->element(elt_parent->child(3));

    //------------- update elements --------------------

    // update activity flags
    elt0->setInactive();
    elt1->setInactive();
    elt2->setInactive();
    elt3->setInactive();

    // set parent active
    elt_parent->setActive();

    // update geometry for ALE
    elt_parent->initializeGeometry();

    // set zero error for parent
    this->error_xi->at(parent_id) = 0;
    this->error_eta->at(parent_id) = 0;

    //------------- update of state vector -----------------

    igFittingRefinedSurface fit;

    fit.setElements(elt_parent, elt0, elt1, elt2, elt3);
    fit.setSolution(state, mesh->variableNumber());

    fit.run();
}

//////////////////////////////////////////////////////

void igMeshCoarsenerIsotropic::coarsenMidFace(igFace *face)
{
    face->setInactive();
}

void igMeshCoarsenerIsotropic::coarsenFace(igFace *face)
{
    IG_ASSERT(face, "no face 0");

    igFace *face_parent;

    if(face->type() == 0) // interior
        face_parent = mesh->face(face->parent());
    else
        face_parent = mesh->boundaryFace(face->parent());

    int child0 = face_parent->child(0);
    int child1 = face_parent->child(1);

    igFace *face0;
    igFace *face1;

    if(face->type() == 0){
        face0 = mesh->face(child0);
        face1 = mesh->face(child1);
    } else {
        face0 = mesh->boundaryFace(child0);
        face1 = mesh->boundaryFace(child1);
    }
    // update activity flags
    face0->setInactive();
    face1->setInactive();

    face_parent->setActive();
}



void igMeshCoarsenerIsotropic::updateFace(igFace *face, int elt_active_id, bool left)
{
    igElement *elt = mesh->element(elt_active_id);

    int dimension = elt->physicalDimension();
    int max_field_number = max(mesh->variableNumber(),mesh->derivativeNumber());
    int degree = elt->cellDegree();

    int orientation;
    int sense;
    bool sign = false;
    int index_beg;
    int index_incr;

    if(left){
        orientation = face->leftOrientation();
        sense = face->leftSense();
        face->setLeftElementIndex(elt_active_id);
    } else {
        orientation = face->rightOrientation();
        sense = face->rightSense();
        face->setRightElementIndex(elt_active_id);
    }
    if(sense<0)
        sign = true;


    if(orientation == 0){

        index_beg = 0;
        index_incr = 1;
        if(sign){
            index_beg = degree;
            index_incr *= -1;
        }
    }
    else if(orientation == 1){

        index_beg = degree;
        index_incr = degree+1;
        if(sign){
            index_beg = (degree+1)*(degree+1)-1;
            index_incr *= -1;
        }
    }
    else if(orientation == 2){

        index_beg = degree*(degree+1);
        index_incr = 1;
        if(sign){
            index_beg = (degree+1)*(degree+1)-1;
            index_incr *= -1;
        }
    }
    else {

        index_beg = 0;
        index_incr = degree+1;
        if(sign){
            index_beg = (degree+1)*degree;
            index_incr *= -1;
        }
    }

    for (int idof=0; idof<face->controlPointNumber(); idof++){
        for (int ivar=0; ivar<max_field_number; ivar++){
            if(left){
                face->setLeftDofMap(idof,ivar,elt->globalId(index_beg+index_incr*idof,ivar));
                face->setLeftPointMap(idof, index_beg+index_incr*idof);
            }
            else{
                face->setRightDofMap(idof,ivar,elt->globalId(index_beg+index_incr*idof,ivar));
                face->setRightPointMap(idof, index_beg+index_incr*idof);
            }
        }
    }

}

void igMeshCoarsenerIsotropic::updateHalfFace(igFace *face, bool left)
{
    igFace *face_parent = mesh->face(face->parent());

    int childrank;
    if(face_parent->child(0) == face->cellId())
        childrank = 1;
    else
        childrank = 2;
    face->setChildRank(childrank);

    if(left){

        face->setHalfFaceLeft(true);

        if(childrank == 1)
            face->setParameterIntervalLeft(0., 0.5);
        else
            face->setParameterIntervalLeft(0.5, 1.);

    } else {

        face->setHalfFaceRight(true);

        if(childrank == 1)
            face->setParameterIntervalRight(0., 0.5);
        else
            face->setParameterIntervalRight(0.5, 1.);
    }

    face->evaluateGaussPoints();

}



//////////////////////////////////////////////////////

void igMeshCoarsenerIsotropic::checkCoarsening(void)
{
    // by default all elements can be refined
    this->elt_coarsenable->assign(mesh->elementNumber()+mesh->sharedElementNumber(),0);

    for (int ifac=0; ifac<mesh->faceNumber(); ifac++){

        igFace *face = mesh->face(ifac);

        if(face->isActive()){

            int left_index = face->leftElementIndex();
            int right_index = face->rightElementIndex();

            int level_left = mesh->elementLevel(left_index,0);
            int level_right = mesh->elementLevel(right_index,0);

            if(level_left < level_right)
                elt_coarsenable->at(left_index) = 1;
            if(level_left > level_right)
                elt_coarsenable->at(right_index) = 1;

        }
    }

    // MPI communication
    communicator->addElementArrayInt(elt_coarsenable);

    return;
}

//////////////////////////////////////////////////////////

bool igMeshCoarsenerIsotropic::checkBrotherhood(igElement *left_elt, igElement *right_elt)
{
    IG_ASSERT(left_elt, "no element 0");
    IG_ASSERT(right_elt, "no element 1");


    //------------- retrieve brother elements --------------------

    igElement *elt_parent = mesh->element(left_elt->parent());

    int index0 = elt_parent->child(0);
    int index1 = elt_parent->child(1);
    int index2 = elt_parent->child(2);
    int index3 = elt_parent->child(3);

    int index_right = right_elt->cellId();

    //------------- check -------------------

    bool brother = false;

    if(index_right == index0 ||
       index_right == index1 ||
       index_right == index2 ||
       index_right == index3 )
        brother = true;

    return brother;

}

////////////////////////////////////////////////////////////////////////////////////////////////

void igMeshCoarsenerIsotropic::computeSharedDataNumber(void)
{
    this->new_shared_elt_number = 0;
    this->new_shared_dof_number = 0;

    int index_elt_left;
    int index_dof_left;
    int index_elt_right;
    int index_dof_right;

    // update number of shared data
    for (int ifac=0; ifac<mesh->faceNumber(); ifac++){

        igFace *face = mesh->face(ifac);

        if(face->isActive() && face->shared()){

            if(face->leftInside()){

                if( face->childRank() == 0 || face->halfFaceLeft() || ( face->halfFaceRight() && face->childRank()==1 ) ){
                    new_shared_dof_number += face->controlPointNumber();
                    new_shared_elt_number++;
                }
            }

            if(face->rightInside()){

                if( face->childRank() == 0 || face->halfFaceRight() || ( face->halfFaceLeft() && face->childRank()==1 ) ){
                    new_shared_dof_number += face->controlPointNumber();
                    new_shared_elt_number++;
                }

            }

        }
    }

}
