/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igLinAlg.h"

igLinAlg::igLinAlg()
{

}

void igLinAlg::inverse(double* A, int N)
{
	int *IPIV = new int[N+1];
	int LWORK = N*N;
	double *WORK = new double[LWORK];
	int INFO;

	dgetrf_(&N,&N,A,&N,IPIV,&INFO);
	dgetri_(&N,A,&N,IPIV,WORK,&LWORK,&INFO);

	delete[] IPIV;
	delete[] WORK;
}

// Compute C=A*B (Matrix-Matrix multiplication)
void igLinAlg::square_mat_mult(double* A, double* B, double* C, int dim)
{
	char trans = 'N';
	double alpha = 1.;
	double beta = 0.;

	dgemm_(&trans,&trans,&dim,&dim,&dim,&alpha,A,&dim,B,&dim,&beta,C,&dim);
}

// Compute c=A*b (Matrix-Vector multiplication)
void igLinAlg::vec_mat_mult(double* A, double* b, double* c, int dim)
{
	char trans = 'N';
	double alpha = 1.;
	double beta = 0.;
	int incxy = 1;

	dgemv_(&trans,&dim,&dim,&alpha,A,&dim,b,&incxy,&beta,c,&incxy);
}

// Cross product in 3D
vector<double> igLinAlg::cross_product(vector<double> A, vector<double> B)
{
	vector<double> cross_product(3,0.);

	cross_product[0] = A[1]*B[2] - A[2]*B[1];
	cross_product[1] = A[2]*B[0] - A[0]*B[2];
	cross_product[2] = A[0]*B[1] - A[1]*B[0];

	return cross_product;
}


