/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igBasisBSpline.h"

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igBasisBSpline::igBasisBSpline(int degree_value, vector<double> *knot_values) : igBasis(degree_value)
{
	this->knot_vector = knot_values;
	this->function_number = knot_vector->size() - this->degree - 1;
}

/////////////////////////////////////////////////////////////////////////////

int igBasisBSpline::functionNumber()
{
	return function_number;
}

void igBasisBSpline::evalFunction(double var, vector<double> &values)
{
	// Initialize functions to 0
	values.assign(function_number,0.);
	vector<double> nonzero_values(degree+1,0.);

	//Computation of B-Spline basis functions, following the algorithm contained in The NURBS Book
	int span = this->findSpan(var);
	this->evalNonZeroFunctions(span,var,nonzero_values);

	// Values of all basis functions
	for(int i=0; i<=degree; i++)
		values[span-degree+i] = nonzero_values[i];

}

int igBasisBSpline::findSpan(double var)
{
	//Find the knot span that contains the parametric coordinate var
	int span;

	if(var == knot_vector->at(function_number))
		span = function_number - 1;
	else{

		int low = degree;
		int high = function_number;
		int mid = (low+high)/2;

		while(var<knot_vector->at(mid) || var>=knot_vector->at(mid+1)){

			if(var<knot_vector->at(mid))
				high = mid;
			else
				low = mid;

			mid = (low+high)/2;
		}
		span = mid;

	}
	return span;
}

void igBasisBSpline::evalNonZeroFunctions(int i, double var, vector<double> &values)
{
	//Compute the nonzero basis functions in the i-th knot span
	vector<double> left(degree+1,0.);
	vector<double> right(degree+1,0.);

	values[0] = 1.;
	for(int j=1; j<(degree+1); j++){

		left.at(j) = var - knot_vector->at(i+1-j);
		right.at(j) = knot_vector->at(i+j) - var;

		double saved = 0.;
		for(int r=0; r<j; r++){

			double temp = values.at(r)/(right.at(r+1) + left.at(j-r));
			values.at(r) = saved + right.at(r+1)*temp;
			saved = left.at(j-r)*temp;
		}
		values.at(j) = saved;

	}
}

void igBasisBSpline::evalGradient(double var, vector<double> &values)
{
    // Initialize functions to 0
    values.assign(function_number,0.);

    vector<double> nonzero_values(degree+1,0.);
    vector<double> nonzero_ders(degree+1,0.);

    //Computation of B-Spline basis derivatives, following the algorithm contained in The NURBS Book
    int span = this->findSpan(var);
    this->evalNonZeroDerivatives(span,var,nonzero_values,nonzero_ders);

    // Values of all basis functions
    for(int i=0; i<=degree; i++)
        values[span-degree+i] = nonzero_ders[i];
}

void igBasisBSpline::evalNonZeroDerivatives(int i, double var, vector<double> &values, vector<double> &ders)
{
	//Compute the nonzero basis functions in the i-th knot span
	vector<double> left(degree+1,0.);
	vector<double> right(degree+1,0.);

	values[0] = 1.;
	for(int j=1; j<(degree+1); j++){

		left.at(j) = var - knot_vector->at(i+1-j);
		right.at(j) = knot_vector->at(i+j) - var;

		double saved = 0.;
        double saved_der = 0.;
		for(int r=0; r<j; r++){

			double temp = values.at(r)/(right.at(r+1) + left.at(j-r));
			values.at(r) = saved + right.at(r+1)*temp;
            ders.at(r) = saved_der - degree*temp;
			saved = left.at(j-r)*temp;
            saved_der = degree*temp;
		}
		values.at(j) = saved;
        ders.at(j) = saved_der;

	}
}

int igBasisBSpline::extractBezier(vector<double> *X_spline, vector<double> *Y_spline,
    		vector<vector<double>> *X_bezier, vector<vector<double>> *Y_bezier)
{
	// Extraction of Bezier segments from B-Spline curve
	// Algorithm proposed in the NURBS book (pp. 173-176)
	int m = knot_vector->size() - 1;
	int a = degree;
	int b = degree + 1;
	int N_bezier = 0;

	// Auxiliary variables
	int i, mult, r, save, s;
	double numer, alpha;
	vector<double> alphas(degree-1,0.);	// coefficients of knot refinement

	// Initialize first Bezier segment
	X_bezier->push_back(vector<double> (degree+1,0.));
	Y_bezier->push_back(vector<double> (degree+1,0.));
	for(i=0; i<(degree+1); i++){
		X_bezier->at(N_bezier).at(i) = X_spline->at(i);
		Y_bezier->at(N_bezier).at(i) = Y_spline->at(i);
	}

	// Extraction
	while(b<m){

		// Compute multiplicity of knot
		i = b;
		while(b<m && knot_vector->at(b+1)==knot_vector->at(b))
			b++;
		mult = b - i + 1;

		if(b<m){
			X_bezier->push_back(vector<double> (degree+1,0.));
			Y_bezier->push_back(vector<double> (degree+1,0.));
		}

		// Knot insertion
		if(mult<degree){

			// Compute coefficients for knot insertion
			numer = knot_vector->at(b) - knot_vector->at(a);
			for(int j=degree; j>mult; j--)
				alphas.at(j - mult - 1) = numer/(knot_vector->at(a+j) - knot_vector->at(a));

			// Compute new points
			r = degree - mult;	// number of knots to insert
			for(int j=1; j<=r; j++){
				save = r - j;
				s = mult + j;
				for(int k=degree; k>=s; k--){
					alpha = alphas.at(k-s);
					X_bezier->at(N_bezier).at(k) = alpha*X_bezier->at(N_bezier).at(k)
							+ (1.-alpha)*X_bezier->at(N_bezier).at(k-1);
					Y_bezier->at(N_bezier).at(k) = alpha*Y_bezier->at(N_bezier).at(k)
							+ (1.-alpha)*Y_bezier->at(N_bezier).at(k-1);
				}

				if(b<m){
					X_bezier->at(N_bezier+1).at(save) = X_bezier->at(N_bezier).at(degree);
					Y_bezier->at(N_bezier+1).at(save) = Y_bezier->at(N_bezier).at(degree);
				}
			}

		}

		// Initialize next Bezier segment
		N_bezier = N_bezier + 1;
		if(b<m){
			for(int i=(degree-mult); i<(degree+1); i++){
				X_bezier->at(N_bezier).at(i) = X_spline->at(b - degree + i);
				Y_bezier->at(N_bezier).at(i) = Y_spline->at(b - degree + i);
			}
			a = b;
			b = b + 1;
		}
	}

	return N_bezier;
}
