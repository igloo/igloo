/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igCoreExport.h>

#include <vector>

using namespace std;

extern "C" {
    // LU decomoposition of a general matrix
    void dgetrf_(int* M, int *N, double* A, int* lda, int* IPIV, int* INFO);

    // generate inverse of a matrix given its LU decomposition
    void dgetri_(int* N, double* A, int* lda, int* IPIV, double* WORK, int* lwork, int* INFO);

    // matrix-matrix multiplication
    void dgemm_(char* transa, char* transb, int* m, int* n, int* k, double* alpha, double* a, int* lda,
    			double* b, int* ldb, double* beta, double* c, int* ldc);
    // matrix-vector multiplication
    void dgemv_(char* trans, int* m, int* n, double* alpha, double* a, int* lda, double* b,
    			int* INCX, double* beta, double* c, int* INCY);
}

class IGCORE_EXPORT igLinAlg
{
public:
	igLinAlg(void);
	~igLinAlg(void) = default;

	void inverse(double* A, int N);
	void square_mat_mult(double* A, double* B, double* C, int dim);
	void vec_mat_mult(double* A, double* b, double* c, int dim);
	vector<double> cross_product(vector<double> A, vector<double> B);

};

//
// igLinAlg.h ends here

