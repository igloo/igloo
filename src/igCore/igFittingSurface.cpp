/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/

#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>

#include "igElement.h"
#include "igFittingSurface.h"
#include "igBasisBezier.h"


using namespace std;

/////////////////////////////////////////////////////////////////////////////

igFittingSurface::igFittingSurface()
{

}

/////////////////////////////////////////////////////////////////////////////

void igFittingSurface::setFunction(function<double(double *coord, double time, int var_id, double *coord_elt, double *func_param)> fun, int var_id, double *func_param)
{
    this->fun = fun;
    this->var_id = var_id;
    this->func_param = func_param;
}

///////////////////////////////////////////////////////////////

double igFittingSurface::zControlPointFit(int index)
{
    return sol_z.at(index);
}

///////////////////////////////////////////////////////////////

void igFittingSurface::run(void)
{
    int degree = this->element->cellDegree();

    igBasis *basis = new igBasisBezier(degree);

    double coord[2];
    double coord_elt[2];
    double *generic_param;
    
    vector<double> values1;
    values1.assign(degree+1,0.);
    vector<double> values2;
    values2.assign(degree+1,0.);

    int dof_number = (degree+1)*(degree+1);
    int size = (degree+1)*(degree+1);

    double *rhs = new double[size];
    double *mat = new double[size*size];

    // initialize matrix and rhs, sol
    for (int i=0; i<size; i++){
        rhs[i] = 0.;
        for (int j=0; j<size; j++)
            mat[i*size+j] = 0.;
    }

    coord_elt[0] = 0.;
    coord_elt[1] = 0.;
    
    for (int i=0; i<degree+1; i++){
      for(int j=0; j<degree+1; j++){
          coord_elt[0] += this->element->controlPointCoordinate(i*(degree+1)+j, 0);
          coord_elt[1] += this->element->controlPointCoordinate(i*(degree+1)+j, 1);
      }
    }

    coord_elt[0] /= (degree+1)*(degree+1);
    coord_elt[1] /= (degree+1)*(degree+1);
    
    // compute matrix and rhs
    for (int k2=0; k2<sample_number; k2++){
        for (int k1=0; k1<sample_number; k1++){

            double coord1 = float(k1)/float(sample_number-1);
            basis->evalFunction(coord1,values1);
            double coord2 = float(k2)/float(sample_number-1);
            basis->evalFunction(coord2,values2);

            double weighted_sum = 0.;
            for (int ifun2=0; ifun2<degree+1; ifun2++){
                for (int ifun1=0; ifun1<degree+1; ifun1++){
                    int index = ifun1+(degree+1)*ifun2;
                    weighted_sum += values1.at(ifun1)*values2.at(ifun2)* this->element->controlPointWeight(index);
                }
            }

            coord[0] = 0.;
            coord[1] = 0.;
            for (int ifun2=0; ifun2<degree+1; ifun2++){
                for (int ifun1=0; ifun1<degree+1; ifun1++){
                    int index = ifun1+(degree+1)*ifun2;
                    coord[0] += values1.at(ifun1)*values2.at(ifun2)*this->element->controlPointWeight(index)*this->element->controlPointCoordinate(index, 0)/weighted_sum;
                    coord[1] += values1.at(ifun1)*values2.at(ifun2)*this->element->controlPointWeight(index)*this->element->controlPointCoordinate(index, 1)/weighted_sum;

                }
            }

            double function_value = fun(coord, 0., var_id, coord_elt, this->func_param);

            for (int i2=0; i2<degree+1; i2++){
                for (int i1=0; i1<degree+1; i1++){

                    int index1 =  i1+(degree+1)*i2;
                    rhs[i1+(degree+1)*i2] += values1.at(i1)*values2.at(i2)*this->element->controlPointWeight(index1)/weighted_sum * function_value;

                    for (int j2=0; j2<degree+1; j2++){
                        for (int j1=0; j1<degree+1; j1++){
                            int index2 = j1+(degree+1)*j2;
                            mat[(i1+(degree+1)*i2)*size+(j1+(degree+1)*j2)] += values1.at(i1)*values2.at(i2)*this->element->controlPointWeight(index1)/weighted_sum *
                                                                               values1.at(j1)*values2.at(j2)*this->element->controlPointWeight(index2)/weighted_sum;
                        }
                    }

                }
            }

        }
    }


    // solve system
    inverse_system(mat,size);

    // store solution
    sol_z.resize(size);

    for (int ils=0; ils<size; ils++){
        sol_z[ils] = 0.;
        for (int jls=0; jls<size; jls++){
            sol_z[ils] += mat[ils*size+jls]*rhs[jls];
        }
    }

    delete basis;
    delete[] mat;
    delete[] rhs;
}
