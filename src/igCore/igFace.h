/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igCoreExport.h>

#include "igCell.h"

#include <vector>

class igElement;

using namespace std;

class IGCORE_EXPORT igFace : public igCell
{
public:
     igFace(void);
    ~igFace(void) = default;

public:
    void setLeftDofMap(int local_id, int var_id, int global_id);
    void setRightDofMap(int local_id, int var_id, int global_id);
    void setLeftPointMap(int pt_face_id, int pt_elt_id);
    void setRightPointMap(int pt_face_id, int pt_elt_id);

    void setLeftDofMap(vector<int> dof);
    void setRightDofMap(vector<int> dof);

    void setLeftElementIndex(int index);
    void setRightElementIndex(int index);

    void setType(int type);
    void setShared(void);
    void setLeftInside(void);
    void setRightInside(void);
    void setLeftPartitionId(int neighbor_id);
    void setRightPartitionId(int neighbor_id);

    void setOrientationLeft(int orientation);
    void setOrientationRight(int orientation);
    void setSenseLeft(int sense);
    void setSenseRight(int sense);

    void setParameterIntervalLeft(double p_min, double p_max);
    void setParameterIntervalRight(double p_min, double p_max);
    void setParameterIntervalLeft(double p_min1, double p_max1,double p_min2, double p_max2);
    void setParameterIntervalRight(double p_min1, double p_max1,double p_min2, double p_max2);

    void setHalfFaceLeft(bool half);
    void setHalfFaceRight(bool half);

    void setSliding(void);
    void setParentSliding(void);
    void setLocalCoordinate(int index, int component, double coord);
    void setLocalWeight(int index, double weight);
    void setLocalVelocity(int index, int component, double vel);

    void setInterfaceQuadratureCoordinate(int index, double par);
    void setInterfaceQuadratureWeight(int index, double weight);
    void setInterfaceQuadratureLabel(int index, int label);

public:
    void initializeDegreesOfFreedom(void);
    void initializeGeometry(void);
    void initializeNormal(vector<double> *left_center);
    void initializeNormalFromRight(vector<double> *right_center);
    void computeNormal(void);
    void updateNormal(void);
    void evaluateGaussPoints(void);
    void initializeInterfaceProperties(igFace *face_parent);

public:
    double controlPointCoordinate(int index, int component) const;
    void controlPointCoordinates(int index, vector<double> *coord);
    double controlPointWeight(int index) const;

    double controlPointVelocityComponent(int index, int component) const;
    void controlPointVelocity(int index, vector<double> *vel);

    double *gaussPointNormalPtr(int pt_index);
    double *gaussPointTangent1Ptr(int pt_index);
    double *gaussPointTangent2Ptr(int pt_index);
    double gaussPointFunctionValueLeft(int pt_index, int fun_index);
    double *gaussPointFunctionPtrLeft(int pt_index);
    double gaussPointFunctionValueRight(int pt_index, int fun_index);
    double *gaussPointFunctionPtrRight(int pt_index);

    double interfaceQuadratureCoordinate(int index);
    double interfaceQuadratureWeight(int index);
    int interfaceQuadratureLabel(int index);

    int dofLeft(int local_id, int var_id) const;
    int dofRight(int local_id, int var_id) const;
    int pointLeft(int pt_face_id);
    int pointRight(int pt_face_id);

    vector<int> dofLeft(void) const;
    vector<int> dofRight(void) const;

    int leftElementIndex(void);
    int rightElementIndex(void);

    int leftOrientation(void);
    int rightOrientation(void);
    int leftSense(void);
    int rightSense(void);

    double leftParameterBegin(void);
    double leftParameterEnd(void);
    double rightParameterBegin(void);
    double rightParameterEnd(void);
    double parameterBegin(int direction, bool left);
    double parameterEnd(int direction, bool left);

    int type(void);
    bool shared(void);
    int leftPartitionId(void);
    int rightPartitionId(void);
    bool leftInside(void);
    bool rightInside(void);

    bool halfFaceLeft(void);
    bool halfFaceRight(void);

    bool isSliding(void);
    bool isParentSliding(void);

private:
    void evaluateSlidingGaussPoints(void);

private:
    vector<double> gauss_point_normal_list;
    vector<double> gauss_point_tangent1_list;
    vector<double> gauss_point_tangent2_list;
    vector<double> gauss_point_value_left_list;
    vector<double> gauss_point_value_right_list;

    vector<int> dof_map_left;
    vector<int> dof_map_right;
    vector<int> pt_map_left;
    vector<int> pt_map_right;

    int left_element_index;
    int right_element_index;

    int left_orientation;
    int right_orientation;
    int left_sense;
    int right_sense;

    int face_type;

    bool half_face_left;
    bool half_face_right;

    bool shared_face;
    int left_partition_id;
    int right_partition_id;
    bool left_inside;
    bool right_inside;

    double param_min_left1;
    double param_max_left1;
    double param_min_right1;
    double param_max_right1;
    double param_min_left2;
    double param_max_left2;
    double param_min_right2;
    double param_max_right2;

    bool sliding_face;
    bool parent_sliding_face;
    vector<double> local_coordinates;
    vector<double> local_velocities;

    vector<double> interface_quadrature_coordinate_list;
    vector<double> interface_quadrature_weight_list;
    vector<int> interface_quadrature_label_list;
};

//
// igFace.h ends here
