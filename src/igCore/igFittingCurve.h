/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include "igFitting.h"

#include <igCoreExport>

#include <functional>
#include <vector>

using namespace std;

class IGCORE_EXPORT igFittingCurve : public igFitting
{
public:
     igFittingCurve(void);
    ~igFittingCurve(void);

public:
    void setParameterBounds(double p_min, double p_max);
    void setCoordinateBounds(double x_min, double x_max, double y_min, double y_max, double z_min, double z_max);
    void setParameterFixed(double f_fixed1, double f_fixed2);
    void selectParameterFixed(int index1, int index2);
    void setXFunction(function<double(double, double, double)> x_fun);
    void setYFunction(function<double(double, double, double)> y_fun);
    void setZFunction(function<double(double, double, double)> z_fun);

public:
    void run(void);

public:
    double xControlPointFit(int index);
    double yControlPointFit(int index);
    double zControlPointFit(int index);

private:
    int fixed_parameter1;
    int fixed_parameter2;

    double p_min;
    double p_max;
    double p_fixed1;
    double p_fixed2;

    double x_min;
    double x_max;
    double y_min;
    double y_max;
    double z_min;
    double z_max;

    vector<double> sol_x;
    vector<double> sol_y;
    vector<double> sol_z;

    function<double(double, double, double)> x_fun;
    function<double(double, double, double)> y_fun;
    function<double(double, double, double)> z_fun;
};

//
// igFittingCurve.h ends here
