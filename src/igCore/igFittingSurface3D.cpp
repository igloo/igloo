/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/

#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>

#include "igFittingSurface3D.h"
#include "igBasisBezier.h"


using namespace std;

/////////////////////////////////////////////////////////////////////////////

igFittingSurface3D::igFittingSurface3D()
{

}

igFittingSurface3D::~igFittingSurface3D(void)
{

}

/////////////////////////////////////////////////////////////////////////////


void igFittingSurface3D::setParameterBounds(double p_min1, double p_max1, double p_min2, double p_max2)
{
    this->p_min1 = p_min1;
    this->p_max1 = p_max1;
    this->p_min2 = p_min2;
    this->p_max2 = p_max2;
}

void igFittingSurface3D::setParameterFixed(double p_fixed)
{
    this->p_fixed = p_fixed;
}

void igFittingSurface3D::selectParameterFixed(int index)
{
    this->fixed_parameter = index;
}

void igFittingSurface3D::setXFunction(function<double (double, double, double)> x_fun)
{
    this->x_fun = x_fun;
}

void igFittingSurface3D::setYFunction(function<double (double, double, double)> y_fun)
{
    this->y_fun = y_fun;
}

void igFittingSurface3D::setZFunction(function<double (double, double, double)> z_fun)
{
    this->z_fun = z_fun;
}

void igFittingSurface3D::setCoordinateBounds(vector<double> *edge_x, vector<double> *edge_y, vector<double> *edge_z)
{
    this->edge_x = edge_x;
    this->edge_y = edge_y;
    this->edge_z = edge_z;
}

///////////////////////////////////////////////////////////////

double igFittingSurface3D::xControlPointFit(int index)
{
    return sol_x.at(index);
}

double igFittingSurface3D::yControlPointFit(int index)
{
    return sol_y.at(index);
}

double igFittingSurface3D::zControlPointFit(int index)
{
    return sol_z.at(index);
}

///////////////////////////////////////////////////////////////

void igFittingSurface3D::run(void)
{

    igBasis *basis = new igBasisBezier(degree);

    int size = (degree+1)*(degree+1); // NOTE: complete system including constraint
    double *rhs_x = new double[size];
    double *rhs_y = new double[size];
    double *rhs_z = new double[size];
    double *mat = new double[size*size];

    double target_x00, target_x10, target_x01, target_x11;
    double target_y00, target_y10, target_y01, target_y11;
    double target_z00, target_z10, target_z01, target_z11;

    vector<double> values1;
    values1.assign(degree+1,0.);
    vector<double> values2;
    values2.assign(degree+1,0.);

    // initialize matrix and rhs, sol
    for (int ils=0; ils<size; ils++){
        rhs_x[ils] = 0.;
        rhs_y[ils] = 0.;
        rhs_z[ils] = 0.;
        for (int jls=0; jls<size; jls++)
            mat[ils*size+jls] = 0.;
    }

    // compute matrix and rhs
    for (int kls2=0; kls2<sample_number; kls2++){
        for (int kls1=0; kls1<sample_number; kls1++){

            double coord1 = float(kls1)/float(sample_number-1);
            basis->evalFunction(coord1,values1);
            double coord2 = float(kls2)/float(sample_number-1);
            basis->evalFunction(coord2,values2);

            double param1 = p_min1 + (p_max1-p_min1)*coord1;
            double param2 = p_min2 + (p_max2-p_min2)*coord2;

            double target_x;
            double target_y;
            double target_z;
            if(fixed_parameter == 0){
                target_x = x_fun(p_fixed,param1,param2);
                target_y = y_fun(p_fixed,param1,param2);
                target_z = z_fun(p_fixed,param1,param2);
            } else if (fixed_parameter == 1){
                target_x = x_fun(param1,p_fixed,param2);
                target_y = y_fun(param1,p_fixed,param2);
                target_z = z_fun(param1,p_fixed,param2);
            } else {
                target_x = x_fun(param1,param2,p_fixed);
                target_y = y_fun(param1,param2,p_fixed);
                target_z = z_fun(param1,param2,p_fixed);
            }

            int edge_counter = 0;
            for (int i2=0; i2<degree+1; i2++){
                for (int i1=0; i1<degree+1; i1++){

                    int ils =  i1+(degree+1)*i2;

                    if( i1==0 || i1==degree || i2==0 || i2==degree){ // edge constraints

                        // rhs term
                        rhs_x[ils] = (*edge_x)[edge_counter];
                        rhs_y[ils] = (*edge_y)[edge_counter];
                        rhs_z[ils] = (*edge_z)[edge_counter];
                        edge_counter++;

                        // matrix
                        for (int j2=0; j2<degree+1; j2++){
                            for (int j1=0; j1<degree+1; j1++){
                                int jls = j1+(degree+1)*j2;
                                if(jls==ils)
                                    mat[ils*size+jls] = 1;
                                else
                                    mat[ils*size+jls] = 0;
                            }
                        }

                    } else { // interior

                        // rhs term
                        rhs_x[ils] += values1.at(i1)*values2.at(i2) * target_x;
                        rhs_y[ils] += values1.at(i1)*values2.at(i2) * target_y;
                        rhs_z[ils] += values1.at(i1)*values2.at(i2) * target_z;

                        // matrix
                        for (int j2=0; j2<degree+1; j2++){
                            for (int j1=0; j1<degree+1; j1++){
                                int jls = j1+(degree+1)*j2;
                                mat[ils*size+jls] += values1.at(i1)*values2.at(i2)*
                                                     values1.at(j1)*values2.at(j2);
                            }
                        }
                    }

                }
            }

        }
    }


    // solve system
    if (size > 0) {
        inverse_system(mat,size);
    }

    // store solution
    sol_x.resize(size);
    sol_y.resize(size);
    sol_z.resize(size);

    for (int ils=0; ils<size; ils++){
        sol_x[ils] = 0.;
        sol_y[ils] = 0.;
        sol_z[ils] = 0.;
        for (int jls=0; jls<size; jls++){
            sol_x[ils] += mat[ils*size+jls]*rhs_x[jls];
            sol_y[ils] += mat[ils*size+jls]*rhs_y[jls];
            sol_z[ils] += mat[ils*size+jls]*rhs_z[jls];
        }
    }
    delete basis;

}
