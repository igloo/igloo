/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igDistributor.h"

#include "igAssert.h"
#include "igMesh.h"
#include "igElement.h"
#include "igFace.h"


#include <fstream>
#include <iostream>


using namespace std;

/////////////////////////////////////////////////////////////////////////////

igDistributor::igDistributor(void)
{
    this->single_patch = false;
    this->sliding = false;
}

igDistributor::~igDistributor(void)
{
}

////////////////////////////////////////////////////////////////////////


void igDistributor::setMesh(igMesh *mesh)
{
    this->mesh  = mesh;
}

void igDistributor::setPartitionNumber(int number)
{
    this->total_partition_number = number;
}

void igDistributor::setPartitionNumber(int number1, int number2, int number3)
{
    this->partition_number_1 = number1;
    this->partition_number_2 = number2;
    this->partition_number_3 = number3;

    this->total_partition_number = partition_number_1*partition_number_2*partition_number_3;
    this->single_patch = true;
}

void igDistributor::setElementNumber(int number1, int number2, int number3)
{
    this->element_number_1 = number1;
    this->element_number_2 = number2;
    this->element_number_3 = number3;

    this->single_patch = true;
}

void igDistributor::setSliding(bool flag_sliding)
{
	this->sliding = flag_sliding;
}

///////////////////////////////////////////////////////////////////////

int igDistributor::neighbourId(int part_id, int neighbour_id)
{
    return this->partition_map.at(part_id)[neighbour_id];
}

int igDistributor::neighbourNumber(int part_id)
{
    return this->neighbour_number.at(part_id);
}

int igDistributor::neighbourFaceNumber(int part_id, int neighbour_id)
{
    return this->shared_face_map.at(part_id)[neighbour_id];
}

int igDistributor::neighbourDofNumber(int part_id, int neighbour_id)
{
    return this->shared_dof_map.at(part_id)[neighbour_id];
}

int igDistributor::elementNumber(int part_id)
{
    return element_numbers.at(part_id);
}

int igDistributor::dofNumber(int part_id)
{
    return dof_numbers.at(part_id);
}

////////////////////////////////////////////////////////////////////////

void igDistributor::run(void)
{
    int element_number = mesh->elementNumber();
    int face_number = mesh->faceNumber();
    int degree = mesh->element(0)->cellDegree();

    element_numbers.assign(total_partition_number, 0);
    dof_numbers.assign(total_partition_number, 0);

    mesh->setPartitionNumber(total_partition_number);

    //----------- first step : attribute a partition to each element -------------

    if(single_patch){

        int kmax = 1;
        if(element_number_3 > 0)
            kmax = element_number_3;

        int element_number_1_part = element_number_1 / partition_number_1;
        int element_number_2_part = element_number_2 / partition_number_2;
        int element_number_3_part = 1;
        if(element_number_3>0)
            element_number_3_part = element_number_3 / partition_number_3;

        for(int iel3=0; iel3<kmax; iel3++){
            for(int iel2=0; iel2<element_number_2; iel2++){
                for(int iel1=0; iel1<element_number_1; iel1++){

                    int index = iel3*element_number_1*element_number_2 + iel2*element_number_1 + iel1;
                    int current_partition_1 = iel1 / element_number_1_part;
                    int current_partition_2 = iel2 / element_number_2_part;
                    int current_partition_3 = iel3 / element_number_3_part;
                    int current_partition = current_partition_3*partition_number_1*partition_number_2 +
                        current_partition_2*partition_number_1 + current_partition_1;

                    igElement *elt = mesh->element(index);
                    elt->setPartition(current_partition);
                    element_numbers.at(current_partition)++;
                    dof_numbers.at(current_partition) += elt->controlPointNumber();
                }
            }
        }

    } else {

    	int interface_element_number = 0;

    	for(int iel=0; iel<element_number; iel++){
    		igElement *elt = mesh->element(iel);
    		elt->setPartition(-1);
    	}

    	// Prioritize elements for parallel sliding mesh
    	if(sliding){

    		for(int iface=0; iface<face_number; iface++){

    			igFace *face = mesh->face(iface);

    			// Check if left and right elements are in different subdomains
    			int left_subdomain = mesh->element(face->leftElementIndex())->subdomain();
    			int right_subdomain = mesh->element(face->rightElementIndex())->subdomain();

    			if(left_subdomain!=right_subdomain){

    				igElement *elt;

    				elt = mesh->element(face->leftElementIndex());
    				elt->setPartition(0);

    				elt = mesh->element(face->rightElementIndex());
    				elt->setPartition(0);

    				element_numbers.at(0) += 2;
    				dof_numbers.at(0) += 2*elt->controlPointNumber();

    				interface_element_number += 2;
    			}

    		}

    	}

        int element_number_per_partition = element_number / total_partition_number;
        int queue_number = element_number - total_partition_number*element_number_per_partition;

        if(interface_element_number>element_number_per_partition)
        	cout << "WARNING: interface element number greater than elements per partition!" << endl;

        int current_partition = 0;
        int element_in_current_partition = interface_element_number;

        for(int iel=0; iel<element_number; iel++){

            igElement *elt = mesh->element(iel);

        	int add_elt = 0;
        	if(queue_number > 0)
        		add_elt = 1;

            if(elt->partition()==-1){

            	elt->setPartition(current_partition);

            	element_numbers.at(current_partition)++;
            	dof_numbers.at(current_partition) += elt->controlPointNumber();

            	element_in_current_partition++;

            }

            if(element_in_current_partition == element_number_per_partition + add_elt){
            	current_partition++;
                element_in_current_partition = 0;
                queue_number--;
            }

        }
    }

    //---------- second step : fill the communication arrays --------------------

    neighbour_number.resize(total_partition_number,0);

    partition_map.resize(total_partition_number);
    shared_dof_map.resize(total_partition_number);
    shared_face_map.resize(total_partition_number);

    // loop over all interior faces
    for (int ifac=0; ifac<face_number; ifac++){

        igFace *face = mesh->face(ifac);

        int partition_left = mesh->element(face->leftElementIndex())->partition();
        int partition_right = mesh->element(face->rightElementIndex())->partition();

        face->setLeftPartitionId(partition_left);
        face->setRightPartitionId(partition_right);

        int dof_per_face = face->controlPointNumber();

        if(partition_left != partition_right){ // test interface between partitions

            // loop on neighbours of left part
            int neighbour_index = -1;
            for(int ineigh=0; ineigh<neighbour_number.at(partition_left); ineigh++){

                if(partition_right == partition_map.at(partition_left)[ineigh])
                    neighbour_index = ineigh;

            }


            // case 1 : neighbour already exists
            if(neighbour_index >= 0){

                shared_face_map.at(partition_left)[neighbour_index]++;
                shared_dof_map.at(partition_left)[neighbour_index] += dof_per_face;

                // case 2 : new neighbour
            } else {

                neighbour_number.at(partition_left)++;
                partition_map.at(partition_left).push_back(partition_right);
                shared_face_map.at(partition_left).push_back(1);
                shared_dof_map.at(partition_left).push_back(dof_per_face);

            }


            // loop on neighbours of right part
            neighbour_index = -1;
            for(int ineigh=0; ineigh<neighbour_number.at(partition_right); ineigh++){

                if(partition_left == partition_map.at(partition_right)[ineigh])
                    neighbour_index = ineigh;

            }

            // case 1 : neighbour already exists
            if(neighbour_index >= 0){

                shared_face_map.at(partition_right)[neighbour_index]++;
                shared_dof_map.at(partition_right)[neighbour_index] += dof_per_face;

                // case 2 : new neighbour
            } else {

                neighbour_number.at(partition_right)++;
                partition_map.at(partition_right).push_back(partition_left);
                shared_face_map.at(partition_right).push_back(1);
                shared_dof_map.at(partition_right).push_back(dof_per_face);

            }

        }

    }

    // loop over all boundary faces
    for (int ifac=0; ifac<mesh->boundaryFaceNumber(); ifac++){

        igFace *face = mesh->boundaryFace(ifac);

        int partition_left = mesh->element(face->leftElementIndex())->partition();

        face->setLeftPartitionId(partition_left);
    }

}
