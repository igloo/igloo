/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <iostream>
#include <stdlib.h>
#include <iomanip>
#include <igAssert.h>

#include <math.h>


using namespace std;

/////////////////////////////////////////////////////////

double x_rotate(double x, double y, double *params)
{
    double alpha = params[0];
    double xc = params[1];
    double yc = params[2];
    double r0 = params[3];
    double r1 = params[4];

    double vec_rot[2]{x-xc, y-yc}; // vector which will be rotated
    double r = sqrt(vec_rot[0]*vec_rot[0] + vec_rot[1]*vec_rot[1]); // radius to rotation center

    double phi; // angle coefficient
    if (r >= r1)
        phi = 0.; // fixed external region
    else if (r <= r0)
        phi = 1.; // rigidly moving internal region
    else
        phi = 0.5*(1. + cos(M_PI*(r - r0)/(r1 - r0))); // transition region

    double theta = phi*alpha*M_PI/180.; // angle of rotation for actual control point
    double mat_rot[4]{cos(theta), -sin(theta), sin(theta), cos(theta)}; // rotation matrix

    return xc + mat_rot[0*2+0]*vec_rot[0] + mat_rot[0*2+1]*vec_rot[1];
}

double y_rotate(double x, double y, double *params)
{
    double alpha = params[0];
    double xc = params[1];
    double yc = params[2];
    double r0 = params[3];
    double r1 = params[4];

    double vec_rot[2]{x-xc, y-yc}; // vector which will be rotated
    double r = sqrt(vec_rot[0]*vec_rot[0] + vec_rot[1]*vec_rot[1]); // radius to rotation center

    double phi; // angle coefficient
    if (r >= r1)
        phi = 0.; // fixed external region
    else if (r <= r0)
        phi = 1.; // rigidly moving internal region
    else
        phi = 0.5*(1. + cos(M_PI*(r - r0)/(r1 - r0))); // transition region

    double theta = phi*alpha*M_PI/180.; // angle of rotation for actual control point
    double mat_rot[4]{cos(theta), -sin(theta), sin(theta), cos(theta)}; // rotation matrix

    return yc + mat_rot[1*2+0]*vec_rot[0] + mat_rot[1*2+1]*vec_rot[1];
}


/////////////////////////////////////////////////////////

double x_translate(double x, double y, double *params)
{
    double vec = params[0];

    return x + vec;
}

double y_translate(double x, double y, double *params)
{
    double vec = params[1];

    return y + vec;
}

/////////////////////////////////////////////////////////////

double x_curv(double x, double y, double *params)
{
    // double ampl = params[0];
    //
    // double xmin = -0.5034846559588531;
    // double xmax = 0.4965930421787618;
    // double y1 = -0.5;
    // double y2 = -0.15;
    // double y3 = 0.15;
    // double y4 = 0.5;
    // double alpha = 0.5;
    //
    // double c = xmax-xmin;
    // double m = 0.02;
    // double p = 0.4;
    // double t = 0.12;
    //
    // double omega = 1.;
    // double dx = 0.;
    // double dy = 0.;
    //
    // if ((xmin < x) && (x < xmax) && (y1 < y) && (y < y4)) {
    //     double xc = (x-xmin)/c;
    //     double yt = 5.*t * (0.2969*sqrt(xc+1.e-15) - 0.1260*xc - 0.3516*pow(xc, 2.) + 0.2843*pow(xc, 3.) - 0.1036*pow(xc, 4.));
    //     double yc = 0.;
    //     double dx_yc = 0.;
    //     if (xc < p) {
    //         yc = m/pow(p, 2.) * (2.*p*xc - pow(xc, 2.));
    //         dx_yc = 2.*m/pow(p, 2.) * (p - xc);
    //     } else {
    //         yc = m/pow(1.-p, 2.) * ((1.-2.*p) + 2*p*xc - pow(xc, 2.));
    //         dx_yc = 2.*m/pow(1.-p, 2.) * (p - xc);
    //     }
    //     double theta = atan(dx_yc);
    //     dx = 0.;
    //     dy = yc;
    //     if (y < 0.) {
    //         dx += yt*sin(theta);
    //         dy += yt*(1.-cos(theta));
    //     } else {
    //         dx -= yt*sin(theta);
    //         dy -= yt*(1.-cos(theta));
    //     }
    //
    //     if (y < y2) {
    //         omega = 0.5*( 1.0 + tanh( alpha*(y2-y1) * (1.0/(y2-y) - 1.0/(y-y1)) ) );
    //     }
    //     if (y3 < y) {
    //         omega = 0.5*( 1.0 + tanh( alpha*(y4-y3) * (1.0/(y-y3) - 1.0/(y4-y)) ) );
    //     }
    // }
    //
    // return x + ampl*dx;
    return x;
}

double y_curv(double x, double y, double *params)
{
    double ampl = params[0];

    double xmin = -0.5034846559588531;
    double xmax = 0.4965930421787618;
    // double y1 = -0.5;
    // double y2 = -0.15;
    // double y3 = 0.15;
    // double y4 = 0.5;
    // double alpha = 0.5;
    //
    // double c = xmax-xmin;
    // double m = 0.02;
    // double p = 0.4;
    // double t = 0.12;
    //
    // double omega = 1.;
    // double dx = 0.;
    // double dy = 0.;
    //
    // if ((xmin < x) && (x < xmax) && (y1 < y) && (y < y4)) {
    //     double xc = (x-xmin)/c;
    //     double yt = 5.*t * (0.2969*sqrt(xc+1.e-15) - 0.1260*xc - 0.3516*pow(xc, 2.) + 0.2843*pow(xc, 3.) - 0.1036*pow(xc, 4.));
    //     double yc = 0.;
    //     double dx_yc = 0.;
    //     if (xc < p) {
    //         yc = m/pow(p, 2.) * (2.*p*xc - pow(xc, 2.));
    //         dx_yc = 2.*m/pow(p, 2.) * (p - xc);
    //     } else {
    //         yc = m/pow(1.-p, 2.) * ((1.-2.*p) + 2*p*xc - pow(xc, 2.));
    //         dx_yc = 2.*m/pow(1.-p, 2.) * (p - xc);
    //     }
    //     double theta = atan(dx_yc);
    //     dx = 0.;
    //     dy = yc;
    //     if (y < 0.) {
    //         dx += yt*sin(theta);
    //         dy += yt*(1.-cos(theta));
    //     } else {
    //         dx -= yt*sin(theta);
    //         dy -= yt*(1.-cos(theta));
    //     }
    //
    //     if (y < y2) {
    //         omega = 0.5*( 1.0 + tanh( alpha*(y2-y1) * (1.0/(y2-y) - 1.0/(y-y1)) ) );
    //     }
    //     if (y3 < y) {
    //         omega = 0.5*( 1.0 + tanh( alpha*(y4-y3) * (1.0/(y-y3) - 1.0/(y4-y)) ) );
    //     }
    // }
    //
    // return y + omega*ampl*dy;

    double dy = 0;

    if ((xmin < x) && (x < xmax) && (0. < y) && (y < 0.2)) {

        dy = -(x-xmin)*(x-xmax) * fmax(0.,0.2-y);

    }
    if ((xmin < x) && (x < xmax) && (-0.2 < y) && (y < 0.)) {

        dy = -(x-xmin)*(x-xmax) * fmax(0.,0.2+y);

    }

    return y + ampl*dy;

}

//////////////////////////////////////////////////////////////////

double x_sinus(double x, double y, double *params)
{
     return x;
}

double y_sinus(double x, double y, double *params)
{
    double xmin = 0.0;
    double xmax = 1.0;
    double freq = 2.0*M_PI*10.0/(xmax-xmin);
    double ya = -0.55;
    double yb = -0.5;
    double yc = 0.5;
    double yd = 0.55;
    double alpha = 1.0;

    double ampl = params[0];

    double dy = 0.;
    double omega = 1.;

    if ((xmin < x) && (x < xmax) && (ya < y) && (y < yd)) {
        dy = sin(freq*(x-xmin));

        if (y < yb) {
            omega = 0.5*( 1.0 + tanh( alpha*(yb-ya) * (1.0/(yb-y) - 1.0/(y-ya)) ) );
        }
        if (yc < y) {
            omega = 0.5*( 1.0 + tanh( alpha*(yd-yc) * (1.0/(y-yc) - 1.0/(yd-y)) ) );
        }

    }

     return y + omega*ampl*dy;
}



//////////////////////////////////////////////////////////////////

double x_thickness(double x, double y, double *params)
{
     return x;
}

double y_thickness(double x, double y, double *params)
{
    double ampl = params[0];

    double xmin = -0.5034846559588531;
    double xmax = 0.4965930421787618;
    double y1 = -0.5;
    double y2 = -0.15;
    double y3 = 0.15;
    double y4 = 0.5;
    double alpha = 0.5;

    double c = xmax-xmin;
    double t = 0.03;

    double omega = 1.;
    double dx = 0.;
    double dy = 0.;

    if ((xmin < x) && (x < xmax) && (y1 < y) && (y < y4)) {
        double xc = (x-xmin)/c;
        double yt = 5.*t * (0.2969*sqrt(xc+1.e-15) - 0.1260*xc - 0.3516*pow(xc, 2.) + 0.2843*pow(xc, 3.) - 0.1036*pow(xc, 4.));
        double yc = 0.;

        dx = 0.;
        dy = yc;
        if (y < 0.) {
            dy -= yt;
        } else {
            dy += yt;
        }

        if (y < y2) {
            omega = 0.5*( 1.0 + tanh( alpha*(y2-y1) * (1.0/(y2-y) - 1.0/(y-y1)) ) );
        }
        if (y3 < y) {
            omega = 0.5*( 1.0 + tanh( alpha*(y4-y3) * (1.0/(y-y3) - 1.0/(y4-y)) ) );
        }
    }

    return y + omega*ampl*dy;
}
