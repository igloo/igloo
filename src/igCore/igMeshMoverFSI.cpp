/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igMeshMoverFSI.h"

#include "igMesh.h"
#include "igCell.h"
#include "igElement.h"
#include "igFace.h"
#include "igInterface.h"

#include <igAssert.h>

#include <igDistributed/igCommunicator.h>

#include <iostream>
#include <stdlib.h>
#include <cmath>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igMeshMoverFSI::igMeshMoverFSI(igMesh *mesh, vector<double> *coordinates, vector<double> *velocities) : igMeshMover(mesh,coordinates,velocities)
{
	IG_ASSERT(mesh, "no mesh");

	this->adaptive_mesh = false;

	int internal_dof = mesh->controlPointNumber();
	int shared_dof = mesh->sharedControlPointNumber();
	int dimension = mesh->meshDimension();
	// this->total_coordinate_number = (internal_dof + shared_dof)*dimension;

    this->interface_displacements_ref = new vector<double>;
    this->interface_velocities_ref = new vector<double>;

    int degree = mesh->element(0)->cellDegree();

    this->dof_number_per_elt = (degree+1)*(degree+1);
}

igMeshMoverFSI::~igMeshMoverFSI(void)
{
    delete this->interface_velocities_ref;
    delete this->interface_displacements_ref;
}

/////////////////////////////////////////////////////////////////////////////

void igMeshMoverFSI::setInterfaceFSI(vector<double> *displacements, vector<double> *velocities)
{
    this->interface_displacements = displacements;
    this->interface_velocities = velocities;
}

void igMeshMoverFSI::setFractionalStep(double step)
{
    this->fractional_step = step;
}

/////////////////////////////////////////////////////////////////////////////

void igMeshMoverFSI::initializeFractionalStep(void)
{
    this->interface_displacements_ref->resize(this->interface_displacements->size());
    this->interface_velocities_ref->resize(this->interface_velocities->size());

    for(int i=0; i<interface_displacements->size(); i++){
        this->interface_displacements_ref->at(i) = this->interface_displacements->at(i);
        this->interface_velocities_ref->at(i) = this->interface_velocities->at(i);
    }
}
