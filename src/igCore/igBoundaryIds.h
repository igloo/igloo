/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

// boundary ids for compressible flow solvers (Euler & NavierStokes)

const int id_slip_wall = 1;
const int id_slip_wall_fsi = 11;
const int id_noslip_wall = 3;
const int id_noslip_wall_fsi = 13;

const int id_io_riemann = 2;
const int id_io_density_velocity_pressure = 4;
const int id_io_supersonic = 5;

const int id_interior = 0;
const int id_periodic = -1;

// boundary ids for analytical cases for compressible flow solvers (Euler & NavierStokes)

const int id_max_std = 100;

const int id_vortex = 100;
const int id_cylinder = 101;
const int id_ringleb = 110;
const int id_shock_vortex_left = 102;
const int id_shock_vortex_right = 103;

// boundary ids for acoustics

const int id_acoustic_wall = 1;
const int id_acoustic_sym = 2;

// boundary ids for advection

const int id_advection = 100;
