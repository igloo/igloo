/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igCoreExport.h>

#include "igMeshMoverFSI.h"

#include <vector>
#include <functional>
#include <string>

using namespace std;

class igMesh;
class igElement;
class igFace;
class igInterface;
class igCommunicator;

class IGCORE_EXPORT igMeshMoverFSIPiston : public igMeshMoverFSI
{
public:
	igMeshMoverFSIPiston(igMesh *mesh, vector<double> *coordinates, vector<double> *velocities);
	virtual ~igMeshMoverFSIPiston(void);

public:
    void initializeMovement(const string& integrator, const string& movement_type, vector<double> *param_ALE, int max_refine_level) override;

protected:
    void retrieveParameters(int iel,int icp) override;

private:
    vector<double> *damping;

};

//
// igMeshMoverFSI.h ends here
