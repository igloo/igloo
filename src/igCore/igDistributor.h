/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igCoreExport.h>

#include <vector>

class igMesh;

using namespace std;

class IGCORE_EXPORT igDistributor
{
public:
     igDistributor(void);
    ~igDistributor(void);

public:
    void setMesh(igMesh *mesh);
    void setPartitionNumber(int number);
    void setPartitionNumber(int number1, int number2, int number3);
    void setElementNumber(int number1, int number2, int number3);
    void setSliding(bool flag_sliding);

public:
    void run(void);

public:
    int neighbourNumber(int part_id);
    int neighbourId(int part_id, int neighbour_id);
    int neighbourFaceNumber(int part_id, int neighbour_id);
    int neighbourDofNumber(int part_id, int neighbour_id);
    int elementNumber(int part_id);
    int dofNumber(int part_id);

private:

    int total_partition_number;
    int partition_number_1;
    int partition_number_2;
    int partition_number_3;

    int element_number_1;
    int element_number_2;
    int element_number_3;

    bool single_patch;
    bool sliding;

    vector<int> neighbour_number;
    vector<vector<int>> partition_map;
    vector<vector<int>> shared_face_map;
    vector<vector<int>> shared_dof_map;

    vector<int> element_numbers;
    vector<int> dof_numbers;

    igMesh *mesh;

};

//
// igCommunicator.h ends here
