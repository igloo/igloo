/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include "igFitting.h"

#include <igCoreExport>

#include <vector>

class igElement;

using namespace std;

class IGCORE_EXPORT igFittingRefinedSurface : public igFitting
{
public:
     igFittingRefinedSurface(void);
    ~igFittingRefinedSurface(void) = default;

public:
    void setElements(igElement *elt_parent, igElement *elt0, igElement *elt1, igElement *elt2, igElement *elt3);
    void setSolution(vector<double> *state, int variable_number);
    
public:
    void run(void);

private:
    double computeIntegralElementState(igElement *element,int ivar);
    double solutionAtGaussPoint(igElement *element, int index_pt, int component);
    
private:
    igElement *elt_parent;
    igElement *elt0;
    igElement *elt1;
    igElement *elt2;
    igElement *elt3;

    int variable_number;
    
    vector<double> *state;
    
};

//
// igFittingSurface.h ends here
