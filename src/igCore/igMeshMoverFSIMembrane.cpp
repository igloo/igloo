/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igMeshMoverFSIMembrane.h"

#include "igMesh.h"
#include "igCell.h"
#include "igElement.h"
#include "igFace.h"
#include "igInterface.h"
#include "igBoundaryIds.h"
#include "igBasisBSpline.h"

#include <igAssert.h>

#include <igGenerator/igMeshMovementFSIMembrane.h>
#include <igDistributed/igCommunicator.h>

#include <iostream>
#include <stdlib.h>
#include <cmath>
#include <limits>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igMeshMoverFSIMembrane::igMeshMoverFSIMembrane(igMesh *mesh, vector<double> *coordinates, vector<double> *velocities) : igMeshMoverFSI(mesh,coordinates,velocities)
{
	IG_ASSERT(mesh, "no mesh");

    // damping coefficients for level 0
    this->damping = new vector<double>;
    this->damping->resize(mesh->controlPointNumber(),0);

    double y_rigid = 0.1;
    double y_damp = 1.;

    int counter = 0;
    for (int iel = 0; iel < mesh->elementNumber(); ++iel){
        igElement *elt = mesh->element(iel);
        for(int idof=0; idof<elt->controlPointNumber(); idof++){

            double y_pt = this->coordinates->at(elt->globalId(idof,1));

            double damping_function_y = 0;
            if(-y_rigid <= y_pt && y_pt <= y_rigid){ // rigid deformation
                damping_function_y = 1;
            } else if (-y_damp <= y_pt && y_pt < -y_rigid){
                damping_function_y = cos(M_PI/2*(-y_rigid - y_pt)/(y_damp - y_rigid)); // damping y<0
            } else if (y_rigid < y_pt && y_pt <= y_damp){
                damping_function_y = cos(M_PI/2*(y_pt - y_rigid)/(y_damp - y_rigid)); // damping y>0
            }

            this->damping->at(counter) = damping_function_y;
            counter++;
        }
    }

    // movement mapping for level 0
    this->mover_map = new vector<int>;
    this->mover_map->resize(mesh->controlPointNumber(),-1);

}

igMeshMoverFSIMembrane::~igMeshMoverFSIMembrane(void)
{
    delete this->damping;
    delete this->mover_map;
}

/////////////////////////////////////////////////////////////////////////////

void igMeshMoverFSIMembrane::initializeMovement(const string& integrator, const string& movement_type,  vector<double> *param_ALE, int refine_max_level)
{
    this->movement_law = movement_type;
    this->movement_param = param_ALE;

    this->refine_max_level = refine_max_level;
    if(refine_max_level == 0)
        this->adaptive_mesh = false;
    else
        this->adaptive_mesh = true;

    this->mesh_movement_law = membraneDeformation;

    movement_param->resize(5);

    return;
}

void igMeshMoverFSIMembrane::initializeMapping(vector<double> *knot_vector, vector<double> *pt_x, vector<double> *pt_y)
{

    double x_min_membrane = 0;
    double x_max_membrane = 1;

    int ctrl_pt_number = pt_x->size();
    int degree = knot_vector->size() - ctrl_pt_number -1;
    int elt_number = ctrl_pt_number - degree;
    igBasisBSpline *basis = new igBasisBSpline(degree, knot_vector);

    // extraction of Bezier elements
    vector<vector<double>> elt_x;
    vector<vector<double>> elt_y;
    basis->extractBezier(pt_x, pt_y, &elt_x, &elt_y);

    // store left and right data for defomation
    dx_membrane_left = elt_x[0][1];
    dx_membrane_right = 1-elt_x[elt_number-1][degree-1];

    // loop over all elements
    int counter = 0;
    for (int iel = 0; iel < mesh->elementNumber(); ++iel){

        if(mesh->elementLevel(iel,0) == 0){

            igElement *elt = mesh->element(iel);

            // loop over all control points
            for(int idof=0; idof<elt->controlPointNumber(); idof++){

                double x_pt = this->coordinates->at(elt->globalId(idof,0));
                int index_x_min = -1;
                double dist_x_min = std::numeric_limits<double>::max();

                // search for corresponding interface point
                if(x_min_membrane <= x_pt && x_pt <= x_max_membrane){

                    // loop over membrane elements
                    for (int jel = 0; jel < elt_number; ++jel){

                        // loop over control points
                        for(int jdof=0; jdof<degree+1; jdof++){

                            double x_membrane = elt_x[jel][jdof];
                            double dist_x = fabs(x_pt - x_membrane);
                            if(dist_x < dist_x_min){
                                dist_x_min = dist_x;
                                index_x_min = jel*(degree+1)+jdof;
                            }
                        }
                    }

                }
                mover_map->at(counter) = index_x_min;
                counter++;
            }
        }

    }

    delete basis;
}

/////////////////////////////////////////////////////////////////////////////

void igMeshMoverFSIMembrane::retrieveParameters(int iel,int icp)
{
    int index = iel * dof_number_per_elt + icp;
    int index_map = mover_map->at(index);

    if(index_map >= 0){
        movement_param->at(0) = damping->at(index); // damping coefficient
        movement_param->at(1) = 0; // membrane velocity along x
        movement_param->at(2) = interface_velocities_ref->at(index_map)*(1-fractional_step) + interface_velocities->at(index_map)*fractional_step; // membrane velocity along y
        movement_param->at(3) = 0; // membrane displacement along x
        movement_param->at(4) = interface_displacements_ref->at(index_map)*(1-fractional_step) + interface_displacements->at(index_map)*fractional_step; // membrane displacement along y
    } else {
        movement_param->at(0) = 0; // damping coefficient
        movement_param->at(1) = 0; // membrane velocity along x
        movement_param->at(2) = 0; // membrane velocity along y
        movement_param->at(3) = 0; // membrane displacement along x
        movement_param->at(4) = 0; // membrane displacement along y
    }

}
