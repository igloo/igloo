/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igMeshMover.h"

#include "igMesh.h"
#include "igCell.h"
#include "igElement.h"
#include "igFace.h"
#include "igInterface.h"

#include <igAssert.h>

#include <igGenerator/igMeshMovement.h>
#include <igDistributed/igCommunicator.h>

#include <iostream>
#include <cmath>
#include <stdlib.h>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igMeshMover::igMeshMover(igMesh *mesh, vector<double> *coordinates, vector<double> *velocities)
{
	IG_ASSERT(mesh, "no mesh");

	this->rigid = false;
	this->mesh = mesh;
	this->coordinates = coordinates;
	this->velocities = velocities;

	this->point_coord = nullptr;
	this->point_vel = nullptr;
	this->initial_coord = nullptr;

    // storage of initial coordinates for level 0
    int dimension = mesh->meshDimension();
    this->initial_coord_level0 = new vector<double>;
    this->initial_coord_level0->resize(mesh->controlPointNumber()*dimension,0);

    int counter = 0;
    for (int iel = 0; iel < mesh->elementNumber(); ++iel){
        igElement *elt = mesh->element(iel);
        for(int idof=0; idof<elt->controlPointNumber(); idof++){
            for(int idim=0; idim<dimension; idim++){
                this->initial_coord_level0->at(counter) = this->coordinates->at(elt->globalId(idof,idim));
                counter++;
            }
        }
    }

    // working arays
    this->point_coord = new vector<double>(mesh->meshDimension());
    this->point_vel = new vector<double>(mesh->meshDimension());
    this->initial_coord = new vector<double>(mesh->meshDimension());
}

igMeshMover::~igMeshMover(void)
{
    delete initial_coord_level0;

	delete point_coord;
	delete point_vel;
    delete initial_coord;

}

/////////////////////////////////////////////////////////////////////////////

bool igMeshMover::rigidMotion(void)
{
	return rigid;
}

/////////////////////////////////////////////////////////////////////////////

void igMeshMover::setCommunicator(igCommunicator *communicator)
{
	this->communicator = communicator;
}


void igMeshMover::setDeformation(vector<double> *deformation)
{

}

void igMeshMover::setInterfaceFSI(vector<double> *displacements, vector<double> *velocities)
{

}

void igMeshMover::setFractionalStep(double step)
{

}

void igMeshMover::initializeFractionalStep(void)
{

}

/////////////////////////////////////////////////////////////////////////////


void igMeshMover::initializeMovement(const string& integrator, const string& movement_type, vector<double> *param_ALE, int refine_max_level)
{
    // select movement type
	this->movement_law = movement_type;
    this->movement_param = param_ALE;

	if(movement_type == "uniform"){
		mesh_movement_law = uniformLaw;
		rigid = true;
	}
	else if(movement_type == "rotation"){
		mesh_movement_law = rotationLaw;
		rigid = true;
	}
	else if(movement_type == "sinusoidal"){
		mesh_movement_law = sinusoidalLaw;
	}
	else if(movement_type == "gradual"){
		mesh_movement_law = gradualStart;
		rigid = true;
	}
	else if(movement_type == "smooth_heaving"){
		mesh_movement_law = oscillatingCylinder;
	}
	else if(movement_type == "rigid_heaving"){
		mesh_movement_law = oscillatingCylinder;
		rigid = true;
	}
	else if(movement_type == "airfoil_pitching"){
		mesh_movement_law = pitchingAirfoil;
	}
	else if(movement_type == "transonic_pitching"){
		mesh_movement_law = pitchingTransonic;
	}
	else if(movement_type == "ellipse_pitching"){
		mesh_movement_law = pitchingEllipse;
	}
    else if(movement_type == "oscillating_channel"){
		mesh_movement_law = oscillatingBodyChannel;
	}


	this->refine_max_level = refine_max_level;
	if(refine_max_level == 0)
		this->adaptive_mesh = false;
	else
		this->adaptive_mesh = true;

	return;
}


void igMeshMover::initializeVelocity(double time)
{
	mesh->setTime(time);

	//Initialize mesh at level 0
    this->evalCoordinateAndVelocity(time);

}

void igMeshMover::initializeMapping(vector<double> *knot_vector, vector<double> *pt_x, vector<double> *pt_y)
{

}

void igMeshMover::retrieveParameters(int iel,int icp)
{

}


/////////////////////////////////////////////////////////////////////////////


bool igMeshMover::move(double solver_time)
{
	if(solver_time != mesh->meshTime())
	{
		// Move mesh
        this->evalCoordinateAndVelocity(solver_time);

		mesh->setTime(solver_time);
		return true;
	} else
		return false;
}


/////////////////////////////////////////////////////////////////////////////

void igMeshMover::evalCoordinateAndVelocity(double solver_time)
{
    double time_step = solver_time - mesh->meshTime();
    int dimension = mesh->meshDimension();
    int counter = 0;

    // loop over elements
    for (int iel = 0; iel < mesh->elementNumber(); ++iel) {
        igElement *elt = mesh->element(iel);

        // move level 0 only
        if(mesh->elementLevel(iel,0) == 0){
            for(int icp=0; icp<elt->controlPointNumber(); icp++){

                for(int idim=0; idim<dimension; idim++){
                    (*point_coord)[idim] = this->coordinates->at(elt->globalId(icp,idim));
                    (*point_vel)[idim] = this->velocities->at(elt->globalId(icp,idim));
                    (*initial_coord)[idim] = this->initial_coord_level0->at(counter);
                    counter++;
                }

                // evaluate new coordinate and velocity
                this->retrieveParameters(iel,icp);
                this->mesh_movement_law(solver_time, time_step, point_coord, point_vel, movement_param, initial_coord, elt->subdomain());

                // update geometry fields
                elt->setControlPointCoordinates(icp,point_coord);
                elt->setControlPointVelocity(icp,point_vel);
            }
        }

    }

	// Use splitting to compute velocity and coordinates at higher levels
	if(this->adaptive_mesh)
		this->refineCoordinateAndVelocity();

	// MPI communication
	communicator->distributeField(coordinates, mesh->meshDimension()+1);
	communicator->distributeField(velocities, mesh->meshDimension());

    // Update elements
    for (int iel = 0; iel < mesh->elementNumber(); ++iel) {
        igElement *elt = mesh->element(iel);
        if(elt->isActive()){
            elt->initializeGeometry();
            elt->computeGaussPointVelocity();
        }
    }

	// Update inner faces
	for(int ifac = 0; ifac < mesh->faceNumber(); ifac++){
		igFace *face = mesh->face(ifac);
        if(face->isActive() || face->isParentSliding()){
            face->initializeGeometry();
			face->updateNormal();
			face->computeGaussPointVelocity();
        }
	}

	// Update boundary faces
	for (int ifac = 0; ifac < mesh->boundaryFaceNumber(); ++ifac) {
		igFace *face = mesh->boundaryFace(ifac);
        if(face->isActive()){
			face->initializeGeometry();
			face->updateNormal();
			face->computeGaussPointVelocity();
        }
	}

	if( !this->rigid && !adaptive_mesh)
		this->halfFaceCorrection();

}

/////////////////////////////////////////////////////////////////////////////

void igMeshMover::refineCoordinateAndVelocity(void)
{

	int index;
	int dimension = mesh->meshDimension();
	int degree = mesh->element(0)->cellDegree();
	int ctrl_pts_per_elt = (degree+1)*(degree+1);
	int variable_number = mesh->variableNumber();
	int field_to_split = 2*dimension+1;

	vector<double> coord_tmp;
	coord_tmp.resize(degree+1);
	vector<double> coord_curve1;
	coord_curve1.resize(degree+1);
	vector<double> coord_curve2;
	coord_curve2.resize(degree+1);
	vector<double> tmp_split1;
	tmp_split1.resize(ctrl_pts_per_elt*field_to_split);
	vector<double> tmp_split2;
	tmp_split2.resize(ctrl_pts_per_elt*field_to_split);

	for (int level_id = 0; level_id<refine_max_level; level_id++){

		for (int iel = 0; iel<mesh->elementNumber(); iel++){
			igElement *elt = mesh->element(iel);

			if(mesh->elementLevel(iel,0) == level_id){

				if(elt->hasChild()){

					igElement *elt0 = mesh->element(elt->child(0));
					igElement *elt1 = mesh->element(elt->child(1));
					igElement *elt2 = mesh->element(elt->child(2));
					igElement *elt3 = mesh->element(elt->child(3));

					// first loop over lines of control net => treatment of direction Xi
					for (int icurve=0; icurve<degree+1; icurve++){

						// treatment of components (geometrical + solution)
						for(int icomp=0; icomp<field_to_split; icomp++){

							// retrieve curve control points
							for(int ipt=0; ipt<degree+1; ipt++){

								index = (degree+1)*icurve + ipt;

								if(icomp < dimension) // geometry : coordinates * weights
									coord_tmp.at(ipt) = coordinates->at(elt->globalId(index, icomp)) *
									coordinates->at(elt->globalId(index, dimension));
								else if(icomp == dimension) // geometry : weights
									coord_tmp.at(ipt) = coordinates->at(elt->globalId(index, dimension));
								else //velocity * weights
									coord_tmp.at(ipt) = velocities->at(elt->globalId(index,icomp-dimension-1)) *
									coordinates->at(elt->globalId(index, dimension));
							}

							// keep extremities
							coord_curve1.at(0) = coord_tmp.at(0);
							coord_curve2.at(degree) = coord_tmp.at(degree);

							// subdivision procedure
							for (int istep=0; istep<degree; istep++){

								for (int ipt=0; ipt<degree-istep; ipt++)
									coord_tmp.at(ipt) = 0.5*coord_tmp.at(ipt) + 0.5*coord_tmp.at(ipt+1);

								coord_curve1.at(1+istep) = coord_tmp.at(0);
								coord_curve2.at(degree-1-istep) = coord_tmp.at(degree-1-istep);
							}

							// store control points
							for (int ipt=0; ipt<degree+1; ipt++){

								index = (degree+1)*icurve + ipt;

								tmp_split1.at(ctrl_pts_per_elt*icomp + index) = coord_curve1.at(ipt);
								tmp_split2.at(ctrl_pts_per_elt*icomp + index) = coord_curve2.at(ipt);
							}
						}
					}

					// second loop over lines of control net => treatment of direction Eta (first pair of elements)
					for (int icurve=0; icurve<degree+1; icurve++){

						// treatment of components (geometrical + solution)
						for(int icomp=0; icomp<field_to_split; icomp++){

							// retrieve curve control points
							for(int ipt=0; ipt<degree+1; ipt++){

								index = (degree+1)*ipt + icurve;

								coord_tmp.at(ipt) = tmp_split1.at(ctrl_pts_per_elt*icomp + index);
							}

							// keep extremities
							coord_curve1.at(0) = coord_tmp.at(0);
							coord_curve2.at(degree) = coord_tmp.at(degree);

							// subdivision procedure
							for (int istep=0; istep<degree; istep++){

								for (int ipt=0; ipt<degree-istep; ipt++)
									coord_tmp.at(ipt) = 0.5*coord_tmp.at(ipt) + 0.5*coord_tmp.at(ipt+1);

								coord_curve1.at(1+istep) = coord_tmp.at(0);
								coord_curve2.at(degree-1-istep) = coord_tmp.at(degree-1-istep);
							}

							// store control points
							for (int ipt=0; ipt<degree+1; ipt++){

								index = (degree+1)*ipt + icurve;

								if(icomp < dimension){ // geometry : coordinates *weights
									coordinates->at(elt0->globalId(index,icomp)) = coord_curve1.at(ipt);
									coordinates->at(elt2->globalId(index,icomp)) = coord_curve2.at(ipt);
								} else if (icomp == dimension){
									coordinates->at(elt0->globalId(index,dimension)) = coord_curve1.at(ipt);
									coordinates->at(elt2->globalId(index,dimension)) = coord_curve2.at(ipt);
								}else{
									velocities->at(elt0->globalId(index,icomp-dimension-1)) = coord_curve1.at(ipt);
									velocities->at(elt2->globalId(index,icomp-dimension-1)) = coord_curve2.at(ipt);
								}

							}
						}

						// recover NURBS formulation
						for(int icomp=0; icomp<dimension; icomp++){
							for (int ipt=0; ipt<degree+1; ipt++){

								index = (degree+1)*ipt + icurve;
								coordinates->at(elt0->globalId(index,icomp)) /= coordinates->at(elt0->globalId(index,dimension));
								coordinates->at(elt2->globalId(index,icomp)) /= coordinates->at(elt2->globalId(index,dimension));
							}
						}

						for(int icomp=0; icomp<dimension; icomp++){
							for (int ipt=0; ipt<degree+1; ipt++){

								index = (degree+1)*ipt + icurve;
								velocities->at(elt0->globalId(index,icomp)) /= coordinates->at(elt0->globalId(index,dimension));
								velocities->at(elt2->globalId(index,icomp)) /= coordinates->at(elt2->globalId(index,dimension));
							}
						}

					}

					// last loop over lines of control net => treatment of direction Eta (second pair of elements)
					for (int icurve=0; icurve<degree+1; icurve++){

						// treatment of components (geometrical + solution)
						for(int icomp=0; icomp<field_to_split; icomp++){

							// retrieve curve control points
							for(int ipt=0; ipt<degree+1; ipt++){
								index = (degree+1)*ipt + icurve;
								coord_tmp.at(ipt) = tmp_split2.at(ctrl_pts_per_elt*icomp + index);
							}

							// keep extremities
							coord_curve1.at(0) = coord_tmp.at(0);
							coord_curve2.at(degree) = coord_tmp.at(degree);

							// subdivision procedure
							for (int istep=0; istep<degree; istep++){

								for (int ipt=0; ipt<degree-istep; ipt++)
									coord_tmp.at(ipt) = 0.5*coord_tmp.at(ipt) + 0.5*coord_tmp.at(ipt+1);

								coord_curve1.at(1+istep) = coord_tmp.at(0);
								coord_curve2.at(degree-1-istep) = coord_tmp.at(degree-1-istep);
							}

							// store control points
							for (int ipt=0; ipt<degree+1; ipt++){

								index = (degree+1)*ipt + icurve;

								if(icomp < dimension){ // geometry
									coordinates->at(elt1->globalId(index,icomp)) = coord_curve1.at(ipt);
									coordinates->at(elt3->globalId(index,icomp)) = coord_curve2.at(ipt);
								} else if (icomp == dimension){
									coordinates->at(elt1->globalId(index,dimension)) = coord_curve1.at(ipt);
									coordinates->at(elt3->globalId(index,dimension)) = coord_curve2.at(ipt);
								} else {
									velocities->at(elt1->globalId(index,icomp-dimension-1)) = coord_curve1.at(ipt);
									velocities->at(elt3->globalId(index,icomp-dimension-1)) = coord_curve2.at(ipt);
								}
							}
						}

						// recover coordinates and weights
						for(int icomp=0; icomp<dimension; icomp++){
							for (int ipt=0; ipt<degree+1; ipt++){

								index = (degree+1)*ipt + icurve;
								coordinates->at(elt1->globalId(index,icomp)) /= coordinates->at(elt1->globalId(index,dimension));
								coordinates->at(elt3->globalId(index,icomp)) /= coordinates->at(elt3->globalId(index,dimension));
							}
						}

						for(int icomp=0; icomp<dimension; icomp++){
							for (int ipt=0; ipt<degree+1; ipt++){

								index = (degree+1)*ipt + icurve;
								velocities->at(elt1->globalId(index,icomp)) /= coordinates->at(elt1->globalId(index,dimension));
								velocities->at(elt3->globalId(index,icomp)) /= coordinates->at(elt3->globalId(index,dimension));
							}
						}
					}

					elt0->initializeGeometry();
					elt0->computeGaussPointVelocity();
					elt1->initializeGeometry();
					elt1->computeGaussPointVelocity();
					elt2->initializeGeometry();
					elt2->computeGaussPointVelocity();
					elt3->initializeGeometry();
					elt3->computeGaussPointVelocity();

				}
			}
		}
	}
}


void igMeshMover::halfFaceCorrection(void)
{
	int size = mesh->element(0)->controlPointNumberByDirection();
	vector<double> *x_coord = new vector<double> (size);
	vector<double> *y_coord = new vector<double> (size);
	vector<double> *w_coord = new vector<double> (size);
	vector<double> *x_vel = new vector<double> (size);
	vector<double> *y_vel = new vector<double> (size);
	vector<double> *split_x_coord = new vector<double> (size);
	vector<double> *split_y_coord = new vector<double> (size);
	vector<double> *split_w_coord = new vector<double> (size);
	vector<double> *split_x_vel = new vector<double> (size);
	vector<double> *split_y_vel = new vector<double> (size);

	double mid_param;
	int sense;
	bool first_child;

	for(int ifac = 0; ifac < mesh->faceNumber(); ifac++){

		igFace *face = mesh->face(ifac);

		double left_param_begin = face->leftParameterBegin();
		double left_param_end = face->leftParameterEnd();
		double right_param_begin = face->rightParameterBegin();
		double right_param_end = face->rightParameterEnd();
		double left_mid_param = 0.5*(left_param_begin + left_param_end);
		double right_mid_param = 0.5*(right_param_begin +right_param_end);

		bool left = face->halfFaceLeft();
		bool right = face->halfFaceRight();

		if(left||right){ // Hanging nodes detected

			if(left){
				sense = face->leftSense();
				mid_param = left_mid_param;
			} else {
				sense = face->rightSense();
				mid_param = right_mid_param;
			}

			if(mid_param<0.5){
				if(sense>0)
					first_child = true;
				else
					first_child = false;
			} else {
				if(sense>0)
					first_child = false;
				else
					first_child = true;
			}

			// get data to split
			for (int idof=0; idof<face->controlPointNumber(); idof++){

				if(left){
					x_coord->at(idof) = coordinates->at(face->dofLeft(idof, 0));
					y_coord->at(idof) = coordinates->at(face->dofLeft(idof, 1));
					w_coord->at(idof) = coordinates->at(face->dofLeft(idof, 2));
					x_vel->at(idof) = velocities->at(face->dofLeft(idof, 0));
					y_vel->at(idof) = velocities->at(face->dofLeft(idof, 1));
				} else {
					x_coord->at(idof) = coordinates->at(face->dofRight(idof, 0));
					y_coord->at(idof) = coordinates->at(face->dofRight(idof, 1));
					w_coord->at(idof) = coordinates->at(face->dofRight(idof, 2));
					x_vel->at(idof) = velocities->at(face->dofRight(idof, 0));
					y_vel->at(idof) = velocities->at(face->dofRight(idof, 1));
				}
			}

			// split data
			this->splitFace(x_coord,split_x_coord,y_coord,split_y_coord,w_coord,split_w_coord,first_child);
			this->splitFace(x_vel,split_x_vel,y_vel,split_y_vel,w_coord,split_w_coord,first_child);

			// update splitted data
			for(int idof=0; idof<face->controlPointNumber(); idof++){

				if(right){
					coordinates->at(face->dofLeft(idof, 0)) = split_x_coord->at(idof);
					coordinates->at(face->dofLeft(idof, 1)) = split_y_coord->at(idof);
					coordinates->at(face->dofLeft(idof, 2)) = split_w_coord->at(idof);
					velocities->at(face->dofLeft(idof, 0)) = split_x_vel->at(idof);
					velocities->at(face->dofLeft(idof, 1)) = split_y_vel->at(idof);
				} else {
					coordinates->at(face->dofRight(idof, 0)) = split_x_coord->at(idof);
					coordinates->at(face->dofRight(idof, 1)) = split_y_coord->at(idof);
					coordinates->at(face->dofRight(idof, 2)) = split_w_coord->at(idof);
					velocities->at(face->dofRight(idof, 0)) = split_x_vel->at(idof);
					velocities->at(face->dofRight(idof, 1)) = split_y_vel->at(idof);
				}
			}

			if(left && face->rightInside()){
				igElement *elt = mesh->element(face->rightElementIndex());
				elt->initializeGeometry();
				elt->computeGaussPointVelocity();
			}

			if(right && face->leftInside()){
				igElement *elt = mesh->element(face->leftElementIndex());
				elt->initializeGeometry();
				elt->computeGaussPointVelocity();
			}

		}
	}

	// MPI communication : necessary to get coordinates and velocicites after correction
	communicator->distributeField(coordinates, mesh->meshDimension()+1);
	communicator->distributeField(velocities, mesh->meshDimension());

	// final update of all faces -> splitted faces + middle faces (easier in parallel)
	for(int ifac = 0; ifac < mesh->faceNumber(); ifac++){
		igFace *face = mesh->face(ifac);
		face->initializeGeometry();
		face->updateNormal();
		face->computeGaussPointVelocity();
	}


	delete x_coord;
	delete y_coord;
	delete w_coord;
	delete x_vel;
	delete y_vel;
	delete split_x_coord;
	delete split_y_coord;
	delete split_w_coord;
	delete split_x_vel;
	delete split_y_vel;
}


void igMeshMover::splitFace(vector<double> *x_field, vector<double> *x_split_field,vector<double> *y_field, vector<double> *y_split_field,
		vector<double> *w_field, vector<double> *w_split_field,bool first_child)
{
	int degree = x_field->size() - 1;

	// move NURBS to  to n+1 space BSpline
	for (int ipt=0; ipt<degree+1; ipt++){
		x_field->at(ipt) *=  w_field->at(ipt);
		y_field->at(ipt) *=  w_field->at(ipt);
	}

	// keep extremities
	if(first_child){
		x_split_field->at(0) = x_field->at(0);
		y_split_field->at(0) = y_field->at(0);
		w_split_field->at(0) = w_field->at(0);
	} else {
		x_split_field->at(degree) = x_field->at(degree);
		y_split_field->at(degree) = y_field->at(degree);
		w_split_field->at(degree) = w_field->at(degree);
	}

	// subdivision procedure
	for (int istep=0; istep<degree; istep++){

		for (int ipt=0; ipt<degree-istep; ipt++){
			x_field->at(ipt) = 0.5*x_field->at(ipt) + 0.5*x_field->at(ipt+1);
			y_field->at(ipt) = 0.5*y_field->at(ipt) + 0.5*y_field->at(ipt+1);
			w_field->at(ipt) = 0.5*w_field->at(ipt) + 0.5*w_field->at(ipt+1);
		}

		if(first_child){
			x_split_field->at(1+istep) = x_field->at(0);
			y_split_field->at(1+istep) = y_field->at(0);
			w_split_field->at(1+istep) = w_field->at(0);
		} else {
			x_split_field->at(degree-1-istep) = x_field->at(degree-1-istep);
			y_split_field->at(degree-1-istep) = y_field->at(degree-1-istep);
			w_split_field->at(degree-1-istep) = w_field->at(degree-1-istep);
		}
	}

	// recover NURBS
	for (int ipt=0; ipt<degree+1; ipt++){
		x_split_field->at(ipt) /= w_split_field->at(ipt);
		y_split_field->at(ipt) /= w_split_field->at(ipt);
	}

}
