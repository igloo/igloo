/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igCoreExport.h>

#include "igRungeKutta.h"

#include <vector>

using namespace std;

class IGCORE_EXPORT igRungeKutta2 : public igRungeKutta
{
public:
     igRungeKutta2(void);
    ~igRungeKutta2(void);

public:
    void initialize(vector<double> *initial_solution, double initial_time);
    vector<double>* nextEvaluation(void);
    double nextTime(void);
    void setIncrement(vector<double> *increment);
    void updateSolution(void);

private:
    vector<double> increment1;
    vector<double> increment2;
};

//
// igRungeKutta4.h ends here
