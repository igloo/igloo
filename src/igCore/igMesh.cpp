/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igMesh.h"

#include "igMeshMover.h"
#include "igCell.h"
#include "igElement.h"
#include "igFace.h"
#include "igInterface.h"
#include "igBoundaryIds.h"

#include <fstream>
#include <iostream>
#include <iomanip>
#include <math.h>
#include <limits>

#include <igCommunicator.h>
#include <igAssert.h>

using namespace std;

igMesh::igMesh(void)
{
    this->communicator = nullptr;
    this->distributed_global_to_local = nullptr;

    this->face_number = 0;
    this->total_face_number = 0;
    this->active_face_number = 0;
    this->total_active_face_number = 0;
    this->interface_number = 0;
    this->element_number = 0;
    this->total_element_number = 0;
    this->active_element_number = 0;
    this->total_active_element_number = 0;
    this->boundary_face_number = 0;
    this->total_boundary_face_number = 0;
    this->active_boundary_face_number = 0;
    this->total_active_boundary_face_number = 0;

    this->shared_face_number = 0;
    this->shared_dof_number = 0;
    this->shared_elt_number = 0;

    this->time = 0.;

    this->element_level_list_0 = new vector<int>();
    this->element_level_list_1 = new vector<int>();

}

igMesh::~igMesh(void)
{
    for (int iel=0; iel < element_number; iel++) {
        delete element_list[iel];
    }
    for (int ifac=0; ifac < face_number; ifac++) {
        delete face_list[ifac];
    }
    for (int ibnd=0; ibnd < boundary_face_number; ibnd++) {
        delete boundary_list[ibnd];
    }
    for (int iifc=0; iifc < interface_number; iifc++) {
    	delete interface_list[iifc];
    }
}

///////////////////////////////////////////////////////////////

void igMesh::setCommunicator(igCommunicator *com)
{
    IG_ASSERT(com, "no Communicator");
    this->communicator = com;
}

void igMesh::setVariableNumber(int number)
{
    this->variable_number = number;
}

void igMesh::setDerivativeNumber(int number)
{
    this->derivative_number = number;
}

void igMesh::setDimension(int dimension)
{
    this->dimension = dimension;
}

void igMesh::setExactSolutionId(int id)
{
    this->exact_solution_id = id;
}

void igMesh::setPartitionNumber(int number)
{
    this->partition_number = number;
}

void igMesh::setCoordinates(vector<double> *coord)
{
    this->coordinates = coord;
}

void igMesh::setVelocities(vector<double> *vel)
{
    this->velocities = vel;
}

void igMesh::setTime(double time)
{
    this->time = time;
}

void igMesh::setElementLevel(int index, int direction, int level)
{
    if(direction == 0)
        this->element_level_list_0->at(index) = level;
    else
        this->element_level_list_1->at(index) = level;
}

///////////////////////////////////////////////////////////////

int igMesh::meshDimension(void)
{
    return dimension;
}

int igMesh::elementNumber(void)
{
    return element_number;
}

int igMesh::totalElementNumber(void)
{
    return total_element_number;
}

int igMesh::activeElementNumber(void)
{
    return active_element_number;
}

int igMesh::totalActiveElementNumber(void)
{
    return total_active_element_number;
}

int igMesh::faceNumber(void)
{
    return face_number;
}

int igMesh::totalFaceNumber(void)
{
    return total_face_number;
}

int igMesh::activeFaceNumber(void)
{
    return active_face_number;
}

int igMesh::totalActiveFaceNumber(void)
{
    return total_active_face_number;
}

int igMesh::boundaryFaceNumber(void)
{
    return boundary_face_number;
}

int igMesh::totalBoundaryFaceNumber(void)
{
    return total_boundary_face_number;
}

int igMesh::activeBoundaryFaceNumber(void)
{
    return active_boundary_face_number;
}

int igMesh::totalActiveBoundaryFaceNumber(void)
{
    return total_active_boundary_face_number;
}

int igMesh::interfaceNumber(void)
{
    return interface_number;
}

int igMesh::controlPointNumber(void)
{
    return this->control_point_number;
}

int igMesh::sharedControlPointNumber(void)
{
    return shared_dof_number;
}

int igMesh::sharedFaceNumber(void)
{
    return shared_face_number;
}

int igMesh::sharedElementNumber(void)
{
    return shared_elt_number;
}

int igMesh::variableNumber(void)
{
    return variable_number;
}

int igMesh::derivativeNumber(void)
{
    return derivative_number;
}

int igMesh::exactSolutionId(void)
{
    return exact_solution_id;
}

int igMesh::partitionNumber(void)
{
    return partition_number;
}

double igMesh::meshTime(void)
{
    return time;
}

igElement* igMesh::element(int index)
{
    return element_list.at(index);
}

igFace* igMesh::face(int index)
{
    return face_list.at(index);
}

igFace* igMesh::boundaryFace(int index)
{
    return boundary_list.at(index);
}

igInterface* igMesh::interface(int index)
{
    return interface_list.at(index);
}

double igMesh::minElementSize(void)
{
    double min_size = numeric_limits<double>::max();

    for (int iel=0; iel<element_number-1; iel++){
        igElement *elt = this->element(iel);
        igElement *elt_next = this->element(iel+1);
        min_size = min(min_size,fabs(elt_next->controlPointCoordinate(0,0) - elt->controlPointCoordinate(0,0)));
    }

    return min_size;
}

double igMesh::minCharacteristicSize(void)
{
    double min_size = numeric_limits<double>::max();

    for (int iel=0; iel<element_number-1; iel++){
        igElement *elt = this->element(iel);
        igElement *elt_next = this->element(iel+1);
        min_size = min(min_size,fabs(elt_next->controlPointCoordinate(0,0) - elt->controlPointCoordinate(0,0))/elt->controlPointNumber());
    }

    return min_size;
}

double igMesh::minCoordinateX(void)
{
    double xmin = numeric_limits<double>::max();

    for (int iel=0; iel<element_number; iel++){
        igElement *elt = this->element(iel);
        xmin = min(xmin,elt->barycenter(0));
    }
    return xmin;
}

double igMesh::maxCoordinateX(void)
{
    double xmax = numeric_limits<double>::min();

    for (int iel=0; iel<element_number; iel++){
        igElement *elt = this->element(iel);
        xmax = max(xmax,elt->barycenter(0));
    }
    return xmax;
}
double igMesh::minCoordinateY(void)
{
    double ymin = numeric_limits<double>::max();

    for (int iel=0; iel<element_number; iel++){
        igElement *elt = this->element(iel);
        ymin = min(ymin,elt->barycenter(1));
    }
    return ymin;
}
double igMesh::maxCoordinateY(void)
{
    double ymax = numeric_limits<double>::min();

    for (int iel=0; iel<element_number; iel++){
        igElement *elt = this->element(iel);
        ymax = max(ymax,elt->barycenter(1));
    }
    return ymax;
}

int igMesh::elementLevel(int index, int direction)
{
    if(direction == 0)
        return this->element_level_list_0->at(index);
    else
        return this->element_level_list_1->at(index);
}


void igMesh::updateMeshProperties(void)
{
    this->computeElementNumber();
    this->computeActiveElementNumber();

    this->computeControlPointNumber();

    this->computeFaceNumber();
    this->computeActiveFaceNumber();

    this->computeBoundaryFaceNumber();
    this->computeActiveBoundaryFaceNumber();

    this->computeInterfaceNumber();

    return;
}

void igMesh::updateGlobalMeshProperties(void)
{
    this->computeTotalElementNumber();

    this->computeTotalActiveElementNumber();

    this->computeTotalFaceNumber();

    this->computeTotalActiveFaceNumber();

    this->computeTotalBoundaryFaceNumber();

    this->computeTotalActiveBoundaryFaceNumber();

    return;
}

void igMesh::addElement(igElement *element)
{
    IG_ASSERT(element, "no element");

    element_list.push_back(element);

    return;
}

void igMesh::addFace(igFace *face)
{
    IG_ASSERT(face, "no face");

    face_list.push_back(face);
    return;
}

void igMesh::addBoundaryFace(igFace *face)
{
    IG_ASSERT(face, "no face");
    boundary_list.push_back(face);
    return;
}

void igMesh::addInterface(igInterface *interface)
{
    IG_ASSERT(interface, "no interface");

    interface_list.push_back(interface);
    return;
}

void igMesh::initializeGlobalToLocalMap(void)
{
    if(this->distributed_global_to_local)
        delete distributed_global_to_local;

    distributed_global_to_local = new vector<int>;
    for(int i=0; i<this->elementNumber(); i++)
        distributed_global_to_local->push_back(i);

}

////////////////////////////////////////////////////////////////

int igMesh::minDegree(void)
{
    int degree_min = numeric_limits<int>::max();

    for (int iel=0; iel<element_number; iel++){
        degree_min = min(degree_min,element_list.at(iel)->cellDegree());
        degree_min = min(degree_min,element_list.at(iel)->cellDegree());
        degree_min = min(degree_min,element_list.at(iel)->cellDegree());
    }

    return degree_min;
}

int igMesh::maxDegree(void)
{
    int degree_max = 0;

    for (int iel=0; iel<element_number; iel++){
        degree_max = max(degree_max,element_list.at(iel)->cellDegree());
        degree_max = max(degree_max,element_list.at(iel)->cellDegree());
        degree_max = max(degree_max,element_list.at(iel)->cellDegree());
    }
    return degree_max;
}

int igMesh::minControlPointNumber(void)
{
    int number_min = numeric_limits<int>::max();

    for (int iel=0; iel<element_number; iel++)
        number_min = min(number_min,element_list.at(iel)->controlPointNumber());

    return number_min;
}

int igMesh::maxControlPointNumber(void)
{
    int number_max = 0;

    for (int iel=0; iel<element_number; iel++)
        number_max = max(number_max,element_list.at(iel)->controlPointNumber());

    return number_max;
}

int igMesh::minGaussPointNumber(void)
{
    int number_min = numeric_limits<int>::max();

    for (int iel=0; iel<element_number; iel++){
        number_min = min(number_min,element_list.at(iel)->gaussPointNumber());
    }

    return number_min;
}

int igMesh::maxGaussPointNumber(void)
{
    int number_max = 0;

    for (int iel=0; iel<element_number; iel++){
        number_max = max(number_max,element_list.at(iel)->gaussPointNumber());
    }

    return number_max;
}

int igMesh::localElementId(int global_index)
{
    return distributed_global_to_local->at(global_index);
}

void igMesh::computeControlPointNumber(void)
{
    control_point_number = 0;

    for (int iel=0; iel<element_number; iel++){
            control_point_number += element_list.at(iel)->controlPointNumber();
    }

    return;
}

void igMesh::computeActiveElementNumber(void)
{
    active_element_number = 0;

    for (int iel=0; iel<element_number; iel++){
        if(element_list.at(iel)->isActive())
            active_element_number ++;
    }

    return;
}

void igMesh::computeElementNumber(void)
{
    element_number = element_list.size();
    return;
}

void igMesh::computeActiveFaceNumber(void)
{
    active_face_number = 0;

    for (int ifac=0; ifac<face_number; ifac++){
        if( face_list.at(ifac)->isActive() )
            active_face_number ++;
    }

    return;
}

void igMesh::computeFaceNumber(void)
{
    face_number = face_list.size();
    return;
}

void igMesh::computeActiveBoundaryFaceNumber(void)
{
    active_boundary_face_number = 0;

    for (int ibnd=0; ibnd<boundary_face_number; ibnd++){
        if(boundary_list.at(ibnd)->isActive())
            active_boundary_face_number ++;
    }

    return;
}

void igMesh::computeBoundaryFaceNumber(void)
{
    boundary_face_number = boundary_list.size();
    return;
}


void igMesh::computeTotalElementNumber(void)
{
    total_element_number = communicator->addInt(this->element_number);
}

void igMesh::computeTotalActiveElementNumber(void)
{
    total_active_element_number = communicator->addInt(this->active_element_number);
}

void igMesh::computeTotalFaceNumber(void)
{
    total_face_number = communicator->addInt(this->face_number);
}

void igMesh::computeTotalActiveFaceNumber(void)
{
    total_active_face_number = communicator->addInt(this->active_face_number);
}

void igMesh::computeTotalBoundaryFaceNumber(void)
{
    total_boundary_face_number = communicator->addInt(this->boundary_face_number);
}

void igMesh::computeTotalActiveBoundaryFaceNumber(void)
{
    total_active_boundary_face_number = communicator->addInt(this->active_boundary_face_number);
}

void igMesh::computeInterfaceNumber(void)
{
    interface_number = interface_list.size();
    return;
}

////////////////////////////////////////////////////////////////////////////


void igMesh::build(const string& filename)
{
    int my_id = 0;
    int total_face_number = 0;
    int total_boundary_face_number = 0;
    int partition_number = 0;

    int internal_dof_number = 0;

    if(communicator)
        my_id = communicator->rank();

    if(my_id == 0)
        cout << "construction from mesh file: " << filename << endl;

    ifstream mesh_file(filename.c_str(), ios::in);
    if(mesh_file.is_open()){

        // general mesh info
        mesh_file >> dimension;
        mesh_file >> total_element_number;
        mesh_file >> variable_number;
        mesh_file >> derivative_number;
        mesh_file >> exact_solution_id;

        total_active_element_number = total_element_number;

        // total number of partitions
        mesh_file >> partition_number;
        if((communicator)&&(communicator->size() != partition_number)) {
            if(my_id == 0)
                cout << '\n' << "Number of processes and partitions does not match" << endl;
            return;
        }

        shared_dof_number = 0;
        shared_face_number = 0;

        this->max_field_number = max(variable_number,derivative_number);
        this->max_field_number = max(max_field_number,dimension+1);

        //--------- construction of distributed partitions -----------
        for (int ipart=0; ipart<partition_number; ipart++) {

            if(ipart == my_id) {
                mesh_file >> internal_dof_number;

                int neighbor_number;
                mesh_file >> neighbor_number;

                partition_map = new vector<int>;
                partition_map->resize(neighbor_number);
                shared_dof_map = new vector<int>;
                shared_dof_map->resize(neighbor_number);
                send_dof_map = new vector< vector<int>* >;
                send_dof_map->resize(neighbor_number);
                receive_dof_map = new vector< vector<int>* >;
                receive_dof_map->resize(neighbor_number);
                shared_elt_map = new vector<int>;
                shared_elt_map->resize(neighbor_number);
                send_elt_map = new vector< vector<int>* >;
                send_elt_map->resize(neighbor_number);
                receive_elt_map = new vector< vector<int>* >;
                receive_elt_map->resize(neighbor_number);

                for (int ineigh=0; ineigh<neighbor_number; ineigh++){

                    int neighbor_id;
                    mesh_file >> neighbor_id;
                    partition_map->at(ineigh) = neighbor_id;

                    int neighbor_face_number;
                    mesh_file >> neighbor_face_number;
                    shared_face_number += neighbor_face_number;

                    int neighbor_dof_number;
                    mesh_file >> neighbor_dof_number;
                    shared_dof_number += neighbor_dof_number;

                    shared_dof_map->at(ineigh) = neighbor_dof_number;
                    shared_elt_map->at(ineigh) = neighbor_face_number;
                    send_dof_map->at(ineigh) = new vector<int>;
                    receive_dof_map->at(ineigh) = new vector<int>;
                    send_elt_map->at(ineigh) = new vector<int>;
                    receive_elt_map->at(ineigh) = new vector<int>;

                }

                // allocate array for coordinates and weights
                coordinates->assign((internal_dof_number+shared_dof_number)*(dimension+1), 0.);

                // allocate array for mesh velocity
                velocities->assign((internal_dof_number+shared_dof_number)*dimension, 0.);

                if (communicator) {
                    communicator->setPartitionMap(partition_map);
                    communicator->setSharedDofMap(shared_dof_map);
                    communicator->setSendDofMap(send_dof_map);
                    communicator->setReceiveDofMap(receive_dof_map);
                    communicator->setSharedEltMap(shared_elt_map);
                    communicator->setSendEltMap(send_elt_map);
                    communicator->setReceiveEltMap(receive_elt_map);

                    communicator->initializeDofBuffers(max_field_number);
                    communicator->initializeEltBuffers();
                }
            }
            else{
                int dof_number;
                mesh_file >> dof_number;
                int neighbor_number;
                mesh_file >> neighbor_number;
                for (int ineigh=0; ineigh<neighbor_number; ineigh++){
                    int neighbor_id;
                    mesh_file >> neighbor_id;
                    int neighbor_face_number;
                    mesh_file >> neighbor_face_number;
                    int neighbor_dof_number;
                    mesh_file >> neighbor_dof_number;
                }
            }
        }

        // array for global -> local element id
        distributed_global_to_local = new vector<int>;
        distributed_global_to_local->resize(total_element_number,-1);

        vector<double> *coord = new vector<double>;
        coord->resize(dimension);

        element_number = 0;
        face_number = 0;
        boundary_face_number = 0;

        int global_id_counter = 0;

        //---------- construction of element list -------------
        for (int iel=0; iel < total_element_number; iel++){

            int degree_by_direction;
            int gauss_point_number_by_direction;
            int partition_id;
            int subdomain_id;
            bool is_active;

            mesh_file >> degree_by_direction;
            mesh_file >> gauss_point_number_by_direction;
            mesh_file >> partition_id;
            mesh_file >> subdomain_id;
            mesh_file >> is_active;

            int dof_per_elt = degree_by_direction+1;
            if(dimension == 2)
                dof_per_elt = (degree_by_direction+1)*(degree_by_direction+1);
            if(dimension == 3)
                dof_per_elt = (degree_by_direction+1)*(degree_by_direction+1)*(degree_by_direction+1);

            if(partition_id == my_id){

                igElement *new_elt = new igElement;

                new_elt->setCellId(element_number);
                new_elt->setCellGlobalId(iel);
                new_elt->setPartition(partition_id);
                new_elt->setSubdomain(subdomain_id);
                new_elt->setPhysicalDimension(dimension);
                new_elt->setParametricDimension(dimension);
                new_elt->setDegree(degree_by_direction);
                new_elt->setGaussPointNumberByDirection(gauss_point_number_by_direction);
                new_elt->setVariableNumber(variable_number);
                new_elt->setDerivativeNumber(derivative_number);
                new_elt->setCoordinates(this->coordinates);
                new_elt->setVelocities(this->velocities);

                new_elt->initializeDegreesOfFreedom();

                // set dof map
                for (int ipt=0; ipt<new_elt->controlPointNumber(); ipt++){
                    for (int ivar=0; ivar<max_field_number; ivar++){
                        int index = (shared_dof_number+internal_dof_number)*ivar + global_id_counter;
                        new_elt->setGlobalId(ipt, ivar, index);
                    }
                    global_id_counter++;
                }

                // store control points and weights
                for (int ipt=0; ipt<new_elt->controlPointNumber(); ipt++){

                    for (int idim=0; idim<dimension; idim++)
                        mesh_file >> coordinates->at(new_elt->globalId(ipt, idim));
                    mesh_file >> coordinates->at(new_elt->globalId(ipt, dimension));

                }
                new_elt->initializeGeometry();

                // storage of the new element in the list
                element_list.push_back(new_elt);
                distributed_global_to_local->at(iel) = element_number;
                element_number++;

                if(is_active)
                    active_element_number++;
                else
                    new_elt->setInactive();

            }
            else{
                for (int i=0; i< dof_per_elt*(dimension+1); i++)
                    mesh_file >> coord->at(0);
            }

        }
        this->computeControlPointNumber();

        if(my_id == 0)
            cout << "elements list : ok"<< endl;

        //------------------  global id test ------------------------

        bool test_ok = true;
        int test_id_counter = 0;
        for (int ivar=0; ivar<max_field_number; ivar++){
            for (int iel=0; iel < element_number; iel++){ // internal dofs
                igElement *elt = this->element(iel);
                for (int ipt=0; ipt<elt->controlPointNumber(); ipt++){
                    if(elt->globalId(ipt, ivar) != test_id_counter)
                        test_ok = false;
                    test_id_counter++;
                }
            }
            test_id_counter += shared_dof_number; // shared dofs
        }

        if(my_id == 0)
            cout << "dof mapping : ok"<< endl;


        //-------------- construction of face list -----------------

        mesh_file >> total_face_number;

        int current_shared_id = 0;
        vector<double> left_elt_barycenter;
        left_elt_barycenter.assign(total_face_number*dimension, 0.);

        for (int ifac=0; ifac < total_face_number; ifac++){

            int degree_by_direction;
            int gauss_point_number_by_direction;
            int face_type;
            int face_orientation;
            int face_sense;
            int elt_left_index;
            int elt_right_index;
            int dof_index;
            int partition_id_left;
            int partition_id_right;
            bool left_inside = false;
            bool right_inside = false;
            bool is_active;
            double param_min_left1, param_max_left1, param_min_right1, param_max_right1;
            double param_min_left2, param_max_left2, param_min_right2, param_max_right2;

            mesh_file >> degree_by_direction;
            mesh_file >> gauss_point_number_by_direction;
            mesh_file >> face_type;
            mesh_file >> partition_id_left;
            mesh_file >> partition_id_right;
            mesh_file >> is_active;
            mesh_file >> param_min_left1;
            mesh_file >> param_max_left1;
            mesh_file >> param_min_right1;
            mesh_file >> param_max_right1;
            mesh_file >> param_min_left2;
            mesh_file >> param_max_left2;
            mesh_file >> param_min_right2;
            mesh_file >> param_max_right2;

            int dof_per_face = 1;
            if(dimension == 2)
                dof_per_face = degree_by_direction+1;
            if(dimension == 3)
                dof_per_face = (degree_by_direction+1)*(degree_by_direction+1);

            if(partition_id_left == my_id)
                left_inside = true;
            if(partition_id_right == my_id)
                right_inside = true;

            if(left_inside || right_inside){

                igFace *new_face = new igFace;

                new_face->setCellId(face_number);
                new_face->setCellGlobalId(ifac);
                new_face->setPhysicalDimension(dimension);
                new_face->setParametricDimension(dimension-1);
                new_face->setDegree(degree_by_direction);
                new_face->setGaussPointNumberByDirection(gauss_point_number_by_direction);
                new_face->setVariableNumber(variable_number);
                new_face->setDerivativeNumber(derivative_number);
                new_face->setType(face_type);
                new_face->setLeftPartitionId(partition_id_left);
                new_face->setRightPartitionId(partition_id_right);
                new_face->setParameterIntervalLeft(param_min_left1,param_max_left1,param_min_left2,param_max_left2);
                new_face->setParameterIntervalRight(param_min_right1,param_max_right1,param_min_right2,param_max_right2);

                if(left_inside)
                    new_face->setLeftInside();
                if(right_inside)
                    new_face->setRightInside();

                new_face->setCoordinates(this->coordinates);
                new_face->setVelocities(this->velocities);

                new_face->initializeDegreesOfFreedom();

                mesh_file >> elt_left_index;
                if(left_inside)
                    new_face->setLeftElementIndex(distributed_global_to_local->at(elt_left_index));

                mesh_file >> face_orientation;
                new_face->setOrientationLeft(face_orientation);
                if(left_inside){
                    igElement *left_elt = this->element(distributed_global_to_local->at(elt_left_index));
                    left_elt->setFaceId(face_orientation,face_number);
                }
                mesh_file >> face_sense;
                new_face->setSenseLeft(face_sense);
                for (int idof=0; idof<new_face->controlPointNumber(); idof++){
                    mesh_file >> dof_index;
                    for (int ivar=0; ivar<max_field_number; ivar++){
                        int index;
                        if(left_inside){
                            index = this->element(distributed_global_to_local->at(elt_left_index))->globalId(dof_index,ivar);
                            new_face->setLeftDofMap(idof,ivar,index);
                        }
                        else{
                            new_face->setShared();
                        }
                    }
                    new_face->setLeftPointMap(idof, dof_index);
                }

                mesh_file >> elt_right_index;
                if(right_inside)
                    new_face->setRightElementIndex(distributed_global_to_local->at(elt_right_index));

                mesh_file >> face_orientation;
                new_face->setOrientationRight(face_orientation);
                if(right_inside){
                    igElement *right_elt = this->element(distributed_global_to_local->at(elt_right_index));
                    right_elt->setFaceId(face_orientation,face_number);
                }
                mesh_file >> face_sense;
                new_face->setSenseRight(face_sense);

                for (int idof=0; idof<new_face->controlPointNumber(); idof++){
                    mesh_file >> dof_index;
                    for (int ivar=0; ivar<max_field_number; ivar++){
                        int index;
                        if(right_inside){
                            index = this->element(distributed_global_to_local->at(elt_right_index))->globalId(dof_index,ivar);
                            new_face->setRightDofMap(idof,ivar,index);
                        }
                        else{
                            new_face->setShared();
                        }
                    }
                    new_face->setRightPointMap(idof, dof_index);
                }

                for (int idim=0; idim<dimension; idim++)
                    mesh_file >> left_elt_barycenter.at(ifac*dimension+idim);

                // storage in the face list
                face_list.push_back(new_face);
                face_number++;

                if(is_active)
                    active_face_number++;
                else
                    new_face->setInactive();

                // update shared counter
                if(left_inside && !right_inside)
                    current_shared_id++;
                if(!left_inside && right_inside)
                    current_shared_id++;

            }
            else{
                for (int i=0; i< 2*dof_per_face+6; i++)
                    mesh_file >> dof_index;
                for (int i=0; i< dimension; i++)
                    mesh_file >> coord->at(0);
            }

        }

        // initialise send-recv maps and set external dof
        this->refreshCommunicatorMaps();

        // initialize level arrays
        element_level_list_0->assign(element_number+shared_elt_number,0);
        element_level_list_1->assign(element_number+shared_elt_number,0);

        //------------- communication of coordinates array ---------

       communicator->distributeField(coordinates, dimension+1);

        for(auto &face : this->face_list){

            face->initializeGeometry();

            for(int idim=0; idim<dimension; idim++)
                coord->at(idim) = left_elt_barycenter.at(face->cellGlobalId()*dimension+idim);
            face->initializeNormal(coord);

        }

        if(my_id == 0)
            cout << "faces list : ok"<< endl;

        mesh_file >> total_boundary_face_number;

        //-------------- construction of boundary face list -----------------
        for (int ibnd=0; ibnd < total_boundary_face_number; ibnd++){

            int degree_by_direction;
            int gauss_point_number_by_direction;
            int elt_left_index;
            int dof_int_index;
            int face_type;
            int face_orientation;
            int face_sense;
            int partition_id;
            bool is_active;
            double param_min_left1;
            double param_max_left1;
            double param_min_left2;
            double param_max_left2;

            mesh_file >> degree_by_direction;
            mesh_file >> gauss_point_number_by_direction;
            mesh_file >> face_type;
            mesh_file >> partition_id;
            mesh_file >> is_active;
            mesh_file >> param_min_left1;
            mesh_file >> param_max_left1;
            mesh_file >> param_min_left2;
            mesh_file >> param_max_left2;

            int dof_per_face = 1;
            if(dimension == 2)
                dof_per_face = degree_by_direction+1;
            if(dimension == 3)
                dof_per_face = (degree_by_direction+1)*(degree_by_direction+1);

            if(partition_id == my_id){

                igFace *new_face = new igFace;

                new_face->setCellId(boundary_face_number);
                new_face->setCellGlobalId(ibnd);
                new_face->setPhysicalDimension(dimension);
                new_face->setParametricDimension(dimension-1);
                new_face->setDegree(degree_by_direction);
                new_face->setGaussPointNumberByDirection(gauss_point_number_by_direction);
                new_face->setVariableNumber(variable_number);
                new_face->setDerivativeNumber(derivative_number);
                new_face->setType(face_type);
                new_face->setLeftPartitionId(partition_id);
                new_face->setParameterIntervalLeft(param_min_left1,param_max_left1,param_min_left2,param_max_left2);
                new_face->setCoordinates(this->coordinates);
                new_face->setVelocities(this->velocities);
                new_face->setLeftInside();

                new_face->initializeDegreesOfFreedom();

                mesh_file >> elt_left_index;
                new_face->setLeftElementIndex(distributed_global_to_local->at(elt_left_index));
                mesh_file >> face_orientation;
                new_face->setOrientationLeft(face_orientation);
                mesh_file >> face_sense;
                new_face->setSenseLeft(face_sense);
                for (int idof=0; idof<new_face->controlPointNumber(); idof++){
                    mesh_file >> dof_int_index;
                    for (int ivar=0; ivar<max_field_number; ivar++){
                        int index = this->element(distributed_global_to_local->at(elt_left_index))->globalId(dof_int_index,ivar);
                        new_face->setLeftDofMap(idof,ivar,index);
                    }
                    new_face->setLeftPointMap(idof, dof_int_index);
                }

                for (int idim=0; idim<dimension; idim++)
                    mesh_file >> coord->at(idim);

                new_face->initializeGeometry();
                new_face->initializeNormal(coord);

                // storage in the boundary face list
                boundary_list.push_back(new_face);
                boundary_face_number++;

                if(is_active)
                    active_boundary_face_number++;
                else
                    new_face->setInactive();

            }
            else{
                for (int i=0; i< dof_per_face+3; i++)
                    mesh_file >> dof_int_index;
                for (int i=0; i< dimension; i++)
                    mesh_file >> coord->at(0);
            }

        }
        if(my_id == 0){
            cout << "boundaries list : ok"<< endl;
            cout << endl;
        }

        delete coord;

    } else
        cout << "problem to read mesh file" << endl;


    // provide some statistics

    if(my_id == 0){
        cout << "number of partitions: " << partition_number << endl;
        cout << "number of elements: " << total_element_number << endl;
        cout << "number of interior faces: " << total_face_number << endl;
        cout << "number of boundary faces: " << total_boundary_face_number << endl;
        cout << "number of variables: " << variable_number << endl;
        cout << "element degree: " << maxDegree() << endl;
        cout << "number of control points per element: " << maxControlPointNumber() << endl;
        cout << "number of dofs: " << maxControlPointNumber()*total_element_number << endl;
        cout << "number of Gauss points per element: " << maxGaussPointNumber() << endl;
        cout << endl;
    }

    return;
}

////////////////////////////////////////////////////////////////////////////

void igMesh::refreshCommunicatorMaps(void)
{
    int index_elt_left;
    int index_dof_left;
    int index_elt_right;
    int index_dof_right;

    int my_id = communicator->rank();
    int partition_number = communicator->size();
    int neighbor_number = this->partition_map->size();

    // clean maps
    for (int neighbor_id=0; neighbor_id<neighbor_number; neighbor_id++){
        this->shared_dof_map->at(neighbor_id) = 0;
        this->shared_elt_map->at(neighbor_id) = 0;
        send_dof_map->at(neighbor_id)->clear();
        receive_dof_map->at(neighbor_id)->clear();
        send_elt_map->at(neighbor_id)->clear();
        receive_elt_map->at(neighbor_id)->clear();
    }

    shared_dof_number = 0;
    shared_face_number = 0;
    shared_elt_number = 0;

    // update number of shared data
    for (int ifac=0; ifac<face_number; ifac++){

        igFace *face = this->face(ifac);

        if(face->isActive() && face->shared()){

            int left_partition_id = face->leftPartitionId();
            int right_partition_id = face->rightPartitionId();

            shared_face_number++;

            if(left_partition_id == my_id){
                int neighbor_id = 0;
                bool next_part = true;
                do{
                    if(right_partition_id == partition_map->at(neighbor_id)){
                        shared_elt_map->at(neighbor_id)++;
                        shared_dof_map->at(neighbor_id) += face->controlPointNumber();
                        if( face->childRank() == 0 || face->halfFaceLeft() || ( face->halfFaceRight() && face->childRank()==1 ) ){
                            shared_dof_number += face->controlPointNumber();
                            shared_elt_number++;
                        }
                        next_part = false;
                    }
                    else
                        neighbor_id++;
                } while(next_part);
            }

            if(right_partition_id == my_id){
                int neighbor_id = 0;
                bool next_part = true;
                do{
                    if(left_partition_id == partition_map->at(neighbor_id)){
                        shared_elt_map->at(neighbor_id)++;
                        shared_dof_map->at(neighbor_id) += face->controlPointNumber();
                        if( face->childRank() == 0 || face->halfFaceRight() || ( face->halfFaceLeft() && face->childRank()==1 ) ){
                            shared_dof_number += face->controlPointNumber();
                            shared_elt_number++;
                        }
                        next_part = false;
                    }
                    else
                        neighbor_id++;
                } while(next_part);
            }
        }
    }

    // resize communication buffers
    communicator->resizeDofBuffers(max_field_number);
    communicator->resizeEltBuffers();

    // update dof communication maps
    for (int ivar=0; ivar<max_field_number; ivar++){

        int current_shared_dof_id = ivar*(control_point_number+shared_dof_number) + control_point_number;

        for (int ifac=0; ifac<face_number; ifac++){

            igFace *face = face_list.at(ifac);

            if(face->isActive() && face->shared()){

                int left_partition_id = face->leftPartitionId();
                int right_partition_id = face->rightPartitionId();

                for (int idof=0; idof<face->controlPointNumber(); idof++){

                    if(face->leftInside()){
                        index_dof_left = face->dofLeft(idof,ivar);
                    } else {
                        index_dof_left = current_shared_dof_id;
                        face->setLeftDofMap(idof,ivar,index_dof_left);
                    }

                    if(face->rightInside()){
                        index_dof_right = face->dofRight(idof,ivar);
                    } else {
                        index_dof_right = current_shared_dof_id;
                        face->setRightDofMap(idof,ivar,index_dof_right);
                    }

                    if(left_partition_id == my_id){
                        int neighbor_id = 0;
                        bool next_part = true;
                        do{
                            if(right_partition_id == partition_map->at(neighbor_id)){
                                send_dof_map->at(neighbor_id)->push_back(index_dof_left);
                                receive_dof_map->at(neighbor_id)->push_back(index_dof_right);
                                next_part = false;
                            }
                            else
                                neighbor_id++;
                        } while(next_part);
                    }

                    if(right_partition_id == my_id){
                        int neighbor_id = 0;
                        bool next_part = true;
                        do{
                            if(left_partition_id == partition_map->at(neighbor_id)){
                                send_dof_map->at(neighbor_id)->push_back(index_dof_right);
                                receive_dof_map->at(neighbor_id)->push_back(index_dof_left);
                                next_part = false;
                            }
                            else
                                neighbor_id++;
                        } while(next_part);
                    }
                    current_shared_dof_id++;
                }

                // increase counter only when it is required
                bool increase = false;
                if(left_partition_id == my_id){
                    if( face->childRank() == 0 || face->halfFaceLeft() || ( face->halfFaceRight() && face->childRank()==2 ) )
                        increase = true;
                }
                if(right_partition_id == my_id){
                    if( face->childRank() == 0 || face->halfFaceRight() || ( face->halfFaceLeft() && face->childRank()==2 ) )
                        increase = true;
                }

                if(!increase){
                    current_shared_dof_id -= face->controlPointNumber();
                }
            }
        }
    }

    // update elements communication maps
    int current_shared_elt_id = this->element_number;

    for (int ifac=0; ifac<face_number; ifac++){

        igFace *face = face_list.at(ifac);

        if(face->isActive() && face->shared()){

            int left_partition_id = face->leftPartitionId();
            int right_partition_id = face->rightPartitionId();

            if(face->leftInside()){
                index_elt_left = face->leftElementIndex();
            } else {
                index_elt_left = current_shared_elt_id;
                face->setLeftElementIndex(index_elt_left);
            }

            if(face->rightInside()){
                index_elt_right = face->rightElementIndex();
            } else {
                index_elt_right = current_shared_elt_id;
                face->setRightElementIndex(index_elt_right);
            }

            if(left_partition_id == my_id){
                int neighbor_id = 0;
                bool next_part = true;
                do{
                    if(right_partition_id == partition_map->at(neighbor_id)){
                        send_elt_map->at(neighbor_id)->push_back(index_elt_left);
                        receive_elt_map->at(neighbor_id)->push_back(index_elt_right);
                        next_part = false;
                    }
                    else
                        neighbor_id++;
                } while(next_part);
            }

            if(right_partition_id == my_id){
                int neighbor_id = 0;
                bool next_part = true;
                do{
                    if(left_partition_id == partition_map->at(neighbor_id)){
                        send_elt_map->at(neighbor_id)->push_back(index_elt_right);
                        receive_elt_map->at(neighbor_id)->push_back(index_elt_left);
                        next_part = false;
                    }
                    else
                        neighbor_id++;
                } while(next_part);
            }
            current_shared_elt_id++;

            // increase counter only when it is required
            bool increase = false;
            if(left_partition_id == my_id){
                if( face->childRank() == 0 || face->halfFaceLeft() || ( face->halfFaceRight() && face->childRank()==2 ) )
                    increase = true;
            }
            if(right_partition_id == my_id){
                if( face->childRank() == 0 || face->halfFaceRight() || ( face->halfFaceLeft() && face->childRank()==2 ) )
                    increase = true;
            }

            if(!increase){
                current_shared_elt_id--;
            }

        }
    }


}

void igMesh::refreshLevelArray(int new_elt_number, int new_shared_elt_number)
{
    // tmp array
    vector<int> *level_tmp = new vector<int>;
    level_tmp->resize(element_level_list_0->size(),-1);

    // copy existing array data direction 0
    for(int i=0; i<element_level_list_0->size(); i++){
        level_tmp->at(i) = element_level_list_0->at(i);
    }

    // new array direction 0
    element_level_list_0->assign(new_elt_number+new_shared_elt_number,-1);

    // copy current data in new array direction 0
    for(int iel=0; iel<element_number; iel++){
        element_level_list_0->at(iel) = level_tmp->at(iel);
    }

    // copy existing array data direction 1
    for(int i=0; i<element_level_list_1->size(); i++){
        level_tmp->at(i) = element_level_list_1->at(i);
    }

    // new array direction 1
    element_level_list_1->assign(new_elt_number+new_shared_elt_number,-1);

    // copy current data in new array direction 1
    for(int iel=0; iel<element_number; iel++){
        element_level_list_1->at(iel) = level_tmp->at(iel);
    }

    delete level_tmp;

}

void igMesh::distributeLevelArray(void){

    communicator->distributeElementArrayInt(this->element_level_list_0);

}

////////////////////////////////////////////////////////////////////////////

void igMesh::cleanInactiveCells(vector<double> *solution){

    int degree = element_list.at(0)->cellDegree();
    int dof_per_elt = (degree+1)*(degree+1);

    bool iterate = true;

    // loop over elements to remove inactive ones
    int iel=0;
    while(iterate){

        igElement *elt = this->element(iel);
        if(!elt->isActive()){

            // remove useless element
            delete element_list.at(iel);

            // first : move next elements
            for (int jel=iel; jel<element_number-1; jel++){
                element_list.at(jel) = element_list.at(jel+1);
                element_list.at(jel)->setCellId(jel);
            }

            // clean
            element_list.pop_back();
            element_number--;

            // update solution vector
            for(int ivar=0; ivar<variable_number; ivar++){
                for (int jel=iel; jel<element_number; jel++){
                    int index_beg = element_number*dof_per_elt*ivar + jel*dof_per_elt;
                    for(int idof=0; idof<dof_per_elt; idof++){
                        solution->at(index_beg+idof) = solution->at(index_beg+idof+dof_per_elt);
                    }
                }
                solution->erase(solution->begin()+element_number*dof_per_elt*(ivar+1)-dof_per_elt, solution->begin()+element_number*dof_per_elt*(ivar+1));
            }

            // update coordinate vector
            for(int ivar=0; ivar<meshDimension()+1; ivar++){
                for (int jel=iel; jel<element_number; jel++){
                    int index_beg = element_number*dof_per_elt*ivar + jel*dof_per_elt;
                    for(int idof=0; idof<dof_per_elt; idof++){
                        coordinates->at(index_beg+idof) = coordinates->at(index_beg+idof+dof_per_elt);
                    }
                }
                coordinates->erase(coordinates->begin()+element_number*dof_per_elt*(ivar+1)-dof_per_elt, coordinates->begin()+element_number*dof_per_elt*(ivar+1));
            }


            // update velocity vector
            for(int ivar=0; ivar<meshDimension(); ivar++){
                for (int jel=iel; jel<element_number; jel++){
                    int index_beg = element_number*dof_per_elt*ivar + jel*dof_per_elt;
                    for(int idof=0; idof<dof_per_elt; idof++){
                        velocities->at(index_beg+idof) = velocities->at(index_beg+idof+dof_per_elt);
                    }
                }
                velocities->erase(velocities->begin()+element_number*dof_per_elt*(ivar+1)-dof_per_elt, velocities->begin()+element_number*dof_per_elt*(ivar+1));
            }


            // second : update of faces
            for(auto *face : this->face_list){
                if(face->isActive()){

                    if(face->leftElementIndex()>iel){
                        int current_left_id = face->leftElementIndex();
                        face->setLeftElementIndex(current_left_id-1);
                        for(int ivar=0; ivar<variable_number; ivar++){
                            for(int idof=0; idof<face->controlPointNumber(); idof++){
                                int current_index = face->dofLeft(idof, ivar);
                                face->setLeftDofMap(idof, ivar, current_index-dof_per_elt);
                            }
                        }
                    }
                    if(face->rightElementIndex()>iel){
                        int current_right_id = face->rightElementIndex();
                        face->setRightElementIndex(current_right_id-1);
                        for(int ivar=0; ivar<variable_number; ivar++){
                            for(int idof=0; idof<face->controlPointNumber(); idof++){
                                int current_index = face->dofRight(idof, ivar);
                                face->setRightDofMap(idof, ivar, current_index-dof_per_elt);
                            }
                        }
                    }

                }
            }

            // third : update of boundary faces
            for(auto *face : this->boundary_list){
                if(face->isActive()){

                    if(face->leftElementIndex()>iel){
                        int current_left_id = face->leftElementIndex();
                        face->setLeftElementIndex(current_left_id-1);
                        for(int ivar=0; ivar<variable_number; ivar++){
                            for(int idof=0; idof<face->controlPointNumber(); idof++){
                                int current_index = face->dofLeft(idof, ivar);
                                face->setLeftDofMap(idof, ivar, current_index-dof_per_elt);
                            }
                        }
                    }

                }
            }

        } else {
            iel++;
        }

        if(iel == element_number)
            iterate = false;
    }

    // update element map
    int current_index = 0;
    int max_field_number = max(variable_number,dimension+1);
    for(int ivar=0; ivar<max_field_number; ivar++){
        for (auto *elt : this->element_list){
            for(int idof=0; idof<elt->controlPointNumber(); idof++){
                elt->setGlobalId(idof, ivar, current_index);
                current_index++;
            }
        }
    }


    // loop over faces to remove inactive ones
    iterate = true;
    int ifac = 0;
    while(iterate){

        igFace *face = this->face(ifac);
        if(!face->isActive()){

            // remove useless face
            delete face_list.at(ifac);

            // move next faces
            for (int jfac=ifac; jfac<face_number-1; jfac++){
                face_list.at(jfac) = face_list.at(jfac+1);
                face_list.at(jfac)->setCellId(jfac);
            }

            // clean
            face_list.pop_back();
            face_number--;

        } else {
            ifac++;
        }

        if(ifac == face_number)
            iterate =false;

    }

    // loop over boundaries to remove inactive ones
    iterate = true;
    ifac = 0;
    while(iterate){

        igFace *face = this->boundaryFace(ifac);
        if(!face->isActive()){

            // remove useless boundary
            delete boundary_list.at(ifac);

            // move next faces
            for (int jfac=ifac; jfac<boundary_face_number-1; jfac++){
                boundary_list.at(jfac) = boundary_list.at(jfac+1);
                boundary_list.at(jfac)->setCellId(jfac);
            }

            // clean
            boundary_list.pop_back();
            boundary_face_number--;

        } else {
            ifac++;
        }

        if(ifac == boundary_face_number)
            iterate =false;
    }


    cout << endl;
    cout << "After cleaning:" << endl;
    cout << "Number of elements: " << this->elementNumber() << endl;
    cout << "Number of faces: " << this->faceNumber() << endl;
    cout << "Number of boundary faces: " << this->boundaryFaceNumber() << endl;
}

////////////////////////////////////////////////////////////////////////////

void igMesh::linearize(void)
{

    //Linearize elements
    for(auto& elt : this->element_list){

        // linearize element boundaries
        this->linearizeBoundary(elt,0,0);
        this->linearizeBoundary(elt,elt->controlPointNumber() - elt->controlPointNumberByDirection(),0);
        this->linearizeBoundary(elt,0,1);
        this->linearizeBoundary(elt,elt->controlPointNumberByDirection()-1,1);

        // linearization of interior points (Coons formula)
        this->coonsMethod(elt);

        elt->initializeGeometry();
    }

    //compute velocity for inner faces
     for(auto& face : this->face_list){
         face->initializeGeometry();
         face->updateNormal();
     }

     //compute velocity for boundary faces
     for(auto& face : this->boundary_list){
         face->initializeGeometry();
         face->updateNormal();
     }

     igMeshMover *mover = new igMeshMover(this,this->coordinates,this->velocities);
     mover->setCommunicator(this->communicator);
     mover->halfFaceCorrection();

     delete mover;
}


void igMesh::linearizeBoundary(igElement *elt, int start, int direction)
{
    int degree = elt->cellDegree();;
    int H = 1;
    vector<double> *coord_0 = new vector<double>(this->dimension);
    vector<double> *coord_p = new vector<double>(this->dimension);
    vector<double> *coord_old = new vector<double>(this->dimension);
    vector<double> *coord_new = new vector<double>(this->dimension);

    if(direction==1)
        H = elt->controlPointNumberByDirection();

    vector<double> tau(this->dimension);
    vector<double> temp(this->dimension);
    double norm_square = 0.;

    elt->controlPointCoordinates(start,coord_0);
    elt->controlPointCoordinates(start+degree*H,coord_p);

    // Compute tangent unit vector of linear face
    for(int i=0; i<this->dimension; i++){
        tau[i] = coord_p->at(i) - coord_0->at(i);
        norm_square += tau[i]*tau[i];
    }
    for(int i=0; i<this->dimension; i++)
        tau[i] = tau[i]/sqrt(norm_square);

    // Compute projection
    for(int i=0; i<elt->controlPointNumberByDirection(); i++){
        elt->controlPointCoordinates(start+i*H,coord_old);

        double scalar = 0.;
        for(int j=0; j<this->dimension; j++)
            scalar += (coord_old->at(j)-coord_0->at(j))*tau[j];

        for(int j=0; j<this->dimension; j++)
            temp[j] = scalar*tau[j];

        for(int j=0; j<this->dimension; j++)
            coord_new->at(j) = coord_0->at(j) + temp[j];

        elt->setControlPointCoordinates(start+i*H,coord_new);
        elt->setControlPointWeight(start+i*H,1.);
    }

    delete coord_0;
    delete coord_p;
    delete coord_old;
    delete coord_new;
}


void igMesh::coonsMethod(igElement *elt)
{

    vector<double> *X_00 = new vector<double> (this->dimension);
    vector<double> *X_p0 = new vector<double> (this->dimension);
    vector<double> *X_0p = new vector<double> (this->dimension);
    vector<double> *X_pp = new vector<double> (this->dimension);

    vector<double> *X_i0 = new vector<double> (this->dimension);
    vector<double> *X_ip = new vector<double> (this->dimension);
    vector<double> *X_0j = new vector<double> (this->dimension);
    vector<double> *X_pj = new vector<double> (this->dimension);

    vector<double> *X_ij = new vector<double> (this->dimension);

    int p = elt->cellDegree();
    int H = elt->controlPointNumberByDirection();
    int N = elt->controlPointNumber();

    // Get element vertices
    elt->controlPointCoordinates(0,X_00);
    elt->controlPointCoordinates(p,X_p0);
    elt->controlPointCoordinates(H*p,X_0p);
    elt->controlPointCoordinates(N-1,X_pp);

    for(int i=1; i<p; i++){
        for(int j=1; j<p; j++){

            elt->controlPointCoordinates(i,X_i0);
            elt->controlPointCoordinates(i+p*H,X_ip);
            elt->controlPointCoordinates(j*H,X_0j);
            elt->controlPointCoordinates(p+j*H,X_pj);

            double w_0j = 1. - double(i)/double(p);
            double w_pj = double(i)/double(p);
            double w_i0 = 1. - double(j)/double(p);
            double w_ip = double(j)/double(p);

            // Coons formula
            for(int k=0; k<this->dimension; k++)
                X_ij->at(k) = w_0j*X_0j->at(k) + w_pj*X_pj->at(k)
                            + w_i0*X_i0->at(k) + w_ip*X_ip->at(k)
                            - w_0j*(w_i0*X_00->at(k) + w_ip*X_0p->at(k))
                            - w_pj*(w_i0*X_p0->at(k) + w_ip*X_pp->at(k));

            elt->setControlPointCoordinates(i+j*H,X_ij);
            elt->setControlPointWeight(i+j*H,1.);

        }
    }

    delete X_00;
    delete X_p0;
    delete X_0p;
    delete X_pp;

    delete X_i0;
    delete X_ip;
    delete X_0j;
    delete X_pj;

    delete X_ij;

}


////////////////////////////////////////////////////////////////////////////
