/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/

#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>

#include "igInterpolatorCurve.h"
#include "igBasisBSpline.h"


using namespace std;

/////////////////////////////////////////////////////////////////////////////

igInterpolatorCurve::igInterpolatorCurve()
{
    this->knot_vector = new vector<double>;
}

igInterpolatorCurve::~igInterpolatorCurve(void)
{
    delete knot_vector;
}

/////////////////////////////////////////////////////////////////////////////


void igInterpolatorCurve::setDegree(int degree)
{
    this->degree = degree;
}

void igInterpolatorCurve::setKnotVector(vector<double> *knot_vector)
{
    this->knot_vector = knot_vector;
}

void igInterpolatorCurve::setPoints(vector<double> &parameters, vector<double> &coordinates)
{
    this->parameters = parameters;
    this->coordinates = coordinates;
}

///////////////////////////////////////////////////////////////

double igInterpolatorCurve::controlPoint(int index)
{
    return this->control_points.at(index);
}


//////////////////////////////////////////////////////////////

void igInterpolatorCurve::run(void)
{
    int knot_number = knot_vector->size();
    int ctrl_pt_number = coordinates.size();

    igBasis *basis = new igBasisBSpline(degree, knot_vector);

    int size = ctrl_pt_number;
    double *rhs = new double[size];
    double *mat = new double[size*size];

    vector<double> values;
    values.assign(size, 0.);

    control_points.assign(size, 0.);

    // initialize matrix and rhs, sol
    for (int ils=0; ils<size; ils++){
        rhs[ils] = 0.;
        for (int jls=0; jls<size; jls++)
            mat[ils*size+jls] = 0.;
    }

    // compute matrix and rhs
    for (int ils=0; ils<size; ils++){

        basis->evalFunction(parameters.at(ils), values);

        // rhs term
        rhs[ils] = coordinates.at(ils);

        // matrix
        for (int jls=0; jls<size; jls++){
            mat[ils*size+jls] = values.at(jls);
        }

    }

    // solve system
    inverse_system(mat,size);

    // store solution
    for (int ils=0; ils<size; ils++){
        control_points[ils] = 0.;
        for (int jls=0; jls<size; jls++){
            control_points[ils] += mat[ils*size+jls]*rhs[jls];
        }
    }

    delete basis;

}

////////////////////////////////////////////////////////////

void igInterpolatorCurve::inverse_system(double* A, int N)
{
    lin_solver.inverse(A,N);
}
