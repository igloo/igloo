/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igCoreExport.h>

#include "igBasis.h"

#include <vector>

using namespace std;

class IGCORE_EXPORT igBasisBSpline : public igBasis
{
public:
     igBasisBSpline(int degree_value, vector<double> *knot_values);
    ~igBasisBSpline(void) = default;
private:
    vector<double> *knot_vector;

public:
    int functionNumber() override;
    void evalFunction(double var, vector<double> &values) override;
    void evalGradient(double var, vector<double> &values) override;
   	int extractBezier(vector<double> *X_spline, vector<double> *Y_spline,
    		vector<vector<double>> *X_bezier, vector<vector<double>> *Y_bezier);
private:
    int findSpan(double var);
    void evalNonZeroFunctions(int i, double var, vector<double> &values);
    void evalNonZeroDerivatives(int i, double var, vector<double> &values, vector<double> &ders);
};

//
// igBasisBSpline.h ends here
