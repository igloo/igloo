/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igBasisBezier.h"

#include <cmath>
#include <iostream>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igBasisBezier::igBasisBezier(int degree_value) : igBasis(degree_value)
{
	this->function_number = this->degree + 1;
}

/////////////////////////////////////////////////////////////////////////////

int igBasisBezier::functionNumber()
{
	return function_number;
}

void igBasisBezier::evalFunction(double var, vector<double> &values)
{
	// initialization
	values[0] = 1.;
	for (int ideg=1; ideg<degree+1; ++ideg) {
		values[ideg] = 0;
	}

	// evaluation by recursion
	for (int ideg=1; ideg<degree+1; ++ideg) {

		// last terms
		for (int ifun=ideg; ifun>0; --ifun) {
			values[ifun] = values[ifun]*(1.-var) + values[ifun-1]*var;
		}

		// first term
		values[0] = values[0]*(1.-var);
	}
}

void igBasisBezier::evalGradient(double var, vector<double> &values)
{
	// initialization
	values[0] = 1.;
	for (int ideg=1; ideg<degree+1; ++ideg) {
		values[ideg] = 0;
	}

	// evaluation by recursion until degree n-1 only
	for (int ideg=1; ideg<degree; ++ideg) {

		// last terms
		for (int ifun=ideg; ifun>0; --ifun) {
			values[ifun] = values[ifun]*(1.-var) + values[ifun-1]*var;
		}

		// first term
		values[0] = values[0]*(1.-var);
	}

	// evaluation of gradient
	for (int ifun=degree; ifun>0; --ifun) {
		values[ifun] = degree * (values[ifun-1] - values[ifun]);
	}
	values[0] = -degree*values[0];
}

void igBasisBezier::evalCurve(double var, vector<double> &points, vector<double> &weights, double *curve)
{
	vector<double> functions(function_number);
	vector<double> gradients(function_number);

	this->evalFunction(var,functions);
	this->evalGradient(var,gradients);

	double weighted_sum = 0.;
	double weighted_sum_grad = 0.;
	for (int ifun=0; ifun<function_number; ifun++){
		weighted_sum += functions[ifun] * weights[ifun];
		weighted_sum_grad += gradients[ifun] * weights[ifun];
	}

	curve[0] = 0.;
	curve[1] = 0.;

	for (int ifun=0; ifun<function_number; ifun++){
		curve[0] += functions[ifun] * weights[ifun] * points[ifun] / weighted_sum;
		curve[1] += ( gradients[ifun] * weighted_sum - functions[ifun] * weighted_sum_grad )
                            												* weights[ifun] * points[ifun] / (weighted_sum*weighted_sum);
	}

}


double igBasisBezier::pointInversion(vector<double> &x_curve, vector<double> &y_curve,
		vector<double> &w_curve, double &x_target, double &y_target, double &param0, double &param1)
{

	int iter = 0;
	int iter_max = 10;
	double tol1 = 1e-15;
	double tol2 = 1e-14;

	double eps, funct0, funct1, param2;
	double bezier_x[2], bezier_y[2];

	// Evaluate function in point 0
	this->evalCurve(param0,x_curve,w_curve,bezier_x);
	this->evalCurve(param0,y_curve,w_curve,bezier_y);
	funct0 = bezier_x[1]*(bezier_x[0] - x_target) + bezier_y[1]*(bezier_y[0] - y_target);

	// Evaluate function in point 1
	this->evalCurve(param1,x_curve,w_curve,bezier_x);
	this->evalCurve(param1,y_curve,w_curve,bezier_y);
	funct1 = bezier_x[1]*(bezier_x[0] - x_target) + bezier_y[1]*(bezier_y[0] - y_target);

	eps = fabs(param1-param0);

	// Secant algorithm to find parametric coordinate
	while(fabs(funct1)>tol1 && eps>tol2 && iter<iter_max){

		param2 = (param0*funct1 - param1*funct0)/(funct1 - funct0);
		param0 = param1;
		param1 = param2;
		funct0 = funct1;

		// Update function
		this->evalCurve(param1,x_curve,w_curve,bezier_x);
		this->evalCurve(param1,y_curve,w_curve,bezier_y);
		funct1 = bezier_x[1]*(bezier_x[0] - x_target) + bezier_y[1]*(bezier_y[0] - y_target);

		eps = fabs(param1-param0);

		iter++;
	}

	return param2;

}

