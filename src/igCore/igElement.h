/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igCoreExport.h>

#include "igCell.h"

#include <vector>

using namespace std;

class IGCORE_EXPORT igElement : public igCell
{
public:
     igElement(void);
    ~igElement(void) = default;

public:
    void setGlobalId(int local_id, int var_id, int global_id);
    void setPartition(int partition);
    void setSubdomain(int subdomain);

    void setControlPointCoordinate(int index, int component, double coord);
    void setControlPointCoordinates(int index, vector<double> *coord);
    void setControlPointWeight(int index, double weight);

    void setControlPointVelocity(int index, int component, double vel);
    void setControlPointVelocity(int index, vector<double> *vel);

    void setFaceId(int orientation, int id);

public:
    void initializeDegreesOfFreedom(void);
    void initializeGeometry(void);

public:
    double controlPointCoordinate(int index, int component) const ;
    void controlPointCoordinates(int index, vector<double> *coord);
    double controlPointWeight(int index) const;

    double controlPointVelocityComponent(int index, int component) const;
    void controlPointVelocity(int index, vector<double> *vel);

    double radius(void) const;
    double barycenter(int component) const;

    int partition(void);
    int subdomain(void);

    int globalId(int local_id, int var_id) const;
    int *globalIdPtr(int var_id);

    int faceId(int orientation);

private:
    vector<int> global_id_list;

    double element_radius;

    int element_partition;
    int element_subdomain;

    vector<int> face_list;

private:
    void evaluateGaussPoints(void);
};

//
// igElement.h ends here
