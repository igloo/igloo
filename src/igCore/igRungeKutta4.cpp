/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igRungeKutta4.h"

#include <iostream>
#include <igAssert.h>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igRungeKutta4::igRungeKutta4() : igRungeKutta()
{
	current_solution = new vector<double>;
}

igRungeKutta4::~igRungeKutta4(void)
{
	delete current_solution;
}

/////////////////////////////////////////////////////////////////////////////

void igRungeKutta4::initialize(vector<double> *initial_solution, double initial_time)
{
	IG_ASSERT(initial_solution, "no initial_solution");
	solution = initial_solution;

	step_number = 4;
	current_step = 1;

	current_time = initial_time;

	int size = solution->size();

	increment1.resize(size);
	increment2.resize(size);
	increment3.resize(size);
	increment4.resize(size);

	current_solution->resize(size);

	return;
}

void igRungeKutta4::setIncrement(vector<double> *increment)
{
	IG_ASSERT(increment, "no increment");

	int size = increment->size();

	switch (current_step) {

	case 1:
		for (int i=0; i<size; i++){
			increment1[i] = increment->at(i);
		}
		break;

	case 2:
		for (int i=0; i<size; i++){
			increment2[i] = increment->at(i);
		}
		break;

	case 3:
		for (int i=0; i<size; i++){
			increment3[i] = increment->at(i);
		}
		break;

	case 4:
		for (int i=0; i<size; i++){
			increment4[i] = increment->at(i);
		}
		break;
	}

	current_step++;

	return;
}


vector<double>* igRungeKutta4::nextEvaluation(void)
{
	switch (current_step) {

	case 1:
		return solution;
		break;

	case 2:
		for (int i=0; i<current_solution->size(); i++){
			current_solution->at(i) = solution->at(i) + 0.5 * current_time_step * increment1.at(i);
		}
		return current_solution;
		break;

	case 3:
		for (int i=0; i<current_solution->size(); i++){
			current_solution->at(i) = solution->at(i) + 0.5 * current_time_step * increment2.at(i);
		}
		return current_solution;
		break;

	case 4:
		for (int i=0; i<current_solution->size(); i++){
			current_solution->at(i) = solution->at(i) + current_time_step * increment3.at(i);
		}
		return current_solution;
		break;
	}

	return solution;
}

double igRungeKutta4::nextTime(void)
{
	switch (current_step) {

	case 1:
		return current_time;
		break;

	case 2:
		return current_time + 0.5* current_time_step;
		break;

	case 3:
		return current_time + 0.5* current_time_step;
		break;

	case 4:
		return current_time + current_time_step;
		break;
	}
	return current_time;
}

void igRungeKutta4::updateSolution(void)
{
	for (int i=0; i<solution->size(); i++){

		solution->at(i) += current_time_step/6. * ( increment1.at(i) + 2.* increment2.at(i)
				+ 2.* increment3.at(i) + increment4.at(i) );
	}

	current_time += current_time_step;
	current_step = 1;

	return;
}
