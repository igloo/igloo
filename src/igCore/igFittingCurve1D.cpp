/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/

#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>

#include "igElement.h"

#include "igFittingCurve1D.h"
#include "igBasisBezier.h"



using namespace std;

/////////////////////////////////////////////////////////////////////////////

igFittingCurve1D::igFittingCurve1D()
{

}

/////////////////////////////////////////////////////////////////////////////

void igFittingCurve1D::setFunction(function<double(double *coord, double time, int var_id, double *coord_elt, double *func_param)> fun, int var_id, double *func_param)
{
    this->fun = fun;
    this->var_id = var_id;
    this->func_param = func_param;
}

///////////////////////////////////////////////////////////////

double igFittingCurve1D::zControlPointFit(int index)
{
    return sol_z.at(index);
}

///////////////////////////////////////////////////////////////

void igFittingCurve1D::run(void)
{
    double *generic_param;
    
    int degree = this->element->cellDegree();
    
    igBasis *basis = new igBasisBezier(degree);

    vector<double> values1;
    values1.assign(degree+1,0.);

    int size = (degree+1);

    double *rhs = new double[size];
    double *mat = new double[size*size];

    // initialize matrix and rhs, sol
    for (int i=0; i<size; i++){
        rhs[i] = 0.;
        for (int j=0; j<size; j++)
            mat[i*size+j] = 0.;
    }

    // compute matrix and rhs
    for (int k1=0; k1<sample_number; k1++){

        double coord1 = float(k1)/float(sample_number-1);
        basis->evalFunction(coord1,values1);

        double x = 0.;
        for (int ifun1=0; ifun1<degree+1; ifun1++){
            int index = ifun1;
            x += values1.at(ifun1)* this->element->controlPointCoordinate(index, 0);
        }

        double function_value = fun(&x,0.,var_id, generic_param, func_param);

        for (int i1=0; i1<degree+1; i1++){
            rhs[i1] += values1.at(i1) * function_value;

            for (int j1=0; j1<degree+1; j1++){
                mat[i1*size+j1] += values1.at(i1) * values1.at(j1);
            }
        }
    }


    // solve system
    inverse_system(mat,size);

    // store solution
    sol_z.resize(size);

    for (int ils=0; ils<size; ils++){
        sol_z[ils] = 0.;
        for (int jls=0; jls<size; jls++){
            sol_z[ils] += mat[ils*size+jls]*rhs[jls];
        }
    }

    delete basis;

}
