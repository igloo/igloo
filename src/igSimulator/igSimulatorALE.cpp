/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igSimulatorALE.h"

#include <igCore/igMesh.h>
#include <igCore/igElement.h>
#include <igCore/igFace.h>
#include <igCore/igMeshRefinerIsotropic.h>
#include <igCore/igMeshCoarsenerIsotropic.h>
#include <igCore/igMeshMover.h>
#include <igCore/igMeshMoverSliding.h>
#include <igCore/igRungeKutta4.h>
#include <igCore/igRungeKuttaSSP.h>
#include <igCore/igBoundaryIds.h>

#include <igDistributed/igCommunicator.h>

#include <igFiles/igFileManager.h>


#include <igSolver/igCouplingInterface.h>
#include <igSolver/igErrorEstimatorBox.h>
#include <igSolver/igErrorEstimatorJump.h>
#include <igSolver/igErrorEstimatorViscosity.h>
#include <igSolver/igSolverAcoustic.h>
#include <igSolver/igSolverAdvection.h>
#include <igSolver/igSolverBurger.h>
#include <igSolver/igSolverEuler.h>
#include <igSolver/igSolverNavierStokes.h>
#include <igSolver/igSolverViscousBurger.h>
#include <igSolver/igStructuralSolver.h>

#include <iostream>
#include <fstream>
#include <math.h>
#include <iomanip>
#include <sstream>
#include <algorithm>
#include <igAssert.h>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igSimulatorALE::igSimulatorALE(void) : igSimulator()
{
	this->ALE_formulation = true;
    first_pass = true;
}

igSimulatorALE::~igSimulatorALE(void)
{

}

//////////////////////////////////////////////////////////////////////////////

void igSimulatorALE::initializeTimeLoop(void)
{
    state_increment->resize(state->size());
    state_rk->resize(state->size());

    solver->updateSolution(state_rk,state,mesh->variableNumber());

    this->initializeInterfaceFSI();
    if(first_pass){
        mover->setInterfaceFSI(interface->interfaceDisplacementsFluid(), interface->interfaceVelocitiesFluid());
        mover->initializeMapping(interface->knotVector(), interface->controlPointsX(), interface->controlPointsY());
        first_pass = false;
    }
    mover->initializeFractionalStep();
    mover->setFractionalStep(1);
    mover->initializeVelocity(this->time);
    rk->initialize(state_rk,time);
}


void igSimulatorALE::iteration(void)
{
    vector<double> *current_state;
    double current_time;
    bool movement;

    mover->initializeFractionalStep();

    //------------------ structural solver -----------------

    structural_solver->setTimeStep(time_step);
    structural_solver->compute();
    interface->convertStructureDisplacementsToFluid();
    interface->convertStructureVelocitiesToFluid();

    //--------------------- Runge-Kutta loop ----------------
    for (int irk=0; irk<rk->stepNumber(); irk++){

        // ask RK next state and time to evaluate
        current_state = rk->nextEvaluation();
        current_time = rk->nextTime();

        //---- move mesh and compute new mass matrix
        mover->setFractionalStep((current_time-time)/time_step);
        movement = mover->move(current_time);
        if(movement)
        {
            solver->computeMassMatrix();
            solver->inverseMassMatrix();
        }

        //----- set solver current state and time
        solver->updateSolution(state,current_state,mesh->variableNumber());
        solver->setState(state);
        solver->setTime(current_time);

        //---- ask solver new increment
        solver->computeIncrementAle(state_increment);

        //----- tell RK new increment
        rk->setIncrement(state_increment);

    }
    //------------------ end of RK loop ------------------

    //----- update time ---------------
    time += time_step;

    //----- update mesh -----------
    mover->setFractionalStep(1);
    movement = mover->move(this->time);
    if(movement){
        solver->computeMassMatrix();
        solver->inverseMassMatrix();
    }
    //----- update solution -----------
    rk->updateSolution();
    solver->updateSolution(state,state_rk,mesh->variableNumber());
    solver->setState(state);
    solver->setTime(time);

}

//////////////////////////////////////////////////////////////////////////////

void igSimulatorALE::setMeshMover(igMeshMover *mesh_mover)
{
	this->mover = mesh_mover;
}

//////////////////////////////////////////////////////////////////////////////

void igSimulatorALE::initializeInterfaceFSI(void)
{
    int gauss_pts_number = mesh->face(0)->gaussPointNumber();
    int dimension = mesh->meshDimension();
    int ctrl_pts_number = mesh->face(0)->controlPointNumber();

    int quadrature_number = 0;
    int pts_number = 0;
    int fsi_face_number = 0;

    // set quadrature points number per face
    interface->initializeQuadraturePointNumber(gauss_pts_number);

    // count interface quadrature and control points
    for(int ifac=0; ifac<mesh->boundaryFaceNumber(); ifac++){

        igFace *face = mesh->boundaryFace(ifac);
        int elt_index = face->leftElementIndex();

        // velocity and displacement :level 0 control points
        if(mesh->elementLevel(elt_index,0) == 0){
            if(face->type() == id_slip_wall_fsi || face->type() == id_noslip_wall_fsi){
                pts_number += ctrl_pts_number;
            }
        }

        // efforts : quadrature points
        if(face->isActive()){
            if(face->type() == id_slip_wall_fsi || face->type() == id_noslip_wall_fsi){
                quadrature_number += interface->quadraturePointNumber();
            }
        }
    }

    // set local quadrature number in interface and communicate to initialize mapping
    interface->initializeQuadratureMapping(quadrature_number);

    // communicate size for global interface arrays
    int global_quadrature_number = communicator->addInt(quadrature_number);
    int global_pts_number = communicator->addInt(pts_number);

    // fill local quadrature data and communicate global interface arrays
    interface->fillQuadrature(mesh,global_quadrature_number);

    // prepare effort storage
    interface->initializeFluidInterface(global_quadrature_number, global_pts_number, dimension);
    interface->initializeMapping(mesh);


}
