/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igSimulatorRigidALE.h"

#include <igCore/igMesh.h>
#include <igCore/igElement.h>
#include <igCore/igMeshRefinerIsotropic.h>
#include <igCore/igMeshCoarsenerIsotropic.h>
#include <igCore/igMeshMover.h>
#include <igCore/igMeshMoverSliding.h>
#include <igCore/igRungeKutta4.h>
#include <igCore/igRungeKuttaSSP.h>

#include <igDistributed/igCommunicator.h>

#include <igFiles/igFileManager.h>

#include <igSolver/igErrorEstimatorBox.h>
#include <igSolver/igErrorEstimatorJump.h>
#include <igSolver/igErrorEstimatorViscosity.h>
#include <igSolver/igSolverAcoustic.h>
#include <igSolver/igSolverAdvection.h>
#include <igSolver/igSolverBurger.h>
#include <igSolver/igSolverEuler.h>
#include <igSolver/igSolverNavierStokes.h>
#include <igSolver/igSolverViscousBurger.h>

#include <iostream>
#include <fstream>
#include <math.h>
#include <iomanip>
#include <sstream>
#include <algorithm>
#include <igAssert.h>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igSimulatorRigidALE::igSimulatorRigidALE(void) : igSimulatorALE()
{

}

igSimulatorRigidALE::~igSimulatorRigidALE(void)
{

}

//////////////////////////////////////////////////////////////////////////////

void igSimulatorRigidALE::initializeTimeLoop(void)
{

    state_increment->resize(state->size());

	mover->initializeVelocity(this->time);
    rk->initialize(state, time);

}


void igSimulatorRigidALE::iteration(void)
{
    vector<double> *current_state;
    double current_time;

    //--------------------- Runge-Kutta loop ----------------
    for (int irk=0; irk<rk->stepNumber(); irk++){

        // ask RK next state and time to evaluate
        current_state = rk->nextEvaluation();
        current_time = rk->nextTime();

        //----- set solver current state and time
        solver->setState(current_state);
        solver->setTime(current_time);

        //---- move mesh
        mover->move(current_time);

        //---- ask solver new increment
        solver->computeIncrement(state_increment);

        //----- tell RK new increment
        rk->setIncrement(state_increment);

    }
    //------------------ end of RK loop ------------------

    //----- update time ---------------
    time += time_step;

    //----- update mesh ---------------
    mover->move(this->time);

    //----- update solution -----------
    rk->updateSolution();
    solver->setState(state);
    solver->setTime(time);

}
