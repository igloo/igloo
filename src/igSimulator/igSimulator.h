/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igSimulatorExport.h>

#include <vector>
#include <string>

using namespace std;

class igCouplingInterface;
class igErrorEstimator;
class igFileManager;
class igMesh;
class igMeshMover;
class igMeshRefinerIsotropic;
class igMeshCoarsenerIsotropic;
class igSolver;
class igStructuralSolver;
class igRungeKutta;
class igCommunicator;

class IGSIMULATOR_EXPORT igSimulator
{
public:
    typedef void(*callback_t)(int);

public:
     igSimulator(void);
    ~igSimulator(void);

public:
    void run(void);
    void initialize(void);
    void finalize(void);

public:
    void setMesh(igMesh *mesh);
    virtual void setMeshMover(igMeshMover *mesh_mover);
    void setInitialSolution(vector<double> *state);
    void setCommunicator(igCommunicator *communicator);
    void setInitialTime(double initial_time);

    void setCoordinates(vector<double> *coord);
    void setVelocities(vector<double> *vel);

    void setCallback(callback_t callback);

    void setSolver(const string& solver_name);
    void setStructure(const string& structure_name);
    void setIntegrator(const string& integrator_name);

    void setFinalTime(double end_time);
    void setSavePeriod(double period_save);
    void setCflCoefficient(double cfl_coefficient);
    void setSolverParameters(vector<double> *solver_parameters);
    void setRefineCoefficient(double refine_coef);
    void setCoarsenCoefficient(double coarsen_coef);
    void setRefineMaxLevel(int level);
    void setAdaptBox(vector<double> *amr_box);
    void setAdaptPeriod(double adapt_period);
    void setSmoothingViscosityNumber(int number);

    void enableErrorComputation(bool compute_error);
    void setShockCapturing(double shock_capturing_coef);
    void enableVorticity(bool vorticity_flag);
    void enableCheckPositivity(bool positivity);

    void enableRestart(bool restart);

protected:
    double refinementTimeStep(igSolver *solver, igMesh *mesh);

    virtual void initializeTimeLoop(void);
    virtual void iteration(void);

protected:
    double time;
    double time_step;

    igCommunicator *communicator;
    igErrorEstimator *error_estimator;
    igFileManager *file_manager;
    igMesh *mesh;
    igMeshCoarsenerIsotropic *coarsener;
    igMeshRefinerIsotropic *refiner;
    igRungeKutta *rk;
    igSolver *solver;
    igStructuralSolver *structural_solver;
    igCouplingInterface *interface;

    vector<double> *state;
    vector<double> *coordinates;
    vector<double> *velocities;

    vector<double> *error_xi;
    vector<double> *error_eta;
    vector<double> *artificial_viscosity;
    vector<double> *vorticity;
    vector<double> *state_increment;
    vector<double> *state_rk;

    string solver_name;
    string structure_name;
    string integrator_name;
    double end_time;
    double period_save;
    double cfl_coefficient;
    double refine_coef;
    double coarsen_coef;
    int refine_max_level;
    double adapt_period;
    bool compute_error;
    bool shock_capturing;
    bool flag_vorticity;
    bool ALE_formulation;
    double shock_capturing_coef;
    bool restart;
    int smoothing_step_number;
    bool check_positivity;

    vector<double> *solver_parameters;
    vector<double> *amr_box;

    callback_t callback = nullptr;
};

//
// igSimulator.h ends here
