/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igSimulator.h"

#include <igCore/igMesh.h>
#include <igCore/igElement.h>
#include <igCore/igMeshRefinerIsotropic.h>
#include <igCore/igMeshCoarsenerIsotropic.h>
#include <igCore/igMeshMover.h>
#include <igCore/igMeshMoverSliding.h>
#include <igCore/igRungeKutta2.h>
#include <igCore/igRungeKutta4.h>
#include <igCore/igRungeKuttaSSP.h>

#include <igDistributed/igCommunicator.h>

#include <igFiles/igFileManager.h>

#include <igSolver/igErrorEstimatorBox.h>
#include <igSolver/igErrorEstimatorJump.h>
#include <igSolver/igErrorEstimatorViscosity.h>
#include <igSolver/igCouplingInterface.h>
#include <igSolver/igCouplingInterfaceMembrane.h>
#include <igSolver/igCouplingInterfacePiston.h>
#include <igSolver/igCouplingInterfaceSpring.h>
#include <igSolver/igSolverAcoustic.h>
#include <igSolver/igSolverAdvection.h>
#include <igSolver/igSolverBurger.h>
#include <igSolver/igSolverEuler.h>
#include <igSolver/igSolverNavierStokes.h>
#include <igSolver/igSolverViscousBurger.h>
#include <igSolver/igStructuralSolverPiston.h>
#include <igSolver/igStructuralSolverSpring.h>
#include <igSolver/igStructuralSolverMembrane.h>

#include <iostream>
#include <fstream>
#include <math.h>
#include <iomanip>
#include <sstream>
#include <algorithm>
#include <igAssert.h>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igSimulator::igSimulator(void)
{
    this->end_time = 1.;
    this->period_save = end_time;
    this->cfl_coefficient = 0.5;
    this->refine_coef = 2.;
    this->coarsen_coef = 0.5;
    this->adapt_period = 1;
    this->refine_max_level = 0;
    this->compute_error = false;
    this->shock_capturing = false;
    this->flag_vorticity = false;
    this->ALE_formulation = false;
    this->shock_capturing_coef = 1;
    this->restart = false;
    this->smoothing_step_number = 0;
    this->check_positivity = false;
}

igSimulator::~igSimulator(void)
{

}

/////////////////////////////////////////////////////////////////////////////

void igSimulator::run(void)
{

    //------------------------------------------------------
    //------------------- refinement loop ------------------
    //------------------------------------------------------

    bool refine_iteration = true;

    // Write initial solution
    file_manager->writeMeshSolution(state, mesh);

    while(refine_iteration){

        solver->initialize();
        if(shock_capturing)
            solver->applyLimiter(shock_capturing_coef);

        //--------- computation of the inverse of the mass matrix---------

        solver->computeMassMatrix();
        if(communicator->rank() == 0){
            cout << ">>> Mass matrix computed" << endl;
        }

        this->initializeTimeLoop();

        solver->inverseMassMatrix();
        if(communicator->rank() == 0){
            cout << ">>> Mass matrix inverted" << endl;
            cout << endl;
        }

        //------------------------------------------------------
        //------------------- time loop ------------------------
        //------------------------------------------------------

        bool time_iteration = true;
        int time_step_number = 0;
        double end_time_adaption;

        solver->computeEfforts();
        interface->convertFluidEffortsToStructure();

        if(refine_max_level > 0){
            end_time_adaption  = time + this->refinementTimeStep(solver, mesh);
            if(end_time_adaption > end_time){
                end_time_adaption = end_time;
                refine_iteration = false;
            }
        } else
            end_time_adaption = end_time;

        while (time_iteration){

            this->time_step = solver->timeStep();

            if(time+time_step > end_time_adaption)
                time_step = end_time_adaption - time;

            rk->setTimeStep(time_step);

            this->iteration();

            if(shock_capturing)
                solver->applyLimiter(shock_capturing_coef);

            if(flag_vorticity)
            	solver->computeVorticity();

            solver->computeEfforts();
            interface->convertFluidEffortsToStructure();

            time_step_number++;

            if(time >= end_time_adaption) time_iteration = false;

            // write solution
            file_manager->writeMeshSolution(state, artificial_viscosity, vorticity, mesh, time);
            file_manager->writeInterface(interface,time);

        }
        //------------------------------------------------------
        //---------------- end of time loop --------------------
        //------------------------------------------------------

        if(compute_error)
            solver->computeGlobalErrorNorm();

        if(communicator->rank() == 0){
            cout <<"final time: " << time << endl;
            cout <<"number of time steps: " <<  time_step_number << endl;
            cout << endl;
        }

        // refinement
        if(refine_max_level == 0)
            refine_iteration = false;
        else if(refine_iteration){
            error_estimator->setTime(time);
            error_estimator->computeEstimator();

            coarsener->select();
            coarsener->coarsen();

            refiner->select();
            refiner->refine();
        }

    }
    //------------------------------------------------------
    //--------------- end of refinement loop ---------------
    //------------------------------------------------------

}

////////////////////////////////////////////////////////////////////////////////////

void igSimulator::initialize()
{
	file_manager = new igFileManager;
	file_manager->setSavePeriod(period_save);
	file_manager->setPartitionNumber(communicator->size());
	file_manager->setMyPartition(communicator->rank());

	if(refine_max_level > 0 || ALE_formulation)
		file_manager->enableSaveMesh();
	if(shock_capturing)
		file_manager->enableSaveProperty();
	if(flag_vorticity)
		file_manager->enableSaveVorticity();

	if(restart){
		file_manager->readRestartFile(state, mesh, time, ALE_formulation);
		communicator->distributeField(coordinates, mesh->meshDimension()+1);
		communicator->distributeField(velocities, mesh->meshDimension());
		file_manager->applyMeshRestart(mesh);
	}

	//---------------- state construction -----------------

	state_increment = new vector<double>;
	state_rk = new vector<double>;
	artificial_viscosity = new vector<double>;
	vorticity = new vector<double>;

	//------------------ integrator construction ---------

	if(integrator_name == "rk4")
		rk = new igRungeKutta4;
	else if(integrator_name == "ssp")
		rk = new igRungeKuttaSSP;
    else if(integrator_name == "rk2")
    	this->rk = new igRungeKutta2;
	else{
		cout << "integrator does not exist !"<< endl;
	}

    //----------------- structural solver -----------------

    if(structure_name == "piston"){
        interface = new igCouplingInterfacePiston();
        structural_solver = new igStructuralSolverPiston(interface->interfaceGaussPoints(), interface->interfaceGaussWeights(), interface->interfaceEffortsStructure(), interface->interfaceDisplacementsStructure(), interface->interfaceVelocitiesStructure());
    } else if(structure_name == "spring"){
        interface = new igCouplingInterfaceSpring();
        structural_solver = new igStructuralSolverSpring(interface->interfaceGaussPoints(), interface->interfaceGaussWeights(), interface->interfaceEffortsStructure(), interface->interfaceDisplacementsStructure(), interface->interfaceVelocitiesStructure());
    } else if(structure_name == "membrane"){
        interface = new igCouplingInterfaceMembrane();
        file_manager->readNurbsCurve(interface->knotVector(), interface->controlPointsX(), interface->controlPointsY());
        interface->initializeStructureInterface();
        structural_solver = new igStructuralSolverMembrane(interface->interfaceGaussPoints(), interface->interfaceGaussWeights(), interface->interfaceEffortsStructure(), interface->interfaceDisplacementsStructure(), interface->interfaceVelocitiesStructure());
        structural_solver->initialize(interface->interfaceDegree(), interface->controlPointNumber(), interface->knotVector(), interface->controlPointsX(), interface->controlPointsY(), interface->gaussPointNumber());
    } else {
        interface = new igCouplingInterface();
        structural_solver = new igStructuralSolver(interface->interfaceGaussPoints(), interface->interfaceGaussWeights(), interface->interfaceEffortsStructure(), interface->interfaceDisplacementsStructure(), interface->interfaceVelocitiesStructure());
    }
    structural_solver->setCommunicator(communicator);
    interface->setCommunicator(communicator);
    interface->initializeQuadrature(mesh);


	//--------------- fluid solver -------------------

	if(solver_name == "acoustic")
		solver = new igSolverAcoustic(mesh->variableNumber(),mesh->meshDimension(),solver_parameters, ALE_formulation);
	else if(solver_name == "advection")
		solver = new igSolverAdvection(mesh->variableNumber(),mesh->meshDimension(),solver_parameters, ALE_formulation);
	else if(solver_name == "advection2D")
		solver = new igSolverAdvection(mesh->variableNumber(),mesh->meshDimension(),solver_parameters, ALE_formulation);
	else if(solver_name == "burger")
		solver = new igSolverBurger(mesh->variableNumber(),mesh->meshDimension(),solver_parameters, ALE_formulation);
	else if(solver_name == "viscous_burger")
		solver = new igSolverViscousBurger(mesh->variableNumber(),mesh->meshDimension(),solver_parameters, ALE_formulation);
	else if(solver_name == "euler")
		solver = new igSolverEuler(mesh->variableNumber(),mesh->meshDimension(),solver_parameters, ALE_formulation);
	else if(solver_name == "navier-stokes")
		solver = new igSolverNavierStokes(mesh->variableNumber(),mesh->meshDimension(),solver_parameters, ALE_formulation);
	else
		cout << "solver does not exist !"<< endl;

	solver->setTime(time);
	solver->setMesh(mesh);
	solver->setState(state);
	solver->setArtificialViscosity(artificial_viscosity);
	solver->setVorticity(vorticity);
	solver->setCommunicator(communicator);
	solver->setStabilityCoefficient(cfl_coefficient);
	solver->setSmoothingViscosityNumber(smoothing_step_number);
    solver->setInterface(interface);
    if(shock_capturing)
        solver->enableShockCapturing();
    if(check_positivity)
        solver->enableCheckPositivity();

	//----------------- error estimator ------------------

	error_xi = new vector<double>;
	error_eta = new vector<double>;

	error_estimator = new igErrorEstimatorJump;
	error_estimator->setMesh(mesh);
	error_estimator->setState(state);
	error_estimator->setSolver(solver);
	error_estimator->setError(error_xi,error_eta);

	//---------------- mesh refinement ------------------

	refiner = new igMeshRefinerIsotropic;
	refiner->initialize(mesh, state, coordinates, velocities);
    refiner->setCommunicator(communicator);
	refiner->setRefineCoefficient(refine_coef);
	refiner->setRefineMaxLevel(refine_max_level);
	refiner->setError(error_xi, error_eta);
	refiner->setAdaptBox(amr_box);

	coarsener = new igMeshCoarsenerIsotropic;
	coarsener->initialize(mesh, state, coordinates, velocities);
    coarsener->setCommunicator(communicator);
	coarsener->setCoarseningCoefficient(coarsen_coef);
	coarsener->setError(error_xi, error_eta);
}

void igSimulator::finalize(void)
{
    // cleaning
    if(communicator->rank() == 0){
        cout << ">>> cleaning data" << endl;
        cout << endl;
    }

    delete coarsener;
    delete refiner;
    delete error_estimator;
    delete solver;
    delete structural_solver;
    delete rk;
    delete file_manager;

    delete error_xi;
    delete error_eta;
    delete artificial_viscosity;
    delete vorticity;
    delete state_increment;
    delete state_rk;

    if(communicator->rank() == 0){
        cout << ">>> end of program" << endl;
    }
}

//////////////////////////////////////////////////////////////////////////////

void igSimulator::initializeTimeLoop(void)
{

    state_increment->resize(state->size());
    rk->initialize(state, time);

}

void igSimulator::iteration(void)
{
    vector<double> *current_state;
    double current_time;

    //--------------------- Runge-Kutta loop ----------------
    for (int irk=0; irk<rk->stepNumber(); irk++){

        // ask RK next state and time to evaluate
        current_state = rk->nextEvaluation();
        current_time = rk->nextTime();

        //----- set solver current state and time
        solver->setState(current_state);
        solver->setTime(current_time);

        //---- ask solver new increment
        solver->computeIncrement(state_increment);

        //----- tell RK new increment
        rk->setIncrement(state_increment);

    }
    //------------------ end of RK loop ------------------

    //----- update time ---------------
    time += time_step;

    //----- update solution -----------
    rk->updateSolution();
    solver->setState(state);
    solver->setTime(time);

}


//////////////////////////////////////////////////////////////////////////////

void igSimulator::setCallback(igSimulator::callback_t callback)
{
    this->callback = callback;
}

void igSimulator::setInitialTime(double initial_time)
{
    this->time = initial_time;
}

void igSimulator::setMesh(igMesh *mesh)
{
    this->mesh = mesh;
}

void igSimulator::setMeshMover(igMeshMover *mesh_mover)
{

}

void igSimulator::setInitialSolution(vector<double> *state)
{
    this->state = state;
}

void igSimulator::setCoordinates(vector<double> *coord)
{
    this->coordinates = coord;
}

void igSimulator::setVelocities(vector<double> *vel)
{
    this->velocities = vel;
}

void igSimulator::setCommunicator(igCommunicator *communicator)
{
    this->communicator = communicator;
}

void igSimulator::setSolver(const string& solver_name)
{
    this->solver_name = solver_name;
}

void igSimulator::setStructure(const string& structure_name)
{
    this->structure_name = structure_name;
}

void igSimulator::setIntegrator(const string& integrator_name)
{
    this->integrator_name = integrator_name;
}

void igSimulator::setFinalTime(double end_time)
{
    this->end_time = end_time;
}

void igSimulator::setSavePeriod(double period_save)
{
    this->period_save = period_save;
}

void igSimulator::setCflCoefficient(double cfl_coefficient)
{
    this->cfl_coefficient = cfl_coefficient;
}

void igSimulator::setSolverParameters(vector<double> *solver_parameters)
{
    IG_ASSERT(solver_parameters, "no solver_parameters");

    this->solver_parameters = solver_parameters;
}

void igSimulator::setRefineCoefficient(double refine_coef)
{
    this->refine_coef = refine_coef;
}

void igSimulator::setCoarsenCoefficient(double coarsen_coef)
{
    this->coarsen_coef = coarsen_coef;
}

void igSimulator::setAdaptPeriod(double adapt_period)
{
    this->adapt_period = adapt_period;
}

void igSimulator::setRefineMaxLevel(int level)
{
    this->refine_max_level = level;
}

void igSimulator::setAdaptBox(vector<double> *amr_box)
{
    IG_ASSERT(amr_box, "no amr_box");

    this->amr_box = amr_box;
}

///////////////////////////////////////////////

void igSimulator::enableErrorComputation(bool compute_error)
{
    this->compute_error = compute_error;
}

void igSimulator::setShockCapturing(double shock_capturing_coef)
{
    this->shock_capturing_coef = shock_capturing_coef;
    if(shock_capturing_coef > 1.E-15)
        this->shock_capturing = true;
    else
        this->shock_capturing = false;
}

void igSimulator::setSmoothingViscosityNumber(int number)
{
    smoothing_step_number = number;
}

void igSimulator::enableVorticity(bool vorticity_flag)
{
	this->flag_vorticity = vorticity_flag;
}

double igSimulator::refinementTimeStep(igSolver *solver, igMesh *mesh)
{
    int degree = mesh->element(0)->cellDegree();

    double time_step = solver->timeStep();

    return time_step * double(2*degree+1) / cfl_coefficient * this->adapt_period;

}

void igSimulator::enableRestart(bool restart)
{
    this->restart = restart;
}

void igSimulator::enableCheckPositivity(bool positivity)
{
    this->check_positivity = positivity;
}
