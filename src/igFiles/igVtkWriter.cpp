/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igVtkWriter.h"

#include <igCore/igCell.h>
#include <igCore/igFace.h>
#include <igCore/igElement.h>
#include <igCore/igMesh.h>
#include <igCore/igDistributor.h>

#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <math.h>
#include <igCore/igAssert.h>
#include <igCore/igBasisBezier.h>
#include <igCore/igBoundaryIds.h>

using namespace std;

/////////////////////////////////////////////////////////////////////////////

igVtkWriter::igVtkWriter()
{

}

igVtkWriter::~igVtkWriter(void)
{

}

/////////////////////////////////////////////////////////////////////////////

void igVtkWriter::convertGlVis(int mesh_id, int file_id, int proc_number, int refine_number, bool density, bool energy, bool momentum)
{
    vector<int> solution_ids;
    if(density)
        solution_ids.push_back(0);
    if(momentum){
        solution_ids.push_back(1);
        solution_ids.push_back(2);
        solution_ids.push_back(3);
    }
    if(energy)
        solution_ids.push_back(4);

    int solution_number = solution_ids.size();


    //--------------- loop over processes -----------------

    for(int iproc=0; iproc<proc_number; iproc++){

        ostringstream mesh_filename;
        mesh_filename << "igloo.mesh" << mesh_id << "."  << setfill('0') << setw(6) << iproc;

        ifstream mesh_file;
        mesh_file.open(mesh_filename.str(), ios::in);

        // skip header
        mesh_file.ignore(100,'\n');
        mesh_file.ignore(100,'\n');
        mesh_file.ignore(100,'\n');
        mesh_file.ignore(100,'\n');
        mesh_file.ignore(100,'\n');
        mesh_file.ignore(100,'\n');

        int total_element_number = 0;
        mesh_file >> total_element_number;
        cout << "total elt " << total_element_number << endl;
        mesh_file.ignore(100,'\n');

        // skip elements data
        for (int iel=0; iel<total_element_number; iel++){
            mesh_file.ignore(100,'\n');
        }

        // skip boundary and edges data
        mesh_file.ignore(100,'\n');
        mesh_file.ignore(100,'\n');
        mesh_file.ignore(100,'\n');
        mesh_file.ignore(100,'\n');
        mesh_file.ignore(100,'\n');

        int edge_number = 0;
        mesh_file >> edge_number;
        cout << "edge number " << edge_number << endl;
        mesh_file.ignore(100,'\n');

        for (int iel=0; iel<total_element_number; iel++){
            mesh_file.ignore(100,'\n');
            mesh_file.ignore(100,'\n');
            mesh_file.ignore(100,'\n');
            mesh_file.ignore(100,'\n');
            mesh_file.ignore(100,'\n');
            mesh_file.ignore(100,'\n');
            mesh_file.ignore(100,'\n');
            mesh_file.ignore(100,'\n');
            mesh_file.ignore(100,'\n');
            mesh_file.ignore(100,'\n');
            mesh_file.ignore(100,'\n');
            mesh_file.ignore(100,'\n');
        }

        // skip vertices and knot data
        mesh_file.ignore(100,'\n');
        mesh_file.ignore(100,'\n');
        mesh_file.ignore(100,'\n');
        mesh_file.ignore(100,'\n');
        mesh_file.ignore(100,'\n');
        mesh_file.ignore(100,'\n');

        int degree = 0;
        mesh_file >> degree;
        mesh_file.ignore(100,'\n');

        cout << "degree " << degree << endl;

        int dof_per_elt = (degree+1)*(degree+1)*(degree+1);

        mesh_file.ignore(100,'\n');
        mesh_file.ignore(100,'\n');

        int element_number = 0;
        mesh_file >> element_number;
        mesh_file.ignore(100,'\n');

        cout << "element number " << element_number << endl;

        // skip global ids
        for(int iel=0; iel<element_number; iel++){
            mesh_file.ignore(100,'\n');
        }

        mesh_file.ignore(100,'\n');
        mesh_file.ignore(100,'\n');

        //----------------- read weights ------------------------

        vector< vector<double> > weights;
        weights.resize(element_number);
        for(int iel=0; iel<element_number; iel++){
            weights.at(iel).resize(dof_per_elt);
        }
        int offset;

        // corner data
        for(int iel=0; iel<element_number; iel++){
                mesh_file >> weights.at(iel).at(0);
                mesh_file >> weights.at(iel).at(degree);
                mesh_file >> weights.at(iel).at((degree+1)*degree + degree);
                mesh_file >> weights.at(iel).at((degree+1)*degree);
                mesh_file >> weights.at(iel).at((degree+1)*(degree+1)*degree);
                mesh_file >> weights.at(iel).at((degree+1)*(degree+1)*degree + degree);
                mesh_file >> weights.at(iel).at((degree+1)*(degree+1)*degree + (degree+1)*degree + degree);
                mesh_file >> weights.at(iel).at((degree+1)*(degree+1)*degree + (degree+1)*degree);
        }

        // edge data
        for (int iel=0; iel<element_number; iel++){

            // bottom edges
            for (int ipt=1; ipt<degree; ipt++)
                mesh_file >> weights.at(iel).at(ipt) ;
            for (int ipt=1; ipt<degree; ipt++)
                mesh_file >> weights.at(iel).at(degree+ipt*(degree+1)) ;
            for (int ipt=1; ipt<degree; ipt++)
                mesh_file >> weights.at(iel).at((degree+1)*(degree+1)-ipt-1) ;
            for (int ipt=1; ipt<degree; ipt++)
                mesh_file >> weights.at(iel).at(ipt*(degree+1)) ;

            // top edges
            offset = (degree+1)*(degree+1)*degree;
            for (int ipt=1; ipt<degree; ipt++)
                mesh_file >> weights.at(iel).at(offset+ipt) ;
            for (int ipt=1; ipt<degree; ipt++)
                mesh_file >> weights.at(iel).at(offset+degree+ipt*(degree+1)) ;
            for (int ipt=1; ipt<degree; ipt++)
                mesh_file >> weights.at(iel).at(offset+(degree+1)*(degree+1)-ipt-1) ;
            for (int ipt=1; ipt<degree; ipt++)
                mesh_file >> weights.at(iel).at(offset+ipt*(degree+1)) ;

            // side edges
            for (int ipt=1; ipt<degree; ipt++)
                mesh_file >> weights.at(iel).at(ipt*(degree+1)*(degree+1)) ;
            for (int ipt=1; ipt<degree; ipt++)
                mesh_file >> weights.at(iel).at(degree + ipt*(degree+1)*(degree+1)) ;
            for (int ipt=1; ipt<degree; ipt++)
                mesh_file >> weights.at(iel).at(degree*(degree+1)+degree + ipt*(degree+1)*(degree+1)) ;
            for (int ipt=1; ipt<degree; ipt++)
                mesh_file >> weights.at(iel).at(degree*(degree+1) + ipt*(degree+1)*(degree+1)) ;

        }

        //face data
        for (int iel=0; iel<element_number; iel++){

            // bottom face
            for (int ipt2=1; ipt2<degree; ipt2++){
                for (int ipt1=1; ipt1<degree; ipt1++){
                    mesh_file >> weights.at(iel).at((degree+1)*degree + ipt1 - (degree+1)*ipt2) ;
                }
            }

            // south face
            for (int ipt3=1; ipt3<degree; ipt3++){
                for (int ipt1=1; ipt1<degree; ipt1++){
                    mesh_file >> weights.at(iel).at(ipt1 + (degree+1)*(degree+1)*ipt3) ;
                }
            }

            //  east face
            for (int ipt3=1; ipt3<degree; ipt3++){
                for (int ipt2=1; ipt2<degree; ipt2++){
                    mesh_file >> weights.at(iel).at(degree + ipt2*(degree+1) +(degree+1)*(degree+1)*ipt3) ;
                }
            }

            //  north face
            for (int ipt3=1; ipt3<degree; ipt3++){
                for (int ipt1=1; ipt1<degree; ipt1++){
                    mesh_file >> weights.at(iel).at((degree+1)*(degree+1) -1 - ipt1 +(degree+1)*(degree+1)*ipt3) ;
                }
            }

            // west face
            for (int ipt3=1; ipt3<degree; ipt3++){
                for (int ipt2=1; ipt2<degree; ipt2++){
                    mesh_file >> weights.at(iel).at(degree*(degree+1) -ipt2*(degree+1) + (degree+1)*(degree+1)*ipt3) ;
                }
            }

            // top face
            for (int ipt2=1; ipt2<degree; ipt2++){
                for (int ipt1=1; ipt1<degree; ipt1++){
                    mesh_file >> weights.at(iel).at((degree+1)*(degree+1)*degree + ipt1+(degree+1)*ipt2) ;
                }
            }
        }

        // interior data
        for (int iel=0; iel<element_number; iel++){

            for (int ipt3=1; ipt3<degree; ipt3++){
                for (int ipt2=1; ipt2<degree; ipt2++){
                    for (int ipt1=1; ipt1<degree; ipt1++){

                        mesh_file >> weights.at(iel).at(ipt1 + (degree+1)*ipt2 + (degree+1)*(degree+1)*ipt3) ;
                    }
                }
            }
        }

        mesh_file.ignore(100,'\n');
        mesh_file.ignore(100,'\n');
        mesh_file.ignore(100,'\n');
        mesh_file.ignore(100,'\n');
        mesh_file.ignore(100,'\n');
        mesh_file.ignore(100,'\n');
        mesh_file.ignore(100,'\n');


        //----------------- read setCoordinates ------------------------

        vector< vector<double> > coordx;
        coordx.resize(element_number);
        for(int iel=0; iel<element_number; iel++){
            coordx.at(iel).resize(dof_per_elt);
        }
        vector< vector<double> > coordy;
        coordy.resize(element_number);
        for(int iel=0; iel<element_number; iel++){
            coordy.at(iel).resize(dof_per_elt);
        }
        vector< vector<double> > coordz;
        coordz.resize(element_number);
        for(int iel=0; iel<element_number; iel++){
            coordz.at(iel).resize(dof_per_elt);
        }

        // corner data
        for(int iel=0; iel<element_number; iel++){
                mesh_file >> coordx.at(iel).at(0);
                mesh_file >> coordy.at(iel).at(0);
                mesh_file >> coordz.at(iel).at(0);
                mesh_file >> coordx.at(iel).at(degree);
                mesh_file >> coordy.at(iel).at(degree);
                mesh_file >> coordz.at(iel).at(degree);
                mesh_file >> coordx.at(iel).at((degree+1)*degree + degree);
                mesh_file >> coordy.at(iel).at((degree+1)*degree + degree);
                mesh_file >> coordz.at(iel).at((degree+1)*degree + degree);
                mesh_file >> coordx.at(iel).at((degree+1)*degree);
                mesh_file >> coordy.at(iel).at((degree+1)*degree);
                mesh_file >> coordz.at(iel).at((degree+1)*degree);
                mesh_file >> coordx.at(iel).at((degree+1)*(degree+1)*degree);
                mesh_file >> coordy.at(iel).at((degree+1)*(degree+1)*degree);
                mesh_file >> coordz.at(iel).at((degree+1)*(degree+1)*degree);
                mesh_file >> coordx.at(iel).at((degree+1)*(degree+1)*degree + degree);
                mesh_file >> coordy.at(iel).at((degree+1)*(degree+1)*degree + degree);
                mesh_file >> coordz.at(iel).at((degree+1)*(degree+1)*degree + degree);
                mesh_file >> coordx.at(iel).at((degree+1)*(degree+1)*degree + (degree+1)*degree + degree);
                mesh_file >> coordy.at(iel).at((degree+1)*(degree+1)*degree + (degree+1)*degree + degree);
                mesh_file >> coordz.at(iel).at((degree+1)*(degree+1)*degree + (degree+1)*degree + degree);
                mesh_file >> coordx.at(iel).at((degree+1)*(degree+1)*degree + (degree+1)*degree);
                mesh_file >> coordy.at(iel).at((degree+1)*(degree+1)*degree + (degree+1)*degree);
                mesh_file >> coordz.at(iel).at((degree+1)*(degree+1)*degree + (degree+1)*degree);
        }

        // edge data
        for (int iel=0; iel<element_number; iel++){

            // bottom edges
            for (int ipt=1; ipt<degree; ipt++){
                mesh_file >> coordx.at(iel).at(ipt) ;
                mesh_file >> coordy.at(iel).at(ipt) ;
                mesh_file >> coordz.at(iel).at(ipt) ;
            }
            for (int ipt=1; ipt<degree; ipt++){
                mesh_file >> coordx.at(iel).at(degree+ipt*(degree+1)) ;
                mesh_file >> coordy.at(iel).at(degree+ipt*(degree+1)) ;
                mesh_file >> coordz.at(iel).at(degree+ipt*(degree+1)) ;
            }
            for (int ipt=1; ipt<degree; ipt++){
                mesh_file >> coordx.at(iel).at((degree+1)*(degree+1)-ipt-1) ;
                mesh_file >> coordy.at(iel).at((degree+1)*(degree+1)-ipt-1) ;
                mesh_file >> coordz.at(iel).at((degree+1)*(degree+1)-ipt-1) ;
            }
            for (int ipt=1; ipt<degree; ipt++){
                mesh_file >> coordx.at(iel).at(ipt*(degree+1)) ;
                mesh_file >> coordy.at(iel).at(ipt*(degree+1)) ;
                mesh_file >> coordz.at(iel).at(ipt*(degree+1)) ;
            }

            // top edges
            offset = (degree+1)*(degree+1)*degree;
            for (int ipt=1; ipt<degree; ipt++){
                mesh_file >> coordx.at(iel).at(offset+ipt) ;
                mesh_file >> coordy.at(iel).at(offset+ipt) ;
                mesh_file >> coordz.at(iel).at(offset+ipt) ;
            }
            for (int ipt=1; ipt<degree; ipt++){
                mesh_file >> coordx.at(iel).at(offset+degree+ipt*(degree+1)) ;
                mesh_file >> coordy.at(iel).at(offset+degree+ipt*(degree+1)) ;
                mesh_file >> coordz.at(iel).at(offset+degree+ipt*(degree+1)) ;
            }
            for (int ipt=1; ipt<degree; ipt++){
                mesh_file >> coordx.at(iel).at(offset+(degree+1)*(degree+1)-ipt-1) ;
                mesh_file >> coordy.at(iel).at(offset+(degree+1)*(degree+1)-ipt-1) ;
                mesh_file >> coordz.at(iel).at(offset+(degree+1)*(degree+1)-ipt-1) ;
            }
            for (int ipt=1; ipt<degree; ipt++){
                mesh_file >> coordx.at(iel).at(offset+ipt*(degree+1)) ;
                mesh_file >> coordy.at(iel).at(offset+ipt*(degree+1)) ;
                mesh_file >> coordz.at(iel).at(offset+ipt*(degree+1)) ;
            }

            // side edges
            for (int ipt=1; ipt<degree; ipt++){
                mesh_file >> coordx.at(iel).at(ipt*(degree+1)*(degree+1)) ;
                mesh_file >> coordy.at(iel).at(ipt*(degree+1)*(degree+1)) ;
                mesh_file >> coordz.at(iel).at(ipt*(degree+1)*(degree+1)) ;
            }
            for (int ipt=1; ipt<degree; ipt++){
                mesh_file >> coordx.at(iel).at(degree + ipt*(degree+1)*(degree+1)) ;
                mesh_file >> coordy.at(iel).at(degree + ipt*(degree+1)*(degree+1)) ;
                mesh_file >> coordz.at(iel).at(degree + ipt*(degree+1)*(degree+1)) ;
            }
            for (int ipt=1; ipt<degree; ipt++){
                mesh_file >> coordx.at(iel).at(degree*(degree+1)+degree + ipt*(degree+1)*(degree+1)) ;
                mesh_file >> coordy.at(iel).at(degree*(degree+1)+degree + ipt*(degree+1)*(degree+1)) ;
                mesh_file >> coordz.at(iel).at(degree*(degree+1)+degree + ipt*(degree+1)*(degree+1)) ;
            }
            for (int ipt=1; ipt<degree; ipt++){
                mesh_file >> coordx.at(iel).at(degree*(degree+1) + ipt*(degree+1)*(degree+1)) ;
                mesh_file >> coordy.at(iel).at(degree*(degree+1) + ipt*(degree+1)*(degree+1)) ;
                mesh_file >> coordz.at(iel).at(degree*(degree+1) + ipt*(degree+1)*(degree+1)) ;
            }

        }

        //face data
        for (int iel=0; iel<element_number; iel++){

            // bottom face
            for (int ipt2=1; ipt2<degree; ipt2++){
                for (int ipt1=1; ipt1<degree; ipt1++){
                    mesh_file >> coordx.at(iel).at((degree+1)*degree + ipt1 - (degree+1)*ipt2) ;
                    mesh_file >> coordy.at(iel).at((degree+1)*degree + ipt1 - (degree+1)*ipt2) ;
                    mesh_file >> coordz.at(iel).at((degree+1)*degree + ipt1 - (degree+1)*ipt2) ;
                }
            }

            // south face
            for (int ipt3=1; ipt3<degree; ipt3++){
                for (int ipt1=1; ipt1<degree; ipt1++){
                    mesh_file >> coordx.at(iel).at(ipt1 + (degree+1)*(degree+1)*ipt3) ;
                    mesh_file >> coordy.at(iel).at(ipt1 + (degree+1)*(degree+1)*ipt3) ;
                    mesh_file >> coordz.at(iel).at(ipt1 + (degree+1)*(degree+1)*ipt3) ;
                }
            }

            //  east face
            for (int ipt3=1; ipt3<degree; ipt3++){
                for (int ipt2=1; ipt2<degree; ipt2++){
                    mesh_file >> coordx.at(iel).at(degree + ipt2*(degree+1) +(degree+1)*(degree+1)*ipt3) ;
                    mesh_file >> coordy.at(iel).at(degree + ipt2*(degree+1) +(degree+1)*(degree+1)*ipt3) ;
                    mesh_file >> coordz.at(iel).at(degree + ipt2*(degree+1) +(degree+1)*(degree+1)*ipt3) ;
                }
            }

            //  north face
            for (int ipt3=1; ipt3<degree; ipt3++){
                for (int ipt1=1; ipt1<degree; ipt1++){
                    mesh_file >> coordx.at(iel).at((degree+1)*(degree+1) -1 - ipt1 +(degree+1)*(degree+1)*ipt3) ;
                    mesh_file >> coordy.at(iel).at((degree+1)*(degree+1) -1 - ipt1 +(degree+1)*(degree+1)*ipt3) ;
                    mesh_file >> coordz.at(iel).at((degree+1)*(degree+1) -1 - ipt1 +(degree+1)*(degree+1)*ipt3) ;
                }
            }

            // west face
            for (int ipt3=1; ipt3<degree; ipt3++){
                for (int ipt2=1; ipt2<degree; ipt2++){
                    mesh_file >> coordx.at(iel).at(degree*(degree+1) -ipt2*(degree+1) + (degree+1)*(degree+1)*ipt3) ;
                    mesh_file >> coordy.at(iel).at(degree*(degree+1) -ipt2*(degree+1) + (degree+1)*(degree+1)*ipt3) ;
                    mesh_file >> coordz.at(iel).at(degree*(degree+1) -ipt2*(degree+1) + (degree+1)*(degree+1)*ipt3) ;
                }
            }

            // top face
            for (int ipt2=1; ipt2<degree; ipt2++){
                for (int ipt1=1; ipt1<degree; ipt1++){
                    mesh_file >> coordx.at(iel).at((degree+1)*(degree+1)*degree + ipt1+(degree+1)*ipt2) ;
                    mesh_file >> coordy.at(iel).at((degree+1)*(degree+1)*degree + ipt1+(degree+1)*ipt2) ;
                    mesh_file >> coordz.at(iel).at((degree+1)*(degree+1)*degree + ipt1+(degree+1)*ipt2) ;
                }
            }
        }

        // interior data
        for (int iel=0; iel<element_number; iel++){

            for (int ipt3=1; ipt3<degree; ipt3++){
                for (int ipt2=1; ipt2<degree; ipt2++){
                    for (int ipt1=1; ipt1<degree; ipt1++){

                        mesh_file >> coordx.at(iel).at(ipt1 + (degree+1)*ipt2 + (degree+1)*(degree+1)*ipt3) ;
                        mesh_file >> coordy.at(iel).at(ipt1 + (degree+1)*ipt2 + (degree+1)*(degree+1)*ipt3) ;
                        mesh_file >> coordz.at(iel).at(ipt1 + (degree+1)*ipt2 + (degree+1)*(degree+1)*ipt3) ;
                    }
                }
            }
        }
        mesh_file.close();

        //-------------------- read solution file ------------------------

        vector< vector< vector<double> > > solution;
        solution.resize(solution_number);

        for(int isol = 0; isol < solution_number; isol++){

            int sol_id = solution_ids.at(isol);

            ostringstream sol_filename;
            sol_filename << "igloo_" << sol_id << "_" << file_id << ".sol." << setfill('0') << setw(6) << iproc;
            cout << "file " << sol_filename.str() << endl;
            ifstream sol_file;
            sol_file.open(sol_filename.str(), ios::in);

            // skip header
            sol_file.ignore(100,'\n');
            sol_file.ignore(100,'\n');
            sol_file.ignore(100,'\n');
            sol_file.ignore(100,'\n');
            sol_file.ignore(100,'\n');

            solution.at(isol).resize(element_number);
            for(int iel=0; iel<element_number; iel++){
                solution.at(isol).at(iel).resize(dof_per_elt);
            }
            int offset;

            // corner data
            for(int iel=0; iel<element_number; iel++){
                    sol_file >> solution.at(isol).at(iel).at(0);
                    sol_file >> solution.at(isol).at(iel).at(degree);
                    sol_file >> solution.at(isol).at(iel).at((degree+1)*degree + degree);
                    sol_file >> solution.at(isol).at(iel).at((degree+1)*degree);
                    sol_file >> solution.at(isol).at(iel).at((degree+1)*(degree+1)*degree);
                    sol_file >> solution.at(isol).at(iel).at((degree+1)*(degree+1)*degree + degree);
                    sol_file >> solution.at(isol).at(iel).at((degree+1)*(degree+1)*degree + (degree+1)*degree + degree);
                    sol_file >> solution.at(isol).at(iel).at((degree+1)*(degree+1)*degree + (degree+1)*degree);
            }

            // edge data
            for (int iel=0; iel<element_number; iel++){

                // bottom edges
                for (int ipt=1; ipt<degree; ipt++)
                    sol_file >> solution.at(isol).at(iel).at(ipt) ;
                for (int ipt=1; ipt<degree; ipt++)
                    sol_file >> solution.at(isol).at(iel).at(degree+ipt*(degree+1)) ;
                for (int ipt=1; ipt<degree; ipt++)
                    sol_file >> solution.at(isol).at(iel).at((degree+1)*(degree+1)-ipt-1) ;
                for (int ipt=1; ipt<degree; ipt++)
                    sol_file >> solution.at(isol).at(iel).at(ipt*(degree+1)) ;

                // top edges
                offset = (degree+1)*(degree+1)*degree;
                for (int ipt=1; ipt<degree; ipt++)
                    sol_file >> solution.at(isol).at(iel).at(offset+ipt) ;
                for (int ipt=1; ipt<degree; ipt++)
                    sol_file >> solution.at(isol).at(iel).at(offset+degree+ipt*(degree+1)) ;
                for (int ipt=1; ipt<degree; ipt++)
                    sol_file >> solution.at(isol).at(iel).at(offset+(degree+1)*(degree+1)-ipt-1) ;
                for (int ipt=1; ipt<degree; ipt++)
                    sol_file >> solution.at(isol).at(iel).at(offset+ipt*(degree+1)) ;

                // side edges
                for (int ipt=1; ipt<degree; ipt++)
                    sol_file >> solution.at(isol).at(iel).at(ipt*(degree+1)*(degree+1)) ;
                for (int ipt=1; ipt<degree; ipt++)
                    sol_file >> solution.at(isol).at(iel).at(degree + ipt*(degree+1)*(degree+1)) ;
                for (int ipt=1; ipt<degree; ipt++)
                    sol_file >> solution.at(isol).at(iel).at(degree*(degree+1)+degree + ipt*(degree+1)*(degree+1)) ;
                for (int ipt=1; ipt<degree; ipt++)
                    sol_file >> solution.at(isol).at(iel).at(degree*(degree+1) + ipt*(degree+1)*(degree+1)) ;

            }

            //face data
            for (int iel=0; iel<element_number; iel++){

                // bottom face
                for (int ipt2=1; ipt2<degree; ipt2++){
                    for (int ipt1=1; ipt1<degree; ipt1++){
                        sol_file >> solution.at(isol).at(iel).at((degree+1)*degree + ipt1 - (degree+1)*ipt2) ;
                    }
                }

                // south face
                for (int ipt3=1; ipt3<degree; ipt3++){
                    for (int ipt1=1; ipt1<degree; ipt1++){
                        sol_file >> solution.at(isol).at(iel).at(ipt1 + (degree+1)*(degree+1)*ipt3) ;
                    }
                }

                //  east face
                for (int ipt3=1; ipt3<degree; ipt3++){
                    for (int ipt2=1; ipt2<degree; ipt2++){
                        sol_file >> solution.at(isol).at(iel).at(degree + ipt2*(degree+1) +(degree+1)*(degree+1)*ipt3) ;
                    }
                }

                //  north face
                for (int ipt3=1; ipt3<degree; ipt3++){
                    for (int ipt1=1; ipt1<degree; ipt1++){
                        sol_file >> solution.at(isol).at(iel).at((degree+1)*(degree+1) -1 - ipt1 +(degree+1)*(degree+1)*ipt3) ;
                    }
                }

                // west face
                for (int ipt3=1; ipt3<degree; ipt3++){
                    for (int ipt2=1; ipt2<degree; ipt2++){
                        sol_file >> solution.at(isol).at(iel).at(degree*(degree+1) -ipt2*(degree+1) + (degree+1)*(degree+1)*ipt3) ;
                    }
                }

                // top face
                for (int ipt2=1; ipt2<degree; ipt2++){
                    for (int ipt1=1; ipt1<degree; ipt1++){
                        sol_file >> solution.at(isol).at(iel).at((degree+1)*(degree+1)*degree + ipt1+(degree+1)*ipt2) ;
                    }
                }
            }

            // interior data
            for (int iel=0; iel<element_number; iel++){

                for (int ipt3=1; ipt3<degree; ipt3++){
                    for (int ipt2=1; ipt2<degree; ipt2++){
                        for (int ipt1=1; ipt1<degree; ipt1++){

                            sol_file >> solution.at(isol).at(iel).at(ipt1 + (degree+1)*ipt2 + (degree+1)*(degree+1)*ipt3) ;
                        }
                    }
                }
            }
            sol_file.close();

        }

        //-------------------- write VTK file ------------------------

        int sample_number = 2 + refine_number;
        int cell_number = element_number*(refine_number+1)*(refine_number+1)*(refine_number+1);
        int control_point_number_by_direction = degree+1;
        vector<double> values1(control_point_number_by_direction,0.);
        vector<double> values2(control_point_number_by_direction,0.);
        vector<double> values3(control_point_number_by_direction,0.);
        igBasisBezier basis1(degree);
        igBasisBezier basis2(degree);
        igBasisBezier basis3(degree);

        ostringstream vtk_filename;
        vtk_filename << "igloo."  << setfill('0') << setw(6) << iproc << ".vtk";

        ofstream vtk_file;
        vtk_file.open(vtk_filename.str(), ios::out);

        // VTK header
        vtk_file << "# vtk DataFile Version 2.0" << endl;
        vtk_file << "Unstructured Grid Igloo" << endl;
        vtk_file << "ASCII" << endl;
        vtk_file << "DATASET UNSTRUCTURED_GRID" << endl;
        vtk_file << endl;

        // points
        vtk_file << "POINTS " << element_number*sample_number*sample_number*sample_number << " float" << endl;
        for (int iel=0; iel<element_number; iel++){

            // internal element sampling
            for(int k=0; k<sample_number; k++){

                double param3 = float(k)/float(sample_number-1);
                basis3.evalFunction(param3, values3);

                for(int j=0; j<sample_number; j++){

                    double param2 = float(j)/float(sample_number-1);
                    basis2.evalFunction(param2, values2);

                    for(int i=0; i<sample_number; i++){

                        double param1 = float(i)/float(sample_number-1);
                        basis1.evalFunction(param1, values1);

                        // evaluation of weighted sum
                        double weighted_sum = 0;
                        int index_w = 0;
                        for (int ictl3 = 0; ictl3 < control_point_number_by_direction; ++ictl3) {
                            for (int ictl2 = 0; ictl2 < control_point_number_by_direction; ++ictl2) {
                                for (int ictl1 = 0; ictl1 < control_point_number_by_direction; ++ictl1) {
                                    weighted_sum += values1[ictl1]*values2[ictl2]*values3[ictl3]*weights.at(iel).at(index_w);
                                    ++index_w;
                                }
                            }
                        }

                        // evaluation of NURBS value
                        index_w = 0;
                        double value_x = 0;
                        double value_y = 0;
                        double value_z = 0;
                        for (int ictl3 = 0; ictl3 < control_point_number_by_direction; ++ictl3) {
                            for (int ictl2 = 0; ictl2 < control_point_number_by_direction; ++ictl2) {
                                for (int ictl1 = 0; ictl1 < control_point_number_by_direction; ++ictl1) {

                                    value_x += values1[ictl1]*values2[ictl2]*values3[ictl3]*
                                                weights.at(iel).at(index_w) * coordx.at(iel).at(index_w)/weighted_sum;
                                    value_y += values1[ictl1]*values2[ictl2]*values3[ictl3]*
                                                weights.at(iel).at(index_w) * coordy.at(iel).at(index_w)/weighted_sum;
                                    value_z += values1[ictl1]*values2[ictl2]*values3[ictl3]*
                                                weights.at(iel).at(index_w) * coordz.at(iel).at(index_w)/weighted_sum;
                                    ++index_w;
                                }
                            }
                        }

                        vtk_file << value_x << " " << value_y << " " << value_z << endl;

                    }
                }
            }

        }
        vtk_file << endl;

        // cells
        offset = 0;
        vtk_file << "CELLS " << cell_number << " " << cell_number*9 << endl;

        for (int iel=0; iel<element_number; iel++){

            for(int k=0; k<sample_number-1; k++){
                for(int j=0; j<sample_number-1; j++){
                    for(int i=0; i<sample_number-1; i++){

                        int index0 = i + sample_number*j + sample_number*sample_number*k;
                        int index1 = (i+1) + sample_number*j + sample_number*sample_number*k;
                        int index2 = (i+1) + sample_number*(j+1) + sample_number*sample_number*k;
                        int index3 = i + sample_number*(j+1) + sample_number*sample_number*k;
                        int index4 = i + sample_number*j + sample_number*sample_number*(k+1);
                        int index5 = (i+1) + sample_number*j + sample_number*sample_number*(k+1);
                        int index6 = (i+1) + sample_number*(j+1) + sample_number*sample_number*(k+1);
                        int index7 = i + sample_number*(j+1) + sample_number*sample_number*(k+1);

                        vtk_file << 8 << " " ;
                        vtk_file << offset+index0 << " " << offset+index1 << " " << offset+index2 << " "<< offset+index3 << " ";
                        vtk_file << offset+index4 << " " << offset+index5 << " " << offset+index6 << " "<< offset+index7 << " ";
                        vtk_file << endl;
                    }
                }
            }
            offset +=sample_number*sample_number*sample_number;
        }
        vtk_file << endl;

        // types
        vtk_file << "CELL_TYPES " << cell_number << endl;
        for (int iel=0; iel<cell_number; iel++){
            vtk_file << 12 << endl;
        }
        vtk_file << endl;

        vtk_file << "POINT_DATA " << element_number*sample_number*sample_number*sample_number << endl;

        for(int isol=0; isol < solution_number; isol++){

            int sol_id = solution_ids.at(isol);

            string solution_name;
            if(sol_id == 0)
                solution_name = "density";
            if(sol_id == 1)
                    solution_name = "mom-x";
            if(sol_id == 2)
                    solution_name = "mom-y";
            if(sol_id == 3)
                    solution_name = "mom-z";
            if(sol_id == 4)
                solution_name = "energy";

            vtk_file << "SCALARS " << solution_name << " float 1" << endl;
            vtk_file << "LOOKUP_TABLE default" << endl;

            for (int iel=0; iel<element_number; iel++){

                // internal element sampling
                for(int k=0; k<sample_number; k++){

                    double param3 = float(k)/float(sample_number-1);
                    basis3.evalFunction(param3, values3);

                    for(int j=0; j<sample_number; j++){

                        double param2 = float(j)/float(sample_number-1);
                        basis2.evalFunction(param2, values2);

                        for(int i=0; i<sample_number; i++){

                            double param1 = float(i)/float(sample_number-1);
                            basis1.evalFunction(param1, values1);

                            // evaluation of weighted sum
                            double weighted_sum = 0;
                            int index_w = 0;
                            for (int ictl3 = 0; ictl3 < control_point_number_by_direction; ++ictl3) {
                                for (int ictl2 = 0; ictl2 < control_point_number_by_direction; ++ictl2) {
                                    for (int ictl1 = 0; ictl1 < control_point_number_by_direction; ++ictl1) {
                                        weighted_sum += values1[ictl1]*values2[ictl2]*values3[ictl3]*weights.at(iel).at(index_w);
                                        ++index_w;
                                    }
                                }
                            }

                            // evaluation of NURBS value
                            index_w = 0;
                            double value_sol = 0;
                            for (int ictl3 = 0; ictl3 < control_point_number_by_direction; ++ictl3) {
                                for (int ictl2 = 0; ictl2 < control_point_number_by_direction; ++ictl2) {
                                    for (int ictl1 = 0; ictl1 < control_point_number_by_direction; ++ictl1) {

                                        value_sol += values1[ictl1]*values2[ictl2]*values3[ictl3]*
                                                    weights.at(iel).at(index_w) * solution.at(isol).at(iel).at(index_w)/weighted_sum;
                                            ++index_w;
                                    }
                                }
                            }

                            vtk_file << value_sol << endl;

                        }
                    }
                }

            }
            vtk_file << endl;


        }

    }

    return;

}

////////////////////////////////////////////////////////////////////////////////////

void igVtkWriter::extractWall(igMesh *mesh, vector<double> *coordinates, int refine_number)
{

    // vtk file init
    ostringstream vtk_filename;
    vtk_filename << "igloo_wall.vtk";

    ofstream vtk_file;
    vtk_file.open(vtk_filename.str(), ios::out);

    // VTK header
    vtk_file << "# vtk DataFile Version 2.0" << endl;
    vtk_file << "Unstructured Grid Igloo" << endl;
    vtk_file << "ASCII" << endl;
    vtk_file << "DATASET UNSTRUCTURED_GRID" << endl;
    vtk_file << endl;

    // compute number of wall Faces
    int wall_face_number = 0;
    for (int ifac = 0; ifac < mesh->boundaryFaceNumber(); ++ifac) {
        igFace *face = mesh->boundaryFace(ifac);
        int face_type = face->type();
        if(face_type == id_slip_wall || face_type == id_noslip_wall){
            wall_face_number++;
        }
    }

    int sample_number = 2 + refine_number;
    int cell_number = wall_face_number*(refine_number+1)*(refine_number+1);
    int degree = mesh->element(0)->cellDegree();
    int control_point_number_by_direction = degree+1;
    vector<double> values1(control_point_number_by_direction,0.);
    vector<double> values2(control_point_number_by_direction,0.);
    igBasisBezier basis1(degree);
    igBasisBezier basis2(degree);

    // points
    vtk_file << "POINTS " << wall_face_number*sample_number*sample_number << " float" << endl;

    for (int ifac = 0; ifac < mesh->boundaryFaceNumber(); ++ifac) {

        igFace *face = mesh->boundaryFace(ifac);
        int face_type = face->type();

        if(face_type == id_slip_wall || face_type == id_noslip_wall){

            // internal face sampling
            for(int j=0; j<sample_number; j++){

                double param2 = float(j)/float(sample_number-1);
                basis2.evalFunction(param2, values2);

                for(int i=0; i<sample_number; i++){

                    double param1 = float(i)/float(sample_number-1);
                    basis1.evalFunction(param1, values1);

                    // evaluation of weighted sum
                    double weighted_sum = 0;
                    int index_w = 0;
                    for (int ictl2 = 0; ictl2 < control_point_number_by_direction; ++ictl2) {
                        for (int ictl1 = 0; ictl1 < control_point_number_by_direction; ++ictl1) {
                            weighted_sum += values1[ictl1] * values2[ictl2] * face->controlPointWeight(index_w);
                            ++index_w;
                        }
                    }

                    // evaluation of NURBS value
                    index_w = 0;
                    double value_x = 0;
                    double value_y = 0;
                    double value_z = 0;
                    for (int ictl2 = 0; ictl2 < control_point_number_by_direction; ++ictl2) {
                        for (int ictl1 = 0; ictl1 < control_point_number_by_direction; ++ictl1) {

                            value_x += values1[ictl1] * values2[ictl2] * face->controlPointWeight(index_w)
                                        *coordinates->at(face->dofLeft(index_w,0))/weighted_sum;
                            value_y += values1[ictl1] * values2[ictl2] * face->controlPointWeight(index_w)
                                        *coordinates->at(face->dofLeft(index_w,1))/weighted_sum;
                            value_z += values1[ictl1] * values2[ictl2] * face->controlPointWeight(index_w)
                                        *coordinates->at(face->dofLeft(index_w,2))/weighted_sum;
                            ++index_w;
                        }
                    }

                    vtk_file << value_x << " " << value_y << " " << value_z << endl;

                }
            }

        }
    }
    vtk_file << endl;

    // cells
    int offset = 0;
    vtk_file << "CELLS " << cell_number << " " << cell_number*5 << endl;

    for (int ifac = 0; ifac < mesh->boundaryFaceNumber(); ++ifac) {

        igFace *face = mesh->boundaryFace(ifac);
        int face_type = face->type();

        if(face_type == id_slip_wall || face_type == id_noslip_wall){

            for(int j=0; j<sample_number-1; j++){
                for(int i=0; i<sample_number-1; i++){

                    int index0 = i + sample_number*j;
                    int index1 = (i+1) + sample_number*j;
                    int index2 = (i+1) + sample_number*(j+1);
                    int index3 = i + sample_number*(j+1);

                    vtk_file << 4 << " " ;
                    vtk_file << offset+index0 << " " << offset+index1 << " " << offset+index2 << " "<< offset+index3 ;
                    vtk_file << endl;
                }
            }
            offset +=sample_number*sample_number;
        }
    }
    vtk_file << endl;

    // types
    vtk_file << "CELL_TYPES " << cell_number << endl;
    for (int iel=0; iel<cell_number; iel++){
        vtk_file << 9 << endl;
    }
    vtk_file << endl;

    return;
}
