/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#include "igFileManager.h"

#include <igCore/igCell.h>
#include <igCore/igFace.h>
#include <igCore/igElement.h>
#include <igCore/igBasisBSpline.h>
#include <igCore/igMesh.h>
#include <igCore/igDistributor.h>
#include <igCore/igAssert.h>
#include <igCore/igBasisBezier.h>
#include <igCore/igBoundaryIds.h>

#include <igSolver/igCouplingInterface.h>

#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <math.h>



using namespace std;

/////////////////////////////////////////////////////////////////////////////

igFileManager::igFileManager()
{
    save_counter = 0;
    save_mesh_counter = -1;
    save_interface_counter = 0;

    partition_number = 1;
    my_partition = 0;

    save_mesh = false;
    save_artificial_viscosity = false;
    save_vorticity = false;
}

igFileManager::~igFileManager(void)
{

}

/////////////////////////////////////////////////////////////////////////////

void igFileManager::enableSaveMesh(void)
{
    save_mesh = true;
}

void igFileManager::enableSaveProperty(void)
{
    save_artificial_viscosity = true;
}

void igFileManager::enableSaveVorticity(void)
{
    save_vorticity = true;
}
////////////////////////////////////////////////////////////////////////////

void igFileManager::readSolutionIgloo(const string& filename, const int variable_number, igMesh *mesh, vector<double> *state)
{
    IG_ASSERT(mesh, "no mesh");
    IG_ASSERT(state, "no state");

    if(my_partition == 0)
        cout << "read solution from file: " << filename << endl;

    ifstream sol_file(filename.c_str(), ios::in);
    if(sol_file.is_open()){

        int total_element_number;
        sol_file >> total_element_number;

        int internal_dof = mesh->controlPointNumber();
        int shared_dof = mesh->sharedControlPointNumber();

        state->resize((internal_dof+shared_dof)*variable_number);

        int control_point_number;
        double sol_value;

        for (int ivar=0; ivar<variable_number; ivar++){

            for (int iel=0; iel<total_element_number; iel++){

                int local_elt_id = mesh->localElementId(iel);

                if(local_elt_id != -1){ // test if current element is in my partition

                    igElement *elt = mesh->element(local_elt_id);

                    sol_file >> control_point_number;
                    for (int idof=0; idof<control_point_number; idof++){
                        sol_file >> sol_value;
                        state->at(elt->globalId(idof,ivar)) = sol_value;
                    }
                }
                else{
                    sol_file >> control_point_number;
                    for (int idof=0; idof<control_point_number; idof++){
                        sol_file >> sol_value;
                    }
                }

            }
        }

    } else
        cout << "problem to read solution file" << endl;

    if(my_partition == 0){
        cout << "solution reading: ok " << endl;
        cout << endl;
    }
}


void igFileManager::writeSolutionIgloo( const string& filename, const int variable_number, igMesh *mesh, vector<double> *state)
{
    ofstream solution_file;

    solution_file.open(filename.c_str(), ios::out);
    solution_file << mesh->elementNumber()  << endl;
    solution_file << endl;

    solution_file.precision(16);

    for (int ivar=0; ivar<variable_number; ivar++){

        for (int iel=0; iel<mesh->elementNumber(); iel++){

            igElement *elt = mesh->element(iel);

            solution_file << elt->controlPointNumber() << endl;

            for (int ipt=0; ipt<elt->controlPointNumber(); ipt++){
                solution_file << state->at(elt->globalId(ipt,ivar))<< endl;
            }
        }

    }

    solution_file.close();
}

//////////////////////////////////////////////////////////////

void igFileManager::writeRestartFile(vector<double> *state, igMesh *mesh, double time)
{
    stringstream ss_partition;
    ss_partition << my_partition;

    ostringstream restart_filename;
    restart_filename << "tmp_restart_" << ss_partition.str();

    ofstream restart_file;
    restart_file.open(restart_filename.str(), ios::out);
    restart_file.precision(16);

    restart_file << time << endl;
    restart_file << save_counter << endl;
    restart_file << save_mesh_counter-1 << endl;

    for (int ivar=0; ivar<mesh->variableNumber(); ivar++){
        for (int iel=0; iel<mesh->elementNumber(); iel++){

            igElement *elt = mesh->element(iel);

            for (int ipt=0; ipt<elt->controlPointNumber(); ipt++){
                restart_file << state->at(elt->globalId(ipt,ivar))<< endl;
            }
        }

    }

    if(save_mesh){

        for (int iel=0; iel<mesh->elementNumber(); iel++){

            igElement *elt = mesh->element(iel);

            for (int ipt=0; ipt<elt->controlPointNumber(); ipt++){
                for(int idim=0; idim<mesh->meshDimension(); idim++)
                    restart_file << elt->controlPointCoordinate(ipt, idim)<< endl;
                restart_file << elt->controlPointWeight(ipt)<< endl;
                for(int idim=0; idim<mesh->meshDimension(); idim++)
                    restart_file << elt->controlPointVelocityComponent(ipt, idim)<< endl;
            }
        }
    }

    restart_file.close();
}

void igFileManager::readRestartFile(vector<double> *state, igMesh *mesh, double &time, bool flag_ALE)
{
    stringstream ss_partition;
    ss_partition << my_partition;

    ostringstream restart_filename;
    restart_filename << "tmp_restart_" << ss_partition.str();

    ifstream restart_file;
    restart_file.open(restart_filename.str(), ios::in);

    restart_file >> time;
    restart_file >> save_counter;
    restart_file >> save_mesh_counter;

    mesh->setTime(time);

    for (int ivar=0; ivar<mesh->variableNumber(); ivar++){
        for (int iel=0; iel<mesh->elementNumber(); iel++){

            igElement *elt = mesh->element(iel);

            for (int ipt=0; ipt<elt->controlPointNumber(); ipt++){
                restart_file >> state->at(elt->globalId(ipt,ivar));
            }
        }

    }

    if(flag_ALE){

        double coord;
        double weight;

        for (int iel=0; iel<mesh->elementNumber(); iel++){

            igElement *elt = mesh->element(iel);

            for (int ipt=0; ipt<elt->controlPointNumber(); ipt++){
                for(int idim=0; idim<mesh->meshDimension(); idim++){
                    restart_file >> coord;
                    elt->setControlPointCoordinate(ipt, idim, coord);
                }
                restart_file >> weight;
                elt->setControlPointWeight(ipt,weight);
                for(int idim=0; idim<mesh->meshDimension(); idim++){
                    restart_file >> coord;
                    elt->setControlPointVelocity(ipt, idim, coord);
                }
            }
        }

    }
    restart_file.close();
}

void igFileManager::applyMeshRestart(igMesh *mesh)
{

    for (int iel=0; iel<mesh->elementNumber(); iel++){
        igElement *elt = mesh->element(iel);
        elt->initializeGeometry();
    }

    for(int ifac = 0; ifac < mesh->faceNumber(); ifac++){
        igFace *face = mesh->face(ifac);
        face->initializeGeometry();
        face->updateNormal();
    }

    for (int ifac = 0; ifac < mesh->boundaryFaceNumber(); ++ifac) {
        igFace *face = mesh->boundaryFace(ifac);
        face->initializeGeometry();
        face->updateNormal();
    }

}


///////////////////////////////////////////////////////////////


void igFileManager::writeMesh(igMesh *mesh)
{
   IG_ASSERT(mesh, "no mesh");

   switch(mesh->meshDimension()){

       case 2:
       this->writeMesh2DGlvis("igloo.mesh",mesh,my_partition,false);
       save_counter++;
       break;

       case 3:
       this->writeMesh3DGlvis("igloo.mesh",mesh,my_partition,false);
       save_counter++;
       break;

    }

}


void igFileManager::writeMesh(igMesh *mesh, double time)
{
    IG_ASSERT(mesh, "no mesh");

    if(save_counter*save_period - time < 1.E-10) {

        switch(mesh->meshDimension()){

            case 2:
            this->writeMesh2DGlvis("igloo.mesh",mesh,my_partition,false);
            save_counter++;
            break;

            case 3:
            this->writeMesh3DGlvis("igloo.mesh",mesh,my_partition,false);
            save_counter++;
            break;

         }

    }
}


void igFileManager::writeMeshSolution(vector<double> *state, igMesh *mesh)
{
    IG_ASSERT(state, "no state");
    IG_ASSERT(mesh, "no mesh");

   switch(mesh->meshDimension()){

    case 1:

        this->writeSolutionField1DGnuplot(state,mesh);
        break;

    case 2:

        if(save_mesh)
            this->writeMesh2DGlvis("igloo.mesh",mesh,my_partition,false);

        for (int ivar=0; ivar<mesh->variableNumber(); ivar++)
            this->writeSolutionField2DGlviz("igloo_",state,mesh,ivar,ivar);

        break;

    case 3:

        if(save_mesh)
            this->writeMesh3DGlvis("igloo.mesh",mesh,my_partition,false);

        for (int ivar=0; ivar<mesh->variableNumber(); ivar++)
            this->writeSolutionField3DGlviz("igloo_",state,mesh,ivar,ivar);

        break;

    }

   save_counter++;

}

void igFileManager::writeMeshSolution(vector<double> *state, vector<double> *artificial_viscosity, vector<double> *vorticity, igMesh *mesh, double time)
{
    IG_ASSERT(mesh, "no mesh");
    IG_ASSERT(state, "no state");

    int iprop;

    if(save_counter*save_period - time < 1.E-10) {

        switch(mesh->meshDimension()){

        case 1:

            this->writeSolutionField1DGnuplot(state,mesh);
            break;

        case 2:

            if(save_mesh)
                this->writeMesh2DGlvis("igloo.mesh",mesh,my_partition,false);

            iprop = mesh->variableNumber();
            if(save_artificial_viscosity){
                this->writeSolutionField2DGlviz("igloo_",artificial_viscosity,mesh,0,iprop);
                iprop++;
            }
            if(save_vorticity)
                this->writeSolutionField2DGlviz("igloo_",vorticity,mesh,0,iprop);

            for (int ivar=0; ivar<mesh->variableNumber(); ivar++)
                this->writeSolutionField2DGlviz("igloo_",state,mesh,ivar,ivar);

            break;

        case 3:

            if(save_mesh)
                this->writeMesh3DGlvis("igloo.mesh",mesh,my_partition,false);

            for (int ivar=0; ivar<mesh->variableNumber(); ivar++)
                this->writeSolutionField3DGlviz("igloo_",state,mesh,ivar,ivar);

            break;
        }

        this->writeRestartFile(state,mesh,time);
        //this->writeWallData(state,mesh);

        save_counter++;

    }
}


void igFileManager::writeSolutionField1DGnuplot(vector<double> *state, igMesh *mesh)
{
    IG_ASSERT(mesh, "no mesh");
    IG_ASSERT(state, "no state");

    for(int ivar = 0; ivar<mesh->variableNumber(); ivar++){

        stringstream ss;
        ss << ivar;
        string filename = "sol"+ss.str()+".dat";
        ofstream output_file(filename.c_str(), ios::out);
        output_file.is_open();

        for (int iel=0; iel<mesh->elementNumber(); iel++){

            igElement *elt = mesh->element(iel);

            int degree = elt->cellDegree();
            int dof_number = elt->controlPointNumber();
            int sample_number = 2*dof_number;

            igBasisBezier basis(degree);

            vector<double> values;
            values.assign(dof_number,0.);

            for (int k=0; k<sample_number; k++){

                double coord = double(k)/double(sample_number-1);
                basis.evalFunction(coord,values);
                double position_value = elt->controlPointCoordinate(0,0) + 2*coord*elt->radius();
                if(degree == 0)
                    position_value = elt->controlPointCoordinate(0,0) +
                        coord*(mesh->element(mesh->elementNumber()-1)->controlPointCoordinate(0,0)
                               - mesh->element(0)->controlPointCoordinate(0,0) )/mesh->elementNumber();

                double solution_value = 0.;
                for (int idof=0; idof<dof_number; idof++){
                    solution_value += values.at(idof) * state->at(elt->globalId(idof,ivar));
                }
                output_file << setprecision(15) << position_value << " " << solution_value << endl;

            }
            output_file << endl;
        }

        output_file.close();
    }
}

void igFileManager::writeSolutionField2DGlviz(const string& filename, vector<double> *state, igMesh *mesh, int var_id, int label)
{
    IG_ASSERT(mesh, "no mesh");
    IG_ASSERT(state, "no state");

    int degree = mesh->element(0)->cellDegree();
    int size = (degree+1)*(degree+1);

    stringstream ss_id;
    ss_id << label;

    stringstream ss_counter;
    ss_counter << save_counter;

    stringstream ss_partition;
    ss_partition << my_partition;

    ostringstream sol_name;
    sol_name << filename + ss_id.str() + "_" + ss_counter.str() + ".sol." << setfill('0') << setw(6) << ss_partition.str();

    ofstream sol_file(sol_name.str().c_str(), ios::out);
    if(sol_file.is_open()){

        stringstream ss;
        ss << degree;
        string elt_coll = "NURBS"+ss.str();

        sol_file << "FiniteElementSpace" << endl;
        sol_file << "FiniteElementCollection: " << elt_coll << endl;
        sol_file << "VDim: 1" << endl;
        sol_file << "Ordering: 1" << endl;
        sol_file << endl;

        //
        // first loop de define corner dof coordinates
        //

        for (int iel=0; iel<mesh->elementNumber(); iel++){

            igElement *elt = mesh->element(iel);
            if(elt->isActive()){

                sol_file << state->at(elt->globalId(0,var_id)) << endl;
                sol_file << state->at(elt->globalId(degree,var_id)) << endl;
                sol_file << state->at(elt->globalId(size-1,var_id)) << endl;
                sol_file << state->at(elt->globalId(size-1-degree,var_id)) << endl;

            }
        }

        //
        // second loop de define mid-edge dof coordinates
        //

        for (int iel=0; iel<mesh->elementNumber(); iel++){

            igElement *elt = mesh->element(iel);

            if(elt->isActive()){

                for (int ipt=0; ipt<degree-1; ipt++){
                    int offset = 1+ipt;
                    sol_file << state->at(elt->globalId(offset,var_id)) << endl;
                }
                for (int ipt=0; ipt<degree-1; ipt++){
                    int offset = (ipt+2)*(degree+1) -1;
                    sol_file << state->at(elt->globalId(offset,var_id)) << endl;
                }
                for (int ipt=0; ipt<degree-1; ipt++){
                    int offset = size-ipt-2;
                    sol_file << state->at(elt->globalId(offset,var_id)) << endl;
                }
                for (int ipt=0; ipt<degree-1; ipt++){
                    int offset = (ipt+1)*(degree+1);
                    sol_file << state->at(elt->globalId(offset,var_id)) << endl;
                }
            }

        }

        //
        // third loop de define center dof coordinates
        //

        for (int iel=0; iel<mesh->elementNumber(); iel++){

            igElement *elt = mesh->element(iel);

            if(elt->isActive()){

                for (int ipt2=0; ipt2<degree-1; ipt2++){
                    for (int ipt1=0; ipt1<degree-1; ipt1++){

                        int offset = (ipt2+1)*(degree+1) + ipt1 + 1;
                        sol_file << state->at(elt->globalId(offset,var_id)) << endl;
                    }
                }
            }

        }

    }
}

void igFileManager::writeSolutionField3DGlviz(const string& filename, vector<double> *state, igMesh *mesh, int var_id, int label)
{
    IG_ASSERT(mesh, "no mesh");
    IG_ASSERT(state, "no state");

    int degree = mesh->element(0)->cellDegree();
    int size = (degree+1)*(degree+1)*(degree+1);
    int offset;

    stringstream ss_id;
    ss_id << label;

    stringstream ss_counter;
    ss_counter << save_counter;

    stringstream ss_partition;
    ss_partition << my_partition;

    ostringstream sol_name;
    sol_name << filename + ss_id.str() + "_" + ss_counter.str() + ".sol." << setfill('0') << setw(6) << ss_partition.str();

    ofstream sol_file(sol_name.str().c_str(), ios::out);
    if(sol_file.is_open()){

        stringstream ss;
        ss << degree;
        string elt_coll = "NURBS"+ss.str();

        sol_file << "FiniteElementSpace" << endl;
        sol_file << "FiniteElementCollection: " << elt_coll << endl;
        sol_file << "VDim: 1" << endl;
        sol_file << "Ordering: 1" << endl;
        sol_file << endl;

        //
        // first loop de define corner dof coordinates
        //

        for (int iel=0; iel<mesh->elementNumber(); iel++){

            igElement *elt = mesh->element(iel);
            if(elt->isActive()){

                sol_file << state->at(elt->globalId(0,var_id)) << endl;
                sol_file << state->at(elt->globalId(degree,var_id)) << endl;
                sol_file << state->at(elt->globalId((degree+1)*degree + degree,var_id)) << endl;
                sol_file << state->at(elt->globalId((degree+1)*degree,var_id)) << endl;
                sol_file << state->at(elt->globalId((degree+1)*(degree+1)*degree,var_id)) << endl;
                sol_file << state->at(elt->globalId((degree+1)*(degree+1)*degree + degree,var_id)) << endl;
                sol_file << state->at(elt->globalId((degree+1)*(degree+1)*degree + (degree+1)*degree + degree,var_id)) << endl;
                sol_file << state->at(elt->globalId((degree+1)*(degree+1)*degree + (degree+1)*degree,var_id)) << endl;
            }
        }

        //
        // second loop de define mid-edge dof coordinates
        //

        for (int iel=0; iel<mesh->elementNumber(); iel++){

            igElement *elt = mesh->element(iel);

            if(elt->isActive()){

                // bottom edges
                for (int ipt=1; ipt<degree; ipt++)
                    sol_file << state->at(elt->globalId(ipt,var_id)) << endl;

                for (int ipt=1; ipt<degree; ipt++)
                    sol_file << state->at(elt->globalId(degree+ipt*(degree+1),var_id)) << endl;

                for (int ipt=1; ipt<degree; ipt++)
                    sol_file << state->at(elt->globalId((degree+1)*(degree+1)-ipt-1,var_id)) << endl;

                for (int ipt=1; ipt<degree; ipt++)
                    sol_file << state->at(elt->globalId(ipt*(degree+1),var_id)) << endl;

                // top edges
                offset = (degree+1)*(degree+1)*degree;
                for (int ipt=1; ipt<degree; ipt++)
                    sol_file << state->at(elt->globalId(offset+ipt,var_id)) << endl;

                for (int ipt=1; ipt<degree; ipt++)
                    sol_file << state->at(elt->globalId(offset+degree+ipt*(degree+1),var_id)) << endl;

                for (int ipt=1; ipt<degree; ipt++)
                    sol_file << state->at(elt->globalId(offset+(degree+1)*(degree+1)-ipt-1,var_id)) << endl;

                for (int ipt=1; ipt<degree; ipt++)
                    sol_file << state->at(elt->globalId(offset+ipt*(degree+1),var_id)) << endl;

                // side edges
                for (int ipt=1; ipt<degree; ipt++)
                    sol_file << state->at(elt->globalId(ipt*(degree+1)*(degree+1),var_id)) << endl;

                for (int ipt=1; ipt<degree; ipt++)
                    sol_file << state->at(elt->globalId(degree + ipt*(degree+1)*(degree+1),var_id)) << endl;

                for (int ipt=1; ipt<degree; ipt++)
                    sol_file << state->at(elt->globalId(degree*(degree+1)+degree + ipt*(degree+1)*(degree+1),var_id)) << endl;

                for (int ipt=1; ipt<degree; ipt++)
                    sol_file << state->at(elt->globalId(degree*(degree+1) + ipt*(degree+1)*(degree+1),var_id)) << endl;

            }
        }

        //
        // second loop de define mid-edge dof coordinates
        //

        for (int iel=0; iel<mesh->elementNumber(); iel++){

            igElement *elt = mesh->element(iel);

            if(elt->isActive()){

                // bottom face
                for (int ipt2=1; ipt2<degree; ipt2++){
                    for (int ipt1=1; ipt1<degree; ipt1++){
                        sol_file << state->at(elt->globalId((degree+1)*degree + ipt1 - (degree+1)*ipt2,var_id)) << endl;
                    }
                }

                // south face
                for (int ipt3=1; ipt3<degree; ipt3++){
                    for (int ipt1=1; ipt1<degree; ipt1++){
                        sol_file << state->at(elt->globalId(ipt1 + (degree+1)*(degree+1)*ipt3,var_id)) << endl;
                    }
                }

                //  east face
                for (int ipt3=1; ipt3<degree; ipt3++){
                    for (int ipt2=1; ipt2<degree; ipt2++){
                        sol_file << state->at(elt->globalId(degree + ipt2*(degree+1) +(degree+1)*(degree+1)*ipt3,var_id)) << endl;
                    }
                }

                //  north face
                for (int ipt3=1; ipt3<degree; ipt3++){
                    for (int ipt1=1; ipt1<degree; ipt1++){
                        sol_file << state->at(elt->globalId((degree+1)*(degree+1) -1 - ipt1 +(degree+1)*(degree+1)*ipt3,var_id)) << endl;
                    }
                }

                // west face
                for (int ipt3=1; ipt3<degree; ipt3++){
                    for (int ipt2=1; ipt2<degree; ipt2++){
                        sol_file << state->at(elt->globalId(degree*(degree+1) -ipt2*(degree+1) + (degree+1)*(degree+1)*ipt3,var_id)) << endl;
                    }
                }

                // top face
                for (int ipt2=1; ipt2<degree; ipt2++){
                    for (int ipt1=1; ipt1<degree; ipt1++){
                        sol_file << state->at(elt->globalId((degree+1)*(degree+1)*degree + ipt1+(degree+1)*ipt2,var_id)) << endl;
                    }
                }

            }
        }


        //
        // third loop de define center dof coordinates
        //

        for (int iel=0; iel<mesh->elementNumber(); iel++){

            igElement *elt = mesh->element(iel);

            if(elt->isActive()){

                for (int ipt3=1; ipt3<degree; ipt3++){
                    for (int ipt2=1; ipt2<degree; ipt2++){
                        for (int ipt1=1; ipt1<degree; ipt1++){

                            sol_file << state->at(elt->globalId(ipt1 + (degree+1)*ipt2 + (degree+1)*(degree+1)*ipt3,var_id)) << endl;
                        }
                    }
                }

            }

        }

    }
}


///////////////////////////////////////////////////////////////

void igFileManager::readSolverParameters(vector<double> *solver_parameters)
{
    IG_ASSERT(solver_parameters, "no solver_parameters");

    ifstream input_file("solver_parameters.dat", ios::in);
    if(input_file.is_open()){

        int parameters_number;

        input_file >> parameters_number;
        for (int i=0; i<parameters_number; i++)
            input_file >> solver_parameters->at(i);

        input_file.close();

    } else {
        cout << "Problem to read solver_parameter.dat" << endl;
    }
}

/////////////////////////////////////////////////////////////////

void igFileManager::readNurbsCurve(vector<double> *knot_vector, vector<double> *pt_x, vector<double> *pt_y)
{
    ifstream nurbs_file("nurbs.dat", ios::in);

    int degree;
    int ctrl_pt_number;

    nurbs_file >> degree;
    nurbs_file >> ctrl_pt_number;

    int knot_number = ctrl_pt_number + degree + 1;

    pt_x->resize(ctrl_pt_number);
    pt_y->resize(ctrl_pt_number);
    knot_vector->resize(knot_number);

    for(int i=0; i<knot_number; i++){
        nurbs_file >> knot_vector->at(i);
    }

    for(int i=0; i<ctrl_pt_number; i++){
        nurbs_file >> pt_x->at(i);
        nurbs_file >> pt_y->at(i);
    }
	nurbs_file.close();

}


///////////////////////////////////////////////////////////////

void igFileManager::writeWallData(vector<double> *state, igMesh *mesh)
{
    IG_ASSERT(mesh, "no mesh");
    IG_ASSERT(state, "no state");

    vector<double> *state_left = new vector<double>;
    state_left->resize(mesh->variableNumber());
    vector<double> *derivative_left = new vector<double>;
    derivative_left->resize(mesh->variableNumber());

    ostringstream filename;
    filename << "wall_" << save_counter << "_" << setfill('0') << setw(6) << my_partition << ".dat";

    ofstream output_file(filename.str().c_str(), ios::out);
    output_file.precision(16);

    for (int ifac=0; ifac<mesh->boundaryFaceNumber(); ifac++){

        igFace *face = mesh->boundaryFace(ifac);
        int eval_number = face->gaussPointNumber();
        int face_type = face->type();

        if(face_type == id_slip_wall || face_type == id_noslip_wall){

            // loop over quadrature points
            for (int k=0; k<eval_number; k++){

                double *gauss_point_position = face->gaussPointPhysicalCoordinate(k);

                // solution at point k
                for (int ivar=0; ivar<mesh->variableNumber(); ivar++){

                    int function_number = face->controlPointNumber();

                    // loop over basis functions
                    double value_left = 0.;
                    for (int i=0; i<function_number; i++)
                        value_left += face->gaussPointFunctionValue(k,i) * state->at(face->dofLeft(i,ivar));

                    state_left->at(ivar) = value_left;
                }

                double rho = state_left->at(0);
                double u = state_left->at(1)/rho;
                double v = state_left->at(2)/rho;
                double energy = state_left->at(3);
                double p = (0.4)*(energy - 0.5*rho*(u*u+v*v));

                output_file << gauss_point_position[0] << "\t";
                output_file << gauss_point_position[1] << "\t";
                output_file << rho << "\t";
                output_file << u << "\t";
                output_file << v << "\t";
                output_file << p << "\t";

                output_file << endl;
            }

        }

    }

    output_file.close();
}

/////////////////////////////////////////////////////////////////////////////////////////////

void igFileManager::writeMeshIgloo( const string& filename, igMesh *mesh, igDistributor *distributor)
{
    IG_ASSERT(mesh, "no mesh");
    IG_ASSERT(distributor, "no distributor");

    ofstream mesh_file(filename.c_str(), ios::out);
    if(mesh_file.is_open()){

        int dimension = mesh->meshDimension();

        // general info
        mesh_file << dimension << endl;
        mesh_file << mesh->elementNumber() << endl;
        mesh_file << mesh->variableNumber() << endl;
        mesh_file << mesh->derivativeNumber() << endl;
        mesh_file << mesh->exactSolutionId() << endl;
        mesh_file << endl;

        // distribution info
        mesh_file << mesh->partitionNumber() << endl;
        for(int ipart =0; ipart<mesh->partitionNumber(); ipart++){
            mesh_file << distributor->dofNumber(ipart) << endl;
            mesh_file << distributor->neighbourNumber(ipart) << endl;
            for (int ineigh=0; ineigh<distributor->neighbourNumber(ipart); ineigh++){
                mesh_file << distributor->neighbourId(ipart, ineigh) << endl;
                mesh_file << distributor->neighbourFaceNumber(ipart, ineigh) << endl;
                mesh_file << distributor->neighbourDofNumber(ipart, ineigh) << endl;
            }
        }
        mesh_file << endl;

        // list of elements
        for (int iel=0; iel<mesh->elementNumber(); iel++){

            igElement *elt = mesh->element(iel);

            mesh_file << elt->cellDegree() << " " << elt->gaussPointNumberByDirection() << endl;
            mesh_file << elt->partition() << endl;
            mesh_file << elt->subdomain() << endl;
            mesh_file << elt->isActive() << endl;

            for (int ipt=0; ipt<elt->controlPointNumber(); ipt++){

                for (int idim=0; idim<dimension; idim++)
                    mesh_file << setprecision(15) << scientific << elt->controlPointCoordinate(ipt,idim) << "\t"  ;
                mesh_file << setprecision(15) << scientific << elt->controlPointWeight(ipt) << endl;

            }
            mesh_file << endl;
        }
        mesh_file << endl;

        // list of faces
        mesh_file << mesh->faceNumber() << endl;
        mesh_file << endl;

        for(int ifac=0; ifac<mesh->faceNumber(); ifac++){

            igFace *face = mesh->face(ifac);

            mesh_file << face->cellDegree() << " " << face->gaussPointNumberByDirection() << " " << face->type() << endl;
            mesh_file << face->leftPartitionId() << endl;
            mesh_file << face->rightPartitionId() << endl;
            mesh_file << face->isActive() << endl;
            mesh_file << setprecision(15) << scientific << face->parameterBegin(0,true) << " \t" << face->parameterEnd(0,true) << " \t" << face->parameterBegin(0,false) << " \t" << face->parameterEnd(0,false)  << endl;
            mesh_file << setprecision(15) << scientific << face->parameterBegin(1,true) << " \t" << face->parameterEnd(1,true) << " \t" << face->parameterBegin(1,false) << " \t" << face->parameterEnd(1,false)  << endl;

            mesh_file << face->leftElementIndex() << endl;
            mesh_file << face->leftOrientation() << endl;
            mesh_file << face->leftSense() << endl;
            for (int idof=0; idof<face->controlPointNumber(); idof++){
                // search for local left id
                int index;
                for(int idofleft=0; idofleft<mesh->element(face->leftElementIndex())->controlPointNumber(); idofleft++){
                    if(mesh->element(face->leftElementIndex())->globalId(idofleft,0) == face->dofLeft(idof,0))
                        index = idofleft;
                }
                mesh_file << index << " ";
            }
            mesh_file << endl;

            mesh_file << face->rightElementIndex() << endl;
            mesh_file << face->rightOrientation() << endl;
            mesh_file << face->rightSense() << endl;
            for (int idof=0; idof<face->controlPointNumber(); idof++){
                // search for local right id
                int index;
                for(int idofright=0; idofright<mesh->element(face->rightElementIndex())->controlPointNumber(); idofright++){
                    if(mesh->element(face->rightElementIndex())->globalId(idofright,0) == face->dofRight(idof,0))
                        index = idofright;
                }
                mesh_file << index << " ";
            }
            mesh_file << endl;

            for (int idim=0; idim<dimension; idim++)
                mesh_file << setprecision(15) << scientific << mesh->element(face->leftElementIndex())->barycenter(idim) << "\t";
            mesh_file << endl;
            mesh_file << endl;
        }
        mesh_file << endl;

        // list of boundary faces
        mesh_file << mesh->boundaryFaceNumber() << endl;
        mesh_file << endl;

        for(int ifac=0; ifac<mesh->boundaryFaceNumber(); ifac++){

            igFace *face = mesh->boundaryFace(ifac);

            mesh_file << face->cellDegree() << " " << face->gaussPointNumberByDirection() << " " << face->type() << endl;
            mesh_file << face->leftPartitionId() << endl;
            mesh_file << face->isActive() << endl;
            mesh_file << setprecision(15) << scientific << face->parameterBegin(0,true) << " \t" << face->parameterEnd(0,true) << endl;
            mesh_file << setprecision(15) << scientific << face->parameterBegin(1,true) << " \t" << face->parameterEnd(1,true)  << endl;


            mesh_file << face->leftElementIndex() << endl;
            mesh_file << face->leftOrientation() << endl;
            mesh_file << face->leftSense() << endl;
            for (int idof=0; idof<face->controlPointNumber(); idof++){
                // search for local left id
                int index;
                for(int idofleft=0; idofleft<mesh->element(face->leftElementIndex())->controlPointNumber(); idofleft++){
                    if(mesh->element(face->leftElementIndex())->globalId(idofleft,0) == face->dofLeft(idof,0))
                        index = idofleft;
                }
                mesh_file << index << " ";
            }
            mesh_file << endl;

            for (int idim=0; idim<dimension; idim++)
                mesh_file << setprecision(15) << scientific << mesh->element(face->leftElementIndex())->barycenter(idim) << "\t";
            mesh_file << endl;
            mesh_file << endl;
        }
        mesh_file << endl;


        mesh_file.close();
    } else {
        cout << "problem to open mesh file" << endl;
    }

}


void igFileManager::writeMesh2DGlvis(const string& filename, igMesh *mesh, int part_id, bool sequential_run)
{
    IG_ASSERT(mesh, "no mesh");

    int degree = mesh->element(0)->cellDegree();
    int pts_per_elt = (degree+1)*(degree+1);

    stringstream ss_counter_mesh;

    if(sequential_run)
        save_mesh_counter = 0;
    else
        save_mesh_counter++;
    ss_counter_mesh << save_mesh_counter;

    ostringstream mesh_name;
    mesh_name << filename + ss_counter_mesh.str() + "." << setfill('0') << setw(6) << part_id;

    ofstream output_file(mesh_name.str().c_str(), ios::out);
    if(output_file.is_open()){

        output_file << "MFEM NURBS mesh v1.0" << endl;
        output_file << endl;

        output_file << "dimension" << endl;
        output_file << 2 << endl;
        output_file << endl;

        int total_element_number = mesh->totalActiveElementNumber();


        int local_elt_number;
        if(sequential_run){
            local_elt_number = 0;
            for (int iel=0; iel<mesh->elementNumber(); iel++){
                igElement *elt = mesh->element(iel);
                if(elt->partition() == part_id)
                    local_elt_number++;
            }
        } else {
            local_elt_number = mesh->activeElementNumber();
        }

        output_file << "elements" << endl;
        output_file << total_element_number << endl;

        int iel_counter = 0;

        for (int iel=0; iel<total_element_number; iel++){

            output_file << 1 << " ";
            output_file << 3 << " ";
            output_file << iel_counter*4 + 0 << " ";
            output_file << iel_counter*4 + 1 << " ";
            output_file << iel_counter*4 + 2 << " ";
            output_file << iel_counter*4 + 3 << " ";
            output_file << endl;

            iel_counter++;
        }
        output_file << endl;


        output_file << "boundary" << endl;
        output_file << 0 << endl;
        output_file << endl;

        output_file << "edges" << endl;
        output_file << 4*total_element_number << endl;

        iel_counter = 0;
        for (int iel=0; iel<total_element_number; iel++){

            output_file << 0 << " ";
            output_file << iel_counter*4 << " ";
            output_file << iel_counter*4 + 1 << " ";
            output_file << endl;
            output_file << 0 << " ";
            output_file << iel_counter*4 + 1 << " ";
            output_file << iel_counter*4 + 2 << " ";
            output_file << endl;
            output_file << 0 << " ";
            output_file << iel_counter*4 + 3 << " ";
            output_file << iel_counter*4 + 2 << " ";
            output_file << endl;
            output_file << 0 << " ";
            output_file << iel_counter*4 << " ";
            output_file << iel_counter*4 + 3 << " ";
            output_file << endl;

            iel_counter++;
        }
        output_file << endl;

        output_file << "vertices" << endl;
        output_file << total_element_number*(degree+1)*(degree+1) << endl;
        output_file << endl;

        output_file << "knotvectors" << endl;
        output_file << 1 << endl;
        output_file << degree << " ";
        output_file << degree+1 << " ";
        for (int i=0; i<degree+1; i++){
            output_file << 0 << " ";
        }
        for (int i=0; i<degree+1; i++){
            output_file << 1 << " ";
        }
        output_file << endl;
        output_file << endl;

        // specific to distributed data

        if(sequential_run || partition_number >1){

            output_file << "mesh_elements" << endl;
            output_file << local_elt_number << endl;
            for (int iel=0; iel<mesh->elementNumber(); iel++){
                if(mesh->element(iel)->partition() == part_id && mesh->element(iel)->isActive())
                    output_file << mesh->element(iel)->cellGlobalId() << endl;
            }
            output_file << endl;
        }
        // end of specific section

        output_file << "weights" << endl;

        for (int iel=0; iel<mesh->elementNumber(); iel++){
            igElement *elt = mesh->element(iel);
            if(elt->isActive()){
                if(elt->partition() == part_id){
                    int degree = elt->cellDegree();
                    int ctrl_pts_number = elt->controlPointNumber();
                    output_file << elt->controlPointWeight(0) << endl;
                    output_file << elt->controlPointWeight(degree) << endl;
                    output_file << elt->controlPointWeight(ctrl_pts_number-1) << endl;
                    output_file << elt->controlPointWeight(ctrl_pts_number-degree-1) << endl;
                }
            }
        }

        for (int iel=0; iel<mesh->elementNumber(); iel++){
            igElement *elt = mesh->element(iel);
            if(elt->isActive()){
                if(elt->partition() == part_id){
                    int degree = elt->cellDegree();
                    int ctrl_pts_number = elt->controlPointNumber();

                    for (int idof=1; idof<degree; idof++)
                        output_file << elt->controlPointWeight(idof) << endl;

                    for (int idof=1; idof<degree; idof++)
                        output_file << elt->controlPointWeight(degree+idof*(degree+1)) << endl;

                    for (int idof=1; idof<degree; idof++)
                        output_file << elt->controlPointWeight(ctrl_pts_number-1-idof) << endl;

                    for (int idof=1; idof<degree; idof++)
                        output_file << elt->controlPointWeight(idof*(degree+1)) << endl;
                }
            }
        }

        for (int iel=0; iel<mesh->elementNumber(); iel++){
            igElement *elt = mesh->element(iel);
            if(elt->isActive()){
                if(elt->partition() == part_id){
                    int degree = elt->cellDegree();
                    int ctrl_pts_number = elt->controlPointNumber();

                    for (int jdof=1; jdof<degree; jdof++)
                        for (int idof=1; idof<degree; idof++)
                            output_file << elt->controlPointWeight(jdof*(degree+1)+idof) << endl;
                }
            }
        }
        output_file << endl;

        stringstream ss;
        ss << degree;
        string elt_coll = "NURBS"+ss.str();

        output_file << "FiniteElementSpace" << endl;
        output_file << "FiniteElementCollection: " << elt_coll << endl;
        output_file << "VDim: 2" << endl;
        output_file << "Ordering: 1" << endl;
        output_file << endl;


        for (int iel=0; iel<mesh->elementNumber(); iel++){
            igElement *elt = mesh->element(iel);
            if(elt->isActive()){
                if(elt->partition() == part_id){
                    int degree = elt->cellDegree();
                    int ctrl_pts_number = elt->controlPointNumber();
                    output_file << elt->controlPointCoordinate(0,0) << " "<< elt->controlPointCoordinate(0,1) << endl;
                    output_file << elt->controlPointCoordinate(degree,0) << " "<< elt->controlPointCoordinate(degree,1) << endl;
                    output_file << elt->controlPointCoordinate(ctrl_pts_number-1,0) << " "<< elt->controlPointCoordinate(ctrl_pts_number-1,1) << endl;
                    output_file << elt->controlPointCoordinate(ctrl_pts_number-degree-1,0) << " "<< elt->controlPointCoordinate(ctrl_pts_number-degree-1,1) << endl;
                }
            }
        }

        for (int iel=0; iel<mesh->elementNumber(); iel++){
            igElement *elt = mesh->element(iel);
            if(elt->isActive()){
                if(elt->partition() == part_id){
                    int degree = elt->cellDegree();
                    int ctrl_pts_number = elt->controlPointNumber();

                    for (int idof=1; idof<degree; idof++)
                        output_file << elt->controlPointCoordinate(idof,0) << " "<< elt->controlPointCoordinate(idof,1) << endl;

                    for (int idof=1; idof<degree; idof++)
                        output_file << elt->controlPointCoordinate(degree+idof*(degree+1),0) << " "<< elt->controlPointCoordinate(degree+idof*(degree+1),1) << endl;

                    for (int idof=1; idof<degree; idof++)
                        output_file << elt->controlPointCoordinate(ctrl_pts_number-1-idof,0) << " "<< elt->controlPointCoordinate(ctrl_pts_number-1-idof,1) << endl;

                    for (int idof=1; idof<degree; idof++)
                        output_file << elt->controlPointCoordinate(idof*(degree+1),0) << " "<< elt->controlPointCoordinate(idof*(degree+1),1) << endl;
                }
            }
        }

        for (int iel=0; iel<mesh->elementNumber(); iel++){
            igElement *elt = mesh->element(iel);
            if(elt->isActive()){
                if(elt->partition() == part_id){
                    int degree = elt->cellDegree();
                    int ctrl_pts_number = elt->controlPointNumber();

                    for (int jdof=1; jdof<degree; jdof++)
                        for (int idof=1; idof<degree; idof++)
                            output_file << elt->controlPointCoordinate(jdof*(degree+1)+idof,0) << " "<< elt->controlPointCoordinate(jdof*(degree+1)+idof,1) << endl;
                }
            }
        }

        output_file.close();
    }
}



void igFileManager::writeMesh3DGlvis(const string& filename, igMesh *mesh, int part_id, bool sequential_run)
{
    IG_ASSERT(mesh, "no mesh");

    int degree = mesh->element(0)->cellDegree();
    int pts_per_elt = (degree+1)*(degree+1)*(degree+1);
    int offset, index;

    stringstream ss_counter_mesh;

    if(sequential_run)
        save_mesh_counter = 0;
    else
        save_mesh_counter++;
    ss_counter_mesh << save_mesh_counter;

    ostringstream mesh_name;
    mesh_name << filename + ss_counter_mesh.str() + "." << setfill('0') << setw(6) << part_id;

    ofstream output_file(mesh_name.str().c_str(), ios::out);
    if(output_file.is_open()){

        output_file << "MFEM NURBS mesh v1.0" << endl;
        output_file << endl;

        output_file << "dimension" << endl;
        output_file << 3 << endl;
        output_file << endl;

        int total_element_number = mesh->totalActiveElementNumber();

        int local_elt_number;
        if(sequential_run){
            local_elt_number = 0;
            for (int iel=0; iel<mesh->elementNumber(); iel++){
                igElement *elt = mesh->element(iel);
                if(elt->partition() == part_id)
                    local_elt_number++;
            }
        } else {
            local_elt_number = mesh->activeElementNumber();
        }

        output_file << "elements" << endl;
        output_file << total_element_number << endl;

        int iel_counter = 0;

        for (int iel=0; iel<total_element_number; iel++){
            output_file << 1 << " ";
            output_file << 5 << " ";
            output_file << 8*iel_counter + 0 << " ";
            output_file << 8*iel_counter + 1 << " ";
            output_file << 8*iel_counter + 2 << " ";
            output_file << 8*iel_counter + 3 << " ";
            output_file << 8*iel_counter + 4 << " ";
            output_file << 8*iel_counter + 5 << " ";
            output_file << 8*iel_counter + 6 << " ";
            output_file << 8*iel_counter + 7 << " ";
            output_file << endl;

            iel_counter++;
        }
        output_file << endl;


        output_file << "boundary" << endl;
        output_file << 0 << endl;
        output_file << endl;

        output_file << "edges" << endl;
        output_file << 12*total_element_number << endl;

        iel_counter = 0;
        for (int iel=0; iel<total_element_number; iel++){
            output_file << 0 << " ";
            output_file << 8*iel_counter + 0 << " ";
            output_file << 8*iel_counter + 1 << " ";
            output_file << endl;
            output_file << 0 << " ";
            output_file << 8*iel_counter + 1 << " ";
            output_file << 8*iel_counter + 2 << " ";
            output_file << endl;
            output_file << 0 << " ";
            output_file << 8*iel_counter + 3 << " ";
            output_file << 8*iel_counter + 2 << " ";
            output_file << endl;
            output_file << 0 << " ";
            output_file << 8*iel_counter + 0 << " ";
            output_file << 8*iel_counter + 3 << " ";
            output_file << endl;
            output_file << 0 << " ";
            output_file << 8*iel_counter + 4 << " ";
            output_file << 8*iel_counter + 5 << " ";
            output_file << endl;
            output_file << 0 << " ";
            output_file << 8*iel_counter + 5 << " ";
            output_file << 8*iel_counter + 6 << " ";
            output_file << endl;
            output_file << 0 << " ";
            output_file << 8*iel_counter + 7 << " ";
            output_file << 8*iel_counter + 6 << " ";
            output_file << endl;
            output_file << 0 << " ";
            output_file << 8*iel_counter + 4 << " ";
            output_file << 8*iel_counter + 7 << " ";
            output_file << endl;
            output_file << 0 << " ";
            output_file << 8*iel_counter + 0 << " ";
            output_file << 8*iel_counter + 4 << " ";
            output_file << endl;
            output_file << 0 << " ";
            output_file << 8*iel_counter + 1 << " ";
            output_file << 8*iel_counter + 5 << " ";
            output_file << endl;
            output_file << 0 << " ";
            output_file << 8*iel_counter + 2 << " ";
            output_file << 8*iel_counter + 6 << " ";
            output_file << endl;
            output_file << 0 << " ";
            output_file << 8*iel_counter + 3 << " ";
            output_file << 8*iel_counter + 7 << " ";
            iel_counter++;
            output_file << endl;
        }
        output_file << endl;

        output_file << "vertices" << endl;
        output_file << total_element_number*(degree+1)*(degree+1)*(degree+1) << endl;
        output_file << endl;

        output_file << "knotvectors" << endl;
        output_file << 1 << endl;
        output_file << degree << " ";
        output_file << degree+1 << " ";
        for (int i=0; i<degree+1; i++){
            output_file << 0 << " ";
        }
        for (int i=0; i<degree+1; i++){
            output_file << 1 << " ";
        }
        output_file << endl;
        output_file << endl;

        // specific to distributed data

        if(sequential_run || partition_number >1){

            output_file << "mesh_elements" << endl;
            output_file << local_elt_number << endl;
            for (int iel=0; iel<mesh->elementNumber(); iel++){
                if(mesh->element(iel)->partition() == part_id && mesh->element(iel)->isActive())
                    output_file << mesh->element(iel)->cellGlobalId() << endl;
            }
            output_file << endl;
        }
        // end of specific section

        output_file << "weights" << endl;

        // first loop for corners
        for (int iel=0; iel<mesh->elementNumber(); iel++){
            igElement *elt = mesh->element(iel);
            if(elt->isActive()){
                if(elt->partition() == part_id){
                    int degree = elt->cellDegree();
                    output_file << elt->controlPointWeight(0) << endl;
                    output_file << elt->controlPointWeight(degree) << endl;
                    output_file << elt->controlPointWeight((degree+1)*degree + degree) << endl;
                    output_file << elt->controlPointWeight((degree+1)*degree) << endl;
                    output_file << elt->controlPointWeight((degree+1)*(degree+1)*degree) << endl;
                    output_file << elt->controlPointWeight((degree+1)*(degree+1)*degree + degree) << endl;
                    output_file << elt->controlPointWeight((degree+1)*(degree+1)*degree + (degree+1)*degree + degree) << endl;
                    output_file << elt->controlPointWeight((degree+1)*(degree+1)*degree + (degree+1)*degree) << endl;
                }
            }
        }

        // second loop for edges
        for (int iel=0; iel<mesh->elementNumber(); iel++){
            igElement *elt = mesh->element(iel);
            if(elt->isActive()){
                if(elt->partition() == part_id){
                    int degree = elt->cellDegree();

                    // bottom edges
                    for (int ipt=1; ipt<degree; ipt++)
                        output_file << elt->controlPointWeight(ipt) << endl;

                    for (int ipt=1; ipt<degree; ipt++)
                        output_file << elt->controlPointWeight(degree+ipt*(degree+1)) << endl;

                    for (int ipt=1; ipt<degree; ipt++)
                        output_file << elt->controlPointWeight((degree+1)*(degree+1)-ipt-1) << endl;

                    for (int ipt=1; ipt<degree; ipt++)
                        output_file << elt->controlPointWeight(ipt*(degree+1)) << endl;

                    // top edges
                    offset = (degree+1)*(degree+1)*degree;
                    for (int ipt=1; ipt<degree; ipt++)
                        output_file << elt->controlPointWeight(offset+ipt) << endl;

                    for (int ipt=1; ipt<degree; ipt++)
                        output_file << elt->controlPointWeight(offset+degree+ipt*(degree+1)) << endl;

                    for (int ipt=1; ipt<degree; ipt++)
                        output_file << elt->controlPointWeight(offset+(degree+1)*(degree+1)-ipt-1) << endl;

                    for (int ipt=1; ipt<degree; ipt++)
                        output_file << elt->controlPointWeight(offset+ipt*(degree+1)) << endl;

                    // side edges
                    for (int ipt=1; ipt<degree; ipt++)
                        output_file << elt->controlPointWeight(ipt*(degree+1)*(degree+1)) << endl;

                    for (int ipt=1; ipt<degree; ipt++)
                        output_file << elt->controlPointWeight(degree + ipt*(degree+1)*(degree+1)) << endl;

                    for (int ipt=1; ipt<degree; ipt++)
                        output_file << elt->controlPointWeight(degree*(degree+1)+degree + ipt*(degree+1)*(degree+1)) << endl;

                    for (int ipt=1; ipt<degree; ipt++)
                        output_file << elt->controlPointWeight(degree*(degree+1) + ipt*(degree+1)*(degree+1)) << endl;

                }
            }
        }

        //third loop for face points
        for (int iel=0; iel<mesh->elementNumber(); iel++){
            igElement *elt = mesh->element(iel);
            if(elt->isActive()){
                if(elt->partition() == part_id){
                    int degree = elt->cellDegree();

                    // bottom face
                    for (int ipt2=1; ipt2<degree; ipt2++){
                        for (int ipt1=1; ipt1<degree; ipt1++){
                            output_file << elt->controlPointWeight((degree+1)*degree + ipt1 - (degree+1)*ipt2) << endl;
                        }
                    }

                    // south face
                    for (int ipt3=1; ipt3<degree; ipt3++){
                        for (int ipt1=1; ipt1<degree; ipt1++){
                            output_file << elt->controlPointWeight(ipt1 + (degree+1)*(degree+1)*ipt3) << endl;
                        }
                    }

                    //  east face
                    for (int ipt3=1; ipt3<degree; ipt3++){
                        for (int ipt2=1; ipt2<degree; ipt2++){
                            output_file << elt->controlPointWeight(degree + ipt2*(degree+1) +(degree+1)*(degree+1)*ipt3) << endl;
                        }
                    }

                    //  north face
                    for (int ipt3=1; ipt3<degree; ipt3++){
                        for (int ipt1=1; ipt1<degree; ipt1++){
                            output_file << elt->controlPointWeight((degree+1)*(degree+1) -1 - ipt1 +(degree+1)*(degree+1)*ipt3) << endl;
                        }
                    }

                    // west face
                    for (int ipt3=1; ipt3<degree; ipt3++){
                        for (int ipt2=1; ipt2<degree; ipt2++){
                            output_file << elt->controlPointWeight(degree*(degree+1) -ipt2*(degree+1) + (degree+1)*(degree+1)*ipt3) << endl;
                        }
                    }

                    // top face
                    for (int ipt2=1; ipt2<degree; ipt2++){
                        for (int ipt1=1; ipt1<degree; ipt1++){
                            output_file << elt->controlPointWeight((degree+1)*(degree+1)*degree + ipt1+(degree+1)*ipt2) << endl;
                        }
                    }

                }
            }
        }

        // fourth loop for interior points
        for (int iel=0; iel<mesh->elementNumber(); iel++){
            igElement *elt = mesh->element(iel);
            if(elt->isActive()){
                if(elt->partition() == part_id){
                    int degree = elt->cellDegree();

                    for (int ipt3=1; ipt3<degree; ipt3++){
                        for (int ipt2=1; ipt2<degree; ipt2++){
                            for (int ipt1=1; ipt1<degree; ipt1++){

                                output_file << elt->controlPointWeight(ipt1 + (degree+1)*ipt2 + (degree+1)*(degree+1)*ipt3) << endl;
                            }
                        }
                    }


                }
            }
        }
        output_file << endl;

        stringstream ss;
        ss << degree;
        string elt_coll = "NURBS"+ss.str();

        output_file << "FiniteElementSpace" << endl;
        output_file << "FiniteElementCollection: " << elt_coll << endl;
        output_file << "VDim: 3" << endl;
        output_file << "Ordering: 1" << endl;
        output_file << endl;


        // first loop for corners
        for (int iel=0; iel<mesh->elementNumber(); iel++){
            igElement *elt = mesh->element(iel);
            if(elt->isActive()){
                if(elt->partition() == part_id){
                    int degree = elt->cellDegree();
                    index = 0;
                    output_file << elt->controlPointCoordinate(index,0) << " " << elt->controlPointCoordinate(index,1) << " " << elt->controlPointCoordinate(index,2) << endl;
                    index = degree;
                    output_file << elt->controlPointCoordinate(index,0) << " " << elt->controlPointCoordinate(index,1) << " " << elt->controlPointCoordinate(index,2) << endl;
                    index = (degree+1)*degree + degree;
                    output_file << elt->controlPointCoordinate(index,0) << " " << elt->controlPointCoordinate(index,1) << " " << elt->controlPointCoordinate(index,2) << endl;
                    index = (degree+1)*degree;
                    output_file << elt->controlPointCoordinate(index,0) << " " << elt->controlPointCoordinate(index,1) << " " << elt->controlPointCoordinate(index,2) << endl;
                    index = (degree+1)*(degree+1)*degree;
                    output_file << elt->controlPointCoordinate(index,0) << " " << elt->controlPointCoordinate(index,1) << " " << elt->controlPointCoordinate(index,2) << endl;
                    index = (degree+1)*(degree+1)*degree + degree;
                    output_file << elt->controlPointCoordinate(index,0) << " " << elt->controlPointCoordinate(index,1) << " " << elt->controlPointCoordinate(index,2) << endl;
                    index = (degree+1)*(degree+1)*degree + (degree+1)*degree + degree;
                    output_file << elt->controlPointCoordinate(index,0) << " " << elt->controlPointCoordinate(index,1) << " " << elt->controlPointCoordinate(index,2) << endl;
                    index = (degree+1)*(degree+1)*degree + (degree+1)*degree;
                    output_file << elt->controlPointCoordinate(index,0) << " " << elt->controlPointCoordinate(index,1) << " " << elt->controlPointCoordinate(index,2) << endl;
                }
            }
        }

        // second loop for edges
        for (int iel=0; iel<mesh->elementNumber(); iel++){
            igElement *elt = mesh->element(iel);
            if(elt->isActive()){
                if(elt->partition() == part_id){
                    int degree = elt->cellDegree();

                    // bottom edges
                    for (int ipt=1; ipt<degree; ipt++){
                        index = ipt;
                        output_file << elt->controlPointCoordinate(index,0) << " " << elt->controlPointCoordinate(index,1) << " " << elt->controlPointCoordinate(index,2) << endl;
                    }

                    for (int ipt=1; ipt<degree; ipt++){
                        index = degree+ipt*(degree+1);
                        output_file << elt->controlPointCoordinate(index,0) << " " << elt->controlPointCoordinate(index,1) << " " << elt->controlPointCoordinate(index,2) << endl;
                    }

                    for (int ipt=1; ipt<degree; ipt++){
                        index = (degree+1)*(degree+1)-ipt-1;
                        output_file << elt->controlPointCoordinate(index,0) << " " << elt->controlPointCoordinate(index,1) << " " << elt->controlPointCoordinate(index,2) << endl;
                    }

                    for (int ipt=1; ipt<degree; ipt++){
                        index = ipt*(degree+1);
                        output_file << elt->controlPointCoordinate(index,0) << " " << elt->controlPointCoordinate(index,1) << " " << elt->controlPointCoordinate(index,2) << endl;
                    }

                    // top edges
                    offset = (degree+1)*(degree+1)*degree;
                    for (int ipt=1; ipt<degree; ipt++){
                        index = offset+ipt;
                        output_file << elt->controlPointCoordinate(index,0) << " " << elt->controlPointCoordinate(index,1) << " " << elt->controlPointCoordinate(index,2) << endl;
                    }

                    for (int ipt=1; ipt<degree; ipt++){
                        index = offset+degree+ipt*(degree+1);
                        output_file << elt->controlPointCoordinate(index,0) << " " << elt->controlPointCoordinate(index,1) << " " << elt->controlPointCoordinate(index,2) << endl;
                    }

                    for (int ipt=1; ipt<degree; ipt++){
                        index = offset+(degree+1)*(degree+1)-ipt-1;
                        output_file << elt->controlPointCoordinate(index,0) << " " << elt->controlPointCoordinate(index,1) << " " << elt->controlPointCoordinate(index,2) << endl;
                    }

                    for (int ipt=1; ipt<degree; ipt++){
                        index = offset+ipt*(degree+1);
                        output_file << elt->controlPointCoordinate(index,0) << " " << elt->controlPointCoordinate(index,1) << " " << elt->controlPointCoordinate(index,2) << endl;
                    }

                    // side edges
                    for (int ipt=1; ipt<degree; ipt++){
                        index = ipt*(degree+1)*(degree+1);
                        output_file << elt->controlPointCoordinate(index,0) << " " << elt->controlPointCoordinate(index,1) << " " << elt->controlPointCoordinate(index,2) << endl;
                    }

                    for (int ipt=1; ipt<degree; ipt++){
                        index = degree + ipt*(degree+1)*(degree+1);
                        output_file << elt->controlPointCoordinate(index,0) << " " << elt->controlPointCoordinate(index,1) << " " << elt->controlPointCoordinate(index,2) << endl;
                    }

                    for (int ipt=1; ipt<degree; ipt++){
                        index = degree*(degree+1)+degree + ipt*(degree+1)*(degree+1);
                        output_file << elt->controlPointCoordinate(index,0) << " " << elt->controlPointCoordinate(index,1) << " " << elt->controlPointCoordinate(index,2) << endl;
                    }

                    for (int ipt=1; ipt<degree; ipt++){
                        index = degree*(degree+1) + ipt*(degree+1)*(degree+1);
                        output_file << elt->controlPointCoordinate(index,0) << " " << elt->controlPointCoordinate(index,1) << " " << elt->controlPointCoordinate(index,2) << endl;
                    }

                }
            }
        }

        //third loop for face points
        for (int iel=0; iel<mesh->elementNumber(); iel++){
            igElement *elt = mesh->element(iel);
            if(elt->isActive()){
                if(elt->partition() == part_id){
                    int degree = elt->cellDegree();

                    // bottom face
                    for (int ipt2=1; ipt2<degree; ipt2++){
                        for (int ipt1=1; ipt1<degree; ipt1++){
                            index = (degree+1)*degree + ipt1 - (degree+1)*ipt2;
                            output_file << elt->controlPointCoordinate(index,0) << " " << elt->controlPointCoordinate(index,1) << " " << elt->controlPointCoordinate(index,2) << endl;
                        }
                    }

                    // south face
                    for (int ipt3=1; ipt3<degree; ipt3++){
                        for (int ipt1=1; ipt1<degree; ipt1++){
                            index = ipt1 + (degree+1)*(degree+1)*ipt3;
                            output_file << elt->controlPointCoordinate(index,0) << " " << elt->controlPointCoordinate(index,1) << " " << elt->controlPointCoordinate(index,2) << endl;
                        }
                    }

                    //  east face
                    for (int ipt3=1; ipt3<degree; ipt3++){
                        for (int ipt2=1; ipt2<degree; ipt2++){
                            index = degree + ipt2*(degree+1) +(degree+1)*(degree+1)*ipt3;
                            output_file << elt->controlPointCoordinate(index,0) << " " << elt->controlPointCoordinate(index,1) << " " << elt->controlPointCoordinate(index,2) << endl;
                        }
                    }

                    //  north face
                    for (int ipt3=1; ipt3<degree; ipt3++){
                        for (int ipt1=1; ipt1<degree; ipt1++){
                            index = (degree+1)*(degree+1) -1 - ipt1 +(degree+1)*(degree+1)*ipt3;
                            output_file << elt->controlPointCoordinate(index,0) << " " << elt->controlPointCoordinate(index,1) << " " << elt->controlPointCoordinate(index,2) << endl;
                        }
                    }

                    // west face
                    for (int ipt3=1; ipt3<degree; ipt3++){
                        for (int ipt2=1; ipt2<degree; ipt2++){
                            index = degree*(degree+1) -ipt2*(degree+1) + (degree+1)*(degree+1)*ipt3;
                            output_file << elt->controlPointCoordinate(index,0) << " " << elt->controlPointCoordinate(index,1) << " " << elt->controlPointCoordinate(index,2) << endl;
                        }
                    }

                    // top face
                    for (int ipt2=1; ipt2<degree; ipt2++){
                        for (int ipt1=1; ipt1<degree; ipt1++){
                            index = (degree+1)*(degree+1)*degree + ipt1+(degree+1)*ipt2;
                            output_file << elt->controlPointCoordinate(index,0) << " " << elt->controlPointCoordinate(index,1) << " " << elt->controlPointCoordinate(index,2) << endl;
                        }
                    }

                }
            }
        }

        // fourth loop for interior points
        for (int iel=0; iel<mesh->elementNumber(); iel++){
            igElement *elt = mesh->element(iel);
            if(elt->isActive()){
                if(elt->partition() == part_id){
                    int degree = elt->cellDegree();

                    for (int ipt3=1; ipt3<degree; ipt3++){
                        for (int ipt2=1; ipt2<degree; ipt2++){
                            for (int ipt1=1; ipt1<degree; ipt1++){

                                index = ipt1 + (degree+1)*ipt2 + (degree+1)*(degree+1)*ipt3;
                                output_file << elt->controlPointCoordinate(index,0) << " " << elt->controlPointCoordinate(index,1) << " " << elt->controlPointCoordinate(index,2) << endl;
                            }
                        }
                    }


                }
            }
        }

        output_file.close();
    }
}



// //////////////////////////////////////////////////////////
// set parameters
// //////////////////////////////////////////////////////////

void igFileManager::setPartitionNumber(int part)
{
    this->partition_number = part;
}

void igFileManager::setMyPartition(int part)
{
    this->my_partition = part;
}

void igFileManager::setSavePeriod(double period)
{
    save_period = period;
}

// //////////////////////////////////////////////////////////
// streams for glvis
// //////////////////////////////////////////////////////////


void igFileManager::writeMesh2DGlvis(igMesh *mesh, iostream& stream)
{
    IG_ASSERT(mesh, "no mesh");

    int degree = mesh->element(0)->cellDegree();
    int pts_per_elt = (degree+1)*(degree+1);

    int part_id = my_partition;

    stream << "MFEM NURBS mesh v1.0" << endl;
    stream << endl;

    stream << "dimension" << endl;
    stream << 2 << endl;
    stream << endl;

    stream << "elements" << endl;
    stream << mesh->activeElementNumber() << endl;

    int iel_counter = 0;
    for (int iel=0; iel<mesh->elementNumber(); iel++){
        igElement *elt = mesh->element(iel);
        if(elt->isActive()){

            int degree = elt->cellDegree();
            int ctrl_pts_number = elt->controlPointNumber();

            stream << 1 << " ";
            stream << 3 << " ";
            stream << iel_counter*pts_per_elt + 0 << " ";
            stream << iel_counter*pts_per_elt + 1 << " ";
            stream << iel_counter*pts_per_elt + 2 << " ";
            stream << iel_counter*pts_per_elt + 3 << " ";
            stream << endl;

            iel_counter++;
        }
    }
    stream << endl;

    stream << "boundary" << endl;
    stream << 0 << endl;
    stream << endl;

    stream << "edges" << endl;
    stream << 4*mesh->activeElementNumber() << endl;

    iel_counter = 0;

    for (int iel=0; iel<mesh->elementNumber(); iel++){

        igElement *elt = mesh->element(iel);

        if(elt->isActive()){

            int degree = elt->cellDegree();
            int ctrl_pts_number = elt->controlPointNumber();
            stream << 0 << " ";
            stream << iel_counter*pts_per_elt << " ";
            stream << iel_counter*pts_per_elt + 1 << " ";
            stream << endl;
            stream << 0 << " ";
            stream << iel_counter*pts_per_elt + 1 << " ";
            stream << iel_counter*pts_per_elt + 2 << " ";
            stream << endl;
            stream << 0 << " ";
            stream << iel_counter*pts_per_elt + 3 << " ";
            stream << iel_counter*pts_per_elt + 2 << " ";
            stream << endl;
            stream << 0 << " ";
            stream << iel_counter*pts_per_elt << " ";
            stream << iel_counter*pts_per_elt + 3 << " ";
            stream << endl;

            iel_counter++;
        }
    }

    stream << endl;

    degree = mesh->element(0)->cellDegree();

    stream << "vertices" << endl;
    stream << mesh->activeElementNumber()*(degree+1)*(degree+1) << endl;
    stream << endl;

    stream << "knotvectors" << endl;
    stream << 1 << endl;
    stream << degree << " ";
    stream << degree+1 << " ";
    for (int i=0; i<degree+1; i++){
        stream << 0 << " ";
    }
    for (int i=0; i<degree+1; i++){
        stream << 1 << " ";
    }
    stream << endl;
    stream << endl;

    stream << "weights" << endl;

            for (int iel=0; iel<mesh->elementNumber(); iel++){
            igElement *elt = mesh->element(iel);
            if(elt->isActive()){
                if(elt->partition() == part_id){
                    int degree = elt->cellDegree();
                    int ctrl_pts_number = elt->controlPointNumber();
                    stream << elt->controlPointWeight(0) << endl;
                    stream << elt->controlPointWeight(degree) << endl;
                    stream << elt->controlPointWeight(ctrl_pts_number-1) << endl;
                    stream << elt->controlPointWeight(ctrl_pts_number-degree-1) << endl;
                }
            }
        }

        for (int iel=0; iel<mesh->elementNumber(); iel++){
            igElement *elt = mesh->element(iel);
            if(elt->isActive()){
                if(elt->partition() == part_id){
                    int degree = elt->cellDegree();
                    int ctrl_pts_number = elt->controlPointNumber();

                    for (int idof=1; idof<degree; idof++)
                        stream << elt->controlPointWeight(idof) << endl;

                    for (int idof=1; idof<degree; idof++)
                        stream << elt->controlPointWeight(degree+idof*(degree+1)) << endl;

                    for (int idof=1; idof<degree; idof++)
                        stream << elt->controlPointWeight(ctrl_pts_number-1-idof) << endl;

                    for (int idof=1; idof<degree; idof++)
                        stream << elt->controlPointWeight(idof*(degree+1)) << endl;
                }
            }
        }

        for (int iel=0; iel<mesh->elementNumber(); iel++){
            igElement *elt = mesh->element(iel);
            if(elt->isActive()){
                if(elt->partition() == part_id){
                    int degree = elt->cellDegree();
                    int ctrl_pts_number = elt->controlPointNumber();

                    for (int jdof=1; jdof<degree; jdof++)
                        for (int idof=1; idof<degree; idof++)
                            stream << elt->controlPointWeight(jdof*(degree+1)+idof) << endl;
                }
            }
        }


    stream << endl;

    stringstream ss;
    ss << degree;
    string elt_coll = "NURBS"+ss.str();

    stream << "FiniteElementSpace" << endl;
    stream << "FiniteElementCollection: " << elt_coll << endl;
    stream << "VDim: 2" << endl;
    stream << "Ordering: 1" << endl;
    stream << endl;

    for (int iel=0; iel<mesh->elementNumber(); iel++){
        igElement *elt = mesh->element(iel);
        if(elt->isActive()){
            int degree = elt->cellDegree();
            int ctrl_pts_number = elt->controlPointNumber();
            stream << elt->controlPointCoordinate(0,0) << " "<< elt->controlPointCoordinate(0,1) << endl;
            stream << elt->controlPointCoordinate(degree,0) << " "<< elt->controlPointCoordinate(degree,1) << endl;
            stream << elt->controlPointCoordinate(ctrl_pts_number-1,0) << " "<< elt->controlPointCoordinate(ctrl_pts_number-1,1) << endl;
            stream << elt->controlPointCoordinate(ctrl_pts_number-degree-1,0) << " "<< elt->controlPointCoordinate(ctrl_pts_number-degree-1,1) << endl;
        }
    }

    for (int iel=0; iel<mesh->elementNumber(); iel++){
        igElement *elt = mesh->element(iel);
        if(elt->isActive()){
            int degree = elt->cellDegree();
            int ctrl_pts_number = elt->controlPointNumber();

            for (int idof=1; idof<degree; idof++)
                stream << elt->controlPointCoordinate(idof,0) << " "<< elt->controlPointCoordinate(idof,1) << endl;

            for (int idof=1; idof<degree; idof++)
                stream << elt->controlPointCoordinate(degree+idof*(degree+1),0) << " "<< elt->controlPointCoordinate(degree+idof*(degree+1),1) << endl;

            for (int idof=1; idof<degree; idof++)
                stream << elt->controlPointCoordinate(ctrl_pts_number-1-idof,0) << " "<< elt->controlPointCoordinate(ctrl_pts_number-1-idof,1) << endl;

            for (int idof=1; idof<degree; idof++)
                stream << elt->controlPointCoordinate(idof*(degree+1),0) << " "<< elt->controlPointCoordinate(idof*(degree+1),1) << endl;
        }
    }

    for (int iel=0; iel<mesh->elementNumber(); iel++){
        igElement *elt = mesh->element(iel);
        if(elt->isActive()){
            int degree = elt->cellDegree();
            int ctrl_pts_number = elt->controlPointNumber();

            for (int jdof=1; jdof<degree; jdof++)
                for (int idof=1; idof<degree; idof++)
                    stream << elt->controlPointCoordinate(jdof*(degree+1)+idof,0) << " "<< elt->controlPointCoordinate(jdof*(degree+1)+idof,1) << endl;
        }
    }
}

// ////////////////////////////////////////////////////////////////
//
// ////////////////////////////////////////////////////////////////

void igFileManager::writeSolutionField2DGlviz(vector<double> *state, igMesh *mesh, int var_id, int label, iostream& stream)
{
    IG_ASSERT(mesh, "no mesh");
    IG_ASSERT(state, "no state");

    int degree = mesh->element(0)->cellDegree();
    int size = (degree+1)*(degree+1);

    stringstream ss;
    ss << degree;

    string elt_coll = "NURBS"+ss.str();

    stream << "FiniteElementSpace" << endl;
    stream << "FiniteElementCollection: " << elt_coll << endl;
    stream << "VDim: 1" << endl;
    stream << "Ordering: 1" << endl;
    stream << endl;

    //
    // first loop de define corner dof coordinates
    //

    for (int iel=0; iel<mesh->elementNumber(); iel++){

        igElement *elt = mesh->element(iel);
        if(elt->isActive()){

            stream << state->at(elt->globalId(0,var_id)) << endl;
            stream << state->at(elt->globalId(degree,var_id)) << endl;
            stream << state->at(elt->globalId(size-1,var_id)) << endl;
            stream << state->at(elt->globalId(size-1-degree,var_id)) << endl;
        }
    }

    //
    // second loop de define mid-edge dof coordinates
    //

    for (int iel=0; iel<mesh->elementNumber(); iel++){

        igElement *elt = mesh->element(iel);

        if(elt->isActive()){

            for (int ipt=0; ipt<degree-1; ipt++){
                int offset = 1+ipt;
                stream << state->at(elt->globalId(offset,var_id)) << endl;
            }
            for (int ipt=0; ipt<degree-1; ipt++){
                int offset = (ipt+2)*(degree+1) -1;
                stream << state->at(elt->globalId(offset,var_id)) << endl;
            }
            for (int ipt=0; ipt<degree-1; ipt++){
                int offset = size-ipt-2;
                stream << state->at(elt->globalId(offset,var_id)) << endl;
            }
            for (int ipt=0; ipt<degree-1; ipt++){
                int offset = (ipt+1)*(degree+1);
                stream << state->at(elt->globalId(offset,var_id)) << endl;
            }
        }
    }

    //
    // third loop de define center dof coordinates
    //

    for (int iel=0; iel<mesh->elementNumber(); iel++){

        igElement *elt = mesh->element(iel);

        if(elt->isActive()){

            for (int ipt2=0; ipt2<degree-1; ipt2++){
                for (int ipt1=0; ipt1<degree-1; ipt1++){

                    int offset = (ipt2+1)*(degree+1) + ipt1 + 1;
                    stream << state->at(elt->globalId(offset,var_id)) << endl;
                }
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////////

void igFileManager::writeInterface(igCouplingInterface *interface, double time)
{
    if(interface->name() == "membrane"){

        if(save_interface_counter*save_period - time < 1.E-10) {

            this->plotBSplineCurve(interface->interfaceDegree(), interface->controlPointNumber(), interface->knotVector(), interface->controlPointsX(), interface->interfaceDisplacementsStructure(), save_interface_counter);
            this->plotControlNet(interface->controlPointNumber(), interface->controlPointsX(), interface->interfaceDisplacementsStructure(), save_interface_counter);

            save_interface_counter++;
        }
    }
}

void igFileManager::plotBSplineCurve(int degree, int ctrl_pt_number, vector<double> *knot_vector, vector<double> *pt_x, vector<double> *pt_y, int id)
{
    int sample_number = 100;

    int element_number = ctrl_pt_number - degree;
    int knot_number = ctrl_pt_number + degree + 1;

    stringstream ss_t;
    ss_t << id;

    ostringstream curve_filename;
    curve_filename << "curve_" << ss_t.str() << ".dat";

    ofstream curve_file(curve_filename.str(), ios::out);

    // B-Spline basis
    igBasisBSpline *basis = new igBasisBSpline(degree, knot_vector);

    // basis function values
    vector<double> values;
    values.resize(ctrl_pt_number);

    for(int i=0; i<sample_number; i++){

        double param = double(i)*knot_vector->at(knot_number-1)/double(sample_number-1);

        basis->evalFunction(param,values);

        double coord_x = 0.;
        double coord_y = 0.;
        for(int ipt=0; ipt<ctrl_pt_number; ipt++){
            coord_x += values[ipt]*pt_x->at(ipt);
            coord_y += values[ipt]*pt_y->at(ipt);
        }
        curve_file << coord_x << " " << coord_y << endl;
    }
    curve_file << endl;
    curve_file.close();

    delete basis;

}

void igFileManager::plotControlNet(int ctrl_pt_number, vector<double> *pt_x, vector<double> *pt_y, int id)
{
    stringstream ss_t;
    ss_t << id;

    ostringstream pt_filename;
    pt_filename << "pt_" << ss_t.str() << ".dat";

    ofstream pt_file(pt_filename.str(), ios::out);

    for(int ipt=0; ipt<ctrl_pt_number; ipt++){
        pt_file << pt_x->at(ipt) << " " << pt_y->at(ipt) << endl;
    }
    pt_file.close();

}
