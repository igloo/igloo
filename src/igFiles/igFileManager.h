/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igFilesExport.h>

#include <vector>
#include <string>
#include <iostream>

class igMesh;
class igDistributor;
class igCouplingInterface;

using namespace std;

class IGFILES_EXPORT igFileManager
{
public:
    igFileManager(void);
    ~igFileManager(void);

public:
    void setSavePeriod(double period);
    void setPartitionNumber(int part);
    void setMyPartition(int part);
    void enableSaveMesh(void);
    void enableSaveProperty(void);
    void enableSaveVorticity(void);

public:
    void readSolutionIgloo(const string& filename, const int variable_number, igMesh *mesh, vector<double> *state);
    void readSolverParameters(vector<double> *solver_parameters);

    void readNurbsCurve(vector<double> *knot_vector, vector<double> *pt_x, vector<double> *pt_y);

public:

    // mesh writer interface
    void writeMesh(igMesh *mesh);
    void writeMesh(igMesh *mesh,double time);

    // generic writers (mesh+solution)
    void writeMeshSolution(vector<double> *state, igMesh *mesh);
    void writeMeshSolution(vector<double> *state, vector<double> *artificial_viscosity, vector<double> *vorticity, igMesh *mesh, double time);

    // --

    // mesh writer for igloo
    void writeMeshIgloo( const string& filename, igMesh *mesh, igDistributor *distributor);

    // solution writer for igloo
    void writeSolutionIgloo(const string& filename, const int variable_number, igMesh *mesh, vector<double> *state);

    // --

    // tmp file for restart
    void writeRestartFile(vector<double> *state, igMesh *mesh, double time);
    void readRestartFile(vector<double> *state, igMesh *mesh, double &time, bool flag_ALE);
    void applyMeshRestart(igMesh *mesh);

    // --

    // mesh writer for glvis
    void writeStructuredMesh3DGlvis(int element_number_1, int element_number_2, int element_number_3, int degree, vector<double> &xcoord, vector<double> &ycoord, vector<double> &zcoord, vector<double> &weight, int nb_part_1, int nb_part_2, int nb_part_3);
    void writeMesh2DGlvis(const string& filename, igMesh *mesh, int part_id, bool sequential_run);
    void writeMesh3DGlvis(const string& filename, igMesh *mesh, int part_id, bool sequential_run);

    // solution writer for glvis
    void writeSolutionField2DGlviz(const string& filename, vector<double> *state, igMesh *mesh, int var_id, int label);
    void writeSolutionField3DGlviz(const string& filename, vector<double> *state, igMesh *mesh, int var_id, int label);

    // --

    // solution for gnuplot
    void writeSolutionField1DGnuplot(vector<double> *state, igMesh *mesh);

    // --

    // stream for glvis
    void writeMesh2DGlvis(igMesh *mesh, iostream& stream);
    void writeSolutionField2DGlviz(vector<double> *state, igMesh *mesh, int var_id, int label, iostream&);


    // wall data
    void writeWallData(vector<double> *state, igMesh *mesh);

public:
    void writeInterface(igCouplingInterface *interface, double time);
    void plotBSplineCurve(int degree, int ctrl_pt_number, vector<double> *knot_vector, vector<double> *pt_x, vector<double> *pt_y, int id);
    void plotControlNet(int ctrl_pt_number, vector<double> *pt_x, vector<double> *pt_y, int id);

private:
    double save_period;
    double last_save;
    int save_counter;
    int save_mesh_counter;

    int save_interface_counter;

    int partition_number;
    int my_partition;

    bool save_mesh;
    bool save_artificial_viscosity;
    bool save_vorticity;
};

//
// igFileManager.h ends here
