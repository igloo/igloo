/***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
            https://gitlab.inria.fr/igloo/igloo/-/wikis/home
****************************************************************************
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
***************************************************************************/
#pragma once

#include <igFilesExport.h>

#include <vector>
#include <string>
#include <iostream>

class igMesh;

using namespace std;

class IGFILES_EXPORT igVtkWriter
{
public:
    igVtkWriter(void);
    ~igVtkWriter(void);

public:
    void convertGlVis(int mesh_id, int file_id, int proc, int refine, bool density, bool energy, bool momentum);
    void extractWall(igMesh *mesh, vector<double> *coordinates, int refine_number);


};

//
// igVtkWriter.h ends here
