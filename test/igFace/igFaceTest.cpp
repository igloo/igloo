
#include <iostream>
#include <math.h>

#include "igCore/igFace.h"
#include "igCore/igBasis.h"

using namespace std;


// main program
int main(int argc, char **argv)
{
    bool test_fail = false;
    double tol = 1.E-14;

    cout.precision(16);

    //-------------- 1D test --------------------

    igFace *new_face = new igFace;

    new_face->setCellId(0);
    new_face->setDegree(0);
    new_face->setPhysicalDimension(1);
    new_face->setParametricDimension(0);
    new_face->setGaussPointNumberByDirection(1);
    new_face->setVariableNumber(1);
    new_face->setDerivativeNumber(0);

    new_face->initializeDegreesOfFreedom();

    // test number of Gauss points
    int gauss_point_number = new_face->gaussPointNumber();

    if ( (gauss_point_number != 1)){
        test_fail = true;
        cout << "gauss pts number " << gauss_point_number << endl;
    }

    // test coordinate
    if ( fabs(new_face->gaussPointParametricCoordinate(0)[0] - 0.5 ) > tol ){
        test_fail = true;
        cout << "gauss pts coord " << new_face->gaussPointParametricCoordinate(0)[0] << endl;
    }

    // test weight
    if ( fabs(new_face->gaussPointWeight(0) - 1. ) > tol ){
        test_fail = true;
        cout << "gauss pts weight " << new_face->gaussPointWeight(0) << endl;
    }

    //-------------- 2D test --------------------

    vector<double> *coordinates = new vector<double>;
    coordinates->resize(9,0.);

    vector<double> *coord = new vector<double>;
    coord->resize(2);

    igFace *new_face2D = new igFace;

    new_face2D->setCellId(0);
    new_face2D->setDegree(2);
    new_face2D->setPhysicalDimension(2);
    new_face2D->setParametricDimension(1);
    new_face2D->setGaussPointNumberByDirection(3);
    new_face2D->setVariableNumber(1);
    new_face2D->setDerivativeNumber(0);
    new_face2D->setCoordinates(coordinates);

    new_face2D->initializeDegreesOfFreedom();

    // dof Maps
    for(int ivar=0; ivar<3; ivar++){
        for (int idof=0; idof<3; idof++){
            new_face2D->setLeftDofMap(idof,ivar,idof+3*ivar);
        }
    }

    for(int ivar=0; ivar<3; ivar++){
        for (int idof=0; idof<3; idof++){
            new_face2D->setRightDofMap(idof,ivar,idof+3*ivar);
        }
    }

    // store control points
    for (int ipt=0; ipt<new_face2D->controlPointNumber(); ipt++){

        for (int idim=0; idim<2; idim++){
            coordinates->at(idim*3+ipt) = double(ipt+1)*double(idim+2);
        }
        coordinates->at(2*3+ipt) = 1.;
    }

    coord->at(0) = 0.;
    coord->at(1) = 0.;

    new_face2D->initializeGeometry();
    new_face2D->initializeNormal(coord);

    // test number of Gauss points
    int gauss_point_number2D = new_face2D->gaussPointNumber();

    if ( (gauss_point_number2D != 3)){
        test_fail = true;
        cout << "gauss pts number 2D " << gauss_point_number << endl;
    }

    // test gauss pts coordinate
    if ( fabs(new_face2D->gaussPointParametricCoordinate(0)[0] - 0.1127016653792585) > tol ){
        test_fail = true;
        cout << "2D gauss pts 1 coord " << new_face2D->gaussPointParametricCoordinate(0)[0] << endl;
    }
    if ( fabs(new_face2D->gaussPointParametricCoordinate(1)[0] - 0.5) > tol ){
        test_fail = true;
        cout << "2D gauss pts 2 coord " << new_face2D->gaussPointParametricCoordinate(1)[0] << endl;
    }
    if ( fabs(new_face2D->gaussPointParametricCoordinate(2)[0] - 0.8872983346207415) > tol ){
        test_fail = true;
        cout << "2D gauss pts 3 coord " << new_face2D->gaussPointParametricCoordinate(2)[0] << endl;
    }

    // test gauss pts weight
    if ( fabs(new_face2D->gaussPointWeight(0) - 0.277777777777778 ) > tol ){
        test_fail = true;
        cout << "2D gauss pts 1 weight " << new_face2D->gaussPointWeight(0) << endl;
    }
    if ( fabs(new_face2D->gaussPointWeight(1) - 0.444444444444445 ) > tol ){
        test_fail = true;
        cout << "2D gauss pts 2 weight " << new_face2D->gaussPointWeight(1) << endl;
    }
    if ( fabs(new_face2D->gaussPointWeight(2) - 0.277777777777778 ) > tol ){
        test_fail = true;
        cout << "2D gauss pts 3 weight " << new_face2D->gaussPointWeight(2) << endl;
    }


    // test control pts coordinates
    for (int ipt=0; ipt<new_face2D->controlPointNumber(); ipt++){

        for (int idim=0; idim<2; idim++){
            if ( fabs(new_face2D->controlPointCoordinate(ipt,idim) - double(ipt+1)*double(idim+2)) > tol ){
                 test_fail = true;
                 cout << "control point " << ipt << " coord " << idim << " : " << new_face2D->controlPointCoordinate(ipt,idim) << endl;
            }
        }
    }

    // test dof maps

    for(int ivar=0; ivar<3; ivar++){
        for (int idof=0; idof<3; idof++){
            if ( new_face2D->dofLeft(idof,ivar) != idof+3*ivar ){
                test_fail = true;
                cout << "left dof map " << idof << " : "<< new_face2D->dofLeft(idof,ivar) << endl;
            }
        }
    }

    for(int ivar=0; ivar<3; ivar++){
        for (int idof=0; idof<3; idof++){
            if ( new_face2D->dofRight(idof,ivar) != idof+3*ivar ){
                test_fail = true;
                cout << "right dof map " << idof << " : "<< new_face2D->dofRight(idof,ivar) << endl;
            }
        }
    }

    // test normal at gauss pts
    if ( fabs(new_face2D->gaussPointNormalPtr(0)[0] + 0.832050294337844) > tol ){
        test_fail = true;
        cout << "2D Normal gauss pts 1 coord 1: " << new_face2D->gaussPointNormalPtr(0)[0] << endl;
    }
    if ( fabs(new_face2D->gaussPointNormalPtr(0)[1] - 0.554700196225229) > tol ){
        test_fail = true;
        cout << "2D Normal gauss pts 1 coord 2: " << new_face2D->gaussPointNormalPtr(0)[1] << endl;
    }
    if ( fabs(new_face2D->gaussPointNormalPtr(1)[0] + 0.832050294337844) > tol ){
        test_fail = true;
        cout << "2D Normal gauss pts 2 coord 1: " << new_face2D->gaussPointNormalPtr(1)[0] << endl;
    }
    if ( fabs(new_face2D->gaussPointNormalPtr(1)[1] - 0.554700196225229) > tol ){
        test_fail = true;
        cout << "2D Normal gauss pts 2 coord 2: " << new_face2D->gaussPointNormalPtr(1)[1] << endl;
    }
    if ( fabs(new_face2D->gaussPointNormalPtr(2)[0] + 0.832050294337844) > tol ){
        test_fail = true;
        cout << "2D Normal gauss pts 3 coord 1: " << new_face2D->gaussPointNormalPtr(2)[0] << endl;
    }
    if ( fabs(new_face2D->gaussPointNormalPtr(2)[1] - 0.554700196225229) > tol ){
        test_fail = true;
        cout << "2D Normal gauss pts 3 coord 2: " << new_face2D->gaussPointNormalPtr(2)[1] << endl;
    }

    // test Jacobian at Gauss pts
    if ( fabs(new_face2D->gaussPointJacobian(0) - 7.211102550927978) > tol ){
        test_fail = true;
        cout << "2D Jacobian gauss pts 1: " << new_face2D->gaussPointJacobian(0) << endl;
    }
    if ( fabs(new_face2D->gaussPointJacobian(1) - 7.211102550927978) > tol ){
        test_fail = true;
        cout << "2D Jacobian gauss pts 2: " << new_face2D->gaussPointJacobian(1) << endl;
    }
    if ( fabs(new_face2D->gaussPointJacobian(2) - 7.211102550927978) > tol ){
        test_fail = true;
        cout << "2D Jacobian gauss pts 3: " << new_face2D->gaussPointJacobian(2) << endl;
    }

    return test_fail;
}
