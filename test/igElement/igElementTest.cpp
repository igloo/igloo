
#include <iostream>
#include <math.h>

#include "igCore/igElement.h"
#include "igCore/igFace.h"
#include "igCore/igBasis.h"

using namespace std;


// main program
int main(int argc, char **argv)
{
    bool test_fail = false;
    double tol = 1.E-15;

    cout.precision(16);

    //-------------- 2D test --------------------

    vector<double> *coordinates = new vector<double>;
    coordinates->resize(27,0.);

    vector<double> *coord = new vector<double>;
    coord->resize(2);

    igElement *elt = new igElement;

    elt->setCellId(0);
    elt->setPhysicalDimension(2);
    elt->setParametricDimension(2);
    elt->setDegree(2);
    elt->setGaussPointNumberByDirection(3);
    elt->setVariableNumber(3);
    elt->setCoordinates(coordinates);

    elt->initializeDegreesOfFreedom();

    // store control points
    for (int ipt=0; ipt<9; ipt++){
        for (int idim=0; idim<2; idim++){
            coordinates->at(idim*9 + ipt) = double(ipt+1)*double(2*idim+3);
        }
    }

    // store map dof
    for (int ivar=0; ivar<3; ivar++){
        for (int ipt=0; ipt<elt->controlPointNumber(); ipt++){
            elt->setGlobalId(ipt, ivar, 9*ivar+ipt);
        }
    }

    elt->initializeGeometry();



    // test number of Gauss points
    int gauss_point_number = elt->gaussPointNumber();

    if ( (gauss_point_number != 9)){
        test_fail = true;
        cout << "gauss pts number " << gauss_point_number << endl;
    }

    // test control points
    for (int ipt=0; ipt<elt->controlPointNumber(); ipt++){

        for (int idim=0; idim<2; idim++){
            if ( fabs(elt->controlPointCoordinate(ipt,idim) - double(ipt+1)*double(2*idim+3)) > tol ){
                 test_fail = true;
                 cout << "control point " << ipt << " coord " << idim << " : " << elt->controlPointCoordinate(ipt,idim) << endl;
            }
        }
    }

    // test dof map
    for (int ipt=0; ipt<elt->controlPointNumber(); ipt++){

        for (int ivar=0; ivar<3; ivar++){
            if ( elt->globalId(ipt,ivar) != 9*ivar+ipt ){
                 test_fail = true;
                 cout << "control point " << ipt << " dof ID " <<  elt->globalId(ipt,ivar) << endl;
            }
        }
    }

    // test radius
    if ( fabs(elt->radius() - 5.830951894845301) > tol ){
         test_fail = true;
         cout << "radius : " <<  elt->radius() << endl;
    }

    // test gauss pts coordinate
    if ( fabs(elt->gaussPointParametricCoordinate(0,0) - 0.1127016653792585) > tol ){
        test_fail = true;
        cout << "gauss pts 0 coord 0 " << elt->gaussPointParametricCoordinate(0,0) << endl;
    }
    if ( fabs(elt->gaussPointParametricCoordinate(0,1) - 0.1127016653792585) > tol ){
        test_fail = true;
        cout << "gauss pts 0 coord 1 " << elt->gaussPointParametricCoordinate(0,1) << endl;
    }
    if ( fabs(elt->gaussPointParametricCoordinate(1,0) - 0.5) > tol ){
        test_fail = true;
        cout << "gauss pts 1 coord 0 " << elt->gaussPointParametricCoordinate(1,0) << endl;
    }
    if ( fabs(elt->gaussPointParametricCoordinate(1,1) - 0.1127016653792585) > tol ){
        test_fail = true;
        cout << "gauss pts 1 coord 1 " << elt->gaussPointParametricCoordinate(1,1) << endl;
    }
    if ( fabs(elt->gaussPointParametricCoordinate(2,0) - 0.8872983346207415) > tol ){
        test_fail = true;
        cout << "gauss pts 2 coord 0 " << elt->gaussPointParametricCoordinate(2,0) << endl;
    }
    if ( fabs(elt->gaussPointParametricCoordinate(2,1) - 0.1127016653792585) > tol ){
        test_fail = true;
        cout << "gauss pts 2 coord 1 " << elt->gaussPointParametricCoordinate(2,1) << endl;
    }

    if ( fabs(elt->gaussPointParametricCoordinate(3,0) - 0.1127016653792585) > tol ){
        test_fail = true;
        cout << "gauss pts 3 coord 0 " << elt->gaussPointParametricCoordinate(3,0) << endl;
    }
    if ( fabs(elt->gaussPointParametricCoordinate(3,1) - 0.5) > tol ){
        test_fail = true;
        cout << "gauss pts 3 coord 1 " << elt->gaussPointParametricCoordinate(3,1) << endl;
    }
    if ( fabs(elt->gaussPointParametricCoordinate(4,0) - 0.5) > tol ){
        test_fail = true;
        cout << "gauss pts 4 coord 0 " << elt->gaussPointParametricCoordinate(4,0) << endl;
    }
    if ( fabs(elt->gaussPointParametricCoordinate(4,1) - 0.5) > tol ){
        test_fail = true;
        cout << "gauss pts 4 coord 1 " << elt->gaussPointParametricCoordinate(4,1) << endl;
    }
    if ( fabs(elt->gaussPointParametricCoordinate(5,0) - 0.8872983346207415) > tol ){
        test_fail = true;
        cout << "gauss pts 5 coord 0 " << elt->gaussPointParametricCoordinate(5,0) << endl;
    }
    if ( fabs(elt->gaussPointParametricCoordinate(5,1) - 0.5) > tol ){
        test_fail = true;
        cout << "gauss pts 5 coord 1 " << elt->gaussPointParametricCoordinate(5,1) << endl;
    }

    if ( fabs(elt->gaussPointParametricCoordinate(6,0) - 0.1127016653792585) > tol ){
        test_fail = true;
        cout << "gauss pts 6 coord 0 " << elt->gaussPointParametricCoordinate(6,0) << endl;
    }
    if ( fabs(elt->gaussPointParametricCoordinate(6,1) - 0.8872983346207415) > tol ){
        test_fail = true;
        cout << "gauss pts 6 coord 1 " << elt->gaussPointParametricCoordinate(6,1) << endl;
    }
    if ( fabs(elt->gaussPointParametricCoordinate(7,0) - 0.5) > tol ){
        test_fail = true;
        cout << "gauss pts 7 coord 0 " << elt->gaussPointParametricCoordinate(7,0) << endl;
    }
    if ( fabs(elt->gaussPointParametricCoordinate(7,1) - 0.8872983346207415) > tol ){
        test_fail = true;
        cout << "gauss pts 7 coord 1 " << elt->gaussPointParametricCoordinate(7,1) << endl;
    }
    if ( fabs(elt->gaussPointParametricCoordinate(8,0) - 0.8872983346207415) > tol ){
        test_fail = true;
        cout << "gauss pts 8 coord 0 " << elt->gaussPointParametricCoordinate(8,0) << endl;
    }
    if ( fabs(elt->gaussPointParametricCoordinate(8,1) - 0.8872983346207415) > tol ){
        test_fail = true;
        cout << "gauss pts 8 coord 1 " << elt->gaussPointParametricCoordinate(8,1) << endl;
    }

    // test gauss pts weight
    if ( fabs(elt->gaussPointWeight(0) - 0.07716049382716063 ) > tol ){
        test_fail = true;
        cout << "2D gauss pts 1 weight " << elt->gaussPointWeight(0) << endl;
    }
    if ( fabs(elt->gaussPointWeight(1) - 0.1234567901234569 ) > tol ){
        test_fail = true;
        cout << "2D gauss pts 2 weight " << elt->gaussPointWeight(1) << endl;
    }
    if ( fabs(elt->gaussPointWeight(2) - 0.07716049382716063 ) > tol ){
        test_fail = true;
        cout << "2D gauss pts 3 weight " << elt->gaussPointWeight(2) << endl;
    }
    // test gauss pts weight
    if ( fabs(elt->gaussPointWeight(3) - 0.1234567901234569 ) > tol ){
        test_fail = true;
        cout << "2D gauss pts 4 weight " << elt->gaussPointWeight(3) << endl;
    }
    if ( fabs(elt->gaussPointWeight(4) - 0.1975308641975309 ) > tol ){
        test_fail = true;
        cout << "2D gauss pts 5 weight " << elt->gaussPointWeight(4) << endl;
    }
    if ( fabs(elt->gaussPointWeight(5) - 0.1234567901234569 ) > tol ){
        test_fail = true;
        cout << "2D gauss pts 6 weight " << elt->gaussPointWeight(5) << endl;
    }
    // test gauss pts weight
    if ( fabs(elt->gaussPointWeight(6) - 0.07716049382716063 ) > tol ){
        test_fail = true;
        cout << "2D gauss pts 7 weight " << elt->gaussPointWeight(6) << endl;
    }
    if ( fabs(elt->gaussPointWeight(7) - 0.1234567901234569 ) > tol ){
        test_fail = true;
        cout << "2D gauss pts 8 weight " << elt->gaussPointWeight(7) << endl;
    }
    if ( fabs(elt->gaussPointWeight(8) - 0.07716049382716063 ) > tol ){
        test_fail = true;
        cout << "2D gauss pts 9 weight " << elt->gaussPointWeight(8) << endl;
    }

    return test_fail;
}
