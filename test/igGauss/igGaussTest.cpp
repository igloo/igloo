
#include <iostream>
#include <math.h>

#include "../../src/igCore/igBasisBezier.h"
#include "igCore/igElement.h"

using namespace std;


// main program
int main(int argc, char **argv)
{

    bool test_fail = false;
    double tol = 1.E-15;

    igElement new_elt;

    igBasisBezier fun(3);

    vector<double> *coordinates = new vector<double>;
    coordinates->resize(4, 0.);
    
    vector<double> *coord = new vector<double>;
    coord->resize(1);

    new_elt.setPhysicalDimension(1);
    new_elt.setParametricDimension(1);
    new_elt.setDegree(3);
    new_elt.setGaussPointNumberByDirection(4);
    new_elt.setVariableNumber(1);
    new_elt.setDerivativeNumber(0);
    new_elt.setCoordinates(coordinates);
    
    new_elt.initializeDegreesOfFreedom();

    
    new_elt.setGlobalId(0, 0, 0);
    new_elt.setGlobalId(1, 0, 1);
    new_elt.setGlobalId(2, 0, 2);
    new_elt.setGlobalId(3, 0, 3);
    
    coordinates->at(0) = 0.;
    coordinates->at(1) = 1./3.;
    coordinates->at(2) = 2./3.;
    coordinates->at(3) = 1.;

    new_elt.initializeGeometry();


//    // test number of Gauss points
   int n0 = new_elt.gaussPointNumber();

    if( n0 != 4 ){
        test_fail = true;
//        cout << "gauss pt number " << n0 << endl;
    }

//    // test weights
    double w10 = new_elt.gaussPointWeight(0);
    double w11 = new_elt.gaussPointWeight(1);
    double w12 = new_elt.gaussPointWeight(2);
    double w13 = new_elt.gaussPointWeight(3);

    if ( fabs(w10 - 0.347854845137454/2 ) > tol ||
         fabs(w11 - 0.652145154862545/2 ) > tol ||
         fabs(w12 - 0.652145154862545/2 ) > tol ||
         fabs(w13 - 0.347854845137454/2 ) > tol
         ){
        test_fail = true;
//        cout << "gauss pt weights " << endl;
//        for (int i=0;i<4;i++){
//            cout << new_elt.gaussPointWeight(i) << endl;
//        }
    }


//    // test some function values

    vector<double> values;
    values.resize(4);
    fun.evalFunction(new_elt.gaussPointParametricCoordinate(2,0),values);

    double f020 = new_elt.gaussPointFunctionValue(2,0);
    double f021 = new_elt.gaussPointFunctionValue(2,1);
    double f022 = new_elt.gaussPointFunctionValue(2,2);
    double f023 = new_elt.gaussPointFunctionValue(2,3);


    if ( fabs(f020 - values.at(0) ) > tol ||
         fabs(f021 - values.at(1) ) > tol ||
         fabs(f022 - values.at(2) ) > tol ||
         fabs(f023 - values.at(3) ) > tol
         )
         test_fail = true;

    // test some gradient values

    values.resize(4);
    fun.evalGradient(new_elt.gaussPointParametricCoordinate(3,0),values);

    double f130 = new_elt.gaussPointGradientValue(3,0,0);
    double f131 = new_elt.gaussPointGradientValue(3,1,0);
    double f132 = new_elt.gaussPointGradientValue(3,2,0);
    double f133 = new_elt.gaussPointGradientValue(3,3,0);

    if ( fabs(f130 - values.at(0) ) > tol ||
         fabs(f131 - values.at(1) ) > tol ||
         fabs(f132 - values.at(2) ) > tol ||
         fabs(f133 - values.at(3) ) > tol
         ){
        test_fail = true;
//        cout << "basis gradient ref: " <<  values.at(0) << "value : " << f130 << endl;
//        cout << "basis gradient ref: " <<  values.at(1) << "value : " << f131 << endl;
//        cout << "basis gradient ref: " <<  values.at(2) << "value : " << f132 << endl;
//        cout << "basis gradient ref: " <<  values.at(3) << "value : " << f133 << endl;
    }

    // test Jacobian
    double jacobian = new_elt.gaussPointJacobian(2);

    if( fabs(jacobian - 1.) > tol ){
        test_fail = true;
//        cout << "jacobian ref: " <<  3. << " value : " << jacobian << endl;
    }

    delete coord;

    return test_fail;
}
