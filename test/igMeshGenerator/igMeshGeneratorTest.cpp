
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <math.h>

#include <igGenerator/igMeshGenerator.h>
#include <igGenerator/igMeshGeneratorFitting.h>
#include <igGenerator/igMeshGeneratorSplitting.h>
#include <igGenerator/igShapeAnalytic.h>
#include <igDistributionAnalytic.h>

using namespace std;


// main program
int main(int argc, char **argv)
{
    bool test_fail = false;
    double tol = 1.E-15;

    function<double(double, double, double )> x_fun = x_id;
    function<double(double, double, double )> y_fun = y_id;
    function<double(double, double, double )> z_fun = z_id;

    function<double(int, int, double, double)> distrib_xi_fun = distrib_xi_id_fun;
    function<double(int, int, double, double)> distrib_eta_fun = distrib_eta_id_fun;
    function<double(int, int, double, double)> distrib_zeta_fun = distrib_zeta_id_fun;

    vector<double> xcoord_in;
    vector<double> ycoord_in;
    vector<double> weight_in;

    //------------- test generator fitting --------------------

    igMeshGeneratorFitting *gen_fitting = new igMeshGeneratorFitting;

    gen_fitting->setDegree(3);
    gen_fitting->setElementNumber(2,2,0);
    gen_fitting->setVariableNumber(4);
    gen_fitting->setDerivativeNumber(4);
    gen_fitting->setGaussPointNumber(5);
    gen_fitting->setDomainBounds(-1.5, 1.5, 0., 0.8, 0., 1.);
    gen_fitting->setRotation(0.);
    gen_fitting->setScale(1.);
    gen_fitting->setXFunction(x_fun);
    gen_fitting->setYFunction(y_fun);
    gen_fitting->setZFunction(z_fun);
    gen_fitting->setDitributionXiFunction(distrib_xi_fun);
    gen_fitting->setDitributionEtaFunction(distrib_eta_fun);
    gen_fitting->setDitributionZetaFunction(distrib_zeta_fun);
    gen_fitting->setBoundaryConditions(1,2,1,2,0,0);

    gen_fitting->generate();

    gen_fitting->setExactSolutionId(0);


    if( fabs(gen_fitting->xCoordinate()[0] + 1.5) > tol ){
        test_fail = true;
        cout << setprecision(16) << "mesh generator fitting = point 0" << " reference " << -1.5 << " x mesh " << gen_fitting->xCoordinate()[0] << endl;
    }
    if( fabs(gen_fitting->xCoordinate()[15] - 0.) > tol ){
        test_fail = true;
        cout << setprecision(16) << "mesh generator fitting = point 15" << " reference " << 0. << " x mesh " << gen_fitting->xCoordinate()[15] << endl;
    }
    if( fabs(gen_fitting->xCoordinate()[31] - 1.5) > tol ){
        test_fail = true;
        cout << setprecision(16) << "mesh generator fitting = point 31" << " reference " << 1.5 << " x mesh " << gen_fitting->xCoordinate()[31] << endl;
    }
    if( fabs(gen_fitting->xCoordinate()[45] + 1.) > tol ){
        test_fail = true;
        cout << setprecision(16) << "mesh generator fitting = point 45" << " reference " << -1. << " x mesh " << gen_fitting->xCoordinate()[45] << endl;
    }
    if( fabs(gen_fitting->yCoordinate()[0] - 0.) > tol ){
        test_fail = true;
        cout << setprecision(16) << "mesh generator fitting = point 0" << " reference " << 0. << " y mesh " << gen_fitting->yCoordinate()[0] << endl;
    }
    if( fabs(gen_fitting->yCoordinate()[15] - 0.4) > tol ){
        test_fail = true;
        cout << setprecision(16) << "mesh generator fitting = point 15" << " reference " << 0.4 << " y mesh " << gen_fitting->yCoordinate()[15] << endl;
    }
    if( fabs(gen_fitting->yCoordinate()[31] - 0.4) > tol ){
        test_fail = true;
        cout << setprecision(16) << "mesh generator fitting = point 31" << " reference " << 0.4 << " y mesh " << gen_fitting->yCoordinate()[31] << endl;
    }
    if( fabs(gen_fitting->yCoordinate()[45] - 0.8) > tol ){
        test_fail = true;
        cout << setprecision(16) << "mesh generator fitting = point 45" << " reference " << 0.8 << " y mesh " << gen_fitting->yCoordinate()[45] << endl;
    }



    //------------ test generator splitting ----------------------

    igMeshGeneratorSplitting *gen_splitting = new igMeshGeneratorSplitting;

    xcoord_in.resize(9);
    ycoord_in.resize(9);
    weight_in.resize(9);

    xcoord_in.at(0) = 1.;
    ycoord_in.at(0) = 0.;
    weight_in.at(0) = 1.;

    xcoord_in.at(1) = 2.5;
    ycoord_in.at(1) = 0.;
    weight_in.at(1) = 1.;

    xcoord_in.at(2) = 4.;
    ycoord_in.at(2) = 0.;
    weight_in.at(2) = 1.;

    xcoord_in.at(3) = 1.;
    ycoord_in.at(3) = 1.;
    weight_in.at(3) = sqrt(2.)*0.5;

    xcoord_in.at(4) = 2.5;
    ycoord_in.at(4) = 2.5;
    weight_in.at(4) = sqrt(2.)*0.5;

    xcoord_in.at(5) = 4.;
    ycoord_in.at(5) = 4.;
    weight_in.at(5) = sqrt(2.)*0.5;

    xcoord_in.at(6) = 0.;
    ycoord_in.at(6) = 1.;
    weight_in.at(6) = 1.;

    xcoord_in.at(7) = 0.;
    ycoord_in.at(7) = 2.5;
    weight_in.at(7) = 1.;

    xcoord_in.at(8) = 0.;
    ycoord_in.at(8) = 4;
    weight_in.at(8) = 1.;

    distrib_xi_fun = distrib_xi_cylinder_fun;
    distrib_eta_fun = distrib_eta_cylinder_fun;

    gen_splitting->setDegree(2);
    gen_splitting->setElementNumber(2,4,0);
    gen_splitting->setGaussPointNumber(5);
    gen_splitting->setDomainBounds(-1.5, 1.5, 0., 0.8, 0., 1.);
    gen_splitting->setRotation(0.);
    gen_splitting->setScale(1.);
    gen_splitting->setXFunction(x_fun);
    gen_splitting->setYFunction(y_fun);
    gen_splitting->setDitributionXiFunction(distrib_xi_fun);
    gen_splitting->setDitributionEtaFunction(distrib_eta_fun);
    gen_splitting->setInitialPatch(xcoord_in,ycoord_in,weight_in,1,1);
    gen_splitting->setBoundaryConditions(1,2,1,2,0,0);

    gen_splitting->generate();

    gen_splitting->setExactSolutionId(0);

    if( fabs(gen_splitting->xCoordinate()[0] - 1.) > tol ){
        test_fail = true;
        cout << setprecision(16) << "mesh generator splitting = point 0" << " reference " << 1. << " x mesh " << gen_splitting->xCoordinate()[0] << endl;
    }
    if( fabs(gen_splitting->xCoordinate()[15] - 2.309698831278217) > tol ){
        test_fail = true;
        cout << setprecision(16) << "mesh generator splitting = point 15" << " reference " << 2.309698831278217 << " x mesh " << gen_splitting->xCoordinate()[15] << endl;
    }
    if( fabs(gen_splitting->xCoordinate()[31] - 2.755216961323364) > tol ){
        test_fail = true;
        cout << setprecision(16) << "mesh generator splitting = point 31" << " reference " << 2.755216961323364 << " x mesh " << gen_splitting->xCoordinate()[31] << endl;
    }
    if( fabs(gen_splitting->xCoordinate()[45] - 1.767766952966369) > tol ){
        test_fail = true;
        cout << setprecision(16) << "mesh generator splitting = point 45" << " reference " << 1.767766952966369 << " x mesh " << gen_splitting->xCoordinate()[45] << endl;
    }
    if( fabs(gen_splitting->yCoordinate()[0] - 0.) > tol ){
        test_fail = true;
        cout << setprecision(16) << "mesh generator splitting = point 0" << " reference " << 0. << " y mesh " << gen_splitting->yCoordinate()[0] << endl;
    }
    if( fabs(gen_splitting->yCoordinate()[15] - 0.9567085809127245) > tol ){
        test_fail = true;
        cout << setprecision(16) << "mesh generator splitting = point 15" << " reference " << 0.9567085809127245 << " y mesh " << gen_splitting->yCoordinate()[15] << endl;
    }
    if( fabs(gen_splitting->yCoordinate()[31] - 1.840977116389195) > tol ){
        test_fail = true;
        cout << setprecision(16) << "mesh generator splitting = point 31" << " reference " << 1.840977116389195 << " y mesh " << gen_splitting->yCoordinate()[31] << endl;
    }
    if( fabs(gen_splitting->yCoordinate()[45] - 1.767766952966369) > tol ){
        test_fail = true;
        cout << setprecision(16) << "mesh generator splitting = point 45" << " reference " << 1.767766952966369 << " y mesh " << gen_splitting->yCoordinate()[45] << endl;
    }


    delete gen_fitting;
    delete gen_splitting;

    return test_fail;

}
