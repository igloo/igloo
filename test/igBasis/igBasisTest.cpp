
#include <iostream>
#include <math.h>

#include "../../src/igCore/igBasisBezier.h"

using namespace std;


// main program
int main(int argc, char **argv)
{

    igBasisBezier basis(3);
    bool test_fail = false;
    double tol = 1.E-15;

    vector<double> values;
    values.resize(4);

    for (int i=0; i<11; i++){

        double var = float(i)/(10);

        // test function evaluation
        basis.evalFunction(var,values);

        if( fabs(values.at(0) - (1.-var)*(1.-var)*(1.-var) ) > tol ||
            fabs(values.at(1) - 3*var*(1.-var)*(1.-var) ) > tol ||
            fabs(values.at(2) - 3*var*var*(1.-var) ) > tol ||
            fabs(values.at(3) - var*var*var) > tol )
            test_fail = true;


        // test gradient evaluation
        basis.evalGradient(var,values);

        if( fabs(values.at(0)  +3*(1.-var)*(1.-var) ) > tol ||
            fabs(values.at(1)  -3*(1.-var)*(1.-var) +6*var*(1.-var) ) > tol ||
            fabs(values.at(2)  -6*var*(1.-var) +3*var*var ) > tol ||
            fabs(values.at(3)  -3*var*var) > tol )
            test_fail = true;

    }

    return test_fail;

}
