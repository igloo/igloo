#include <iostream>
#include <iomanip>
#include <math.h>

#include <igSolver/igFlux.h>
#include <igSolver/igFluxAdvection.h>
#include <igSolver/igFluxBurger.h>
#include <igSolver/igFluxViscousBurger.h>
#include <igSolver/igFluxViscousBurgerGradient.h>
#include <igSolver/igFluxEuler2D.h>
#include <igSolver/igFluxEuler3D.h>
#include <igSolver/igFluxNavierStokes2D.h>
#include <igSolver/igFluxNavierStokes3D.h>
#include <igSolver/igFluxNavierStokesGradient2D.h>
#include <igSolver/igFluxNavierStokesGradient3D.h>

using namespace std;

// main program
int main(int argc, char **argv)
{

    bool test_fail = false;
    double tol = 1.E-13;

    double *normal = new double[1];
    normal[0] = 1.;

    vector<double> *state_left = new vector<double>;
    state_left->resize(1);
    vector<double> *derivative_left = new vector<double>;
    derivative_left->resize(1);
    vector<double> *flux_left = new vector<double>;
    flux_left->resize(1);
    vector<double> *state_right = new vector<double>;
    state_right->resize(1);
    vector<double> *derivative_right = new vector<double>;
    derivative_right->resize(1);
    vector<double> *flux_right = new vector<double>;
    flux_right->resize(1);
    vector<double> *mesh_vel = new vector<double>;
    mesh_vel->resize(2,0.);

    //--------------- test flux advection ----------------

    igFluxAdvection *flux_ad = new igFluxAdvection(1,1,0);

    state_left->at(0) = 1.256;
    derivative_left->at(0) = 4.567;

    flux_ad->physical(state_left,derivative_left,mesh_vel,flux_left);

    if( fabs(flux_left->at(0) - 1.256) > tol ){
            test_fail = true;
            cout << setprecision(16) << "physical advection flux" << " reference 1.256" << " calcul " << flux_left->at(0) << endl;
        }

    state_right->at(0) = 3.567;
    derivative_right->at(0) = 7.567;

    flux_ad->numerical(state_left,derivative_left,state_right,derivative_right,
                       flux_left,flux_right,mesh_vel,normal);

    if( fabs(flux_left->at(0) - 1.256) > tol  ||
        fabs(flux_right->at(0) - 1.256) > tol  ){
        test_fail = true;
        cout << setprecision(16) << "numerical advection flux" << " reference 1.256" << " calcul " << flux_left->at(0) << endl;
    }


    double speed = flux_ad->waveSpeed(state_left,mesh_vel);

    if( fabs(speed - 1.) > tol ){
        test_fail = true;
        cout << setprecision(16) << "advection speed" << " reference 1." << " calcul " << speed << endl;
    }

    delete flux_ad;

    //---------------- test flux advection 2D -----------

    igFluxAdvection *flux_ad2D = new igFluxAdvection(1,2,0);

    double *normal2D = new double[2];
    normal2D[0] = 1.;
    normal2D[1] = 0.5;

    vector<double> *flux = new vector<double>;
    flux->resize(2);

    state_left->at(0) = 1.256;
    derivative_left->at(0) = 4.567;

    flux_ad2D->physical(state_left,derivative_left,mesh_vel,flux);

    if( fabs(flux->at(0) - 1.256) > tol ){
        test_fail = true;
        cout << setprecision(16) << "physical advection2D flux 1" << " reference 1.256" << " calcul " << flux->at(0) << endl;
    }
    if( fabs(flux->at(1) - 1.256) > tol ){
        test_fail = true;
        cout << setprecision(16) << "physical advection2D flux 2" << " reference 1.256" << " calcul " << flux->at(1) << endl;
    }

    state_right->at(0) = 3.567;
    derivative_right->at(0) = 7.567;

    flux_ad2D->numerical(state_left,derivative_left,state_right,derivative_right,
                       flux_left,flux_right,mesh_vel,normal2D);

    if( fabs(flux_left->at(0) - 1.884) > tol  ||
        fabs(flux_right->at(0) - 1.884) > tol  ){
        test_fail = true;
        cout << setprecision(16) << "numerical advection2D flux" << " reference 1.884" << " calcul " << flux_left->at(0) << endl;
    }


    speed = flux_ad2D->waveSpeed(state_left,mesh_vel);

    if( fabs(speed - 1.414213562373095) > tol ){
        test_fail = true;
        cout << setprecision(16) << "advection speed 2D" << " reference 1.414213562373095" << " calcul " << speed << endl;
    }

    delete flux_ad2D;



    //--------------- test flux Burger ----------------

    igFluxBurger *flux_b = new igFluxBurger(1,1,0);

    state_left->at(0) = 1.256;
    derivative_left->at(0) = 4.567;

    flux_b->physical(state_left,derivative_left,mesh_vel,flux_left);

    if( fabs(flux_left->at(0) - 0.788768) > tol ){
        test_fail = true;
        cout << setprecision(16) << "physical burger flux" << " reference 0.788768" << " calcul " << flux_left->at(0) << endl;
    }

    state_right->at(0) = 3.567;
    derivative_right->at(0) = 7.567;

    flux_b->numerical(state_left,derivative_left,state_right,derivative_right,
                       flux_left,flux_right,mesh_vel,normal);

    if( fabs(flux_left->at(0) + 0.54641225) > tol  ||
        fabs(flux_right->at(0) + 0.54641225) > tol  ){
        test_fail = true;
        cout << setprecision(16) << "numerical burger flux" << " reference -0.54641225" << " calcul " << flux_left->at(0) << endl;
    }

    speed = flux_b->waveSpeed(state_left,mesh_vel);

    if( fabs(speed - 1.256) > tol ){
        test_fail = true;
        cout << setprecision(16) << "burger speed" << " reference 1.256" << " calcul " << speed << endl;
    }

    delete flux_b;


    //--------------- test flux viscous Burger ----------------

    igFluxViscousBurger *flux_vb = new igFluxViscousBurger(1,1,0);
    flux_vb->setDiffusionCoefficient(0.1);

    igFluxViscousBurgerGradient *flux_vbg = new igFluxViscousBurgerGradient(1,1,0);
    flux_vbg->setDiffusionCoefficient(0.1);

    vector<double> *ref = new vector<double>;
    ref->resize(1);

    state_left->resize(1);
    state_right->resize(1);
    derivative_left->resize(1);
    derivative_right->resize(1);
    flux_left->resize(1);
    flux_right->resize(1);

    state_left->at(0) = 1.2;
    derivative_left->at(0) = 4.5;

    flux_vb->physical(state_left,derivative_left,mesh_vel,flux_left);

    ref->at(0) = -0.703024947075771;

    for (int i=0; i<1; i++){
        if( fabs(flux_left->at(i) - ref->at(i)) > tol){
            test_fail = true;
            cout << setprecision(16) << "physical flux u " << i << " reference " << ref->at(i) << " calcul " << flux_left->at(i) << endl;
        }

    }

    flux_vbg->physical(state_left,derivative_left,mesh_vel,flux_left);

    ref->at(0) = -0.379473319220205;

    for (int i=0; i<1; i++){
        if( fabs(flux_left->at(i) - ref->at(i)) > tol){
            test_fail = true;
            cout << setprecision(16) << "physical flux q " << i << " reference " << ref->at(i) << " calcul " << flux_left->at(i) << endl;
        }

    }

    state_right->at(0) = 3.5;
    derivative_right->at(0) = 7.5;
    flux_vb->numerical(state_left,derivative_left,state_right,derivative_right,
                       flux_left,flux_right,mesh_vel,normal);

    ref->at(0) = -2.02552494707577;

    for (int i=0; i<1; i++){
        if( fabs(flux_left->at(i) - ref->at(i)) > tol){
            test_fail = true;
            cout << setprecision(17) << "numerical flux u " << i << " reference " << ref->at(i) << " calcul " << flux_left->at(i) << endl;
        }

    }


    flux_vbg->numerical(state_left,derivative_left,state_right,derivative_right,
                       flux_left,flux_right,mesh_vel,normal);


    ref->at(0) = -1.106797181058933;

    for (int i=0; i<1; i++){
        if( fabs(flux_left->at(i) - ref->at(i)) > tol){
            test_fail = true;
            cout << setprecision(17) << "numerical flux q " << i << " reference " << ref->at(i) << " calcul " << flux_left->at(i) << endl;
        }

    }


    speed = flux_vb->waveSpeed(state_left,mesh_vel);

    if( fabs(speed - 1.2) > tol ){
        test_fail = true;
        cout << setprecision(17) << "wave speed  reference " << 1.2 << " calcul " << speed << endl;
    }

    speed = flux_vbg->waveSpeed(state_left,mesh_vel);

    if( fabs(speed) > tol )
        test_fail = true;

    delete flux_vb;
    delete flux_vbg;


    //---------------- test flux Euler 2D -----------

    igFluxEuler2D *flux_euler2D = new igFluxEuler2D(4,2,0);

    flux_left->resize(8);

    state_left->resize(4);
    state_left->at(0) = 1.;
    state_left->at(1) = 2.;
    state_left->at(2) = -3.;
    state_left->at(3) = 14.;

    ref->resize(8);
    ref->at(0) = 2.;
    ref->at(1) = -3.;
    ref->at(2) = 7.;
    ref->at(3) = -6.;
    ref->at(4) = -6.;
    ref->at(5) = 12.;
    ref->at(6) = 34.;
    ref->at(7) = -51.;

    flux_euler2D->physical(state_left,derivative_left,mesh_vel,flux_left);

    for (int i=0; i<8; i++){
        if( fabs(flux_left->at(i) - ref->at(i)) > tol ){
            test_fail = true;
            cout << setprecision(16) << "physical euler flux " << i << " reference " << ref->at(i) << " calcul " << flux_left->at(i) << endl;
        }
    }


    state_right->resize(4);
    state_right->at(0) = 2.;
    state_right->at(1) = -3.;
    state_right->at(2) = 4.;
    state_right->at(3) = 12.;

    flux_left->resize(4);
    flux_right->resize(4);

    ref->resize(4);
    ref->at(0) = -1.158727179517139;
    ref->at(1) = 9.139531501837526;
    ref->at(2) = -7.65821916324727;
    ref->at(3) = 4.177967738163723;

    flux_euler2D->numerical(state_left,derivative_left,state_right,derivative_right,
                       flux_left,flux_right,mesh_vel,normal2D);


    for (int i=0; i<4; i++){
        if( fabs(flux_left->at(i) - ref->at(i)) > tol ){
            test_fail = true;
            cout << setprecision(16) << "numerical euler flux " << i << " reference " << ref->at(i) << " calcul " << flux_left->at(i) << endl;
        }
    }

    delete flux_euler2D;



    //---------------- test flux Navier-Stokes 2D -----------

    igFluxNavierStokes2D *flux_ns = new igFluxNavierStokes2D(4,2,0);
    flux_ns->setDiffusionCoefficient(0.5);

    flux_left->resize(8);

    state_left->resize(4);
    state_left->at(0) = 1.;
    state_left->at(1) = 2.;
    state_left->at(2) = -3.;
    state_left->at(3) = 14.;

    derivative_left->resize(8);
    derivative_left->at(0) = 1.;
    derivative_left->at(1) = -1.;
    derivative_left->at(2) = -3.;
    derivative_left->at(3) = 1.;
    derivative_left->at(4) = -1.;
    derivative_left->at(5) = 2.;
    derivative_left->at(6) = -3.;
    derivative_left->at(7) = 1.;

    ref->resize(8);
    ref->at(0) = 2.;
    ref->at(1) = -3.;
    ref->at(2) = 10.;
    ref->at(3) = -8.5;
    ref->at(4) = -8.5;
    ref->at(5) = 11.;
    ref->at(6) = 48.47222222222222;
    ref->at(7) = -58.83333333333334;

    flux_ns->physical(state_left,derivative_left,mesh_vel,flux_left);

    for (int i=0; i<8; i++){
        if( fabs(flux_left->at(i) - ref->at(i)) > tol ){
            test_fail = true;
            cout << setprecision(16) << "physical navier-stokes flux " << i << " reference " << ref->at(i) << " calcul " << flux_left->at(i) << endl;
        }
    }

    state_right->resize(4);
    state_right->at(0) = 2.;
    state_right->at(1) = -3.;
    state_right->at(2) = 4.;
    state_right->at(3) = 12.;

    derivative_right->resize(8);
    derivative_right->at(0) = -1.;
    derivative_right->at(1) = 2.;
    derivative_right->at(2) = -3.;
    derivative_right->at(3) = 1.;
    derivative_right->at(4) = 1.;
    derivative_right->at(5) = -2.;
    derivative_right->at(6) = -3.;
    derivative_right->at(7) = 4.;

    flux_left->resize(4);
    flux_right->resize(4);

    ref->resize(12);
    ref->at(0) = -1.158727179517139;
    ref->at(1) = 9.827031501837526;
    ref->at(2) = -9.720719163247271;
    ref->at(3) = 9.766509404830389;

    flux_ns->numerical(state_left,derivative_left,state_right,derivative_right,
                       flux_left,flux_right,mesh_vel,normal2D);

    for (int i=0; i<4; i++){
        if( fabs(flux_left->at(i) - ref->at(i)) > tol ){
            test_fail = true;
            cout << setprecision(16) << "numerical navier-stokes flux " << i << " reference " << ref->at(i) << " calcul " << flux_left->at(i) << endl;
        }
    }

    speed = flux_ns->waveSpeed(state_left,mesh_vel);

    if( fabs(speed - 5.654941428655908) > tol ){
        test_fail = true;
        cout << setprecision(16) << "navier-stokes speed 2D" << " reference 5.654941428655908" << " calcul " << speed << endl;
    }

    delete flux_ns;


    //---------------- test flux Navier-Stokes gradient 2D -----------

    igFluxNavierStokesGradient2D *flux_nsgrad = new igFluxNavierStokesGradient2D(4,2,0);

    flux_left->resize(16);

    state_left->resize(4);
    state_left->at(0) = 1.;
    state_left->at(1) = 2.;
    state_left->at(2) = -3.;
    state_left->at(3) = 14.;

    ref->resize(16);
    ref->at(0) = -1.;
    ref->at(1) = 0.;
    ref->at(2) = 0.;
    ref->at(3) = -1.;
    ref->at(4) = -2.;
    ref->at(5) = 0.;
    ref->at(6) = 0.;
    ref->at(7) = -2.;
    ref->at(8) = 3.;
    ref->at(9) = 0.;
    ref->at(10) = 0.;
    ref->at(11) = 3.;
    ref->at(12) = -14.;
    ref->at(13) = 0.;
    ref->at(14) = 0.;
    ref->at(15) = -14.;

    flux_nsgrad->physical(state_left,derivative_left,mesh_vel,flux_left);

    for (int i=0; i<16; i++){
        if( fabs(flux_left->at(i) - ref->at(i)) > tol ){
            test_fail = true;
            cout << setprecision(16) << "physical navier-stokes gradient flux " << i << " reference " << ref->at(i) << " calcul " << flux_left->at(i) << endl;
        }
    }

    state_right->resize(4);
    state_right->at(0) = 2.;
    state_right->at(1) = -3.;
    state_right->at(2) = 4.;
    state_right->at(3) = 12.;

    flux_left->resize(8);
    flux_right->resize(8);

    ref->resize(8);
    ref->at(0) = -1.5;
    ref->at(1) = -0.75;
    ref->at(2) = 0.5;
    ref->at(3) = 0.25;
    ref->at(4) = -0.5;
    ref->at(5) = -0.25;
    ref->at(6) = -13;
    ref->at(7) = -6.5;

    flux_nsgrad->numerical(state_left,derivative_left,state_right,derivative_right,
                       flux_left,flux_right,mesh_vel,normal2D);

    for (int i=0; i<8; i++){
        if( fabs(flux_left->at(i) - ref->at(i)) > tol ){
            test_fail = true;
            cout << setprecision(16) << "numerical navier-stokes gradient flux " << i << " reference " << ref->at(i) << " calcul " << flux_left->at(i) << endl;
        }
    }


    delete flux_nsgrad;



    return test_fail;

}
