    
project( igFluxTest )

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/bin)

add_executable( igFluxTest igFluxTest.cpp )

target_link_libraries(
  igFluxTest
  igSolver )

add_test( igFluxTest
  ${PROJECT_BINARY_DIR}/bin/igFluxTest )

