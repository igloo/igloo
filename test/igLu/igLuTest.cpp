
#include <iostream>
#include <math.h>

#include <igCore/igLinAlg.h>

using namespace std;


// main program
int main(int argc, char **argv)
{

    bool test_fail = false;
    double tol = 1.E-15;

    double A [3*3] = {
        0.4, 0.2, 0.0666666666666667,
        0.2, 0.266666666666666667, 0.2,
        0.0666666666666666667, 0.2, 0.4
    };

    igLinAlg lin_solver;
    lin_solver.inverse(A, 3);

    if ( fabs( A[0] - 4.5 ) > tol ||
         fabs( A[1] + 4.5 ) > tol ||
         fabs( A[2] - 1.5 ) > tol ||
         fabs( A[3] + 4.5 ) > tol ||
         fabs( A[4] - 10.5 ) > tol ||
         fabs( A[5] + 4.5 ) > tol ||
         fabs( A[6] - 1.5 ) > tol ||
         fabs( A[7] + 4.5 ) > tol ||
         fabs( A[8] - 4.5 ) > tol
         )
         test_fail = true;

    return test_fail;
}
