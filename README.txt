***************************************************************************
        Igloo, Iso-Geometric Libraries for discOntinuOus galerkin
                   copyright INRIA 2016-2020
****************************************************************************

Igloo (Iso-Geometric Libraries for discOntinuOus galerkin) is a software suite,
supported by Inria Acumes Project-Team, dedicated to the implementation of the
isogeometric paradigm in the DG (Discontinuous Galerkin) framework. Igloo is an
object-oriented, cross-platform, C++ software, that aims at integrating CAD
(Computer-Aided Design) features and simulation components in a single
geometrically-consistent tool.

Igloo software is licensed under the GNU General Public License v3.



DOWNLOAD:
--------

Igloo source code can be downloaded from Inria GitLab:

Using ssh:
git clone git@gitlab.inria.fr:igloo/igloo.git

Using https:
git clone https://gitlab.inria.fr/igloo/igloo.git



COMPILATION:
-----------

Prerequisites

Igloo uses standard C++ and has only a few dependencies.
C++ compiler and CMake system are mandatory for compiling the code, whereas
Blas/Lapack and MPI libraries are needed for building. All required or
advised components are detailed below.

Operating system
Igloo is usually used on Linux and Mac OS X systems. MS Windows could also
be used, provided that all prerequisites are satisfied.

Configuration
To configure the code, CMake 3.6.0 or newer should be installed.

Compilation
Any recent version of C++ compiler can be used, such as GNU GCC, Clang, Intel C++ compiler.

Building
Code building requires the following libraries to be installed:

Blas/Lapack 3.6.1 or newer
OpenMPI 2.0.2 or newer

Documentation
Doxygen should be installed to build the code documentation.

Post-processing
We recommend to install GLvis software as visualization tool.

Configuration and building
The configuration is achieved using standard CMake commands on Linux and
Mac OS X systems. We denote  $IGLOO_HOME the top Igloo directory and we
advise to compile the code as follows:

cd $IGLOO_HOME
mkdir build
cd build
cmake ..

Advanced compilation options can obviously be selected using standard CMake
syntax, e.g. CMAKE_BUILD_TYPE, CMAKE_INSTALL_PREFIX, etc.
The libraries and applications can then be compiled using:

make

Installation is achieved using:

make install

The documentation is built using:

make doc

Applications can then be found in $IGLOO_HOME/build/bin directory and libraries
in $IGLOO_HOME/build/lib directory.



TESTS:
------

A set of unitary tests are implemented using the CMake test framework. They can
be launched directly from the build directory:

cd build
make test

Igloo contains a set of test-cases corresponding to complete geometry + simulation
pipelines, including comparisons to reference outputs. This constitutes both
non-regression tests and baseline tutorials. They can be launched from the case
directory:

cd case
./run_all.sh



WIKI:
-----

More information can be found on wiki pages :
https://gitlab.inria.fr/igloo/igloo/-/wikis/home
